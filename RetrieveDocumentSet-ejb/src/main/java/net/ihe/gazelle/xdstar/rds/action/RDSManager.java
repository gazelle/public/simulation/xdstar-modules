/*
 * Copyright 2012 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.rds.action;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.ParserConfigurationException;

import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.xua.SignatureException;
import net.ihe.gazelle.xdstar.common.model.AttachmentFile;
import net.ihe.gazelle.xdstar.common.model.ContentType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import net.ihe.gazelle.xdstar.comon.util.ConfigurationFilter;
import net.ihe.gazelle.xdstar.comon.util.Pair;
import net.ihe.gazelle.xdstar.comon.util.Util;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespacePrefixMapper;
import net.ihe.gazelle.xdstar.conf.model.RepositoryConfiguration;
import net.ihe.gazelle.xdstar.conf.model.RepositoryConfigurationQuery;
import net.ihe.gazelle.xdstar.core.AttachmentsUtil;
import net.ihe.gazelle.xdstar.core.MTOMBuilder;
import net.ihe.gazelle.xdstar.core.MTOMElement;
import net.ihe.gazelle.xdstar.core.MessageSenderCommon;
import net.ihe.gazelle.xdstar.core.soap.SOAPRequestBuilder;
import net.ihe.gazelle.xdstar.rds.common.CommonRetrieveManager;
import net.ihe.gazelle.xdstar.rds.model.RDSMessage;

import org.apache.axiom.attachments.Attachments;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  
 * @author                	Abderrazek Boufahja / INRIA Rennes IHE development Project
 *
 */

@Name("rDSManagerBean")
@Scope(ScopeType.PAGE)
public class RDSManager extends CommonRetrieveManager<RepositoryConfiguration, RDSMessage> implements Serializable {

	private static final String ENABLE_TO_CREATE_THE_MESSAGE = "Enable to create the message.";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final static String TRANSACTION_TYPE = "RDS";

	
	private static Logger log = LoggerFactory.getLogger(RDSManager.class);

	@Override
	public void listAllConfigurations() {
		RepositoryConfigurationQuery rc = new RepositoryConfigurationQuery();
		if (selectedAffinityDomain != null) {
			rc.listUsages().affinity().eq(selectedAffinityDomain);
		}
		if (selectedTransaction != null) {
			rc.listUsages().transaction().eq(selectedTransaction);
		}
		rc.name().order(true);
		ConfigurationFilter.filterConfigurationQueryAccodingToUserCredential(rc);
		configurations = rc.getList();
	}

	@Override
	public void updateSpecificClient() {
		this.initSelectedRequest();
		
	}
	

	@Override
	public List<AffinityDomain> listAllAffinityDomains() {
		return this.listAllAffinityDomains(RDSManager.TRANSACTION_TYPE);
	}
	
	public String previewMessage() throws IOException{
		try {
			String soap = createMessage();
			Pair<String, byte[]> pp = buildMTOMFromData(soap);
			return new String(pp.getObject2()); 
		} catch (ParserConfigurationException e) {
			log.info(ENABLE_TO_CREATE_THE_MESSAGE, e);
		} catch (SignatureException e) {
			log.info(ENABLE_TO_CREATE_THE_MESSAGE, e);
		} catch (JAXBException e) {
			log.info(ENABLE_TO_CREATE_THE_MESSAGE, e);
		}
		return null;
	}
	
	public String createMessage() throws ParserConfigurationException, SignatureException, JAXBException{
		String res = "";
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		save(baos, this.selectedRetrieveDocumentSetRequest);
		res = SOAPRequestBuilder.createSOAPMessage(baos.toString(), useXUA, praticianID, attributes, null, this.selectedConfiguration.getUrl(), "urn:ihe:iti:2007:RetrieveDocumentSet", false);
		return res;
	}
	
	public static Pair<String, byte[]> buildMTOMFromData(String soapMessage) throws IOException{
		List<MTOMElement> listDataToSend = new ArrayList<MTOMElement>();
		MTOMElement soap = new MTOMElement();
		soap.setContent(("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + soapMessage + "\r\n").getBytes(StandardCharsets.UTF_8));
		soap.setContentType(" application/xop+xml; charset=UTF-8; type=\"application/soap+xml\"");
		soap.setTransfertEncoding("binary");
		soap.setIndex(0);
		soap.setUuid(UUID.randomUUID().toString().replace("-", "").toUpperCase());
		listDataToSend.add(soap);
		
		byte[] body = MTOMBuilder.buildMTOM(listDataToSend);
		
		String boundaryUUID = MTOMBuilder.getBoundaryUUIDFromGeneratedMTOM(body);
		
		String boundary = "MIMEBoundaryurn_uuid_" + boundaryUUID;
		String boundaryStart = "0.urn:uuid:" + soap.getUuid() + "@ws.jboss.org";
		String contentTypeProperties = "multipart/related; ";
		contentTypeProperties = contentTypeProperties + "boundary=" + boundary + "; ";
		contentTypeProperties = contentTypeProperties + "type=\"application/xop+xml\"; ";
		contentTypeProperties = contentTypeProperties + "start=\"<" + boundaryStart + ">\"; ";
		contentTypeProperties = contentTypeProperties + "start-info=\"application/soap+xml\"; ";
		contentTypeProperties = contentTypeProperties + "action=\"urn:ihe:iti:2007:RetrieveDocumentSet\"";
		return new Pair<String, byte[]>(contentTypeProperties, body);
	}
	
	public static void save(OutputStream os, Object xdww) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.xds");
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new XDSNamespacePrefixMapper());
		m.marshal(xdww, os);
	}

	public void executeRequest(){
		Pair<String, byte[]> mtom = null;
		try{
			String soap = createMessage();
			mtom = buildMTOMFromData(soap);
		}
		catch(Exception e){
			log.info(ENABLE_TO_CREATE_THE_MESSAGE, e);
		}
		MessageSenderCommon msc = new MessageSenderCommon();
		
		message = new RDSMessage();
		net.ihe.gazelle.util.Pair<String, InputStream> resp2 = msc.sendMessageAndGetInputStream(this.selectedConfiguration.getUrl(), mtom.getObject2(), message, mtom.getObject1());
		String resp = null;
		String respHeader = resp2 != null? resp2.getObject1():null;

		Attachments attachments = null;
		if (resp2 != null){
			if (respHeader != null && respHeader.contains("multipart")){
				attachments = new Attachments(resp2.getObject2(), resp2.getObject1());
				resp = AttachmentsUtil.transformMultipartToSOAP(attachments);
			}
			else{
				resp = AttachmentsUtil.transformInputToString(resp2.getObject2());
			}
		}

		if (resp != null){
			resp = formatStringIfContainsXML(resp);
		}
		
		message.setTimeStamp(new Date());
		message.setConfiguration(selectedConfiguration);
		message.setTransaction(selectedTransaction);
		message.setMessageType("RetrieveDocumentSet");
		message.setAffinityDomain(this.selectedAffinityDomain);
		message.setContentType(ContentType.MTOM_XOP);
		message.setGazelleDriven(false);
		
		MetadataMessage sent = new MetadataMessage();
		sent.setMessageContent(formatStringIfContainsXML(new String(mtom.getObject2())));
		sent.setHttpHeader(mtom.getObject1());
		sent.setMessageType(MetadataMessageType.RETRIEVE_DOCUMENT_SET_REQUEST);
		
		MetadataMessage received = new MetadataMessage();
		received.setMessageContent(resp);
		received.setHttpHeader(respHeader);
		received.setMessageType(MetadataMessageType.RETRIEVE_DOCUMENT_SET_RESPONSE);
		if (attachments != null){
			try {
				List<AttachmentFile> fileAttachedToReceiver = AttachmentsUtil.generateListAttachmentFiles(attachments, resp);
				if (fileAttachedToReceiver != null && fileAttachedToReceiver.size()>0){
					received.setListAttachments(fileAttachedToReceiver);
					for (AttachmentFile attachmentFile : fileAttachedToReceiver) {
						attachmentFile.setMetadataMessage(received);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		message.setSentMessageContent(sent);
		message.setReceivedMessageContent(received);
		message = RDSMessage.storeMessage(message);
		Contexts.getSessionContext().set("messagesToDisplay", message);
		displayResultPanel = true;
	}
	
	private String formatStringIfContainsXML(String string){
		String res = "";
		String soapRegex = "<\\?xml.*?Envelope>";
		Pattern pp = Pattern.compile(soapRegex,Pattern.MULTILINE|Pattern.DOTALL);
		Matcher mm = pp.matcher(string);
		
		if (mm.find()){
			String ss = mm.group();
			int ind = string.indexOf(ss);
			String toModify = Util.prettyFormat(ss);
			String begin = string.substring(0, ind);
			String end = string.substring(ind + ss.length());
			res = begin + toModify + end;
		}
		else{
			res = string;
		}
		return res;
	}
	

	@Override
	public String getTheNameOfThePage() {
		String res = this.getNameOfTheAffinityDomainTransaction();
		if (res == null || res.equals("")) {
			res = "Retrieve Document Set";
		}
		return res;
	}
	
	public String extractIDFromInitialisation(){
		if (this.selectedTransaction != null &&
				this.selectedTransaction.getKeyword().equals("ITI-43") &&
				this.selectedAffinityDomain != null && 
				this.selectedAffinityDomain.getKeyword().equals("IHE_XDS-b")){
			return "XDSB3";
		}
		else if (this.selectedTransaction != null &&
				this.selectedTransaction.getKeyword().equals("ITI-43") &&
				this.selectedAffinityDomain != null && 
				this.selectedAffinityDomain.getKeyword().equals("IHE_XDW_XDSb")){
			return "XDW3";
		}
		return null;
	}

}
