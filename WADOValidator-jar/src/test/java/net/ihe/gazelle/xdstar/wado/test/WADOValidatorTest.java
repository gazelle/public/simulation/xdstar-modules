package net.ihe.gazelle.xdstar.wado.test;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.xdstar.wado.validator.WADOValidator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class WADOValidatorTest {
	
	private String validatorName ;
	private String wadoRequest ;
	private String idSchema ;
	private String idAssert ;
	private String expectedVerdict ;
	
	
	@Parameters(name = "Test {index} : Assertion {3}, {4} case")
	public static Collection<Object[]> WADOTestDataSet() {
		return Arrays.asList(new Object[][] {
				{ "Dicom - WADO",
					"http://www.wadovalidator/test/wado.php"
					+ "?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2",
					"WADO",
					"WADO-005",
					"PASSED" }, 
				{ "Dicom - WADO",
					"http://www.wadovalidator/test/wado.php"
					+ "?studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2",
					"WADO",
					"WADO-005",
					"FAILED" },
				{ "Dicom - WADO",
					"http://www.wadovalidator/test/wado.php"
					+ "?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2",
					"WADO",
					"WADO-006",
					"PASSED" }, 
				{ "Dicom - WADO",
					"http://www.wadovalidator/test/wado.php"
					+ "?requestType=DICOMObject"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2",
					"WADO",
					"WADO-006",
					"FAILED" },
				{ "Dicom - WADO",
					"http://www.wadovalidator/test/wado.php"
					+ "?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2",
					"WADO",
					"WADO-007",
					"PASSED" }, 
				{ "Dicom - WADO",
					"http://www.wadovalidator/test/wado.php"
					+ "?requestType=WADO"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2",
					"WADO",
					"WADO-007",
					"FAILED" },
				{ "Dicom - WADO",
					"http://www.wadovalidator/test/wado.php"
					+ "?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2",
					"WADO",
					"WADO-008",
					"PASSED" },
				{ "Dicom - WADO",
					"http://www.wadovalidator/test/wado.php"
					+ "?requestType=WADO"
					+ "&studyUID=01.2.250.1.59.40211.1234567.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2",
					"WADO",
					"WADO-008",
					"FAILED" },
				{ "Dicom - WADO",
					"http://www.wadovalidator/test/wado.php"
					+ "?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2",
					"WADO",
					"WADO-009",
					"PASSED" },
				{ "Dicom - WADO",
					"http://www.wadovalidator/test/wado.php"
					+ "?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2",
					"WADO",
					"WADO-009",
					"FAILED" },
				{ "Dicom - WADO",
					"http://www.wadovalidator/test/wado.php"
					+ "?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2",
					"WADO",
					"WADO-010",
					"PASSED" },
				{ "Dicom - WADO",
					"http://www.wadovalidator/test/wado.php"
					+ "?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211-789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2",
					"WADO",
					"WADO-010",
					"FAILED" },
				{ "Dicom - WADO",
					"http://www.wadovalidator/test/wado.php"
					+ "?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2",
					"WADO",
					"WADO-011",
					"PASSED" },
				{ "Dicom - WADO",
					"http://www.wadovalidator/test/wado.php"
					+ "?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789",
					"WADO",
					"WADO-011",
					"FAILED" },
				{ "Dicom - WADO",
					"http://www.wadovalidator/test/wado.php"
					+ "?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2",
					"WADO",
					"WADO-012",
					"PASSED" },
				{ "Dicom - WADO",
					"http://www.wadovalidator/test/wado.php"
					+ "?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2.87991027.899772.250.1.59",
					"WADO",
					"WADO-012",
					"FAILED" },
				{ "Dicom - WADO",
					"http://www.wadovalidator/test/wado.php"
					+ "?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom"
					+ "&anonymize=yes"
					+ "&transferSyntax=1.2.840.10008.1.2.4.50",
					"WADO",
					"WADO-020",
					"PASSED" },
				{ "Dicom - WADO",
					"http://www.wadovalidator/test/wado.php"
					+ "?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjpeg"
					+ "&anonymize=yes"
					+ "&transferSyntax=1.2.840.10008.1.2.4.50",
					"WADO",
					"WADO-020",
					"FAILED" },
				{ "Dicom - WADO",
					"http://www.wadovalidator/test/wado.php"
					+ "?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom"
					+ "&anonymize=yes"
					+ "&transferSyntax=1.2.840.10008.1.2.4.50",
					"WADO",
					"WADO-021",
					"PASSED" },
				{ "Dicom - WADO",
					"http://www.wadovalidator/test/wado.php?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom"
					+ "&anonymize=no"
					+ "&transferSyntax=1.2.840.10008.1.2.4.50",
					"WADO",
					"WADO-021",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-024",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom%3Bq%3D0.5"
					+ "&annotation=patient,technique",
					"WADO",
					"WADO-024",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-025",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,incharge"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-025",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-027",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom;level%3D1"
					+ "&annotation=patient,technique"
					+ "&rows=300",
					"WADO",
					"WADO-027",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-028",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=maximal"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-028",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-029",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=-30%2002"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-029",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-031",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom"
					+ "&annotation=patient,technique"
					+ "&columns=300",
					"WADO",
					"WADO-031",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-032",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=mini"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-032",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-033",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=45,5"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-033",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-035",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2F*,application%2Fdicom"
					+ "&region=0.3,0.4,0.5,0.5",
					"WADO",
					"WADO-035",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-036",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-036",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-037",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,1.2,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-037",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-039",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-039",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&presentationUID=1.2.250.1.59.40211.111213.141516"
					+ "&presentationSeriesUID=1.2.250.1.59.40211.222120.191817",
					"WADO",
					"WADO-040",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&presentationUID=1.2.250.1.59.40211.111213.141516"
					+ "&presentationSeriesUID=1.2.250.1.59.40211.222120.191817",
					"WADO",
					"WADO-040",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom",
					"WADO",
					"WADO-041",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-041",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-10.00e2"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-042",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=+.0e"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-042",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-044",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000",
					"WADO",
					"WADO-044",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&presentationUID=1.2.250.1.59.40211.111213.141516"
					+ "&presentationSeriesUID=1.2.250.1.59.40211.222120.191817",
					"WADO",
					"WADO-045",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowWidth=2500"
					+ "&presentationUID=1.2.250.1.59.40211.111213.141516"
					+ "&presentationSeriesUID=1.2.250.1.59.40211.222120.191817",
					"WADO",
					"WADO-045",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom",
					"WADO",
					"WADO-046",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom"
					+ "&windowWidth=2500",
					"WADO",
					"WADO-046",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=%2B2.5E3",
					"WADO",
					"WADO-047",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=patient,technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&windowCenter=-1000"
					+ "&windowWidth=20.25-5",
					"WADO",
					"WADO-047",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom",
					"WADO",
					"WADO-049",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom"
					+ "&frameNumber=350",
					"WADO",
					"WADO-049",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fgif%3Blevel%3D1,video%2Fmpeg%3Bq%3D0.2"
					+ "&annotation=patient"
					+ "&frameNumber=600",
					"WADO",
					"WADO-050",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fgif%3Blevel%3D1,video%2Fmpeg%3Bq%3D0.2"
					+ "&annotation=patient"
					+ "&frameNumber=default",
					"WADO",
					"WADO-050",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom"
					+ "&anonymize=yes"
					+ "&transferSyntax=1.2.840.10008.1.2.4.50",
					"WADO",
					"WADO-052",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom"
					+ "&imageQuality=80"
					+ "&anonymize=yes"
					+ "&transferSyntax=1.2.840.10008.1.2.4.50",
					"WADO",
					"WADO-052",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom"
					+ "&imageQuality=80",
					"WADO",
					"WADO-052",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom"
					+ "&imageQuality=100"
					+ "&transferSyntax=1.2.840.10008.1.2",
					"WADO",
					"WADO-052",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjpeg"
					+ "&annotation=technique"
					+ "&imageQuality=50",
					"WADO",
					"WADO-053",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjpeg"
					+ "&annotation=technique"
					+ "&imageQuality=130",
					"WADO",
					"WADO-053",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjpeg"
					+ "&annotation=technique"
					+ "&imageQuality=0",
					"WADO",
					"WADO-053",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fpng"
					+ "&annotation=technique"
					+ "&imageQuality=5",
					"WADO",
					"WADO-054",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fpng"
					+ "&annotation=technique"
					+ "&imageQuality=max",
					"WADO",
					"WADO-054",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom",
					"WADO",
					"WADO-056",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom"
					+ "&presentationUID=1.2.250.1.59.40211.111213.141516",
					"WADO",
					"WADO-056",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&presentationUID=1.2.250.1.59.40211.111213.141516"
					+ "&presentationSeriesUID=1.2.250.1.59.40211.222120.191817",
					"WADO",
					"WADO-057",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&presentationUID=1.2.250.1.F0.40211.111A13.1D1C16"
					+ "&presentationSeriesUID=1.2.250.1.59.40211.222120.191817",
					"WADO",
					"WADO-057",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&presentationUID=1.2.250.1.59.40211.111213.141516"
					+ "&presentationSeriesUID=1.2.250.1.59.40211.222120.191817",
					"WADO",
					"WADO-059",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&presentationSeriesUID=1.2.250.1.59.40211.222120.191817",
					"WADO",
					"WADO-059",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&presentationUID=1.2.250.1.59.40211.111213.141516"
					+ "&presentationSeriesUID=1.2.250.1.59.40211.222120.191817",
					"WADO",
					"WADO-060",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5"
					+ "&annotation=technique"
					+ "&columns=400"
					+ "&rows=300"
					+ "&region=0.3,0.4,0.5,0.5"
					+ "&presentationUID=1.2.250.1.59.40211.111213.141516"
					+ "&presentationSeriesUID=01.2.250.1.59.40211.222120.191817",
					"WADO",
					"WADO-060",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom"
					+ "&transferSyntax=1.2.840.10008.1.2.4.50",
					"WADO",
					"WADO-062",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=image%2Fjpeg"
					+ "&transferSyntax=1.2.840.10008.1.2.4.50",
					"WADO",
					"WADO-062",
					"FAILED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom"
					+ "&transferSyntax=1.2.840.10008.1.2.4.50",
					"WADO",
					"WADO-063",
					"PASSED" },
				{ "Dicom - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom"
					+ "&transferSyntax=1,2,840,10008,1,2,4,50",
					"WADO",
					"WADO-063",
					"FAILED" },
				{ "IHE - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
					+ "&contentType=application%2Fdicom",
					"WADO",
					"WADO-064",
					"PASSED" },
				{ "IHE - WADO",
					"https://gazelle/wadoValid?requestType=WADO"
					+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
					+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
					+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2",
					"WADO",
					"WADO-064",
					"PASSED" }
		}) ;
	}
	
	//Constructor, JUnit use it to build different data sets.
	public WADOValidatorTest (
			String nameOfValidator,
			String urlWADO,
			String idOfAssertSchema,
			String idOfAssert,
			String verdict) {
		
		validatorName = nameOfValidator ;
		wadoRequest = urlWADO ;
		idSchema = idOfAssertSchema ;
		idAssert = idOfAssert ;
		expectedVerdict = verdict ;
		
	}
	
	@Test
	public void ValidWADORequestTest() {
		
		boolean idAssertFounded = false ;
		
		WADOValidator wadoValidator = new WADOValidator() ;
		DetailedResult rlt = wadoValidator.validateMessageByValidator(wadoRequest, validatorName) ;
		
		//displayResult (rlt) ;
		
		//Check MDAValidation
		List<Object> notificationList =  rlt.getMDAValidation().getWarningOrErrorOrNote() ;
		for (Object objet : notificationList) {
			Notification notif = (Notification)objet ;
			List<Assertion> assertionList = notif.getAssertions() ;
			for (Assertion ast : assertionList) {
				if (ast.getIdScheme().equals(idSchema) && ast.getAssertionId().equals(idAssert)) {
					if (expectedVerdict.equals("PASSED")) {
						assertTrue("Assertion " + idAssert + " should be PASSED.", notif instanceof Note || notif instanceof Warning) ;
					} else if (expectedVerdict.equals("FAILED")) {
						assertTrue("Assertion " + idAssert + " should be FAILED.", notif instanceof net.ihe.gazelle.validation.Error) ;
					}
					idAssertFounded = true ;
				}
			}
		}
		assertTrue("The test of the assertion" + idAssert + " is not found.", idAssertFounded) ;
		assertTrue("The result of the MDA Validation should be " + expectedVerdict, rlt.getMDAValidation().getResult().equals(expectedVerdict)) ;
		
		//check ValidationResultOverview
		assertTrue("The result of the validation should be "+ expectedVerdict, rlt.getValidationResultsOverview().getValidationTestResult().equals(expectedVerdict)) ;
	}
	
	private void displayResult(DetailedResult rlt) {
		
		System.out.println("DetailedResult");
		
		System.out.println("MDAValidation : ");
		List<Object> notificationList =  rlt.getMDAValidation().getWarningOrErrorOrNote() ;
		for (Object objet : notificationList) {
			Notification notif = (Notification)objet ;
			System.out.println("\t" + notif.getClass().getName() + "### " + notif.getTest()) ;
			System.out.println("\t\t" + notif.getLocation()) ;
			System.out.println("\t\t" + notif.getDescription()) ;
		}
		System.out.println("\tMDAValidation Result : " + rlt.getMDAValidation().getResult());
		
		System.out.println("ValidationResultOverview : ");
		System.out.println("\tValidationDate : " + rlt.getValidationResultsOverview().getValidationDate());
		System.out.println("\tValidationTime : " + rlt.getValidationResultsOverview().getValidationTime());
		System.out.println("\tValidationServiceName : " + rlt.getValidationResultsOverview().getValidationServiceName());
		System.out.println("\tValidationServiceVersion : " + rlt.getValidationResultsOverview().getValidationServiceVersion());
		System.out.println("\tValidationTestResult : " + rlt.getValidationResultsOverview().getValidationTestResult());
	}

}
