package net.ihe.gazelle.xdstar.wado.test;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({WADORequestParserTest.class, WADOValidatorTest.class})
public class AllTests {

}
