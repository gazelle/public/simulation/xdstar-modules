package net.ihe.gazelle.xdstar.wado.test;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;

import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.xdstar.wado.validator.WADOValidator;

import org.junit.Test;

public class WADORequestParserTest {
	
	@Test
	public void RequestParserTestInit() {
		
		DetailedResult result = new DetailedResult() ;
		result.setDocumentWellFormed(new DocumentWellFormed()) ;
		
		WADOValidator wadoValid = new WADOValidator() ;
		HashMap<String,String> map = wadoValid.parseRequest("http://www.test.fr/req?id=001&value=atest", result);
		
		assertTrue(map.containsKey("id")) ;
		assertTrue(map.get("id").equals("001")) ;
		assertTrue(map.containsKey("value"));
		assertTrue(map.get("value").equals("atest"));
	}
	
	@Test
	public void RequestParserTest01() {
		
		DetailedResult result = new DetailedResult() ;
		result.setDocumentWellFormed(new DocumentWellFormed()) ;
		
		WADOValidator wadoValid = new WADOValidator() ;
		HashMap<String,String> map = wadoValid.parseRequest("https://gazelle/wadoValid?requestType=WADO"
				+ "&studyUID=1.2.250.1.59.40211.12345678.678910",
				result) ;
		
		assertTrue(map.containsKey("requestType")) ;
		assertTrue(map.get("requestType").equals("WADO")) ;
		assertTrue(map.containsKey("studyUID"));
		assertTrue(map.get("studyUID").equals("1.2.250.1.59.40211.12345678.678910")) ;
		
	}
	
	@Test
	public void RequestParserTest02() {
		
		DetailedResult result = new DetailedResult() ;
		result.setDocumentWellFormed(new DocumentWellFormed()) ;
		
		WADOValidator wadoValid = new WADOValidator() ;
		HashMap<String,String> map = wadoValid.parseRequest("https://gazelle/wadoValid?requestType=WADO"
				+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
				+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
				+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2",
				result) ;
		
		assertTrue(map.containsKey("requestType")) ;
		assertTrue(map.get("requestType").equals("WADO")) ;
		assertTrue(map.containsKey("studyUID"));
		assertTrue(map.get("studyUID").equals("1.2.250.1.59.40211.12345678.678910")) ;
		assertTrue(map.containsKey("seriesUID"));
		assertTrue(map.get("seriesUID").equals("1.2.250.1.59.40211.789001276.14556172.67789")) ;
		assertTrue(map.containsKey("objectUID"));
		assertTrue(map.get("objectUID").equals("1.2.250.1.59.40211.2678810.87991027.899772.2")) ;
		
	}
	
	@Test
	public void RequestParserTest03() {
		
		DetailedResult result = new DetailedResult() ;
		result.setDocumentWellFormed(new DocumentWellFormed()) ;
		
		WADOValidator wadoValid = new WADOValidator() ;
		HashMap<String,String> map = wadoValid.parseRequest("https://gazelle/wadoValid?requestType=WADO"
				+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
				+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
				+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
				+ "&contentType=application%2Fdicom,image%2Fjpeg",
				result) ;
		
		assertTrue(map.containsKey("requestType")) ;
		assertTrue(map.get("requestType").equals("WADO")) ;
		assertTrue(map.containsKey("studyUID"));
		assertTrue(map.get("studyUID").equals("1.2.250.1.59.40211.12345678.678910")) ;
		assertTrue(map.containsKey("seriesUID"));
		assertTrue(map.get("seriesUID").equals("1.2.250.1.59.40211.789001276.14556172.67789")) ;
		assertTrue(map.containsKey("objectUID"));
		assertTrue(map.get("objectUID").equals("1.2.250.1.59.40211.2678810.87991027.899772.2")) ;
		assertTrue(map.containsKey("contentType"));
		assertTrue(map.get("contentType").equals("application/dicom,image/jpeg")) ;
		
	}
	
	@Test
	public void RequestParserTestFull() {
		
		DetailedResult result = new DetailedResult() ;
		result.setDocumentWellFormed(new DocumentWellFormed()) ;
		
		WADOValidator wadoValid = new WADOValidator() ;
		HashMap<String,String> map = wadoValid.parseRequest("https://gazelle/wadoValid?requestType=WADO"
				+ "&studyUID=1.2.250.1.59.40211.12345678.678910"
				+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
				+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
				+ "&contentType=application%2Fdicom,image%2Fjpeg%3Bq%3D0.5"
				+ "&annotation=patient,technique",
				result) ;
		
		assertTrue(map.containsKey("requestType")) ;
		assertTrue(map.get("requestType").equals("WADO")) ;
		assertTrue(map.containsKey("studyUID"));
		assertTrue(map.get("studyUID").equals("1.2.250.1.59.40211.12345678.678910")) ;
		assertTrue(map.containsKey("seriesUID"));
		assertTrue(map.get("seriesUID").equals("1.2.250.1.59.40211.789001276.14556172.67789")) ;
		assertTrue(map.containsKey("objectUID"));
		assertTrue(map.get("objectUID").equals("1.2.250.1.59.40211.2678810.87991027.899772.2")) ;
		assertTrue(map.containsKey("contentType"));
		assertTrue(map.get("contentType").equals("application/dicom,image/jpeg;q=0.5")) ;
		assertTrue(map.containsKey("annotation"));
		assertTrue(map.get("annotation").equals("patient,technique")) ;
		
	}
	
	@Test
	public void RequestParserTestFailedCase() {
		
		DetailedResult result = new DetailedResult() ;
		result.setDocumentWellFormed(new DocumentWellFormed()) ;
		
		WADOValidator wadoValid = new WADOValidator() ;
		HashMap<String,String> map = wadoValid.parseRequest("https://gazelle/wadoValid?requestType=WADO"
				+ "&  studyUID=1.2.250.1.59.40211.12345678.678910"
				+ "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
				+ "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
				+ "&contentType=application/dicom,image/jpeg;q=0.5"
				+ "&annotation=patient,technique",
				result) ;
		
		assertTrue("Parsing should fail.", result.getDocumentWellFormed().getResult().equals("FAILED")) ;
		
	}

}
