package net.ihe.gazelle.xdstar.wado.validator;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import net.ihe.gazelle.gen.common.CommonOperations;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.validation.MDAValidation;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.ValidationResultsOverview;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.XSDMessage;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public class WADOValidator implements WADOValidatorMethod {

	private static final String SERVICE_NAME = "Gazelle WADO Validator" ;
	private static final String SERVICE_VERSION = "0.0.1" ;
	private static final String OK_STATUS = "PASSED" ;
	private static final String KO_STATUS = "FAILED" ;
	public static final String UTF_8 = "UTF-8";

	private static DateFormat dateFormat = new SimpleDateFormat("yyyy, MM dd") ;
	private static DateFormat timeFormat = new SimpleDateFormat("hh:mm:ss") ;
	
	
	/**
	 * Validate an HTTP WADO request against DICOM, IHE or KSA standard according to the given validatorName.
	 * 
	 * @param message The URL to validate
	 * @param validatorName The validator to use.
	 * @return return a DetailedResult XML document.
	 */
	@Override
	public DetailedResult validateMessageByValidator(String message, String validatorName) {
		
		HashMap<String,String> mapRequest ;
		DetailedResult result = null ;
		
		WADOValidatorType validatorType = WADOValidatorType.getValidatorFromName(validatorName);
		
		if (validatorType != null) {
			
			result = new DetailedResult() ;
			result.setDocumentWellFormed(new DocumentWellFormed()) ;
			result.setMDAValidation(new MDAValidation()) ;
			result.setValidationResultsOverview(new ValidationResultsOverview()) ;
			
			mapRequest = parseRequest(message, result);
			
			switch (validatorType) {
			case DICOM_WADO:

				validateDicomWado(mapRequest, result);
				consolidateWADOvalidation(result, WADOValidatorType.DICOM_WADO.getName()) ;
				break;

			case KSA_WADO:
			case IHE_WADO:

				validateDicomWado(mapRequest, result);
				validateIHEWado(mapRequest, result) ;
				consolidateWADOvalidation(result, WADOValidatorType.IHE_WADO.getName()) ;
				break;

			default:
				break;
			}
		}
			
		return result ;
	}
	

	/**
	 * <p>Parse HTTP WADO request. If the request is correctly parsed, the result of DocumentWellFormed of the detailed result will
	 * be set to PASSED. Otherwise it will be set to FAILED.</p>
	 * 
	 * @param message	The URL to parse.
	 * @param result	The DetailedResult of the WADO validation.
	 * @return the HashMap containing Parameters and values of the query of the URL.
	 */
	public HashMap<String,String> parseRequest(String message, DetailedResult result) {
		
		try{

			URI wadoUri = new URI(message);
			List<NameValuePair> parsedUri = URLEncodedUtils.parse(wadoUri.normalize(), UTF_8);

			HashMap<String,String> map = new HashMap<String,String>() ;
			for (NameValuePair param : parsedUri) {
				map.put(param.getName(), param.getValue()) ;
			}

			result.getDocumentWellFormed().setResult(OK_STATUS);
			return map;

		} catch(Exception e) {

			XSDMessage notif = new XSDMessage() ;
			notif.setMessage(e.getMessage());
			notif.setSeverity("Error") ;
			result.getDocumentWellFormed().getXSDMessage().add(notif) ;
			result.getDocumentWellFormed().setResult(KO_STATUS);
			return null ;

		}
	}
	
	/**
	 * <p>Consolidate result. It goes through the XML result document to set results of 
	 * DocumentWellFormed and MDAValidation, Then it build result overview according to 
	 * the given validatorName.</p>
	 */
	private void consolidateWADOvalidation(DetailedResult result, String validatorName) {
		
		boolean mdaIsValid = false ;
		boolean formatIsValid = false ;
		Date currentDate ;
		currentDate = new Date() ;
		
		//consolidate Format Validation
		if(result.getDocumentWellFormed().getResult().equals(OK_STATUS)) {
			formatIsValid = true ;
		} else {
			formatIsValid = false ;
		}
		
		//consolidate MDA validation
		if (nbOfErrorInNotificationList(result.getMDAValidation().getWarningOrErrorOrNote()) > 0
			|| result.getMDAValidation().getWarningOrErrorOrNote().size() == 0) {
			
			mdaIsValid = false ;
			result.getMDAValidation().setResult(KO_STATUS) ;
			
		} else {
			
			mdaIsValid = true ;
			result.getMDAValidation().setResult(OK_STATUS) ;
			
		}
		
		//build ValidationResultsOverview
		result.getValidationResultsOverview().setValidationServiceName(SERVICE_NAME + " : " + validatorName);
		result.getValidationResultsOverview().setValidationServiceVersion(SERVICE_VERSION);
		result.getValidationResultsOverview().setValidationDate(dateFormat.format(currentDate));
		result.getValidationResultsOverview().setValidationTime(timeFormat.format(currentDate));
		if (mdaIsValid && formatIsValid) {
			result.getValidationResultsOverview().setValidationTestResult(OK_STATUS);
		} else {
			result.getValidationResultsOverview().setValidationTestResult(KO_STATUS);
		}
	}
	
	//Count Error notification in notification list
	private int nbOfErrorInNotificationList(List<java.lang.Object> notificationList) {
		
		int index ;
		int nbOfError = 0 ;
		
		for (index = 0 ; index < notificationList.size() ; index++ ) {
			if (notificationList.get(index) instanceof net.ihe.gazelle.validation.Error) {
				nbOfError++ ;
			}
		}
		return nbOfError ;
	}
	
	private void validateDicomWado(HashMap<String,String> mapRequest, DetailedResult result) {
		
		if (mapRequest != null) {
			//declaration
			List<Notification> notificationList ;
			notificationList = new ArrayList<Notification>();
			
			//validation
			validateWado002(mapRequest, notificationList);
			validateWado005(mapRequest, notificationList);
			validateWado006(mapRequest, notificationList);
			validateWado007(mapRequest, notificationList);
			validateWado008(mapRequest, notificationList);
			validateWado009(mapRequest, notificationList);
			validateWado010(mapRequest, notificationList);
			validateWado011(mapRequest, notificationList);
			validateWado012(mapRequest, notificationList);
			validateWado014(mapRequest, notificationList);
			validateWado017(mapRequest, notificationList);
			validateWado020(mapRequest, notificationList);
			validateWado021(mapRequest, notificationList);
			validateWado024(mapRequest, notificationList);
			validateWado025(mapRequest, notificationList);
			validateWado027(mapRequest, notificationList);
			validateWado028_029(mapRequest, notificationList);
			validateWado031(mapRequest, notificationList);
			validateWado032_033(mapRequest, notificationList);
			validateWado035(mapRequest, notificationList);
			validateWado036(mapRequest, notificationList);
			validateWado037(mapRequest, notificationList);
			validateWado039(mapRequest, notificationList);
			validateWado040(mapRequest, notificationList);
			validateWado041(mapRequest, notificationList);
			validateWado042(mapRequest, notificationList);
			validateWado044(mapRequest, notificationList);
			validateWado045(mapRequest, notificationList);
			validateWado046(mapRequest, notificationList);
			validateWado047(mapRequest, notificationList);
			validateWado049(mapRequest, notificationList);
			validateWado050(mapRequest, notificationList);
			validateWado052(mapRequest, notificationList);
			validateWado053(mapRequest, notificationList);
			validateWado054(mapRequest, notificationList);
			validateWado056(mapRequest, notificationList);
			validateWado057(mapRequest, notificationList);
			validateWado059(mapRequest, notificationList);
			validateWado060(mapRequest, notificationList);
			validateWado062(mapRequest, notificationList);
			validateWado063(mapRequest, notificationList);
			
			//add notificationList into MDAValidation
			result.getMDAValidation().getWarningOrErrorOrNote().addAll(notificationList) ;
		}
	}
	
	private void validateIHEWado(HashMap<String,String> mapRequest, DetailedResult result) {
		
		if (mapRequest != null) {
			
			//declaration
			List<Notification> notificationList ;
			notificationList = new ArrayList<Notification>();
			
			//validation
			validateWado064(mapRequest, notificationList) ;
			
			//add notificationList into MDAValidation
			result.getMDAValidation().getWarningOrErrorOrNote().addAll(notificationList) ;
			
		}
	}
	
	private void validateWado002(HashMap<String, String> mapRequest, List<Notification> notificationList) {
		// TODO Check WADO-002
	}
	
	private void validateWado005(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null;
		
		if (mapRequest.containsKey("requestType")) {
			notif = new Note() ;	
		} else {
			notif = new net.ihe.gazelle.validation.Error() ;
		}
		
		notif.setTest("Wado005");
		notif.setLocation("requestType");
		notif.setDescription("Parameter requestType is REQUIRED.(PS 3.18, 8.1.1)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-005");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
	}
	
	private void validateWado006(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null;
		
		if (mapRequest.containsKey("requestType")) {
			if (mapRequest.get("requestType").equals("WADO")) {
				notif = new Note() ;	
			} else {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new net.ihe.gazelle.validation.Error() ;
		}
		
		notif.setTest("Wado006");
		notif.setLocation("requestType");
		notif.setDescription("The value of requestType shall be \"WADO\".(PS 3.18, 8.1.1)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-006");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
	}
	
	private void validateWado007(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null;
		
		if (mapRequest.containsKey("studyUID")) {
			notif = new Note() ;	
		} else {
			notif = new net.ihe.gazelle.validation.Error() ;
		}
		
		notif.setTest("Wado007");
		notif.setLocation("studyUID");
		notif.setDescription("Parameter studyUID is REQUIRED.(PS 3.18, 8.1.2)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-007");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado008(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		CommonOperations operator = new CommonOperations() ;
		
		if (mapRequest.containsKey("studyUID")) {
			if (operator.isOID(mapRequest.get("studyUID"))) {
				notif = new Note() ;	
			} else {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new net.ihe.gazelle.validation.Error() ;
		}
		
		notif.setTest("Wado008");
		notif.setLocation("studyUID");
		notif.setDescription("The value shall be encoded as a Unique Identifier (UID) string, "
				+ "as specified in PS 3.5.(PS 3.18, 8.1.2)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-008");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado009(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null;
		
		if (mapRequest.containsKey("seriesUID")) {
			notif = new Note() ;	
		} else {
			notif = new net.ihe.gazelle.validation.Error() ;
		}
		
		notif.setTest("Wado009");
		notif.setLocation("seriesUID");
		notif.setDescription("Parameter seriesUID is REQUIRED.(PS 3.18, 8.1.3)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-009");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado010(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		CommonOperations operator = new CommonOperations() ;
		
		if (mapRequest.containsKey("seriesUID")) {
			if (operator.isOID(mapRequest.get("seriesUID"))) {
				notif = new Note() ;	
			} else {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new net.ihe.gazelle.validation.Error() ;
		}
		
		notif.setTest("Wado010");
		notif.setLocation("seriesUID");
		notif.setDescription("The value shall be encoded as a Unique Identifier (UID) string, as "
				+ "specified in PS 3.5.(PS 3.18, 8.1.3)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-010");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
private void validateWado011(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null;
		
		if (mapRequest.containsKey("objectUID")) {
			notif = new Note() ;	
		} else {
			notif = new net.ihe.gazelle.validation.Error() ;
		}
		
		notif.setTest("Wado011");
		notif.setLocation("objectUID");
		notif.setDescription("Parameter objectUID is REQUIRED.(PS 3.18, 8.1.4)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-011");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado012(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		CommonOperations operator = new CommonOperations() ;
		
		if (mapRequest.containsKey("objectUID")) {
			if (operator.isOID(mapRequest.get("objectUID"))) {
				notif = new Note() ;	
			} else {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new net.ihe.gazelle.validation.Error() ;
		}
		
		notif.setTest("Wado012");
		notif.setLocation("objectUID");
		notif.setDescription("The value shall be encoded as a Unique Identifier (UID) string, as "
				+ "specified in PS 3.5.(PS 3.18, 8.1.4)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-012");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado014(HashMap<String,String> mapRequest, List<Notification> notificationList){
		//TODO validate WADO-014
	}
	
	private void validateWado017(HashMap<String,String> mapRequest, List<Notification> notificationList){
		//TODO validate WADO-017
	}
	
	private void validateWado020(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("anonymize")) {
			if (mapRequest.containsKey("contentType")) {
				if (mapRequest.get("contentType").contains("application/dicom")) {
					notif = new Note() ;
				} else {
					notif = new net.ihe.gazelle.validation.Error() ;
				}
			} else {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado020");
		notif.setLocation("anonymize");
		notif.setDescription("The OPTIONAL parameter anonymize shall only be present if content type "
				+ "is \"application/dicom\". (PS 3.18, 8.1.7)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-020");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}

	private void validateWado021(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("anonymize")) {
			if (mapRequest.get("anonymize").equals("yes")) {
				notif = new Note() ;
			} else {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado021");
		notif.setLocation("anonymize");
		notif.setDescription("The value of the OPTIONAL parameter anonymize shall be \"yes\". (PS 3.18, 8.1.7)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-021");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado024(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("annotation")) {
			if (mapRequest.containsKey("contentType")) {
				if (mapRequest.get("contentType").contains("application/dicom") ||
						mapRequest.get("contentType").contains("text/")) {
					notif = new net.ihe.gazelle.validation.Error() ;
				} else {
					notif = new Note() ;
				}
			} else {
				notif = new Note() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado024");
		notif.setLocation("annotation");
		notif.setDescription("the OPTIONAL parameter annotation shall not be present if contentType is "
				+ "\"application/dicom\", or is a non-image MIME type (e.g., \"text/*\"). (PS 3.18, 8.2.1)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-024");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado025(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("annotation")) {
			if (mapRequest.get("annotation").equals("patient") ||
				mapRequest.get("annotation").equals("technique") ||
				mapRequest.get("annotation").equals("patient,technique") ||
				mapRequest.get("annotation").equals("technique,patient")) {
				notif = new Note() ;
			} else {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado025");
		notif.setLocation("annotatoin");
		notif.setDescription("The value of the OPTIONAL parameter annotation shall be a non-empty list "
				+ "of one or more of the following items, separated by a \",\" character :"
				+ "\n - \"patient\"\n - \"technique\"\n(PS 3.18, 8.2.1)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-025");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado027(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("rows")) {
			if (mapRequest.containsKey("contentType")) {
				if (mapRequest.get("contentType").contains("application/dicom")) {
					notif = new net.ihe.gazelle.validation.Error() ;
				} else {
					notif = new Note() ;
				}
			} else {
				notif = new Note() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado027");
		notif.setLocation("rows");
		notif.setDescription("the OPTIONAL parameter rows shall not be present if contentType "
				+ "is \"application/dicom\". (PS 3.18, 8.2.2)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-027");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado028_029(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("rows")) {
			try {
				Integer.parseInt(mapRequest.get("rows"));
				notif = new Note() ;
			} catch (NumberFormatException e) {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado028, Wado029");
		notif.setLocation("rows");
		notif.setDescription("The value of the OPTIONAL parameter \"rows\" shall be expressed as an integer, "
				+ "representing the image height to be returned. The value shall be encoded as an "
				+ "integer string (IS), as specified in PS 3.5. (PS 3.18, 8.2.2)");
		
		Assertion ast1 = new Assertion();
		ast1.setAssertionId("WADO-028");
		ast1.setIdScheme("WADO");
		Assertion ast2 = new Assertion();
		ast2.setAssertionId("WADO-029");
		ast2.setIdScheme("WADO");
		
		notif.getAssertions().add(ast1);
		notif.getAssertions().add(ast2);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado031(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("columns")) {
			if (mapRequest.containsKey("contentType")) {
				if (mapRequest.get("contentType").contains("application/dicom")) {
					notif = new net.ihe.gazelle.validation.Error() ;
				} else {
					notif = new Note() ;
				}
			} else {
				notif = new Note() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado031");
		notif.setLocation("columns");
		notif.setDescription("the OPTIONAL parameter \"columns\" shall not be present if contentType "
				+ "is \"application/dicom\". (PS 3.18, 8.2.3)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-031");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado032_033(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("columns")) {
			try {
				Integer.parseInt(mapRequest.get("columns"));
				notif = new Note() ;
			} catch (NumberFormatException e) {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado032, Wado033");
		notif.setLocation("columns");
		notif.setDescription("The value of the OPTIONAL parameter \"columns\" shall be expressed as an integer, "
				+ "representing the image width to be returned. The value shall be encoded as an "
				+ "integer string (IS), as specified in PS 3.5. (PS 3.18, 8.2.3)");
		
		Assertion ast1 = new Assertion();
		ast1.setAssertionId("WADO-032");
		ast1.setIdScheme("WADO");
		Assertion ast2 = new Assertion();
		ast2.setAssertionId("WADO-033");
		ast2.setIdScheme("WADO");
		
		notif.getAssertions().add(ast1);
		notif.getAssertions().add(ast2);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado035(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("region")) {
			if (mapRequest.containsKey("contentType")) {
				if (mapRequest.get("contentType").contains("application/dicom")) {
					notif = new net.ihe.gazelle.validation.Error() ;
				} else {
					notif = new Note() ;
				}
			} else {
				notif = new Note() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado035");
		notif.setLocation("region");
		notif.setDescription("the OPTIONAL parameter \"region\" shall not be present if contentType "
				+ "is \"application/dicom\". (PS 3.18, 8.2.4)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-035");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado036(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("region")) {
			if(Pattern.matches("(\\d+(\\.\\d+)?)(,\\d+(\\.\\d+)?){3}", mapRequest.get("region"))) {
				notif = new Note() ;
			} else {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado036");
		notif.setLocation("region");
		notif.setDescription("The value of the OPTIONAL parameter \"region\" shall be expressed as a "
				+ "list of four positive decimal strings, separated by the ',' character, representing "
				+ "the region of the source image to be returned. (PS 3.18, 8.2.4)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-036");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado037(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("region")) {
			if(Pattern.matches("(\\W?(0(\\.\\d+)?)|(1(\\.0)?))+", mapRequest.get("region"))) {
				notif = new Note() ;
			} else {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado037");
		notif.setLocation("region");
		notif.setDescription("These decimal values shall be values in a normalized coordinate system "
				+ "relative to the size of the original image matrix measured in rows and columns, "
				+ "with values ranging from 0.0 to 1.0 (PS 3.18, 8.2.4)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-037");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado039(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("windowWidth")) {
			if(mapRequest.containsKey("windowCenter")) {
				notif = new Note() ;
			} else {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado039");
		notif.setLocation("windowCenter");
		notif.setDescription("The parameter \"windowCenter\" is REQUIRED if \"windowWidth\" "
				+ "is present. (PS 3.18, 8.2.5)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-039");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado040(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("presentationUID")) {
			if(mapRequest.containsKey("windowCenter")) {
				notif = new net.ihe.gazelle.validation.Error() ;
			} else {
				notif = new Note() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado040");
		notif.setLocation("windowCenter");
		notif.setDescription("The parameter \"windowCenter\" shall not be present if there is a "
				+ "\"presentationUID\" parameter. (PS 3.18, 8.2.5)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-040");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado041(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("windowCenter")) {
			if (mapRequest.containsKey("contentType")) {
				if (mapRequest.get("contentType").contains("application/dicom")) {
					notif = new net.ihe.gazelle.validation.Error() ;
				} else {
					notif = new Note() ;
				}
			} else {
				notif = new Note() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado041");
		notif.setLocation("windowCenter");
		notif.setDescription("The parameter \"windowCenter\" shall not be present if \"contentType\" "
				+ "is application/dicom. (PS 3.18, 8.2.5)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-041");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado042(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("windowCenter")) {
			if(Pattern.matches("[\\+-]?\\d+(\\.\\d+)?([Ee]\\d+)?", mapRequest.get("windowCenter"))) {
				notif = new Note() ;
			} else {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado042");
		notif.setLocation("windowCenter");
		notif.setDescription("The value of the parameter \"windowCenter\" shall be encoded "
				+ "as a decimal string (DS), as specified in PS 3.5. (PS 3.18, 8.2.5)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-042");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado044(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("windowCenter")) {
			if(mapRequest.containsKey("windowWidth")) {
				notif = new Note() ;
			} else {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado044");
		notif.setLocation("windowWidth");
		notif.setDescription("The parameter \"windowWidth\" is REQUIRED if \"windowCenter\" "
				+ "is present. (PS 3.18, 8.2.6)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-044");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado045(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("presentationUID")) {
			if(mapRequest.containsKey("windowWidth")) {
				notif = new net.ihe.gazelle.validation.Error() ;
			} else {
				notif = new Note() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado045");
		notif.setLocation("windowWidth");
		notif.setDescription("The parameter \"windowWidth\" shall not be present if there is a "
				+ "\"presentationUID\" parameter. (PS 3.18, 8.2.6)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-045");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado046(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("windowWidth")) {
			if (mapRequest.containsKey("contentType")) {
				if (mapRequest.get("contentType").contains("application/dicom")) {
					notif = new net.ihe.gazelle.validation.Error() ;
				} else {
					notif = new Note() ;
				}
			} else {
				notif = new Note() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado046");
		notif.setLocation("windowWidth");
		notif.setDescription("The parameter \"windowWidth\" shall not be present if \"contentType\" "
				+ "is application/dicom. (PS 3.18, 8.2.6)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-046");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado047(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("windowWidth")) {
			if(Pattern.matches("[\\+-]?\\d+(\\.\\d+)?([Ee]\\d+)?", mapRequest.get("windowWidth"))) {
				notif = new Note() ;
			} else {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado047");
		notif.setLocation("windowWidth");
		notif.setDescription("The value of the parameter \"windowWidth\" shall be encoded "
				+ "as a decimal string (DS), as specified in PS 3.5. (PS 3.18, 8.2.6)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-047");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado049(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("frameNumber")) {
			if (mapRequest.containsKey("contentType")) {
				if (mapRequest.get("contentType").contains("application/dicom")) {
					notif = new net.ihe.gazelle.validation.Error() ;
				} else {
					notif = new Note() ;
				}
			} else {
				notif = new Note() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado049");
		notif.setLocation("frameNumber");
		notif.setDescription("The parameter \"frameNumber\" shall not be present if \"contentType\" "
				+ "is application/dicom. (PS 3.18, 8.2.7)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-049");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado050(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("frameNumber")) {
			try {
				Integer.parseInt(mapRequest.get("frameNumber"));
				notif = new Note() ;
			} catch (NumberFormatException e) {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado050");
		notif.setLocation("frameNumber");
		notif.setDescription("The value of the OPTIONAL parameter \"frameNumber\" shall be encoded as an "
				+ "integer string (IS), as specified in PS 3.5. (PS 3.18, 8.2.7)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-050");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado052(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("imageQuality")) {
			if (mapRequest.containsKey("contentType")) {
				if (mapRequest.get("contentType").contains("application/dicom")) {
					if (mapRequest.containsKey("transferSyntax")) {
						if (mapRequest.get("transferSyntax").equals("1.2.840.10008.1.2.4.50") ||
							mapRequest.get("transferSyntax").equals("1.2.840.10008.1.2.4.51")) {
							notif = new Note() ;
						} else {
							notif = new net.ihe.gazelle.validation.Error() ;
						}
					} else {
						notif = new net.ihe.gazelle.validation.Error() ;
					}
				} else {
					notif = new Note() ;
				}
			} else {
				notif = new Note() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado052");
		notif.setLocation("imageQuality");
		notif.setDescription("The parameter \"imageQuality\" shall not be present if \"contentType\" "
				+ "is application/dicom, except if the transferSyntax parameter is present and "
				+ "corresponds to a lossy compression. (PS 3.18, 8.2.8)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-052");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado053(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if(mapRequest.containsKey("contentType")) {
			if(mapRequest.get("contentType").contains("image/jpeg")) {
				if (mapRequest.containsKey("imageQuality")) {
					if(Pattern.matches("[1-9]|[1-9][0-9]|100", mapRequest.get("imageQuality"))) {
						notif = new Note() ;
					} else {
						notif = new net.ihe.gazelle.validation.Error() ;
					}
				} else {
					notif = new Note() ;
				}
			} else {
				notif = new Note() ;
			}
		} else {
			notif = new Note() ;
		}
		
		
		notif.setTest("Wado053");
		notif.setLocation("imageQuality");
		notif.setDescription("If the requested MIME type is for a lossy compressed image (e.g. image/jpeg), "
				+ "this parameter indicates the required quality of the image to be returned within the "
				+ "range 1 to 100, 100 being the best quality. (PS 3.18, 8.2.8)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-053");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado054(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("imageQuality")) {
			try {
				Integer.parseInt(mapRequest.get("imageQuality"));
				notif = new Note() ;
			} catch (NumberFormatException e) {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado054");
		notif.setLocation("imageQuality");
		notif.setDescription("The value of the OPTIONAL parameter \"imageQuality\" shall be encoded as an "
				+ "integer string (IS), as specified in PS 3.5. (PS 3.18, 8.2.8)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-054");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado056(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("presentationUID")) {
			if (mapRequest.containsKey("contentType")) {
				if (mapRequest.get("contentType").contains("application/dicom")) {
					notif = new net.ihe.gazelle.validation.Error() ;
				} else {
					notif = new Note() ;
				}
			} else {
				notif = new Note() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado056");
		notif.setLocation("presentationUID");
		notif.setDescription("The parameter \"presentationUID\" shall not be present if \"contentType\" "
				+ "is application/dicom. (PS 3.18, 8.2.9)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-056");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado057(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		CommonOperations operator = new CommonOperations() ;
		
		if (mapRequest.containsKey("presentationUID")) {
			if (operator.isOID(mapRequest.get("presentationUID"))) {
				notif = new Note() ;	
			} else {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado057");
		notif.setLocation("presentationUID");
		notif.setDescription("The value shall be encoded as a Unique Identifier (UID) string, as "
				+ "specified in PS 3.5.(PS 3.18, 8.2.9)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-057");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado059(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("presentationSeriesUID") == mapRequest.containsKey("presentationUID")) {
			notif = new Note() ;
		} else {
			notif = new net.ihe.gazelle.validation.Error() ;
		}

		
		notif.setTest("Wado059");
		notif.setLocation("windowCenter");
		notif.setDescription("The parameter \"presentationSeriesUID\" is REQUIRED if \"presentationUID\" "
				+ "is present. (PS 3.18, 8.2.10)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-059");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado060(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		CommonOperations operator = new CommonOperations() ;
		
		if (mapRequest.containsKey("presentationSeriesUID")) {
			if (operator.isOID(mapRequest.get("presentationSeriesUID"))) {
				notif = new Note() ;	
			} else {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado060");
		notif.setLocation("presentationSeriesUID");
		notif.setDescription("The value shall be encoded as a Unique Identifier (UID) string, as "
				+ "specified in PS 3.5.(PS 3.18, 8.2.10)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-060");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado062(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		
		if (mapRequest.containsKey("transferSyntax")) {
			if (mapRequest.containsKey("contentType")) {
				if (!mapRequest.get("contentType").contains("application/dicom")) {
					notif = new net.ihe.gazelle.validation.Error() ;
				} else {
					notif = new Note() ;
				}
			} else {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado062");
		notif.setLocation("transferSyntax");
		notif.setDescription("The parameter \"transferSyntax\" shall not be present if \"contentType\" "
				+ "is other than application/dicom. (PS 3.18, 8.2.11)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-062");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado063(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null ;
		CommonOperations operator = new CommonOperations() ;
		
		if (mapRequest.containsKey("transferSyntax")) {
			if (operator.isOID(mapRequest.get("transferSyntax"))) {
				notif = new Note() ;	
			} else {
				notif = new net.ihe.gazelle.validation.Error() ;
			}
		} else {
			notif = new Note() ;
		}
		
		notif.setTest("Wado063");
		notif.setLocation("transferSyntax");
		notif.setDescription("The value shall be encoded as a Unique Identifier (UID) string, as "
				+ "specified in PS 3.5.(PS 3.18, 8.2.11)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-063");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
		
	}
	
	private void validateWado064(HashMap<String,String> mapRequest, List<Notification> notificationList){
		
		Notification notif = null;
		
		if (mapRequest.containsKey("contentType")) {
			notif = new Note() ;	
		} else {
			notif = new Warning() ;
		}
		
		notif.setTest("Wado064");
		notif.setLocation("contentType");
		notif.setDescription("The parameter contentType is recommended.(IHE_RAD_TF_Vol3, 4.55.4.1.2)");
		
		Assertion ast = new Assertion();
		ast.setAssertionId("WADO-064");
		ast.setIdScheme("WADO");
		notif.getAssertions().add(ast);
		
		notificationList.add(notif) ;
	}
}
