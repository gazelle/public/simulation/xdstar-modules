package net.ihe.gazelle.xdstar.wado.validator;


public enum WADOValidatorType {
	
	DICOM_WADO("Dicom - WADO"), IHE_WADO("IHE - WADO"), KSA_WADO("KSA - WADO");
	
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private WADOValidatorType(String name) {
		this.name = name;
	}
	
	public static WADOValidatorType getValidatorFromName(String value){
		for (WADOValidatorType val : WADOValidatorType.values()) {
			if (val.name.equals(value)){
				return val;
			}
		}
		return null;
	}

}
