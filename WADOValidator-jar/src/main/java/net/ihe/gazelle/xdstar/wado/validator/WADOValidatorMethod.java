package net.ihe.gazelle.xdstar.wado.validator;

import net.ihe.gazelle.validation.DetailedResult;

public interface WADOValidatorMethod {
	
	public DetailedResult validateMessageByValidator(String message, String validatorName);

}
