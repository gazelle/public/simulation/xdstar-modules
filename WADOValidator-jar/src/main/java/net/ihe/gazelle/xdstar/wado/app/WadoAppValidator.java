package net.ihe.gazelle.xdstar.wado.app;

import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.xdstar.wado.validator.WADOValidator;

import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.ExampleMode;
import org.kohsuke.args4j.Option;

/**
 * 
 * @author abderrazek boufahja
 *
 */
public class WadoAppValidator {
	
	final static String documentationString = "\nWadoAppValidator -val validatorName -url wadoURL  [-out outputFile]";
	
	@Option(name="-val", metaVar="validatorName", usage= "validator name, one of : {IHE - WADO}", required=true)
	private String validatorName;
	
	@Option(name="-help", usage= "how to use the tool", required=false)
	private Boolean viewHelp = false;

	@Option(name="-url", metaVar="wadoURL", usage= "wadoURL to be validated", required=true)
	private String wadoURL;

	@Option(name="-out", metaVar="outputFile", usage= "path to the output file", required=false)
	private String outputFilePath;
	
	public String getWadoURL() {
		return wadoURL;
	}

	public void setWadoURL(String wadoURL) {
		this.wadoURL = wadoURL;
	}

	public String getOutputFilePath() {
		return outputFilePath;
	}

	public void setOutputFilePath(String outputFilePath) {
		this.outputFilePath = outputFilePath;
	}

	public Boolean getViewHelp() {
		return viewHelp;
	}

	public void setViewHelp(Boolean viewHelp) {
		this.viewHelp = viewHelp;
	}

	public String getValidatorName() {
		return validatorName;
	}

	public void setValidatorName(String validatorName) {
		this.validatorName = validatorName;
	}

	public static void main(String[] args) {
		WadoAppValidator gaz = new WadoAppValidator();
		gaz.execute(args);
	}

	public void execute(String[] args){
		CmdLineParser parser = new CmdLineParser(this);
		try {
			parser.parseArgument(args);
			
			if (this.viewHelp){
				System.out.println(WadoAppValidator.documentationString);
				parser.printUsage(System.out);
				parser.printExample(ExampleMode.ALL);
				return;
			}
			
			DetailedResult valRes = (new WADOValidator()).validateMessageByValidator(wadoURL, validatorName);
			if (outputFilePath != null){
				save(new FileOutputStream(outputFilePath), valRes);
				System.out.println("The output file was updated (" + outputFilePath + ")");
			}
			else {
				save(System.out, valRes);
			}
		} 
		catch (Exception e) {
			if (this.viewHelp){
				System.out.println(WadoAppValidator.documentationString);
				parser.printUsage(System.out);
				System.out.println("sample : -out outputFile -url wadoURL -val 'IHE - WADO'");
				return;
			}
			
			System.err.println(e.getMessage());
			System.out.println(WadoAppValidator.documentationString);
			parser.printUsage(System.err);
		}
	}
	
	public static void save(OutputStream os, DetailedResult txdw) throws JAXBException{
		JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.validation");
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_ENCODING, "UTF8");
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.marshal(txdw, os);
	}

}
