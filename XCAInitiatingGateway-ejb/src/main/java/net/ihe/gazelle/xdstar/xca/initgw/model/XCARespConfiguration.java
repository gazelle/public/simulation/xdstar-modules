package net.ihe.gazelle.xdstar.xca.initgw.model;



import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationFiltering;

import org.jboss.seam.annotations.Name;

@Entity
@Name("xCARespConfiguration")
@DiscriminatorValue("XCARESP")
public class XCARespConfiguration extends SystemConfiguration implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String repositoryUniqueId;
	
	private String homeCommunityId;
	
	public XCARespConfiguration(){}
	
	public XCARespConfiguration(XCARespConfiguration configuration) {
		super(configuration);
		this.repositoryUniqueId = configuration.getRepositoryUniqueId();
		this.homeCommunityId = configuration.getHomeCommunityId();
	}

	public String getRepositoryUniqueId() {
		return repositoryUniqueId;
	}

	public void setRepositoryUniqueId(String repositoryUniqueId) {
		this.repositoryUniqueId = repositoryUniqueId;
	}

	public String getHomeCommunityId() {
		return homeCommunityId;
	}

	public void setHomeCommunityId(String homeCommunityId) {
		this.homeCommunityId = homeCommunityId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((homeCommunityId == null) ? 0 : homeCommunityId.hashCode());
		result = prime
				* result
				+ ((repositoryUniqueId == null) ? 0 : repositoryUniqueId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		XCARespConfiguration other = (XCARespConfiguration) obj;
		if (homeCommunityId == null) {
			if (other.homeCommunityId != null)
				return false;
		} else if (!homeCommunityId.equals(other.homeCommunityId))
			return false;
		if (repositoryUniqueId == null) {
			if (other.repositoryUniqueId != null)
				return false;
		} else if (!repositoryUniqueId.equals(other.repositoryUniqueId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "XCARespConfiguration [repositoryUniqueId=" + repositoryUniqueId
				+ ", homeCommunityId=" + homeCommunityId + ", id=" + id
				+ ", name=" + name + ", systemName=" + systemName + ", url="
				+ url + "]";
	}
	
	
}
