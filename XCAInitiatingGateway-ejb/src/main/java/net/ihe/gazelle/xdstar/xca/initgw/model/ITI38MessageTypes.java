/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.xca.initgw.model;


/**
 *  <b>Class Description :  </b>ITI38MessageTypes<br><br>
 * This class enumerates the ITI-38 message types.
 *
 * @class                   ITI38MessageTypes.java
 * @package        			net.ihe.gazelle.simulator.xca.initgw.model
 * @author 					Abderrazek Boufahja / IHE Europe
 * @author                	Anne-Gaëlle Bergé / INRIA Rennes IHE development Project
 *
 */

public class ITI38MessageTypes {
	
	public static final String FIND_DOCUMENTS = "FindDocuments (returns ObjectRef)";
	public static final String GET_DOCUMENTS = "GetDocuments";
	public static final String GET_RELATED = "GetRelatedDocuments";
	public static final String GET_SUBMISSION_SETS = "GetSubmissionSets";
	public static final String GET_ASSOCIATIONS = "GetAssociations";
	public static final String FIND_DOCUMENTS_LEAF_CLASS = "Find Documents (returns LeafClass)";

}
