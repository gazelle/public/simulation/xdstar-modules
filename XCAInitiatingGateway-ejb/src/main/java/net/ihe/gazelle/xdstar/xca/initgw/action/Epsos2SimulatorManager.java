/* Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.xca.initgw.action;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.xdstar.rds.common.CommonRetrieveManager;
import net.ihe.gazelle.xdstar.xca.initgw.model.XCAMessage;
import net.ihe.gazelle.xdstar.xca.initgw.model.XCARespConfiguration;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

/**
 *  
 * @author                	Abderrazek Boufahja
 *
 */

@Name("epSOS2SimulatorManager")
@Scope(ScopeType.PAGE)
public class Epsos2SimulatorManager extends Epsos1SimulatorManager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static String TRANSACTION_TYPE = "XCA_EPSOS2";

	public List<AffinityDomain> listAllAffinityDomains() {
		return listAllAffinityDomains(TRANSACTION_TYPE);
	}
	
	public void init(){
		//////////
		initSamlAttributes();
		useXUA = false;
		selectedConfiguration = null;
		displayResultPanel = false;
		message = null;
		///////////
		this.selectedTransaction = Transaction.GetTransactionByKeyword("FetchDocumentService");
		this.selectedAffinityDomain = AffinityDomain.getAffinityDomainByKeyword("epSOS-2");
		this.initFromMenu = true;
		this.commonRetrieve = new CommonRetrieveManager<XCARespConfiguration, XCAMessage>() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void updateSpecificClient() {
				// NOT Used
			}
			
			@Override
			public void listAllConfigurations() {
				// NOT Used
			}
			
			@Override
			public List<AffinityDomain> listAllAffinityDomains() {
				// NOT Used
				return null;
			}
			
			@Override
			public String previewMessage() throws IOException {
				// NOT Used
				return null;
			}

			@Override
			public String getTheNameOfThePage() {
				// Not Used
				return null;
			}
		};
		this.initCommonRetrieve();
	}
	
	@Override
	public String getTheNameOfThePage() {
		String res = this.getNameOfTheAffinityDomainTransaction();
		if (res == null || res.equals("")) res = "epSOS-2 - FetchDocumentService";
		return res;
	}
	
	@Override
	public String getTRansactionPictureName(){
		return "/img/FDS.png";
	}
	
}
