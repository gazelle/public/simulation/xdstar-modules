package net.ihe.gazelle.xdstar.xca.initgw.action;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.simulator.sut.action.AbstractSystemConfigurationManager;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.xdstar.common.model.AbstractMessageQuery;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationFiltering;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationType;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationTypeQuery;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationTypeType;
import net.ihe.gazelle.xdstar.xca.initgw.model.XCARespConfiguration;
import net.ihe.gazelle.xdstar.xca.initgw.model.XCARespConfigurationQuery;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

@Name("xcaRespConfigurationManager")
@Scope(ScopeType.PAGE)
public class XCARespConfigurationManager extends AbstractSystemConfigurationManager<XCARespConfiguration> implements Serializable, UserAttributeCommon {

	private static final long serialVersionUID = 1L;

	@In(value="gumUserService")
	private UserService userService;

	@Override
	public void addConfiguration() {
		selectedSystemConfiguration = new XCARespConfiguration();
		selectedSystemConfiguration.setIsPublic(true);
		editSelectedConfiguration();
	}
	
	@Override
	public void copySelectedConfiguration(XCARespConfiguration inConfiguration) {
		this.selectedSystemConfiguration = new XCARespConfiguration(inConfiguration);
		editSelectedConfiguration();		
	}

	@Override
	public void deleteSelectedSystemConfiguration() {
		if (isSelectedConfigurationAlreadyUsed()) {
			this.deleteSelectedSystemConfiguration(XCARespConfiguration.class);
		} else {
			FacesMessages.instance().add(StatusMessage.Severity.WARN, "Unable to delete this configuration because it is already used !");
		}
	}

	public boolean isSelectedConfigurationAlreadyUsed() {
		XCARespConfiguration xcaRespConfiguration = getSelectedSystemConfiguration();
		if (xcaRespConfiguration != null ) {
			AbstractMessageQuery q = new AbstractMessageQuery();
			q.configuration().id().eq(xcaRespConfiguration.getId());
			return q.getCount() == 0;
		}
		return false;
	}
	
	@Override
	public List<Usage> getPossibleListUsages() {
		ConfigurationTypeQuery qq = new ConfigurationTypeQuery();
		qq.confTypeType().eq(ConfigurationTypeType.RESPONDING_GATEWAY_CONF);
		ConfigurationType conf  = qq.getUniqueResult();
		if (conf != null){
			return conf.getListUsages();
		}
		return null;
	}

	@Override
	public void modifyQuery(HQLQueryBuilder<XCARespConfiguration> queryBuilder, Map<String, Object> filterValuesApplied) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected HQLCriterionsForFilter<XCARespConfiguration> getHQLCriterionsForFilter() {
		return new ConfigurationFiltering<XCARespConfiguration, XCARespConfigurationQuery>(new XCARespConfigurationQuery()).getCriterionList();
	}

	@Override
	public String getUserName(String userId) {
		return userService.getUserDisplayNameWithoutException(userId);
	}
}
