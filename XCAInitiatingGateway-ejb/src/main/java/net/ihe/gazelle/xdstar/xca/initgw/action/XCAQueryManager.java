/* Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.xca.initgw.action;

import com.rits.cloning.Cloner;
import net.ihe.gazelle.metadata.common.RIMGenerator;
import net.ihe.gazelle.metadata.model.SlotMetadata;
import net.ihe.gazelle.query.AdhocQueryRequestType;
import net.ihe.gazelle.query.ResponseOptionType;
import net.ihe.gazelle.rim.AdhocQueryType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.rim.ValueListType;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.xua.SignatureException;
import net.ihe.gazelle.xdstar.common.model.ContentType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import net.ihe.gazelle.xdstar.comon.util.ConfigurationFilter;
import net.ihe.gazelle.xdstar.core.MessageSenderCommon;
import net.ihe.gazelle.xdstar.core.soap.SOAPRequestBuilder;
import net.ihe.gazelle.xdstar.rsq.common.CommonAdhocQueryManager;
import net.ihe.gazelle.xdstar.xca.initgw.model.XCAMessage;
import net.ihe.gazelle.xdstar.xca.initgw.model.XCARespConfiguration;
import net.ihe.gazelle.xdstar.xca.initgw.model.XCARespConfigurationQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.log.Log;

import javax.xml.parsers.ParserConfigurationException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Abderrazek Boufahja
 */

@Name("xcaQueryManager")
@Scope(ScopeType.PAGE)
public class XCAQueryManager extends CommonAdhocQueryManager<XCARespConfiguration, XCAMessage> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static String TRANSACTION_TYPE = "XCA_QUERY_IHE";

    @Logger
    private static Log log;

    public void listAllConfigurations() {
        XCARespConfigurationQuery rc = new XCARespConfigurationQuery();
        if (selectedAffinityDomain != null) {
            rc.listUsages().affinity().eq(selectedAffinityDomain);
        }
        if (selectedTransaction != null) {
            rc.listUsages().transaction().eq(selectedTransaction);
        }
        rc.name().order(true);
        ConfigurationFilter.filterConfigurationQueryAccodingToUserCredential(rc);
        configurations = rc.getList();
    }

    public void sendMessage() {
        message = new XCAMessage();
        message.setMessageType(selectedMessageType.getName());
        message.setTransaction(selectedTransaction);
        message.setAffinityDomain(selectedAffinityDomain);
        message.setContentType(ContentType.SOAP_MESSAGE);
        message.setTimeStamp(new Date());
        message.setGazelleDriven(false);
        message.setConfiguration(selectedConfiguration);

        MetadataMessage sent = new MetadataMessage();
        String messageToSend = this.createMessage();
        sent.setMessageContent(messageToSend);
        sent.setMessageType(MetadataMessageType.ADHOC_QUERY_REQUEST);
        message.setSentMessageContent(sent);
        message = XCAMessage.storeMessage(message);

        MessageSenderCommon sender = new MessageSenderCommon(selectedConfiguration.getUrl(), message);
        try {
            sender.sendMessageSOAP(MetadataMessageType.ADHOC_QUERY_RESPONSE);
            message = (XCAMessage) sender.getRequest();
            message = (XCAMessage) XCAMessage.storeMessage(message);
        } catch (Exception e) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.xca.initgw.errorWhenSendingMessage", e.getMessage
                    ());
        }
        Contexts.getSessionContext().set("messagesToDisplay", message);
        displayResultPanel = true;
    }

    public void updateSpecificClient() {
        // TODO Auto-generated method stub

    }

    public List<AffinityDomain> listAllAffinityDomains() {
        return listAllAffinityDomains(TRANSACTION_TYPE);
    }

    public String createMessage() {
        String res = null;
        AdhocQueryRequestType aqr = new AdhocQueryRequestType();
        aqr.setAdhocQuery(new AdhocQueryType());
        aqr.getAdhocQuery().setId(this.adhocQuery.getId());
        if ((this.selectedConfiguration.getHomeCommunityId() != null) &&
                (!this.selectedConfiguration.getHomeCommunityId().equals(""))) {
            aqr.getAdhocQuery().setHome(this.selectedConfiguration.getHomeCommunityId());
        }
        Cloner cloner = new Cloner();
        for (SlotType1 sl : this.adhocQuery.getSlot()) {
            SlotType1 newsl = cloner.deepClone(sl);
            SlotMetadata sm = RIMGenerator.slWeak.get(sl);
            if ((sm.getIsNumber() == null) || !sm.getIsNumber()) {
                for (String ss : sl.getValueList().getValue()) {
                    newsl.getValueList().getValue().remove(ss);
                    newsl.getValueList().getValue().add("'" + ss.replace("'", "''") + "'");
                }
            }
            if ((sm.getMultiple() != null && sm.getMultiple()) || (sm.getSupportAndOr() != null && sm.getSupportAndOr())) {
                ValueListType vlt = cloner.deepClone(newsl.getValueList());
                for (String ss : vlt.getValue()) {
                    newsl.getValueList().getValue().remove(ss);
                    newsl.getValueList().getValue().add("(" + ss + ")");
                }
            }
            aqr.getAdhocQuery().getSlot().add(newsl);
        }
        aqr.setResponseOption(new ResponseOptionType());
        aqr.getResponseOption().setReturnComposedObjects(true);
        aqr.getResponseOption().setReturnType(this.selectedReturnType);
        res = this.getAdhocQueryRequestTypeAsString(aqr);
        String receiverUrl = this.selectedConfiguration.getUrl();
        try {
            res = SOAPRequestBuilder.createSOAPMessage(res, useXUA, praticianID, attributes, null, receiverUrl,
                    "urn:ihe:iti:2007:CrossGatewayQuery", null);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public String getTheNameOfThePage() {
        String res = this.getNameOfTheAffinityDomainTransaction();
        if (res == null || res.trim().equals("")) {
            res = "Cross Gateway Query";
        }
        return res;
    }
}
