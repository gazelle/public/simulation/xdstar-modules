/* Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.discard.action;

import net.ihe.gazelle.lcm.RemoveObjectsRequestType;
import net.ihe.gazelle.rim.ObjectRefListType;
import net.ihe.gazelle.rim.ObjectRefType;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.xua.SignatureException;
import net.ihe.gazelle.xdstar.common.action.CommonSimulatorManager;
import net.ihe.gazelle.xdstar.common.model.ContentType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import net.ihe.gazelle.xdstar.comon.util.ConfigurationFilter;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespacePrefixMapper;
import net.ihe.gazelle.xdstar.conf.model.RegistryConfiguration;
import net.ihe.gazelle.xdstar.conf.model.RegistryConfigurationQuery;
import net.ihe.gazelle.xdstar.core.MessageSenderCommon;
import net.ihe.gazelle.xdstar.core.soap.SOAPRequestBuilder;
import net.ihe.gazelle.xdstar.discard.model.DiscardMessage;
import net.ihe.gazelle.xdstar.xca.initgw.model.XCAMessage;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Abderrazek Boufahja
 */

@Name("discardManager")
@Scope(ScopeType.PAGE)
public class DiscardManager extends CommonSimulatorManager<RegistryConfiguration, DiscardMessage> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    private static Logger log = LoggerFactory.getLogger(DiscardManager.class);


    private static String TRANSACTION_TYPE = "DISC";

    private String selectedMessageType;

    private RemoveObjectsRequestType removeObjectsRequest;

    private ObjectRefType selectedObjectRef;

    private Boolean useTRC = false;

    private String patientId = null;

    public static void main(String[] args) {
        DiscardManager dis = new DiscardManager();
        dis.removeObjectsRequest = new RemoveObjectsRequestType();
        dis.removeObjectsRequest.setObjectRefList(new ObjectRefListType());
        dis.initSelectedObjectRef();
        dis.selectedObjectRef.setId("rrrrr");
        dis.addSelectedObjectRef();
        System.out.println(dis.removeObjectsRequest.getObjectRefList().getObjectRef().size());
    }

    public ObjectRefType getSelectedObjectRef() {
        return selectedObjectRef;
    }

    public void setSelectedObjectRef(ObjectRefType selectedObjectRef) {
        this.selectedObjectRef = selectedObjectRef;
    }

    public String getSelectedMessageType() {
        return selectedMessageType;
    }

    public void setSelectedMessageType(String selectedMessageType) {
        this.selectedMessageType = selectedMessageType;
    }

    public RemoveObjectsRequestType getRemoveObjectsRequest() {
        return removeObjectsRequest;
    }

    public void setRemoveObjectsRequest(
            RemoveObjectsRequestType removeObjectsRequest) {
        this.removeObjectsRequest = removeObjectsRequest;
    }

    public Boolean getUseTRC() {
        return useTRC;
    }

    public void setUseTRC(Boolean useTRC) {
        this.useTRC = useTRC;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    @Override
    public void init() {
        super.init();
        this.removeObjectsRequest = new RemoveObjectsRequestType();
        this.removeObjectsRequest.setObjectRefList(new ObjectRefListType());
    }

    public void listAllConfigurations() {
        RegistryConfigurationQuery rc = new RegistryConfigurationQuery();
        if (selectedAffinityDomain != null) {
            rc.listUsages().affinity().eq(selectedAffinityDomain);
        }
        if (selectedTransaction != null) {
            rc.listUsages().transaction().eq(selectedTransaction);
        }
        rc.name().order(true);
        ConfigurationFilter.filterConfigurationQueryAccodingToUserCredential(rc);
        configurations = rc.getList();
    }

    public void sendMessage() {
        message = new DiscardMessage();
        message.setMessageType(selectedMessageType);
        message.setTransaction(selectedTransaction);
        message.setAffinityDomain(selectedAffinityDomain);
        message.setContentType(ContentType.SOAP_MESSAGE);
        message.setTimeStamp(new Date());
        message.setGazelleDriven(false);
        message.setConfiguration(selectedConfiguration);

        MetadataMessage sent = new MetadataMessage();
        String messageToSend = this.createMessage();
        sent.setMessageContent(messageToSend);
        sent.setMessageType(MetadataMessageType.REMOVE_OBJECT_REQUEST);
        message.setSentMessageContent(sent);
        message = XCAMessage.storeMessage(message);

        MessageSenderCommon sender = new MessageSenderCommon(selectedConfiguration.getUrl(), message);
        try {
            sender.sendMessageSOAP(MetadataMessageType.REGIQTRY_RESPONSE);
            message = (DiscardMessage) sender.getRequest();
            message = (DiscardMessage) DiscardMessage.storeMessage(message);
        } catch (Exception e) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.xca.initgw.errorWhenSendingMessage", e.getMessage
                    ());
        }
        Contexts.getSessionContext().set("messagesToDisplay", message);
        displayResultPanel = true;
    }

    public void updateSpecificClient() {
        if (this.selectedTransaction != null) {
            this.selectedMessageType = this.selectedTransaction.getKeyword();
        }
    }

    public List<AffinityDomain> listAllAffinityDomains() {
        return listAllAffinityDomains(TRANSACTION_TYPE);
    }

    public String createMessage() {
        if (this.removeObjectsRequest == null) {
            return "an error occure, reload your page";
        }
        String res = this.getRemoveObjectsRequestTypeAsString(removeObjectsRequest);
        try {
            res = SOAPRequestBuilder.createSOAPMessage(res, useXUA, praticianID, attributes, patientId, this.selectedConfiguration.getUrl(),
                    "urn:ihe:iti:2010:DeleteDocumentSet", useTRC);
        } catch (ParserConfigurationException e) {
            log.info("ParserConfigurationException error", e);
        } catch (SignatureException e) {
            log.info("SignatureException error", e);
        }
        return res;
    }

    public String previewMessage() {
        String res = this.createMessage();
        return res;
    }

    @Override
    public String getTheNameOfThePage() {
        String res = this.getNameOfTheAffinityDomainTransaction();
        if (res == null || res.trim().equals("")) {
            res = "Delete Document Set";
        }
        return res;
    }

    private String getRemoveObjectsRequestTypeAsString(RemoveObjectsRequestType aqr) {
        try {
            JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.lcm");
            Marshaller m = jc.createMarshaller();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new XDSNamespacePrefixMapper());
            m.marshal(aqr, baos);
            return baos.toString();
        } catch (Exception e) {
            return null;
        }
    }

    public void addSelectedObjectRef() {
        this.removeObjectsRequest.getObjectRefList().addObjectRef(selectedObjectRef);
    }

    public void initSelectedObjectRef() {
        this.selectedObjectRef = new ObjectRefType();
    }

    public String getImageDiscard() {
        final String res = "/img/DISC.gif";
        if (this.selectedTransaction == null) {
            return res;
        }
        if (this.selectedTransaction.getKeyword().toLowerCase().contains("dispensation")) {
            return "/img/DISC_DISP.gif";
        }
        if (this.selectedTransaction.getKeyword().toLowerCase().contains("consent")) {
            return "/img/DISC_CONS.gif";
        }
        return res;
    }

    public void deleteObjectRef(ObjectRefType objref) {
        if (objref == null) {
            return;
        }
        ObjectRefType objtodel = null;
        for (ObjectRefType obj : this.removeObjectsRequest.getObjectRefList().getObjectRef()) {
            if (obj.getId() != null && obj.getId().equals(objref.getId())) {
                objtodel = obj;
            }
        }
        if (objtodel != null) {
            this.removeObjectsRequest.getObjectRefList().getObjectRef().remove(objtodel);
        }
    }

    @Override
    public void reset() {
        super.reset();
        this.patientId = null;
        this.useXUA = false;
        this.useTRC = false;
        this.removeObjectsRequest = new RemoveObjectsRequestType();
        this.removeObjectsRequest.setObjectRefList(new ObjectRefListType());
    }

    public String extractIDFromInitialisation() {
        if (this.selectedTransaction != null &&
                this.selectedTransaction.getKeyword().equals("DispensationService:Discard()") &&
                this.selectedAffinityDomain != null &&
                this.selectedAffinityDomain.getKeyword().equals("epSOS")) {
            return "DISPDISC";
        } else if (this.selectedTransaction != null &&
                this.selectedTransaction.getKeyword().equals("ConsentService:Discard()") &&
                this.selectedAffinityDomain != null &&
                this.selectedAffinityDomain.getKeyword().equals("epSOS")) {
            return "CONSDISC";
        } else if (this.selectedTransaction != null &&
                this.selectedTransaction.getKeyword().equals("ITI-62") &&
                this.selectedAffinityDomain != null &&
                this.selectedAffinityDomain.getKeyword().equals("IHE_XDS-b")) {
            return "XDSB4";
        } else if (this.selectedTransaction != null &&
                this.selectedTransaction.getKeyword().equals("ITI-62") &&
                this.selectedAffinityDomain != null &&
                this.selectedAffinityDomain.getKeyword().equals("IHE_XDR")) {
            return "XDR2";
        }
        return null;
    }

}
