package net.ihe.gazelle.metadata.common;

import java.util.UUID;
import java.util.WeakHashMap;

import net.ihe.gazelle.metadata.model.AdhocQueryMetadata;
import net.ihe.gazelle.metadata.model.ClassificationMetaData;
import net.ihe.gazelle.metadata.model.ExternalIdentifierMetadata;
import net.ihe.gazelle.metadata.model.ExtrinsicObjectMetadata;
import net.ihe.gazelle.metadata.model.RegistryPackageMetadata;
import net.ihe.gazelle.metadata.model.SlotMetadata;
import net.ihe.gazelle.rim.AdhocQueryType;
import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.ExtrinsicObjectType;
import net.ihe.gazelle.rim.InternationalStringType;
import net.ihe.gazelle.rim.RegistryPackageType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.rim.ValueListType;
import net.ihe.gazelle.xdstar.comon.util.RIMBuilderCommon;
import net.ihe.gazelle.xdstar.comon.util.Util;

public class RIMGenerator {
	
	public static WeakHashMap<ClassificationType, ClassificationMetaData> clWeak = new WeakHashMap<ClassificationType, ClassificationMetaData>();

	public static WeakHashMap<ExternalIdentifierType, ExternalIdentifierMetadata> extWeak = new WeakHashMap<ExternalIdentifierType, ExternalIdentifierMetadata>();
	
	public static WeakHashMap<RegistryPackageType, RegistryPackageMetadata> rpWeak = new WeakHashMap<RegistryPackageType, RegistryPackageMetadata>();
	
	public static WeakHashMap<ExtrinsicObjectType, ExtrinsicObjectMetadata> docWeak = new WeakHashMap<ExtrinsicObjectType, ExtrinsicObjectMetadata>();
	
	public static WeakHashMap<SlotType1, SlotMetadata> slWeak = new WeakHashMap<SlotType1, SlotMetadata>();
	
	public static WeakHashMap<AdhocQueryType, AdhocQueryMetadata> aqWeak = new WeakHashMap<AdhocQueryType, AdhocQueryMetadata>();
	
	@SuppressWarnings("unused")
	public static <T extends ExtrinsicObjectType> void generateExtrinsicObjectTypeChild(ExtrinsicObjectMetadata docm, T child){
		if (docm.getUuid() != null){
			child.setObjectType(docm.getUuid());
		}
		if (docm.getName() != null){
			child.setName(RIMBuilderCommon.createNameOrDescription(docm.getName()));
		}
		if (docm.getClassification_required() != null){
			for (ClassificationMetaData cl : docm.getClassification_required()) {
				ClassificationType clt = RIMGenerator.generateClassificationType(cl);
				child.getClassification().add(clt);
			}
		}

		if (docm.getExt_identifier_required() != null){
			for (ExternalIdentifierMetadata extm : docm.getExt_identifier_required()) {
				ExternalIdentifierType ext = RIMGenerator.generateExternalIdentifierType(extm);
				child.getExternalIdentifier().add(ext);
			}
		}
		if (docm.getSlot_required() != null){
			for (SlotMetadata slm : docm.getSlot_required()) {
				SlotType1 sl = generateSlot(slm);
				if (sl.getName().equals("creationTime")) sl.getValueList().getValue().add(Util.getDate());
				child.getSlot().add(sl);
			}
		}
		for (SlotMetadata ss : docm.getSlot_optional()) {}	
		docWeak.put(child, docm);
	}
	
	public static <T extends RegistryPackageType> void generateRegistryPackageTypeChild(RegistryPackageMetadata rpm, T child){
		child.setObjectType("urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:RegistryPackage");
		child.setId("urn:uuid:" + UUID.randomUUID().toString());
		if (rpm.getName() != null){
			InternationalStringType name = RIMBuilderCommon.createNameOrDescription(rpm.getName());
			child.setName(name);
		}
		if (rpm.getClassification_required() != null){
			for (ClassificationMetaData cl : rpm.getClassification_required()) {
				ClassificationType clt = RIMGenerator.generateClassificationType(cl);
				child.getClassification().add(clt);
			}
		}
		
		if (rpm.getExt_identifier_required() != null){
			for (ExternalIdentifierMetadata extm : rpm.getExt_identifier_required()) {
				ExternalIdentifierType ext = RIMGenerator.generateExternalIdentifierType(extm);
				child.getExternalIdentifier().add(ext);
			}
		}
		if (rpm.getSlot_required() != null){
			for (SlotMetadata slm : rpm.getSlot_required()) {
				SlotType1 sl = generateSlot(slm);
				child.getSlot().add(sl);
			}
		}
		rpWeak.put(child, rpm);
	}
	
	public static <T extends AdhocQueryType> void generateAdhocQueryTypeChild(AdhocQueryMetadata aqm, T child){
		child.setId(aqm.getUuid());
		if (aqm.getSlot_required() != null){
			for (SlotMetadata slm : aqm.getSlot_required()) {
				SlotType1 sl = generateSlot(slm);
				child.getSlot().add(sl);
			}
		}
		aqWeak.put(child, aqm);
	}
	
	public static ClassificationType generateClassificationType(ClassificationMetaData clm){
		ClassificationType cl = new ClassificationType();
		cl.setObjectType("urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification");
		if (clm != null){
			cl.setId("urn:uuid:" + UUID.randomUUID().toString());
			cl.setClassificationScheme(clm.getUuid());
			if (clm.getName() != null){
				cl.setName(RIMBuilderCommon.createNameOrDescription(clm.getName()));
			}
			if (clm.getValue() != null){
				cl.setNodeRepresentation(clm.getValue());
			}
			if (clm.getSlot_required() != null){
				for (SlotMetadata slm : clm.getSlot_required()) {
					SlotType1 sl = generateSlot(slm);
					cl.getSlot().add(sl);
				}
			}
		}
		clWeak.put(cl, clm);
		return cl;
	}
	
	public static ExternalIdentifierType generateExternalIdentifierType(ExternalIdentifierMetadata extm){
		ExternalIdentifierType ext = new ExternalIdentifierType();
		ext.setObjectType("urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier");
		if (extm != null){
			ext.setId("urn:uuid:" + UUID.randomUUID().toString());
			ext.setIdentificationScheme(extm.getUuid());
			ext.setValue(extm.getValue());
			if (extm.getName() != null){
				ext.setName(RIMBuilderCommon.createNameOrDescription(extm.getName()));
			}
		}
		extWeak.put(ext, extm);
		return ext;
	}
	
	public static SlotType1 generateSlot(SlotMetadata slm){
		SlotType1 sl = new SlotType1();
		if (slm != null){
			sl.setName(slm.getName());
			sl.setValueList(new ValueListType());
			if ((slm.getDefaultValue() != null) && (!slm.getDefaultValue().equals("")) ){
				sl.getValueList().getValue().add(slm.getDefaultValue());
			}
		}
		slWeak.put(sl, slm);
		return sl;
	}

}
