/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.metadata.converter;


import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.persistence.EntityManager;

import net.ihe.gazelle.metadata.model.SlotMetadata;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.log.Log;


/**
 * 
 * @author abderrazek boufahja
 *
 */
@BypassInterceptors
@Name("smConverter")
@Converter(forClass=SlotMetadata.class)
public class SlotMetadataConverter implements javax.faces.convert.Converter, Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Logger
    public static  Log log;

    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
        if (value instanceof SlotMetadata)
        {
        	SlotMetadata cc = (SlotMetadata) value;
            return cc.toString();
        }
        else
        {
            return null;
        }

    }


    @Transactional
    public Object getAsObject(FacesContext Context,
            UIComponent Component, String value) throws ConverterException {
        if (value != null)
        {
            try
            {

                EntityManager entityManager = (EntityManager)org.jboss.seam.Component.getInstance("entityManager",true) ;

                SlotMetadata testInstance = entityManager.find(SlotMetadata.class, value) ;

                return testInstance ;

            }
            catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


}
