package net.ihe.gazelle.xdstar.rsq.common;

/**
 * 
 * @author abderrazek boufahja
 *
 */
public class XDSEntryDescriber {

	public XDSEntryDescriber() {
	}

	private String patientId;

	private String title;

	private String creationTime;

	private String status;

	private String homeCommunityId;

	private String repositoryUniqueId;

	private String documentUniqueId;

	private String mimeType;

	private String uri;

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getHomeCommunityId() {
		return homeCommunityId;
	}

	public void setHomeCommunityId(String homeCommunityId) {
		this.homeCommunityId = homeCommunityId;
	}

	public String getRepositoryUniqueId() {
		return repositoryUniqueId;
	}

	public void setRepositoryUniqueId(String repositoryUniqueId) {
		this.repositoryUniqueId = repositoryUniqueId;
	}

	public String getDocumentUniqueId() {
		return documentUniqueId;
	}

	public void setDocumentUniqueId(String documentUniqueId) {
		this.documentUniqueId = documentUniqueId;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result)
				+ ((creationTime == null) ? 0 : creationTime.hashCode());
		result = (prime * result)
				+ ((documentUniqueId == null) ? 0 : documentUniqueId.hashCode());
		result = (prime * result)
				+ ((homeCommunityId == null) ? 0 : homeCommunityId.hashCode());
		result = (prime * result)
				+ ((mimeType == null) ? 0 : mimeType.hashCode());
		result = (prime * result)
				+ ((patientId == null) ? 0 : patientId.hashCode());
		result = (prime * result)
				+ ((repositoryUniqueId == null) ? 0 : repositoryUniqueId
						.hashCode());
		result = (prime * result) + ((status == null) ? 0 : status.hashCode());
		result = (prime * result) + ((title == null) ? 0 : title.hashCode());
		result = (prime * result) + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		XDSEntryDescriber other = (XDSEntryDescriber) obj;
		if (creationTime == null) {
			if (other.creationTime != null) {
				return false;
			}
		} else if (!creationTime.equals(other.creationTime)) {
			return false;
		}
		if (documentUniqueId == null) {
			if (other.documentUniqueId != null) {
				return false;
			}
		} else if (!documentUniqueId.equals(other.documentUniqueId)) {
			return false;
		}
		if (homeCommunityId == null) {
			if (other.homeCommunityId != null) {
				return false;
			}
		} else if (!homeCommunityId.equals(other.homeCommunityId)) {
			return false;
		}
		if (mimeType == null) {
			if (other.mimeType != null) {
				return false;
			}
		} else if (!mimeType.equals(other.mimeType)) {
			return false;
		}
		if (patientId == null) {
			if (other.patientId != null) {
				return false;
			}
		} else if (!patientId.equals(other.patientId)) {
			return false;
		}
		if (repositoryUniqueId == null) {
			if (other.repositoryUniqueId != null) {
				return false;
			}
		} else if (!repositoryUniqueId.equals(other.repositoryUniqueId)) {
			return false;
		}
		if (status == null) {
			if (other.status != null) {
				return false;
			}
		} else if (!status.equals(other.status)) {
			return false;
		}
		if (title == null) {
			if (other.title != null) {
				return false;
			}
		} else if (!title.equals(other.title)) {
			return false;
		}
		if (uri == null) {
			if (other.uri != null) {
				return false;
			}
		} else if (!uri.equals(other.uri)) {
			return false;
		}
		return true;
	}

}
