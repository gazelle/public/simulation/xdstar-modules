package net.ihe.gazelle.xdstar.common.action;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.HQLReloader;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.xdstar.common.model.TransactionType;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

@Name("TTManager")
@Scope(ScopeType.PAGE)
public class TransactionTypeManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private TransactionType selectedTransactionType;

    private Transaction selectedTransaction;

    private AffinityDomain selectedAffinityDomain;

    public TransactionType getSelectedTransactionType() {
        if (this.selectedTransactionType == null) {
            this.selectedTransactionType = new TransactionType();
        }
        return selectedTransactionType;
    }

    public void setSelectedTransactionType(TransactionType selectedTransactionType) {
        this.selectedTransactionType = selectedTransactionType;
    }

    public Transaction getSelectedTransaction() {
        if (this.selectedTransaction == null) {
            this.selectedTransaction = new Transaction();
        }
        return selectedTransaction;
    }

    public void setSelectedTransaction(Transaction selectedTransaction) {
        this.selectedTransaction = selectedTransaction;
    }

    public AffinityDomain getSelectedAffinityDomain() {
        if (this.selectedAffinityDomain == null) {
            this.selectedAffinityDomain = new AffinityDomain();
        }
        return selectedAffinityDomain;
    }

    public void setSelectedAffinityDomain(AffinityDomain selectedAffinityDomain) {
        this.selectedAffinityDomain = selectedAffinityDomain;
    }

    public void mergeObject(Object obj) {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        em.merge(obj);
        em.flush();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Object saved !");
    }

    public void deleteObject(Object obj) {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        em.remove(obj);
        em.flush();
    }

    public void deleteSelectedTransactionType() {
        selectedTransactionType = HQLReloader.reloadDetached(selectedTransactionType);
        deleteObject(selectedTransactionType);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Transaction type deleted !");
    }

    public void deleteSelectedTransaction() {
        selectedTransaction = HQLReloader.reloadDetached(selectedTransaction);
        deleteObject(selectedTransaction);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Transaction deleted !");
    }

    public void deleteSelectedAffinityDomain() {
        selectedAffinityDomain = HQLReloader.reloadDetached(selectedAffinityDomain);
        deleteObject(selectedAffinityDomain);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Affinity Domain deleted !");
    }

    public List<TransactionType> getListTransactionType() {
        HQLQueryBuilder<TransactionType> hh = new HQLQueryBuilder<TransactionType>(TransactionType.class);
        return hh.getList();
    }

    public void init() {
        this.selectedAffinityDomain = null;
        this.selectedTransaction = null;
        this.selectedTransactionType = null;
    }

}
