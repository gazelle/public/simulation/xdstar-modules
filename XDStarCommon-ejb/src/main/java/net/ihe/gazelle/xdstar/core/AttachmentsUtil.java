package net.ihe.gazelle.xdstar.core;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.xdstar.common.model.AttachmentFile;

import org.apache.axiom.attachments.Attachments;
import org.apache.axiom.attachments.IncomingAttachmentInputStream;
import org.apache.axiom.attachments.IncomingAttachmentStreams;
import org.apache.commons.io.IOUtils;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author abderrazek boufahja
 *
 */
public class AttachmentsUtil {


	private static Logger log = LoggerFactory.getLogger(AttachmentsUtil.class);

	public static String transformMultipartToSOAP(InputStream inStream, String contentTypeString) {
		try{
			Attachments attachments = new Attachments(inStream, contentTypeString);
			return AttachmentsUtil.transformMultipartToSOAP(attachments);
		}
		catch(Exception e){
			e.printStackTrace();

		}
		return null;
	}

	public static String transformMultipartToSOAP(Attachments attachments) {
		InputStream rr = attachments.getSOAPPartInputStream();
		return transformInputToString(rr);
	}

	public static String transformInputToString(InputStream inputStream){
		try{
			BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));

			StringBuffer response = new StringBuffer();
			String line = null;
			response.append(in.readLine());
			while((line = in.readLine()) != null)
			{
				response.append(line);
				response.append("\n");
			}
			in.close();
			return response.toString();
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}



	public static List<AttachmentFile> generateListAttachmentFiles(Attachments attachments, String reqsoap) throws IOException{
		return generateListAttachmentFiles(attachments, reqsoap, true);
	}

	public static List<AttachmentFile> generateListAttachmentFiles(Attachments attachments, String reqsoap, boolean persist) throws IOException{
		List<AttachmentFile> res = null;
		boolean findxop = findIfAttachmentExists(reqsoap);
		if (findxop){
			IncomingAttachmentStreams tt = attachments.getIncomingAttachmentStreams();
			while (tt.isReadyToGetNextStream()){
				IncomingAttachmentInputStream oo = null;
				try{
					if (tt.isReadyToGetNextStream())
						oo = tt.getNextStream();
				}
				catch(Throwable e){
					log.info("empty attachments stream !!");
				}
				if (oo != null){
					AttachmentFile af = new AttachmentFile();
					af.setContentID(oo.getContentId());
					af.setContentTransferEncoding(oo.getHeader("Content-Transfer-Encoding"));
					af.setContentType(oo.getContentType());
					if (persist){
						try{
							EntityManager em = (EntityManager) Component.getInstance("entityManager");
							em.persist(af);
							FileOutputStream out = new FileOutputStream(ApplicationConfiguration.getValueOfVariable("attachement_files_directory") + "/" + af.getId());
							IOUtils.copy(oo, out);
							oo.close();
							out.close();
						}
						catch(Exception e) {
							log.error("no persistence was performed.");
						}
					}
					if (res == null) {
						res = new ArrayList<AttachmentFile>();
					}
					res.add(af);
				}
				else{
					break;
				}
			}
		}
		return res;
	}

	private static boolean findIfAttachmentExists(String reqsoap) {
		boolean match = false;
		Pattern pat = Pattern.compile("Include.*?href=.*?", Pattern.DOTALL|Pattern.MULTILINE);
		Matcher mat = pat.matcher(reqsoap);
		if (mat.find()){
			match = true;
		}
		return match;
	}

}
