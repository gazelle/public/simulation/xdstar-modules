package net.ihe.gazelle.xdstar.users.action;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.ssov7.authn.interlay.adapter.GazelleIdentityImpl;
import net.ihe.gazelle.xdstar.common.model.AbstractMessage;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

@Scope(ScopeType.PAGE)
@Name("userManager")
@Synchronized(timeout = 10000)
@GenerateInterface("UserManagerLocal")
public class UserManager implements Serializable, UserManagerLocal {

    private static final Logger LOG = LoggerFactory.getLogger(UserManager.class);

    private String getMessageCreator(AbstractMessage message) {
        return message.getConfiguration().getOwner();
    }

    private boolean confIsPublic(AbstractMessage message) {
        return message.getConfiguration().getIsPublic();
    }

    public boolean userIsCreator(String username, AbstractMessage message) {
        return userIsLoggedIn() && username != null && username.equals(getMessageCreator(message));
    }

    public boolean userIsLoggedIn() {
        return GazelleIdentityImpl.instance().isLoggedIn();
    }

    public boolean userIsAdmin() {
        return GazelleIdentityImpl.instance().hasRole("admin_role");
    }


    public boolean userIsMonitor() {
        return GazelleIdentityImpl.instance().hasRole("monitor_role");
    }

    public static boolean isLoggedUserMonitor() {
        return GazelleIdentityImpl.instance().hasRole("monitor_role");
    }

    public static boolean isLoggedUserAdmin() {
        return GazelleIdentityImpl.instance().hasRole("admin_role");
    }

    public String getLoggedInUsername() {
        if (userIsLoggedIn()) {
            return GazelleIdentityImpl.instance().getUsername();
        }
        return "";
    }


    public boolean userCanValidateMessage(String username, AbstractMessage message) {
        if (userIsCreator(username, message) || confIsPublic(message)
                || userIsAdmin() || userIsMonitor()) {
            return true;
        }
        return false;
    }

}
