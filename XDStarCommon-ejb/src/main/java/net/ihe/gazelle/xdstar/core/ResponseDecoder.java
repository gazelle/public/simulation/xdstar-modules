/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.ihe.gazelle.rim.AssociationType1;
import net.ihe.gazelle.simulator.common.model.DocumentMetadata;
import net.ihe.gazelle.xdstar.comon.util.Util;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  <b>Class Description :  </b>ResponseDecoder<br><br>
 *  
 * @class			ResponseDecoder
 * @package			net.ihe.gazelle.simulator.xca.initgw.core
 * @author 			Abderrazek Boufahja / IHE Europe
 * @author			Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version			2.0 - 2010, December 7th
 *
 */

public class ResponseDecoder {
	
	
	private static final String AT_NAME = "@name";

	private static final String AT_VALUE = "@value";

	private static final String AT_STATUS = "@status";

	private static Logger log = LoggerFactory.getLogger(ResponseDecoder.class);
	
	private Node response;
	
	private ResponseType decodingType;
	
	private List<DocumentMetadata> documentInformations;
	
	private List<AssociationType1> associationsList;
	
	private List<String> documentsUUID;
	
	private boolean leafClass;
	
	/**
	 * Empty constructor
	 */
	public ResponseDecoder()
	{
		
	}
	
	/**
	 * 
	 * @param inStringResponse
	 * @param wrappedInSoap
	 * @param isLeafClass
	 */
	public ResponseDecoder(String inStringResponse, boolean wrappedInSoap, boolean isLeafClass)
	{
		this.leafClass = isLeafClass;
		
		if (inStringResponse.contains("RetrieveDocumentSetResponse"))
		{
			decodingType = ResponseType.RETRIEVE_DOCUMENT;
		}
		else if (!leafClass)
		{
			decodingType = ResponseType.REGISTRY_OBJECT_UUID;
		}
		else if (inStringResponse.contains("RegistryPackage"))
		{
			decodingType = ResponseType.REGISTRY_PACKAGE;
		}
		else if (inStringResponse.contains("ExtrinsicObject"))
		{
			decodingType = ResponseType.DOCUMENT_ENTRY;
		}
		else if (inStringResponse.contains("RegistryError"))
		{
			decodingType = ResponseType.REGISTRY_ERROR;
		}
		else
		{
			decodingType = ResponseType.REGISTRY_PACKAGE;
		}
		
		if (wrappedInSoap)
		{
			this.response = extractXDSResponseFromSoapEnvelop(inStringResponse);
		}
		else
		{
			Document xdsResponse = Util.stringToXmlTransformerDocument(inStringResponse);
			this.response = xdsResponse.getRootElement();
		}
	}
	
	public void setResponse(Node response) {
		this.response = response;
	}

	public Node getResponse() {
		return response;
	}

	public void setDecodingType(ResponseType decodingType) {
		this.decodingType = decodingType;
	}

	public ResponseType getDecodingType() {
		return decodingType;
	}

	public void setDocumentInformations(List<DocumentMetadata> documentInformations) {
		this.documentInformations = documentInformations;
	}

	public List<DocumentMetadata> getDocumentInformations() {
		return documentInformations;
	}

	
	public void setDocumentsUUID(List<String> documentsUUID) {
		this.documentsUUID = documentsUUID;
	}

	public List<String> getDocumentsUUID() {
		return documentsUUID;
	}

	public void setAssociationsList(List<AssociationType1> associations) {
		this.associationsList = associations;
	}

	public List<AssociationType1> getAssociationsList() {
		return associationsList;
	}

	public void setLeafClass(boolean leafClass) {
		this.leafClass = leafClass;
	}

	public boolean isLeafClass() {
		return leafClass;
	}

	public boolean decode()
	{
		switch(decodingType)
		{
			case REGISTRY_PACKAGE:
				extractRegistryPackageMetadata();
				extractAssociations();
				break;
			case DOCUMENT_ENTRY:
				extractDocumentEntryMetadata();
				extractAssociations();
				break;
			case REGISTRY_OBJECT_UUID:
				extractDocumentsFromResponse();
				if (documentsUUID == null){
					return false;
				}
				break;
			case RETRIEVE_DOCUMENT:
				break;
			default:
				return false;
		}
		return true;
	}
	
	/**
	 * Takes the XCA response and returns the content of the SOAP body
	 * @param inResponse
	 * @return
	 */
	private Node extractXDSResponseFromSoapEnvelop(String inResponse)
	{
		String currentInResponse = inResponse;
		if (currentInResponse.contains("MIMEBoundaryurn_uuid"))
		{
			int begin = currentInResponse.indexOf("<?xml");
			int end = currentInResponse.indexOf("--MIMEBoundaryurn", begin);
			currentInResponse = currentInResponse.substring(begin, end);
		}
		try{
			Document soapResponse = Util.stringToXmlTransformerDocument(currentInResponse);
			if (soapResponse == null){
				return null;
			}
			else {
				Element root = soapResponse.getRootElement();
				Node xdsResponse = null;
				if (decodingType == null) {
					return null;
				}
				else if (decodingType.equals(ResponseType.RETRIEVE_DOCUMENT)){
					xdsResponse = root.selectSingleNode("*[local-name()='Body'][1]/*[local-name()='RetrieveDocumentSetResponse'][1]");
				}
				else{ 
					xdsResponse = root.selectSingleNode("*[local-name()='Body'][1]/*[local-name()='AdhocQueryResponse'][1]");
				}
				
				if (xdsResponse != null){
					return xdsResponse.detach();
				}
				else{
					return null;
				}
			}
		}catch(Exception e){
			log.info("Cannot extract XDS response", e);
			return null;
		}
	}
	
	/**
	 * Returns the list of document UUIDs contained in the response
	 * @return
	 */
	private void extractDocumentsFromResponse()
	{
		try{
			Node registryObjectList = response.selectSingleNode("*[local-name()='RegistryObjectList'][1]");
			if (registryObjectList != null)
			{
				List<?> objectRefs = registryObjectList.selectNodes("*[local-name()='ObjectRef']");
				if (objectRefs != null && objectRefs.size() > 0)
				{
					documentsUUID = new ArrayList<String>();
					for (Object objectRef : objectRefs)
					{
						if (objectRef instanceof Node){
							documentsUUID.add(((Node)objectRef).valueOf("@id"));
						}
					}
				}
				else{
					documentsUUID = null;
				}
			}
		}catch(Exception e){
			log.info("Cannot parse response", e);
			documentsUUID = null;
		}
	}
	
	
	
	/**
	 * 
	 */
	private void extractRegistryPackageMetadata()
	{
		List<?> registryPackages = response.selectNodes("*[local-name()='RegistryObjectList'][1]/*[local-name()='RegistryPackage']");
		if (registryPackages != null && !registryPackages.isEmpty())
		{
			documentInformations = new ArrayList<DocumentMetadata>();
			for (Object object : registryPackages)
			{
				if (object instanceof Node)
				{
					Node rpackage = (Node) object;
					DocumentMetadata metadata = new DocumentMetadata();
					metadata.setStatus(rpackage.valueOf(ResponseDecoder.AT_STATUS));
					metadata.setMimeType(null);
					// registryPackage attributes
					if (rpackage.selectSingleNode("*[local-name()='Name'][1]/*[local-name()='LocalizedString'][1]") != null){
						metadata.setName(rpackage.selectSingleNode("*[local-name()='Name'][1]/*[local-name()='LocalizedString'][1]").valueOf(ResponseDecoder.AT_VALUE));
					}
					if(rpackage.selectSingleNode("*[local-name()='Description'][1]/*[local-name()='LocalizedString'][1]") != null){
						metadata.setDescription(rpackage.selectSingleNode("*[local-name()='Description'][1]/*[local-name()='LocalizedString'][1]").valueOf(ResponseDecoder.AT_VALUE));
					}
					if (rpackage.selectSingleNode("*[local-name()='VersionInfo'][1]") != null){
						metadata.setVersion(rpackage.selectSingleNode("*[local-name()='VersionInfo'][1]").valueOf("@versionName"));
					}
					
					// slots
					List<?> slots = rpackage.selectNodes("*[local-name()='Slot']");
					if (slots != null && !slots.isEmpty())
					{
						for (Object slotObject : slots)
						{
							if (slotObject instanceof Node)
							{
								Node slot = (Node) slotObject;
								if (slot.valueOf(ResponseDecoder.AT_NAME).equals("submissionTime"))
								{
									metadata.setDate(slot.selectSingleNode("*[local-name()='ValueList'][1]/*[local-name()='Value'][1]").getText());
									continue;
								}
								// other slots ???
							}
						}
					}
					
					Map<String, String> newParametersMap = new HashMap<String, String>();
					// classifications
					List<?> classifications = rpackage.selectNodes("*[local-name()='Classification']");
					if (classifications != null && !classifications.isEmpty())
					{
						for (Object classificationObject : classifications)
						{
							if (classificationObject instanceof Node)
							{
								Node classification = (Node) classificationObject;
								if (classification.selectSingleNode("*[local-name()='Name'][1]/*[local-name()='LocalizedString'][1]") != null){
									newParametersMap.put(classification.valueOf("@classificationScheme"), classification.selectSingleNode("*[local-name()='Name'][1]/*[local-name()='LocalizedString'][1]").valueOf(ResponseDecoder.AT_VALUE));
								}
								else{
									continue;
								}
							}
							else{
								continue;
							}
						}
					}
					// externalIdentifiers
					List<?> externalIdentifiers = rpackage.selectNodes("*[local-name()='ExternalIdentifier']");
					if (externalIdentifiers != null && !externalIdentifiers.isEmpty())
					{
						for (Object externalIdObject : externalIdentifiers)
						{
							if (externalIdObject instanceof Node)
							{
								Node externalIdentifier = (Node) externalIdObject;
								newParametersMap.put(externalIdentifier.valueOf("@identificationScheme"), externalIdentifier.valueOf(ResponseDecoder.AT_VALUE));
							}
							else{
								continue;
							}
						}
					}
					
					metadata.setMetadata(newParametersMap);
					documentInformations.add(metadata);
				}
				else{
					continue;
				}
			}
		}
		else{
			documentInformations = null;
		}
	}
	
	/**
	 * 
	 */
	private void extractDocumentEntryMetadata()
	{
		List<?> extrinsicObjects = response.selectNodes("*[local-name()='RegistryObjectList'][1]/*[local-name()='ExtrinsicObject']");
		if (extrinsicObjects != null && !extrinsicObjects.isEmpty())
		{
			documentInformations = new ArrayList<DocumentMetadata>();
			for (Object object : extrinsicObjects)
			{
				if (object instanceof Node)
				{
					Node extrinsicObject = (Node) object;
					DocumentMetadata metadata = new DocumentMetadata();
					metadata.setStatus(extrinsicObject.valueOf(ResponseDecoder.AT_STATUS));
					metadata.setMimeType(extrinsicObject.valueOf("@mimeType"));
					metadata.setHomeCommunityId(extrinsicObject.valueOf("@home"));
					// registryPackage attributes
					if (extrinsicObject.selectSingleNode("*[local-name()='Name'][1]/*[local-name()='LocalizedString'][1]") != null){
						metadata.setName(extrinsicObject.selectSingleNode("*[local-name()='Name'][1]/*[local-name()='LocalizedString'][1]").valueOf(ResponseDecoder.AT_VALUE));
					}
					if(extrinsicObject.selectSingleNode("*[local-name()='Description'][1]/*[local-name()='LocalizedString'][1]") != null){
						metadata.setDescription(extrinsicObject.selectSingleNode("*[local-name()='Description'][1]/*[local-name()='LocalizedString'][1]").valueOf(ResponseDecoder.AT_VALUE));
					}
					if (extrinsicObject.selectSingleNode("*[local-name()='VersionInfo'][1]") != null){
						metadata.setVersion(extrinsicObject.selectSingleNode("*[local-name()='VersionInfo'][1]").valueOf("@versionName"));
					}
					
					// slots
					List<?> slots = extrinsicObject.selectNodes("*[local-name()='Slot']");
					if (slots != null && !slots.isEmpty())
					{
						for (Object slotObject : slots)
						{
							if (slotObject instanceof Node)
							{
								Node slot = (Node) slotObject;
								if (slot.valueOf(ResponseDecoder.AT_NAME).equals("creationTime"))
								{
									metadata.setDate(slot.selectSingleNode("*[local-name()='ValueList'][1]/*[local-name()='Value'][1]").getText());
									continue;
								}
								else if (slot.valueOf(ResponseDecoder.AT_NAME).equals("repositoryUniqueId"))
								{
									metadata.setRepositoryUniqueId(slot.selectSingleNode("*[local-name()='ValueList'][1]/*[local-name()='Value'][1]").getText());
									continue;
								}
								else if (slot.valueOf(ResponseDecoder.AT_NAME).equals("sourcePatientId"))
								{
									metadata.setPatientId(slot.selectSingleNode("*[local-name()='ValueList'][1]/*[local-name()='Value'][1]").getText());
									continue;
								}
								else if (slot.valueOf(ResponseDecoder.AT_NAME).equals("URI"))
								{
									metadata.setURI(slot.selectSingleNode("*[local-name()='ValueList'][1]/*[local-name()='Value'][1]").getText());
									continue;
								}
								else{
									continue;
								}
								// other slots ???
							}
						}
					}
					
					Map<String, String> newParametersMap = new HashMap<String, String>();
					// classifications
					List<?> classifications = extrinsicObject.selectNodes("*[local-name()='Classification']");
					if (classifications != null && !classifications.isEmpty())
					{
						for (Object classificationObject : classifications)
						{
							if (classificationObject instanceof Node)
							{
								Node classification = (Node) classificationObject;
								Node nameNode = classification.selectSingleNode("*[local-name()='Name'][1]") ;
								String value = "unspecified" ;
								if (nameNode != null && nameNode.hasContent()) {
									Node localizedStringNode = classification.selectSingleNode("*[local-name()='Name'][1]/*[local-name()='LocalizedString'][1]") ;
									if (localizedStringNode != null && localizedStringNode.hasContent()){
										value = nameNode.valueOf(ResponseDecoder.AT_VALUE);
									}								
								}
								newParametersMap.put(classification.valueOf("@classificationScheme"), value);
							}
							else{
								continue;
							}
						}
					}
					// externalIdentifiers
					List<?> externalIdentifiers = extrinsicObject.selectNodes("*[local-name()='ExternalIdentifier']");
					if (externalIdentifiers != null && !externalIdentifiers.isEmpty())
					{
						for (Object externalIdObject : externalIdentifiers)
						{
							if (externalIdObject instanceof Node)
							{
								Node externalIdentifier = (Node) externalIdObject;
								newParametersMap.put(externalIdentifier.valueOf("@identificationScheme"), externalIdentifier.valueOf(ResponseDecoder.AT_VALUE));
								if (externalIdentifier.selectSingleNode("*[local-name()='Name'][1]/*[local-name()='LocalizedString'][1]") != null 
										&& externalIdentifier.selectSingleNode("*[local-name()='Name'][1]/*[local-name()='LocalizedString'][1]").valueOf(ResponseDecoder.AT_VALUE).equals("XDSDocumentEntry.uniqueId"))
								{
									metadata.setDocumentUniqueId(externalIdentifier.valueOf(ResponseDecoder.AT_VALUE));
								}
							}
							else{
								continue;
							}
						}
					}
					
					metadata.setMetadata(newParametersMap);
					documentInformations.add(metadata);
				}
				else{
					continue;
				}
			}
		}
		else{
			documentInformations = null;
		}
	}
	
	/**
	 * 
	 */
	private void extractAssociations()
	{
		
	}
	
	/**
	 * 
	 * @return
	 */
	public String checkSuccess()
	{
		String registryResponse;
		if (response != null)
		{
			switch(decodingType)
			{
				case RETRIEVE_DOCUMENT:
					registryResponse = response.selectSingleNode("*[local-name()='RegistryResponse'][1]").valueOf(ResponseDecoder.AT_STATUS);
					break;
				default:
					registryResponse = response.valueOf(ResponseDecoder.AT_STATUS);
					break;
			}
			if (registryResponse != null && !registryResponse.isEmpty())
			{
				return registryResponse;
			}
			else{
				return null;
			}
		}
		else{
			return null;
		}
	}
	
	public enum ResponseType
	{
		REGISTRY_OBJECT_UUID,
		REGISTRY_PACKAGE,
		DOCUMENT_ENTRY,
		REGISTRY_ERROR,
		RETRIEVE_DOCUMENT;
		// ...
	}
}
