package net.ihe.gazelle.xdstar.conf.action;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.simulator.sut.action.AbstractSystemConfigurationManager;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.xdstar.common.model.AbstractMessageQuery;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationFiltering;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationType;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationTypeQuery;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationTypeType;
import net.ihe.gazelle.xdstar.conf.model.RepositoryConfiguration;
import net.ihe.gazelle.xdstar.conf.model.RepositoryConfigurationQuery;
import net.ihe.gazelle.xdstar.core.MessageSenderCommon;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Name("repositoryConfigurationManager")
@Scope(ScopeType.PAGE)
public class RepositoryConfigurationManager extends AbstractSystemConfigurationManager<RepositoryConfiguration> implements Serializable, UserAttributeCommon {

    private static final Logger log = LoggerFactory.getLogger(RepositoryConfigurationManager.class);
    private static final long serialVersionUID = 1L;


    @In(value="gumUserService")
    private UserService userService;

    @Override
    public void addConfiguration() {
        selectedSystemConfiguration = new RepositoryConfiguration();
        selectedSystemConfiguration.setIsPublic(true);
        editSelectedConfiguration();
    }

    @Override
    public void copySelectedConfiguration(RepositoryConfiguration inConfiguration) {
        selectedSystemConfiguration = new RepositoryConfiguration(inConfiguration);
        editSelectedConfiguration();
    }

    @Override
    public void deleteSelectedSystemConfiguration() {
        if (isSelectedConfigurationAlreadyUsed()) {
            this.deleteSelectedSystemConfiguration(RepositoryConfiguration.class);
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, "Unable to delete this configuration because it is already used !");
        }
    }

    public boolean isSelectedConfigurationAlreadyUsed() {
        RepositoryConfiguration repositoryConfiguration = getSelectedSystemConfiguration();
        if (repositoryConfiguration != null) {
            AbstractMessageQuery q = new AbstractMessageQuery();
            q.configuration().id().eq(repositoryConfiguration.getId());
            return q.getCount() == 0;
        }
        return false;
    }

    @Override
    public List<Usage> getPossibleListUsages() {
        ConfigurationTypeQuery qq = new ConfigurationTypeQuery();
        qq.confTypeType().eq(ConfigurationTypeType.REPOSITORY_CONF);
        ConfigurationType conf = qq.getUniqueResult();
        if (conf != null) {
            return conf.getListUsages();
        }
        return null;
    }

    public void checkSelectedConfiguration(SystemConfiguration sysconf) throws Exception {
        (new MessageSenderCommon()).checkSelectedConfiguration(sysconf);
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<RepositoryConfiguration> arg0, Map<String, Object> arg1) {
        // TODO Auto-generated method stub

    }

    @Override
    protected HQLCriterionsForFilter<RepositoryConfiguration> getHQLCriterionsForFilter() {
        return new ConfigurationFiltering<RepositoryConfiguration, RepositoryConfigurationQuery>(new RepositoryConfigurationQuery()).getCriterionList();
    }

    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }
}
