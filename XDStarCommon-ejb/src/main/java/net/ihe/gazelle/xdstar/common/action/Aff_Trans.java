package net.ihe.gazelle.xdstar.common.action;

public enum Aff_Trans{
	IHE_XDR_ITI41("IHE_XDR/ITI-41"), 
	IHE_XDW_XDSb_ITI41("IHE_XDW_XDSb/ITI-41"), IHE_XDW_XDSb_ITI18("IHE_XDW_XDSb/ITI-18"), IHE_XDW_XDSb_ITI43("IHE_XDW_XDSb/ITI-43"),
	IHE_XDSb_ITI41("IHE_XDS-b/ITI-41"), IHE_XDSb_ITI18("IHE_XDS-b/ITI-18"), IHE_XDSb_ITI43("IHE_XDS-b/ITI-43"), 
	epSOS_XDR_DispensationService("epSOS_XDR/DispensationService:initialize()"), epSOS_XDR_ConsentService("epSOS_XDR/ConsentService:put()"),
	;
	private Aff_Trans(String val) {
		this.value = val;
	}

	String value;

	static Aff_Trans getByValue(String value){
		for (Aff_Trans aa : Aff_Trans.values()) {
			if (aa.value.equals(value)) return aa;
		}
		return null;
	}
}