package net.ihe.gazelle.xdstar.validator;

import java.util.List;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.validation.MDAValidation;
import net.ihe.gazelle.validation.Notification;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RootValidatorByValidatorModule {
	
	

	private static Logger log = LoggerFactory.getLogger(RootValidatorByValidatorModule.class);
	
	private RootValidatorByValidatorModule() {}
	
	public static DetailedResult validate(String message, String type, List<ConstraintValidatorModule> listConstraintValidatorModule, String validatorName){
		DetailedResult res = new DetailedResult();
		validateToSchema(res, message);
		Object obj;
		try {
			obj = extractRootFromString(message, type);
			MetadataValidatorByValidatorModule.validate(res, obj, listConstraintValidatorModule);
		} catch (ValidatorException vexp) {
			if (vexp != null && vexp.getDiagnostic() != null){
				for (Notification notification : vexp.getDiagnostic()) {
					if (res.getMDAValidation() == null){
						res.setMDAValidation(new MDAValidation());
					}
					res.getMDAValidation().getWarningOrErrorOrNote().add(notification);
				}
			}
		}
		
		return res;
	}
	
	
	public static Object extractRootFromString(String rr, String type) throws ValidatorException{
		if (type.equals("AdhocQueryRequestType")){
			return MetadataExtractor.getAdhocQueryRequestTypeFromXml(rr);
		}
		else if (type.equals("AdhocQueryResponseType")){
			return MetadataExtractor.getAdhocQueryResponseTypeFromXml(rr);
		}
		else if (type == "SubmitObjectsRequestType"){
			return MetadataExtractor.getSubmitObjectsRequestTypeFromXml(rr);
		}
		else if (type.equals("ProvideAndRegisterDocumentSetRequestType")){
			return MetadataExtractor.getProvideAndRegisterDocumentSetRequestTypeFromXml(rr);
		}
		else if (type.equals("RegistryResponseType")){
			return MetadataExtractor.getRegistryResponseTypeFromXml(rr);
		}
		else if (type.equals("RetrieveDocumentSetRequestType")){
			return MetadataExtractor.getRetrieveDocumentSetRequestTypeFromXml(rr);
		}
		else if (type.equals("RetrieveDocumentSetResponseType")){
			return MetadataExtractor.getRetrieveDocumentSetResponseTypeFromXml(rr);
		}
		else if (type.equals("RemoveObjectsRequestType")){
			return MetadataExtractor.getRemoveObjectsRequestTypeFromXml(rr);
		}
		else if (type.equals("RetrieveImagingDocumentSetRequestType")){
			return MetadataExtractor.getRetrieveImagingDocumentSetRequestTypeFromXml(rr);
		}
		
		return null;
	}
	
	
	public static void validateToSchema(DetailedResult res, String sub){
		DocumentWellFormed dd = new DocumentWellFormed(); 
		dd = XMLValidation.isXMLWellFormed(sub);
		res.setDocumentWellFormed(dd);
		try{
			res.setDocumentValidXSD(XMLValidation.isXDSValid(sub));
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
