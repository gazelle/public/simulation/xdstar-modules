package net.ihe.gazelle.xdstar.filter;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.simulator.sut.model.UsageAttributes;
import net.ihe.gazelle.simulator.sut.model.UsageQuery;


public class UsageDataModel extends FilterDataModel<Usage> {

	private AffinityDomain affinityDomain;
	
	private Transaction transaction;
	
	public AffinityDomain getAffinityDomain() {
		return affinityDomain;
	}

	public void setAffinityDomain(AffinityDomain affinityDomain) {
		this.affinityDomain = affinityDomain;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	
	public UsageDataModel() {
		super(new Filter<Usage>(getCriterionList()));
	}
	
	private static HQLCriterionsForFilter<Usage> getCriterionList() {
		UsageQuery query = new UsageQuery();
		HQLCriterionsForFilter<Usage> result = query.getHQLCriterionsForFilter();
		addTestCriterions(result, query, true);
		return result;
	}
	
	public static void addTestCriterions(HQLCriterionsForFilter<Usage> result, 
			UsageAttributes<Usage> messAttributes,
			boolean onlyConnectathon) {
		result.addPath("affinity", messAttributes.affinity());
		result.addPath("transaction", messAttributes.transaction());
	}
	
	public void appendFiltersFields(HQLQueryBuilder<Usage> queryBuilder) {}
	

@Override
        protected Object getId(Usage t) {
        // TODO Auto-generated method stub
        return t.getId();
        }
}
