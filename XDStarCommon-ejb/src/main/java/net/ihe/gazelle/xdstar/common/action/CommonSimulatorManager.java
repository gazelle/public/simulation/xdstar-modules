package net.ihe.gazelle.xdstar.common.action;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.common.xua.SamlAssertionAttributes;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.xdstar.common.model.AbstractMessage;
import net.ihe.gazelle.xdstar.common.model.TransactionType;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespacePrefixMapper;
import org.jboss.seam.contexts.Contexts;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author abderrazek boufahja
 *
 * @param <T> subclass of SystemConfiguration
 * @param <X>subclass of AbstractMessage
 */
public abstract class CommonSimulatorManager<T extends SystemConfiguration, X extends AbstractMessage> {
	
	{
		String affinityDomain = null;
		String transaction = null;
		try{
			Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
			affinityDomain = params.get("affinityDomainKeyword");
			transaction = params.get("transactionKeyword");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		if (transaction != null){
			this.selectedTransaction = Transaction.GetTransactionByKeyword(transaction);
		}
		if (affinityDomain != null){
			this.selectedAffinityDomain = AffinityDomain.getAffinityDomainByKeyword(affinityDomain);
		}
		if (this.selectedAffinityDomain != null && this.selectedTransaction != null){
			this.initFromMenu = true;
		}
		else{
			this.initFromMenu = false;
		}
	}
	
	protected static final String HL7_PERMISSION_PRD_006 = "urn:oasis:names:tc:xspa:1.0:subject:hl7:permission:PRD-006";
	protected static final String HL7_PERMISSION_PRD_003 = "urn:oasis:names:tc:xspa:1.0:subject:hl7:permission:PRD-003";
	protected static final String HL7_PERMISSION_PRD_004 = "urn:oasis:names:tc:xspa:1.0:subject:hl7:permission:PRD-004";
	protected static final String HL7_PERMISSION_PRD_005 = "urn:oasis:names:tc:xspa:1.0:subject:hl7:permission:PRD-005";
	protected static final String HL7_PERMISSION_PRD_010 = "urn:oasis:names:tc:xspa:1.0:subject:hl7:permission:PRD-010";
	protected static final String HL7_PERMISSION_PRD_016 = "urn:oasis:names:tc:xspa:1.0:subject:hl7:permission:PRD-016";
	protected static final String HL7_PERMISSION_PRD_032 = "urn:oasis:names:tc:xspa:1.0:subject:hl7:permission:PRD-032";
	protected static final String HL7_PERMISSION_PRD_033 = "urn:oasis:names:tc:xspa:1.0:subject:hl7:permission:PRD-033";
	
	protected static final String HL7_PERMISSION_PPD_032 = "urn:oasis:names:tc:xspa:1.0:subject:hl7:permission:PPD-032";
	protected static final String HL7_PERMISSION_PPD_033 = "urn:oasis:names:tc:xspa:1.0:subject:hl7:permission:PPD-033";
	protected static final String HL7_PERMISSION_PPD_046 = "urn:oasis:names:tc:xspa:1.0:subject:hl7:permission:PPD-046";

	private static String SOURCE_OID = "source_root_oid";
	
	private static String HOME_COMM_OID = "home_root_oid";

	protected List<T> configurations;

	protected T selectedConfiguration;

	protected AffinityDomain selectedAffinityDomain;

	protected boolean useXUA;

	protected SamlAssertionAttributes attributes;

	protected String praticianID;

	protected boolean displayResultPanel;

	protected Transaction selectedTransaction;

	protected List<SelectItem> availableTransactions;

	protected X message;

	protected TransactionType selectedTransactionType = null;
	
	protected Boolean initFromMenu;

	public Boolean getInitFromMenu() {
		return initFromMenu;
	}

	public void setInitFromMenu(Boolean initFromMenu) {
		this.initFromMenu = initFromMenu;
	}

	public TransactionType getSelectedTransactiontype(String tt) {
		if (selectedTransactionType == null) {
			HQLQueryBuilder<TransactionType> hh = new HQLQueryBuilder<TransactionType>(TransactionType.class);
			hh.addEq("name", tt);
			List<TransactionType> ll = hh.getList();
			if (ll.size() > 0)
				selectedTransactionType = ll.get(0);
		}
		return selectedTransactionType;
	}

	public List<T> getConfigurations() {
		return configurations;
	}

	public void setConfigurations(List<T> configurations) {
		this.configurations = configurations;
	}

	public T getSelectedConfiguration() {
		return selectedConfiguration;
	}

	public void setSelectedConfiguration(T selectedConfiguration) {
		this.selectedConfiguration = selectedConfiguration;
		try{
			Contexts.getSessionContext().set("selectedConfiguration", this.selectedConfiguration);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public AffinityDomain getSelectedAffinityDomain() {
		return selectedAffinityDomain;
	}

	public void setSelectedAffinityDomain(AffinityDomain selectedAffinityDomain) {
		this.selectedAffinityDomain = selectedAffinityDomain;
	}

	public Transaction getSelectedTransaction() {
		return selectedTransaction;
	}

	public void setSelectedTransaction(Transaction selectedTransaction) {
		this.selectedTransaction = selectedTransaction;
	}

	public boolean getUseXUA() {
		return useXUA;
	}

	public void setUseXUA(boolean useXUA) {
		this.useXUA = useXUA;
	}

	public SamlAssertionAttributes getAttributes() {
		return attributes;
	}

	public void setAttributes(SamlAssertionAttributes attributes) {
		this.attributes = attributes;
	}

	public String getPraticianID() {
		return praticianID;
	}

	public void setPraticianID(String praticianID) {
		this.praticianID = praticianID;
	}

	public void setDisplayResultPanel(boolean displayResultPanel) {
		this.displayResultPanel = displayResultPanel;
	}

	public boolean isDisplayResultPanel() {
		return displayResultPanel;
	}

	public List<SelectItem> getAvailableTransactions() {
		return availableTransactions;
	}

	public void setAvailableTransactions(List<SelectItem> availableTransactions) {
		this.availableTransactions = availableTransactions;
	}

	public X getMessage() {
		return message;
	}

	public void setMessage(X message) {
		this.message = message;
	}

	public void init() {
		initSamlAttributes();
		useXUA = false;
		selectedConfiguration = null;
		displayResultPanel = false;
		message = null;
	}
	
	public void initSamlAttributes(){
		attributes = new SamlAssertionAttributes(false);
		attributes.getXSPAPermissionsHL7().add(CommonSimulatorManager.HL7_PERMISSION_PRD_006);
		attributes.getXSPAPermissionsHL7().add(CommonSimulatorManager.HL7_PERMISSION_PRD_003);
		attributes.getXSPAPermissionsHL7().add(CommonSimulatorManager.HL7_PERMISSION_PRD_004);
		attributes.getXSPAPermissionsHL7().add(CommonSimulatorManager.HL7_PERMISSION_PRD_005);
		attributes.getXSPAPermissionsHL7().add(CommonSimulatorManager.HL7_PERMISSION_PRD_010);
		attributes.getXSPAPermissionsHL7().add(CommonSimulatorManager.HL7_PERMISSION_PRD_016);
		
		attributes.getXSPAPermissionsHL7().add(CommonSimulatorManager.HL7_PERMISSION_PPD_032);
		attributes.getXSPAPermissionsHL7().add(CommonSimulatorManager.HL7_PERMISSION_PPD_046);
	}

	public void reset() {
		init();
		if (!this.initFromMenu){
			selectedTransaction = null;
			selectedAffinityDomain = null;
		}
	}

	public List<AffinityDomain> listAllAffinityDomains(String tt) {
		List<AffinityDomain> res = new ArrayList<AffinityDomain>();
		if ((getSelectedTransactiontype(tt) != null)
				&& (getSelectedTransactiontype(tt).getListTransactions() != null)) {
			List<AffinityDomain> ll = AffinityDomain.listAllAffinityDomains();
			for (AffinityDomain affinityDomain : ll) {
				for (Transaction ss : affinityDomain.getSupportedTransactions()) {
					if (getSelectedTransactiontype(tt).getListTransactions()
							.contains(ss)) {
						if (!res.contains(affinityDomain))
							res.add(affinityDomain);
						continue;
					}
				}
			}
		}
		if ((res.size() == 1) && (this.selectedAffinityDomain == null)){
			this.selectedAffinityDomain = res.get(0);
			this.getTransactionsForDomain();
		}
		return res;
	}

	public void getTransactionsForDomain() {
		if (selectedAffinityDomain != null && selectedAffinityDomain.getSupportedTransactions() != null) {
			List<Transaction> ll = new ArrayList<Transaction>();
			for (Transaction transaction : selectedAffinityDomain
					.getSupportedTransactions()) {
				if ((selectedTransactionType != null)
						&& (selectedTransactionType.getListTransactions() != null)) {
					if (selectedTransactionType.getListTransactions().contains(
							transaction)) {
						ll.add(transaction);
					}
				}
			}
			availableTransactions = null;
			availableTransactions = new ArrayList<SelectItem>();
			for (Transaction t : ll) {
				availableTransactions
						.add(new SelectItem(t, t.getKeyword()));
			}
			if (!ll.isEmpty() && (ll.size() == 1)) {
				selectedTransaction = ll.get(0);
				this.updateSpecificClient();
			} else {
				selectedTransaction = null;
			}
		}

	}
	
	public boolean listContaintMoreThanOneElement(List<?> l){
		if ((l != null) && (l.size()>1)){
			return true;
		}
		return false;
	}
	
	public static String getSourceOid()
	{
		return ApplicationConfiguration.getValueOfVariable(SOURCE_OID);
	}
	
	public static String gethomeCommunityId()
	{
		return ApplicationConfiguration.getValueOfVariable(HOME_COMM_OID);
	}
	
//	public void initSimulatorForDomainAndTransaction(String adKeyword, String transKeyword){
//		HQLQueryBuilder<AffinityDomain> hh = new HQLQueryBuilder<AffinityDomain>(AffinityDomain.class);
//		hh.addEq("keyword", adKeyword);
//		this.selectedAffinityDomain = hh.getList().get(0);
//		this.selectedTransaction = Transaction.GetTransactionByKeyword(transKeyword);
//		this.initFromMenu = true;
//		Contexts.getSessionContext().set("initFromMenu", this.initFromMenu);
//		Contexts.getSessionContext().set("selectedAffinityDomain", this.selectedAffinityDomain);
//		Contexts.getSessionContext().set("selectedTransaction", this.selectedTransaction);
//	}
//	
	public static void saveXDS(OutputStream os, Object xdww) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.xds");
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new XDSNamespacePrefixMapper());
		m.marshal(xdww, os);
	}
	
	public String getNameOfTheAffinityDomainTransaction(){
		String res = null;
		HQLQueryBuilder<Usage> hh = new HQLQueryBuilder<Usage>(Usage.class);
		hh.addEq("affinity", this.selectedAffinityDomain);
		hh.addEq("transaction", this.selectedTransaction);
		List<Usage> ll = hh.getList();
		if (ll.size()>0) res = ll.get(0).getAction();
		return res;
	}
	
	public abstract void listAllConfigurations();
	
	public abstract void updateSpecificClient();
	
	public abstract List<AffinityDomain> listAllAffinityDomains();
	
	public abstract String getTheNameOfThePage();
	
}
