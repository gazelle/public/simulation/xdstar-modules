package net.ihe.gazelle.xdstar.validator;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.jboss.seam.annotations.Name;


@Entity
@Name("PackClass")
@Table(name="pack_class", schema = "public")
@SequenceGenerator(name = "pack_class_sequence", sequenceName = "pack_class_id_seq", allocationSize=1)
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("packClass")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PackClass", propOrder = {
	"packClass"
})
@XmlRootElement(name = "PackClass")

public class PackClass implements Comparable<PackClass>,Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XmlTransient
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="pack_class_sequence")
	private Integer id;
	
	private String packClass;

	public String getPackClass() {
		return packClass;
	}

	public void setPackClass(String packClass) {
		this.packClass = packClass;
	}
	
	public String getPackClassShort() {
		String[] splitString = packClass.split("\\.");
		int taille = splitString.length-1;
		if(taille > 0){
			return splitString[taille];
		}
		return packClass;
	}

	@Override
	public int compareTo(PackClass arg0) {
		if (this.getPackClassShort() == null) {
			return -1;
		}
		if (arg0 == null) {
			return 1;
		}
		if (arg0.getPackClassShort() == null) {
			return 1;
		}
		return this.getPackClassShort().compareTo(arg0.getPackClassShort());
	}
	
	
}
