package net.ihe.gazelle.xdstar.core;

/**
 * 
 * @author abderrazek boufahja
 * 
 */
public class MTOMElement implements Comparable<MTOMElement> {

	private byte[] content;

	private String uuid;

	private String transfertEncoding;

	private String contentType;

	private Integer index;

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public MTOMElement() {
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getTransfertEncoding() {
		return transfertEncoding;
	}

	public void setTransfertEncoding(String transfertEncoding) {
		this.transfertEncoding = transfertEncoding;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	@Override
	public int compareTo(MTOMElement arg0) {
		if (this.index == null) {
			if ((arg0 != null) && (arg0.index != null)) {
				return -1;
			} else {
				return 0;
			}
		} else {
			if ((arg0 == null) || (arg0.index == null)) {
				return 1;
			}
			return index.compareTo(arg0.index);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result)
				+ ((content == null) ? 0 : content.hashCode());
		result = (prime * result)
				+ ((contentType == null) ? 0 : contentType.hashCode());
		result = (prime * result) + ((index == null) ? 0 : index.hashCode());
		result = (prime * result)
				+ ((transfertEncoding == null) ? 0 : transfertEncoding
						.hashCode());
		result = (prime * result) + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MTOMElement other = (MTOMElement) obj;
		if (content == null) {
			if (other.content != null) {
				return false;
			}
		} else if (!content.equals(other.content)) {
			return false;
		}
		if (contentType == null) {
			if (other.contentType != null) {
				return false;
			}
		} else if (!contentType.equals(other.contentType)) {
			return false;
		}
		if (index == null) {
			if (other.index != null) {
				return false;
			}
		} else if (!index.equals(other.index)) {
			return false;
		}
		if (transfertEncoding == null) {
			if (other.transfertEncoding != null) {
				return false;
			}
		} else if (!transfertEncoding.equals(other.transfertEncoding)) {
			return false;
		}
		if (uuid == null) {
			if (other.uuid != null) {
				return false;
			}
		} else if (!uuid.equals(other.uuid)) {
			return false;
		}
		return true;
	}

}