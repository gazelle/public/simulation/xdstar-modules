package net.ihe.gazelle.xdstar.validator;

import gov.nist.registry.common2.validation.engine.MessageValidatorEngine;
import gov.nist.registry.common2.validation.engine.MessageValidatorEngine.ValidationStep;
import gov.nist.registry.common2.validation.factories.MessageValidatorFactory;
import gov.nist.registry.common2.validation.message.client.MessageValidationResults;
import gov.nist.registry.common2.validation.message.client.ValidatorErrorItem;
import gov.nist.registry.common2.validation.shared.ValidationContext;
import gov.nist.registry.xdstools2.client.MessageValidatorDisplay;
import gov.nist.registry.xdstools2.server.GwtErrorRecorder;
import gov.nist.registry.xdstools2.server.GwtErrorRecorderBuilder;
import gov.nist.registry.xdstools2.server.ValService.HtmlValFormatter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.ihe.gazelle.edit.FileReadWrite;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class NistValidator {
    private static final Logger LOG = LoggerFactory.getLogger(NistValidator.class);

    private NistValidator() {
    }

    public static String epSOSCodePath;

    public static String IHECodePath;

    public static String ebRIMXSDPath;

    static {
        try {
            epSOSCodePath = getNistCodeUrl("epSOSCodePath");
            IHECodePath = getNistCodeUrl("IHECodePath");
            ebRIMXSDPath = getNistCodeUrl("ebRIMXSDPath");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.setProperty("XDSSchemaDir", ebRIMXSDPath);
    }

    public static String validateDocumentToCodes(String doc, String codepath, ValidationContext vc) {

        try {
            if (codepath.equals(IHECodePath) && ApplicationConfiguration.getBooleanValue("avoid_nist_validation_ihe")) {
                return null;
            }
            if (codepath.equals(epSOSCodePath) && ApplicationConfiguration.getBooleanValue("avoid_nist_validation_epsos")) {
                return null;
            }
        } catch (Exception e) {
        }
        if (!httpLinkExists(codepath) || !httpLinkExists(ebRIMXSDPath)) {
            return null;
        }
        GwtErrorRecorderBuilder dd = new GwtErrorRecorderBuilder();
        MessageValidatorEngine mvc = null;

        if (vc == null) {
            vc = new ValidationContext();
        }
        try {
            byte[] docBytes = doc.getBytes(StandardCharsets.UTF_8);
            mvc = MessageValidatorFactory.getValidator(dd, docBytes, null, vc, null);
        } catch (Exception e) {
            LOG.error("" + e.getMessage());
        }
        if (mvc == null) {
            return "---";
        }
        mvc.run(codepath);
        for (int i = 0; i < mvc.getValidationStepCount(); i++) {
            ValidationStep vs = mvc.getValidationStep(i);
            GwtErrorRecorder ger = (GwtErrorRecorder) vs.getErrorRecorder();
            if (ger.hasErrors()) {
                for (ValidatorErrorItem vei : ger.getValidatorErrorInfo()) {
                    String fault = "";
                    fault = fault + vei.toString();
                }
            }
        }

        HtmlValFormatter hvf = new HtmlValFormatter();
        MessageValidatorDisplay mvd = new MessageValidatorDisplay(hvf);


        MessageValidationResults mvr = new MessageValidationResults();
        for (int step = 0; step < mvc.getValidationStepCount(); step++) {
            ValidationStep vs = mvc.getValidationStep(step);
            GwtErrorRecorder ger = (GwtErrorRecorder) vs.getErrorRecorder();
            List<ValidatorErrorItem> errs = ger.getValidatorErrorInfo();
            mvr.addResult(vs.getStepName(), errs);
        }
        mvr.addResult("Validation Summary", buildValidationSummary(vc, mvc));


        mvd.displayResults(mvr);
        String ress = hvf.toHtml();
        if (ress != null) {
            ress = ress.replace("<td>null</td>", "<td></td>");
        }
        return ress;
    }

    private static String getNistCodeUrl(String nistCode) {
        NistCodeQuery ncq = new NistCodeQuery();
        ncq.nistCodeName().eq(nistCode);
        return ncq.getUniqueResult().getUrl();
    }

    static List<ValidatorErrorItem> buildValidationSummary(ValidationContext vc, MessageValidatorEngine mvc) {
        List<ValidatorErrorItem> info = new ArrayList<ValidatorErrorItem>();

        ValidatorErrorItem vei = new ValidatorErrorItem();
        vei.msg = "Validation Context: " + vc.toString();
        vei.level = ValidatorErrorItem.ReportingLevel.DETAIL;
        info.add(vei);

        for (int i = 0; i < mvc.getValidationStepCount(); i++) {
            ValidationStep vs = mvc.getValidationStep(i);
            vei = new ValidatorErrorItem();
            vei.msg = vs.getStepName();
            vei.level = ValidatorErrorItem.ReportingLevel.DETAIL;
            info.add(vei);
        }
        return info;
    }

    static String validateToNistTool(String message, String val, Boolean isR, Boolean isPnr, Boolean isXc, Boolean isXdr, String metadataCodes) {
        String res = "";
        if (val == null) {
            return res;
        }

        ValidationContext vc = setValidationContext(isR, isPnr, isXc, isXdr);
        res = NistValidator.validateDocumentToCodes(message, metadataCodes, vc);

        return res;
    }

    private static ValidationContext setValidationContext(Boolean isR, Boolean isPnr, Boolean isXc, Boolean isXdr) {

        ValidationContext vc = new ValidationContext();
        vc.isR = isR;
        vc.isPnR = isPnr;
        vc.isXC = isXc;
        vc.isXDR = isXdr;
        return vc;
    }

    private static String updatePnRMessage(String message) {
        Pattern pat = Pattern.compile("(.*)ProvideAndRegisterDocumentSetRequest(.*)ProvideAndRegisterDocumentSetRequest(.*)", Pattern.DOTALL);
        Matcher m = pat.matcher(message);
        if (!m.find()) {
            message = "<ProvideAndRegisterDocumentSetRequest xmlns=\"urn:ihe:iti:xds-b:2007\">" + message + "</ProvideAndRegisterDocumentSetRequest>";
        }
        return message;
    }

    private static boolean httpLinkExists(String uRLName) {
        try {
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection con = (HttpURLConnection) new URL(uRLName).openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            return false;
        }
    }


    public static void main(String[] args) throws IOException {
        String doc = FileReadWrite.readDoc("/home/aboufahj/onr.xml");
        String res = updatePnRMessage(doc);
        System.out.println(res);
        boolean test = NistValidator.httpLinkExists("ftp://ftp.ihe.net");
        System.out.println(test);
        //NistValidator.validateDocumentToCodes("<query:AdhocQueryRequest xmlns=\"urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0\" xmlns:query=\"urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0\" xmlns:rs=\"urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><query:ResponseOption returnComposedObjects=\"true\" returnType=\"ObjectRef\"></query:ResponseOption><AdhocQuery home=\"\" id=\"urn:uuid:b909a503-523d-4517-8acf-8e5834dfc4c7\"><Slot name=\"$homeCommunityId\"><ValueList><Value>'sssss'</Value></ValueList></Slot></AdhocQuery></query:AdhocQueryRequest>\", \"http://jumbo.irisa.fr:9080/xdsref/codes/codes.xml", null, null);
    }

}
