package net.ihe.gazelle.xdstar.conf.action;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.simulator.sut.action.AbstractSystemConfigurationManager;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.xdstar.common.model.AbstractMessageQuery;
import net.ihe.gazelle.xdstar.conf.model.*;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Name("updateConfigurationManager")
@Scope(ScopeType.PAGE)
public class UpdateConfigurationManager extends AbstractSystemConfigurationManager<UpdateConfiguration> implements Serializable, UserAttributeCommon {

    private static final long serialVersionUID = 1L;

    @In(value="gumUserService")
    private UserService userService;

    @Override
    public void addConfiguration() {
        selectedSystemConfiguration = new UpdateConfiguration();
        selectedSystemConfiguration.setIsPublic(true);
        editSelectedConfiguration();
    }

    @Override
    public void copySelectedConfiguration(UpdateConfiguration inConfiguration) {
        this.selectedSystemConfiguration = new UpdateConfiguration(inConfiguration);
        editSelectedConfiguration();
    }

    @Override
    public void deleteSelectedSystemConfiguration() {
        if (isSelectedConfigurationAlreadyUsed()) {
            this.deleteSelectedSystemConfiguration(UpdateConfiguration.class);
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, "Unable to delete this configuration because it is already used !");
        }
    }

    public boolean isSelectedConfigurationAlreadyUsed() {
        UpdateConfiguration updateConfiguration = getSelectedSystemConfiguration();
        if (updateConfiguration != null) {
            AbstractMessageQuery q = new AbstractMessageQuery();
            q.configuration().id().eq(updateConfiguration.getId());
            return q.getCount() == 0;
        }
        return false;
    }

    @Override
    public List<Usage> getPossibleListUsages() {
        ConfigurationTypeQuery qq = new ConfigurationTypeQuery();
        qq.confTypeType().eq(ConfigurationTypeType.UPDATE_CONF);
        ConfigurationType conf = qq.getUniqueResult();
        if (conf != null) {
            return conf.getListUsages();
        }
        return null;
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<UpdateConfiguration> arg0, Map<String, Object> arg1) {

    }

    @Override
    protected HQLCriterionsForFilter<UpdateConfiguration> getHQLCriterionsForFilter() {
        return new ConfigurationFiltering<UpdateConfiguration, UpdateConfigurationQuery>(new UpdateConfigurationQuery()).getCriterionList();
    }

    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }
}
