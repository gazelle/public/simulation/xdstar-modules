package net.ihe.gazelle.xdstar.xua.common;

import net.ihe.gazelle.xdstar.core.XUAType;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.ArrayList;
import java.util.List;

@Name("xuaConfiguration")
@Scope(ScopeType.APPLICATION)
public class XUAConfiguration {

    private List<String> listXUAType = new ArrayList<>();

    public List<String> getListXUAType() {
        if (listXUAType.isEmpty()) {
            for (XUAType xuaType : XUAType.values()) {
                listXUAType.add(xuaType.getXuaType());
            }
        }
        return listXUAType;
    }
    /*public SelectItem[] getListXUAType() {
        final List<XUAType> list = Arrays.asList(XUAType.values());
        final List<SelectItem> result = new ArrayList<SelectItem>();
        for (XUAType type : list) {
            result.add(new SelectItem(type, type.getXuaType()));
        }
        return result.toArray(new SelectItem[result.size()]);
    }*/


}
