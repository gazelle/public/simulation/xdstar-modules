package net.ihe.gazelle.xdstar.validator.ws;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;

import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;

import net.ihe.gazelle.common.StringMatcher;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.xdstar.comon.util.SVSConsumerNew;
import net.ihe.gazelle.xdstar.validator.GenericMetadataValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public final class XDSMetadataValidator implements Serializable{



	private static Logger log = LoggerFactory.getLogger(XDSMetadataValidator.class);

	private XDSMetadataValidator() {}

	static{
		StringMatcher.setValueSetProvider(new SVSConsumerNew());
	}


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public static String validateDocument(String document, String validator) throws SOAPException {
		DetailedResult dr = GenericMetadataValidator.validate(document, validator);
		String res = getDetailedResultAsString(dr);
		if (res != null && res.contains("?>")){
			res = res.substring(res.indexOf("?>") + 2);
		}
		return res;
	}

	private static String getDetailedResultAsString(DetailedResult dr){
		if (dr != null){
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			try {
				DetailedResultTransformer.save(baos, dr);
			} catch (JAXBException e) {
				log.error("JAXBException error in the method getDetailedResultAsString. ", e);
			}
			return baos.toString();
		}
		return null;
	}



}


