package net.ihe.gazelle.xdstar.conf.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import net.ihe.gazelle.simulator.sut.model.Usage;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.jboss.seam.annotations.Name;

@Entity
@Name("configurationType")
@Table(name = "configuration_type", schema = "public")
@SequenceGenerator(name = "configuration_type_sequence", sequenceName = "configuration_type_id_seq", allocationSize = 1)
public class ConfigurationType implements Comparable<ConfigurationType>,Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@NotNull
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "configuration_type_sequence")
	protected Integer id;

	@Column(name = "conf_type")
	@Enumerated(EnumType.STRING)
	private ConfigurationTypeType confTypeType;

	@ManyToMany(fetch=FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	@JoinTable(name="conf_type_usages")
	private List<Usage> listUsages;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((confTypeType == null) ? 0 : confTypeType.hashCode());
		result = prime * result
				+ ((listUsages == null) ? 0 : listUsages.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConfigurationType other = (ConfigurationType) obj;
		if (confTypeType != other.confTypeType)
			return false;
		if (listUsages == null) {
			if (other.listUsages != null)
				return false;
		} else if (!listUsages.equals(other.listUsages))
			return false;
		return true;
	}

	public ConfigurationTypeType getConfTypeType() {
		return confTypeType;
	}

	public void setConfTypeType(ConfigurationTypeType confTypeType) {
		this.confTypeType = confTypeType;
	}

	public List<Usage> getListUsages() {
		return listUsages;
	}

	public void setListUsages(List<Usage> listUsages) {
		this.listUsages = listUsages;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public int compareTo(ConfigurationType o) {
		if (this.confTypeType == null) return -1;
		if (o == null) return 1;
		if (o.confTypeType == null) return 1;
		return this.confTypeType.getValue().compareTo(o.confTypeType.getValue());
		
	}

}
