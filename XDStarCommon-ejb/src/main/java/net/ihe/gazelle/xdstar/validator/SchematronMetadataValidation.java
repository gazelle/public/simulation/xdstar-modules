package net.ihe.gazelle.xdstar.validator;


/**
 * 
 * @author abderrazek boufahja
 *
 */

public class SchematronMetadataValidation{

	public static String validateEPSOS_IDENTIFICATION_SERVICE_REQ(String req){
		String res = null;
		res = SchematronValidator.getValidationResultSchematron("epSOS - Identification Service (request)", req);
		return res;
	}

	public static String validateEPSOS_IDENTIFICATION_SERVICE_RESP(String resp){
		String res = null;
		res = SchematronValidator.getValidationResultSchematron("epSOS - Identification Service (response)", resp);
		return res;
	}
	

	public static String validateIHE_XCPD_ITI_55_REQ(String req){
		String res = null;
		res = SchematronValidator.getValidationResultSchematron("IHE - XCPD ITI-55 (request)", req);
		return res;
	}

	public static String validateIHE_XCPD_ITI_55_RESP(String resp){
		String res = null;
		res = SchematronValidator.getValidationResultSchematron("IHE - XCPD ITI-55 (response)", resp);
		return res;
	}
	
}