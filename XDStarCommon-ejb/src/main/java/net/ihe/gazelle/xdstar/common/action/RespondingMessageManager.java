package net.ihe.gazelle.xdstar.common.action;


import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

import javax.ejb.Remove;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.xml.bind.JAXBException;

import net.ihe.gazelle.hql.paths.HQLSafePathEntity;
import net.ihe.gazelle.query.AdhocQueryResponseType;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.xdstar.common.model.AbstractMessage;
import net.ihe.gazelle.xdstar.common.model.RespondingMessage;
import net.ihe.gazelle.xdstar.filter.RepondingMessageDataModel;
import net.ihe.gazelle.xdstar.validator.ExternalValidationTools;
import net.ihe.gazelle.xdstar.validator.GenericMetadataValidator;
import net.ihe.gazelle.xdstar.validator.MetadataExtractor;
import net.ihe.gazelle.xdstar.validator.MetadataValidator;
import net.ihe.gazelle.xdstar.validator.ws.ExternalValidators;

import org.ajax4jsf.model.DataVisitResult;
import org.ajax4jsf.model.DataVisitor;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;
import org.jdom.JDOMException;


/**
 *  <b>Class Description :  </b>RespondingMessageManager<br><br>
 *  RespondingMessageManager
 *  
 * @class			RespondingMessageManager
 * @package			net.ihe.gazelle.xdstar.common.action
 * @author			Abderrazek Boufahja / INRIA Rennes IHE development Project
 *
 */
@Name("respondingMessageManager")
@Scope(ScopeType.PAGE)
public class RespondingMessageManager extends MessageManager<RespondingMessage> implements Serializable {
	
	private static final String NO_VALIDATOR_AVAILABLE_MSG = "No Validator is available for this type of document.";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Logger
	private static Log log;
	
	private transient RepondingMessageDataModel availableMessages;
	
	private Date startDate;
	
	private Date endDate;
	
	private RespondingMessage selectedRespondingMessage;
	
	private AdhocQueryResponseType selectedAdhocQueryResponseType;
	
	public AdhocQueryResponseType getSelectedAdhocQueryResponseType() {
		return selectedAdhocQueryResponseType;
	}

	public void setSelectedAdhocQueryResponseType(
			AdhocQueryResponseType selectedAdhocQueryResponseType) {
		this.selectedAdhocQueryResponseType = selectedAdhocQueryResponseType;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.availableMessages.setStartDate(startDate);
		this.startDate = startDate;
		this.applyFilter();
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.availableMessages.setEndDate(endDate);
		this.endDate = endDate;
		this.applyFilter();
	}

	public void setAvailableMessages(RepondingMessageDataModel availableMessages) {
		this.availableMessages = availableMessages;
	}

	public RepondingMessageDataModel getAvailableMessages() {
		if (availableMessages == null){
			try {
				HQLSafePathEntity.filterLabelMethodInit.remove(Transaction.class);
				HQLSafePathEntity.filterLabelMethodInit.put(Transaction.class, true);
				HQLSafePathEntity.filterLabelMethods.put(Transaction.class, Transaction.class.getMethod("getKeyword", null));
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
			availableMessages = new RepondingMessageDataModel();
		}
		return availableMessages;
	}
	
	private void applyFilter(){
        this.getAvailableMessages().resetCache();
        this.getAvailableMessages().getFilter().modified();
    }
	
	public void resetFilter(){
		this.setEndDate(null);
		this.setStartDate(null);
		this.getAvailableMessages().getFilter().clear();
		this.getAvailableMessages().resetCache();
	}
	
	public void listMessagesToDisplay() {
		RespondingMessage abs = (RespondingMessage)Component.getInstance("messagesToDisplay");
		if (abs != null){
			this.getAvailableMessages().setId(abs.getId());
		}
		selectedMessage = null;
		stringRequestContent = null;
		stringResponseContent = null;
	}
	
	public void initFirstMessage() throws IOException, JDOMException, JAXBException{
		try{
			if (availableMessages != null){
				availableMessages.walk(null, new DataVisitor() {
					
					@Override
					public DataVisitResult process(FacesContext arg0, Object arg1, Object arg2) {
						EntityManager em = (EntityManager)Component.getInstance("entityManager");
						Integer id = (Integer)arg1;
						selectedRespondingMessage = em.find(RespondingMessage.class, id);
						return DataVisitResult.CONTINUE;
					}
		        }, null, null);
			}
			if ((this.selectedRespondingMessage != null) && (this.selectedRespondingMessage.getReceivedMessageContent() != null) &&
					this.selectedRespondingMessage.getReceivedMessageContent().getMessageContent() != null){
				String mess = this.selectedRespondingMessage.getReceivedMessageContent().getMessageContent();
				String rr = MetadataValidator.extractNode(mess, "AdhocQueryResponse", "urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0");
				selectedAdhocQueryResponseType = MetadataExtractor.getAdhocQueryResponseTypeFromXml(rr);
				
			}
		}
		catch(Exception e){
			this.selectedAdhocQueryResponseType = null;
		}
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public void getMessageById()
	{
		this.getMessageById(RespondingMessage.class);
		
	}
	
	@Override
	protected void validatedSentMessage(RespondingMessage inMessage) {
		if ((inMessage.getSentMessageContent() != null) && (inMessage.getSentMessageContent().getMessageContent() != null)){
			String transKeyword = null;
			if (inMessage.getTransaction() != null){
				transKeyword = inMessage.getTransaction().getKeyword();
			}
			String affDomain = null;
			if (inMessage.getAffinityDomain() != null){
				affDomain = inMessage.getAffinityDomain().getKeyword();
			}
			ExternalValidators val = ExternalValidators.getExternalValidatorFromDescription(affDomain, transKeyword + "-" + inMessage.getMessageType(), 
					inMessage.getMessageType(), false);
			DetailedResult validationRequest = null;
			if (val != null) {
				validationRequest =  GenericMetadataValidator.validate(inMessage.getSentMessageContent().getMessageContent(), val.getValue());
			}
			else {
				validationRequest = this.validateMetadataMessage(inMessage.getSentMessageContent(), 
						transKeyword,
						affDomain, inMessage.getMessageType(), false);
			}
			
			if (validationRequest != null){
				if (validationRequest.getValidationResultsOverview().getValidationTestResult().equals("PASSED")){
					inMessage.getSentMessageContent().setValidationResult(true);
				} else{
					inMessage.getSentMessageContent().setValidationResult(false);
				}
				inMessage.getSentMessageContent().setValidationDetails(MetadataValidator.getDetailedResultAsString(validationRequest));
			}
			else {
				inMessage.getSentMessageContent().setValidationResult(true);
				inMessage.getSentMessageContent().setValidationDetails(ExternalValidationTools.createMessageWarning(NO_VALIDATOR_AVAILABLE_MSG));
			}
		}
	}
	
	protected void validateReceivedMessage(RespondingMessage inMessage) {
		if ((inMessage.getReceivedMessageContent() != null) && (inMessage.getReceivedMessageContent().getMessageContent() != null)){
			String transKeyword = null;
			if (inMessage.getTransaction() != null){
				transKeyword = inMessage.getTransaction().getKeyword();
			}
			String affDomain = null;
			if (inMessage.getAffinityDomain() != null){
				affDomain = inMessage.getAffinityDomain().getKeyword();
			}
			ExternalValidators val = ExternalValidators.getExternalValidatorFromDescription(affDomain, transKeyword + "-" + inMessage.getMessageType(), 
					inMessage.getMessageType(), false);
			DetailedResult validationRequest = null;
			if (val != null) {
				validationRequest =  GenericMetadataValidator.validate(inMessage.getSentMessageContent().getMessageContent(), val.getValue());
			}
			else {
				validationRequest = this.validateMetadataMessage(inMessage.getReceivedMessageContent(), 
					inMessage.getTransaction().getKeyword(),
					inMessage.getAffinityDomain().getKeyword(), inMessage.getMessageType(), true);
			}
			if (validationRequest != null){
				if (validationRequest.getValidationResultsOverview().getValidationTestResult().equals("PASSED")){
					inMessage.getReceivedMessageContent().setValidationResult(true);
				} else{
					inMessage.getReceivedMessageContent().setValidationResult(false);
				}
				inMessage.getReceivedMessageContent().setValidationDetails(MetadataValidator.getDetailedResultAsString(validationRequest));
			}
			else{
				inMessage.getReceivedMessageContent().setValidationResult(true);
				inMessage.getReceivedMessageContent().setValidationDetails(ExternalValidationTools.createMessageWarning(NO_VALIDATOR_AVAILABLE_MSG));
			}
		}
	}
	
	public String getFullPermanentLink(AbstractMessage message) {
		String baseUrl = ApplicationConfiguration.getValueOfVariable("application_url");
		return baseUrl + "/messages/messageResponder.seam?id=" + message.getId();
	}

	@Override
	public RespondingMessage getReloadedSelectedMessage() {
		EntityManager em = (EntityManager)Component.getInstance("entityManager");
		this.selectedMessage =  em.find(RespondingMessage.class, this.selectedMessage.getId());
		return this.selectedMessage;
	}
	
}
