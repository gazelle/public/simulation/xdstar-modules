package net.ihe.gazelle.xdstar.common.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.xdstar.validator.XSLTransformator;

import org.hibernate.annotations.Type;
import org.jboss.seam.annotations.Name;

@Entity
@Name("metadataMessage")
@Table(name = "metadata_message", schema = "public")
@SequenceGenerator(name = "metadata_message_sequence", sequenceName = "metadata_message_id_seq", allocationSize = 1)
public class MetadataMessage implements Serializable {

	protected static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "metadata_message_sequence", strategy = GenerationType.SEQUENCE)
	@NotNull
	@Column(name = "id")
	private Integer id;

	@Column(name = "message_type_id")
	private MetadataMessageType messageType;

	@Lob
	@Column(name = "message_content")
	@Basic(fetch = FetchType.LAZY)
	@Type(type = "text")
	private String messageContent;

	@Column(name = "validation_result")
	private Boolean validationResult;

	@Lob
	@Column(name = "validation_details")
	@Basic(fetch = FetchType.LAZY)
	@Type(type = "text")
	private String validationDetails;
	
	@Lob
	@Column(name = "http_header")
	@Type(type = "text")
	private String httpHeader;
	
	@OneToMany(mappedBy="metadataMessage", cascade={CascadeType.ALL})
	private List<AttachmentFile> listAttachments;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public MetadataMessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MetadataMessageType messageType) {
		this.messageType = messageType;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public Boolean getValidationResult() {
		return validationResult;
	}

	public void setValidationResult(Boolean validationResult) {
		this.validationResult = validationResult;
	}

	public String getValidationDetails() {
		return validationDetails;
	}

	public void setValidationDetails(String validationDetails) {
		this.validationDetails = validationDetails;
	}

	public String getHttpHeader() {
		return httpHeader;
	}

	public void setHttpHeader(String httpHeader) {
		this.httpHeader = httpHeader;
	}

	public List<AttachmentFile> getListAttachments() {
		return listAttachments;
	}

	public void setListAttachments(List<AttachmentFile> listAttachments) {
		this.listAttachments = listAttachments;
	}

	public MetadataMessage() {
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result)
				+ ((messageContent == null) ? 0 : messageContent.hashCode());
		result = (prime * result)
				+ ((messageType == null) ? 0 : messageType.hashCode());
		result = (prime * result)
				+ ((validationDetails == null) ? 0 : validationDetails
						.hashCode());
		result = (prime * result)
				+ ((validationResult == null) ? 0 : validationResult.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MetadataMessage other = (MetadataMessage) obj;
		if (messageContent == null) {
			if (other.messageContent != null) {
				return false;
			}
		} else if (!messageContent.equals(other.messageContent)) {
			return false;
		}
		if (messageType == null) {
			if (other.messageType != null) {
				return false;
			}
		} else if (!messageType.equals(other.messageType)) {
			return false;
		}
		if (validationDetails == null) {
			if (other.validationDetails != null) {
				return false;
			}
		} else if (!validationDetails.equals(other.validationDetails)) {
			return false;
		}
		if (validationResult == null) {
			if (other.validationResult != null) {
				return false;
			}
		} else if (!validationResult.equals(other.validationResult)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "MetadataMessage [id=" + id + ", messageType=" + messageType
				+ ", messageContent=" + messageContent + ", validationResult="
				+ validationResult + ", validationDetails=" + validationDetails
				+ "]";
	}

	public String getValidationDetailsAsHtml(String index) {
		if (this.validationDetails != null) {
			String inXslPath = ApplicationConfiguration
					.getValueOfVariable("validation_result_xslt");
			HashMap<String, String> hash = new HashMap<String, String>();
			hash.put("index", index);
			String res = XSLTransformator.resultTransformation(
					this.validationDetails, inXslPath, hash);
			return res;
		}
		return null;
	}

}
