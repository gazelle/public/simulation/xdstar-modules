package net.ihe.gazelle.xdstar.filter;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.xdstar.validator.ValidatorPackClass;
import net.ihe.gazelle.xdstar.validator.ValidatorPackClassAttributes;
import net.ihe.gazelle.xdstar.validator.ValidatorPackClassQuery;

/**
 * @author aboufahj
 */
public class ValidatorPackClassDataModel extends FilterDataModel<ValidatorPackClass> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private AffinityDomain affinityDomain;

	private Transaction transaction;

	public AffinityDomain getAffinityDomain() {
		return affinityDomain;
	}

	public void setAffinityDomain(AffinityDomain affinityDomain) {
		this.affinityDomain = affinityDomain;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}


	public ValidatorPackClassDataModel() {
		super(new Filter<ValidatorPackClass>(getCriterionList()));
	}

	private static HQLCriterionsForFilter<ValidatorPackClass> getCriterionList() {
		ValidatorPackClassQuery query = new ValidatorPackClassQuery();
		HQLCriterionsForFilter<ValidatorPackClass> result = query.getHQLCriterionsForFilter();
		addTestCriterions(result, query, true);
		return result;
	}

	public static void addTestCriterions(HQLCriterionsForFilter<ValidatorPackClass> result, 
			ValidatorPackClassAttributes<ValidatorPackClass> messAttributes,
			boolean onlyConnectathon) {
		result.addPath("affinity", messAttributes.usages().affinity());
		result.addPath("transaction", messAttributes.usages().transaction());
	}

	public void appendFiltersFields(HQLQueryBuilder<ValidatorPackClass> queryBuilder) {}


	@Override
	protected Object getId(ValidatorPackClass t) {
		// TODO Auto-generated method stub
		return t.getId();
	}
}
