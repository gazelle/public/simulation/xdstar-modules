package net.ihe.gazelle.xdstar.common.action;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.xdstar.common.model.RespondingMessage;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;

/**
 * @author abderrazek boufahja
 */
@Name("messageRespReportManager")
@Scope(ScopeType.PAGE)
public class MessageRespReportManager extends MessageManager<RespondingMessage>{
	
	private static final String NO_VALIDATOR_AVAILABLE_MSG = "No Validator is available for this type of document.";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void getMessageById() {
		this.getMessageById(RespondingMessage.class);
	}
	
	@Create
	public void initCreation(){
		this.getMessageById();
		this.setSelectedMessageTab("Request");
	}
	
	public String getFullPermanentLink(RespondingMessage message) {
		String baseUrl = ApplicationConfiguration.getValueOfVariable("application_url");
		return baseUrl + "/messages/messageResponder.seam?id=" + message.getId();
	}
	
	@Override
	protected void validatedSentMessage(RespondingMessage inMessage) {
		super.validateReceivedMessage(inMessage);
	}

	@Override
	protected void validateReceivedMessage(RespondingMessage inMessage) {
		super.validatedSentMessage(inMessage);
	}

	@Override
	public RespondingMessage getReloadedSelectedMessage() {
		EntityManager em = (EntityManager)Component.getInstance("entityManager");
		this.selectedMessage =  em.find(RespondingMessage.class, this.selectedMessage.getId());
		return this.selectedMessage;
	}

}
