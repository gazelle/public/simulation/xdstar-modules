/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.rds.common;

import java.io.IOException;
import java.io.Serializable;

import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.xds.DocumentRequestType;
import net.ihe.gazelle.xds.RetrieveDocumentSetRequestType;
import net.ihe.gazelle.xdstar.common.action.CommonSimulatorManager;
import net.ihe.gazelle.xdstar.common.model.AbstractMessage;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.log.Log;

/**
 *  
 * @author                	Abderrazek Boufahja
 *
 */

public abstract class CommonRetrieveManager<T extends SystemConfiguration, X extends AbstractMessage> extends CommonSimulatorManager<T,X> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Logger
	private static Log log;
	
	protected RetrieveDocumentSetRequestType selectedRetrieveDocumentSetRequest;
	
	protected DocumentRequestType selectedDocumentRequest;
	
	public void setSelectedDocumentRequest(DocumentRequestType selectedDocumentRequest) {
		this.selectedDocumentRequest = selectedDocumentRequest;
	}

	public DocumentRequestType getSelectedDocumentRequest() {
		if (this.selectedDocumentRequest == null){
			this.selectedDocumentRequest = new DocumentRequestType();
		}
		return selectedDocumentRequest;
	}


	public RetrieveDocumentSetRequestType getSelectedRetrieveDocumentSetRequest() {
		return selectedRetrieveDocumentSetRequest;
	}

	public void setSelectedRetrieveDocumentSetRequest(
			RetrieveDocumentSetRequestType selectedRetrieveDocumentSetRequest) {
		this.selectedRetrieveDocumentSetRequest = selectedRetrieveDocumentSetRequest;
	}
	
	public void initSelectedRequest(){
		this.selectedRetrieveDocumentSetRequest = new RetrieveDocumentSetRequestType();
		this.selectedRetrieveDocumentSetRequest.getDocumentRequest().add(new DocumentRequestType());
		this.selectedDocumentRequest = this.selectedRetrieveDocumentSetRequest.getDocumentRequest().get(0);
	}
	
	public void deleteDocumentRequest(DocumentRequestType doc){
		if ((this.selectedRetrieveDocumentSetRequest != null) && (doc != null)){
			this.selectedRetrieveDocumentSetRequest.getDocumentRequest().remove(doc);
			if (this.selectedRetrieveDocumentSetRequest.getDocumentRequest().size()==0){
				this.initSelectedRequest();
			}
			if (this.selectedDocumentRequest == doc){
				this.selectedDocumentRequest = this.selectedRetrieveDocumentSetRequest.getDocumentRequest().get(0);
			}
		}
	}
	
	public abstract String previewMessage() throws IOException;
	
	public void addDocumentRequest(){
		if (this.selectedRetrieveDocumentSetRequest == null) this.selectedRetrieveDocumentSetRequest = new RetrieveDocumentSetRequestType();
		DocumentRequestType doc = new DocumentRequestType();
		this.selectedRetrieveDocumentSetRequest.getDocumentRequest().add(doc);
		this.selectedDocumentRequest = doc;
	}
	
}
