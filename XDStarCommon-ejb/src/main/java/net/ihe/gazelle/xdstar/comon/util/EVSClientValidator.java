package net.ihe.gazelle.xdstar.comon.util;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import net.ihe.gazelle.mb.validator.ModelBasedValidationWSServiceStub;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.validator.mb.ws.ValidateBase64Document;
import net.ihe.gazelle.validator.mb.ws.ValidateBase64DocumentE;
import net.ihe.gazelle.validator.mb.ws.ValidateBase64DocumentResponseE;
import net.ihe.gazelle.validator.mb.ws.ValidateDocument;
import net.ihe.gazelle.validator.mb.ws.ValidateDocumentE;
import net.ihe.gazelle.validator.mb.ws.ValidateDocumentResponseE;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.ByteArrayPartSource;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.PartSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EVSClientValidator {


	public static final String UTF_8 = "UTF-8";
	private static Logger log = LoggerFactory.getLogger(EVSClientValidator.class);
	
	public static void validate(byte[] bytes) {
		ExternalContext extCtx = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletResponse response = (HttpServletResponse) extCtx.getResponse();
		validate(bytes, response);
	}

	public static void validate(byte[] bytes, HttpServletResponse response) {
		String evspage = ApplicationConfiguration.getValueOfVariable("evs_client_url");
		// First send the file to EVSClient
		HttpClient client = new HttpClient();
		PostMethod filePost = new PostMethod(evspage + "/" + "upload");

		// use the type as file name
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
		String messageType = "comment" + sdf.format(new Date());

		PartSource partSource = new ByteArrayPartSource(messageType, bytes);
		Part[] parts = { new FilePart("message", partSource) };
		filePost.setRequestEntity(new MultipartRequestEntity(parts, filePost.getParams()));
		int status = -1;
		try {
			status = client.executeMethod(filePost);
		} catch (Exception e) {
			status = -1;
			log.error("", e);
		}
		if (status == (-1)) {
			log.error("error to post to EVSClient : " + evspage);
		}
		boolean fail = false;

		if (status == HttpStatus.SC_OK) {
			try {
				String key = filePost.getResponseBodyAsString();
				String encodedKey = URLEncoder.encode(key, UTF_8);
				String url = evspage + "/" + "validate.seam?file=" + encodedKey;
				response.sendRedirect(url);
			} catch (IOException e) {
				fail = true;
				log.error("", e);
			}
		} else {
			fail = true;
		}
		if (fail) {
			log.error("error to second post to EVSClient : " + evspage);
		}

	}

	
	public static String validate(String document, String validator, boolean isDocumentBase64, String endpoint)
	{
		String validationResult = null;
		ModelBasedValidationWSServiceStub stub = null;
		try{
			if (stub == null)
			{
				stub = new ModelBasedValidationWSServiceStub(endpoint);
			}
			
			if (isDocumentBase64)
			{
				
				ValidateBase64Document params = new ValidateBase64Document();
				params.setBase64Document(document);
				params.setValidator(validator);
				ValidateBase64DocumentE paramsE = new ValidateBase64DocumentE();
				paramsE.setValidateBase64Document(params);
				ValidateBase64DocumentResponseE responseE = stub.validateBase64Document(paramsE);
				validationResult = responseE.getValidateBase64DocumentResponse().getDetailedResult();
			}
			else
			{
				ValidateDocument params = new ValidateDocument();
				params.setDocument(document);
				params.setValidator(validator);
				ValidateDocumentE paramsE = new ValidateDocumentE();
				paramsE.setValidateDocument(params);
				ValidateDocumentResponseE responseE = stub.validateDocument(paramsE);
				validationResult = responseE.getValidateDocumentResponse().getDetailedResult();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return validationResult;
	}
}
