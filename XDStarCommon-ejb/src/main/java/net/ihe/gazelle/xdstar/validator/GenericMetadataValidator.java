package net.ihe.gazelle.xdstar.validator;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.xpath.XPathExpressionException;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.metadata.model.AdhocQueryMetadata;
import net.ihe.gazelle.metadata.model.ConstraintSpecification;
import net.ihe.gazelle.metadata.model.RegistryObjectListMetadata;
import net.ihe.gazelle.utils.ValidatorUtil;
import net.ihe.gazelle.utils.XpathUtils;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.MDAValidation;
import net.ihe.gazelle.validation.NistValidation;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.ValidationResultsOverview;
import net.ihe.gazelle.validator.AdhocQueryMetadataValidator;
import net.ihe.gazelle.validator.RegistryObjectListMetadataValidator;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespaceContext;
import net.ihe.gazelle.xdstar.validator.ws.ExternalValidators;

public class GenericMetadataValidator {

	private static final String UNKNOWN = "UNKNOWN";
	private static final String PASSED = "PASSED";
	private static final String FAILED = "FAILED";

	public static DetailedResult validate(String document,String validatorName) {

		ValidatorPackClass vpc = getValidatorPackClassFromValidatorName(validatorName);
		if(vpc != null){
			List<PackClass> listPackClass = vpc.getListPackClass();
			String namespace = vpc.getNamespaceEnum().getNamespace();
			String extractNode = vpc.getExtractNodeEnum().getExtractNode();
			String extractNodeType = vpc.getExtractNodeEnum().getExtractNodeType();
			List<AdhocQueryMetadata> adhocQueryMetadataList = vpc.getListRegistryObjectMetadata();
			RegistryObjectListMetadata registryObjectList = vpc.getRegistryObjectListMetadata();
			Boolean hasNistValidation = vpc.getHasNistValidation();
			Boolean isR = vpc.getIsR();
			Boolean isPnr = vpc.getIsPnR();
			Boolean isXc = vpc.getIsXc();
			Boolean isXdr = vpc.getIsXdr();
			String metadataCodesUrl = null;
			if (vpc.getMetadataCodes() != null){
				metadataCodesUrl = vpc.getMetadataCodes().getUrl();
			}
			String technicalFramework = vpc.getTechnicalFramework();

			String extractData = null;
			try {
				extractData = MetadataValidator.extractNode(document, extractNode, namespace);
			} catch (Exception e) {
				return MetadataResponseUtil.errorWhenExtracting(document, namespace + ":" + extractNode, e);
			}
			List<ConstraintValidatorModule> listConstraintValidatorModule = toConstraintValidatorModuleList(listPackClass);
			DetailedResult res = RootValidatorByValidatorModule.validate(extractData, extractNodeType, listConstraintValidatorModule, validatorName);
			validateByNistValidator(extractData, validatorName,hasNistValidation, isR, isPnr, isXc, isXdr,metadataCodesUrl, res);
			validateByMetadataModel(extractNode, extractData, validatorName, res, adhocQueryMetadataList, registryObjectList,technicalFramework, vpc);
			summarizeDetailedResult(res);

			return res;

		}else{
			ExternalValidators ext = ExternalValidators.getExternalValidatorByName(validatorName);
			if (ext != null) {
				return ExternalValidationTools.validateWithExternalValidatorDetailedResult(document, ext);
			}
			else {
				return MetadataResponseUtil.notAValidValidator(document, validatorName);
			}
		}

	}

	protected static void validateExtraConstraints(String document,String location, ValidatorPackClass vpc,List<Notification> diagnostic, String technicalFramework) {

		if (vpc.getExtraConstraintSpecifications() != null){
			for (ConstraintSpecification conspec : vpc.getExtraConstraintSpecifications()) {
				validateConstraintSpecification(document, location, conspec, diagnostic, technicalFramework);
			}
		}
	}

	protected static void validateConstraintSpecification(String document, String location, ConstraintSpecification conspec, List<Notification> diagnostic, String technicalFramework) {

		ValidatorUtil.handleXpath(conspec.getXpath(), location, conspec.getDescription(), conspec.getKind(), document, diagnostic, technicalFramework);
	}
	
	public static void summarizeDetailedResult(DetailedResult dr){
		if (dr != null){
			Date dd = new Date();
			DateFormat dateFormat = new SimpleDateFormat("yyyy, MM dd");
			DateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");
			dr.setValidationResultsOverview(new ValidationResultsOverview());
			dr.getValidationResultsOverview().setValidationDate(dateFormat.format(dd));
			dr.getValidationResultsOverview().setValidationTime(timeFormat.format(dd));
			dr.getValidationResultsOverview().setValidationServiceName("Gazelle XDSMetadata Validation");
			dr.getValidationResultsOverview().setValidationTestResult(PASSED);
			if ((dr.getDocumentValidXSD() != null) && (dr.getDocumentValidXSD().getResult() != null) && (dr.getDocumentValidXSD().getResult().equals(FAILED))){
				dr.getValidationResultsOverview().setValidationTestResult(FAILED);
			}
			if ((dr.getDocumentWellFormed() != null) && (dr.getDocumentWellFormed().getResult() !=null) && (dr.getDocumentWellFormed().getResult().equals(FAILED))){
				dr.getValidationResultsOverview().setValidationTestResult(FAILED);
			}
			if ((dr.getMDAValidation() != null) && (dr.getMDAValidation().getResult() != null) && (dr.getMDAValidation().getResult().equals(FAILED))){
				dr.getValidationResultsOverview().setValidationTestResult(FAILED);
			}
			if ((dr.getNistValidation() != null) && (dr.getNistValidation().getResult() != null) && 
					(dr.getNistValidation().getResult().contains("Summary: Errors were found"))){
				dr.getValidationResultsOverview().setValidationTestResult(FAILED);
			}
			dr.getValidationResultsOverview().setValidationServiceVersion(UNKNOWN);
		}
	}


	private static void validateByNistValidator(String extractData, String validatorName,Boolean hasNistValidation, Boolean isR, Boolean isPnr, Boolean isXc, Boolean isXdr, String metadataCodes, DetailedResult res){

		if(hasNistValidation != null && hasNistValidation){
			try{
				res.setNistValidation(new NistValidation());
				String nistRes = NistValidator.validateToNistTool(extractData, validatorName,isR,isPnr,isXc,isXdr,metadataCodes);
				res.getNistValidation().setResult(nistRes);
				res.getNistValidation().setNistVersion("xdstools2 svn revision : 3248");
			}
			catch(Exception e){
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				PrintStream ps = new PrintStream(baos);
				e.printStackTrace(ps);
				res.setNistValidation(new NistValidation());
				res.getNistValidation().setResult(baos.toString());
				res.getNistValidation().setNistVersion("xdstools2 svn revision : 3248");
			}
		}
	}


	/**
	 * 
	 * @param validationPackList : A list of the validation class which need to be instanciated.
	 * @return the list of ConstraintValidatorModule used to validate document by Module
	 */
	private static List<ConstraintValidatorModule> toConstraintValidatorModuleList(List<PackClass> validationPackList) {
		List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<ConstraintValidatorModule>();

		for (PackClass cl : validationPackList) {
			try {
				Class className = Class.forName(cl.getPackClass());
				listConstraintValidatorModule.add((ConstraintValidatorModule) className.newInstance());
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return listConstraintValidatorModule;	
	}

	/**
	 * Validate a document towards metadata model
	 * @param nodeNameExtract : Type of validation
	 * @param document : Document to be validated
	 * @param validatorName : Name of the validator
	 * @param res : Formated result of the validation
	 */
	private static void validateByMetadataModel(String nodeNameExtract, String document, String validatorName, DetailedResult res, List<AdhocQueryMetadata> aqmdlist, RegistryObjectListMetadata rol, String technicalFramework, ValidatorPackClass vpc) {

		List<Notification> diagnostic  = null;
		if(nodeNameExtract.equals(ExtractNodeAndTypeEnum.ADHOC_QUERY_REQUEST.getExtractNode())){
			diagnostic = validateAdhocQueryRequest(document, validatorName, aqmdlist,technicalFramework);
			validateExtraConstraints(document, "/AdhocQueryRequest", vpc, diagnostic, technicalFramework );
			addValidationByMetadata(diagnostic, res);	
		}else if(nodeNameExtract.equals(ExtractNodeAndTypeEnum.PROVIDE_AND_REGISTER_DOCUMENT_SET_STRING.getExtractNode())){
			diagnostic = validateDocumentMetadata(document, "/ProvideAndRegisterDocumentSetRequest/SubmitObjectsRequest/RegistryObjectList", rol, technicalFramework);
			validateExtraConstraints(document, "/ProvideAndRegisterDocumentSetRequest/SubmitObjectsRequest", vpc, diagnostic, technicalFramework );
			addValidationByMetadata(diagnostic, res);	
		}else if(nodeNameExtract.equals(ExtractNodeAndTypeEnum.SUBMIT_OBJECT_REQUEST_STRING.getExtractNode())){
			diagnostic = validateDocumentMetadata(document, "/SubmitObjectsRequest/RegistryObjectList", rol, technicalFramework);
			validateExtraConstraints(document, "/SubmitObjectsRequest", vpc, diagnostic, technicalFramework );
			addValidationByMetadata(diagnostic, res);	
		} else if(nodeNameExtract.equals(ExtractNodeAndTypeEnum.ADHOC_QUERY_RESPONSE.getExtractNode())) {
			diagnostic = validateDocumentMetadata(document, "/AdhocQueryResponse/RegistryObjectList",rol, technicalFramework);
			validateExtraConstraints(document, "/AdhocQueryResponse", vpc, diagnostic, technicalFramework);
			addValidationByMetadata(diagnostic, res);
		}

	}

	/***
	 * Validate Document Metadata
	 *
	 * @param extractData extract data for validation
	 * @param location the metadata location in the document
	 * @param rol the registryObjectList of the document
	 * @param technicalFramework the specification of the profile
	 * @return diagnostic as notification list
	 */
	private static List<Notification> validateDocumentMetadata(String extractData, String location, RegistryObjectListMetadata rol, String technicalFramework) {
		List<Notification> diagnostic = new ArrayList<Notification>();
		RegistryObjectListMetadataValidator.instance.validate(extractData, location, rol, diagnostic, technicalFramework);
		return diagnostic;
	}

	private static void addValidationByMetadata(List<Notification> diagnostic, DetailedResult res) {

		res.getMDAValidation().getWarningOrErrorOrNote().addAll(diagnostic);
		updateMDAValidation(res.getMDAValidation());

	}

	/**
	 * 
	 * @param extractData : ProvideAndRegister Query
	 * @param validatorName : Name of the validator
	 * @return
	 */

	private static List<Notification> validateAdhocQueryRequest(String extractData, String validatorName, List<AdhocQueryMetadata> aqmdlist, String technicalFramework) {
		List<Notification> diagnostic = new ArrayList<Notification>();
		String uuid;
		try {
			uuid = XpathUtils.evaluateByStringValue(extractData, "/query:AdhocQueryRequest/rim:AdhocQuery/@id", XDSNamespaceContext.getInstance());
			boolean listHasValidUuid = AdhocQueryMetadata.listHasValidUuid(aqmdlist, uuid);
			if(listHasValidUuid){
				AdhocQueryMetadata aqmd = AdhocQueryMetadata.searchAdhocQueryMetadataByUuid(aqmdlist, uuid);
				AdhocQueryMetadataValidator.instance.validate(extractData, "/AdhocQueryRequest/AdhocQuery", aqmd, diagnostic, technicalFramework);
			}
			validateConstraintQueryId(listHasValidUuid, uuid, "/", diagnostic, technicalFramework);

		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return diagnostic;
	}

	private static void validateConstraintQueryId(boolean isValidUuid, String uuid, String location, List<Notification> diagnostic, String technicalFramework) {

		diagnostic.add(ValidatorUtil.addNotification(location + "AdhocQueryRequest/", isValidUuid, "QueryID " + uuid + " must be one from the validator", "error",technicalFramework));

	}



	/**
	 * 
	 * @param validatorName : Name of the validator
	 * @return the validation data related to a validator
	 */

	public static ValidatorPackClass getValidatorPackClassFromValidatorName(String validatorName){
		HQLQueryBuilder<ValidatorPackClass> hh = new HQLQueryBuilder<ValidatorPackClass>(ValidatorPackClass.class);
		hh.addEq("validatorName", validatorName);
		return hh.getUniqueResult();	
	}
	
	private static void updateMDAValidation(MDAValidation mda){
		mda.setResult("PASSED");
		List<Object> listResultMda = mda.getWarningOrErrorOrNote();
		for (Object notification : listResultMda) {
			if (notification instanceof net.ihe.gazelle.validation.Error){
				mda.setResult("FAILED");
			}
		}
	}

}
