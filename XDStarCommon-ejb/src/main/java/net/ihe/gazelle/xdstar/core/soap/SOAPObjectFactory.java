package net.ihe.gazelle.xdstar.core.soap;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;

public class SOAPObjectFactory {

    private static SOAPFactory factory;

    static {
        try {
            factory = SOAPFactory.newInstance();
        } catch (SOAPException e) {
            factory = null;
        }
    }

    private SOAPObjectFactory() {
        // Empty
    }

    public static void addSoapHeader(SOAPMessage soapMessage, QName type, Element assertion, String text) {
        try {
            SOAPElement header = factory.createElement(type);
            if (assertion != null) {
                Node importedElement = header.getOwnerDocument().importNode(assertion, true);
                header.addChildElement((SOAPElement) importedElement);
            }
            if (text != null) {
                header.addTextNode(text);
            }
            soapMessage.getSOAPHeader().addChildElement(header);
        } catch (SOAPException e) {
            throw new IllegalStateException(e);
        }
    }

    public static void addSoapHeader(SOAPMessage soapMessage, QName type, Element assertion) {
        addSoapHeader(soapMessage, type, assertion, null);
    }

    public static void addSoapHeader(SOAPMessage soapMessage, QName type, String text) {
        addSoapHeader(soapMessage, type, null, text);
    }

    public static SOAPElement createClaims() {
        try {
            SOAPElement claims = factory.createElement("Claims", "wst", SOAPConstants.WSTRUST_NS);
            claims.addAttribute(factory.createName("Dialect"), "http://bag.admin.ch/epr/2017/annex/5/amendment/2");
            return claims;
        } catch (SOAPException e) {
            throw new IllegalStateException(e);
        }
    }

    public static SOAPElement createAttribute(String name, String nameFormat) {
        try {
            SOAPElement attribute = factory.createElement(SOAPConstants.BODY_ATTRIBUTE_TYPE);
            attribute.addAttribute(factory.createName("Name"), name);
            if (nameFormat != null) {
                attribute.addAttribute(factory.createName("NameFormat"), nameFormat);
            }
            return attribute;
        } catch (SOAPException e) {
            throw new IllegalStateException(e);
        }
    }

    public static SOAPElement createAttribute(String name) {
        return createAttribute(name, null);
    }

    public static SOAPElement createAttributeValue(String type, String text) {
        try {
            SOAPElement attributeValue = factory.createElement(SOAPConstants.BODY_ATTRIBUTE_VALUE_TYPE);
            attributeValue.addNamespaceDeclaration("xs", SOAPConstants.XS_URI);
            attributeValue.addNamespaceDeclaration("xsi", SOAPConstants.XSI_URI);
            if (type != null) {
                attributeValue.addAttribute(SOAPConstants.ATTRIBUTE_TYPE, type);
            }
            if (text != null) {
                attributeValue.addTextNode(text);
            }
            return attributeValue;
        } catch (SOAPException e) {
            throw new IllegalStateException(e);
        }
    }

    public static SOAPElement createAttributeValue() {
        return createAttributeValue(null, null);
    }

    public static SOAPElement createAttributeValue(String type) {
        return createAttributeValue(type, null);
    }

    public static SOAPElement createClaimChild(String name, String code, String codeSystem, String codeSystemName, String displayName) {
        try {
            SOAPElement purposeOfUse = factory.createElement(name, "", SOAPConstants.HL7_NS);
            purposeOfUse.addAttribute(factory.createName("code"), code);
            purposeOfUse.addAttribute(factory.createName("codeSystem"), codeSystem);
            purposeOfUse.addAttribute(factory.createName("codeSystemName"), codeSystemName);
            purposeOfUse.addAttribute(factory.createName("displayName"), displayName);
            purposeOfUse.addAttribute(factory.createName("type", "xsi", SOAPConstants.XSI_URI), "CE");
            return purposeOfUse;
        } catch (SOAPException e) {
            throw new IllegalStateException(e);
        }
    }
}
