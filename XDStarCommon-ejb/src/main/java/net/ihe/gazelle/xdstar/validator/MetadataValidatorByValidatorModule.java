package net.ihe.gazelle.xdstar.validator;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.lcm.RemoveObjectsRequestType;
import net.ihe.gazelle.lcm.SubmitObjectsRequestType;
import net.ihe.gazelle.query.AdhocQueryRequestType;
import net.ihe.gazelle.query.AdhocQueryResponseType;
import net.ihe.gazelle.rs.RegistryResponseType;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.MDAValidation;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.xds.ProvideAndRegisterDocumentSetRequestType;
import net.ihe.gazelle.xds.RetrieveDocumentSetRequestType;
import net.ihe.gazelle.xds.RetrieveDocumentSetResponseType;
import net.ihe.gazelle.xdsi.RetrieveImagingDocumentSetRequestType;

public class MetadataValidatorByValidatorModule {

	static <E> void validate(DetailedResult res, E rr, List<ConstraintValidatorModule> listConstraintValidatorModule){
		List<Notification> ln = new ArrayList<Notification>();
		for (ConstraintValidatorModule cvm : listConstraintValidatorModule) {
			validateByModule(rr, cvm, ln);
		}
		if (res.getMDAValidation() == null) res.setMDAValidation(new MDAValidation());
		for (Notification notification : ln) {
			res.getMDAValidation().getWarningOrErrorOrNote().add(notification);
		}
		updateMDAValidation(res.getMDAValidation(), ln);
	}

	public static  <E> void validateByModule(E rr, ConstraintValidatorModule cvm, List<Notification> ln){
		if (rr instanceof AdhocQueryRequestType){
			AdhocQueryRequestType.validateByModule((AdhocQueryRequestType) rr, "/AdhocQueryRequest", cvm, ln);
		}
		else if (rr instanceof AdhocQueryResponseType){
			AdhocQueryResponseType.validateByModule((AdhocQueryResponseType) rr, "/AdhocQueryResponse", cvm, ln);
		}
		else if (rr instanceof SubmitObjectsRequestType){
			SubmitObjectsRequestType.validateByModule((SubmitObjectsRequestType)rr, "/SubmitObjectsRequest", cvm, ln);
		}
		else if (rr instanceof ProvideAndRegisterDocumentSetRequestType){
			ProvideAndRegisterDocumentSetRequestType.validateByModule((ProvideAndRegisterDocumentSetRequestType) rr, "/ProvideAndRegisterDocumentSetRequest", cvm, ln);
		}
		else if (rr instanceof RegistryResponseType){
			RegistryResponseType.validateByModule((RegistryResponseType) rr, "/RegistryResponse", cvm, ln);
		}
		else if (rr instanceof RetrieveDocumentSetRequestType){
			RetrieveDocumentSetRequestType.validateByModule((RetrieveDocumentSetRequestType) rr, "/RetrieveDocumentSetRequest", cvm, ln);
		}
		else if (rr instanceof RetrieveDocumentSetResponseType){
			RetrieveDocumentSetResponseType.validateByModule((RetrieveDocumentSetResponseType) rr, "/RetrieveDocumentSetResponse", cvm, ln);
		}
		else if (rr instanceof RemoveObjectsRequestType){
			RemoveObjectsRequestType.validateByModule((RemoveObjectsRequestType) rr, "/RemoveObjectsRequest", cvm, ln);
		}
		else if (rr instanceof RetrieveImagingDocumentSetRequestType){
			RetrieveImagingDocumentSetRequestType.validateByModule((RetrieveImagingDocumentSetRequestType) rr, "/RetrieveImagingDocumentSetRequest", cvm, ln);
		}
	}
	
	

	private static void updateMDAValidation(MDAValidation mda, List<Notification> ln){
		mda.setResult("PASSED");
		for (Notification notification : ln) {
			if (notification instanceof net.ihe.gazelle.validation.Error){
				mda.setResult("FAILED");
			}
		}
	}
	

}
