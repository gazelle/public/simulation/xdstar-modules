package net.ihe.gazelle.xdstar.validator;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.MDAValidation;
import net.ihe.gazelle.validation.Warning;

import org.apache.commons.lang.StringUtils;

public final class MetadataResponseUtil {
	
	private MetadataResponseUtil(){}
	
	public static DetailedResult errorWhenExtracting(String req, String extracted, Exception e){
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(baos);
		e.printStackTrace(ps);
		DetailedResult res = new DetailedResult();
		res.setDocumentWellFormed(XMLValidation.isXMLWellFormed(req));
		res.setMDAValidation(new MDAValidation());
		res.getMDAValidation().setResult("FAILED");
		net.ihe.gazelle.validation.Error err = new Error();
		err.setTest("structure_test");
		String desc = baos.toString();
		err.setLocation("all the document");
		err.setDescription("The validator is not able to extract the node element " + extracted + " from the validated document.\n" + 
				StringUtils.substring(desc, 0, 500).replace("\t", "--- ") + (desc.length()>500?"...":""));
		res.getMDAValidation().getWarningOrErrorOrNote().add(err);
		GenericMetadataValidator.summarizeDetailedResult(res);
		return res;
	}

	public static DetailedResult notAValidValidator(String req, String validatorName) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DetailedResult res = new DetailedResult();
		res.setDocumentWellFormed(XMLValidation.isXMLWellFormed(req));
		res.setMDAValidation(new MDAValidation());
		res.getMDAValidation().setResult("PASSED");
		net.ihe.gazelle.validation.Warning warn = new Warning();
		warn.setTest("structure_test");
		String desc = baos.toString();
		warn.setLocation("all the document");
		if (validatorName != null) {
			warn.setDescription("The validator " + validatorName + " is not recognised by the tool \n" + 
					StringUtils.substring(desc, 0, 500).replace("\t", "--- ") + (desc.length()>500?"...":""));
		}
		else {
			warn.setDescription("There is no known validator for such messages.");
		}
		res.getMDAValidation().getWarningOrErrorOrNote().add(warn);
		GenericMetadataValidator.summarizeDetailedResult(res);
		return res;
	}
	

}
