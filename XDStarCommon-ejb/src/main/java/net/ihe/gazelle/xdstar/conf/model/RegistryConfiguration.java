package net.ihe.gazelle.xdstar.conf.model;



import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;

import org.jboss.seam.annotations.Name;

@Entity
@Name("registryConfiguration")
@DiscriminatorValue("Registry")
public class RegistryConfiguration extends SystemConfiguration implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String repositoryUniqueId;
	
	private String homeCommunityId;
	
	public String getHomeCommunityId() {
		return homeCommunityId;
	}

	public void setHomeCommunityId(String homeCommunityId) {
		this.homeCommunityId = homeCommunityId;
	}

	public RegistryConfiguration(){}
	
	public RegistryConfiguration(RegistryConfiguration configuration) {
		this.name = configuration.getName().concat("_COPY");
		this.url= configuration.getUrl();
		this.systemName = configuration.getSystemName();
		this.repositoryUniqueId = configuration.getRepositoryUniqueId();
		this.homeCommunityId = configuration.getHomeCommunityId();
	}

	public String getRepositoryUniqueId() {
		return repositoryUniqueId;
	}

	public void setRepositoryUniqueId(String repositoryUniqueId) {
		this.repositoryUniqueId = repositoryUniqueId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((homeCommunityId == null) ? 0 : homeCommunityId.hashCode());
		result = prime
				* result
				+ ((repositoryUniqueId == null) ? 0 : repositoryUniqueId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegistryConfiguration other = (RegistryConfiguration) obj;
		if (homeCommunityId == null) {
			if (other.homeCommunityId != null)
				return false;
		} else if (!homeCommunityId.equals(other.homeCommunityId))
			return false;
		if (repositoryUniqueId == null) {
			if (other.repositoryUniqueId != null)
				return false;
		} else if (!repositoryUniqueId.equals(other.repositoryUniqueId))
			return false;
		return true;
	}
	
}
