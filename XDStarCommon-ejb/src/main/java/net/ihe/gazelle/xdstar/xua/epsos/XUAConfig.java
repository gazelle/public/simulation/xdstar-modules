package net.ihe.gazelle.xdstar.xua.epsos;

import net.ihe.gazelle.simulator.common.model.Concept;
import net.ihe.gazelle.xdstar.comon.util.SVSConsumerForSimulator;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.ArrayList;
import java.util.List;

@Name("xuaConfig")
@Scope(ScopeType.APPLICATION)
public class XUAConfig {

    private List<String> listXSPRoles;

    private List<String> listClinicalSpecialties;

    private List<String> listHealthcareFacilityType;

    private List<String> listPurposeOfUse;

    public List<String> getListXSPRoles() {
        if (listXSPRoles == null) {
            this.listXSPRoles = new ArrayList<String>();
            this.listXSPRoles.add("dentist");
            this.listXSPRoles.add("nurse");
            this.listXSPRoles.add("pharmacist");
            this.listXSPRoles.add("physician");
            this.listXSPRoles.add("nurse midwife");
            this.listXSPRoles.add("admission clerk");
            this.listXSPRoles.add("ancillary services");
            this.listXSPRoles.add("clinical services");
            this.listXSPRoles.add("medical doctor");
        }
        return listXSPRoles;
    }

    public List<String> getListClinicalSpecialties() {
        if (this.listClinicalSpecialties == null) {
            this.listClinicalSpecialties = new ArrayList<String>();
            List<Concept> lc = SVSConsumerForSimulator.getConceptsListFromValueSet("1.3.6.1.4.1.12559.11.4.1.10", null);
            if (lc != null) {
                for (Concept concept : lc) {
                    this.listClinicalSpecialties.add(concept.getDisplayName());
                }
            }
        }
        return listClinicalSpecialties;
    }

    public List<String> getListHealthcareFacilityType() {
        if (this.listHealthcareFacilityType == null) {
            this.listHealthcareFacilityType = new ArrayList<String>();
            this.listHealthcareFacilityType.add("Hospital");
            this.listHealthcareFacilityType.add("Resident Physician");
            this.listHealthcareFacilityType.add("Pharmacy");
            this.listHealthcareFacilityType.add("Other");
        }
        return listHealthcareFacilityType;
    }

    public List<String> getListPurposeOfUse() {
        if (this.listPurposeOfUse == null) {
            this.listPurposeOfUse = new ArrayList<String>();
            this.listPurposeOfUse.add("TREATMENT");
            this.listPurposeOfUse.add("EMERGENCY");
        }
        return listPurposeOfUse;
    }

}
