package net.ihe.gazelle.xdstar.common.model;


//@Entity
//@Name("contentType")
//@Table(name = "content_type", schema = "public")
public enum ContentType {
	SOAP_MESSAGE("soap message"),
	MTOM_XOP("mtom/xop"),
	HTTP("http");
	
	private ContentType(String value) {
		this.value = value;
	}
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
