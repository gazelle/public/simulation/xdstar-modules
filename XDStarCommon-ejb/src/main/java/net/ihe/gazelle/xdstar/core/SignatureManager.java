package net.ihe.gazelle.xdstar.core;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.xua.SignatureException;
import net.ihe.gazelle.simulator.common.xua.SignatureUtil;

public class SignatureManager {
	
	public static SignatureUtil getSignatureUtil() throws SignatureException {
		String keyStorePass = ApplicationConfiguration.getValueOfVariable("keystore_pass");
		String keyAlias = ApplicationConfiguration.getValueOfVariable("key_alias");
		String keyPass = ApplicationConfiguration.getValueOfVariable("key_pass");
		String simuKeystorePath = ApplicationConfiguration.getValueOfVariable("keystore_path");
		return new SignatureUtil(simuKeystorePath, keyStorePass, keyAlias, keyPass);
	}

}
