package net.ihe.gazelle.xdstar.filter;

import java.util.Date;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.xdstar.common.model.RespondingMessage;
import net.ihe.gazelle.xdstar.common.model.RespondingMessageAttributes;
import net.ihe.gazelle.xdstar.common.model.RespondingMessageQuery;

public class RepondingMessageDataModel extends FilterDataModel<RespondingMessage> {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String creator;

    private Date startDate;

    private AffinityDomain affinityDomain;

    private Transaction transaction;

    private String messageType;

    private Boolean validRequest;

    private Boolean validResponse;

    private Integer id;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AffinityDomain getAffinityDomain() {
        return affinityDomain;
    }

    public void setAffinityDomain(AffinityDomain affinityDomain) {
        this.affinityDomain = affinityDomain;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public Boolean getValidRequest() {
        return validRequest;
    }

    public void setValidRequest(Boolean validRequest) {
        this.validRequest = validRequest;
    }

    public Boolean getValidResponse() {
        return validResponse;
    }

    public void setValidResponse(Boolean validResponse) {
        this.validResponse = validResponse;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


    private Date endDate;

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public RepondingMessageDataModel() {
        super(new Filter<RespondingMessage>(getCriterionList()));
    }

    private static HQLCriterionsForFilter<RespondingMessage> getCriterionList() {
        RespondingMessageQuery query = new RespondingMessageQuery();
        HQLCriterionsForFilter<RespondingMessage> result = query.getHQLCriterionsForFilter();
        addTestCriterions(result, query, true);
        return result;
    }

    public static void addTestCriterions(HQLCriterionsForFilter<RespondingMessage> result,
                                         RespondingMessageAttributes<RespondingMessage> messAttributes,
                                         boolean onlyConnectathon) {
        result.addPath("affinityDomain", messAttributes.affinityDomain());
        result.addPath("transaction", messAttributes.transaction());
        result.addPath("messageType", messAttributes.messageType());
        result.addPath("validRequest", messAttributes.sentMessageContent().validationResult());
        result.addPath("validResponse", messAttributes.receivedMessageContent().validationResult());
        result.addPath("ip", messAttributes.ip());
        result.addPath("date", messAttributes.timeStamp());
    }

    public void appendFiltersFields(HQLQueryBuilder<RespondingMessage> queryBuilder) {
        if (startDate != null) {
            queryBuilder.addRestriction(HQLRestrictions.ge("timeStamp", startDate));
        }

        if (endDate != null) {
            queryBuilder.addRestriction(HQLRestrictions.le("timeStamp", endDate));
        }

        if (id != null) {
            queryBuilder.addRestriction(HQLRestrictions.eq("id", id));
        }

    }


    @Override
    protected Object getId(RespondingMessage t) {
        // TODO Auto-generated method stub
        return t.getId();
    }
}
