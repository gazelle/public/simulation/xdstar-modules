package net.ihe.gazelle.xdstar.conf.model;



import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;

import org.jboss.seam.annotations.Name;

@Entity
@Name("repositoryConfiguration")
@DiscriminatorValue("Repository")
public class RepositoryConfiguration extends SystemConfiguration implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String repositoryUniqueId;
	
	public RepositoryConfiguration(){}
	
	public RepositoryConfiguration(RepositoryConfiguration configuration) {
		this.name = configuration.getName().concat("_COPY");
		this.url= configuration.getUrl();
		this.systemName = configuration.getSystemName();
		this.repositoryUniqueId = configuration.getRepositoryUniqueId();
	}

	public String getRepositoryUniqueId() {
		return repositoryUniqueId;
	}

	public void setRepositoryUniqueId(String repositoryUniqueId) {
		this.repositoryUniqueId = repositoryUniqueId;
	}

	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((repositoryUniqueId == null) ? 0 : repositoryUniqueId
						.hashCode());
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RepositoryConfiguration other = (RepositoryConfiguration) obj;
		if (repositoryUniqueId == null) {
			if (other.repositoryUniqueId != null)
				return false;
		} else if (!repositoryUniqueId.equals(other.repositoryUniqueId))
			return false;
		return true;
	}
	
}
