package net.ihe.gazelle.xdstar.validator;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import net.ihe.gazelle.edit.FileReadWrite;


public class XSLTransformator {
	
	private XSLTransformator(){}
	
	/**
	 * Takes a file and copies its content into a string which is returned
	 * @param inFile
	 * @return
	 */
	public static String getFileContentToString(File inFile)
	{
		if (inFile == null){
			return null;
		}
		
		FileReader fr;
		try {
			fr = new FileReader(inFile);
			StringBuffer stringToValidate = new StringBuffer();
			BufferedReader buff = new BufferedReader(fr);
			String line;
			while( (line = buff.readLine()) !=  null )
			{
				stringToValidate.append(line);
				stringToValidate.append("\n");
			}
			buff.close();
			fr.close();
			return stringToValidate.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}	
	}
	
	/**
	 * Creates a file from its path and calls the method which returns the string
	 * @param filePath
	 * @return
	 */
	public static String getFileContentToString(String filePath) {
		if (filePath == null || filePath.length() == 0){
			return null;
		}
		File file = new File(filePath);
		if(file.exists()){
			return getFileContentToString(file);
		}
		else{
			return null;
		}
	}
	
	public static String resultTransformation(String inDetailedResult, String inXslAbsolutePath, Map<String, String> map)
    {
        if (inDetailedResult == null ||inDetailedResult.length() == 0){
            return null;
        }
        
        if (inDetailedResult.indexOf("?>")>0) inDetailedResult = inDetailedResult.substring(inDetailedResult.indexOf("?>") + 2);
        
        if (inXslAbsolutePath == null || inXslAbsolutePath.length() == 0){
            return null;
        }
        
        try {
            TransformerFactory tFactory = TransformerFactory.newInstance();

            Transformer transformer = tFactory.newTransformer(
                    new javax.xml.transform.stream.StreamSource(inXslAbsolutePath));
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            if (map != null){
                List<String> listOfParameters =  new ArrayList<String>( map.keySet() ) ;
                for ( String param : listOfParameters )
                {
                    transformer.setParameter( param , map.get(param) ) ;
                }
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            StreamResult out = new StreamResult(baos);
            ByteArrayInputStream bais = new ByteArrayInputStream(inDetailedResult.getBytes(StandardCharsets.UTF_8));
            transformer.transform(new javax.xml.transform.stream.StreamSource(bais), out);
            return baos.toString() ;
        } catch (Exception e) {
            e.printStackTrace( );
            return "The document cannot be displayed using this stylesheet";
        }
    }
	
	public static void main(String[] args) throws IOException {
		String doc = FileReadWrite.readDoc("/Users/aboufahj/Downloads/Untitled1.xml");
		String ss = resultTransformation(doc, "http://gazelle.ihe.net/xsl/xdwDetailedResult.xsl", null);
		System.out.println("ss="+ss);
	}
	

}
