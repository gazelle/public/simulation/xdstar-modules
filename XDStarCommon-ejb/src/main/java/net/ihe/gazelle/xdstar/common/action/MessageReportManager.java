package net.ihe.gazelle.xdstar.common.action;

import javax.persistence.EntityManager;

import net.ihe.gazelle.xdstar.common.model.AbstractMessage;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("messageReportManager")
@Scope(ScopeType.PAGE)
public class MessageReportManager extends MessageManager<AbstractMessage>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void getMessageById() {
		this.getMessageById(AbstractMessage.class);
	}
	
	@Create
	public void initCreation(){
		this.getMessageById();
		this.setSelectedMessageTab("Request");
	}

	@Override
	public AbstractMessage getReloadedSelectedMessage() {
		EntityManager em = (EntityManager)Component.getInstance("entityManager");
		this.selectedMessage =  em.find(AbstractMessage.class, this.selectedMessage.getId());
		return this.selectedMessage;
	}

}
