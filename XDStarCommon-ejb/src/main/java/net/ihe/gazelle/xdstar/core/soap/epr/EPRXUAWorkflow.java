package net.ihe.gazelle.xdstar.core.soap.epr;

import net.ihe.gazelle.simulator.sts.client.STSRequestFactory;
import net.ihe.gazelle.wstrust.base.RequestTypeEnum;
import net.ihe.gazelle.xdstar.core.soap.SOAPConstants;
import net.ihe.gazelle.xdstar.core.soap.SOAPObjectFactory;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.ws.rs.core.HttpHeaders;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.*;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

/**
 * This class is used to apply the Swiss XUA assertion retrieving workflow
 * We first authenticate to the IDP configured in application preferences to obtain user information
 * Then we use the result to query the security token service to get the SAML assertion
 * And we add the received assertion to the SOAP header of any RAD-68, RAD-69 and RAD-75 request
 */
public class EPRXUAWorkflow {

    private static final Logger LOG = LoggerFactory.getLogger(EPRXUAWorkflow.class);

    private static final String IDP_USERNAME = "sbaader";
    private static final String IDP_PASSWORD = "azerty";
    private static final String EPR_SPID = "761337610411265304^^^SPID&2.16.756.5.30.1.127.3.10.3&ISO";

    private static final String PURPOSE_OF_USE_CODE = "DICOM_AUTO";
    private static final String ROLE_CODE = "TCU";
    private static final String TECHNICAL_USER_GLN = "7601002466565";
    private static final String TECHNICAL_USER_NAME = "Matthew Marston";

    private static SOAPConnectionFactory soapConnectionFactory;
    private static MessageFactory messageFactory;

    static {
        try {
            soapConnectionFactory = SOAPConnectionFactory.newInstance();
            messageFactory = MessageFactory.newInstance();
        } catch (SOAPException e) {
            soapConnectionFactory = null;
            messageFactory = null;
        }
    }

    private EPRXUAWorkflow() {
        // Empty
    }

    public static Element getSAMLAssertion(String idpEndpoint, String stsEndpoint) {
        try {
            Element authAssertion = idpAuthenticationRequest(idpEndpoint);
            return xuaRequest(stsEndpoint, authAssertion);
        } catch (ParserConfigurationException | SOAPException e) {
            return null;
        }
    }

    private static Element idpAuthenticationRequest(String idpEndpoint) throws ParserConfigurationException, SOAPException {
        return sendRequest(createIDPSoapMessage(), idpEndpoint);
    }

    private static SOAPMessage createIDPSoapMessage() throws ParserConfigurationException, SOAPException {
        SOAPMessage soapMessage = messageFactory.createMessage();

        createIDPSoapBody(soapMessage);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader(HttpHeaders.AUTHORIZATION, getBasicAuthenticator());

        soapMessage.saveChanges();
        return soapMessage;
    }

    private static void createIDPSoapBody(SOAPMessage msg) throws ParserConfigurationException, SOAPException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document request = builder.newDocument();
        request.setXmlVersion("1.0");
        request.setXmlStandalone(true);

        String samlNS = SOAPConstants.PROTOCOL_NS;
        Element authnRequest = request.createElementNS(samlNS, "samlp:AuthnRequest");
        authnRequest.setAttribute("AssertionConsumerServiceURL", "https://sp-clone.ihe-europe.net/Shibboleth.sso/SAML2/ECP");
        authnRequest.setAttribute("ID", "_" + UUID.randomUUID());

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date now = new Date();
        String date = simpleDateFormat.format(now);

        authnRequest.setAttribute("IssueInstant", date);
        authnRequest.setAttribute("ProtocolBinding", SOAPConstants.PROTOCOL_BINDING_NS);
        authnRequest.setAttribute("Version", "2.0");

        Element issuer = request.createElementNS(SOAPConstants.ASSERTION_NS, "saml:Issuer");
        issuer.setTextContent("https://sp-clone.ihe-europe.net/shibboleth");
        Element nameIDPolicy = request.createElementNS(samlNS, "samlp:NameIDPolicy");
        nameIDPolicy.setAttribute("AllowCreate", "1");
        Element scoping = request.createElementNS(samlNS, "samlp:Scoping");

        Element idpList = request.createElementNS(samlNS, "samlp:IDPList");

        Element idpEntry = request.createElementNS(samlNS, "samlp:IDPEntry");
        idpEntry.setAttribute("ProviderID", "https://idp.ihe-europe.net/idp/shibboleth");
        idpList.appendChild(idpEntry);

        scoping.appendChild(idpList);

        authnRequest.appendChild(issuer);
        authnRequest.appendChild(nameIDPolicy);
        authnRequest.appendChild(scoping);

        request.appendChild(authnRequest);

        SOAPBody soapBody = msg.getSOAPBody();
        Document soapDocument = soapBody.getOwnerDocument();
        Node importedNode = soapDocument.importNode(request.getDocumentElement(), true);

        soapBody.appendChild(importedNode);
    }

    public static String getBasicAuthenticator() {
        String builder = IDP_USERNAME + ':' + IDP_PASSWORD;
        byte[] authenticator = builder.getBytes(StandardCharsets.UTF_8);
        byte[] b64Authenticator = Base64.encodeBase64(authenticator);
        return "Basic " + new String(b64Authenticator, StandardCharsets.UTF_8);
    }

    private static Element xuaRequest(String stsEndpoint, Element authAssertion) throws SOAPException, ParserConfigurationException {
        return sendRequest(createSTSSoapMessage(authAssertion), stsEndpoint);
    }

    private static SOAPMessage createSTSSoapMessage(Element authAssertion) throws SOAPException, ParserConfigurationException {
        SOAPMessage soapMessage = messageFactory.createMessage();

        createSTSSoapHeader(soapMessage, authAssertion);
        createSTSSoapBody(soapMessage);

        soapMessage.saveChanges();
        return soapMessage;
    }

    private static void createSTSSoapHeader(SOAPMessage soapMessage, Element authAssertion) {
        SOAPObjectFactory.addSoapHeader(soapMessage, SOAPConstants.HEADER_ACTION_TYPE, SOAPConstants.ISSUE_URI);
        SOAPObjectFactory.addSoapHeader(soapMessage, SOAPConstants.HEADER_MESSAGE_ID_TYPE, "urn:uuid:" + UUID.randomUUID());
        SOAPObjectFactory.addSoapHeader(soapMessage, SOAPConstants.HEADER_SECURITY_TYPE, authAssertion);
    }

    private static void createSTSSoapBody(SOAPMessage msg) throws SOAPException, ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document request = builder.newDocument();

        Element requestSecurityTokenType = STSRequestFactory.createRequest(RequestTypeEnum.ISSUE, "https://localhost:17001/services/iti18", null);
        Node importedRequest = request.importNode(requestSecurityTokenType, true);
        request.appendChild(importedRequest);

        SOAPElement claims = createAttributes();
        Node importedClaims = request.importNode(claims, true);
        importedRequest.appendChild(importedClaims);

        Node importedBody = msg.getSOAPBody().getOwnerDocument().importNode(importedRequest, true);
        msg.getSOAPBody().appendChild(importedBody);
    }

    private static SOAPElement createAttributes() throws SOAPException {
        SOAPElement claims = SOAPObjectFactory.createClaims();

        SOAPElement pidAttribute = SOAPObjectFactory.createAttribute(SOAPConstants.RESOURCE_ID_NS);
        SOAPElement pidAttributeValue = SOAPObjectFactory.createAttributeValue(SOAPConstants.XS_STRING, EPR_SPID);
        pidAttribute.addChildElement(pidAttributeValue);

        SOAPElement purposeOfUseAttribute = SOAPObjectFactory.createAttribute(SOAPConstants.PURPOSE_OF_USE_NS);
        SOAPElement purposeOfUseAttributeValue = SOAPObjectFactory.createAttributeValue(SOAPConstants.XS_ANY);
        purposeOfUseAttributeValue.addChildElement(SOAPObjectFactory.createClaimChild(SOAPConstants.PURPOSE_OF_USE, PURPOSE_OF_USE_CODE, "2.16.756.5.30.1.127.3.10.6", "eHealth Suisse Verwendungszwec", "Normalzugriff"));
        purposeOfUseAttribute.addChildElement(purposeOfUseAttributeValue);

        SOAPElement roleAttribute = SOAPObjectFactory.createAttribute(SOAPConstants.ROLE_NS, SOAPConstants.ATTR_NAME_FORMAT_UNSPECIFIED_NS);
        SOAPElement roleAttributeValue = SOAPObjectFactory.createAttributeValue();
        roleAttributeValue.addChildElement(SOAPObjectFactory.createClaimChild(SOAPConstants.ROLE, ROLE_CODE, "2.16.756.5.30.1.127.3.10.6", "eHealth Suisse EPR Akteure", "Behandelnde(r)"));
        roleAttribute.addChildElement(roleAttributeValue);

        SOAPElement principalIdAttribute = SOAPObjectFactory.createAttribute(SOAPConstants.PRINCIPAL_ID_NS);
        SOAPElement principalIdAttributeValue = SOAPObjectFactory.createAttributeValue(SOAPConstants.XS_STRING, TECHNICAL_USER_GLN);
        principalIdAttribute.addChildElement(principalIdAttributeValue);

        SOAPElement principalNameAttribute = SOAPObjectFactory.createAttribute(SOAPConstants.PRINCIPAL_NAME_NS);
        SOAPElement principalNameAttributeValue = SOAPObjectFactory.createAttributeValue(SOAPConstants.XS_STRING, TECHNICAL_USER_NAME);
        principalNameAttribute.addChildElement(principalNameAttributeValue);

        claims.addChildElement(pidAttribute);
        claims.addChildElement(purposeOfUseAttribute);
        claims.addChildElement(roleAttribute);
        claims.addChildElement(principalIdAttribute);
        claims.addChildElement(principalNameAttribute);

        return claims;
    }

    private static Element sendRequest(SOAPMessage request, String enpoint) throws SOAPException {
        SOAPConnection soapConnection = soapConnectionFactory.createConnection();
        SOAPMessage response = null;
        try {
            response = soapConnection.call(request, enpoint);
        } catch (SOAPException e) {
            LOG.error(e.getMessage());
        }

        if (response != null) {
            return extractAssertionFromResponse(response.getSOAPBody());
        } else {
            throw new SOAPException("Response received from " + enpoint + " is null and cannot be extracted.");
        }
    }

    private static Element extractAssertionFromResponse(SOAPBody soapBody) {
        if (soapBody != null) {
            NodeList assertions = soapBody.getElementsByTagNameNS(SOAPConstants.ASSERTION_NS, SOAPConstants.ASSERTION);
            if (assertions.getLength() > 0) {
                return (Element) assertions.item(0);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
