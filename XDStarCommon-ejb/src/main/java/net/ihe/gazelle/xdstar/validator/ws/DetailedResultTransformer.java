package net.ihe.gazelle.xdstar.validator.ws;

import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespacePrefixMapper;

public final class DetailedResultTransformer {
	
	private DetailedResultTransformer() {}
	
	public static DetailedResult load(InputStream is) throws JAXBException{
		JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.validation");
		Unmarshaller u = jc.createUnmarshaller();
		return (DetailedResult) u.unmarshal(is);
	}
	
	public static void save(OutputStream os, DetailedResult txdw) throws JAXBException{
		JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.validation");
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_ENCODING, "UTF8");
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new XDSNamespacePrefixMapper());
		m.marshal(txdw, os);
	}
	
}
