/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.common.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;

import org.jboss.seam.Component;

/**
 *  <b>Class Description :  </b>AbstractMessage<br><br>
 *  The abstract class to record messages in database. It describes the messages sent and received
 *  
 * AbstractMessage possesses the following attributes :
 * <ul>
 * <li><b>timestamp</b> : timeStamp of sending or received</li>
 * <li><b>messageContent</b> : content of the sent or received message</li>
 * <li><b>messageType</b> : message type</li>
 * <li><b>transaction</b> : pointer to the transaction </li>
 * <li><b>configuration</b> : pointer to the configuration of the sender or receiver </li>
 * <li><b>responseCode</b> : response code extracted from the HTTP header </li>
 * <li><b>contentType</b> : content-type of the message (eg application/soap+xml, related soap ...) </li>
 * <li><b>gazelleDriven</b> : indicates whether the message has been sent in the context of a test instance (= true) or not (=false) </li>
 * <li><b>relativeMessage</b> :  query message relative to the response message. If relativeMessage = null, the message is the query and the mentionned configuration is the receiver one </li>
 * </ul></br>
 *  
 * @class	                AbstractMessage.java
 * @package        			net.ihe.gazelle.xdstar.common.model
 * @author                	Abderrazek Boufahja / INRIA Rennes IHE development Project
 *
 *
 */

@Entity(name="abstractMessage")
@Table(name = "xdstar_message", schema = "public")
@SequenceGenerator(name = "xdstar_message_sequence", sequenceName = "xdstar_message_id_seq", allocationSize = 1)
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("AbstractMessage")
public class AbstractMessage extends CommonAbstractMessage implements Serializable
{
	/**
	 * 
	 */
	protected static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator="xdstar_message_sequence", strategy=GenerationType.SEQUENCE)
	@NotNull
	@Column(name="id")
	private Integer id;


	@Column(name="response_code")
	private Integer responseCode;
	
	@Column(name="is_gazelle_driven")
	private Boolean gazelleDriven;
	
	@ManyToOne
	@JoinColumn(name="configuration_id")
	private SystemConfiguration configuration;
	

	public AbstractMessage(){}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}


	public Boolean getGazelleDriven() {
		return gazelleDriven;
	}

	public void setGazelleDriven(Boolean gazelleDriven) {
		this.gazelleDriven = gazelleDriven;
	}

	public SystemConfiguration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(SystemConfiguration configuration) {
		this.configuration = configuration;
	}

	/**
	 * returns all the messages contained in database	
	 * @return
	 */
	public static <T> List<T> getAllMessages(Class<T> clazz, Boolean gazelleDriven) {
		HQLQueryBuilder<T> hh = new HQLQueryBuilder<T>(clazz);
		hh.addEq("gazelleDriven", gazelleDriven);
		hh.addOrder("timeStamp", false);
		return hh.getList();
	}
	
	/**
	 * 
	 * @param message
	 * @return
	 */
	public static <T extends AbstractMessage> T storeMessage(T message) {
		if (message != null)
		{
			EntityManager em = null;
			try{
				em = (EntityManager) Component.getInstance("entityManager");
				message = em.merge(message);
				em.flush();
				return message;
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((configuration == null) ? 0 : configuration.hashCode());
		result = prime * result
				+ ((gazelleDriven == null) ? 0 : gazelleDriven.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((responseCode == null) ? 0 : responseCode.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AbstractMessage other = (AbstractMessage) obj;
		if (configuration == null) {
			if (other.configuration != null) {
				return false;
			}
		} else if (!configuration.equals(other.configuration)) {
			return false;
		}
		if (gazelleDriven == null) {
			if (other.gazelleDriven != null) {
				return false;
			}
		} else if (!gazelleDriven.equals(other.gazelleDriven)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (responseCode == null) {
			if (other.responseCode != null) {
				return false;
			}
		} else if (!responseCode.equals(other.responseCode)) {
			return false;
		}
		return true;
	}

	public String getFullPermanentLink()
	{
		String baseUrl = ApplicationConfiguration.getValueOfVariable("application_url");
		return baseUrl + "/message.seam?id=" + this.getId();		
	}

	
	
}