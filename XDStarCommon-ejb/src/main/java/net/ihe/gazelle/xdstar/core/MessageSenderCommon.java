/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.core;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import javax.net.ssl.HttpsURLConnection;
import javax.persistence.EntityManager;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.util.Pair;
import net.ihe.gazelle.xdstar.common.model.AbstractMessage;
import net.ihe.gazelle.xdstar.common.model.ContentType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import net.ihe.gazelle.xdstar.comon.util.URLCheck;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.Component;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <b>Class Description :  </b>MessageSender<br><br>
 * Class used to send and receive the messages.
 * 
 * @author                	Abderrazek Boufahja
 *
 */

public class MessageSenderCommon {
	
	
	private static Logger log = LoggerFactory
			.getLogger(MessageSenderCommon.class);

	private AbstractMessage request;
	private String respGwUrl;

	/**
	 * Getters and Setters
	 */
	public AbstractMessage getRequest() {
		return request;
	}

	public void setRequest(AbstractMessage request) {
		this.request = request;
	}

	public String getRespGwUrl() {
		return respGwUrl;
	}

	public void setRespGwUrl(String respGwUrl) {
		this.respGwUrl = respGwUrl;
	}

	/**
	 * Constructors
	 */
	public MessageSenderCommon()
	{

	}

	public MessageSenderCommon(String inRespGwUrl, AbstractMessage inRequest)
	{
		this.respGwUrl = inRespGwUrl;
		this.request = inRequest;
	}

	/**
	 * Sends the message, gets the response and merge the request and response objects
	 * @return true if everything runs properly
	 * @throws Exception 
	 */
	public boolean sendMessageSOAP(MetadataMessageType type)
	{
		if (respGwUrl == null || respGwUrl.isEmpty())
		{
			return false;
		}
		else if (request == null || request.getSentMessageContent() == null ||
				request.getSentMessageContent()== null ||
				request.getSentMessageContent().getMessageContent() == null ||
				request.getSentMessageContent().getMessageContent().length() == 0)
		{
			return false;
		}
		else
		{
			MetadataMessage resp = new MetadataMessage();
			Pair<String, String> respS = null;
			try {
				respS = sendToUrlAndGetResponse(respGwUrl, request.getSentMessageContent().getMessageContent().getBytes(StandardCharsets.UTF_8), request, "application/soap+xml");
			} catch (Exception e) {
				respS = new Pair<String, String>(null, "An exception occure when trying to send the request : \n");
				 OutputStream out = new ByteArrayOutputStream(); 
				 e.printStackTrace(new PrintStream(out));
				 String oo = out.toString();
				 if (oo != null){
					 String sub = StringUtils.substring(oo, 0, 300);
					 if (sub.length()<oo.length()){
						 sub = sub + "...";
					 }
					 respS.setObject2(respS.getObject2() + sub);
				 }
			}
			request.getSentMessageContent().setHttpHeader("Content-Type: application/soap+xml");
			resp.setMessageContent(respS.getObject2());
			resp.setHttpHeader(respS.getObject1());
			resp.setMessageType(type);
			request.setReceivedMessageContent(resp);
			return true;
		}
	}
	
	/**
	 * Sends the message and returns the received response
	 * @param respGwUrl
	 * @param request
	 * @param response
	 * @param entityManager
	 * @return
	 */
	public Pair<String, String> sendMessage(String respGwUrl, byte[] messageToSend, AbstractMessage request, String contentTypeConnection){
		EntityManager em = (EntityManager) Component.getInstance("entityManager");
		return this.sendMessage(respGwUrl, messageToSend, request, contentTypeConnection, em);
	}
	
	public Pair<String, InputStream> sendMessageAndGetInputStream(String respGwUrl, byte[] messageToSend, AbstractMessage request, String contentTypeConnection){
		EntityManager em = null;
		try { 
			em = (EntityManager) Component.getInstance("entityManager");
		}
		catch(Exception e) {
			e.printStackTrace();
		}
			
		return this.sendMessageAndGetInputStream(respGwUrl, messageToSend, request, contentTypeConnection, em);
	}
	
	public Pair<String, InputStream> sendMessageAndGetInputStream(String respGwUrl, byte[] messageToSend, AbstractMessage request, String contentTypeConnection, EntityManager em)
	{
		try{
			return sendToUrlAndGetResponseAsInputStream(respGwUrl, messageToSend, request, contentTypeConnection, em);
		}catch(Exception e){
			String respS = "An exception occure when trying to send the request : \n";
			 OutputStream out = new ByteArrayOutputStream(); 
			 e.printStackTrace(new PrintStream(out));
			 String oo = out.toString();
			 if (oo != null){
				 String sub = StringUtils.substring(oo, 0, 300);
				 if (sub.length()<oo.length()){
					 sub = sub + "...";
				 }
				 respS = respS + sub;
			 }
			 return new Pair<String, InputStream>("", new ByteArrayInputStream(respS.getBytes()));
		}
	}
	

	/**
	 * Sends the message and returns the received response
	 * @param respGwUrl
	 * @param request
	 * @param response
	 * @param entityManager
	 * @return
	 */
	public Pair<String, String> sendMessage(String respGwUrl, byte[] messageToSend, AbstractMessage request, String contentTypeConnection, EntityManager em)
	{
		try{
			return sendToUrlAndGetResponse(respGwUrl, messageToSend, request, contentTypeConnection, em);
		}catch(Exception e){
			String respS = "An exception occure when trying to send the request : \n";
			 OutputStream out = new ByteArrayOutputStream(); 
			 e.printStackTrace(new PrintStream(out));
			 String oo = out.toString();
			 if (oo != null){
				 String sub = StringUtils.substring(oo, 0, 300);
				 if (sub.length()<oo.length()){
					 sub = sub + "...";
				 }
				 respS = respS + sub;
			 }
			 return new Pair<String, String>("", respS);
		}
	}
	
	/**
	 * Sends the message to the given URL
	 * @param inUrl
	 * @param messageToSend
	 * @return
	 * @throws IOException 
	 * @throws SocketTimeoutException 
	 */
	protected Pair<String, String> sendToUrlAndGetResponse(String inUrl, byte[] messageToSend, AbstractMessage request, String contentTypeConnection){
		EntityManager em = (EntityManager) Component.getInstance("entityManager");
		return this.sendToUrlAndGetResponse(inUrl, messageToSend, request, contentTypeConnection, em);
	}

	/**
	 * Sends the message to the given URL
	 * @param inUrl
	 * @param messageToSend
	 * @param EntityManager
	 * @return
	 * @throws IOException 
	 * @throws SocketTimeoutException 
	 */
	protected Pair<String, String> sendToUrlAndGetResponse(String inUrl, byte[] messageToSend, AbstractMessage request, String contentTypeConnection,
			EntityManager em) 
	{
		try{
			Pair<String, InputStream> inputStream = this.sendToUrlAndGetResponseAsInputStream(inUrl, messageToSend, request, contentTypeConnection, em);
			String response = null;
			if (inputStream.getObject1()!= null && inputStream.getObject1().contains("multipart")){
				response = AttachmentsUtil.transformMultipartToSOAP(inputStream.getObject2(), inputStream.getObject1());
			}
			else{
				response = AttachmentsUtil.transformInputToString(inputStream.getObject2());
			}
			return new Pair<String, String>(inputStream.getObject1(), response) ;
		}
		catch(Exception e){
			String respS = "The request has not be sent to the server. An exception occure when trying to send the request : \n";
			 OutputStream out = new ByteArrayOutputStream(); 
			 e.printStackTrace(new PrintStream(out));
			 String oo = out.toString();
			 if (oo != null){
				 String sub = StringUtils.substring(oo, 0, 300);
				 if (sub.length()<oo.length()){
					 sub = sub + "...";
				 }
				 respS = respS + sub;
			 }
			 return new Pair<String, String>("", respS);
		}

	}
	
	protected Pair<String, InputStream> sendToUrlAndGetResponseAsInputStream(String inUrl, byte[] messageToSend, AbstractMessage request, String contentTypeConnection,
			EntityManager em) 
	{
		try{
			URL url;
			Pair<String, InputStream> inputStream;
	
	
			url = new URL(inUrl);
	
			if (inUrl.startsWith("https"))
			{
				inputStream = sendOverHttps(url, messageToSend, request, contentTypeConnection, em);
			}
			else
			{
				inputStream = sendOverHttp(url, messageToSend, request, contentTypeConnection, em);
			}
			return inputStream;
		}
		catch(Exception e){
			String respS = "The request has not be sent to the server. An exception occure when trying to send the request : \n";
			 OutputStream out = new ByteArrayOutputStream(); 
			 e.printStackTrace(new PrintStream(out));
			 String oo = out.toString();
			 if (oo != null){
				 String sub = StringUtils.substring(oo, 0, 300);
				 if (sub.length()<oo.length()){
					 sub = sub + "...";
				 }
				 respS = respS + sub;
			 }
			 return new Pair<String, InputStream>("", new ByteArrayInputStream(respS.getBytes()));
		}

	}

	/**
	 * Sends the message using HTTP protocol
	 * @param inUrl
	 * @param inMessageToSend
	 * @return
	 * @throws IOException
	 * @throws SocketTimeoutException: timeout value is set in the application preferences
	 */
	protected Pair<String, InputStream> sendOverHttp(URL inUrl, byte[] inMessageToSend, AbstractMessage request, String contentTypeConnection, 
			EntityManager em) throws Exception
	{
		HttpURLConnection connection = (HttpURLConnection) inUrl.openConnection();
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setUseCaches(false);
		connection.setRequestProperty("Content-Type", contentTypeConnection);
		if (em != null){
			Integer timeout = Integer.parseInt(ApplicationConfiguration.getValueOfVariable("connection_timeout", em));
			connection.setConnectTimeout(timeout);
			connection.setReadTimeout(timeout);
		}

		DataOutputStream out = new DataOutputStream(connection.getOutputStream());
		out.write(inMessageToSend);
		out.flush();
		out.close();

		request.setResponseCode(connection.getResponseCode());

		if (connection.getErrorStream() != null){
			return new Pair<String, InputStream>(this.getHeaderFromHttpConnexion(connection), connection.getErrorStream());
		}
		else if (connection.getInputStream() != null){
			return new Pair<String, InputStream>(this.getHeaderFromHttpConnexion(connection), connection.getInputStream());
		}
		return null;
	}

	/**
	 * Sends the message using HTTPS protocol
	 * @param inUrl
	 * @param inMessageToSend
	 * @return
	 * @throws IOException
	 * @throws SocketTimeoutException: timeout value is set in the application preferences
	 */
	protected Pair<String, InputStream> sendOverHttps(URL inUrl, byte[] inMessageToSend, AbstractMessage request, String contentTypeConnection, 
			EntityManager em) throws Exception
	{
		HttpsURLConnection connection = URLCheck.createHttpsConnection(inUrl);
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setUseCaches(false);
		connection.setRequestProperty("Content-Type", contentTypeConnection);
		Integer timeout = Integer.parseInt(ApplicationConfiguration.getValueOfVariable("connection_timeout", em));
		connection.setConnectTimeout(timeout);
		connection.setReadTimeout(timeout);

		DataOutputStream out = new DataOutputStream(connection.getOutputStream());
		out.write(inMessageToSend);
		out.flush();
		out.close();

		request.setContentType(ContentType.SOAP_MESSAGE);
		
		request.setResponseCode(connection.getResponseCode());

		if (connection.getErrorStream() != null) {
			return new Pair<String, InputStream>(this.getHeaderFromHttpConnexion(connection), connection.getErrorStream());
		}
		else if (connection.getInputStream() != null) {
			return new Pair<String, InputStream>(this.getHeaderFromHttpConnexion(connection), connection.getInputStream());
		}
		else { 
			return null;
		}
	}

	public void checkSelectedConfiguration(SystemConfiguration sysconf){
		if (sysconf != null){
			boolean isAvailable = URLCheck.checkURL(sysconf.getUrl());
			sysconf.setIsAvailable(isAvailable);
			EntityManager em = (EntityManager)Component.getInstance("entityManager");
			em.merge(sysconf);
			em.flush();
			FacesMessages.instance().add(StatusMessage.Severity.INFO,"Configuration successfully checked !");
		}
	}
	
	
	private String getHeaderFromHttpConnexion(HttpURLConnection myHttpURLConnection){
		return myHttpURLConnection.getContentType();
	}
	
}
