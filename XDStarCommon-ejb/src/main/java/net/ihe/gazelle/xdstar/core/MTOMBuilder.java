package net.ihe.gazelle.xdstar.core;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class MTOMBuilder {
	
	private MTOMBuilder(){}
	
	/**
	 * 
	 * @throws IOException
	 */
	public static byte[] buildMTOM(List<MTOMElement> listDataToSend) throws IOException
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream(); 
		String boundaryUUID = UUID.randomUUID().toString().replace("-", "").toUpperCase();
		String delimiter = "--MIMEBoundaryurn_uuid_" + boundaryUUID + "\r\n";
		String rn = "\r\n";
		
		Collections.sort(listDataToSend);
		
		int i = 0;
		for (MTOMElement mtomElement : listDataToSend) {
			baos.write(delimiter.getBytes());
			baos.write(("Content-Type: " + mtomElement.getContentType() + rn).getBytes());
			baos.write(("Content-Transfer-Encoding: " + mtomElement.getTransfertEncoding() + rn).getBytes());
			if (mtomElement.getIndex() == null) {
				mtomElement.setIndex(i);
			}
			baos.write(("Content-ID: <" + mtomElement.getIndex() + ".urn:uuid:" + mtomElement.getUuid() + "@ws.jboss.org>" + rn).getBytes());
			baos.write(rn.getBytes());
			baos.write(mtomElement.getContent());
			baos.write(rn.getBytes());
			i++;
		}
		
		
		baos.write((delimiter.trim() + "--").getBytes());
		return baos.toByteArray();
	}
	
	public static String getBoundaryUUIDFromGeneratedMTOM(byte[] mtom){
		Pattern pp = Pattern.compile(".*--MIMEBoundaryurn_uuid_(.*)");
		Matcher m = pp.matcher(new String(mtom));
		while (m.find()){
			String bound = m.group(1).trim();
			System.out.println(bound);
			if (bound != null) {
				return bound;
			}
		}
		return null;
	}
	
	public static void main(String[] args) {
		String rr = MTOMBuilder.getBoundaryUUIDFromGeneratedMTOM(("Host: localhost:9085\r\n" + 
			"\r\n" + 
			"--MIMEBoundaryurn_uuid_806D8FD2D542EDCC2C1199332890718\r\n" + 
			"Content-Type: application/xop+xml; charset=UTF-").getBytes());
		System.out.println(rr);
	}
	
	
}
