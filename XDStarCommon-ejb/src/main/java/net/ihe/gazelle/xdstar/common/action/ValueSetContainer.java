package net.ihe.gazelle.xdstar.common.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.ihe.gazelle.simulator.common.model.Concept;

public class ValueSetContainer {
	
	private static Map<String, List<Concept>> listConcepts;

	public static Map<String, List<Concept>> getListConcepts() {
		if (listConcepts == null){
			listConcepts = new HashMap<String, List<Concept>>();
		}
		return listConcepts;
	}


}
