package net.ihe.gazelle.xdstar.core;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.io.IOUtils;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;


public final class DocumentFileUpload {

    private static Logger log = LoggerFactory.getLogger(DocumentFileUpload.class);

    private DocumentFileUpload() {
    }

    public static void showFile(String fullPath) {
        DocumentFileUpload.showFile(fullPath, null, null, false);
    }

    public static void showFile(String fullPath, String filename, String contentType, boolean download) {
        try {
            java.io.File f = new java.io.File(fullPath);
            if (filename == null) {
                filename = f.getName();
            }
            InputStream inputStream = null;
            if (f.exists()) {
                inputStream = new FileInputStream(fullPath);
                log.info("file = " + f.getName());
            } else {
                log.info("The file does not exist !! " + f);
            }
            showFile(inputStream, filename, contentType, download);
        } catch (Exception e) {
            log.error("error on showfile method : ", e.getMessage());
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Impossible to display file");
        }
    }


    public static void showFile(File file, String contentType, boolean download) throws FileNotFoundException {
        showFile(new FileInputStream(file), file.getName(), contentType, download);
    }

    public static void showFile(byte[] bytes, String filename, String contentType, boolean download) {
        showFile(new ByteArrayInputStream(bytes), filename, contentType, download);
    }

    public static void showFile(InputStream inputStream, String filename, String contentType, boolean download) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
        try {
            while (true) {
                response = (HttpServletResponse) PropertyUtils.getProperty(response, "response");
            }
        } catch (Exception e1) {
            // nothing to do
            // we are at org.apache.catalina.connector.ResponseFacade
        }
        response.reset();
        HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();

        showFile(request, response, inputStream, filename, contentType, download);

        facesContext.responseComplete();
    }

    public static void showFile(HttpServletRequest request, HttpServletResponse response, InputStream inputStream,
                                String filename, String contentTypeOverride, boolean download) {
        try {
            if (inputStream != null) {
                String userAgent = request.getHeader("user-agent");
                boolean isInternetExplorer = (userAgent.indexOf("MSIE") > -1);

                int length = inputStream.available();

                if (contentTypeOverride != null && !contentTypeOverride.equals("")) {
                    response.setContentType(contentTypeOverride);
                }

//				if ((filename != null) && filename.toLowerCase().endsWith(".pdf")) {
//					response.setContentType("application/pdf");
//				} else if ((filename != null) && filename.toLowerCase().endsWith(".xml")) {
//					response.setContentType("text/xml");
//				} else {
//					String contentType = FileTypeMap.getDefaultFileTypeMap().getContentType(filename);
//					response.setContentType(contentType);
//				}

                byte[] fileNameBytes = filename.getBytes((isInternetExplorer) ? ("windows-1250") : ("utf-8"));
                String dispositionFileName = "";
                for (byte b : fileNameBytes) {
                    dispositionFileName += (char) (b & 0xff);
                }

                String disposition;

                if (download) {
                    disposition = "attachment; filename=\"" + dispositionFileName + "\"";
                } else {
                    disposition = "inline; filename=\"" + dispositionFileName + "\"";
                }

                response.setHeader("Content-disposition", disposition);

                response.setContentLength(length);

                ServletOutputStream servletOutputStream = response.getOutputStream();

                IOUtils.copy(inputStream, servletOutputStream);

                servletOutputStream.flush();
                servletOutputStream.close();

                inputStream.close();
            }
        } catch (Exception e) {
            log.error("error on showfile method : ", e.getMessage());
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Impossible to display file");
        }
    }


    public static final void zip(Map<Integer, String> listFile, ZipOutputStream zos) throws IOException {
        for (Integer fileIndex : listFile.keySet()) {
            ZipEntry entry = new ZipEntry(String.valueOf(fileIndex) + ".xml");
            zos.putNextEntry(entry);
            zos.write(listFile.get(fileIndex).getBytes());
            zos.flush();
        }
    }

    public static final List<String> unzip(File zip) throws IOException {
        List<String> listZip = new ArrayList<String>();
        ZipFile archive = new ZipFile(zip);
        Enumeration<?> e = archive.entries();
        while (e.hasMoreElements()) {
            ZipEntry entry = (ZipEntry) e.nextElement();

            InputStream in = archive.getInputStream(entry);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            byte[] buffer = new byte[8192];
            int read;

            while (-1 != (read = in.read(buffer))) {
                baos.write(buffer, 0, read);
            }

            in.close();
            baos.close();
            listZip.add(baos.toString());
        }
        archive.close();
        return listZip;
    }

}
