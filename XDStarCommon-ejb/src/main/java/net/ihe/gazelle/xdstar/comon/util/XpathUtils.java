package net.ihe.gazelle.xdstar.comon.util;

import java.io.ByteArrayInputStream;
import java.util.List;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import net.sf.saxon.xpath.XPathFactoryImpl;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class XpathUtils {
	
	public static Boolean evaluateByString(String string, String expression) throws Exception {
		return XpathUtils.evaluateByString(string, expression, null);
	}

	public static Boolean evaluateByString(String string, String expression, NamespaceContext xmlns) throws Exception {
		Boolean b = null;
		XPathFactory fabrique = new XPathFactoryImpl();
		XPath xpath = fabrique.newXPath();
		if (xmlns != null){
			xpath.setNamespaceContext(xmlns);
		}
		InputSource is = new InputSource(new ByteArrayInputStream(string.getBytes()));
		b = (Boolean) xpath.evaluate(expression, is, XPathConstants.BOOLEAN);
		return b;
	}
	
	public static  List extractByNode(String string, String expression, NamespaceContext xmlns) throws Exception {
		XPathFactory fabrique = new XPathFactoryImpl();
		XPath xpath = fabrique.newXPath();
		if (xmlns != null){
			xpath.setNamespaceContext(xmlns);
		}
		InputSource is = new InputSource(new ByteArrayInputStream(string.getBytes()));
		return (List) xpath.evaluate(expression, is, XPathConstants.NODESET);
	}
	
	public static Boolean evaluateByNode(Node node, String expression) throws Exception {
		return XpathUtils.evaluateByNode(node, expression, null);
	}

	public static Boolean evaluateByNode(Node node, String expression, NamespaceContext xmlns) throws Exception {
		Boolean b = null;
		XPathFactory fabrique = new XPathFactoryImpl();
		XPath xpath = fabrique.newXPath();
		if (xmlns != null){
			xpath.setNamespaceContext(xmlns);
		}
		b = (Boolean) xpath.evaluate(expression, node, XPathConstants.BOOLEAN);
		return b;
	}

	public static Node getNodeFromString(String string, String nodeName) throws Exception {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(new ByteArrayInputStream(string.getBytes()));
		NodeList dd = doc.getElementsByTagName(nodeName);
		return dd.item(0);
	}

}
