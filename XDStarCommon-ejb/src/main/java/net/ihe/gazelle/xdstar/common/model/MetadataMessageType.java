package net.ihe.gazelle.xdstar.common.model;


//@Entity
//@Name("metadataMessageType")
//@Table(name = "metadata_message_type", schema = "public")
public enum MetadataMessageType {
	PROVIDE_AND_REGISTER_DOCUMENT_SET_REQUEST("ProvideAndRegisterDocumentSetRequest"),SUBMIT_OBJECT_REQUEST("SubmitObjectsRequest"),
	REGIQTRY_RESPONSE("RegistryResponse"),
	ADHOC_QUERY_REQUEST("AdhocQueryRequest"),
	ADHOC_QUERY_RESPONSE("AdhocQueryResponse"),
	RETRIEVE_DOCUMENT_SET_REQUEST("RetrieveDocumentSetRequest"),
	RETRIEVE_DOCUMENT_SET_RESPONSE("RetrieveDocumentSetResponse"),
	PRPA_IN201305UV02("PRPA_IN201305UV02"),
	PRPA_IN201306UV02("PRPA_IN201306UV02"),
	PatientLocationQueryRequest("PatientLocationQueryRequest"),
	PatientLocationQueryResponse("PatientLocationQueryResponse"),
	REMOVE_OBJECT_REQUEST("RemoveObjectsRequest"),
	Retrieve_Imaging_Document_Set_REQUEST("RetrieveImagingDocumentSetRequest"),
	HTTP_GET_REQUEST("HTTP/GET request"),HTTP_GET_RESPONSE("HTTP/GET response"),
	SUBSCRIBE_REQUEST("Subscribe"), SUBSCRIBE_RESPONSE("SubscribeResponse"),
	UNSUBSCRIBE_REQUEST("Unsubscribe"), UNSUBSCRIBE_RESPONSE("UnsubscribeResponse"),
	NOTIFY("Notify"),NO_RESPONSE("--");
	
	private MetadataMessageType(String value) {
		this.value = value;
	}
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
