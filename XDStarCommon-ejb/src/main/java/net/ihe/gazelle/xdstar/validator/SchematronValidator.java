package net.ihe.gazelle.xdstar.validator;

import net.ihe.gazelle.sch.validator.ws.client.GazelleObjectValidatorServiceStub;
import net.ihe.gazelle.sch.validator.ws.client.GazelleObjectValidatorServiceStub.ValidateObject;
import net.ihe.gazelle.sch.validator.ws.client.GazelleObjectValidatorServiceStub.ValidateObjectE;
import net.ihe.gazelle.sch.validator.ws.client.GazelleObjectValidatorServiceStub.ValidateObjectResponseE;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;

import net.ihe.gazelle.validation.DetailedResult;
import org.jboss.seam.util.Base64;

import static net.ihe.gazelle.xdstar.validator.MetadataValidator.getDetailedResultAsString;

/**
 * 
 * @author abderrazek boufahja
 *
 */
public class SchematronValidator {
    
    public SchematronValidator(){
    }
    
    public static boolean validateHL7V3ByMetaData(String requestV3, String metaData){
        boolean isValid = false;
        if (requestV3 == null){
            return false;
        }
        try{
            String targetEndpoint = ApplicationConfiguration.getValueOfVariable("schematron_validator");
            String xmlMetaData = metaData;
            String xmlRefStandard = "HL7v3";
            String detailedResult = getValidationResult(targetEndpoint, xmlMetaData, xmlRefStandard, requestV3);
            if (detailedResult.contains("<ValidationTestResult>PASSED</ValidationTestResult>")){
                isValid = true;
            }
            else{
                isValid = false;
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return isValid;
    }
    
    public static String getValidationResultSchematron(String xmlMetaData,String message){
        String result = new String();
        if (message == null) {
        	return result;
        }
//        String targetEndpoint = ApplicationConfiguration.getValueOfVariable("schematron_validator");
//        String xmlRefStandard = "HL7v3";
        try {
            DetailedResult dr = GenericMetadataValidator.validate(message, xmlMetaData);
            String res = getDetailedResultAsString(dr);
            if (res != null && res.contains("?>")){
                res = res.substring(res.indexOf("?>") + 2);
            }
            result = res;
//
//            ModelBasedValidationWSServiceStub modelBasedValidationWSServiceStub = new ModelBasedValidationWSServiceStub(targetEndpoint);
//            modelBasedValidationWSServiceStub._getServiceClient().getOptions().setProperty(org.apache.axis2.Constants.Configuration.DISABLE_SOAP_ACTION, true);
//            String msgToSend = Base64.encodeBytes(message.getBytes());
//            net.ihe.gazelle.validator.mb.ws.ValidateBase64Document validateBase64Document = new net.ihe.gazelle.validator.mb.ws.ValidateBase64Document();
//            validateBase64Document.setBase64Document(msgToSend);
//            validateBase64Document.setValidator(xmlMetaData);
//            ValidateBase64DocumentE validateBase64DocumentE = new ValidateBase64DocumentE();
//            validateBase64DocumentE.setValidateBase64Document(validateBase64Document);
//            ValidateBase64DocumentResponseE validateBase64DocumentResponseE = modelBasedValidationWSServiceStub.validateBase64Document(validateBase64DocumentE);
//            result = validateBase64DocumentResponseE.getValidateBase64DocumentResponse().getDetailedResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
        
    }
    
    private static String getValidationResult(String targetEndpoint, String xmlMetaData, String xmlRefStandard, String message){
        String result = new String();
        try {
            GazelleObjectValidatorServiceStub stub = new GazelleObjectValidatorServiceStub(targetEndpoint);
            stub._getServiceClient().getOptions().setProperty(org.apache.axis2.Constants.Configuration.DISABLE_SOAP_ACTION, true);
            ValidateObject params = new ValidateObject();
            params.setXmlMetadata(xmlMetaData);
            params.setXmlReferencedStandard(xmlRefStandard);
            String msgToSend = Base64.encodeBytes(message.getBytes());
            params.setBase64ObjectToValidate(msgToSend);
            
            ValidateObjectE paramsE = new ValidateObjectE();
            paramsE.setValidateObject(params);
            ValidateObjectResponseE responseE = stub.validateObject(paramsE);
            result = responseE.getValidateObjectResponse().getValidationResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
        
    }
    
}
