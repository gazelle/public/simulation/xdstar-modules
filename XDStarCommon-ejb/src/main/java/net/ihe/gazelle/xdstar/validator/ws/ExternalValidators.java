package net.ihe.gazelle.xdstar.validator.ws;


public enum ExternalValidators {
	
	EPSOS_IDENTIFICATION_SERVICE_REQ("epSOS Identification Service - request"),EPSOS_IDENTIFICATION_SERVICE_RESP("epSOS Identification Service - response"),
	IHE_XCPD_ITI_55_REQ("IHE XCPD ITI-55 - request"),IHE_XCPD_ITI_55_RESP("IHE XCPD ITI-55 - response"),
	IHE_XCPD_ITI_56_REQ("IHE XCPD ITI-56 - request"),IHE_XCPD_ITI_56_RESP("IHE XCPD ITI-56 - response");
	
	private static final String IHE = "ihe";

	private static final String EPSOS_2 = "epsos-2";

	private static final String EPSOS = "epsos";

	ExternalValidators(String val){
		this.value = val;
	}
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
	public static ExternalValidators getExternalValidatorFromDescription(String affinityDomain, String transactionKeyword, String messageType, boolean isRequest ){
		if ((affinityDomain.toLowerCase().contains(EPSOS_2)) && transactionKeyword.contains("PatientIdentificationAndAuthenticationService") && isRequest) return EPSOS_IDENTIFICATION_SERVICE_REQ;
		else if ((affinityDomain.toLowerCase().contains(EPSOS_2)) && transactionKeyword.contains("PatientIdentificationAndAuthenticationService") && !isRequest) return EPSOS_IDENTIFICATION_SERVICE_RESP;
		else if ((affinityDomain.toLowerCase().contains(EPSOS)) && transactionKeyword.contains("IdentificationService") && isRequest) return EPSOS_IDENTIFICATION_SERVICE_REQ;
		else if ((affinityDomain.toLowerCase().contains(EPSOS)) && transactionKeyword.contains("IdentificationService") && !isRequest) return EPSOS_IDENTIFICATION_SERVICE_RESP;
		else if ((affinityDomain.toLowerCase().contains(IHE)) && transactionKeyword.contains("ITI-55") && isRequest) return IHE_XCPD_ITI_55_REQ;
		else if ((affinityDomain.toLowerCase().contains(IHE)) && transactionKeyword.contains("ITI-55") && !isRequest) return IHE_XCPD_ITI_55_RESP;
		else if ((affinityDomain.toLowerCase().contains(IHE)) && transactionKeyword.contains("ITI-56") && isRequest) return IHE_XCPD_ITI_56_REQ;
		else if ((affinityDomain.toLowerCase().contains(IHE)) && transactionKeyword.contains("ITI-56") && !isRequest) return IHE_XCPD_ITI_56_RESP;
		return null;
	}
	
	public static ExternalValidators getExternalValidatorByName(String valName) {
		if (valName == null) {
			return null;
		}
		for (ExternalValidators ext : ExternalValidators.values()) {
			if (ext.getValue().equals(valName)) {
				return ext;
			}
		}
		return null;
	}
	
}
