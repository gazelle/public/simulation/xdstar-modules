package net.ihe.gazelle.xdstar.core.soap;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.utils.XmlUtil;
import net.ihe.gazelle.simulator.common.xua.SamlAssertionAttributes;
import net.ihe.gazelle.simulator.common.xua.SamlAssertionSupplier;
import net.ihe.gazelle.simulator.common.xua.SignatureException;
import net.ihe.gazelle.xdstar.comon.util.Util;
import net.ihe.gazelle.xdstar.core.SignatureManager;
import net.ihe.gazelle.xdstar.core.soap.epr.EPRXUAWorkflow;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.InvalidCanonicalizerException;
import org.picketlink.identity.federation.core.wstrust.WSTrustException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.UUID;

public final class SOAPRequestBuilder {
	
	
	private static Logger log = LoggerFactory.getLogger(SOAPRequestBuilder.class);
	private static final String EPR_XUA_ENABLED = "epr_xua_enabled";
	private static final String IDP_AUTHENTICATION_URL = "idp_authentication_url";
	private static final String STS_URL = "sts_url";
	
	private SOAPRequestBuilder(){}
	
	public static String createSOAPMessage(String message, boolean useXUA, 
			String praticianID, SamlAssertionAttributes attributes, 
			String patientId, String receiverUrl, String actionName, Boolean useTRC) throws ParserConfigurationException, SignatureException {
		String res = null;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document request = builder.newDocument();
		request.setXmlVersion("1.0");
		request.setXmlStandalone(true);
		Element root = createEnvelope(request);
		Element assertion = null;
		Element trcAssertion = null;

		if (Boolean.parseBoolean(ApplicationConfiguration.getValueOfVariable(EPR_XUA_ENABLED))) {
			assertion = EPRXUAWorkflow.getSAMLAssertion(ApplicationConfiguration.getValueOfVariable(IDP_AUTHENTICATION_URL), ApplicationConfiguration.getValueOfVariable(STS_URL));
			useXUA = false;
		}
		if (useXUA) {
			assertion = requestAssertion(praticianID, null, attributes);
			if (useTRC!= null && useTRC) {
				trcAssertion = requestTRCAssertion(assertion, attributes, praticianID, patientId);
			}
		}
		
		Element header = createSOAPHeader(request, assertion, trcAssertion, receiverUrl, actionName);
		root.appendChild(header);
		
		Element body = request.createElement("s:Body");
		body.setAttribute("xmlns:s", "http://www.w3.org/2003/05/soap-envelope"); 
		
		Document pnrDoc = Util.string2DOM(message);
		Node firstDocImportedNode = request.importNode(pnrDoc.getDocumentElement(), true);
		body.appendChild(firstDocImportedNode);
		root.appendChild(body);
		request.appendChild(root);
		try {
			res = XmlUtil.outputDOM(request);
		} catch (CanonicalizationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidCanonicalizerException e) {
			e.printStackTrace();
		}

		return res;
	}
	
	private static Element createEnvelope(Document request) {
		Element root = request.createElement("s:Envelope");
		root.setAttribute("xmlns:s", "http://www.w3.org/2003/05/soap-envelope");
		root.setAttribute("xmlns:a", "http://www.w3.org/2005/08/addressing");
		return root;
	}
	
	private static Element requestTRCAssertion(Element identityAssertion, SamlAssertionAttributes samlAssertionAttributes, String praticianID, String patientId) {
		try {
			if (praticianID == null || praticianID.isEmpty()) {
				praticianID = ApplicationConfiguration.getValueOfVariable("default_pratician_id");
			}
			samlAssertionAttributes.setXSPASubjectTRC(patientId);
			samlAssertionAttributes.setXSPAPurposeOfUse("TREATMENT");
			return SamlAssertionSupplier.getAssertionTRC(praticianID, samlAssertionAttributes, identityAssertion, SignatureManager.getSignatureUtil());
		} catch(WSTrustException e) {
			System.err.println("SOAPRequestBuilder::" + "Cannot generate TRC assertion");
			return null;
		} catch(SignatureException e) {
			System.err.println("SOAPRequestBuilder::" + "Cannot sign TRC assertion");
			return null;
		}
	}
	
	private static Element requestAssertion(String praticianID, String stsEndpoint, SamlAssertionAttributes samlAssertionAttributes) {
		try {
			if (praticianID == null || praticianID.isEmpty()) {
				praticianID = ApplicationConfiguration.getValueOfVariable("default_pratician_id");
			}
			return SamlAssertionSupplier.getAssertion(praticianID, samlAssertionAttributes, SignatureManager.getSignatureUtil());
		} catch(Exception e) {
			log.error("exception occure : " + e.getMessage(), e);
			return null;
		}
	}
	
	private static Element createSOAPHeader(Document request, Element assertion, Element trcAssertion,
			String receiverUrl, String actionName) {
		Element header = request.createElement("s:Header");
		header.setAttribute("xmlns:s", "http://www.w3.org/2003/05/soap-envelope");
		
		if (assertion != null) {
			Element security = request.createElement("wss:Security");
			security.setAttribute("xmlns:wss", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
			Node assertionToken = request.importNode(assertion, true);
			security.appendChild(assertionToken);
			if (trcAssertion != null) {
				Node trcAssertionToken = request.importNode(trcAssertion, true);
				security.appendChild(trcAssertionToken);
			}
			header.appendChild(security);
		}
		
		Element action = request.createElement("a:Action");
		action.setAttribute("s:mustUnderstand", "1");
		//action.setTextContent("urn:ihe:iti:2007:ProvideAndRegisterDocumentSet-b");
		action.setTextContent(actionName);
		
		header.appendChild(action);
		
		Element messageID = request.createElement("a:MessageID");
		UUID uuid = UUID.randomUUID();
		messageID.setTextContent("urn:uuid:" + uuid); 
		header.appendChild(messageID);
		
		Element replyTo = request.createElement("a:ReplyTo");
		Element address = request.createElement("a:Address");
		address.setTextContent("http://www.w3.org/2005/08/addressing/anonymous");
		replyTo.appendChild(address);
		header.appendChild(replyTo);
		
		Element to = request.createElement("a:To");
		to.setAttribute("s:mustUnderstand", "1");
		to.setTextContent(receiverUrl);
		header.appendChild(to);
		
		return header;
	}

}
