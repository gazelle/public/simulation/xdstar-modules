/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.common.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

/**
 * 
 * @author aboufahj
 *
 */
@Entity
@Name("attachmentFile")
@Table(name="attachment_file", schema="public", uniqueConstraints=@UniqueConstraint(columnNames="id"))
@SequenceGenerator(name="attachment_file_sequence", sequenceName="attachment_file_id_seq", allocationSize=1)
public class AttachmentFile implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Logger
	private static Log log;

	@Id
	@GeneratedValue(generator="attachment_file_sequence", strategy=GenerationType.SEQUENCE)
	@NotNull
	@Column(name="id", nullable=false, unique=true)
	private Integer id;

	@Column(name="contentType")
	private String contentType;
	
	@Column(name="contentID")
	private String contentID;
	
	@Column(name="contentTransferEncoding")
	private String contentTransferEncoding;
	
	@Column(name="size")
	private String size;
	
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "metadata_message_id")
	private MetadataMessage metadataMessage;

	//////////////////////////////////////////////////////////////////////////
	// Constructors

	public AttachmentFile(){

	}

	//////////////////////////////////////////////////////////////////////////
	// Getters and Setters
	
	public MetadataMessage getMetadataMessage() {
		return metadataMessage;
	}

	public void setMetadataMessage(MetadataMessage metadataMessage) {
		this.metadataMessage = metadataMessage;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getContentID() {
		return contentID;
	}

	public void setContentID(String contentID) {
		this.contentID = contentID;
	}

	public String getContentTransferEncoding() {
		return contentTransferEncoding;
	}

	public void setContentTransferEncoding(String contentTransferEncoding) {
		this.contentTransferEncoding = contentTransferEncoding;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((contentID == null) ? 0 : contentID.hashCode());
		result = prime
				* result
				+ ((contentTransferEncoding == null) ? 0
						: contentTransferEncoding.hashCode());
		result = prime * result
				+ ((contentType == null) ? 0 : contentType.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((size == null) ? 0 : size.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AttachmentFile other = (AttachmentFile) obj;
		if (contentID == null) {
			if (other.contentID != null)
				return false;
		} else if (!contentID.equals(other.contentID))
			return false;
		if (contentTransferEncoding == null) {
			if (other.contentTransferEncoding != null)
				return false;
		} else if (!contentTransferEncoding
				.equals(other.contentTransferEncoding))
			return false;
		if (contentType == null) {
			if (other.contentType != null)
				return false;
		} else if (!contentType.equals(other.contentType))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (size == null) {
			if (other.size != null)
				return false;
		} else if (!size.equals(other.size))
			return false;
		return true;
	}


	
	

}
