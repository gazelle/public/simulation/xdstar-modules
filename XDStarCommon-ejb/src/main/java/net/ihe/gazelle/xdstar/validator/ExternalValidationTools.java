package net.ihe.gazelle.xdstar.validator;

import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.XSDMessage;
import net.ihe.gazelle.xdstar.common.model.CommonAbstractMessage;
import net.ihe.gazelle.xdstar.validator.ws.DetailedResultTransformer;
import net.ihe.gazelle.xdstar.validator.ws.ExternalValidators;
import org.jdom.JDOMException;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExternalValidationTools {

    private static final String PROBLEM_EXTRACT_PRPA05_MSG =
            "The tool is not able to extract PRPA_IN201305UV02 node with a namespace urn:hl7-org:v3 from the request";

    private static final String PROBLEM_EXTRACT_PRPA06_MSG =
            "The tool is not able to extract PRPA_IN201306UV02 node with a namespace urn:hl7-org:v3 from the response";

    private static final String HL7V3_NAMESPACE = "urn:hl7-org:v3";

    public static final String NO_VALIDATOR_AVAILABLE_MSG = "No Validator is available for this type of document.";

    public static DetailedResult validateWithExternalValidatorDetailedResult(CommonAbstractMessage inMessage, ExternalValidators val) {
        String doc = null;
        if (val != null && val.getValue().contains("request")) {
            doc = inMessage.getSentMessageContent().getMessageContent();
        } else {
            doc = inMessage.getReceivedMessageContent().getMessageContent();
        }
        return validateWithExternalValidatorDetailedResult(doc, val);
    }

    public static DetailedResult validateWithExternalValidatorDetailedResult(String doc, ExternalValidators val) {
        String res = null;
        switch (val) {
            case EPSOS_IDENTIFICATION_SERVICE_REQ:
                String toValidate = null;
                try {
                    toValidate = MetadataValidator.extractNode(doc,
                            "PRPA_IN201305UV02", HL7V3_NAMESPACE);
                } catch (JDOMException e) {
                    return createMessageErrorDetailedResult(PROBLEM_EXTRACT_PRPA05_MSG);
                } catch (IOException e) {
                    return createMessageErrorDetailedResult(PROBLEM_EXTRACT_PRPA05_MSG);
                }
                res = SchematronMetadataValidation.validateEPSOS_IDENTIFICATION_SERVICE_REQ(toValidate);
                break;
            case IHE_XCPD_ITI_55_REQ:
                toValidate = null;
                try {
                    toValidate = MetadataValidator.extractNode(doc,
                            "PRPA_IN201305UV02", HL7V3_NAMESPACE);
                } catch (JDOMException e) {
                    return createMessageErrorDetailedResult(PROBLEM_EXTRACT_PRPA05_MSG);
                } catch (IOException e) {
                    return createMessageErrorDetailedResult(PROBLEM_EXTRACT_PRPA05_MSG);
                }
                res = SchematronMetadataValidation.validateIHE_XCPD_ITI_55_REQ(toValidate);
                break;
            case EPSOS_IDENTIFICATION_SERVICE_RESP:
                toValidate = null;
                try {
                    toValidate = MetadataValidator.extractNode(doc,
                            "PRPA_IN201306UV02", HL7V3_NAMESPACE);
                } catch (JDOMException e) {
                    return createMessageErrorDetailedResult(PROBLEM_EXTRACT_PRPA06_MSG);
                } catch (IOException e) {
                    return createMessageErrorDetailedResult(PROBLEM_EXTRACT_PRPA06_MSG);
                }
                res = SchematronMetadataValidation.validateEPSOS_IDENTIFICATION_SERVICE_RESP(toValidate);
                break;
            case IHE_XCPD_ITI_55_RESP:
                toValidate = null;
                try {
                    toValidate = MetadataValidator.extractNode(doc,
                            "PRPA_IN201306UV02", HL7V3_NAMESPACE);
                } catch (JDOMException e) {
                    return createMessageErrorDetailedResult(PROBLEM_EXTRACT_PRPA06_MSG);
                } catch (IOException e) {
                    return createMessageErrorDetailedResult(PROBLEM_EXTRACT_PRPA06_MSG);
                }
                res = SchematronMetadataValidation.validateIHE_XCPD_ITI_55_RESP(toValidate);
                break;
            default:
                return createMessageWarningDetailedResult(NO_VALIDATOR_AVAILABLE_MSG);
        }
        if (res != null) {
            try {
                res = res.replace("SchematronValidation", "MDAValidation");
                res = cleanDeclarations(res);
                DetailedResult ree = DetailedResultTransformer.load(new ByteArrayInputStream(res.getBytes()));
                updateContentOfSchematronValidation(res, ree);
                return ree;
            } catch (JAXBException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }

    private static String cleanDeclarations(String content) {
        String res = content;
        Pattern pat = Pattern.compile("<\\?.*\\?>");
        Matcher mat = pat.matcher(content);
        while (mat.find()) {
            res = res.replace(mat.group(), "");
        }
        return res.trim();
    }

    private static void updateContentOfSchematronValidation(String res, DetailedResult dr) {
        if (res != null && dr != null) {
            Pattern pat = Pattern.compile("<message\\d+>(.*?)</message\\d+>", Pattern.MULTILINE | Pattern.DOTALL);
            java.util.regex.Matcher mat = pat.matcher(res);
            while (mat.find()) {
                String content = mat.group(1);
                if (dr.getDocumentValidXSD() == null) {
                    dr.setDocumentValidXSD(new DocumentValidXSD());
                }
                dr.getDocumentValidXSD().setResult("FAILED");
                XSDMessage xsd = new XSDMessage();
                xsd.setSeverity("error");
                xsd.setMessage(content);
                dr.getDocumentValidXSD().getXSDMessage().add(xsd);
            }
        }
    }

    private static String createMessageError(String mess) {
        String res = "<detailedResult>" +
                "<SchematronValidation>" +
                "<Result>FAILED</Result>" +
                "<Error>" +
                "<Test>Parsing message</Test>" +
                "<Location>All the document</Location>" +
                "<Description>" + mess + "</Description>" +
                "</Error>" +
                "</SchematronValidation>" +
                "<ValidationResultsOverview>" +
                "<ValidationDate>" + new Date() + "</ValidationDate>" +
                "<ValidationServiceName>None</ValidationServiceName>" +
                "<Schematron>None</Schematron>" +
                "<ValidationTestResult>FAILED</ValidationTestResult>" +
                "</ValidationResultsOverview>" +
                "</detailedResult>";
        return res;
    }

    private static DetailedResult createMessageErrorDetailedResult(String mess) {
        String res = createMessageError(mess);
        try {
            DetailedResult resDR = DetailedResultTransformer.load(new ByteArrayInputStream(res.getBytes()));
            return resDR;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String createMessageWarning(String mess) {
        String res = "<detailedResult>" +
                "<SchematronValidation>" +
                "<Result>PASSED</Result>" +
                "<Warning>" +
                "<Test>Validator</Test>" +
                "<Location>All the document</Location>" +
                "<Description>" + mess + "</Description>" +
                "</Warning>" +
                "</SchematronValidation>" +
                "<ValidationResultsOverview>" +
                "<ValidationDate>" + new Date() + "</ValidationDate>" +
                "<ValidationServiceName>None</ValidationServiceName>" +
                "<Schematron>None</Schematron>" +
                "<ValidationTestResult>PASSED</ValidationTestResult>" +
                "</ValidationResultsOverview>" +
                "</detailedResult>";
        return res;
    }

    private static DetailedResult createMessageWarningDetailedResult(String mess) {
        String res = createMessageWarning(mess);
        try {
            DetailedResult resDR = DetailedResultTransformer.load(new ByteArrayInputStream(res.getBytes()));
            return resDR;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
