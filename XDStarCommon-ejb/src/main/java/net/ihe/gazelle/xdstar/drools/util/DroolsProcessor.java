/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.drools.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;

import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespaceContext;
import net.ihe.gazelle.xdstar.comon.util.XpathUtils;

import org.apache.commons.lang.ArrayUtils;
import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.Resource;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <b>Class Description : </b>DroolsManager<br>
 * <br>
 * This class manage the Drools (Security, TFDependencies). It corresponds to the Business Layer. All operations to implement on Drools are done in this class :
 * 
 * 
 * @class DroolsManager.java
 * @package net.ihe.gazelle.pr.application.action
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @see > Jchatel@irisa.fr - http://www.ihe-europe.org
 * @version 1.0 - 2009, April 25
 * 
 */
@Name("droolsManager")
@Scope(ScopeType.PAGE)
public class DroolsProcessor implements Serializable {

	private static final long serialVersionUID = 4206157963568544277L;

	private static Logger log = LoggerFactory.getLogger(DroolsProcessor.class);

	private static KnowledgeBase kbase = null;
	private static Object kbaseBuildSync = new Object();

	static class DroolsStatus {
		KnowledgeBase kbase;
		KnowledgeBuilderErrors errors;
	}

	private static KnowledgeBase getKBase() {
		KnowledgeBase result;
		synchronized (kbaseBuildSync) {
			result = kbase;
		}
		return result;
	}

	public static void initKnowledgeBase() {
		if (kbase == null) {
			synchronized (kbaseBuildSync) {
				if (kbase == null) {
					DroolsStatus droolsStatus = createKnowledgeBase(ResourceFactory
							.newClassPathResource("manifestGen.drl"));
					if (droolsStatus.kbase != null) {
						kbase = droolsStatus.kbase;
					} else {
						System.out.println("Failed to compile Drools : " + errorsToString(droolsStatus.errors));
						log.debug("Failed to compile Drools : " + errorsToString(droolsStatus.errors));
					}
				}
			}
		}
	}

	private static String errorsToString(KnowledgeBuilderErrors errors) {
		StringBuilder sb = new StringBuilder();
		for (KnowledgeBuilderError knowledgeBuilderError : errors) {
			int[] errorLines = knowledgeBuilderError.getLines();
			sb.append("Lines ").append(ArrayUtils.toString(errorLines)).append(" : ")
					.append(knowledgeBuilderError.getMessage()).append("\r\n");
		}
		return sb.toString();
	}

	public static DroolsStatus resetKnowledgeBase(Resource resource) {
		DroolsStatus droolsStatus = createKnowledgeBase(resource);
		if (droolsStatus.kbase != null) {
			synchronized (kbaseBuildSync) {
				kbase = droolsStatus.kbase;
			}
		}
		return droolsStatus;
	}

	private static DroolsStatus createKnowledgeBase(Resource resource) {
		DroolsStatus result = new DroolsStatus();

		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
		kbuilder.add(resource, ResourceType.DRL);
		//kbuilder.add(ResourceFactory.newClassPathResource("xvalidator.dsl"), ResourceType.DSL);

		if (kbuilder.getErrors() != null && kbuilder.getErrors().size() > 0) {
			String errorsToString = errorsToString(kbuilder.getErrors());
			log.error(errorsToString);
		}

		if (kbuilder.hasErrors()) {
			result.errors = kbuilder.getErrors();

			result.kbase = null;
		} else {
			KnowledgeBase kbaseBuild = KnowledgeBaseFactory.newKnowledgeBase();
			kbaseBuild.addKnowledgePackages(kbuilder.getKnowledgePackages());
			result.kbase = kbaseBuild;
		}
		return result;
	}

	public static List<Notification> validateManifestContent(DroolsDocuments drd) {
		
		initKnowledgeBase();
		List<Notification> list = new ArrayList<Notification>();
		StatefulKnowledgeSession session = getKBase().newStatefulKnowledgeSession();
		session.setGlobal("myGlobalList", list);
		
		session.insert(drd);

		session.fireAllRules();
		session.dispose();

		return list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static boolean validateManifest(String xmlContent, String documentReference, String xpath1, String xpath2, XpathComparatorKind kind){
		try{
			switch (kind) {
				case IN:
					List ext1 = XpathUtils.extractByNode(xmlContent, xpath1, XDSNamespaceContext.getInstance());
					List ext2 = XpathUtils.extractByNode(documentReference, xpath2, XDSNamespaceContext.getInstance());
					boolean res = ext2.containsAll(ext1);
					return res;
				case CONTAIN:
					ext1 = XpathUtils.extractByNode(xmlContent, xpath1, XDSNamespaceContext.getInstance());
					ext2 = XpathUtils.extractByNode(documentReference, xpath2, XDSNamespaceContext.getInstance());
					return ext1.containsAll(ext2);
				case EQ:
					ext1 = XpathUtils.extractByNode(xmlContent, xpath1, XDSNamespaceContext.getInstance());
					ext2 = XpathUtils.extractByNode(documentReference, xpath2, XDSNamespaceContext.getInstance());
					return ext1.containsAll(ext2) && ext2.containsAll(ext1);
				case EQSIZE:
					ext1 = XpathUtils.extractByNode(xmlContent, xpath1, XDSNamespaceContext.getInstance());
					ext2 = XpathUtils.extractByNode(documentReference, xpath2, XDSNamespaceContext.getInstance());
					return (ext1 == null && ext2 == null) || (ext1 != null && ext2 != null && ext1.size() == ext2.size());
				default:
					break;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	@Remove
	@Destroy
	public void destroy() {

	}


//	public void installNewDrools() {
//		Resource resource = ResourceFactory.newByteArrayResource(newDrools);
//		DroolsStatus droolsStatus = resetKnowledgeBase(resource);
//		if (droolsStatus.kbase != null) {
//			droolsCompileErrors = "";
//			FacesMessages.instance().addFromResourceBundle(Severity.INFO, "New Drools file installed");
//			DroolsValidatorLocal droolsValidator = (DroolsValidatorLocal) Component.getInstance("droolsValidator");
//			droolsValidator.resetValidation(newDrools);
//			droolsValidator.getValidationResult();
//		} else {
//			droolsCompileErrors = errorsToString(droolsStatus.errors);
//			FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "Failed to install new Drools file");
//		}
//	}

}