package net.ihe.gazelle.xdstar.conf.model;

import net.ihe.gazelle.hql.HQLQueryBuilderInterface;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.simulator.sut.model.SystemConfigurationAttributes;

import org.jboss.seam.security.Identity;

public class ConfigurationFiltering<T extends SystemConfiguration, Q extends SystemConfigurationAttributes<T>> {
	
	Q query;
	
	public ConfigurationFiltering(Q q) {
		this.query = q;
	}

	public HQLCriterionsForFilter<T> getCriterionList() {
		query.name().order(true);
		if (Identity.instance().hasRole("admin_role")){
			HQLCriterionsForFilter<T> result = query.getHQLCriterionsForFilter();
			return result;
		}
		else if (!Identity.instance().isLoggedIn()){
			query.isPublic().eq(true);
		}
		else {
			((HQLQueryBuilderInterface<T>) query).addRestriction(
				HQLRestrictions.or(
					HQLRestrictions.eq("owner", Identity.instance().getCredentials().getUsername()), 
					HQLRestrictions.eq("isPublic", true)
				)
			);
		}
		HQLCriterionsForFilter<T> result = query.getHQLCriterionsForFilter();
		return result;
	}
}
