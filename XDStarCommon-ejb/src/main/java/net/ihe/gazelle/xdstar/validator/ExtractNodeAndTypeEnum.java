package net.ihe.gazelle.xdstar.validator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtractNode", propOrder = {
	"extractNode",
	"extractNodeType",
	"isRequest",
	"isRet",
	"isSq"
})
@XmlRootElement(name = "ExtractNode")

public enum ExtractNodeAndTypeEnum {
	
	REMOVE_OBJECTS_REQUEST("RemoveObjectsRequest","RemoveObjectsRequestType",true,false,false),

	RETRIEVE_DOCUMENT_SET_REQUEST("RetrieveDocumentSetRequest","RetrieveDocumentSetRequestType",true,true,false),

	ADHOC_QUERY_RESPONSE("AdhocQueryResponse","AdhocQueryResponseType",false,false,true),

	ADHOC_QUERY_REQUEST("AdhocQueryRequest","AdhocQueryRequestType",true,false,true),

	REGISTRY_RESPONSE("RegistryResponse","RegistryResponseType",false,false,false),

	SUBMIT_OBJECT_REQUEST_STRING("SubmitObjectsRequest","SubmitObjectsRequestType",true,false,false),
		
	RETRIEVE_DOCUMENT_SET_RESPONSE_STRING("RetrieveDocumentSetResponse","RetrieveDocumentSetResponseType",false,true,false),
		
	RETRIEVE_IMAGING_DOCUMENT_SET_REQUEST_STRING("RetrieveImagingDocumentSetRequest","RetrieveImagingDocumentSetRequestType",true,true,false),
		
	PROVIDE_AND_REGISTER_DOCUMENT_SET_STRING("ProvideAndRegisterDocumentSetRequest","ProvideAndRegisterDocumentSetRequestType",true,false,false);
	    
    private String extractNode;
    
    private String extractNodeType;
    
    private boolean isRequest;
            
    private boolean isRet;
    
    private boolean isSq;
 

	private ExtractNodeAndTypeEnum(String extractNode, String extractNodeType,boolean isRequest, boolean isRet, boolean isSq) {
		this.extractNode = extractNode;
		this.extractNodeType = extractNodeType;
		this.isRequest = isRequest;
		this.isRet = isRet;
		this.isSq = isSq;
	}

	public boolean isRet() {
		return isRet;
	}

	public void setRet(boolean isRet) {
		this.isRet = isRet;
	}

	public boolean isSq() {
		return isSq;
	}

	public void setSq(boolean isSq) {
		this.isSq = isSq;
	}

	public String getExtractNode() {
		return extractNode;
	}

	public void setExtractNode(String extractNode) {
		this.extractNode = extractNode;
	}

	public String getExtractNodeType() {
		return extractNodeType;
	}

	public void setExtractNodeType(String extractNodeType) {
		this.extractNodeType = extractNodeType;
	}

	public boolean isRequest() {
		return isRequest;
	}

	public void setRequest(boolean isRequest) {
		this.isRequest = isRequest;
	}

	
}
