package net.ihe.gazelle.xdstar.common.action;

import java.util.List;

import javax.ejb.Remove;
import javax.faces.model.SelectItem;

import net.ihe.gazelle.xdstar.common.model.AbstractMessage;
import net.ihe.gazelle.xdstar.common.model.CommonAbstractMessage;

import org.jboss.seam.annotations.Destroy;

public interface MessageManagerLocal<T extends CommonAbstractMessage> {

	public void setSelectedMessage(T selectedMessage);

	public T getSelectedMessage();

	public void setStringRequestContent(String stringMessageContent);

	public String getStringRequestContent();

	public String getSelectedMessageTab();

	public void setSelectedMessageTab(String selectedMessageTab);

	public String getGazelleDriven();

	public void setGazelleDriven(String gazelleDriven);

	public void setStringResponseContent(String stringResponseContent);

	public String getStringResponseContent();

	@Destroy
	@Remove
	public void destroy();

	public List<SelectItem> getMessagePossibleOrigins();

	public void selectMessageToDisplay(T inMessage);

	public void getMessageByActorById(Class<T> tt);
	
	public void selectAndValidate(T inMessage, String panel);
	
	public void validate(T inMessage);
	
	public void validateCurrentMessage(String tabpanel);
	
	public String getStringRequestValidation();
	public void setStringRequestValidation(String stringRequestValidation);
	public String getStringResponseValidation();
	public void setStringResponseValidation(String stringResponseValidation);
	
	public void setSelectedMessageAndPanel(T selectedMessage, String panel);
	public void selectMessageToDisplayAndInitPanel(T inMessage) ;
	
	public String getFullPermanentLink(AbstractMessage message);
	
	public abstract T getReloadedSelectedMessage();
	
}