package net.ihe.gazelle.xdstar.comon.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.security.*;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class used to perform a check into an URL
 * 
 * @author abderrazek boufahja
 *
 */
public class URLCheck {

    private static final Logger LOG = LoggerFactory.getLogger(URLCheck.class);


    private URLCheck() {}

	public static boolean checkURL(String inURL){
		if (inURL != null){
			try{
				URLConnection connection = null;
				if (inURL.startsWith("https")){
					URL url = new URL(inURL);
					connection = createHttpsConnection(url);
				}
				else{
					connection = new URL(inURL).openConnection();
				}
				connection.setDoInput(true);
				connection.setDoOutput(true);
				connection.setUseCaches(false);
				Integer timeout = 2000;
				connection.setConnectTimeout(timeout);
				connection.setReadTimeout(timeout);
				connection.connect();
				return true;
			} catch(Exception e){
				return false;
			}
		}
		return false;
	}

	public static HttpsURLConnection createHttpsConnection(URL url) {
		String trustStorePath = ApplicationConfiguration.getValueOfVariable("truststore_path");
		String keystorePath = ApplicationConfiguration.getValueOfVariable("keystore_path");
		String keyStorePassword = ApplicationConfiguration.getValueOfVariable("keystore_pass");
		String keyPass = ApplicationConfiguration.getValueOfVariable("key_pass");
		return createHttpsConnection(url, keystorePath, keyStorePassword, keyPass, trustStorePath);
	}

	public static HttpsURLConnection createHttpsConnection(URL inUrl, String keystorePath, String keyStorePassword,
			String keyPassword, String trustStorePath) {
		try {
			KeyStore keyStore = KeyStore.getInstance("JKS");
			keyStore.load(new FileInputStream(keystorePath),
					keyStorePassword == null ? null : keyStorePassword.toCharArray());

			KeyManagerFactory keyManagerFactory = KeyManagerFactory
					.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			keyManagerFactory.init(keyStore, keyPassword == null ? null : keyPassword.toCharArray());
			KeyManager[] km = keyManagerFactory.getKeyManagers();

			KeyStore trustStore = KeyStore.getInstance("JKS");
			trustStore.load(new FileInputStream(trustStorePath), null);
			TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory
					.getDefaultAlgorithm());
			trustManagerFactory.init(trustStore);
			TrustManager[] tm = trustManagerFactory.getTrustManagers();

			SSLContext context = SSLContext.getInstance("TLS");
			context.init(km, tm, null);
			SSLSocketFactory sslSocketFactory = context.getSocketFactory();
			HttpsURLConnection connection = (HttpsURLConnection) inUrl.openConnection();
			HostnameVerifier verifier = new CustomizedHostnameVerifier();
			connection.setHostnameVerifier(verifier);
			connection.setSSLSocketFactory(sslSocketFactory);
			return connection;
		}catch (KeyStoreException e){
			FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Issue with the KeyStore : "+ e.getMessage());
			LOG.warn("Issue with the KeyStore : "+ e.getMessage());
			return null;
		}catch (FileNotFoundException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "File not found :"+ e.getMessage());
            LOG.warn("File not found : "+ e.getMessage());
            return null;
		}catch (NoSuchAlgorithmException e){
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Impossible to locate a valid certificate : "+ e.getMessage());
            LOG.warn("Impossible to locate a valid certificate : "+ e.getMessage());
            return null;
		}catch (CertificateException e){
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "There is an issue with the certificate : "+ e.getMessage());
            LOG.warn("There is an issue with the certificate : "+ e.getMessage());
            return null;
		}catch (IOException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "There is an issue with input/output connections : " + e.getMessage());
            LOG.warn("There is an issue with input/output connections : " + e.getMessage());
            return null;
		}catch (UnrecoverableKeyException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Cannot recover key : "+ e.getMessage());
            LOG.warn("Cannot recover key : "+ e.getMessage());
            return null;
		}catch (KeyManagementException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Cannot recover key : "+ e.getMessage());
            LOG.warn("Cannot recover key : "+ e.getMessage());
            return null;
		}
	}

	/**
	 * This class is used to avoid java.security.cert.CertificateException: No name matching "hostname" found exception
	 * @author aberge
	 *
	 */
	private static class CustomizedHostnameVerifier implements HostnameVerifier {
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	}


}
