package net.ihe.gazelle.xdstar.conf.model;

public enum ConfigurationTypeType {
	
	REPOSITORY_CONF("Repository / Document Recipient configuration"),
	REGISTRY_CONF("Registry Configuration"), RESPONDING_GATEWAY_CONF("Responding Gateway Configuration"),
	XCPD_RESP_CONF("XCPD Responding Gateway Configuration"), CPM_CONF("Community Pharmacy Manager Configuration"),
	IMR_SRC_CONF("Imaging Document Source Configuration"),INIT_IMG_GAT_CONF("Initiating Imaging Gateway Configuration"),
	RESP_IMG_GAT_CONF("Responding Imaging Gateway Configuration"),
	DOC_NOTIF_BROKER_SUB("Document Metadata Notification Broker Subscribtion/Unsubscription Configuration"),
	DOC_NOTIF_BROKER_PUB("Document Metadata Notification Broker Publish Configuration"),
    DOC_NOTIF_RECIPIENT("Document Metadata Notification Recipient Notify Configuration"),
    UPDATE_CONF("Update Responder Configuration");

    private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	private ConfigurationTypeType(String value) {
		this.value = value;
	}
	
}
