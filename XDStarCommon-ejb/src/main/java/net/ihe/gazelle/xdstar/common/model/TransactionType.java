package net.ihe.gazelle.xdstar.common.model;

import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author abderrazek boufahja
 * 
 */
@Entity
@Name("transactionType")
@Table(name = "transaction_type", schema = "public")
@SequenceGenerator(name = "transaction_type_sequence", sequenceName = "transaction_type_id_seq", allocationSize = 1)
public class TransactionType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TransactionType() {
	}

	@Id
	@GeneratedValue(generator = "transaction_type_sequence", strategy = GenerationType.SEQUENCE)
	@NotNull
	@Column(name = "id")
	private Integer id;

	@NotNull
    @Column(name = "name", unique = true)
    private String name;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(joinColumns = @JoinColumn(name = "transaction_type_id"), 
		inverseJoinColumns = @JoinColumn(name = "transaction_id"), 
		uniqueConstraints = @UniqueConstraint(columnNames = {"transaction_type_id", "transaction_id" })
	)
	private List<Transaction> listTransactions;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Transaction> getListTransactions() {
		return listTransactions;
	}

	public void setListTransactions(List<Transaction> listTransactions) {
		this.listTransactions = listTransactions;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((id == null) ? 0 : id.hashCode());
		result = (prime * result)
				+ ((listTransactions == null) ? 0 : listTransactions.hashCode());
		result = (prime * result) + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TransactionType other = (TransactionType) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (listTransactions == null) {
			if (other.listTransactions != null) {
				return false;
			}
		} else if (!listTransactions.equals(other.listTransactions)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	public TransactionType(String name, List<Transaction> listTransactions) {
		super();
		this.name = name;
		this.listTransactions = listTransactions;
	}

	@Override
	public String toString() {
		return "TransactionType [id=" + id + ", name=" + name
				+ ", listTransactions=" + listTransactions + "]";
	}

}
