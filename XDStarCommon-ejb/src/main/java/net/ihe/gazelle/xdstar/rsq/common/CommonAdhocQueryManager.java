/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.rsq.common;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.metadata.common.RIMGenerator;
import net.ihe.gazelle.metadata.model.AdhocQueryMetadata;
import net.ihe.gazelle.metadata.model.SlotMetadata;
import net.ihe.gazelle.query.AdhocQueryRequestType;
import net.ihe.gazelle.query.ReturnTypeType;
import net.ihe.gazelle.rim.AdhocQueryType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.simulator.common.model.Concept;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.xdstar.common.action.CommonSimulatorManager;
import net.ihe.gazelle.xdstar.common.action.ValueSetContainer;
import net.ihe.gazelle.xdstar.common.model.AbstractMessage;
import net.ihe.gazelle.xdstar.comon.util.SVSConsumerForSimulator;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespacePrefixMapper;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.log.Log;

/**
 *  
 * @author                	Abderrazek Boufahja
 *
 */

public abstract class CommonAdhocQueryManager<T extends SystemConfiguration, X extends AbstractMessage> extends CommonSimulatorManager<T,X> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Logger
	private static Log log;
	
	protected AdhocQueryMetadata selectedMessageType;
	
	protected AdhocQueryType adhocQuery;
	
	protected List<SelectItem> listOptionalMetadata = new ArrayList<SelectItem>();
	
	protected Integer selectedSlotMetadataId;
	
	protected SlotMetadata selectedMetadata;
	
	protected SlotType1 selectedSlotType;
	
	protected String valueToAdd;
	
	protected ReturnTypeType selectedReturnType;
	

	/*********************************************************************
	 * Getters and Setters
	 */
	
	public void setSelectedSlotMetadataId(Integer selectedSlotMetadataId) {
		this.selectedSlotMetadataId = selectedSlotMetadataId;
		EntityManager em = (EntityManager)Component.getInstance("entityManager");
		this.selectedMetadata = em.find(SlotMetadata.class, this.selectedSlotMetadataId);
	}

	public Integer getSelectedSlotMetadataId() {
		return selectedSlotMetadataId;
	}
	
	public SlotType1 getSelectedSlotType() {
		return selectedSlotType;
	}

	public void setSelectedSlotType(SlotType1 selectedSlotType) {
		this.selectedSlotType = selectedSlotType;
	}

	public String getValueToAdd() {
		return valueToAdd;
	}

	public void setValueToAdd(String valueToAdd) {
		this.valueToAdd = valueToAdd;
	}
	
	public List<SelectItem> getListOptionalMetadata() {
		return listOptionalMetadata;
	}
	
	public void setListOptionalMetadata(List<SelectItem> listOptionalMetadata) {
		this.listOptionalMetadata = listOptionalMetadata;
	}
	
	public SlotMetadata getSelectedMetadata() {
		return selectedMetadata;
	}
	
	public void setSelectedMetadata(SlotMetadata selectedMetadata) {
		this.selectedMetadata = selectedMetadata;
	}
	
	public AdhocQueryType getAdhocQuery() {
		return adhocQuery;
	}
	public void setAdhocQuery(AdhocQueryType adhocQuery) {
		this.adhocQuery = adhocQuery;
	}
	
	public void setSelectedMessageType(AdhocQueryMetadata selectedMessageType) {
		this.selectedMessageType = selectedMessageType;
	}
	public AdhocQueryMetadata getSelectedMessageType() {
		return selectedMessageType;
	}
	
	public ReturnTypeType getSelectedReturnType() {
		return selectedReturnType;
	}
	public void setSelectedReturnType(ReturnTypeType selectedReturnType) {
		this.selectedReturnType = selectedReturnType;
	}
	/********************************************************
	 * methods
	 */
	
	
	public abstract void listAllConfigurations();
	
	public abstract void sendMessage();
	
	public void init()
	{
		super.init();
	}
	
	public void reset()
	{
		super.reset();
		this.selectedMessageType = null;
	}
	

	public void updateSpecificClient() {
		// TODO Auto-generated method stub
		
	}
	
	public abstract List<AffinityDomain> listAllAffinityDomains();
	
	public List<AdhocQueryMetadata> getListOfMessageTypesForSelectedTransaction(){
		HQLQueryBuilder<AdhocQueryMetadata> hh = new HQLQueryBuilder<AdhocQueryMetadata>(AdhocQueryMetadata.class);
		AffinityDomain aff = this.selectedAffinityDomain;
		hh.addEq("usages.affinity", aff);
		hh.addEq("usages.transaction", this.selectedTransaction);
		Comparator<AdhocQueryMetadata> comp = new Comparator<AdhocQueryMetadata>() {
			
			@Override
			public int compare(AdhocQueryMetadata o1, AdhocQueryMetadata o2) {
				if (o1.getDisplayAttributeName() == null) return -1;
				return o1.getDisplayAttributeName().compareTo(o2.getDisplayAttributeName());
			}
		}; 
		List<AdhocQueryMetadata> res = hh.getList();
		Collections.sort(res, comp);
		return res;
	}
	
	public String getOptionalitySlotOnAdhocQuery(SlotType1 sl){
		AdhocQueryMetadata aqm = RIMGenerator.aqWeak.get(this.adhocQuery);
		SlotMetadata slm = RIMGenerator.slWeak.get(sl);
		if (aqm.getSlot_required() != null){
			for (SlotMetadata ee : aqm.getSlot_required()) {
				if (slm.getName().equals(ee.getName())) return "R";
			}
		}
		return "O";
	}
	
	public void removeSlotFromAdhocQuery(SlotType1 sl){
		this.adhocQuery.getSlot().remove(sl);
		this.initListOptionalMetadata();
	}
	
	public void initListOptionalMetadata(){
		EntityManager em = (EntityManager) Component.getInstance("entityManager");
		this.listOptionalMetadata = new ArrayList<SelectItem>();
		this.listOptionalMetadata.add(new SelectItem(null, "please select.."));
		AdhocQueryMetadata ss = null;
		ss = RIMGenerator.aqWeak.get(this.adhocQuery);
		ss = em.find(AdhocQueryMetadata.class, ss.getId());
		if (ss.getSlot_optional() != null){
			for (SlotMetadata slm : ss.getSlot_optional()) {
				boolean isused = false;
				for (SlotType1 sl : this.adhocQuery.getSlot()) {
					if (sl.getName().equals(slm.getName())){
						isused = true;
					}
				}
				if (!isused || (slm.getSupportAndOr()!= null && slm.getSupportAndOr())){
					this.listOptionalMetadata.add(new SelectItem(slm.getId(), slm.getName()));
				}
			}
		}
		
		if (ss.getSlot_required() != null){
			for (SlotMetadata slm : ss.getSlot_required()) {
				if (slm.getSupportAndOr()!= null && slm.getSupportAndOr()){
					this.listOptionalMetadata.add(new SelectItem(slm.getId(), slm.getName()));
				}
			}
		}
		this.selectedMetadata = null;
	}
	
	public boolean canAddValueToSlot(SlotType1 sl){
		if (sl != null){
			SlotMetadata slm = RIMGenerator.slWeak.get(sl);
			if (slm != null){
				if ((slm.getMultiple() != null) && (slm.getMultiple() == false)){
					if ((sl.getValueList() != null)&& (sl.getValueList().getValue().size()>0)){
						return false;
					}
				}
			}
		}
		return true;
	}
	
	public void initSelectedSlotType(SlotType1 sl){
		this.selectedSlotType = sl;
		this.valueToAdd = null;
	}
	
	public void initAdhocQueryByMessageType(){
		if (this.selectedMessageType!= null){
			this.adhocQuery = generateAdhocQueryType();
			this.initListOptionalMetadata();
			this.displayResultPanel = false;
		}
	}
	
	private AdhocQueryType generateAdhocQueryType(){
		AdhocQueryType res = new AdhocQueryType();
		RIMGenerator.generateAdhocQueryTypeChild(this.selectedMessageType, res);
		return res;
	}
	
	public String previewMessage(){
		String res = this.createMessage();
		return res;
	}
	
	public abstract String createMessage();
	
	protected String getAdhocQueryRequestTypeAsString(AdhocQueryRequestType aqr){
		try{
			JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.query");
			Marshaller m = jc.createMarshaller();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new XDSNamespacePrefixMapper());
			m.marshal(aqr, baos);
			return baos.toString();
		}
		catch(Exception e){
			return null;
		}
	}
	
	public void addNewMetadataToSubmissionSet(){
		if (this.selectedMetadata != null){
			SlotType1 sl = RIMGenerator.generateSlot(this.selectedMetadata);
			this.adhocQuery.getSlot().add(sl);
		}
		this.initListOptionalMetadata();
		this.selectedMetadata = null;
		this.selectedSlotMetadataId = null;
	}
	
	public List<SelectItem> listValueSet(){
		List<SelectItem> res = new ArrayList<SelectItem>();
		res.add(new SelectItem(null, "please select .."));
		SlotMetadata slm = RIMGenerator.slWeak.get(this.selectedSlotType);
		if ((slm.getValueset() != null)&& (!slm.getValueset().equals(""))){
			String[] listValueSet = slm.getValueset().split("\\s*,\\s*");
			for (String valueSet : listValueSet) {
				List<Concept> lc = ValueSetContainer.getListConcepts().get(valueSet);
				if (lc == null){
					lc = SVSConsumerForSimulator.getConceptsListFromValueSet(valueSet, null);
					Collections.sort(lc);
					ValueSetContainer.getListConcepts().put(valueSet, lc);
				}
				if (lc != null){
					class CompareConcept implements Comparator<Concept>{
	
						@Override
						public int compare(Concept o1, Concept o2) {
							if (o1.getCode() == null) return (-1);
							if (o2.getCode() == null) return 1;
							return o1.getCode().compareTo(o2.getCode());
						}
						
					}
					Collections.sort(lc, new CompareConcept());
					for (Concept concept : lc) {
						String display = concept.getCode() + "^^" + concept.getCodeSystem()  + "(" + concept.getDisplayName() + ")";
						String display2 = StringUtils.substring(display, 0, 50);
						if (display2.length()<display.length()) display2 = display2 + "...";
						SelectItem si = new SelectItem(concept.getCode() + "^^" + concept.getCodeSystem(), display2);
						res.add(si);
					}
				}
			}
		}
		return res;
	}
	
	public Boolean selectedSlotHasValueSets(){
		if (this.selectedSlotType != null){
			SlotMetadata slm = RIMGenerator.slWeak.get(this.selectedSlotType);
			if ((slm.getValueset() != null)&& (!slm.getValueset().equals(""))){
				return true;
			}
		}
		return false;
	}
	
	public List<SelectItem> getListOfReturnTypes() {
		List<SelectItem> availableReturnTypes = new ArrayList<SelectItem>();
		EntityManager em = (EntityManager)Component.getInstance("entityManager");
		this.selectedMessageType = em.find(AdhocQueryMetadata.class, this.selectedMessageType.getId());
		for (ReturnTypeType rtt : this.selectedMessageType.getReturnTypes()) {
			availableReturnTypes.add(new SelectItem(rtt, rtt.value()));
		}
		return availableReturnTypes;
	}
	
	public void addNewValueToSelectedSlot(){
	    if (this.selectedSlotType != null && this.selectedSlotType.getValueList() != null && this.selectedSlotType.getValueList().getValue() != null 
		    && this.valueToAdd != null){
		this.selectedSlotType.getValueList().getValue().add(this.valueToAdd);
	    }
	}
	
}
