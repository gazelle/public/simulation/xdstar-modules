package net.ihe.gazelle.xdstar.validator;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.ValidationEventLocator;

import net.ihe.gazelle.lcm.RemoveObjectsRequestType;
import net.ihe.gazelle.lcm.SubmitObjectsRequestType;
import net.ihe.gazelle.query.AdhocQueryRequestType;
import net.ihe.gazelle.query.AdhocQueryResponseType;
import net.ihe.gazelle.rs.RegistryResponseType;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.xds.ProvideAndRegisterDocumentSetRequestType;
import net.ihe.gazelle.xds.RetrieveDocumentSetRequestType;
import net.ihe.gazelle.xds.RetrieveDocumentSetResponseType;
import net.ihe.gazelle.xdsi.RetrieveImagingDocumentSetRequestType;

public class MetadataExtractor {
	
	private static final String PARSING = "parsing";
	private static final String ALL_THE_DOCUMENT = "all the document";
	private static final String THE_KIND_OF_EXCEPTION_MSG = "Errors when trying to parse the document, the kind of exception : ";
	private static final String ERRORS_WHEN_TRYING_TO_PARSE_THE_DOCUMENT = "Errors when trying to parse the document";
	private static final String UTF8 = "UTF-8";
	
	private MetadataExtractor(){}
	
	static AdhocQueryRequestType getAdhocQueryRequestTypeFromXml(String xml) throws ValidatorException{
		final ValidatorException vexp = new ValidatorException(ERRORS_WHEN_TRYING_TO_PARSE_THE_DOCUMENT, new ArrayList<Notification>());
		try {
			ByteArrayInputStream is = null;
			is = new ByteArrayInputStream(xml.getBytes(UTF8));
			JAXBContext jc = JAXBContext.newInstance(AdhocQueryRequestType.class);
			Unmarshaller u = jc.createUnmarshaller();
			u.setEventHandler(new ValidationEventHandler() {
				
				@Override
				public boolean handleEvent(ValidationEvent event) {
					 return MetadataExtractor.handleEvent(event, vexp);
				}
			});
			return (AdhocQueryRequestType) u.unmarshal(is);
		}
		catch(JAXBException e){
			throw vexp;
		}
		catch(Exception e){
			Notification not = new Error();
			not.setDescription(THE_KIND_OF_EXCEPTION_MSG + e.getClass().getName() + ". " + e.getMessage());
			not.setLocation(ALL_THE_DOCUMENT);
			not.setTest(PARSING);
			vexp.getDiagnostic().add(not);
			throw vexp;
		}
	}
	
	public static AdhocQueryResponseType getAdhocQueryResponseTypeFromXml(String xml) throws ValidatorException{
		final ValidatorException vexp = new ValidatorException(ERRORS_WHEN_TRYING_TO_PARSE_THE_DOCUMENT, new ArrayList<Notification>());
		try {
			ByteArrayInputStream is = null;
			is = new ByteArrayInputStream(xml.getBytes(UTF8));
			JAXBContext jc = JAXBContext.newInstance(AdhocQueryResponseType.class);
			Unmarshaller u = jc.createUnmarshaller();
			u.setEventHandler(new ValidationEventHandler() {
				
				@Override
				public boolean handleEvent(ValidationEvent event) {
					 return MetadataExtractor.handleEvent(event, vexp);
				}
			});
			return (AdhocQueryResponseType) u.unmarshal(is);
		}
		catch(JAXBException e){
			throw vexp;
		}
		catch(Exception e){
			Notification not = new Error();
			not.setDescription(THE_KIND_OF_EXCEPTION_MSG + e.getClass().getName() + ". " + e.getMessage());
			not.setLocation(ALL_THE_DOCUMENT);
			not.setTest(PARSING);
			vexp.getDiagnostic().add(not);
			throw vexp;
		}
	}
	
	static SubmitObjectsRequestType getSubmitObjectsRequestTypeFromXml(String xml) throws ValidatorException{
		final ValidatorException vexp = new ValidatorException(ERRORS_WHEN_TRYING_TO_PARSE_THE_DOCUMENT, new ArrayList<Notification>());
		try {
			ByteArrayInputStream is = null;
			is = new ByteArrayInputStream(xml.getBytes(UTF8));
			JAXBContext jc = JAXBContext.newInstance(SubmitObjectsRequestType.class);
			Unmarshaller u = jc.createUnmarshaller();
			u.setEventHandler(new ValidationEventHandler() {
				
				@Override
				public boolean handleEvent(ValidationEvent event) {
					 return MetadataExtractor.handleEvent(event, vexp);
				}
			});
			return (SubmitObjectsRequestType) u.unmarshal(is);
		}
		catch(JAXBException e){
			throw vexp;
		}
		catch(Exception e){
			Notification not = new Error();
			not.setDescription(THE_KIND_OF_EXCEPTION_MSG + e.getClass().getName() + ". " + e.getMessage());
			not.setLocation(ALL_THE_DOCUMENT);
			not.setTest(PARSING);
			vexp.getDiagnostic().add(not);
			throw vexp;
		}
	}
	
	static ProvideAndRegisterDocumentSetRequestType getProvideAndRegisterDocumentSetRequestTypeFromXml(String xml) throws ValidatorException{
		final ValidatorException vexp = new ValidatorException(ERRORS_WHEN_TRYING_TO_PARSE_THE_DOCUMENT, new ArrayList<Notification>());
		try {
			ByteArrayInputStream is = null;
			is = new ByteArrayInputStream(xml.getBytes(UTF8));
			JAXBContext jc = JAXBContext.newInstance(ProvideAndRegisterDocumentSetRequestType.class);
			Unmarshaller u = jc.createUnmarshaller();
			u.setEventHandler(new ValidationEventHandler() {
				
				@Override
				public boolean handleEvent(ValidationEvent event) {
					 return MetadataExtractor.handleEvent(event, vexp);
				}
			});
			return (ProvideAndRegisterDocumentSetRequestType) u.unmarshal(is);
		}
		catch(JAXBException e){
			throw vexp;
		}
		catch(Exception e){
			Notification not = new Error();
			not.setDescription(THE_KIND_OF_EXCEPTION_MSG + e.getClass().getName() + ". " + e.getMessage());
			not.setLocation(ALL_THE_DOCUMENT);
			not.setTest(PARSING);
			vexp.getDiagnostic().add(not);
			throw vexp;
		}
	}
	
	static RetrieveDocumentSetRequestType getRetrieveDocumentSetRequestTypeFromXml(String xml) throws ValidatorException{
		final ValidatorException vexp = new ValidatorException(ERRORS_WHEN_TRYING_TO_PARSE_THE_DOCUMENT, new ArrayList<Notification>());
		try {
			ByteArrayInputStream is = null;
			is = new ByteArrayInputStream(xml.getBytes(UTF8));
			JAXBContext jc = JAXBContext.newInstance(RetrieveDocumentSetRequestType.class);
			Unmarshaller u = jc.createUnmarshaller();
			u.setEventHandler(new ValidationEventHandler() {
				
				@Override
				public boolean handleEvent(ValidationEvent event) {
					 return MetadataExtractor.handleEvent(event, vexp);
				}
			});
			return (RetrieveDocumentSetRequestType) u.unmarshal(is);
		}
		catch(JAXBException e){
			throw vexp;
		}
		catch(Exception e){
			Notification not = new Error();
			not.setDescription(THE_KIND_OF_EXCEPTION_MSG + e.getClass().getName() + ". " + e.getMessage());
			not.setLocation(ALL_THE_DOCUMENT);
			not.setTest(PARSING);
			vexp.getDiagnostic().add(not);
			throw vexp;
		}
	}
	
	static RetrieveDocumentSetResponseType getRetrieveDocumentSetResponseTypeFromXml(String xml) throws ValidatorException{
		final ValidatorException vexp = new ValidatorException(ERRORS_WHEN_TRYING_TO_PARSE_THE_DOCUMENT, new ArrayList<Notification>());
		try {
			ByteArrayInputStream is = null;
			is = new ByteArrayInputStream(xml.getBytes(UTF8));
			JAXBContext jc = JAXBContext.newInstance(RetrieveDocumentSetResponseType.class);
			Unmarshaller u = jc.createUnmarshaller();
			u.setEventHandler(new ValidationEventHandler() {
				
				@Override
				public boolean handleEvent(ValidationEvent event) {
					 return MetadataExtractor.handleEvent(event, vexp);
				}
			});
			return (RetrieveDocumentSetResponseType) u.unmarshal(is);
		}
		catch(JAXBException e){
			throw vexp;
		}
		catch(Exception e){
			Notification not = new Error();
			not.setDescription(THE_KIND_OF_EXCEPTION_MSG + e.getClass().getName() + ". " + e.getMessage());
			not.setLocation(ALL_THE_DOCUMENT);
			not.setTest(PARSING);
			vexp.getDiagnostic().add(not);
			throw vexp;
		}
	}
	
	static RegistryResponseType getRegistryResponseTypeFromXml(String xml) throws ValidatorException{
		final ValidatorException vexp = new ValidatorException(ERRORS_WHEN_TRYING_TO_PARSE_THE_DOCUMENT, new ArrayList<Notification>());
		try {
			ByteArrayInputStream is = null;
			is = new ByteArrayInputStream(xml.getBytes(UTF8));
			JAXBContext jc = JAXBContext.newInstance(RegistryResponseType.class);
			Unmarshaller u = jc.createUnmarshaller();
			u.setEventHandler(new ValidationEventHandler() {
				
				@Override
				public boolean handleEvent(ValidationEvent event) {
					 return MetadataExtractor.handleEvent(event, vexp);
				}
			});
			return (RegistryResponseType) u.unmarshal(is);
		}
		catch(JAXBException e){
			throw vexp;
		}
		catch(Exception e){
			Notification not = new Error();
			not.setDescription(THE_KIND_OF_EXCEPTION_MSG + e.getClass().getName() + ". " + e.getMessage());
			not.setLocation(ALL_THE_DOCUMENT);
			not.setTest(PARSING);
			vexp.getDiagnostic().add(not);
			throw vexp;
		}
	}
	
	static RemoveObjectsRequestType getRemoveObjectsRequestTypeFromXml(String xml) throws ValidatorException{
		final ValidatorException vexp = new ValidatorException(ERRORS_WHEN_TRYING_TO_PARSE_THE_DOCUMENT, new ArrayList<Notification>());
		try {
			ByteArrayInputStream is = null;
			is = new ByteArrayInputStream(xml.getBytes(UTF8));
			JAXBContext jc = JAXBContext.newInstance(RemoveObjectsRequestType.class);
			Unmarshaller u = jc.createUnmarshaller();
			u.setEventHandler(new ValidationEventHandler() {
				
				@Override
				public boolean handleEvent(ValidationEvent event) {
					 return MetadataExtractor.handleEvent(event, vexp);
				}
			});
			return (RemoveObjectsRequestType) u.unmarshal(is);
		}
		catch(JAXBException e){
			throw vexp;
		}
		catch(Exception e){
			Notification not = new Error();
			not.setDescription(THE_KIND_OF_EXCEPTION_MSG + e.getClass().getName() + ". " + e.getMessage());
			not.setLocation(ALL_THE_DOCUMENT);
			not.setTest(PARSING);
			vexp.getDiagnostic().add(not);
			throw vexp;
		}
	}
	
	static RetrieveImagingDocumentSetRequestType getRetrieveImagingDocumentSetRequestTypeFromXml(String xml) throws ValidatorException{
		final ValidatorException vexp = new ValidatorException(ERRORS_WHEN_TRYING_TO_PARSE_THE_DOCUMENT, new ArrayList<Notification>());
		try {
			ByteArrayInputStream is = null;
			is = new ByteArrayInputStream(xml.getBytes(UTF8));
			JAXBContext jc = JAXBContext.newInstance(RetrieveImagingDocumentSetRequestType.class);
			Unmarshaller u = jc.createUnmarshaller();
			u.setEventHandler(new ValidationEventHandler() {
				
				@Override
				public boolean handleEvent(ValidationEvent event) {
					 return MetadataExtractor.handleEvent(event, vexp);
				}
			});
			return (RetrieveImagingDocumentSetRequestType) u.unmarshal(is);
		}
		catch(JAXBException e){
			throw vexp;
		}
		catch(Exception e){
			Notification not = new Error();
			not.setDescription(THE_KIND_OF_EXCEPTION_MSG + e.getClass().getName() + ". " + e.getMessage());
			not.setLocation(ALL_THE_DOCUMENT);
			not.setTest(PARSING);
			vexp.getDiagnostic().add(not);
			throw vexp;
		}
	}
	
	protected static boolean handleEvent(ValidationEvent event, ValidatorException vexp) {
		 if (event.getSeverity() != ValidationEvent.WARNING) {  
            ValidationEventLocator vel = event.getLocator();
            Notification not = new net.ihe.gazelle.validation.Error();
            not.setDescription("Line:Col[" + vel.getLineNumber() +  
                    ":" + vel.getColumnNumber() +  
                    "]:" +event.getMessage());
            not.setTest("message_parsing");
            not.setLocation("All the document");
            vexp.getDiagnostic().add(not);
        }  
        return true; 
	}


}
