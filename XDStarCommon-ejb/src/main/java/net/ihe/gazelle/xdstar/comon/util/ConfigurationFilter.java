package net.ihe.gazelle.xdstar.comon.util;

import net.ihe.gazelle.hql.HQLQueryBuilderInterface;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.simulator.sut.model.SystemConfigurationAttributes;

import org.jboss.seam.security.Identity;

public class ConfigurationFilter {
	
	private ConfigurationFilter() {}
	
	public static void filterConfigurationQueryAccodingToUserCredential(SystemConfigurationAttributes<? extends SystemConfiguration> quer){
		if (Identity.instance().hasRole("admin_role")){

		}
		 else if (!Identity.instance().isLoggedIn()){
			 quer.isPublic().eq(true);
		 }
		 else {
			 
			 ((HQLQueryBuilderInterface)quer).addRestriction(
					 HQLRestrictions.or(
							 HQLRestrictions.eq("owner", Identity.instance().getCredentials().getUsername()), 
							 HQLRestrictions.eq("isPublic", true)
							 )
					 );
		 }
	}

}
