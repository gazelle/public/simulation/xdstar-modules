package net.ihe.gazelle.xdstar.comon.util;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;


/**
 * @class Util
 * @author abderrazek boufahja
 */
public final class Util {

    public static final String UTF_8 = "UTF-8";

    private Util() {
    }


    private static Logger log = LoggerFactory.getLogger(Util.class);

    public static org.dom4j.Document stringToXmlTransformerDocument(String inString) {
        org.dom4j.Document document = null;
        try {
            document = DocumentHelper.parseText(inString);
            return document;
        } catch (DocumentException e) {
            log.error("error to parse XML", e);
            return null;
        }
    }

    /**
     * @param inString
     * @return
     */
    public static Document stringToXmlTransformer(String inString) {
        if ((inString == null) || (inString.length() == 0)) {
            return null;
        }
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new InputSource(new StringReader(inString)));
            return document;
        } catch (Exception e) {
            System.out.println("cannot convert String to XML");
            return null;
        }
    }

    /**
     * @param input
     * @return
     */
    public static String prettyFormat(String input) {
        try {
            Source xmlInput = new StreamSource(new StringReader(input));
            StringWriter stringWriter = new StringWriter();
            StreamResult output = new StreamResult(stringWriter);
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", String.valueOf(4));
            transformer.setOutputProperty(OutputKeys.ENCODING, UTF_8);
            transformer.transform(xmlInput, output);
            return output.getWriter().toString();
        } catch (Exception e) {
            log.error("error when converting XML to String: ", e);
            return input;
        }
    }

    /**
     * @param documentContent
     * @return
     */
    public static String transformXmlToHtml(String documentContent, String xslPath) {
        if (documentContent == null || documentContent.length() == 0) {
            return null;
        }

        try {
            if (xslPath == null || xslPath.length() == 0) {
                return null;
            }

            Source xmlInput = new StreamSource(new StringReader(documentContent));
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer(new javax.xml.transform.stream.StreamSource(xslPath));
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            StreamResult out = new StreamResult(baos);
            transformer.transform(xmlInput, out);
            String tmp = new String(baos.toByteArray());
            return tmp;
        } catch (Exception e) {
            log.error("error when transformXmlToHtml: ", e);
            return "The document cannot be displayed using this stylesheet";
        }
    }

    public static String getDate() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMddhhmmss", Locale.ENGLISH);
        return sdfDate.format(new Date());
    }

    public static Document string2DOM(String s) {
        if (s == null) {
            return null;
        }
        Document tmpX = null;
        DocumentBuilder builder = null;
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (javax.xml.parsers.ParserConfigurationException error) {
            log.error("error in string2DOM: ", error);
            return null;
        }
        try {
            tmpX = (Document) builder.parse(new ByteArrayInputStream(s.getBytes(StandardCharsets.UTF_8)));
        } catch (org.xml.sax.SAXException error) {
            log.error("error in string2DOM: ", error);
            return null;
        } catch (IOException error) {
            log.error("error in string2DOM: ", error);
            return null;
        }
        return tmpX;
    }

}
