package net.ihe.gazelle.xdstar.comon.util;

import java.util.UUID;

import net.ihe.gazelle.rim.AssociationType1;
import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.InternationalStringType;
import net.ihe.gazelle.rim.LocalizedStringType;
import net.ihe.gazelle.rim.RegistryPackageType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.rim.ValueListType;

public class RIMBuilderCommon {
	
	public static SlotType1 createSlot(String name, String slotValue)
	{
		SlotType1 sl = new SlotType1();
		sl.setName(name);
		sl.setValueList(new ValueListType());
		sl.getValueList().getValue().add(slotValue);
		return sl;
	}
	
	public static RegistryPackageType createRegistryPackage(String id, String objectTypeUUID)
	{
		RegistryPackageType registryPackage = new RegistryPackageType();
		registryPackage.setId(id);
		registryPackage.setObjectType(objectTypeUUID);
		return registryPackage;
	}
	
	public static AssociationType1 createAssociation(String associationType, String source, String target, String slotName, String slotValue, String name)
	{
		AssociationType1 association = new AssociationType1();
		association.setAssociationType(associationType);
		association.setSourceObject(source);
		association.setTargetObject(target);
		association.setId(UUID.randomUUID().toString());
		if (slotName != null) association.getSlot().add(RIMBuilderCommon.createSlot(slotName, slotValue));
		if (name != null) association.setName(createNameOrDescription(name));
		return association;
	}	
	
	public static ExternalIdentifierType createExternalIdentifierType(String name, String identificationScheme, String registryObject, String value){
		ExternalIdentifierType res = new ExternalIdentifierType();
		res.setId(UUID.randomUUID().toString());
		res.setIdentificationScheme(identificationScheme);
		res.setValue(value);
		res.setRegistryObject(registryObject);
		res.setObjectType("urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier");
		res.setName(createNameOrDescription(name));
		return res;
	}
	
	/**
	 * 
	 * @param request
	 * @param elementName: Name or Description
	 * @param documentName
	 * @return
	 */
	public static InternationalStringType createNameOrDescription(String documentName)
	{
		InternationalStringType element = new InternationalStringType();
		if ((documentName != null) && (!documentName.equals(""))){
			LocalizedStringType ll = new LocalizedStringType();
			ll.setValue(documentName);
			element.getLocalizedString().add(ll);
		}
		return element;
	}
	
	public static ClassificationType createClassification(String classificationScheme, String classifiedObject, String nodeRepresentation){
		ClassificationType cl = new ClassificationType();
		cl.setClassificationScheme(classificationScheme);
		cl.setClassifiedObject(classifiedObject);
		cl.setNodeRepresentation(nodeRepresentation);
		cl.setId(UUID.randomUUID().toString());
		return cl;
	}
	

}
