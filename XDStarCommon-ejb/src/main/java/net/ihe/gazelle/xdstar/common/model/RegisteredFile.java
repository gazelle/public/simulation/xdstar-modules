/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.common.model;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

/**
 *  <b>Class Description :  </b>RegisteredFile<br><br>
 *  RegisteredFile represents the file provided to the receiver and registered by it
 *  
 *  RegisteredFile contains the following attributes
 *  <li>
 *  	<ul><b>id</b>id in the database</ul>
 *  	<ul><b>name</b> file name</ul>
 *  	<ul><b>type</b> file type (xml, pdf)</ul>
 *  	<ul><b>path</b> file path on the file system (directory where files are stored is given in the application preferences registered_files_directory)</ul>
 *  </li>
 *  
 * @class			RegisteredFile
 * @package			net.ihe.gazelle.simulator.xdrsrc.model
 * @author 			Abderrazek Boufahja / IHE Europe
 * @author			Anne-Gaëlle Bergé / INRIA Rennes IHE development Project
 * @version			1.0 - 2011, January 26th
 *
 */

@Entity
@Name("registeredFile")
@Table(name="registered_file", schema="public", uniqueConstraints=@UniqueConstraint(columnNames="id"))
@SequenceGenerator(name="registered_file_sequence", sequenceName="registered_file_id_seq", allocationSize=1)
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("RF")
public class RegisteredFile implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Logger
	private static Log log;

	@Id
	@GeneratedValue(generator="registered_file_sequence", strategy=GenerationType.SEQUENCE)
	@NotNull
	@Column(name="id", nullable=false, unique=true)
	private Integer id;

	@Column(name="name")
	private String name;

	@Column(name="path")
	private String path;

	@Column(name="type")
	private String type;

	//////////////////////////////////////////////////////////////////////////
	// Constructors

	public RegisteredFile(){

	}

	public RegisteredFile (String fileName, String fileType)
	{
		this.name = fileName;
		this.type = fileType;
	}

	//////////////////////////////////////////////////////////////////////////
	// Getters and Setters

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	/**
	 * Stores, on the file system, a file to send to the receiver and create a pointer (RegisteredFile object) in the database
	 * @param fileName: name of the uploaded file
	 * @param fileContent: file content
	 * @param fileType: file type (xml, pdf)
	 * @return the relative pointer
	 */
	public static RegisteredFile saveFile(String fileName, String fileType) throws IOException
	{
		String absolutePath = ApplicationConfiguration.getValueOfVariable("registered_files_directory");
		if (absolutePath != null && !absolutePath.isEmpty())
		{
			EntityManager em = (EntityManager) Component.getInstance("entityManager");
			RegisteredFile registeredFile = new RegisteredFile(fileName, fileType);
			registeredFile = em.merge(registeredFile);
			em.flush();
			String extension = "";
			if (fileType.contains("xml")) {
				extension = ".xml";
			}
			else if (fileType.contains("pdf")) {
				extension = ".pdf";
			}
			else {
				extension = "";
			}
			absolutePath = absolutePath + "/file" + registeredFile.getId().toString() + extension;
			registeredFile.setPath(absolutePath);
			registeredFile = em.merge(registeredFile);
			em.flush();
			
			return registeredFile;
		}
		else
		{
			log.error("registered_files_directory is not set in app_configuration table");
			return null;
		}
	}

	/**
	 * Deletes the file on the file system and its reference in the database
	 * @param fileToDelete
	 * @return
	 */
	public static boolean deleteFile (RegisteredFile fileToDelete)
	{
		if (fileToDelete != null)
		{
			File file = new File(fileToDelete.getPath());
			if (file.exists())
			{
				if (file.delete())
				{
					EntityManager em = (EntityManager) Component.getInstance("entityManager");
					em.remove(fileToDelete);
					em.flush();
					return true;
				}
				else
				{
					log.error("Unable to delete file at " + fileToDelete.getPath());
					return false;
				}
			}
			else
			{
				log.error(fileToDelete.getPath() + " not found in file system");
				return false;
			}					
		}
		return false;
	}

	/////////////////////////////////////////////////////////////////////
	// Hash Code and Equals


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RegisteredFile other = (RegisteredFile) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (path == null) {
			if (other.path != null) {
				return false;
			}
		} else if (!path.equals(other.path)) {
			return false;
		}
		if (type == null) {
			if (other.type != null) {
				return false;
			}
		} else if (!type.equals(other.type)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "RegisteredFile [id=" + id + ", name=" + name + ", path=" + path
				+ ", type=" + type + "]";
	}
	
	

}
