package net.ihe.gazelle.xdstar.filter;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.xdstar.common.model.AbstractMessage;
import net.ihe.gazelle.xdstar.common.model.AbstractMessageAttributes;
import net.ihe.gazelle.xdstar.common.model.AbstractMessageQuery;
import net.ihe.gazelle.xdstar.users.action.UserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Map;

public class MessageDataModel extends FilterDataModel<AbstractMessage> {

    private static final Logger LOG = LoggerFactory.getLogger(MessageDataModel.class);
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String creator;

    private Date startDate;

    private AffinityDomain affinityDomain;

    private Transaction transaction;

    private String messageType;

    private SystemConfiguration configuration;

    private Boolean validRequest;

    private Boolean validResponse;

    private Integer id;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AffinityDomain getAffinityDomain() {
        return affinityDomain;
    }

    public void setAffinityDomain(AffinityDomain affinityDomain) {
        this.affinityDomain = affinityDomain;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public SystemConfiguration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(SystemConfiguration configuration) {
        this.configuration = configuration;
    }

    public Boolean getValidRequest() {
        return validRequest;
    }

    public void setValidRequest(Boolean validRequest) {
        this.validRequest = validRequest;
    }

    public Boolean getValidResponse() {
        return validResponse;
    }

    public void setValidResponse(Boolean validResponse) {
        this.validResponse = validResponse;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


    private Date endDate;

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public MessageDataModel() {
        super(new Filter<AbstractMessage>(getCriterionList()));
        if (getFilter().getPossibleValues("public").size() == 1) {
            LOG.info("filter values public : " + getFilter().getPossibleValues("public"));
            getFilter().getFilterValues().put("public", getFilter().getPossibleValues("public").get(0));
        }
        if (getFilter().getPossibleValues("owner").size() == 1) {
            LOG.info("filter values owner : " + getFilter().getPossibleValues("owner"));
            getFilter().getFilterValues().put("owner", getFilter().getPossibleValues("owner").get(0));
        }
    }

    private static HQLCriterionsForFilter<AbstractMessage> getCriterionList() {
        final UserManager userManager = new UserManager();
        final AbstractMessageQuery query = new AbstractMessageQuery();
        HQLCriterionsForFilter<AbstractMessage> result = query.getHQLCriterionsForFilter();
        result.addQueryModifier(new QueryModifier<AbstractMessage>() {
            @Override
            public void modifyQuery(HQLQueryBuilder<AbstractMessage> hqlQueryBuilder, Map<String, Object> map) {
                AbstractMessageQuery q = new AbstractMessageQuery();
                if (!userManager.userIsLoggedIn()) {
                    hqlQueryBuilder.addRestriction(q.configuration().isPublic().eqRestriction(true));
                }
                if (!userManager.getLoggedInUsername().isEmpty() && !(userManager.userIsAdmin() || userManager.userIsMonitor())) {
                    q.configuration().owner().eq(userManager.getLoggedInUsername());
                    if (q.getCount() > 0) {
                        hqlQueryBuilder.addRestriction(HQLRestrictions.or(q.configuration().owner().eqRestriction(userManager.getLoggedInUsername()
                        ), q.configuration().isPublic().eqRestriction(true)));
                    }
                }
            }
        });
        addTestCriterions(result, query, true);
        return result;
    }

    public static void addTestCriterions(HQLCriterionsForFilter<AbstractMessage> result,
                                         AbstractMessageAttributes<AbstractMessage> messAttributes,
                                         boolean onlyConnectathon) {
        result.addPath("affinityDomain", messAttributes.affinityDomain());
        result.addPath("transaction", messAttributes.transaction());
        result.addPath("messageType", messAttributes.messageType());
        result.addPath("configuration", messAttributes.configuration());
        result.addPath("validRequest", messAttributes.sentMessageContent().validationResult());
        result.addPath("validResponse", messAttributes.receivedMessageContent().validationResult());
        result.addPath("date", messAttributes.timeStamp());
        result.addPath("public", messAttributes.configuration().isPublic());
        result.addPath("owner", messAttributes.configuration().owner());
    }

    @Override
    protected Object getId(AbstractMessage t) {
        // TODO Auto-generated method stub
        return t.getId();
    }
}
