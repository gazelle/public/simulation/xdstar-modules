package net.ihe.gazelle.xdstar.core.soap;

import javax.xml.namespace.QName;

public class SOAPConstants {

    /**
     * Element names
     */
    public static final String ATTRIBUTE = "Attribute";
    public static final String ATTRIBUTE_VALUE= "AttributeValue";
    public static final String PURPOSE_OF_USE = "PurposeOfUse";
    public static final String ROLE = "Role";
    public static final String ASSERTION = "Assertion";

    /**
     * Namespaces
     */
    public static final String PROTOCOL_NS = "urn:oasis:names:tc:SAML:2.0:protocol";
    public static final String PROTOCOL_BINDING_NS = "urn:oasis:names:tc:SAML:2.0:bindings:PAOS";
    public static final String HL7_NS = "urn:hl7-org:v3";
    public static final String ASSERTION_NS = "urn:oasis:names:tc:SAML:2.0:assertion";
    public static final String RESOURCE_ID_NS = "urn:oasis:names:tc:xacml:2.0:resource:resource-id";
    public static final String PURPOSE_OF_USE_NS = "urn:oasis:names:tc:xspa:1.0:subject:purposeofuse";
    public static final String ROLE_NS = "urn:oasis:names:tc:xacml:2.0:subject:role";
    public static final String ATTR_NAME_FORMAT_UNSPECIFIED_NS = "urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified";
    public static final String PRINCIPAL_ID_NS = "urn:e-health-suisse:principal-id";
    public static final String PRINCIPAL_NAME_NS = "urn:e-health-suisse:principal-name";
    public static final String WSTRUST_NS = "http://docs.oasis-open.org/ws-sx/ws-trust/200512";
    public static final String XS_STRING = "xs:string";
    public static final String XS_ANY = "xs:anyType";

    /**
     * Namespaces URI
     */
    public static final String XS_URI = "http://www.w3.org/2001/XMLSchema";
    public static final String XSI_URI = "http://www.w3.org/2001/XMLSchema-instance";
    public static final String ISSUE_URI = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue";
    public static final String ADDRESSING_URI = "http://www.w3.org/2005/08/addressing";

    /**
     * Qualified names
     */
    public static final QName HEADER_ACTION_TYPE = new QName(ADDRESSING_URI, "Action", "wsa");
    public static final QName HEADER_MESSAGE_ID_TYPE = new QName(ADDRESSING_URI, "MessageID", "wsa");
    public static final QName HEADER_SECURITY_TYPE = new QName("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security", "wsse");
    public static final QName BODY_ATTRIBUTE_TYPE = new QName(ASSERTION_NS, ATTRIBUTE, "saml2");
    public static final QName BODY_ATTRIBUTE_VALUE_TYPE = new QName(ASSERTION_NS, ATTRIBUTE_VALUE, "saml2");
    public static final QName ATTRIBUTE_TYPE = new QName(XSI_URI, "type", "xsi");

    public SOAPConstants() {
    }
}
