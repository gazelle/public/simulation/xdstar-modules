/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.conf.converter;

import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.persistence.EntityManager;

import net.ihe.gazelle.xdstar.conf.model.ConfigurationType;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

/**
 *  <b>Class Description :  </b>AffinityDomainConverter<br><br>
 * This class is the converter of AffinityDomain
 * 
 * @class                   AffinityDomainConverter.java
 * @package        			net.ihe.gazelle.simulator.xca.initgw.converter
 * @author 					Abderrazek Boufahja / IHE Europe
 * @author                	Anne-Gaëlle Bergé / INRIA Rennes IHE development Project
 * @version                	1.0 - 2010, December 1st
 *
 */ 

@BypassInterceptors
@Name("configurationTypeConverter")
@org.jboss.seam.annotations.faces.Converter(forClass=ConfigurationType.class)
public class ConfigurationTypeConverter implements Serializable, Converter {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if (value != null)
		{
			Integer id = Integer.parseInt(value);
			EntityManager em = (EntityManager) Component.getInstance("entityManager");
			try{
				ConfigurationType affinityDomain = em.find(ConfigurationType.class, id);
				return affinityDomain;
			}catch(NumberFormatException e){
				return null;
			}
		}
		else
			return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object value) {
		if (value instanceof ConfigurationType)
			return ((ConfigurationType)value).getId().toString();
		else
			return null;
	}

}
