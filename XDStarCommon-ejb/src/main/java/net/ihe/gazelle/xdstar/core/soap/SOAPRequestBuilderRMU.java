package net.ihe.gazelle.xdstar.core.soap;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.utils.XmlUtil;
import net.ihe.gazelle.simulator.common.xua.SamlAssertionAttributes;
import net.ihe.gazelle.simulator.common.xua.SamlAssertionSupplier;
import net.ihe.gazelle.simulator.samlassertion.epr.EPRAssertionAttributes;
import net.ihe.gazelle.simulator.samlassertion.epr.EPRAssertionProvider;
import net.ihe.gazelle.xdstar.comon.util.Util;
import net.ihe.gazelle.xdstar.core.SignatureManager;
import net.ihe.gazelle.xdstar.core.XUAType;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.InvalidCanonicalizerException;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.UUID;

public class SOAPRequestBuilderRMU {

    private static org.slf4j.Logger log = LoggerFactory
            .getLogger(SOAPRequestBuilderRMU.class);

    public SOAPRequestBuilderRMU() {
    }

    public String createSOAPMessage(String message, XUAType xuaType,
                                    String subjectNameId, EPRAssertionAttributes attributes,
                                    String receiverUrl, String actionName, String homeCommunityId) throws ParserConfigurationException, net.ihe.gazelle.simulator.samlassertion.common.SignatureException {
        String res = null;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document request = builder.newDocument();
        request.setXmlVersion("1.0");
        request.setXmlStandalone(true);
        Element root = createEnvelope(request);
        Element assertion = null;
        Element trcAssertion = null;

        if (xuaType == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "There is no XUA Type chosen");
        } else if (xuaType.getXuaType().equals("epSOS_assertion")) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "XUA Type not supported");
        } else if (xuaType.getXuaType().equals("ihe_assertion")) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "XUA Type not supported");
        } else if (xuaType.getXuaType().equals("epr_assertion")) {
            assertion = requestEPRAssertion(receiverUrl, subjectNameId, attributes);
        }

        Element header = createSOAPHeader(request, assertion, receiverUrl, actionName, homeCommunityId);
        root.appendChild(header);

        Element body = request.createElement("env:Body");
        //body.setAttribute("xmlns:s", "http://www.w3.org/2003/05/soap-envelope");

        Document pnrDoc = Util.string2DOM(message);
        Node firstDocImportedNode = request.importNode(pnrDoc.getDocumentElement(), true);
        body.appendChild(firstDocImportedNode);
        root.appendChild(body);
        request.appendChild(root);
        try {
            res = XmlUtil.outputDOM(request);
        } catch (CanonicalizationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidCanonicalizerException e) {
            e.printStackTrace();
        }

        return res;
    }

    private static Element createEnvelope(Document request)
    {
        Element root = request.createElement("env:Envelope");
        root.setAttribute("xmlns:env", "http://www.w3.org/2003/05/soap-envelope");
        root.setAttribute("xmlns:a", "http://www.w3.org/2005/08/addressing");
        return root;
    }


    private Element requestEPRAssertion(String appliesTo, String subjectNameId, EPRAssertionAttributes eprAssertionAttributes)
    {
        try{
            EPRAssertionProvider eprAssertionProvider = new EPRAssertionProvider();
            return eprAssertionProvider.getAssertion(appliesTo, subjectNameId, eprAssertionAttributes);
        } catch (Exception e) {
            log.error("exception occure : " + e.getMessage(), e);
            return null;
        }
    }

    private static Element requestEpSOSAssertion(String praticianID, String stsEndpoint, SamlAssertionAttributes samlAssertionAttributes)
    {
        try{
            if (praticianID == null || praticianID.isEmpty()) {
                praticianID = ApplicationConfiguration.getValueOfVariable("default_pratician_id");
            }
            return SamlAssertionSupplier.getAssertion(praticianID, samlAssertionAttributes, SignatureManager.getSignatureUtil());
        }catch(Exception e){
            log.error("exception occure : " + e.getMessage(), e);
            return null;
        }
    }

    private static Element createSOAPHeader(Document request, Element assertion,
                                            String receiverUrl, String actionName, String homeCommunityId)
    {
        Element header = request.createElement("env:Header");

        if (assertion != null)
        {
            Element security = request.createElement("wss:Security");
            security.setAttribute("xmlns:wss", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            Node assertionToken = request.importNode(assertion, true);
            security.appendChild(assertionToken);
            header.appendChild(security);
        }

        /**if (actionName.equals("urn:ihe:iti:2018:RestrictedUpdateDocumentSet")) {
            Element homeCommunityBlock = request.createElement("ihe:homeCommunityBlock");
            homeCommunityBlock.setAttribute("xmlns:ihe","urn:ihe:iti:xdr:2014");
            homeCommunityBlock.setAttribute("env:role","urn:ihe.net:iti:xd:id");
            homeCommunityBlock.setAttribute("env:relay","true");

            Element homeCommunityIdElement = request.createElement("ihe:homeCommunityId");
            homeCommunityIdElement.setTextContent("urn:oid:" + homeCommunityId);

            homeCommunityBlock.appendChild(homeCommunityIdElement);
            header.appendChild(homeCommunityBlock);
         }**/

        Element action = request.createElement("a:Action");
        action.setAttribute("env:mustUnderstand", "1");
        action.setTextContent(actionName);

        header.appendChild(action);

        Element messageID = request.createElement("a:MessageID");
        UUID uuid = UUID.randomUUID();
        messageID.setTextContent("urn:uuid:" + uuid);
        header.appendChild(messageID);

        Element replyTo = request.createElement("a:ReplyTo");
        Element address = request.createElement("a:Address");
        address.setTextContent("http://www.w3.org/2005/08/addressing/anonymous");
        replyTo.appendChild(address);
        header.appendChild(replyTo);

        Element to = request.createElement("a:To");
        to.setAttribute("env:mustUnderstand", "1");
        to.setTextContent(receiverUrl);
        header.appendChild(to);

        return header;
    }
}
