/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.common.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;

import org.jboss.seam.Component;

/**
 * 
 * @class CommonAbstractMessage.java
 * @package net.ihe.gazelle.xdstar.common.model
 * @author abderrazek boufahja
 * 
 * 
 */

@MappedSuperclass
public class CommonAbstractMessage implements Serializable {
	/**
	 * 
	 */
	protected static final long serialVersionUID = 1L;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "time_stamp")
	private Date timeStamp;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "sent_message_content_id")
	private MetadataMessage sentMessageContent;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "received_message_content_id")
	private MetadataMessage receivedMessageContent;

	@Column(name = "message_type")
	protected String messageType;

	@Column(name = "content_type_id")
	private ContentType contentType;

	@ManyToOne(targetEntity=Transaction.class)
	@JoinColumn(name = "transaction_id")
	private Transaction transaction;

	@ManyToOne(targetEntity=AffinityDomain.class)
	@JoinColumn(name = "affinity_domain_id")
	private AffinityDomain affinityDomain;

	public MetadataMessage getSentMessageContent() {
		return sentMessageContent;
	}

	public void setSentMessageContent(MetadataMessage sentMessageContent) {
		this.sentMessageContent = sentMessageContent;
	}

	public MetadataMessage getReceivedMessageContent() {
		return receivedMessageContent;
	}

	public void setReceivedMessageContent(MetadataMessage receivedMessageContent) {
		this.receivedMessageContent = receivedMessageContent;
	}

	public ContentType getContentType() {
		return contentType;
	}

	public void setContentType(ContentType contentType) {
		this.contentType = contentType;
	}

	public void setAffinityDomain(AffinityDomain affinityDomain) {
		this.affinityDomain = affinityDomain;
	}

	public AffinityDomain getAffinityDomain() {
		return affinityDomain;
	}

	public CommonAbstractMessage() {
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	/**
	 * returns all the messages contained in database
	 * 
	 * @return
	 */
	public static <T> List<T> getAllMessages(Class<T> clazz,
			Boolean gazelleDriven) {
		HQLQueryBuilder<T> hh = new HQLQueryBuilder<T>(clazz);
		hh.addEq("gazelleDriven", gazelleDriven);
		hh.addOrder("timeStamp", false);
		return hh.getList();
	}

	/**
	 * 
	 * @param message
	 * @return
	 */
	public static <T extends CommonAbstractMessage> T storeMessage(T message) {
		if (message != null) {
			EntityManager em = (EntityManager) Component
					.getInstance("entityManager");
			message = em.merge(message);
			em.flush();
			return message;
		} else {
			return null;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result)
				+ ((affinityDomain == null) ? 0 : affinityDomain.hashCode());
		result = (prime * result)
				+ ((contentType == null) ? 0 : contentType.hashCode());
		result = (prime * result)
				+ ((messageType == null) ? 0 : messageType.hashCode());
		result = (prime * result)
				+ ((receivedMessageContent == null) ? 0
						: receivedMessageContent.hashCode());
		result = (prime * result)
				+ ((sentMessageContent == null) ? 0 : sentMessageContent
						.hashCode());
		result = (prime * result)
				+ ((timeStamp == null) ? 0 : timeStamp.hashCode());
		result = (prime * result)
				+ ((transaction == null) ? 0 : transaction.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CommonAbstractMessage other = (CommonAbstractMessage) obj;
		if (affinityDomain == null) {
			if (other.affinityDomain != null) {
				return false;
			}
		} else if (!affinityDomain.equals(other.affinityDomain)) {
			return false;
		}
		if (contentType != other.contentType) {
			return false;
		}
		if (messageType == null) {
			if (other.messageType != null) {
				return false;
			}
		} else if (!messageType.equals(other.messageType)) {
			return false;
		}
		if (receivedMessageContent == null) {
			if (other.receivedMessageContent != null) {
				return false;
			}
		} else if (!receivedMessageContent.equals(other.receivedMessageContent)) {
			return false;
		}
		if (sentMessageContent == null) {
			if (other.sentMessageContent != null) {
				return false;
			}
		} else if (!sentMessageContent.equals(other.sentMessageContent)) {
			return false;
		}
		if (timeStamp == null) {
			if (other.timeStamp != null) {
				return false;
			}
		} else if (!timeStamp.equals(other.timeStamp)) {
			return false;
		}
		if (transaction == null) {
			if (other.transaction != null) {
				return false;
			}
		} else if (!transaction.equals(other.transaction)) {
			return false;
		}
		return true;
	}

}