package net.ihe.gazelle.xdstar.drools.util;

public enum XpathComparatorKind {
	
	IN, CONTAIN, EQ, EQSIZE;

}
