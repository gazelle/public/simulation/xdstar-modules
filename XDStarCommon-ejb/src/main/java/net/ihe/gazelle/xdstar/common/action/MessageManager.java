/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.common.action;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.xml.bind.JAXBException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.utils.XmlUtil;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.xdstar.common.model.AbstractMessage;
import net.ihe.gazelle.xdstar.common.model.AttachmentFile;
import net.ihe.gazelle.xdstar.common.model.CommonAbstractMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.RespondingMessage;
import net.ihe.gazelle.xdstar.comon.util.DocumentDownloadTool;
import net.ihe.gazelle.xdstar.comon.util.EVSClientValidator;
import net.ihe.gazelle.xdstar.core.DocumentFileUpload;
import net.ihe.gazelle.xdstar.validator.ExternalValidationTools;
import net.ihe.gazelle.xdstar.validator.GenericMetadataValidator;
import net.ihe.gazelle.xdstar.validator.MetadataValidator;
import net.ihe.gazelle.xdstar.validator.ValidatorPackClass;
import net.ihe.gazelle.xdstar.validator.ws.DetailedResultTransformer;
import net.ihe.gazelle.xdstar.validator.ws.ExternalValidators;
import net.ihe.gazelle.xmltools.xsd.ValidationException;
import net.ihe.gazelle.xmltools.xsd.XMLValidator;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.core.ResourceBundle;
import org.richfaces.event.ItemChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

/**
 *  
 * @author               	abderrazek boufahja
 *
 */

public abstract class MessageManager<T extends CommonAbstractMessage> implements Serializable, MessageManagerLocal<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String FAILED_ELEMENT = "<ValidationTestResult>FAILED</ValidationTestResult>";
	
	private static Logger log = LoggerFactory.getLogger(MessageManager.class);


	protected T selectedMessage;
	protected String stringRequestContent;
	protected String stringResponseContent;
	
	protected String stringRequestValidation;
	protected String stringResponseValidation;
	
	protected String selectedMessageTab = "Request";
	protected String gazelleDriven;

	public String getStringRequestValidation() {
		return stringRequestValidation;
	}


	public void setStringRequestValidation(String stringRequestValidation) {
		this.stringRequestValidation = stringRequestValidation;
	}


	public String getStringResponseValidation() {
		return stringResponseValidation;
	}


	public void setStringResponseValidation(String stringResponseValidation) {
		this.stringResponseValidation = stringResponseValidation;
	}

	public void setSelectedMessage(T selectedMessage) {
		this.selectedMessage = selectedMessage;
	}


	public T getSelectedMessage() {
		return selectedMessage;
	}
	
	public abstract T getReloadedSelectedMessage();


	public void setStringRequestContent(String stringMessageContent) {
		this.stringRequestContent = stringMessageContent;
	}


	public String getStringRequestContent() {
		return stringRequestContent;
	}

	public String getSelectedMessageTab() {
		return selectedMessageTab;
	}


	public void setSelectedMessageTab(String selectedMessageTab) {
		this.selectedMessageTab = selectedMessageTab;
	}


	public String getGazelleDriven() {
		return gazelleDriven;
	}


	public void setGazelleDriven(String gazelleDriven) {
		this.gazelleDriven = gazelleDriven;
	}


	public void setStringResponseContent(String stringResponseContent) {
		this.stringResponseContent = stringResponseContent;
	}


	public String getStringResponseContent() {
		return stringResponseContent;
	}


	@Destroy
	@Remove
	public void destroy() {

	}


	public List<SelectItem> getMessagePossibleOrigins() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem("False", ResourceBundle.instance().getString("gazelle.xca.initgw.WebApplication")));
		items.add(new SelectItem("True",  ResourceBundle.instance().getString("gazelle.xca.initgw.GazelleDriven")));
		return items;
	}

	public void selectMessageToDisplay(T inMessage) {
		if (inMessage != null)
		{
			selectedMessage = inMessage;
			
			if(selectedMessage.getSentMessageContent() != null && 
					selectedMessage.getSentMessageContent().getMessageContent() != null &&
					selectedMessage.getSentMessageContent().getMessageContent().length() > 0)
			{
				stringRequestContent = selectedMessage.getSentMessageContent().getMessageContent();
				stringRequestValidation = selectedMessage.getSentMessageContent().getValidationDetailsAsHtml("1");
			}
			else
			{
				stringRequestContent = "No request stored";
			}
			
			if (selectedMessage.getReceivedMessageContent() != null &&
					selectedMessage.getReceivedMessageContent().getMessageContent() != null &&
					selectedMessage.getReceivedMessageContent().getMessageContent().length() > 0){
				stringResponseContent = selectedMessage.getReceivedMessageContent().getMessageContent();
				stringResponseContent = this.formatString(stringResponseContent);
				stringResponseValidation = selectedMessage.getReceivedMessageContent().getValidationDetailsAsHtml("2");
			}
			else 
			{
				stringResponseContent = "No response stored";	
			}
		}
		else
		{
			stringRequestContent = "No request stored";
			stringResponseContent = "No response stored";
			selectedMessage = null;
		}
	}
	
	private String formatString(String str){
		String res = null;
		if (str != null){
			if (str.indexOf("--MIMEBoundaryurn") == 0){
				String soap = MetadataValidator.extractSoapEnvelope(str);
				String formattedSoap = this.formatXml(soap);
				res = str.replace(soap, formattedSoap);
			}
			else{
				res = this.formatXml(str);
			}
		}
		return res;
	}
	
	private String formatXml(String strxml){
		try {
            Source xmlInput = new StreamSource(new StringReader(strxml));
            StringWriter stringWriter = new StringWriter();
            StreamResult xmlOutput = new StreamResult(stringWriter);
            Transformer transformer = TransformerFactory.newInstance().newTransformer(); 
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            transformer.transform(xmlInput, xmlOutput);
            String res = xmlOutput.getWriter().toString();
            if (res.contains("?>")){
    			res = res.substring(res.indexOf("?>") + 2);
    		}
            return res;
        } catch (Exception e) {
        		log.info("Error when trying to format the message as XML string. formatXml does not work properly.");
                return strxml;
        }
	}


	public void getMessageByActorById(Class<T> tt)
	{
		AbstractMessage.getAllMessages(tt, true);
	}
	
	public abstract void getMessageById();
	
	public void getMessageById(Class<T> tt)
	{
		Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if (params != null && !params.isEmpty())
		{
			String messageIdString = params.get("id");
			Integer messageId = null;
			if (messageIdString != null)
			{
				try{
					messageId = Integer.valueOf(messageIdString);
				}catch(NumberFormatException e){
					messageId = null;
				}
			}
			if (messageId != null){
				EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
				T messageToDisplay = entityManager.find(tt, messageId);
				selectMessageToDisplay(messageToDisplay);
			}
			else{
				selectMessageToDisplay(null);
			}
		}
		
	}
	
	public void selectAndValidate(T inMessage, String panel){
		this.selectMessageToDisplay(inMessage);
		this.selectedMessageTab = panel;
		this.validate(this.selectedMessage);
		EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
		if (selectedMessage.getSentMessageContent() != null) {
			this.selectedMessage.setSentMessageContent(entityManager.merge(selectedMessage.getSentMessageContent()));
		}
		if (this.selectedMessage.getReceivedMessageContent() != null) {
			this.selectedMessage.setReceivedMessageContent(entityManager.merge(selectedMessage.getReceivedMessageContent()));
		}
		entityManager.flush();
	}
	
	public DetailedResult validateMetadataMessage(MetadataMessage inMessage, String transactionKeyword,
			String affinityDomainKeyword, String messageType, Boolean isRequest) {
		
		if (inMessage != null){
			String soapEnvelope = MetadataValidator.extractSoapEnvelope(inMessage.getMessageContent());
			if (soapEnvelope == null || soapEnvelope.trim().equals("")) {
				if (verifyIfMessageContentIsXML(inMessage.getMessageContent())) {
					soapEnvelope = inMessage.getMessageContent();
				}
			}
			if (soapEnvelope != null){
				return validateSoapEnvelope(soapEnvelope, transactionKeyword, affinityDomainKeyword, messageType, isRequest);
			}
		}
		return null;
	}
	
	private boolean verifyIfMessageContentIsXML(String messageContent) {
		try {
			Document doc = XmlUtil.parse(messageContent);
			if (doc != null) {
				return true;
			}
		}
		catch(Exception e) {}
		return false;
	}


	private DetailedResult validateSoapEnvelope(String soapEnvelope, String transactionKeyword,
			String affinityDomainKeyword, String messageType, Boolean isRequest){
//		String type = (messageType!= null) && messageType.contains("V2")?"V2":"V1";
		String val = ValidatorPackClass.getValidatorNameFromDescription(affinityDomainKeyword, transactionKeyword, messageType, isRequest);
		if (val == null) {
			ExternalValidators ext = ExternalValidators.getExternalValidatorFromDescription(affinityDomainKeyword, transactionKeyword, messageType, isRequest);
			if (ext != null) {
				val = ext.getValue();
			}
		}
		return GenericMetadataValidator.validate(soapEnvelope, val);
	}
	
	public void validate(T inMessage) {
		if (inMessage != null){
			validatedSentMessage(inMessage);
			validateReceivedMessage(inMessage);
		}
	}


	protected void validateReceivedMessage(T inMessage) {
		if ((inMessage.getReceivedMessageContent() != null) && (inMessage.getReceivedMessageContent().getMessageContent() != null)){
			DetailedResult validationRequest = this.validateMetadataMessage(inMessage.getReceivedMessageContent(), inMessage.getTransaction().getKeyword(),
					inMessage.getAffinityDomain().getKeyword(), inMessage.getMessageType(), false);
			if (validationRequest != null){
				if (validationRequest.getValidationResultsOverview().getValidationTestResult().equals("PASSED")){
					inMessage.getReceivedMessageContent().setValidationResult(true);
				} else{
					inMessage.getReceivedMessageContent().setValidationResult(false);
				}
				inMessage.getReceivedMessageContent().setValidationDetails(MetadataValidator.getDetailedResultAsString(validationRequest));
			}
			else {
				ExternalValidators val = ExternalValidators.getExternalValidatorFromDescription(inMessage.getAffinityDomain().getKeyword(), inMessage.getTransaction().getKeyword(), null, false);
				if (val != null){
					validateWithExternalValidator(inMessage, val, "Received");
				}
				else{
					inMessage.getReceivedMessageContent().setValidationResult(true);
					inMessage.getReceivedMessageContent().setValidationDetails(
							ExternalValidationTools.createMessageWarning(ExternalValidationTools.NO_VALIDATOR_AVAILABLE_MSG));
				}
			}
		}
	}


	protected void validatedSentMessage(T inMessage) {
		if ((inMessage.getSentMessageContent() != null) && (inMessage.getSentMessageContent().getMessageContent() != null)){
			String transKeyword = null;
			if (inMessage.getTransaction() != null){
				transKeyword = inMessage.getTransaction().getKeyword();
			}
			String affDomain = null;
			if (inMessage.getAffinityDomain() != null){
				affDomain = inMessage.getAffinityDomain().getKeyword();
			}
			DetailedResult validationRequest = this.validateMetadataMessage(inMessage.getSentMessageContent(), transKeyword, affDomain, inMessage.getMessageType(), true);
			if (validationRequest != null){
				if (validationRequest.getValidationResultsOverview().getValidationTestResult().equals("PASSED")){
					inMessage.getSentMessageContent().setValidationResult(true);
				} else{
					inMessage.getSentMessageContent().setValidationResult(false);
				}
				inMessage.getSentMessageContent().setValidationDetails(MetadataValidator.getDetailedResultAsString(validationRequest));
			}
			else {
				ExternalValidators val = ExternalValidators.getExternalValidatorFromDescription(affDomain, transKeyword, null, true);
				if (val != null){
					validateWithExternalValidator(inMessage, val, "Sent");
				}
				else{
					inMessage.getSentMessageContent().setValidationResult(true);
					inMessage.getSentMessageContent().setValidationDetails(
							ExternalValidationTools.createMessageWarning(ExternalValidationTools.NO_VALIDATOR_AVAILABLE_MSG));
				}
			}
		}
	}


	private void validateWithExternalValidator(T inMessage, ExternalValidators val, String sentOrReceived) {
		String res = "";
		try {
			DetailedResult dr = ExternalValidationTools.validateWithExternalValidatorDetailedResult(inMessage, val);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			DetailedResultTransformer.save(baos, dr);
			res = baos.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
			
		MetadataMessage msg = null;
		if (sentOrReceived.equals("Sent")) {
			msg = inMessage.getSentMessageContent();
		}
		else {
			msg = inMessage.getReceivedMessageContent();
		}
		if (res.contains(MessageManager.FAILED_ELEMENT)){
			msg.setValidationResult(false);
		} else{
			msg.setValidationResult(true);
		}
		msg.setValidationDetails(res);
	}
	
	public String getFullPermanentLink(AbstractMessage message) {
		String baseUrl = ApplicationConfiguration.getValueOfVariable("application_url");
		return baseUrl + "/messages/message.seam?id=" + message.getId();
	}
	
	public void validateCurrentMessage(String tabpanel){
		this.selectedMessageTab = tabpanel;
		this.validate(this.selectedMessage);
		EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
		if (selectedMessage.getSentMessageContent()!=null) {
			this.selectedMessage.setSentMessageContent(entityManager.merge(selectedMessage.getSentMessageContent()));
		}
		if (selectedMessage.getReceivedMessageContent()!=null) {
			this.selectedMessage.setReceivedMessageContent(entityManager.merge(selectedMessage.getReceivedMessageContent()));
		}
		entityManager.flush();
		this.selectMessageToDisplay(this.selectedMessage);
	}
	
	public void setSelectedMessageAndPanel(T selectedMessage, String panel){
		this.selectedMessage = selectedMessage;
		this.selectedMessageTab = panel;
		this.selectMessageToDisplay(this.selectedMessage);
	}
	
	public void selectMessageToDisplayAndInitPanel(T inMessage) {
		this.selectMessageToDisplay(inMessage);
		this.selectedMessageTab = "Request";
	}
	
	public void downloadRequest() {
		String fileName = null;
		Integer id = null;
		if (this.selectedMessage instanceof AbstractMessage){
			id = ((AbstractMessage)this.selectedMessage).getId();
		}
		else if (this.selectedMessage instanceof RespondingMessage){
			id = ((RespondingMessage)this.selectedMessage).getId();
		}
		if (this.stringIsXML(this.stringRequestContent)){
			fileName = "request_" + id + ".xml";
		}
		else{
			fileName = "request_" + id + ".txt";
		}
		DocumentDownloadTool.showFile(new ByteArrayInputStream(this.stringRequestContent.getBytes(StandardCharsets.UTF_8)), fileName, true);
	}
	
	public void downloadResponse() {
		String fileName = null;
		Integer id = null;
		if (this.selectedMessage instanceof AbstractMessage){
			id = ((AbstractMessage)this.selectedMessage).getId();
		}
		else if (this.selectedMessage instanceof RespondingMessage){
			id = ((RespondingMessage)this.selectedMessage).getId();
		}
		if (this.stringIsXML(this.stringResponseContent)){
			fileName = "response_" + id + ".xml";
		}
		else{
			fileName = "response_" + id + ".txt";
		}
		DocumentDownloadTool.showFile(new ByteArrayInputStream(this.stringResponseContent.getBytes(StandardCharsets.UTF_8)), fileName, true);
	}
	
	private boolean stringIsXML(String str){
		boolean res = false;
		List<ValidationException> ttt = XMLValidator.validate(new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8)));
		if (ttt == null || ttt.size()==0){
			res = true;
		}
		return res;
	}
	
	public void validateWithEVSClient(String msg){
		EVSClientValidator.validate(msg.getBytes());
	}
	
	public void displayFileOnScreen(AttachmentFile af, boolean download) throws FileNotFoundException{
		DocumentFileUpload.showFile(new File(ApplicationConfiguration.getValueOfVariable("attachement_files_directory") + "/" + af.getId()),
				af.getContentType(), false);
	}
	
	public void changeTabActiveItem(ItemChangeEvent event){
	    System.out.println("changeTabActiveItem");
	    this.setSelectedMessageTab(event.getNewItemName());
	 }

	
}
