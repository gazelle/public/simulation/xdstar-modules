package net.ihe.gazelle.xdstar.drools.util;

import java.util.ArrayList;
import java.util.List;

public class DroolsDocuments {
	
	private String documentToValidate;
	
	private List<String> documentReferences;
	
	public DroolsDocuments() {
		this.documentReferences = new ArrayList<String>();
	}
	
	public DroolsDocuments(String documentToValidate,
			List<String> documentReferences) {
		super();
		this.documentToValidate = documentToValidate;
		this.documentReferences = documentReferences;
	}

	public String getDocumentToValidate() {
		return documentToValidate;
	}

	public void setDocumentToValidate(String documentToValidate) {
		this.documentToValidate = documentToValidate;
	}

	public List<String> getDocumentReferences() {
		return documentReferences;
	}

	public void setDocumentReferences(List<String> documentReferences) {
		this.documentReferences = documentReferences;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((documentReferences == null) ? 0 : documentReferences
						.hashCode());
		result = prime
				* result
				+ ((documentToValidate == null) ? 0 : documentToValidate
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DroolsDocuments other = (DroolsDocuments) obj;
		if (documentReferences == null) {
			if (other.documentReferences != null)
				return false;
		} else if (!documentReferences.equals(other.documentReferences))
			return false;
		if (documentToValidate == null) {
			if (other.documentToValidate != null)
				return false;
		} else if (!documentToValidate.equals(other.documentToValidate))
			return false;
		return true;
	}

}