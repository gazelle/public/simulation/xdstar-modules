package net.ihe.gazelle.xdstar.validator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Namespace", propOrder = {
	"namespace"
})
@XmlRootElement(name = "Namespace")

public enum NamespaceEnum {
	
    LCM_NAMESPACE("urn:oasis:names:tc:ebxml-regrep:xsd:lcm:3.0"),
    QUERY_NAMESPACE("urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0"),
    XDS_NAMESPACE("urn:ihe:iti:xds-b:2007"),
    XDSI_NAMESPACE("urn:ihe:rad:xdsi-b:2009"),
    RS_NAMESPACE("urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0");
    
    private String namespace;

	NamespaceEnum(String namespace){
		this.setNamespace(namespace);
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

}
