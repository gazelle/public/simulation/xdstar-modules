package net.ihe.gazelle.xdstar.comon.util;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.io.IOUtils;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.MimetypesFileTypeMap;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;

/**
 * @author abderrazek boufahja
 */

public class DocumentDownloadTool {


    private static Logger log = LoggerFactory.getLogger(DocumentDownloadTool.class);

    public static void showFile(InputStream inputStream, String filename, boolean download) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
        try {
            while (true) {
                response = (HttpServletResponse) PropertyUtils.getProperty(response, "response");
            }
        } catch (Exception e1) {
            // nothing to do
            // we are at org.apache.catalina.connector.ResponseFacade
        }
        response.reset();
        HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();

        showFile(request, response, inputStream, filename, download);

        facesContext.responseComplete();
    }

    public static void showFile(HttpServletRequest request, HttpServletResponse response, InputStream inputStream,
                                String filename, boolean download) {
        try {
            if (inputStream != null) {
                String userAgent = request.getHeader("user-agent");
                boolean isInternetExplorer = (userAgent.indexOf("MSIE") > -1);

                int length = inputStream.available();

                if (filename != null && filename.toLowerCase().endsWith(".pdf")) {
                    response.setContentType("application/pdf");
                } else if (filename != null && filename.toLowerCase().endsWith(".xml")) {
                    response.setContentType("text/xml");
                } else {
                    String contentType = MimetypesFileTypeMap.getDefaultFileTypeMap().getContentType(filename);
                    response.setContentType(contentType);
                }

                byte[] fileNameBytes = filename.getBytes((isInternetExplorer) ? ("windows-1250") : ("utf-8"));
                String dispositionFileName = "";
                for (byte b : fileNameBytes)
                    dispositionFileName += (char) (b & 0xff);

                String disposition;

                if (download) {
                    disposition = "attachment; filename=\"" + dispositionFileName + "\"";
                } else {
                    disposition = "inline; filename=\"" + dispositionFileName + "\"";
                }

                response.setHeader("Content-disposition", disposition);

                response.setContentLength(length);

                ServletOutputStream servletOutputStream = response.getOutputStream();

                IOUtils.copy(inputStream, servletOutputStream);

                servletOutputStream.flush();
                servletOutputStream.close();

                inputStream.close();
            }
        } catch (Exception e) {
            log.error("exception occure when downloading the document : ", e.getMessage());
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Impossible to display file");
        }
    }

}
