package net.ihe.gazelle.xdstar.conf.action;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import net.ihe.gazelle.xdstar.conf.model.ConfigurationType;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationTypeQuery;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("commonConfigurationManager")
@Scope(ScopeType.PAGE)
public class CommonConfigurationManager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ConfigurationType selectedConfigurationType;
	
	public ConfigurationType getSelectedConfigurationType() {
		return selectedConfigurationType;
	}

	public void setSelectedConfigurationType(
			ConfigurationType selectedConfigurationType) {
		this.selectedConfigurationType = selectedConfigurationType;
	}
	
	public List<ConfigurationType> getListConfigurationTypes(){
		List<ConfigurationType> res = (new ConfigurationTypeQuery()).getList();
		Collections.sort(res);
		return res;
	}
	
}

