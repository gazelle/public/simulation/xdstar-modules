package net.ihe.gazelle.xdstar.conf.action;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.EntityManager;

import net.ihe.gazelle.hql.HQLReloader;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.simulator.sut.model.UsageQuery;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationType;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationTypeQuery;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationTypeType;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

@Name("configurationTypeManager")
@Scope(ScopeType.PAGE)
public class ConfigurationTypeManager implements Serializable, UserAttributeCommon {

	private static final long serialVersionUID = 1L;

	private ConfigurationType selectedConfigurationType;
	
	private AffinityDomain selectedAffinityDomain;
	
	private Transaction selectedTransaction;

	@In(value="gumUserService")
	private UserService userService;
	
	public AffinityDomain getSelectedAffinityDomain() {
		return selectedAffinityDomain;
	}

	public void setSelectedAffinityDomain(AffinityDomain selectedAffinityDomain) {
		this.selectedAffinityDomain = selectedAffinityDomain;
	}

	public Transaction getSelectedTransaction() {
		return selectedTransaction;
	}

	public void setSelectedTransaction(Transaction selectedTransaction) {
		this.selectedTransaction = selectedTransaction;
	}

	public ConfigurationType getSelectedConfigurationType() {
		return selectedConfigurationType;
	}

	public void setSelectedConfigurationType(
			ConfigurationType selectedConfigurationType) {
		this.selectedConfigurationType = selectedConfigurationType;
	}
	
	public List<ConfigurationType> getListConfigurationTypes(){
		ConfigurationTypeQuery qq = new ConfigurationTypeQuery();
		return qq.getList();
	}
	
	public List<ConfigurationTypeType> getListConfigurationTypeTypes(){
		return Arrays.asList(ConfigurationTypeType.values());
	}
	
	public void initConfigurationType(){
		this.selectedConfigurationType = new ConfigurationType();
	}
	
	public void mergeConfigurationType(){
		EntityManager em = (EntityManager)Component.getInstance("entityManager");
		this.selectedConfigurationType = em.merge(this.selectedConfigurationType);
		em.flush();
		FacesMessages.instance().add(StatusMessage.Severity.INFO, "Object saved !");
	}
	
	public void deleteConfigurationType(ConfigurationType selectedConfigurationType){
		EntityManager em = (EntityManager)Component.getInstance("entityManager");
		em.remove(selectedConfigurationType);
		em.flush();
	}

	public void deleteSelectedConfigurationType(){
		selectedConfigurationType = HQLReloader.reloadDetached(selectedConfigurationType);
		deleteConfigurationType(selectedConfigurationType);
		FacesMessages.instance().add(StatusMessage.Severity.INFO, "Configuration Type deleted !");
	}
	
	public List<Usage> getListUsage(){
		UsageQuery qq = new UsageQuery();
		if (this.selectedAffinityDomain != null) qq.affinity().eq(selectedAffinityDomain);
		if (this.selectedTransaction != null) qq.transaction().eq(selectedTransaction);
		class SortUsage implements Comparator<Usage>{
			
			@Override
			public int compare(Usage arg0, Usage arg1) {
				if (arg0 == null) return -1;
				if (arg0.getAffinity() == null) return -1;
				if (arg0.getTransaction() == null) return -1;
				if (arg1 == null) return 1;
				if (arg1.getAffinity() == null) return 1;
				if (arg1.getTransaction() == null) return 1;
				return arg0.getAffinity().getKeyword().compareTo(arg1.getAffinity().getKeyword())*2 + 
				arg0.getTransaction().getKeyword().compareTo(arg1.getTransaction().getKeyword());
			}
		}
		List<Usage> res = qq.getList();
		Collections.sort(res, new SortUsage());
		return res;
	}


	@Override
	public String getUserName(String userId) {
		return userService.getUserDisplayNameWithoutException(userId);
	}
}
