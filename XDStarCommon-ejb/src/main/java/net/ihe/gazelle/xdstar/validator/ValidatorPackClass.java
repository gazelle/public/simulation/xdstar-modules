package net.ihe.gazelle.xdstar.validator;

import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.metadata.model.AdhocQueryMetadata;
import net.ihe.gazelle.metadata.model.ConstraintSpecification;
import net.ihe.gazelle.metadata.model.RegistryObjectListMetadata;
import net.ihe.gazelle.simulator.sut.model.Usage;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
@Name("ValidatorPackClass")
@Table(name="validator_pack_class", schema = "public")
@SequenceGenerator(name = "validator_pack_class_sequence", sequenceName = "validator_pack_class_id_seq", allocationSize=1)
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("validatorPackClass")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidatorPackClass", propOrder = {
	"validatorName",
	"version",
	"listPackClass",
	"namespaceEnum",
	"extractNodeEnum",
	"listRegistryObjectMetadata",
	"registryObjectListMetadata",
	"technicalFramework",
	"hasNistValidation",
	"metadataCodes",
	"isR",
	"isPnR",
	"isXc",
	"isXdr",
	"usages",
		"disactivated",
		"extraConstraintSpecifications"
})
@XmlRootElement(name = "ValidatorPackClass")

public class ValidatorPackClass implements Comparable<ValidatorPackClass>,Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    @XmlTransient
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="validator_pack_class_sequence")
	private Integer id;
	
	private String validatorName;
	
	private String version = "1.00";
	
	@ManyToMany (cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	private List<PackClass> listPackClass;
	
	@Enumerated(EnumType.STRING)
	private NamespaceEnum namespaceEnum;
	
	@Enumerated(EnumType.STRING)
	private ExtractNodeAndTypeEnum extractNodeEnum;
	
	@ManyToMany (cascade = CascadeType.ALL)
	private List<AdhocQueryMetadata> listRegistryObjectMetadata;
	
	@OneToOne
	@JoinTable(name = "validator_registry_object_list_metadata_rol")
	private RegistryObjectListMetadata registryObjectListMetadata;
	
	@ManyToMany(cascade = { CascadeType.PERSIST})
	@Fetch(FetchMode.SUBSELECT)
    @JoinTable(name="validator_pack_class_usage")
	private List<Usage> usages;
	
	private String technicalFramework;
	
	private Boolean hasNistValidation = false;;
	
	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
   	@JoinTable(name="validator_pack_class_nist_code")
	private NistCode metadataCodes;
	
	private Boolean isR = false;
	
	private Boolean isPnR = false;
	
	private Boolean isXc = false;
	
	private Boolean isXdr = false;
	
	private Boolean disactivated;


	@OneToMany(cascade = {CascadeType.ALL})
	@JoinTable(name = "validator_packclass_extra_constraints", joinColumns = @JoinColumn(name = "validator_pack_class_id"),
			inverseJoinColumns = @JoinColumn(name = "extra_constraint_specifications_id"))
	protected List<ConstraintSpecification> extraConstraintSpecifications;
	
	public boolean disactivatedValue() {
		if (disactivated == null) {
			return false;
		}
		return disactivated;
	}

	public Boolean getDisactivated() {
		return disactivated;
	}

	public void setDisactivated(Boolean disactivated) {
		this.disactivated = disactivated;
	}

	public Boolean getHasNistValidation() {
		return hasNistValidation;
	}

	public void setHasNistValidation(Boolean hasNistValidation) {
		this.hasNistValidation = hasNistValidation;
	}

	public NistCode getMetadataCodes() {
		return metadataCodes;
	}

	public void setMetadataCodes(NistCode metadataCodes) {
		this.metadataCodes = metadataCodes;
	}


	public ExtractNodeAndTypeEnum getExtractNodeEnum() {
		return extractNodeEnum;
	}

	public void setExtractNodeEnum(ExtractNodeAndTypeEnum extractNodeEnum) {
		this.extractNodeEnum = extractNodeEnum;
	}

	public NamespaceEnum getNamespaceEnum() {
		return namespaceEnum;
	}

	public void setNamespaceEnum(NamespaceEnum namespaceEnum) {
		this.namespaceEnum = namespaceEnum;
	}

	@FilterLabel
	public String getValidatorName() {
		return validatorName;
	}

	public void setValidatorName(String validatorName) {
		this.validatorName = validatorName;
	}
	
	public List<PackClass> getListPackClass() {
		return listPackClass;
	}

	public void setListPackClass(List<PackClass> listPackClass) {
		this.listPackClass = listPackClass;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public RegistryObjectListMetadata getRegistryObjectListMetadata() {
		return registryObjectListMetadata;
	}

	public void setRegistryObjectListMetadata(RegistryObjectListMetadata registryObjectListMetadata) {
		this.registryObjectListMetadata = registryObjectListMetadata;
	}
	
	public List<AdhocQueryMetadata> getListRegistryObjectMetadata() {
		return listRegistryObjectMetadata;
	}

	public void setListRegistryObjectMetadata(List<AdhocQueryMetadata> listRegistryObjectMetadata) {
		this.listRegistryObjectMetadata = listRegistryObjectMetadata;
	}


	public List<ConstraintSpecification> getExtraConstraintSpecifications() {
		return extraConstraintSpecifications;
	}

	public void setExtraConstraintSpecifications(List<ConstraintSpecification> extraConstraintSpecifications) {
		this.extraConstraintSpecifications = extraConstraintSpecifications;
	}
	
	public void addPackClass(PackClass packClass){
		if(listPackClass == null){
			listPackClass = new ArrayList<PackClass>();
		}
		this.listPackClass.add(packClass);
	}
	
	public void addRegistryObjectMetadata(AdhocQueryMetadata rom){
		if(listRegistryObjectMetadata == null){
			listRegistryObjectMetadata = new ArrayList<AdhocQueryMetadata>();
		}
		this.listRegistryObjectMetadata.add(rom);
	}
	

	public void setUsages(List<Usage> usages) {
		this.usages = usages;
	}

	public List<Usage> getUsages() {
		if (usages == null) {
			usages = new ArrayList<Usage>();
		}
		return usages;
	}
	
	public Boolean getIsR() {
		return isR;
	}

	public void setIsR(Boolean isR) {
		this.isR = isR;
	}

	public Boolean getIsPnR() {
		return isPnR;
	}

	public void setIsPnR(Boolean isPnR) {
		this.isPnR = isPnR;
	}

	public Boolean getIsXc() {
		return isXc;
	}

	public void setIsXc(Boolean isXc) {
		this.isXc = isXc;
	}

	public Boolean getIsXdr() {
		return isXdr;
	}

	public void setIsXdr(Boolean isXdr) {
		this.isXdr = isXdr;
	}
	
	public static String getValidatorNameFromDescription(String affinityDomain, String transactionKeyword, String messageType, boolean isRequest ){
		
		
		ValidatorPackClassQuery queryad = new ValidatorPackClassQuery();
		
		queryad.usages().affinity().keyword().eq(affinityDomain);
		queryad.usages().transaction().keyword().eq(transactionKeyword);
		List<ValidatorPackClass> lvp = queryad.getList();
		List<ValidatorPackClass> lvptmp = new ArrayList<ValidatorPackClass>();
		for (ValidatorPackClass validatorPackClass : lvp) {
			if (validatorPackClass.getExtractNodeEnum() != null && validatorPackClass.getExtractNodeEnum().isRequest() == isRequest){
				lvptmp.add(validatorPackClass);
			}
		}
		if (lvptmp.size()==1){
			return lvptmp.get(0).getValidatorName();
		}
		else if (lvptmp.size()>0){
			for (ValidatorPackClass validatorPackClass : lvptmp) {
				if (messageType != null && (messageType.equals("OrderService:list") || messageType.equals("PatientService:list"))) {
					messageType = "V1 XCA";
				}
				else if (messageType != null && (messageType.equals("OrderService:list V2.2 (XCF)") || messageType.equals("	PatientService:list V2.2 (XCF)"))) {
					messageType = "V2.2";
				}
				if (messageType!= null && 
						validatorPackClass.getValidatorName().toLowerCase().replace(":", " ").contains(messageType.toLowerCase().replace(":", " "))) {
					return validatorPackClass.getValidatorName();
				}
			}
		}
	return null;
}

	public String getTechnicalFramework() {
		return technicalFramework;
	}

	public void setTechnicalFramework(String technicalFramework) {
		this.technicalFramework = technicalFramework;
	}
	
	public static List<ValidatorPackClass> getAllListValidatorPackClass(){
		ValidatorPackClassQuery qq = new ValidatorPackClassQuery();
		List<ValidatorPackClass> res = qq.getList();
		return res;
	}
	
	public int compareTo(ValidatorPackClass arg0) {
		if (this.getId() == null) {
			return -1;
		}
		if (arg0 == null) {
			return 1;
		}
		if (arg0.getId() == null) {
			return 1;
		}
		return this.getId().compareTo(arg0.getId());
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
}