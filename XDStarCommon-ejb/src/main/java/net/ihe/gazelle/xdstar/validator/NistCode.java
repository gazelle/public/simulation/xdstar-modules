package net.ihe.gazelle.xdstar.validator;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.jboss.seam.annotations.Name;

@Entity
@Name("NistCode")
@Table(name="nist_code", schema = "public")
@SequenceGenerator(name = "nist_code_sequence", sequenceName = "nist_code_id_seq", allocationSize=1)
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("nistCode")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "NistCode")

public class NistCode {
	
	@XmlTransient
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="nist_code_sequence")
	private Integer id;
	
    private String nistCodeName;
    
    private String url;

	public String getNistCodeName() {
		return nistCodeName;
	}

	public void setNistCodeName(String nistCodeName) {
		this.nistCodeName = nistCodeName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}
