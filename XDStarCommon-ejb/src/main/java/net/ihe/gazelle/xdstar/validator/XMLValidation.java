package net.ihe.gazelle.xdstar.validator;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.validation.XSDMessage;
import net.ihe.gazelle.xmltools.xsd.ValidationException;
import net.ihe.gazelle.xmltools.xsd.XSDValidator;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XMLValidation {
	
	private static final String PASSED = "PASSED";

	private static final String FAILED = "FAILED";

	private static Logger log = LoggerFactory.getLogger(XMLValidation.class);
	
	private static Schema schema;
	
	private static SAXParserFactory docfactory;
	
	private static SAXParserFactory basicfactory;
	
	static {
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		try {
			schema = factory.newSchema(new File(ApplicationConfiguration.getValueOfVariable("xds_xsd")));
		} catch (Exception e) {
			log.info("error to upload the schema XSD");
		}
		docfactory = SAXParserFactory.newInstance();
		docfactory.setNamespaceAware(true);
		docfactory.setSchema(schema);
		
		try {
			basicfactory = SAXParserFactory.newInstance();
			basicfactory.setValidating(false);
			basicfactory.setNamespaceAware(true);
		} catch (Exception e){}
	}
	
	/**
	 * Parses the file using SAX to check that it is a well-formed XML file
	 * @param file
	 * @return
	 */
	public static DocumentWellFormed isXMLWellFormed(String string)
	{
		DocumentWellFormed dwf = new DocumentWellFormed();
		dwf = XMLValidation.validXMLUsingXSD(string, null, basicfactory, dwf);
		if (string == null){
			String res = "Document passed is empty.";
			System.out.println(res);
			dwf.setResult(FAILED);
			return dwf;
		}
		else
		{
			SAXParser parser;
			DefaultHandler handler;
			try{
				SAXParserFactory spfactory = SAXParserFactory.newInstance();
				spfactory.setNamespaceAware(true);
				spfactory.setXIncludeAware(true);
				spfactory.setValidating(true);
				parser = spfactory.newSAXParser();
				handler = new DefaultHandler();
				
			}catch(Exception e){
				String res = "Error: Cannot initialize SAX parser";
				System.out.println(res);
				dwf.setResult(FAILED);
				return dwf;
			}
			try{
				parser.parse(new ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8)), handler);
				dwf.setResult(PASSED);
				return dwf;
			}catch(SAXException e){
				System.out.println("Error: SAX parser Exception");
				dwf.setResult(FAILED);
				return dwf;
			}catch(Exception e){
				System.out.println("exception when parsing the file: " + e.getMessage());
				dwf.setResult(FAILED);
				return dwf;
			}
		}
	}
	
	private static DocumentValidXSD validXMLUsingXSD(String xdwDocument){
		DocumentValidXSD doc = new DocumentValidXSD();
		return validXMLUsingXSD(xdwDocument, null, docfactory, doc);
	}
	
	
	
	
	private static <T extends DocumentValidXSD> T validXMLUsingXSD(String cdaDocument, String xsdpath, SAXParserFactory factory, T dv)
	{
		List<net.ihe.gazelle.xmltools.xsd.ValidationException> exceptions = new ArrayList<ValidationException>();
		try {
			ByteArrayInputStream bais = new ByteArrayInputStream(cdaDocument.getBytes("UTF8"));
			exceptions =XSDValidator.validateUsingFactoryAndSchema(bais, xsdpath,  factory);
		} catch(Exception e){
			exceptions.add(handleException(e));
		}
		return extractValidationResult(exceptions, xsdpath, factory, dv);
	}
	
	private static <T extends DocumentValidXSD> T extractValidationResult(List<ValidationException> exceptions, String xsdpath, SAXParserFactory factory, T dv)
	{

		dv.setResult(PASSED);

		if (exceptions == null || exceptions.size() == 0)
		{	
			dv.setResult(PASSED);
			return dv;
		}
		else
		{
			Integer nbOfErrors = 0;
			Integer nbOfWarnings = 0;
			Integer exceptionCounter = 0;
			for (ValidationException ve : exceptions)
			{
				if (ve.getSeverity() == null){
					ve.setSeverity("error");
				}
				exceptionCounter ++;
				if ((ve.getSeverity() != null) && (ve.getSeverity().equals("warning"))){
					nbOfWarnings ++;
				}
				else {
					nbOfErrors ++;
				}
				XSDMessage xsd = new XSDMessage();
				xsd.setSeverity(ve.getSeverity());
				
				if (StringUtils.isNumeric(ve.getLineNumber()) && StringUtils.isNumeric(ve.getColumnNumber()) ){
					xsd.setMessage("Line:Col[" + ve.getLineNumber() + ":" + ve.getColumnNumber() + "]:" + ve.getMessage());
				}
				else{
					xsd.setMessage(ve.getMessage());
				}
				dv.getXSDMessage().add(xsd);
			}
			dv.setNbOfErrors(nbOfErrors.toString());
			dv.setNbOfWarnings(nbOfWarnings.toString());
			if (nbOfErrors > 0){
				dv.setResult(FAILED);
			}
			return dv;
		}

	}
	
	private static ValidationException handleException(Exception e)
	{
		ValidationException ve = new ValidationException();
		ve.setLineNumber("0");
		ve.setColumnNumber("0");
		if (e != null && e.getMessage() != null){
			ve.setMessage("error on validating : " + e.getMessage());
		} 
		else if (e != null && e.getCause() != null && e.getCause().getMessage() != null){
			ve.setMessage("error on validating : " + e.getCause().getMessage());
		}
		else{
			ve.setMessage("error on validating. The exception generated is of kind : " + e.getClass().getSimpleName());
		}
		ve.setSeverity("error");
		return ve;
	}
	
	/**
	 * Checks that the XDW document is valid (uses XDW.xsd file)
	 * @param file
	 * @return
	 */
	public static DocumentValidXSD isXDSValid(String document)
	{
		if (document == null){
			return null;
		}
		else {
			return validXMLUsingXSD(document);
		}
	}

}
