package net.ihe.gazelle.xdstar.common.action;


import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.xml.bind.JAXBException;

import net.ihe.gazelle.hql.paths.HQLSafePathEntity;
import net.ihe.gazelle.query.AdhocQueryResponseType;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.users.model.User;
import net.ihe.gazelle.xdstar.common.model.AbstractMessage;
import net.ihe.gazelle.xdstar.filter.MessageDataModel;
import net.ihe.gazelle.xdstar.validator.MetadataExtractor;
import net.ihe.gazelle.xdstar.validator.MetadataValidator;

import org.ajax4jsf.model.DataVisitResult;
import org.ajax4jsf.model.DataVisitor;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.log.Log;
import org.jdom.JDOMException;


/**
 * <b>Class Description :  </b>AbstractMessageManager<br><br>
 * AbstractMessageManager
 *
 * @author Abderrazek Boufahja / INRIA Rennes IHE development Project
 * @class AbstractMessageManager
 * @package net.ihe.gazelle.simulator.rsq.action
 */
@Name("allMessageManagerBean")
@Scope(ScopeType.PAGE)
public class AllMessageManager extends MessageManager<AbstractMessage> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Logger
    private static Log log;

    private transient MessageDataModel availableMessages;
    private transient List<AbstractMessage> availableMessagesList;

    public List<AbstractMessage> getAvailableMessagesList() {
        return availableMessagesList;
    }

    private Date startDate;

    private Date endDate;

    private AbstractMessage selectedAbstractMessage;

    private AdhocQueryResponseType selectedAdhocQueryResponseType;

    public AdhocQueryResponseType getSelectedAdhocQueryResponseType() {
        return selectedAdhocQueryResponseType;
    }

    public void setSelectedAdhocQueryResponseType(
            AdhocQueryResponseType selectedAdhocQueryResponseType) {
        this.selectedAdhocQueryResponseType = selectedAdhocQueryResponseType;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.availableMessages.setStartDate(startDate);
        this.startDate = startDate;
        this.applyFilter();
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.availableMessages.setEndDate(endDate);
        this.endDate = endDate;
        this.applyFilter();
    }

    public void setAvailableMessages(MessageDataModel availableMessages) {
        this.availableMessages = availableMessages;
    }

    public void setAvailableMessagesList(List<AbstractMessage> availableMessagesList) {
        this.availableMessagesList = availableMessagesList;
    }

    public MessageDataModel getAvailableMessages() {
        if (availableMessages == null) {
            try {
                HQLSafePathEntity.filterLabelMethodInit.remove(Transaction.class);
                HQLSafePathEntity.filterLabelMethodInit.put(Transaction.class, true);
                HQLSafePathEntity.filterLabelMethods.put(Transaction.class, Transaction.class.getMethod("getKeyword", null));
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
            availableMessages = new MessageDataModel();
        }
        return availableMessages;
    }

    private void applyFilter() {
        this.getAvailableMessages().resetCache();
        this.getAvailableMessages().getFilter().modified();
    }

    public void resetFilter() {
        this.setEndDate(null);
        this.setStartDate(null);
        this.getAvailableMessages().getFilter().clear();
        this.getAvailableMessages().resetCache();
    }

    public void listMessagesToDisplay() {
        AbstractMessage abs = (AbstractMessage) Component.getInstance("messagesToDisplay");
        availableMessagesList = new ArrayList<>();
        if (abs != null) {
            this.getAvailableMessages().setId(abs.getId());
            availableMessagesList.add(abs);
        }
        selectedMessage = null;
        stringRequestContent = null;
        stringResponseContent = null;
    }

    public void initFirstMessage() throws IOException, JDOMException, JAXBException {
        try {
            if ((this.availableMessagesList.get(0) != null) && (this.availableMessagesList.get(0).getReceivedMessageContent() != null) &&
                    this.availableMessagesList.get(0).getReceivedMessageContent().getMessageContent() != null) {
                String mess = this.availableMessagesList.get(0).getReceivedMessageContent().getMessageContent();
                String rr = MetadataValidator.extractNode(mess, "AdhocQueryResponse", "urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0");
                selectedAdhocQueryResponseType = MetadataExtractor.getAdhocQueryResponseTypeFromXml(rr);

            }
        } catch (Exception e) {
            this.selectedAdhocQueryResponseType = null;
            log.error("Failed to find AdhocQueryResponse in response: " + e.getMessage());
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Failed to find AdhocQueryResponse in response");
        }
    }

    @Destroy
    @Remove
    public void destroy() {
    }

    public void getMessageById() {
        this.getMessageById(AbstractMessage.class);

    }

    @Override
    public AbstractMessage getReloadedSelectedMessage() {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        this.selectedMessage = em.find(AbstractMessage.class, this.selectedMessage.getId());
        return this.selectedMessage;
    }

}
