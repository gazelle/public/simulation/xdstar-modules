/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.core.soap;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.model.MessageType;
import net.ihe.gazelle.simulator.common.model.ParameterValuesForRequest;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.common.utils.XmlUtil;
import net.ihe.gazelle.simulator.common.xua.SamlAssertionAttributes;
import net.ihe.gazelle.simulator.common.xua.SamlAssertionSupplier;
import net.ihe.gazelle.simulator.common.xua.SignatureException;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.xdstar.core.SignatureManager;
import org.apache.log4j.Logger;
import org.picketlink.identity.federation.core.wstrust.WSTrustException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.util.List;
import java.util.UUID;

/**
 *  <b>Class Description :  </b>SOAPBuilder<br><br>
 * This class contains the methods used to create the SOAP message which will be sent to the responding gateway
 *  
 * @class               	SOAPBuilder.java
 * @package        			net.ihe.gazelle.simulator.xca.initgw.action
 * @author 					Abderrazek Boufahja / IHE Europe
 * @author                	Anne-Gaëlle Bergé / INRIA Rennes IHE development Project
 * @version                	1.0 - 2010, October 4th
 *
 */
public class SOAPBuilder {
	
	private static final String SOAP_ENVELOPE_NAMESPACE = "http://www.w3.org/2003/05/soap-envelope";

	/**
	 * the log
	 */
	private static Logger log = Logger.getLogger(SOAPBuilder.class);
	
	private Transaction transaction;
	private MessageType messageType;
	private List<ParameterValuesForRequest> parameters;
	private String returnType;
	private Boolean isXUA;
	private SystemConfiguration configuration;
	private SamlAssertionAttributes samlAssertionAttributes;
	private String praticianID;
	private Boolean useTRC;
	private String patientId;

    /**
     * Constructor
     */
    public SOAPBuilder() {
        this.useTRC = false;
        this.isXUA = false;
    }

    /**
     * creates the root element Envelope of the message
     *
     * @param request
     * @return
     */
    private static Element createEnvelope(Document request) {
//		Element root = request.createElementNS("http://www.w3.org/2003/05/soap-envelope", "Envelope");
        Element root = request.createElement("s:Envelope");
        root.setAttribute("xmlns:s", SOAP_ENVELOPE_NAMESPACE);
        root.setAttribute("xmlns:a", "http://www.w3.org/2005/08/addressing");
        return root;
    }

    /**
     * request an assertion to the X Assertion Provider
     *
     * @param praticianID             TODO
     * @param samlAssertionAttributes TODO
     * @return
     */
    private static Element requestAssertion(String praticianID, SamlAssertionAttributes samlAssertionAttributes) {
        try {
            Element assertion = SamlAssertionSupplier.getAssertion(praticianID, samlAssertionAttributes, SignatureManager.getSignatureUtil());
            return assertion;
        } catch (Exception e) {
            log.error("error occures.", e);
            return null;
        }
    }

	/**
	 * Getters and Setters
	 */

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public MessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public Boolean isXUA() {
		return isXUA;
	}

    public void setXUA(Boolean isXUA) {
		this.isXUA = isXUA;
	}

	public SystemConfiguration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(SystemConfiguration configuration) {
		this.configuration = configuration;
	}

	public SamlAssertionAttributes getSamlAssertionAttributes() {
		return samlAssertionAttributes;
	}

	public void setSamlAssertionAttributes(
			SamlAssertionAttributes samlAssertionAttributes) {
		this.samlAssertionAttributes = samlAssertionAttributes;
	}

    public String getPraticianID() {
        return praticianID;
    }

    public void setPraticianID(String praticianID) {
        this.praticianID = praticianID;
    }

    public List<ParameterValuesForRequest> getParameters() {
        return parameters;
    }

    public void setParameters(List<ParameterValuesForRequest> parameters) {
        this.parameters = parameters;
    }

	public Boolean isUseTRC() {
		return useTRC;
	}

	public void setUseTRC(Boolean useTRC) {
		this.useTRC = useTRC;
	}

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
	}
	
	/**
	 * Builds and returns the message according the various parameters
	 * @return
	 */
	public String buildMessage(String homeCommunityId)
	{
		if (transaction != null)
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			try {
				// create document
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document request = builder.newDocument();
				request.setXmlVersion("1.0");
				request.setXmlStandalone(true);

				if (messageType.getAction() == null)
				{
					log.error("no soap action defined for message type : " + messageType.getName());
					return null;
				}
				else if (messageType.getAction().equals("urn:ihe:iti:2007:CrossGatewayRetrieve")){
					request.appendChild(createCrossGatewayRetrieve(request));
				}
				else if (messageType.getAction().equals("urn:ihe:iti:2007:CrossGatewayQuery")){
					request.appendChild(createCrossGatewayQuery(request, homeCommunityId));
				}
				else if (messageType.getAction().equals("urn:ihe:iti:2007:RegistryStoredQuery")){
					request.appendChild(createRegistryStoredQuery(request, homeCommunityId));
				}
				else if (messageType.getAction().equals("urn:ihe:iti:2007:RetrieveDocumentSet")){
					request.appendChild(createRetrieveDocumentSet(request));
				}
				else
				{
					log.error("unknown action: " + messageType.getAction());
					return null;
				}

				return XmlUtil.outputDOM(request);
			}catch(Exception e){
				log.error("An error occurred when building the message: " + e.getMessage());
                return null;
			}
		}
		else {
			return null;
		}
	}

    /**
     *
	 * @param request
	 * @return
	 */
	private Element createCrossGatewayRetrieve(Document request)
	{
		try {
			Element root = createEnvelope(request);

			Element header = createSOAPHeader(request);
			root.appendChild(header);

			// body
            Element body = request.createElement("s:Body");
			body.setAttribute("xmlns:s", SOAP_ENVELOPE_NAMESPACE);

			Element retrieveDocumentRequest = request.createElement("xdsb:RetrieveDocumentSetRequest");
			retrieveDocumentRequest.setAttribute("xmlns:xdsb", "urn:ihe:iti:xds-b:2007");

			// create <DocumentRequest> elements
			Integer maxIndex = parameters.get(0).getValues().size();
			for (int index = 0; index < maxIndex; index ++)
			{
				retrieveDocumentRequest.appendChild(addDocument(request, index));
			}

			body.appendChild(retrieveDocumentRequest);

			root.appendChild(body);

			return root;

		} catch (Exception e) {
			log.error("An error occurred when building the message", e);
			return null;
		}
	}
	
	/**
     *
	 * @param request
	 * @return
	 */
	private Element createCrossGatewayQuery(Document request, String homeCommunityId)
	{
        try{
			// create envelope
			Element root = createEnvelope(request);

			// create header
			Element header = createSOAPHeader(request);
			root.appendChild(header);

			// create body
			Element body = request.createElement("s:Body");
			body.setAttribute("xmlns:s", SOAP_ENVELOPE_NAMESPACE);
			Element queryRequest = request.createElement("query:AdhocQueryRequest");
			queryRequest.setAttribute("xmlns:query", "urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0");
			queryRequest.setAttribute("xmlns", "urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0");
			queryRequest.setAttribute("xmlns:rs", "urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0");
			queryRequest.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");

			Element queryResponse = request.createElement("query:ResponseOption");
			queryRequest.setAttribute("xmlns:query", "urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0");
			queryResponse.setAttribute("returnComposedObjects", "true");
			queryResponse.setAttribute("returnType", returnType);
			queryRequest.appendChild(queryResponse);

			// create adhocQuery
			Element adhocQuery = request.createElement("AdhocQuery");
//			adhocQuery.setAttribute("home", configuration.getHomeCommunityId());
			adhocQuery.setAttribute("home", homeCommunityId);
			adhocQuery.setAttribute("id", messageType.getUuid());

			for (ParameterValuesForRequest paramValues : parameters)
			{
				if (paramValues.getParameter().getMultiple())
				{
					if (paramValues.getOperation() != null && paramValues.getOperation().equals(ParameterValuesForRequest.OP_AND))
					{
						List<String> parameterValues = paramValues.getValues();
						for (int index = 0 ; index < parameterValues.size(); index ++)
						{
							adhocQuery.appendChild(addSlot(request, paramValues, index));
						}
					}
					else if (paramValues.getOperation() != null && paramValues.getOperation().equals(ParameterValuesForRequest.OP_OR))
					{
						adhocQuery.appendChild(addSlot(request, paramValues, null));
					}
					else if (paramValues.getValues() != null && paramValues.getValues().size() == 1)
					{
						adhocQuery.appendChild(addSlot(request, paramValues, 0));
					}
				}
				else
				{
					adhocQuery.appendChild(addSlot(request, paramValues, null));
				}

			}

			queryRequest.appendChild(adhocQuery);
            body.appendChild(queryRequest);
            root.appendChild(body);

			return root;

		}catch (Exception e) {
			log.error("An error occurred when building the message", e);
			return null;
		}
	}
	
	private Element createRegistryStoredQuery(Document request, String homeCommunityId)
	{
		return createCrossGatewayQuery(request,homeCommunityId);
	}
	
	private Element createRetrieveDocumentSet(Document request) {
        return createCrossGatewayRetrieve(request);
	}
	
	/**
     *
	 * @param request
	 * @return
	 */
	private Element createSOAPHeader(Document request)
	{
		Element header = request.createElement("s:Header");
        header.setAttribute("xmlns:s", SOAP_ENVELOPE_NAMESPACE);


		if (isXUA)
		{
			Element security = request.createElement("wss:Security");
			security.setAttribute("xmlns:wss", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
			if (praticianID == null || praticianID.isEmpty()){
				praticianID = ApplicationConfiguration.getValueOfVariable("default_pratician_id");
			}
			Element identityAssertion = requestAssertion(praticianID, samlAssertionAttributes);
			Node assertionToken = request.importNode(identityAssertion, true);
			security.appendChild(assertionToken);

			if (useTRC!=null && useTRC)
			{
				Node trcAssertionToken = request.importNode(requestTRCAssertion(identityAssertion), true);
				security.appendChild(trcAssertionToken);
			}

			header.appendChild(security);
		}

		Element action = request.createElement("a:Action");
		action.setAttribute("s:mustUnderstand", "1");
		action.setTextContent(messageType.getAction());

		header.appendChild(action);

		Element messageID = request.createElement("a:MessageID");
        UUID uuid = UUID.randomUUID();
        messageID.setTextContent("urn:uuid:" + uuid);
		header.appendChild(messageID);

		Element replyTo = request.createElement("a:ReplyTo");
		Element address = request.createElement("a:Address");
		address.setTextContent("http://www.w3.org/2005/08/addressing/anonymous");
		replyTo.appendChild(address);
		header.appendChild(replyTo);

		Element to = request.createElement("a:To");
		to.setAttribute("s:mustUnderstand", "1");
		to.setTextContent(configuration.getUrl());
		header.appendChild(to);

		return header;
	}

	private Element requestTRCAssertion(Element identityAssertion) {
		if (praticianID == null || praticianID.isEmpty()) {
			praticianID = ApplicationConfiguration.getValueOfVariable("default_pratician_id");
		}
		SamlAssertionAttributes attributes = new SamlAssertionAttributes(true);
		attributes.setXSPASubjectTRC(patientId);
		attributes.setXSPAPurposeOfUse("TREATMENT");
		try {
			return SamlAssertionSupplier.getAssertionTRC(praticianID, attributes, identityAssertion, SignatureManager.getSignatureUtil());
		} catch (WSTrustException e) {
			log.error("WSTrustException exception.", e);
		} catch (SignatureException e) {
			log.error("SignatureException exception.", e);
		}
		return null;
	}

    /**
     *
	 * @param request
	 * @param parameter
	 * @return
	 */
	private Element addSlot(Document request, ParameterValuesForRequest parameter, Integer index)
	{
		Element slot = request.createElement("Slot");
		slot.setAttribute("name", parameter.getParameter().getName());
		Element valueList = request.createElement("ValueList");
		if (index == null)
		{
			List<String> values = parameter.getValues();
			if (values != null && !values.isEmpty())
			{
				for (String value : values)
				{
					valueList.appendChild(addValue(request, value, parameter.getParameter().getMultiple()));
				}
				slot.appendChild(valueList);
			}
		}
		else
		{
			valueList.appendChild(addValue(request, parameter.getValues().get(index), parameter.getParameter().getMultiple()));
            slot.appendChild(valueList);
		}
		return slot;
	}
	
	/**
     *
	 * @param request
	 * @param values
	 * @return
	 */
	private Element addValue(Document request, String value, boolean multiple)
	{
		Element valueElement = request.createElement("Value");
		if (!multiple)
		{
			valueElement.setTextContent("'"+ value.trim() + "'");
		}
		else
		{
			valueElement.setTextContent("('"+ value.trim() + "')");
		}
		return valueElement;
	}
	
	/**
     *
	 * @param request
	 * @param index
	 * @return
	 */
	private Element addDocument(Document request, Integer index)
	{
		Element documentRequest = request.createElement("DocumentRequest");
		documentRequest.setAttribute("xmlns", "urn:ihe:iti:xds-b:2007");

		for (ParameterValuesForRequest param : parameters)
		{
			Element value = request.createElement(param.getParameter().getName());
			if ((param.getValues() != null) && (param.getValues().size()>index)){
				value.setTextContent(param.getValues().get(index));
			}
			documentRequest.appendChild(value);
		}
		return documentRequest;
	}
	

}
