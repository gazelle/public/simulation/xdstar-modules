	package net.ihe.gazelle.xdstar.rsq.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.query.AdhocQueryResponseType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.ExtrinsicObjectType;
import net.ihe.gazelle.rim.SlotType1;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("adhocQueryResponseParser")
@Scope(ScopeType.PAGE)
public class AdhocQueryResponseParser implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String getMessageResponse(AdhocQueryResponseType resp){
		String res = null;
		if (resp != null){
			res = resp.getStatus();
		}
		return res;
	}
	
	public List<XDSEntryDescriber> extractListDocument(AdhocQueryResponseType resp){
		List<XDSEntryDescriber> res = null;
		if (resp != null && (resp.getRegistryObjectList() != null) && (resp.getRegistryObjectList().getExtrinsicObject().size()>0)){
			res = new ArrayList<XDSEntryDescriber>();
			for (ExtrinsicObjectType doc : resp.getRegistryObjectList().getExtrinsicObject()) {
				XDSEntryDescriber desc = new XDSEntryDescriber();
				desc.setPatientId(getExternalIdentifier("urn:uuid:58a6f841-87b3-4a3e-92fd-a8ffeff98427", doc));
				desc.setCreationTime(getSlot("creationTime", doc));
				desc.setDocumentUniqueId(getExternalIdentifier("urn:uuid:2e82c1f6-a085-4c72-9da3-8640a32e42ab", doc));
				desc.setHomeCommunityId(doc.getHome());
				desc.setMimeType(doc.getMimeType());
				desc.setRepositoryUniqueId(getSlot("repositoryUniqueId", doc));
				desc.setStatus(doc.getStatus());
				desc.setTitle(getName(doc));
				desc.setUri(getSlot("URI", doc));
				res.add(desc);
			}
		}
		return res;
	}
	
	private String getSlot(String name, ExtrinsicObjectType doc){
		String res = null;
		for (SlotType1 slot : doc.getSlot()) {
			if (slot.getName().equals(name)){
				if ((slot.getValueList() != null) && (slot.getValueList().getValue().size()>0)){
					return slot.getValueList().getValue().get(0);
				}
			}
		}
		return res;
	}
	
	private String getExternalIdentifier(String id, ExtrinsicObjectType doc){
		String res = null;
		for (ExternalIdentifierType ext : doc.getExternalIdentifier()) {
			if (ext.getIdentificationScheme() != null && ext.getIdentificationScheme().equals(id)){
				return ext.getValue();
			}
		}
		return res;
	}
	
	private String getName(ExtrinsicObjectType doc){
		String res = null;
		if (doc.getName() != null && doc.getName().getLocalizedString().size()>0){
			res = doc.getName().getLocalizedString().get(0).getValue();
		}
		return res;
	}
	
	

}
