package net.ihe.gazelle.xdstar.core;

public enum XUAType {

    NONE("none"),
    EPSOS_ASSERTION("epSOS_assertion"),
    EPR_ASSERTION("epr_assertion"),
    IHE_ASSERTION("ihe_assertion");


    private String xuaType;

    XUAType(String xuaType) {
        this.xuaType = xuaType;
    }

    public String getXuaType() {
        return xuaType;
    }

    public void setXuaType(String xuaType) {
        this.xuaType = xuaType;
    }

    /*public static XUAType getValueByName(String name) throws IllegalArgumentException{
        for (XUAType value : values()) {
            String xuaType = value.getXuaType();
            if (xuaType.equals(name)) {
                return value;
            }
        }
        throw new IllegalArgumentException("xuaType " + name + "is not defined in XUAType");
    }*/
}
