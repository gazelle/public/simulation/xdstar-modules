package net.ihe.gazelle.xdstar.validator;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.edit.FileReadWrite;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.xdstar.validator.ws.DetailedResultTransformer;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

public class MetadataValidator {

	private static final String IHE = "ihe";
	private static final String EPSOS = "epsos";

	public static String extractNode(String ss, String nodeName, String namespace) throws JDOMException, IOException{
		SAXBuilder builder = new SAXBuilder();
		XMLOutputter outputter = new XMLOutputter();
		ByteArrayInputStream bais = new ByteArrayInputStream(ss.getBytes(StandardCharsets.UTF_8));
		Document document = (Document) builder.build(bais);
		Element rootNode = document.getRootElement();
		if (rootNode.getName().equals(nodeName)){
			return ss;
		}
		Element node = getChildFromSubChildren(rootNode, nodeName, namespace);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		if (node != null) {
			outputter.output(node, baos);
		}
		return baos.toString();
	}

	private static Element getChildFromSubChildren(Element rootNode, String nodeName, String namespace){
		List ll = rootNode.getChildren();
		for (Object object : ll) {
			Element node = (Element)object;
			if (node.getName().equals(nodeName) && node.getNamespace().getURI().equals(namespace)) return node;
			Element res = getChildFromSubChildren(node, nodeName, namespace);
			if (res != null) return res;
		}
		return null;
	}

	public static String extractSoapEnvelope(String ss){
		String res = "";
		Pattern p = Pattern.compile("<([A-Za-z0-9_]*:)?Envelope.*?Envelope>",Pattern.MULTILINE|Pattern.DOTALL);
		Matcher mm = p.matcher(ss);
		while (mm.find()){
			return mm.group();
		}
		return res;
	}

	public static String getDetailedResultAsString(DetailedResult res) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			DetailedResultTransformer.save(baos, res);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return baos.toString();
	}

	/*
	// -----------------------------------------------------------------

	public static void main(String[] args) throws IOException {
		String sub = FileReadWrite.readDoc("/Users/aboufahj/Documents/workspace/xds-model/samples/pnr2.xml");
		DetailedResult res = new DetailedResult();
		res = MetadataValidator.validateProvideAndRegisterRequest(sub, "ITI-41", "IHE");
		//MetadataValidator.validateCommonXDSMetadata(res, sub);
		System.out.println("res=" + MetadataValidator.getDetailedResultAsString(res));
	}
	*/

}
