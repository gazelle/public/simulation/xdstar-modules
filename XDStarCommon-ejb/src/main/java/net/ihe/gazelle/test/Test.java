package net.ihe.gazelle.test;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import net.ihe.gazelle.metadata.model.CardinalityEnum;
import net.ihe.gazelle.metadata.model.ClassificationMetaData;
import net.ihe.gazelle.metadata.model.SlotMetadata;
import net.ihe.gazelle.metadata.model.describer.ClassificationMetadataDescriber;
import net.ihe.gazelle.metadata.model.describer.SlotMetadataDescriber;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.sut.model.Usage;



public class Test {
	
	public static void main(String[] args) {
		
		SlotMetadataDescriber sd = new SlotMetadataDescriber();
		SlotMetadata s = new SlotMetadata();
		s.setAttribute("attribute");
		s.setDefaultValue("defaultValue");
		s.setId(1);
		s.setIsNumber(false);
		s.setMultiple(false);
		s.setName("name");
		s.setSupportAndOr(false);
		s.setValueset("Vs");
		sd.setSlotMetadata(s);
		sd.setCardinality(CardinalityEnum._01);
		
		ClassificationMetaData cm = new ClassificationMetaData();
		
		cm.setId(5);
		cm.setName("cmName");
		cm.setValue("cmValue");
		cm.setUuid("cmuuidd");
		cm.setDisplayAttributeName("cmdsppname");
		List<Usage> lu = new ArrayList<Usage>();
		Usage u = new Usage();
		u.setAction("sqd");
		Transaction transaction = new Transaction();
		u.setTransaction(transaction);
		transaction.setDescription("description");
		AffinityDomain affinity = new AffinityDomain();
		affinity.setId(31);
		u.setAffinity(affinity);
		lu.add(u);
		cm.setUsages(lu);
		List<ClassificationMetadataDescriber> classification = new ArrayList<ClassificationMetadataDescriber>();
		ClassificationMetadataDescriber cmd2 = new ClassificationMetadataDescriber();
		ClassificationMetaData cm2 = new ClassificationMetaData();
		cm2.setUuid("azfzaf");
		cmd2.setCmd(cm2);
		cmd2.setCardinality(CardinalityEnum._0star);
		List<SlotMetadataDescriber> slothash = new ArrayList<SlotMetadataDescriber>();
		slothash.add(sd);
			
		cm2.setSlotDescriber(slothash);
		ClassificationMetadataDescriber cmd3 = new ClassificationMetadataDescriber();
		ClassificationMetaData cm3 = new ClassificationMetaData();
		cm3.setUuid("gggggg");
		cmd3.setCmd(cm3);
		cmd3.setCardinality(CardinalityEnum._1star);
		
		classification.add(cmd2);
		classification.add(cmd3);

		cm.setClassificationDescriber(classification);
		
		
		JAXBContext jc;
		try {
			jc = JAXBContext.newInstance("net.ihe.gazelle.metadata.model");
			//jc = JAXBContext.newInstance(net.ihe.gazelle.metadata.model.RegistryObjectMetadata.class);
			Marshaller jaxbMarshaller = jc.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			File XMLfile = new File("output.xml");
			
			jaxbMarshaller.marshal(s, XMLfile); 
			jaxbMarshaller.marshal(cm, System.out); 

			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
				
	}

}
