/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.simulator.common.model;

import java.io.Serializable;
import java.util.Map;


/**
 *  <b>Class Description :  </b>DocumentContent<br><br>
 * This class describes the documents contained in the ITI-38 query response.
 * 
 *  This class is not present as a table in database
 *  
 * DocumentContent possesses the following attributes :
 * <ul>
 * <li><b>URI</b> : The URI of the returned document</li>
 * <li><b>repositoryUniqueId</b> : Id of the repository in which the document is stored (for retrieve operation)</li>
 * <li><b>documentUniqueId</b> : Unique id of the document in the specified repository</li>
 * <li><b>patientId</b> :  id of the patient linked to the document</li>
 * <li><b>name</b> :  name of the document referenced in the response</li>
 * <li><b>description</b> :  description of the document referenced in the response </li>
 * <li><b>version</b> :  version of the document referenced in the response </li>
 * <li><b>sourceId</b> :  source Id of the document referenced in the response </li>
 * <li><b>status</b> :  status of the document referenced in the response  </li>
 * <li><b>mimeType</b> :  mime type of the referenced document</li>
 * </ul></br>
 *
 * @class                   DocumentContent.java
 * @package        			net.ihe.gazelle.simulator.xca.initgw.model
 * @author 					Abderrazek Boufahja / IHE Europe
 * @author                	Anne-Gaëlle Bergé / INRIA Rennes IHE development Project
 * @version                	1.0 - 2010, October 4th
 *
 */

public class DocumentMetadata implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static String XDS_SUBMISSION_SET = "urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd";
	private static String XDS_FOLDER = "urn:uuid:d9d542f3-6cc4-48b6-8870-ea235fbc94c2";
	private static String XDS_DOCUMENT_ENTRY = "urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1";
	
	private String URI;
	private String repositoryUniqueId;
	private String documentUniqueId;
	private String patientId;
	private String name;
	private String description;
	private String version;
	private String status;
	private String mimeType;
	private String date;
	private String homeCommunityId;
	
	private Map<String, String> metadata;
	
	public DocumentMetadata(){
		
	}


	public String getURI() {
		return URI;
	}


	public void setURI(String uRI) {
		URI = uRI;
	}


	public String getRepositoryUniqueId() {
		return repositoryUniqueId;
	}


	public void setRepositoryUniqueId(String repositoryUniqueId) {
		this.repositoryUniqueId = repositoryUniqueId;
	}


	public String getDocumentUniqueId() {
		return documentUniqueId;
	}


	public void setDocumentUniqueId(String documentUniqueId) {
		this.documentUniqueId = documentUniqueId;
	}


	public String getPatientId() {
		return patientId;
	}


	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getVersion() {
		return version;
	}


	public void setVersion(String version) {
		this.version = version;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}


	public String getMimeType() {
		return mimeType;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public String getDate() {
		return date;
	}


	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}


	public Map<String, String> getMetadata() {
		return metadata;
	}


	public void setHomeCommunityId(String homeCommunityId) {
		this.homeCommunityId = homeCommunityId;
	}


	public String getHomeCommunityId() {
		return homeCommunityId;
	}

}
