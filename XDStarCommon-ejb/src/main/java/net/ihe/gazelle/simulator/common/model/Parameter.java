/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.simulator.common.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

/**
 * <b>Class Description : </b>Parameter<br>
 * <br>
 * This class defined the notion of query parameter (for a message type).
 * 
 * Parameter possesses the following attributes:
 * <ul>
 * <li><b>id</b> Id in the database</li>
 * <li><b>name</b> Name of the parameter as defined in the technical framework</li>
 * <li><b>uuid</b> UUID reprensenting the parameter (defined in technical
 * framework)</li>
 * <li><b>attribute</b> Attribute name</li>
 * <li><b>transaction</b> The transaction for which this message type is defined
 * </li>
 * <li><b>supportsAndOr</b> Indicates whether the parameter supports the AND/OR
 * semantics or not</li>
 * <li><b>multiple</b> Indicates whether the parameter can take several values
 * or not</li>
 * <li><b>defaultValue</b> default value if relevant</li>
 * </ul>
 * 
 * @class Parameter.java
 * @package net.ihe.gazelle.simulator.xca.initgw.model
 * @author 			Abderrazek Boufahja / IHE Europe
 * @author Anne-Gaëlle Bergé / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, November 18th
 * 
 */

@Entity
@Name("parameter")
@Table(name = "parameter", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "parameter_sequence", sequenceName = "parameter_id_seq", allocationSize = 1)
public class Parameter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	@GeneratedValue(generator = "parameter_sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "name")
	private String name;

	@Column(name = "uuid")
	private String uuid;

	@Column(name = "attribute")
	private String attribute;

	@Column(name = "supports_and_or")
	private Boolean supportsAndOr;

	@Column(name = "multiple")
	private Boolean multiple;

	// add default value (needed for Patient Service and Order Service)
	@Column(name = "default_value")
	private String defaultValue;

	@ManyToMany(mappedBy = "requiredParameters", targetEntity = MessageType.class, cascade = {
			CascadeType.PERSIST, CascadeType.MERGE })
	private List<MessageType> messageTypesWhereRequired;

	@ManyToMany(mappedBy = "optionalParameters", targetEntity = MessageType.class, cascade = {
			CascadeType.PERSIST, CascadeType.MERGE })
	private List<MessageType> messageTypesWhereOptional;

	/**
	 * Constructor
	 */

	public Parameter() {

	}

	/**
	 * Getters and Setters
	 */

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public Boolean getMultiple() {
		return multiple;
	}

	public void setMultiple(Boolean multiple) {
		this.multiple = multiple;
	}

	public void setSupportsAndOr(Boolean supportsAndOr) {
		this.supportsAndOr = supportsAndOr;
	}

	public Boolean getSupportsAndOr() {
		return supportsAndOr;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setMessageTypesWhereRequired(
			List<MessageType> messageTypesWhereRequired) {
		this.messageTypesWhereRequired = messageTypesWhereRequired;
	}

	public List<MessageType> getMessageTypesWhereRequired() {
		return messageTypesWhereRequired;
	}

	public void setMessageTypesWhereOptional(
			List<MessageType> messageTypesWhereOptional) {
		this.messageTypesWhereOptional = messageTypesWhereOptional;
	}

	public List<MessageType> getMessageTypesWhereOptional() {
		return messageTypesWhereOptional;
	}

	public static Parameter getParameterByNameByMessageType(
			String parameterName, MessageType messageType) {
		if ((parameterName != null) && (messageType != null)) {
			EntityManager em = (EntityManager) Component
					.getInstance("entityManager");
			// Query query =
			// em.createQuery("SELECT DISTINCT p FROM Parameter p " +
			// "JOIN p.messageTypesWhereRequired AS mtRequired " +
			// "JOIN p.messageTypesWhereOptional AS mtOptional " +
			// "WHERE p.name = '" + parameterName.trim() + "' " +
			// "AND (mtRequired.id = :messageTypeR OR mtOptional.id = :messageTypeO)");
			// query.setParameter("messageTypeR", messageType.getId());
			// query.setParameter("messageTypeO", messageType.getId());
			Query query = em.createQuery("SELECT DISTINCT p FROM Parameter p "
					+ "JOIN p.messageTypesWhereRequired AS mtRequired "
					+ "WHERE p.name = '" + parameterName.trim() + "' "
					+ "AND mtRequired.id = :messageTypeR");
			query.setParameter("messageTypeR", messageType.getId());

			List<Parameter> params = query.getResultList();
			if ((params != null) && !params.isEmpty()) {
				return params.get(0);
			} else {
				query = em.createQuery("SELECT DISTINCT p FROM Parameter p "
						+ "JOIN p.messageTypesWhereOptional AS mtOptional "
						+ "WHERE p.name = '" + parameterName.trim() + "' "
						+ "AND mtOptional.id = :messageTypeO");
				query.setParameter("messageTypeO", messageType.getId());
				params = query.getResultList();
				if ((params != null) && !params.isEmpty()) {
					return params.get(0);
				} else {
					return null;
				}
			}
		} else {
			return null;
		}
	}

	/**
	 * hashCode and equals
	 */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result)
				+ ((attribute == null) ? 0 : attribute.hashCode());
		result = (prime * result)
				+ ((defaultValue == null) ? 0 : defaultValue.hashCode());
		result = (prime * result)
				+ ((multiple == null) ? 0 : multiple.hashCode());
		result = (prime * result) + ((name == null) ? 0 : name.hashCode());
		result = (prime * result)
				+ ((supportsAndOr == null) ? 0 : supportsAndOr.hashCode());
		result = (prime * result) + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Parameter other = (Parameter) obj;
		if (attribute == null) {
			if (other.attribute != null) {
				return false;
			}
		} else if (!attribute.equals(other.attribute)) {
			return false;
		}
		if (defaultValue == null) {
			if (other.defaultValue != null) {
				return false;
			}
		} else if (!defaultValue.equals(other.defaultValue)) {
			return false;
		}
		if (multiple == null) {
			if (other.multiple != null) {
				return false;
			}
		} else if (!multiple.equals(other.multiple)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (supportsAndOr == null) {
			if (other.supportsAndOr != null) {
				return false;
			}
		} else if (!supportsAndOr.equals(other.supportsAndOr)) {
			return false;
		}
		if (uuid == null) {
			if (other.uuid != null) {
				return false;
			}
		} else if (!uuid.equals(other.uuid)) {
			return false;
		}
		return true;
	}

}
