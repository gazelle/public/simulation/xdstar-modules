/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.simulator.common.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

/**
 * <b>Class Description :  </b>MessageType<br><br>
 * This class defined the notion of message type.
 * 
 * MessageType possesses the following attributes: 
 * <ul>
 * <li><b>id</b> Id in the database</li>
 * <li><b>name</b> Name of the message type as defined in the technical framework</li>
 * <li><b>uuid</b> UUID reprensenting the message type (defined in technical framework)</li>
 * <li><b>action</b> SOAP Action related to the message (eg. urn:ihe:iti:2007:CrossGatewayQuery)</li>
 * <li><b>transaction</b> The transaction for which this message type is defined</li>
 * <li><b>requiredParameters</b> List of parameters which are mandatory (according TF)</li>
 * <li><b>optionalParameters</b> List of parameters which are optional (according TF)</li>
 * </ul>
 * 
 * @class	                MessageType.java
 * @package        			net.ihe.gazelle.simulator.xca.initgw.model
 * @author 					Abderrazek Boufahja / IHE Europe
 * @author                	Anne-Gaëlle Bergé / INRIA Rennes IHE development Project
 * @version                	1.0 - 2010, November 18th
 *
 */

@Entity
@Name("messageType")
@Table(name="message_type", schema = "public", uniqueConstraints=@UniqueConstraint(columnNames="id"))
@SequenceGenerator(name="message_type_sequence", sequenceName="message_type_id_seq", allocationSize=1)
public class MessageType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_DOCUMENTS = "FindDocuments";
	public static final String GET_DOCUMENTS = "GetDocuments";
	public static final String GET_RELATED = "GetRelatedDocuments";
	public static final String GET_SUBMISSION_SETS = "GetSubmissionSets";
	public static final String GET_ASSOCIATIONS = "GetAssociations";
	public static final String ORDER_SERVICE_LIST = "OrderService::list";
	public static final String PATIENT_SERVICE_LIST = "PatientService::list";
	public static final String PATIENT_SERVICE_RETRIEVE = "PatientService::retrieve";
	public static final String ORDER_SERVICE_RETRIEVE = "OrderService::retrieve";

	@Id
	@NotNull
	@GeneratedValue(generator="message_type_sequence", strategy=GenerationType.SEQUENCE)
	@Column(name="id", nullable = false, unique = true)
	private Integer id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="uuid")
	private String uuid;
	
	@Column(name="action")
	private String action;
	
	@ManyToOne
	@JoinColumn(name="transaction_id")
	private Transaction transaction;
	
	@ManyToMany(targetEntity=Parameter.class,
			cascade={CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name="message_type_required_parameters", 
			joinColumns=@JoinColumn(name="message_type_id"), 
			inverseJoinColumns=@JoinColumn(name="parameter_id"), 
			uniqueConstraints=@UniqueConstraint(columnNames={"message_type_id", "parameter_id"}))
	private List<Parameter> requiredParameters;
	
	@ManyToMany(targetEntity=Parameter.class,
			cascade={CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name="message_type_optional_parameters",
			joinColumns=@JoinColumn(name="message_type_id"), 
			inverseJoinColumns=@JoinColumn(name="parameter_id"),
			uniqueConstraints=@UniqueConstraint(columnNames={"message_type_id", "parameter_id"}))
	private List<Parameter> optionalParameters;
	

	/**
	 * Constructor
	 * 
	 */
	public MessageType()
	{
		
	}


	/*****************************************************************************
	 * 
	 * Getters and Setters
	 * 
	 ******************************************************************************/
	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getUuid() {
		return uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	public void setAction(String action) {
		this.action = action;
	}


	public String getAction() {
		return action;
	}


	public Transaction getTransaction() {
		return transaction;
	}


	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}


	public List<Parameter> getRequiredParameters() {
		return requiredParameters;
	}


	public void setRequiredParameters(List<Parameter> requiredParameters) {
		this.requiredParameters = requiredParameters;
	}


	public List<Parameter> getOptionalParameters() {
		if (this.optionalParameters == null){
			EntityManager em = (EntityManager) Component.getInstance("entityManager");
			MessageType mm = em.find(MessageType.class, this.id);
			this.optionalParameters = mm.getOptionalParameters();
		}
		return optionalParameters;
	}


	public void setOptionalParameters(List<Parameter> optionalParameters) {
		this.optionalParameters = optionalParameters;
	}



	/************************************************************************
	 * Public static methods
	 ************************************************************************/
	/**
	 * Returns the list of message types available for a given transaction
	 * @param inTransaction
	 * @return
	 */
	public static List<MessageType> getMessageTypesForTransaction(Transaction inTransaction)
	{
		if (inTransaction == null)
		{
			return null;
		}
		else
		{
			EntityManager em = (EntityManager) Component.getInstance("entityManager");
			Session session = (Session) em.getDelegate();
			Criteria criteria = session.createCriteria(MessageType.class);
			criteria.add(Restrictions.eq("transaction", inTransaction));
			criteria.addOrder(Order.asc("name"));
			List<MessageType> messageTypes = criteria.list();
			if (messageTypes != null && !messageTypes.isEmpty())
				return messageTypes;
			else
				return null;
		}
	}
	
	/**
	 * Returns the message type by its name
	 * @param inMessageTypeName
	 * @param inTransaction TODO
	 * @return
	 */
	public static MessageType getMessageTypeByNameByTransaction(String inMessageTypeName, Transaction inTransaction)
	{
		if (inMessageTypeName == null || inMessageTypeName.isEmpty())
			return null;
		else
		{
			HQLQueryBuilder<MessageType> hh = new HQLQueryBuilder<MessageType>(MessageType.class);
			hh.addLike("name", inMessageTypeName.trim());
			hh.addEq("transaction", inTransaction);
			List<MessageType> messageTypes = hh.getList();
			if ((messageTypes != null) && (messageTypes.size()>0)){
				return messageTypes.get(0);
			}
			return null;
			
			
//			Session session = (Session) em.getDelegate();
//			Criteria criteria = session.createCriteria(MessageType.class);
//			criteria.add(Restrictions.ilike("name", inMessageTypeName.trim()));
//			criteria.add(Restrictions.eq("transaction", inTransaction));
//			List<MessageType> messageTypes = criteria.list();
//			if (messageTypes != null && !messageTypes.isEmpty())
//				return messageTypes.get(0);
//			else
//				return null;
		}
	}


	/****************************************************************************
	 * HashCode and equals
	 ****************************************************************************/
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime
				* result
				+ ((optionalParameters == null) ? 0 : optionalParameters
						.hashCode());
		result = prime
				* result
				+ ((requiredParameters == null) ? 0 : requiredParameters
						.hashCode());
		result = prime * result
				+ ((action == null) ? 0 : action.hashCode());
		result = prime * result
				+ ((transaction == null) ? 0 : transaction.hashCode());
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MessageType other = (MessageType) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (optionalParameters == null) {
			if (other.optionalParameters != null)
				return false;
		} else if (!optionalParameters.equals(other.optionalParameters))
			return false;
		if (requiredParameters == null) {
			if (other.requiredParameters != null)
				return false;
		} else if (!requiredParameters.equals(other.requiredParameters))
			return false;
		if (action == null) {
			if (other.action != null)
				return false;
		} else if (!action.equals(other.action))
			return false;
		if (transaction == null) {
			if (other.transaction != null)
				return false;
		} else if (!transaction.equals(other.transaction))
			return false;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}


	
	
}
