/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.simulator.common.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * <b>Class Description :  </b>ParameterValuesForRequest<br><br>
 * This class defined the notion of query parameter values. This object is not represented as a table in database.
 * This object is used to manage the query parameter values customed by the user when he/she creates the request
 * 
 * ParameterValuesForRequest possesses the following attributes: 
 * <ul>
 * <li><b>parameter</b> Parameter to use</li>
 * <li><b>values</b> List of the values taken by the parameter in the request to build</li>
 * <li><b>operation</b> Indicates whether the values are separate by a AND or a OR</li>
 * <li><b>required</b> Indicates whether the parameter is required for the selected message type or not (used for displayed purposes)</li>
 * <li><b>newValue</b> The new value to append to the list</li>
 * </ul>
 * 
 * @class	                ParameterValuesForRequest.java
 * @package        			net.ihe.gazelle.simulator.xca.initgw.model
 * @author 					Abderrazek Boufahja / IHE Europe
 * @author                	Anne-Gaëlle Bergé / INRIA Rennes IHE development Project
 * @version                	1.0 - 2010, November 18th
 *
 */
public class ParameterValuesForRequest implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String OP_OR = "OR";
	public static final String OP_AND = "AND";
	
	
	private Parameter parameter;
	private List<String> values;
	private String operation;
	private boolean required;
	private String newValue;
	
	public ParameterValuesForRequest()
	{
		this.values = new ArrayList<String>();
	}
	
	public ParameterValuesForRequest(Parameter parameter, List<String> values)
	{
		this.parameter = parameter;
		this.values = values;
		this.operation = OP_OR;
	}
	
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getOperation() {
		return operation;
	}
	public void setValues(List<String> values) {
		this.values = values;
	}
	public List<String> getValues() {
		return values;
	}
	public void setParameter(Parameter parameter) {
		this.parameter = parameter;
	}
	public Parameter getParameter() {
		return parameter;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public boolean isRequired() {
		return required;
	}

	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	public String getNewValue() {
		return newValue;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((newValue == null) ? 0 : newValue.hashCode());
		result = prime * result
				+ ((operation == null) ? 0 : operation.hashCode());
		result = prime * result
				+ ((parameter == null) ? 0 : parameter.hashCode());
		result = prime * result + (required ? 1231 : 1237);
		result = prime * result + ((values == null) ? 0 : values.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParameterValuesForRequest other = (ParameterValuesForRequest) obj;
		if (newValue == null) {
			if (other.newValue != null)
				return false;
		} else if (!newValue.equals(other.newValue))
			return false;
		if (operation == null) {
			if (other.operation != null)
				return false;
		} else if (!operation.equals(other.operation))
			return false;
		if (parameter == null) {
			if (other.parameter != null)
				return false;
		} else if (!parameter.equals(other.parameter))
			return false;
		if (required != other.required)
			return false;
		if (values == null) {
			if (other.values != null)
				return false;
		} else if (!values.equals(other.values))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ParameterValuesForRequest [parameter=" + parameter
				+ ", values=" + values + ", operation=" + operation
				+ ", required=" + required + ", newValue=" + newValue + "]";
	}
	
	

}
