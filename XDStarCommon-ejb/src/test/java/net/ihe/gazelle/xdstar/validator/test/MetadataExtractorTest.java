package net.ihe.gazelle.xdstar.validator.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Method;

import net.ihe.gazelle.edit.FileReadWrite;
import net.ihe.gazelle.lcm.RemoveObjectsRequestType;
import net.ihe.gazelle.lcm.SubmitObjectsRequestType;
import net.ihe.gazelle.query.AdhocQueryRequestType;
import net.ihe.gazelle.query.AdhocQueryResponseType;
import net.ihe.gazelle.query.ReturnTypeType;
import net.ihe.gazelle.rs.RegistryResponseType;
import net.ihe.gazelle.xds.ProvideAndRegisterDocumentSetRequestType;
import net.ihe.gazelle.xds.RetrieveDocumentSetRequestType;
import net.ihe.gazelle.xds.RetrieveDocumentSetResponseType;
import net.ihe.gazelle.xdsi.RetrieveImagingDocumentSetRequestType;
import net.ihe.gazelle.xdstar.validator.MetadataExtractor;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MetadataExtractorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetAdhocQueryRequestTypeFromXml() {
		try{
			Method method = MetadataExtractor.class.getDeclaredMethod("getAdhocQueryRequestTypeFromXml", String.class);
			method.setAccessible(true);
			String xml = FileReadWrite.readDoc("src/test/resources/docsamples/1.xml");
			AdhocQueryRequestType res = (AdhocQueryRequestType) method.invoke(MetadataExtractor.class, xml);
			assertTrue(res != null && res.getResponseOption() != null && res.getResponseOption().getReturnType() != null 
				&& res.getResponseOption().getReturnType().equals(ReturnTypeType.LEAFCLASS));
		}
		catch(Exception e){
			e.printStackTrace();
			fail("fail to execute the test testGetAdhocQueryRequestTypeFromXml");
		}
	}

	@Test
	public void testGetAdhocQueryResponseTypeFromXml() {
		try{
			Method method = MetadataExtractor.class.getDeclaredMethod("getAdhocQueryResponseTypeFromXml", String.class);
			method.setAccessible(true);
			String xml = FileReadWrite.readDoc("src/test/resources/docsamples/2.xml");
			AdhocQueryResponseType res = (AdhocQueryResponseType) method.invoke(MetadataExtractor.class, xml);
			assertTrue(res != null && res.getRegistryObjectList() != null && res.getStatus() != null 
				&& res.getStatus().equals("urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success"));
		}
		catch(Exception e){
			e.printStackTrace();
			fail("fail to execute the test testGetAdhocQueryRequestTypeFromXml");
		}
	}

	@Test
	public void testGetSubmitObjectsRequestTypeFromXml() {
		try{
			Method method = MetadataExtractor.class.getDeclaredMethod("getSubmitObjectsRequestTypeFromXml", String.class);
			method.setAccessible(true);
			String xml = FileReadWrite.readDoc("src/test/resources/docsamples/3.xml");
			SubmitObjectsRequestType res = (SubmitObjectsRequestType) method.invoke(MetadataExtractor.class, xml);
			System.out.println(res.getRegistryObjectList().getObjectRef().size());
			assertTrue(res != null && res.getRegistryObjectList() != null &&  res.getRegistryObjectList().getAssociation() != null 
					&& (res.getRegistryObjectList().getAssociation().size() == 3));
		}
		catch(Exception e){
			e.printStackTrace();
			fail("fail to execute the test testGetAdhocQueryRequestTypeFromXml");
		}
	}

	@Test
	public void testGetProvideAndRegisterDocumentSetRequestTypeFromXml() {
		try{
			Method method = MetadataExtractor.class.getDeclaredMethod("getProvideAndRegisterDocumentSetRequestTypeFromXml", String.class);
			method.setAccessible(true);
			String xml = FileReadWrite.readDoc("src/test/resources/docsamples/4.xml");
			ProvideAndRegisterDocumentSetRequestType res = (ProvideAndRegisterDocumentSetRequestType) method.invoke(MetadataExtractor.class, xml);
			assertTrue(res != null && res.getSubmitObjectsRequest() != null && res.getDocument().size()==2  
					&& res.getSubmitObjectsRequest().getRegistryObjectList() != null 
					&& res.getSubmitObjectsRequest().getRegistryObjectList().getAssociation() != null 
					&& (res.getSubmitObjectsRequest().getRegistryObjectList().getAssociation().size() == 3));
		}
		catch(Exception e){
			e.printStackTrace();
			fail("fail to execute the test testGetAdhocQueryRequestTypeFromXml");
		}
	}

	@Test
	public void testGetRetrieveDocumentSetRequestTypeFromXml() {
		try{
			Method method = MetadataExtractor.class.getDeclaredMethod("getRetrieveDocumentSetRequestTypeFromXml", String.class);
			method.setAccessible(true);
			String xml = FileReadWrite.readDoc("src/test/resources/docsamples/5.xml");
			RetrieveDocumentSetRequestType res = (RetrieveDocumentSetRequestType) method.invoke(MetadataExtractor.class, xml);
			assertTrue(res != null && res.getDocumentRequest() != null && res.getDocumentRequest().size()==1  
					&& res.getDocumentRequest().get(0).getDocumentUniqueId() != null 
					&& res.getDocumentRequest().get(0).getDocumentUniqueId().equals("190552_3269_3269_20120731200359000000_516380_514985"));
		}
		catch(Exception e){
			e.printStackTrace();
			fail("fail to execute the test testGetAdhocQueryRequestTypeFromXml");
		}
	}

	@Test
	public void testGetRetrieveDocumentSetResponseTypeFromXml() {
		try{
			Method method = MetadataExtractor.class.getDeclaredMethod("getRetrieveDocumentSetResponseTypeFromXml", String.class);
			method.setAccessible(true);
			String xml = FileReadWrite.readDoc("src/test/resources/docsamples/6.xml");
			RetrieveDocumentSetResponseType res = (RetrieveDocumentSetResponseType) method.invoke(MetadataExtractor.class, xml);
			assertTrue(res != null && res.getRegistryResponse() != null && res.getRegistryResponse().getStatus() != null  
					&& res.getRegistryResponse().getStatus().equals("urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success"));
		}
		catch(Exception e){
			e.printStackTrace();
			fail("fail to execute the test testGetAdhocQueryRequestTypeFromXml");
		}
	}

	@Test
	public void testGetRegistryResponseTypeFromXml() {
		try{
			Method method = MetadataExtractor.class.getDeclaredMethod("getRegistryResponseTypeFromXml", String.class);
			method.setAccessible(true);
			String xml = FileReadWrite.readDoc("src/test/resources/docsamples/7.xml");
			RegistryResponseType res = (RegistryResponseType) method.invoke(MetadataExtractor.class, xml);
			assertTrue(res != null && res.getStatus() != null 
					&& res.getStatus().equals("urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success"));
		}
		catch(Exception e){
			e.printStackTrace();
			fail("fail to execute the test testGetAdhocQueryRequestTypeFromXml");
		}
	}

	@Test
	public void testGetRemoveObjectsRequestTypeFromXml() {
		try{
			Method method = MetadataExtractor.class.getDeclaredMethod("getRemoveObjectsRequestTypeFromXml", String.class);
			method.setAccessible(true);
			String xml = FileReadWrite.readDoc("src/test/resources/docsamples/8.xml");
			RemoveObjectsRequestType res = (RemoveObjectsRequestType) method.invoke(MetadataExtractor.class, xml);
			assertTrue(res != null && res.getObjectRefList() != null 
					&& res.getObjectRefList().getObjectRef().size()==4);
		}
		catch(Exception e){
			e.printStackTrace();
			fail("fail to execute the test testGetAdhocQueryRequestTypeFromXml");
		}
	}

	@Test
	public void testGetRetrieveImagingDocumentSetRequestTypeFromXml() {
		try{
			Method method = MetadataExtractor.class.getDeclaredMethod("getRetrieveImagingDocumentSetRequestTypeFromXml", String.class);
			method.setAccessible(true);
			String xml = FileReadWrite.readDoc("src/test/resources/docsamples/9.xml");
			RetrieveImagingDocumentSetRequestType res = (RetrieveImagingDocumentSetRequestType) method.invoke(MetadataExtractor.class, xml);
			assertTrue(res != null && res.getStudyRequest() != null 
					&& res.getTransferSyntaxUIDList() != null);
		}
		catch(Exception e){
			e.printStackTrace();
			fail("fail to execute the test testGetAdhocQueryRequestTypeFromXml");
		}
	}

}
