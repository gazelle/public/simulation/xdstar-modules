package net.ihe.gazelle.xdstar.core.test;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.xml.soap.SOAPEnvelope;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import net.ihe.gazelle.xdstar.common.model.AttachmentFile;
import net.ihe.gazelle.xdstar.core.AttachmentsUtil;

import org.apache.axiom.attachments.Attachments;
import org.apache.axiom.attachments.IncomingAttachmentInputStream;
import org.apache.axiom.attachments.IncomingAttachmentStreams;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMOutputFormat;
import org.apache.axiom.om.OMXMLParserWrapper;
import org.apache.axiom.om.impl.builder.StAXBuilder;
import org.apache.axiom.om.util.StAXUtils;
import org.apache.axiom.soap.SOAP12Constants;
import org.apache.axiom.soap.impl.builder.MTOMStAXSOAPModelBuilder;
import org.apache.axiom.soap.impl.builder.StAXSOAPModelBuilder;
import org.apache.axis2.AxisFault;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OMElemenTest {
	
	
	private static Logger log = LoggerFactory.getLogger(OMElemenTest.class);
	
	   /**
     * @param inputStream Reads input stream that use to build OMElement
     * @return  OMElement that generated from input stream
     * @throws Exception at error in generating parser
     */
	public static SOAPEnvelope createSOAPEnvelope(InputStream in) throws AxisFault {
	       try {
	           XMLStreamReader xmlreader =
	                   StAXUtils.createXMLStreamReader(in);
	           StAXBuilder builder = new StAXSOAPModelBuilder(xmlreader, null);
	           return (SOAPEnvelope) builder.getDocumentElement();
	       } catch (Exception e) {
	           throw new AxisFault(e.getMessage(), e);
	       }
	   }
	
	@Test
	public void testMultipar1() throws IOException {
		String contentTypeString =
                "multipart/Related; charset=\"UTF-8\"; type=\"application/xop+xml\"; boundary=\"----=_AxIs2_Def_boundary_=42214532\"; start=\"SOAPPart\"";
        InputStream inStream = new FileInputStream(
        		"src/test/resources/MTOMBuilderTestIn.txt");
        Attachments attachments = new Attachments(inStream, contentTypeString);
        //System.out.println(attachments.getAllContentIDs().length);
        IncomingAttachmentStreams tt = attachments.getIncomingAttachmentStreams();
        while (tt.isReadyToGetNextStream()){
        	IncomingAttachmentInputStream oo = tt.getNextStream();
        	
        	if (oo != null){
        		System.out.println(oo.getContentType());
        		assertTrue(oo.getContentType().equals("binary"));
	        	FileOutputStream out = new FileOutputStream("hola.txt");
	        	int i = IOUtils.copy(oo, out);
	        	System.out.println(i);
	        	oo.close();
	        	out.close();
        	}
        	else{
        		break;
        	}
        }
	}
	
	@Test
	public void testMultipar2() throws IOException {
		String contentTypeString =
                "multipart/related; boundary=MIMEBoundaryurn_uuid_5B540A5CFFED43839A089713F2BB98F4; type=\"application/xop+xml\"; start=\"<0.urn:uuid:C8E6C69188F94832AE161797ADAF7791@ws.jboss.org>\"; start-info=\"application/soap+xml\"; action=\"urn:ihe:iti:2007:RetrieveDocumentSet\"";
        InputStream inStream = new FileInputStream(
        		"src/test/resources/MTOMBuilderTestIn2.txt");
        Attachments attachments = new Attachments(inStream, contentTypeString);
        String soap = AttachmentsUtil.transformMultipartToSOAP(attachments);
        List<AttachmentFile> toto = AttachmentsUtil.generateListAttachmentFiles(attachments, soap);
        assertTrue(toto == null);
	}
	
	@Test
	public void testMultipar3() throws IOException {
		String contentTypeString =
                "multipart/Related; charset=\"UTF-8\"; type=\"application/xop+xml\"; boundary=\"----=_AxIs2_Def_boundary_=42214532\"; start=\"SOAPPart\"";
        InputStream inStream = new FileInputStream(
        		"src/test/resources/MTOMBuilderTestIn.txt");
        Attachments attachments = new Attachments(inStream, contentTypeString);
        String soap = AttachmentsUtil.transformMultipartToSOAP(attachments);
        List<AttachmentFile> toto = AttachmentsUtil.generateListAttachmentFiles(attachments, soap, false);
        assertTrue(toto.size()==1);
        
	}
    
    public static void main1(String[] args) throws FileNotFoundException, Exception {
    	String contentTypeString =
                "multipart/Related; charset=\"UTF-8\"; type=\"application/xop+xml\"; boundary=\"----=_AxIs2_Def_boundary_=42214532\"; start=\"SOAPPart\"";
        InputStream inStream = new FileInputStream(
        		"src/test/resources/MTOMBuilderTestIn.txt");
        Attachments attachments = new Attachments(inStream, contentTypeString);
        XMLStreamReader reader = XMLInputFactory.newInstance()
                .createXMLStreamReader(new BufferedReader(new InputStreamReader(attachments
                        .getSOAPPartInputStream())));
        OMXMLParserWrapper builder = new MTOMStAXSOAPModelBuilder(reader, attachments,
                                               SOAP12Constants.SOAP_ENVELOPE_NAMESPACE_URI);
        OMElement root = builder.getDocumentElement();

        InputStream rr = attachments.getSOAPPartInputStream();
        System.out.println(transformInputToString(rr));
        System.out.println("----------");
        OMOutputFormat format = new OMOutputFormat();
        format.setDoOptimize(false);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        root.serializeAndConsume(baos, format);
        String msg = baos.toString();
        System.out.println(msg);
	}
    
    private static String transformInputToString(InputStream inputStream){
		try{
			BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
			
			StringBuffer response = new StringBuffer();
			String line = null;
			response.append(in.readLine());
			while((line = in.readLine()) != null)
			{
				response.append(line);
				response.append("\n");
			}
			in.close();
			return response.toString();
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
    

}
