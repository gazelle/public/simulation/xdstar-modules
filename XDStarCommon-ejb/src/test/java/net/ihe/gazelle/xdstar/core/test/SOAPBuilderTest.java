package net.ihe.gazelle.xdstar.core.test;

import java.io.FileInputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

import javax.crypto.Cipher;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import net.ihe.gazelle.simulator.common.xua.SignatureException;

import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.transforms.Transforms;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SOAPBuilderTest {

	@Test
	public void testGetTransaction() {
		//fail("Not yet implemented");
	}

	@Test
	public void testSetTransaction() {
		//fail("Not yet implemented");
	}

	@Test
	public void testGetMessageType() {
		//fail("Not yet implemented");
	}

	@Test
	public void testSetMessageType() {
		//fail("Not yet implemented");
	}

	@Test
	public void testGetReturnType() {
		//fail("Not yet implemented");
	}

	@Test
	public void testSetReturnType() {
		//fail("Not yet implemented");
	}

	@Test
	public void testIsXUA() {
		//fail("Not yet implemented");
	}

	@Test
	public void testSetXUA() {
		//fail("Not yet implemented");
	}

	@Test
	public void testGetConfiguration() {
		//fail("Not yet implemented");
	}

	@Test
	public void testSetConfiguration() {
		//fail("Not yet implemented");
	}

	@Test
	public void testGetSamlAssertionAttributes() {
		//fail("Not yet implemented");
	}

	@Test
	public void testSetSamlAssertionAttributes() {
		//fail("Not yet implemented");
	}

	@Test
	public void testSetPraticianID() {
		//fail("Not yet implemented");
	}

	@Test
	public void testGetPraticianID() {
		//fail("Not yet implemented");
	}

	@Test
	public void testSetStsEndpoint() {
		//fail("Not yet implemented");
	}

	@Test
	public void testGetStsEndpoint() {
		//fail("Not yet implemented");
	}

	@Test
	public void testGetParameters() {
		//fail("Not yet implemented");
	}

	@Test
	public void testIsUseTRC() {
		//fail("Not yet implemented");
	}

	@Test
	public void testSetUseTRC() {
		//fail("Not yet implemented");
	}

	@Test
	public void testSetParameters() {
		//fail("Not yet implemented");
	}

	@Test
	public void testSetPatientId() {
		//fail("Not yet implemented");
	}

	@Test
	public void testGetPatientId() {
		//fail("Not yet implemented");
	}

	@Test
	public void testSOAPBuilder() {
		//fail("Not yet implemented");
	}

	@Test
	public void testBuildMessage() {
		//fail("Not yet implemented");
	}

	@Test
	public void testRequestAssertion(){

	}

	public void testGetSignatureUtil() throws ParserConfigurationException, XMLSecurityException, SignatureException{
		String keyStorePass = "gazelle";
		String keyAlias = "gazelle";
		String keyPass = "gazelle";
		String simuKeystorePath = "/opt/truststore.epsos.jks";
		Certificate certificate = null;
		SignatureUtil signature = null;
		try {
			signature =new SignatureUtil(simuKeystorePath, keyStorePass, keyAlias, keyPass);
			certificate = signature.getKeyStore().getCertificate(keyAlias);
			System.out.println(certificate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
		DocumentBuilder constructeur = fabrique.newDocumentBuilder();
		Document result = constructeur.newDocument();
		XMLSignature xmlSignature = new XMLSignature(result, "", "http://www.w3.org/2000/09/xmldsig#rsa-sha1",
				"http://www.w3.org/2001/10/xml-exc-c14n#");
		// add it before sign, as an enveloped signature
		Element signatureElement = xmlSignature.getElement();
		appendSignature(result, "ii", signatureElement);

		Transforms transforms = new Transforms(result);
		transforms.addTransform("http://www.w3.org/2000/09/xmldsig#enveloped-signature");
		transforms.addTransform("http://www.w3.org/2001/10/xml-exc-c14n#");
		xmlSignature.addDocument("#" + "ii", transforms);

		if (certificate != null && certificate instanceof X509Certificate) {
			xmlSignature.addKeyInfo((X509Certificate) certificate);
		}

		PrivateKey privateKey = null;
		try {
			privateKey = (PrivateKey) signature.getKeyStore().getKey(keyAlias, signature.getKeyPassword().toCharArray());
		} catch (Exception e) {
			throw new SignatureException("Invalid keystore : //failed to load credentials", e);
		}

		xmlSignature.sign(privateKey);
	}

	private void appendSignature(Document document, String id, Element signatureElement) {
		Node nextSibling = null;

		Element elementToBeSigned = getElement(document, id);

		if (elementToBeSigned != null) {
			NodeList childNodes = elementToBeSigned.getChildNodes();
			if (childNodes != null) {
				for (int i = 0; i < childNodes.getLength(); i++) {
					if (childNodes.item(i) instanceof Element) {
						String localName = ((Element) childNodes.item(i)).getLocalName();
						if ("Subject".equals(localName)) {
							nextSibling = childNodes.item(i);
						}
					}
				}
			}
		}
		if (nextSibling == null) {
			elementToBeSigned.appendChild(signatureElement);
		} else {
			elementToBeSigned.insertBefore(signatureElement, nextSibling);
		}
	}

	private Element getElement(Node node, String id) {
		if (node instanceof Element) {
			Element testElement = (Element) node;
			if (testElement.hasAttribute("ID") && testElement.getAttribute("ID").equals(id)) {
				return testElement;
			}
		}
		NodeList childNodes = node.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Element testElement = getElement(childNodes.item(i), id);
			if (testElement != null) {
				return testElement;
			}
		}
		return null;
	}
	
	public void tt1() throws SignatureException{
		String keyStorePass = "gazelle";
		String keyAlias = "gazelle";
		String keyPass = "gazelle";
		String simuKeystorePath = "/opt/truststore.epsos.jks";
		Certificate certificate = null;
		SignatureUtil signature = null;
		try {
			signature =new SignatureUtil(simuKeystorePath, keyStorePass, keyAlias, keyPass);
			certificate = signature.getKeyStore().getCertificate(keyAlias);
			System.out.println(certificate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		PrivateKey privateKey = null;
		try {
			privateKey = (PrivateKey) signature.getKeyStore().getKey(keyAlias, signature.getKeyPassword().toCharArray());
		} catch (Exception e) {
			throw new SignatureException("Invalid keystore : failed to load credentials", e);
		}
	}
	

	@Test
	public void tt() throws Exception{
		KeyPair keyPair = KeyPairGenerator.getInstance("RSA").generateKeyPair();
		PrivateKey privateKey = keyPair.getPrivate();
		String plaintext = "This is the message being signed";

		// Compute signature
		Signature instance = Signature.getInstance("SHA1withRSA");
		instance.initSign(privateKey);
		instance.update((plaintext).getBytes());
		byte[] signature = instance.sign();

		// Compute digest
		MessageDigest sha1 = MessageDigest.getInstance("SHA1");
		byte[] digest = sha1.digest((plaintext).getBytes());

		// Encrypt digest
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, privateKey);
		byte[] cipherText = cipher.doFinal(digest);

		// Display results
		System.out.println("Input data: " + plaintext);
		System.out.println("Digest: " + bytes2String(digest));
		System.out.println("Cipher text: " + bytes2String(cipherText));
		System.out.println("Signature: " + bytes2String(signature));
	}
	
	private static String bytes2String(byte[] bytes) {
	    StringBuilder string = new StringBuilder();
	    for (byte b : bytes) {
	        String hexString = Integer.toHexString(0x00FF & b);
	        string.append(hexString.length() == 1 ? "0" + hexString : hexString);
	    }
	    return string.toString();
	}
	public void testSign() throws Exception{
		DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();
		DocumentBuilder constructeur = fabrique.newDocumentBuilder();
		Document document = constructeur.newDocument();
		Element personne = document.createElement("personne");
		personne.setAttribute("id","0");
		document.appendChild(personne);
		sign(document, "/opt/truststore.epsos.jks", "gazelle", "gazelle", "gazelle");
		String out = net.ihe.gazelle.simulator.common.utils.XmlUtil.outputDOM(document);
		System.out.println(out);
	}


	private static void sign(Document doc, String keystore,
				String storepass, String alias, String keypass) throws
			Exception
			{
		try
		{
			//Add this header to the SOAP message if it does not exist
			String soap_header =
					"http://schemas.xmlsoap.org/soap/envelope/";
			// Initialize the library
			org.apache.xml.security.Init.init();

			/******************* XML SIGNATURE INIT ***********************
			Append the signature element to proper location before signing
			 ************************************************** *************/
			// Look for the SOAP header
			Element headerElement = null;
			NodeList nodes = doc.getElementsByTagNameNS (soap_header,
					"Header");
			//No nodes are expected to be found (length of zero) - add
			//header here.
			if(nodes.getLength() == 0)
			{
				System.out.println("Adding a SOAP Header Element");
				headerElement = doc.createElementNS (soap_header, "Header");
				nodes = doc.getElementsByTagNameNS (soap_header, "Envelope");
				if(nodes != null)
				{
					Element envelopeElement = (Element)nodes.item(0);
					headerElement.setPrefix(envelopeElement.getPrefix( ));
					envelopeElement.appendChild(headerElement);
				}
			}
			else
			{
				//This shouldn't happen unless explicity done elsewhere
				System.out.println("Found " + nodes.getLength() + " SOAP Header elements.");
				headerElement = (Element)nodes.item(0);
			}

			// http://xml-security is the base-uri, which needs to be
			//unique within document
			XMLSignature sig = new XMLSignature(doc, "http://xml-security",
					XMLSignature.ALGO_ID_SIGNATURE_DSA);
			// Add SOAP Body to XML Signature
			headerElement.appendChild(sig.getElement());
			// Due to the bug in the Apache security lib, it does not allow
			// us to sign whole message and make "enveloped-signature"
			// transform - strictly part of the specification.
			// Only sign the body - IT IS NOT CONFORMED TO THE SPEC!!!!!!
			//
			// Neat trick: since the XMLSignature is actually a part of the
			// SOAP XML document, the SOAP body is referenced as a URI
			// fragment
			sig.addDocument("#Body");
			/******************* END XML SIGNATURE INIT
			 *********************/

			/******************* X.509 Certificate INIT *****************/
			FileInputStream fis = new FileInputStream(keystore);
			java.security.KeyStore ks =
					java.security.KeyStore.getInstance(KeyStore.getDefaultType());
			ks.load(fis, storepass.toCharArray());

			// commented out for obvious reasons
			//System.err.println("ClientSecurity::sign -- alias: " + alias);
			//System.err.println("ClientSecurity::sign -- keypass: " + keypass);
			PrivateKey privateKey = (PrivateKey)ks.getKey(alias,
					keypass.toCharArray());
			X509Certificate cert =
					(X509Certificate)ks.getCertificate(alias);
			/***************** END X.509 Certificate INIT
			 *******************/

			/****************** SIGN THE ****ER *******************/
			sig.addKeyInfo(cert);
			sig.addKeyInfo(cert.getPublicKey());
			// Sign the XML Signature document with our private key
			sig.sign(privateKey);
			/****************** ****ER SIGNED *********************/
		}
		catch (Exception e)
		{
			System.err.println("ClientSecurity::sign -- Exception: ");
			e.printStackTrace();
		}
			}
}
