package net.ihe.gazelle.constraint.gen;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.ejb.Ejb3Configuration;

public class HibernateUtil {

	/******************************/
	private static EntityManagerFactory entityManagerFactory;
	private static final Object emfSync = new Object();

	/**
	 * begins a transaction and returns the attached entityManager configured with file 
	 * /Users/aboufahj/Documents/workspace/XDStar-modules/XDStarCommon-ejb/src/main/test/hibernate.cfg.xml
	 * @return
	 */
	public static EntityManager buildEntityManager() {
		if (entityManagerFactory == null) {
			synchronized (emfSync) 
			{
				if (entityManagerFactory == null) 
				{
					Ejb3Configuration cfg = new Ejb3Configuration();
					cfg.configure("META-INF/hibernate.cfg.xml");
					entityManagerFactory = cfg.createEntityManagerFactory();
				}
			}
		}

		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		return entityManager;
	}
	
	/**
	 * Create the entityManagerFactory from the hibernate config file
	 * @param hibernateConfigFilePath: default is /Users/aboufahj/Documents/workspace/XDStar-modules/XDStarCommon-ejb/src/main/test/hibernate.cfg.xml
	 * @return
	 */
	public static EntityManagerFactory createEntityManagerFactory(String hibernateConfigFilePath) throws HibernateException
	{
		if (hibernateConfigFilePath == null || hibernateConfigFilePath.isEmpty())
		{
			hibernateConfigFilePath = "/Users/aboufahj/Documents/workspace/XDStar-modules/XDStarCommon-ejb/src/main/test/hibernate.cfg.xml";
		}
		if (entityManagerFactory == null) {
			synchronized (emfSync) 
			{
				if (entityManagerFactory == null) 
				{
					Ejb3Configuration cfg = new Ejb3Configuration();
					cfg.configure(hibernateConfigFilePath);
					entityManagerFactory = cfg.createEntityManagerFactory();
				}
			}
		}
		return entityManagerFactory;
	}
	
	public static void commitTransaction(EntityManager entityManager)
	{
		if (entityManager != null 
				&& entityManager.getTransaction().isActive())
		{
			if (entityManager.getTransaction().getRollbackOnly())
			{
				entityManager.getTransaction().rollback();
			}
			else
			{
				entityManager.getTransaction().commit();
			}
		}
	}
	
	/**
	 * 
	 * @param emFactory
	 * @return
	 */
	public static EntityManager beginTransaction(EntityManagerFactory emFactory)
	{
		EntityManager entityManager = emFactory.createEntityManager();
		entityManager.getTransaction().begin();
		return entityManager;
	}

	/**
	 * 
	 * @return
	 */
	public static Session getSession() {
		EntityManager entityManager = buildEntityManager();
		if (entityManager != null) {
			Object delegate = entityManager.getDelegate();
			if (delegate instanceof Session) {
				Session session = (Session) delegate;
				return session;
			}
		}
		return null;
	}

	/******************************/

}