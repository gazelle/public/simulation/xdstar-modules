package net.ihe.gazelle.constraint.gen;

import java.util.List;

import javax.persistence.EntityManager;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.simulator.common.model.MessageType;
import net.ihe.gazelle.simulator.common.model.Parameter;

public class ConstraintGen {
	
	public static List<MessageType> getListMessageType(){
		EntityManager em = HibernateUtil.buildEntityManager();
		HQLQueryBuilder<MessageType> hh = new HQLQueryBuilder<MessageType>(em, MessageType.class);
		hh.addEq("transaction.keyword", "ITI-18");
		return hh.getList();
	}
	
	public static void main(String[] args) {
		List<MessageType> ll = getListMessageType();
		for (MessageType messageType : ll) {
			System.out.println("----------------------------------");
			String supp = "";
			System.out.println(messageType.getName());
			System.out.println(messageType.getUuid());
			List<Parameter> ppopt = messageType.getOptionalParameters();
			for (Parameter parameter : ppopt) {
				supp += "sl.name='" + parameter.getName() + "' or ";
				extractConstraints(messageType, parameter, false);
			}
			
			ppopt = messageType.getRequiredParameters();
			for (Parameter parameter : ppopt) {
				supp += "sl.name='" + parameter.getName() + "' or ";
				extractConstraints(messageType, parameter, true);
			}
			
			System.out.println("****");
			
			System.out.println("****self.slot->forAll(sl : SlotType1 | (not sl.name.oclIsUndefined()) and ("+ supp + ") )");
			System.out.println("----------------------------------");
			System.out.println("\n\n");
		}
	}

	private static void extractConstraints(MessageType messageType,
			Parameter parameter, boolean required) {
		System.out.println("\n--"+parameter.getName());
		if (!parameter.getSupportsAndOr()){
			System.out.println("------Not support AND");
			System.out.println("---------constraintITI18_" + parameter.getName().substring(1) + "_notSupportAND");
			System.out.println("---------"+parameter.getName() + " does not support AND/OR on " + messageType.getName() + " MessageType (ITI TF-2a 3.18.4.1.2.3.7.1))");
			System.out.println("---------self.slot->select(sl : SlotType1 | (not sl.name.oclIsUndefined()) and sl.name='" + parameter.getName() + "' )->size()<2");
		}
		if (!parameter.getMultiple()){
			System.out.println("------Not multiple");
			System.out.println("---------constraintITI18_" + parameter.getName().substring(1) + "_notMultiple");
			System.out.println("---------"+parameter.getName() + " is not multivalued on " + messageType.getName() + " MessageType (ITI TF-2a 3.18.4.1.2.3.7.1))");
			System.out.println("---------self.slot->select(sl : SlotType1 | (not sl.name.oclIsUndefined()) and sl.name='" + parameter.getName() + "' )->forAll(sl : SlotType1 | (not sl.valueList.oclIsUndefined()) and (sl.valueList.value->size()=1))");
		}
		if (required){
			System.out.println("------required");
			System.out.println("---------constraintITI18_" + parameter.getName().substring(1) + "_required");
			System.out.println("---------"+parameter.getName() + " is required on " + messageType.getName() + " MessageType (ITI TF-2a 3.18.4.1.2.3.7.1))");
			System.out.println("---------self.slot->select(sl : SlotType1 | (not sl.name.oclIsUndefined()) and sl.name='" + parameter.getName() + "' )->size()>0");
		}
		
		System.out.println("------present");
		System.out.println("---------constraintITI18_" + parameter.getName().substring(1) + "_ifPresentShallHaveValue");
		System.out.println("---------"+parameter.getName() + " shall have at least one value if present on " + messageType.getName() + " MessageType (ITI TF-2a 3.18.4.1.2.3.7.1))");
		System.out.println("---------self.slot->select(sl : SlotType1 | (not sl.name.oclIsUndefined()) and sl.name='" + parameter.getName() + "' )->forAll(sl : SlotType1 | (not sl.valueList.oclIsUndefined()) and (sl.valueList.value->size()>0))");
		
	}
	
	//public static void generateFromParam()

}
