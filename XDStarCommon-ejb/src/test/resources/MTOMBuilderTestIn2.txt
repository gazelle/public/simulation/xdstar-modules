--MIMEBoundaryurn_uuid_5B540A5CFFED43839A089713F2BB98F4
Content-Type:  application/xop+xml; charset=UTF-8; type="application/soap+xml"
Content-Transfer-Encoding: binary
Content-ID: <0.urn:uuid:C8E6C69188F94832AE161797ADAF7791@ws.jboss.org>

<?xml version="1.0" encoding="UTF-8"?>
<s:Envelope xmlns:a="http://www.w3.org/2005/08/addressing"
            xmlns:s="http://www.w3.org/2003/05/soap-envelope">
   <s:Header>
      <a:Action s:mustUnderstand="1">urn:ihe:iti:2007:RetrieveDocumentSet</a:Action>
      <a:MessageID>urn:uuid:ebd0dec0-8f6a-4a8e-b28f-3132acc99f04</a:MessageID>
      <a:ReplyTo>
         <a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address>
      </a:ReplyTo>
      <a:To s:mustUnderstand="1">http://hit-testing.nist.gov:12080/tf6/services/xdsrepositoryb</a:To>
   </s:Header>
   <s:Body>
      <xdsb:RetrieveDocumentSetRequest xmlns:lcm="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:3.0"
                                       xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0"
                                       xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0"
                                       xmlns:xdsb="urn:ihe:iti:xds-b:2007"
                                       xmlns:xop="http://www.w3.org/2004/08/xop/include">
         <xdsb:DocumentRequest>
            <xdsb:HomeCommunityId/>
            <xdsb:RepositoryUniqueId>1.3.6.1.4.1.21367.2011.2.3.7</xdsb:RepositoryUniqueId>
            <xdsb:DocumentUniqueId>1.3.6.1.4.1.12559.11.1.2.2.1.1.3.2.2494</xdsb:DocumentUniqueId>
         </xdsb:DocumentRequest>
      </xdsb:RetrieveDocumentSetRequest>
   </s:Body>
</s:Envelope>


--MIMEBoundaryurn_uuid_5B540A5CFFED43839A089713F2BB98F4--