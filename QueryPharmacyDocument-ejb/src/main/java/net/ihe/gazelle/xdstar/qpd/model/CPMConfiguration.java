package net.ihe.gazelle.xdstar.qpd.model;



import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;

import org.jboss.seam.annotations.Name;

@Entity
@Name("cpmConfiguration")
@DiscriminatorValue("CPM")
public class CPMConfiguration extends SystemConfiguration implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CPMConfiguration(){}
	
	public CPMConfiguration(CPMConfiguration configuration) {
		this.name = configuration.getName().concat("_COPY");
		this.url= configuration.getUrl();
		this.systemName = configuration.getSystemName();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}
	
}
