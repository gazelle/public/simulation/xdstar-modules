package net.ihe.gazelle.validator;

import java.util.List;

import net.ihe.gazelle.metadata.model.ConstraintSpecification;
import net.ihe.gazelle.metadata.model.RegistryPackageMetadata;
import net.ihe.gazelle.utils.ValidatorUtil;
import net.ihe.gazelle.validation.Notification;

public class RegistryPackageMetadataValidator {

	private static final String WARNING = "warning";
	private static final String ERROR = "error";

	protected  RegistryPackageMetadataValidator(){}

	public static final RegistryPackageMetadataValidator instance = new RegistryPackageMetadataValidator(); 

	public void validate(String document,String location, RegistryPackageMetadata rpmd, List<Notification> diagnostic, String technicalFramework){

	    RegistryObjectMetadataValidator.instance.validate(document,location, rpmd, null, diagnostic, technicalFramework);
	    this.validateExtraConstraints(document, location, rpmd, diagnostic, technicalFramework);
	}
	
	/**
	 * Validate the Xpath extra constraints defined in the RegistryPackageMetadata.
	 * @param registryPackageString : The RegistryPackage XML node tested
	 * @param location : Location of the tested element in the entire document
	 * @param rpmd : The ExtrinsicObjectMetadata used to test the document
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */
	protected void validateExtraConstraints(String registryPackageString,String location, RegistryPackageMetadata rpmd,List<Notification> diagnostic, String technicalFramework) {

		if (rpmd.getExtraConstraintSpecifications() != null){
			for (ConstraintSpecification conspec : rpmd.getExtraConstraintSpecifications()) {
				this.validateConstraintSpecification(registryPackageString, location, conspec, diagnostic, technicalFramework);
			}
		}
	}

	/**
	 * Check that the xpath extra constraint is valid
	 * @param registryPackageString : The RegistryPackage XML node tested
	 * @param location : Location of the tested element in the entire document
	 * @param conspec : XPATH extra constraint 
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 * 
	 */
	protected void validateConstraintSpecification(String registryPackageString, String location, ConstraintSpecification conspec, List<Notification> diagnostic, String technicalFramework) {

		ValidatorUtil.handleXpath(conspec.getXpath(), location, conspec.getDescription(), conspec.getKind(), registryPackageString, diagnostic, technicalFramework);
	}

}