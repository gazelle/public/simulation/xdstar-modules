package net.ihe.gazelle.validator;


import java.util.List;

import javax.xml.xpath.XPathExpressionException;

import net.ihe.gazelle.metadata.model.ConstraintSpecification;
import net.ihe.gazelle.metadata.model.ExtrinsicObjectMetadata;
import net.ihe.gazelle.utils.ValidatorUtil;
import net.ihe.gazelle.utils.XpathUtils;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespaceContext;

public class ExtrinsicObjectMetadataValidator {

	private static final String WARNING = "warning";
	private static final String ERROR = "error";

	protected  ExtrinsicObjectMetadataValidator(){}

	public static final ExtrinsicObjectMetadataValidator instance = new ExtrinsicObjectMetadataValidator(); 

	public void validate(String extrinsicString, String location, ExtrinsicObjectMetadata eomd, List<Notification> diagnostic, String technicalFramework){
		
		RegistryObjectMetadataValidator.instance.validate(extrinsicString, location, eomd, null, diagnostic, technicalFramework);
		this.validateObjectTypeValue(extrinsicString,location, eomd, diagnostic, technicalFramework);
		this.validateExtraConstraints(extrinsicString, location, eomd, diagnostic, technicalFramework);
	}

	/**
	 * Validate the Xpath extra constraints defined in the ExtrinsicObjectMetadata.
	 * @param extrinsicString : The ExtrinsicObject XML node tested
	 * @param location : Location of the tested element in the entire document
	 * @param eomd : The ExtrinsicObjectMetadata used to test the document
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */
	protected void validateExtraConstraints(String extrinsicString,String location, ExtrinsicObjectMetadata eomd,List<Notification> diagnostic, String technicalFramework) {

		if (eomd.getExtraConstraintSpecifications() != null){
			for (ConstraintSpecification conspec : eomd.getExtraConstraintSpecifications()) {
				this.validateConstraintSpecification(extrinsicString, location, conspec, diagnostic, technicalFramework);
			}
		}
	}

	/**
	 * Check that the xpath extra constraint is valid
	 * @param extrinsicString : The ExtrinsicObject XML node tested
	 * @param location : Location of the tested element in the entire document
	 * @param conspec : XPATH extra constraint 
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 * 
	 */
	protected void validateConstraintSpecification(String extrinsicString, String location, ConstraintSpecification conspec, List<Notification> diagnostic, String technicalFramework) {

		ValidatorUtil.handleXpath(conspec.getXpath(), location, conspec.getDescription(), conspec.getKind(), extrinsicString, diagnostic, technicalFramework);
	}

	/**
	 * Check that the objectType of an ExtrinsicObject is valid
	 * 
	 * @param document : The ExtrinsicObject XML node tested
	 * @param location : Location of the tested element in the entire document
	 * @param eomd : The ExtrinsicObjectMetadata used to test the document
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */
	protected void validateObjectTypeValue(String document, String location, ExtrinsicObjectMetadata eomd, List<Notification> diagnostic, String technicalFramework){
		
		String xpathString = "/rim:ExtrinsicObject/@objectType";
		try {
			String objectTypeTested = XpathUtils.evaluateByStringValue(document, xpathString, XDSNamespaceContext.getInstance());
			String objectTypeValid = eomd.getUuid();
			boolean isValidClassificationScheme = objectTypeTested.equals(objectTypeValid);
			String message = "The objectType of " + eomd.getName() + " is equal to " + objectTypeTested + ".\n Value allowed is " + objectTypeValid;
			diagnostic.add(ValidatorUtil.addNotification(location, isValidClassificationScheme, message, ERROR, technicalFramework));
			
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
	}	

}