package net.ihe.gazelle.validator;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import net.ihe.gazelle.metadata.model.CardinalityEnum;
import net.ihe.gazelle.metadata.model.ClassificationMetaData;
import net.ihe.gazelle.metadata.model.ExternalIdentifierMetadata;
import net.ihe.gazelle.metadata.model.RegistryObjectMetadata;
import net.ihe.gazelle.metadata.model.SlotMetadata;
import net.ihe.gazelle.metadata.model.describer.ClassificationMetadataDescriber;
import net.ihe.gazelle.metadata.model.describer.ExternalIdentifierMetadataDescriber;
import net.ihe.gazelle.metadata.model.describer.SlotMetadataDescriber;
import net.ihe.gazelle.utils.ValidatorUtil;
import net.ihe.gazelle.utils.XpathUtils;
import net.ihe.gazelle.validation.Notification;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class RegistryObjectMetadataValidator{
	
	private static final String WARNING = "warning";
	private static final String ERROR = "error";
	private static final String RIM_PREFIX = "urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0";
	
	protected  RegistryObjectMetadataValidator(){}
	
	public static final RegistryObjectMetadataValidator instance = new RegistryObjectMetadataValidator(); 

	public void validate(String document, String location, RegistryObjectMetadata rom, String nodeRepresentation, List<Notification> diagnostic, String technicalFramework) {

		this.validateSlot(document, location,rom, nodeRepresentation, diagnostic, technicalFramework);
		this.validateClassification(document,location, rom,diagnostic, technicalFramework);
		this.validateExternalIdentifier(document, location, rom,diagnostic, technicalFramework);

		this.validateSlotCardinality(location, document,rom,diagnostic, technicalFramework);
		this.validateClassificationCardinality(location, document,rom,diagnostic, technicalFramework);
		this.validateExternalIdentifierCardinality(location, document,rom,diagnostic, technicalFramework);

	}

	/**
	 * 
	 * Check that the number of times the object name appears is superior or equal to the cardinality min.
	 * @param location :  Location of the tested element in the entire document
	 * @param name : Name of the metadata
	 * @param valueTested : Number of times the object name appears 
	 * @param cardinalityMin : Minimum number of times the object Name must appears
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */
	
	protected void validateCardinalityMin(String location, String name, int valueTested, int cardinalityMin, List<Notification> diagnostic, String technicalFramework) {
		
		Boolean isSuperiorToCardinalityMin = valueTested >= cardinalityMin;
		diagnostic.add(ValidatorUtil.addNotification(location, isSuperiorToCardinalityMin, name + " : " + valueTested + " elements are present, and it must have " + cardinalityMin + " elements at least", ERROR, technicalFramework));
		
	}
	
	/**
	 * 
	 * Check that the number of times the object name appears is inferior or equal to the cardinality max.
	 * @param location :  Location of the tested element in the entire document
	 * @param name : Name of the metadata
	 * @param valueTested : Number of times the object name appears 
	 * @param cardinalityMax : Maximum number of times the object Name must appears
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */
	
	protected void validateCardinalityMax(String location, String name, int valueTested, int cardinalityMax, List<Notification> diagnostic, String technicalFramework) {
		
		String cardMaxString;
		if(cardinalityMax == -1){
			cardMaxString = "*";
		}else{
			cardMaxString = cardinalityMax+"";
		}
		Boolean isInferiorToCardinalityMax = valueTested <= cardinalityMax || cardinalityMax == -1;
		diagnostic.add(ValidatorUtil.addNotification(location, isInferiorToCardinalityMax, name + " : " + valueTested + " elements are present, and it must have " + cardMaxString + " elements at max", ERROR, technicalFramework));
		
	}
	
	/**
	 * Check if each External Identifier of the document appears the good number of times regarding data from romd
	 * @param document : the complete XML document
	 * @param location :  Location of the tested element in the entire document
	 * @param rom : RegistryObjectMetadata used to validate the External Identifier part of the document.
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */

	protected void validateExternalIdentifierCardinality(String location, String document,RegistryObjectMetadata rom, List<Notification> diagnostic, String technicalFramework) {

		for(ExternalIdentifierMetadataDescriber eid : rom.getExt_identifierDescriber()){
			ExternalIdentifierMetadata eimd = eid.getEim();
			CardinalityEnum cardinality = eid.getCardinality();

			try {
				NodeList nl;
				nl = XpathUtils.getNodeListFromString(document, "ExternalIdentifier", RIM_PREFIX);
				List<Node> listOfNodes = XpathUtils.getNodeListByAttribute(nl, "identificationScheme", eimd.getUuid());
				this.validateCardinalityMin(location + "/ExternalIdentifier/", "External Identifier " + eimd.getDisplayAttributeName(), listOfNodes.size(), cardinality.getMin(), diagnostic, technicalFramework);
				this.validateCardinalityMax(location + "/ExternalIdentifier/", "External Identifier " + eimd.getDisplayAttributeName(), listOfNodes.size(), cardinality.getMax(), diagnostic, technicalFramework);
				
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Check if each Classification of the document appears the good number of times regarding data from romd
	 * @param document : the complete XML document
	 * @param location :  Location of the tested element in the entire document
	 * @param rom : RegistryObjectMetadata used to validate the Classification part of the document.
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */



	protected void validateClassificationCardinality(String location, String document,RegistryObjectMetadata rom, List<Notification> diagnostic, String technicalFramework) {

		for(ClassificationMetadataDescriber cld : rom.getClassificationDescriber()){

			ClassificationMetaData clmd = cld.getCmd();
			CardinalityEnum cardinality = cld.getCardinality();

			try {
				NodeList nl;
				nl = XpathUtils.getNodeListFromString(document, "Classification", RIM_PREFIX);
				List<Node> listOfNodes = XpathUtils.getNodeListByAttribute(nl, "classificationScheme", clmd.getUuid());
				this.validateCardinalityMin(location + "/Classification/", "Classification " + clmd.getDisplayAttributeName(), listOfNodes.size(), cardinality.getMin(), diagnostic, technicalFramework);
				this.validateCardinalityMax(location + "/Classification/", "Classification " + clmd.getDisplayAttributeName(), listOfNodes.size(), cardinality.getMax(), diagnostic, technicalFramework);
				
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Check if each Slot of the document appears the good number of times regarding data from romd
	 * @param document : the complete XML document
	 * @param location :  Location of the tested element in the entire document
	 * @param rom : RegistryObjectMetadata used to validate the Slot part of the document.
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */

	protected void validateSlotCardinality(String location, String document,RegistryObjectMetadata rom, List<Notification> diagnostic, String technicalFramework) {

		for(SlotMetadataDescriber sld : rom.getSlotDescriber()){
			SlotMetadata smd = sld.getSlotMetadata();
			CardinalityEnum cardinality = sld.getCardinality();

			try {
				NodeList nl;
				nl = XpathUtils.getNodeListFromString(document, "Slot", RIM_PREFIX);
				List<Node> listOfNodes = XpathUtils.getNodeListByAttribute(nl, "name", smd.getName());
				this.validateCardinalityMin(location + "/Slot/", "Slot " + smd.getName(), listOfNodes.size(), cardinality.getMin(), diagnostic, technicalFramework);
				this.validateCardinalityMax(location + "/Slot/", "Slot " + smd.getName(), listOfNodes.size(), cardinality.getMax(), diagnostic, technicalFramework);
				
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * Validate the document in front of the External Identifier metadata
	 * @param document : the complete XML document
	 * @param location :  Location of the tested element in the entire document
	 * @param rom : External Identifier Metadata used to validate the ExternalIdentifier part of the document.
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */

	protected void validateExternalIdentifier(String document, String location, RegistryObjectMetadata rom, List<Notification> diagnostic, String technicalFramework) {
		
		for (ExternalIdentifierMetadata eimd : rom.getListExternalIdentifierMetadatas()){
			Map<String,Integer> externalIdentifiersToBeTested = this.extractListOfRegistryObject(document,"ExternalIdentifier", "identificationScheme", eimd.getUuid());
			for (Map.Entry<String, Integer> externalIdentifierMap : externalIdentifiersToBeTested.entrySet()) {
				String externalIdentifier = externalIdentifierMap.getKey();
				Integer position = externalIdentifierMap.getValue();
				ExternalIdentifierMetadataValidator.instance.validate(externalIdentifier, location + "/ExternalIdentifier[" + position + "]", eimd, diagnostic, technicalFramework);
			}
		}
		
	}
	
	/**
	 * Validate the document in front of the Classsification metadata
	 * @param document : the complete XML document
	 * @param location :  Location of the tested element in the entire document
	 * @param rom : Classification Metadata used to validate the Classification part of the document.
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */


	protected void validateClassification(String document,String location, RegistryObjectMetadata rom, List<Notification> diagnostic, String technicalFramework) {
		
		for (ClassificationMetaData clmd : rom.getListClassificationMetadatas()){
			Map<String,Integer> classificationsToBeTested = this.extractListOfRegistryObject(document,"Classification",  "classificationScheme", clmd.getUuid());
			for (Map.Entry<String, Integer> classficationMap : classificationsToBeTested.entrySet()) {
				String classification = classficationMap.getKey();
				Integer position = classficationMap.getValue();
				ClassificationMetadataValidator.instance.validate(classification, location + "/Classification[" + position + "]", clmd, diagnostic, technicalFramework);
			}
		}
	}

	/**
	 * Validate the document in front of the Slot metadata
	 * @param document : the complete XML document
	 * @param location :  Location of the tested element in the entire document
	 * @param rom : Slot Metadata used to validate the Slot part of the document.
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */

	protected void validateSlot(String document, String location,RegistryObjectMetadata rom, String nodeRepresentation, List<Notification> diagnostic, String technicalFramework) {

		for (SlotMetadata smd : rom.getListSlotMetadatas()){
			Map<String,Integer> slotToBeTested = this.extractListOfSlot(document, smd.getName());
			
			for (Map.Entry<String,Integer> slotMap : slotToBeTested.entrySet()) {
				String slot = slotMap.getKey();
				Integer position = slotMap.getValue();
				SlotMetadataValidator.instance.validate(document, location + "/Slot[" + position + "]", slot, rom, smd, nodeRepresentation, diagnostic, technicalFramework);
			}
		}
		
	}


	/**
	 * Extract the Slot XML nodes of the entire document which are valid regarding the name of the tested slot
	 * @param document : The entire XML document
	 * @param slotNameTested : Name of the tested slot from metadata
	 * @return a Map with the extracted Node as the key and the position of this node in the document as the value
	 */
	
	protected Map<String,Integer> extractListOfSlot(String document, String slotNameTested) {
		Map<String,Integer> listSlots = new HashMap<String,Integer>();
		try {
			NodeList nl = XpathUtils.getNodeListFromString(document, "Slot", RIM_PREFIX);
			Map<Node,Integer> nlWithNames = XpathUtils.getNodeMapByAttribute(nl, "name", slotNameTested);
			for(Map.Entry<Node, Integer> n : nlWithNames.entrySet()){
				Node node = n.getKey();
				Integer index = n.getValue();
				listSlots.put(XpathUtils.nodeToString(node),index);
			}
						
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return listSlots;
	}
	

	/**
	 * Extract the RegistryObject XML nodes of the entire document which are valid regarding the uuid of the tested registry object
	 * @param document : The entire XML document
	 * @param registryObjectType : Specific type of the Registry Object [Classification, External Identifier]
	 * @param attributeName : Name of the UUID attribute 
	 * @param uuid : Uuid of the tested Registry Object
	 * @return a Map with the extracted Node as the key and the position of this node in the document as the value
	 */
	
	protected Map<String,Integer> extractListOfRegistryObject(String document, String registryObjectType, String attributeName, String uuid) {
		Map<String,Integer> listRegistryObject = new HashMap<String, Integer>();
		try {
			NodeList nl = XpathUtils.getNodeListFromString(document, registryObjectType, RIM_PREFIX);
			Map<Node,Integer> nlWithNames = XpathUtils.getNodeMapByAttribute(nl, attributeName, uuid);
			for(Map.Entry<Node, Integer> n : nlWithNames.entrySet()){
				Node node = n.getKey();
				Integer index = n.getValue();
				listRegistryObject.put(XpathUtils.nodeToString(node),index);
			}
						
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return listRegistryObject;
	}


}
