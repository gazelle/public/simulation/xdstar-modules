package net.ihe.gazelle.validator;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import net.ihe.gazelle.metadata.model.*;
import net.ihe.gazelle.metadata.model.describer.ExtrinsicObjectMetadataDescriber;
import net.ihe.gazelle.metadata.model.describer.RegistryPackageMetadataDescriber;
import net.ihe.gazelle.utils.ValidatorUtil;
import net.ihe.gazelle.utils.XpathUtils;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespaceContext;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class RegistryObjectListMetadataValidator {


	private static final String WARNING = "warning";
	private static final String ERROR = "error";
	private static final String RIM_PREFIX = "urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0";

	protected  RegistryObjectListMetadataValidator(){}

	public static final RegistryObjectListMetadataValidator instance = new RegistryObjectListMetadataValidator(); 


	/**
	 * Validate the document in front of the Registry Object List Metadata
	 * @param document : the complete XML document
	 * @param location :  Location of the tested element in the entire document
	 * @param rolmd : RegistryObjectListMetadata used to validate the Registry Object List part of the document.
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */
	public void validate(String document, String location, RegistryObjectListMetadata rolmd, List<Notification> diagnostic, String technicalFramework){
		if (rolmd != null){
			this.validateExtrinsicObject(document, location, rolmd, diagnostic, technicalFramework);
			this.validateRegistryPackage(document, location, rolmd,diagnostic, technicalFramework);

			this.validateExtrinsicObjectCardinality(location, document,rolmd,diagnostic, technicalFramework);
			this.validateRegistryPackageCardinality(location, document,rolmd,diagnostic, technicalFramework);

			this.validateExtraConstraints(document, location, rolmd, diagnostic, technicalFramework);
		}

	}

	/**
	 * Validate the document in front of the Registry Object List Metadata
	 * @param document : the complete XML document
	 * @param location :  Location of the tested element in the entire document
	 * @param rolmd : RegistryObjectListMetadata used to validate the Registry Object List part of the document.
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */
	protected void validateExtraConstraints(String document,String location, RegistryObjectListMetadata rolmd,List<Notification> diagnostic, String technicalFramework) {

		if (rolmd.getExtraConstraintSpecifications() != null){
			for (ConstraintSpecification conspec : rolmd.getExtraConstraintSpecifications()) {
				this.validateConstraintSpecification(document, location, conspec, diagnostic, technicalFramework);
			}
		}
	}

	/**
	 * Check that the xpath extra constraint is valid
	 * @param document : the complete XML document
	 * @param location : Location of the tested element in the entire document
	 * @param conspec : XPATH extra constraint
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 *
	 */
	protected void validateConstraintSpecification(String document, String location, ConstraintSpecification conspec, List<Notification> diagnostic, String technicalFramework) {

		ValidatorUtil.handleXpath(conspec.getXpath(), location, conspec.getDescription(), conspec.getKind(), document, diagnostic, technicalFramework);
	}

	/**
	 * Check if each Registry Package of the document appears the good number of times regarding data from rolmd
	 * @param document : the complete XML document
	 * @param location :  Location of the tested element in the entire document
	 * @param rolmd : RegistryObjectListMetadata used to validate the Registry Object List part of the document.
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */

	protected void validateRegistryPackageCardinality(String location, String document,RegistryObjectListMetadata rolmd, List<Notification> diagnostic, String technicalFramework) {

		for(RegistryPackageMetadataDescriber rpmd : rolmd.getRegistryPackage()){

			RegistryPackageMetadata rpm = rpmd.getRpm();
			CardinalityEnum cardinality = rpmd.getCardinality();

			Map<String,Integer> listOfNodes = this.extractListOfRegistryPackage(document, rpm.getUuid());

			this.validateCardinalityMin(location + "/RegistryPackage", "RegistryPackage " + rpm.getName(), listOfNodes.size(), cardinality.getMin(), diagnostic, technicalFramework);
			this.validateCardinalityMax(location + "/RegistryPackage", "RegistryPackage " + rpm.getName(), listOfNodes.size(), cardinality.getMax(), diagnostic, technicalFramework);
		}

	}

	/**
	 * Check if each Extrinsic Object of the document appears the good number of times regarding data from rolmd
	 * @param document : the complete XML document
	 * @param location :  Location of the tested element in the entire document
	 * @param rolmd : RegistryObjectListMetadata used to validate the Registry Object List part of the document.
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */

	protected void validateExtrinsicObjectCardinality(String location, String document,RegistryObjectListMetadata rolmd, List<Notification> diagnostic, String technicalFramework) {

		for(ExtrinsicObjectMetadataDescriber cld : rolmd.getExtrinsicObject()){

			ExtrinsicObjectMetadata eomd = cld.getEom();
			CardinalityEnum cardinality = cld.getCardinality();

			try {
				NodeList nl;
				nl = XpathUtils.getNodeListFromString(document, "ExtrinsicObject", RIM_PREFIX);
				int countExtrinsicObjects = (XpathUtils.getNodeListByAttribute(nl, "objectType", eomd.getUuid())).size();
				this.validateCardinalityMin(location + "/ExtrinsicObject/", "ExtrinsicObject " + eomd.getName(), countExtrinsicObjects, cardinality.getMin(), diagnostic, technicalFramework);
				this.validateCardinalityMax(location + "/ExtrinsicObject/", "ExtrinsicObject " + eomd.getName(), countExtrinsicObjects, cardinality.getMax(), diagnostic, technicalFramework);

			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Validate the document in front of the Registry Package metadata
	 * @param document : the complete XML document
	 * @param location :  Location of the tested element in the entire document
	 * @param rolmd : RegistryObjectListMetadata used to validate the Registry Package part of the document.
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */
	protected void validateRegistryPackage(String document,String location, RegistryObjectListMetadata rolmd, List<Notification> diagnostic, String technicalFramework) {

		this.validateClassifiedObjectReferencesRegistryPackage(document, location, diagnostic, technicalFramework); // Check that each classificationNode references an existing Registry Package
		this.validateRegistryPackageIsReferencedByClassificationObject(document, location, diagnostic, technicalFramework); // Check that each registry package is referenced by a valid classificationNode

		//  Check the valid Registry Packages towards the metadata
		for (RegistryPackageMetadata rpm : rolmd.getListRegistryPackageMetadatas()){

			Map<String,Integer> registryPackageToBeTested = this.extractListOfRegistryPackage(document, rpm.getUuid());

			for (Map.Entry<String, Integer> registryPackageMap : registryPackageToBeTested.entrySet()) {
				String registryPackage = registryPackageMap.getKey();
				Integer position = registryPackageMap.getValue();
				RegistryPackageMetadataValidator.instance.validate(registryPackage,location + "/RegistryPackage[" + position + "]/" , rpm, diagnostic, technicalFramework);
			}
		}

	}


	/**
	 * Verify that for all the classification with classficationNode, they have a classifiedObject from the list of registryPackageIds
	 * @param document : the complete XML document
	 * @param location :  Location of the tested element in the entire document
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */
	protected void validateClassifiedObjectReferencesRegistryPackage(String document, String location,List<Notification> diagnostic, String technicalFramework) {
		List<String> registryPackageIds = getRegistryPackageIds(document);
		List<Node> nodesWithClassificationNode = getNodesWithClassificationNode(document);
		for (Node classification : nodesWithClassificationNode) {
			String classifiedObject = getAttributeValueFromName(classification, "classifiedObject");
			boolean isValidClassifiedObject = registryPackageIds.contains(classifiedObject);
			String message = "The classifiedObject = " + classifiedObject + " should references a registry package Id. \n Registry Package Ids allowed are " + Arrays.toString(registryPackageIds.toArray());
			diagnostic.add(ValidatorUtil.addNotification(location, isValidClassifiedObject, message , WARNING, technicalFramework));
		}
	}

	/**
	 * Verify that for all registry package id, they have a classification with classification node with an attribute classifiedObject which references the registry package
	 * @param document : the complete XML document
	 * @param location :  Location of the tested element in the entire document
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */
	protected void validateRegistryPackageIsReferencedByClassificationObject(String document, String location,List<Notification> diagnostic, String technicalFramework) {
		List<String> registryPackageIds = getRegistryPackageIds(document);
		List<Node> nodesWithClassificationNode = getNodesWithClassificationNode(document);
		List<String> classifiedObjectList = getAttributeValueListFromNodeList(nodesWithClassificationNode,"classifiedObject");

		for (String rpid : registryPackageIds) {

			boolean isReferencingGoodClassification = classifiedObjectList.contains(rpid);
			String message = "The Registry Package = " + rpid + " must be referenced by a valid classifiedObject attribute from a Classification. \n Allowed classifiedObject are " + Arrays.toString(classifiedObjectList.toArray());
			diagnostic.add(ValidatorUtil.addNotification(location, isReferencingGoodClassification, message , ERROR, technicalFramework));
		}
	}



	/**
	 * Get the list of registry package with the rpm uuid. Each member of the list is associated with its position in the entire XML document.
	 * @param document : The entire XML document
	 * @param registryPackageMetadataUuid : The RegistryPackageMetadata Uuid used to extract the registry package from the document
	 * @return the map association between Registry package in string format with the position number in the entire XML document
	 */
	protected Map<String,Integer> extractListOfRegistryPackage(String document, String registryPackageMetadataUuid) {

		Map<String,Integer> listRegistryPackage = new HashMap<String,Integer>();
		try {

			List<Node> classificationsNodes = getClassificationNodesWithClassificationNodeOfRegistryPackageUuid(document, registryPackageMetadataUuid);
			List<String> registryPackageIds = getAttributeValueListFromNodeList(classificationsNodes,"classifiedObject");

			NodeList allRegistryPackagesNodes = XpathUtils.getNodeListFromString(document, "RegistryPackage", RIM_PREFIX);
			for (String rpId : registryPackageIds) {

				Entry<String, Integer> currentRegistryPackageEntry = extractRegistryPackageNodeFromId(rpId, allRegistryPackagesNodes);
				if(currentRegistryPackageEntry != null){
					listRegistryPackage.put(currentRegistryPackageEntry.getKey(), currentRegistryPackageEntry.getValue());
				}
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return listRegistryPackage;
	}


	/**
	 * Get the registry package node with its position in the document from all the registry packages nodes and the registry package id.
	 * @param registryPackageIdValid : The registry Package id of the valid registry package
	 * @param allRegistryPackagesNodes : All the registry packages nodes from the entire XML document
	 * @return a Map entry indexed by the registry package node in String format, with the position on the document as the value.
	 */
	protected static Map.Entry<String, Integer> extractRegistryPackageNodeFromId(String registryPackageIdValid,NodeList allRegistryPackagesNodes) {

		Map.Entry<String,Integer> res = null;

		for(int i = 0; i < allRegistryPackagesNodes.getLength(); i++){
			String currentRegistryPackageString = XpathUtils.nodeToString(allRegistryPackagesNodes.item(i));
			String xpath = "/rim:RegistryPackage/@id";
			try {
				String registryPackageIdTested = XpathUtils.evaluateByStringValue(currentRegistryPackageString, xpath, XDSNamespaceContext.getInstance());
				boolean isGoodRegistryPackage = registryPackageIdValid.equals(registryPackageIdTested);
				if(isGoodRegistryPackage){
					res = new AbstractMap.SimpleEntry<String, Integer>(currentRegistryPackageString, i);
				}
			} catch (XPathExpressionException e) {
				e.printStackTrace();
			}
		}
		return res;
	}


	/**
	 * Get the list of attribute value from a node list and the name of the attribute
	 * @param nodeList : a list of nodes
	 * @param attributeName : The name of the attribute searched
	 * @return list of attribute value from a node list and the name of the attribute
	 */
	protected static List<String> getAttributeValueListFromNodeList(List<Node> nodeList, String attributeName) {

		List<String> res = new ArrayList<String>();
		for (Node node : nodeList) {
			String attributeValue = getAttributeValueFromName(node, attributeName);
			if(attributeValue != null){
				res.add(attributeValue);
			}
		}
		return res;
	}


	/**
	 * Get the attribute value of a node from its attribute name
	 * @param node : The node that we search value
	 * @param attributeName : The name of the attribute searched
	 * @return the attribute value of a node from its attribute name
	 */
	protected static String getAttributeValueFromName(Node node, String attributeName) {
		NamedNodeMap attributes = node.getAttributes();
		for (int j = 0; j < attributes.getLength(); j++){
			Node attributeCurrent = attributes.item(j);
			if(attributeCurrent.getNodeName().equals(attributeName)){
				return attributeCurrent.getNodeValue();
			}
		}
		return null;
	}


	/**
	 * Get the list of classification nodes from the document with a classificationNode attribute.
	 * @param document : The entire XML document
	 * @return the list of classification nodes from the document with a classificationNode attribute.
	 */
	protected static List<Node> getNodesWithClassificationNode(String document) {

		List<Node> res = new ArrayList<Node>();

		try {
			NodeList nl = XpathUtils.getNodeListFromString(document, "Classification", RIM_PREFIX);
			for (int i = 0; i < nl.getLength(); i++) {
				Node current = nl.item(i);
				NamedNodeMap attributs = current.getAttributes();
				for (int j = 0; j < attributs.getLength(); j++){
					Node att = attributs.item(j);
					if(att.getNodeName().equals("classificationNode")){
						res.add(current);
					}
				}
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return res;
	}

	/**
	 * Get the node list of classifications referencing the registryPackage tested
	 * @param document : The entire XML document
	 * @param classificationNodeValid : The classification node value referenced in the registryPackageMetadata
	 * @return the node list of classifications referencing the registryPackage tested
	 */
	protected static List<Node> getClassificationNodesWithClassificationNodeOfRegistryPackageUuid(String document, String classificationNodeValid) {

		List<Node> res = new ArrayList<Node>();
		try {
			NodeList listClassifications = XpathUtils.getNodeListFromString(document, "Classification", RIM_PREFIX);
			for (int i = 0; i < listClassifications.getLength(); i++) {
				Node currentClassification = listClassifications.item(i);
				String classificationNodeTested = getAttributeValueFromName(currentClassification,"classificationNode");
				if(classificationNodeTested != null && classificationNodeTested.equals(classificationNodeValid)){
					res.add(currentClassification);
				}
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return res;
	}


	/**
	 * Validate the document in front of the Extrinsic Object metadatas
	 * @param document : The entire XML document
	 * @param location : Location of the tested element in the entire document
	 * @param rolmd : The RegistryObjectListMetadata used to test the document
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */
	protected void validateExtrinsicObject(String document, String location, RegistryObjectListMetadata rolmd, List<Notification> diagnostic, String technicalFramework) {
		for (ExtrinsicObjectMetadata eom : rolmd.getListExtrinsicObjectMetadatas()){
			Map<Node,Integer> extrinsicObjectToBeTested = this.extractListOfExtrinsicObject(document, eom.getUuid()); // Extract the extrinsicObjects from the entire document which match with the ExtrinsicObjectMetadata UUID.
			for (Map.Entry<Node, Integer> extrinsicObjectMap : extrinsicObjectToBeTested.entrySet()) {
				String extrinsicObject = XpathUtils.nodeToString(extrinsicObjectMap.getKey());
				Integer position = extrinsicObjectMap.getValue();
				ExtrinsicObjectMetadataValidator.instance.validate(extrinsicObject, location + "/ExtrinsicObject[" + position + "]/" , eom, diagnostic, technicalFramework);
			}
		}

	}


	/**
	 * Extract the RegistryObject XML nodes of the entire document which are valid regarding the objectTypeValueTested
	 * @param document : The entire XML document
	 * @param objectTypeValueTested : The objectType value of the metadata used to extract the valid list of extrinsicObject
	 * @return a Map with the extractedNode as the key and the position of this node in the document as the value
	 */
	protected Map<Node,Integer> extractListOfExtrinsicObject(String document, String objectTypeValueTested) {

		Map<Node,Integer> extrinsicObjectListValid = new HashMap<Node, Integer>();
		try {
			NodeList allExtrinsicObject = XpathUtils.getNodeListFromString(document, "ExtrinsicObject", RIM_PREFIX);
			extrinsicObjectListValid = XpathUtils.getNodeMapByAttribute(allExtrinsicObject, "objectType", objectTypeValueTested); // Extract The ExtrinsicObjects with a valid objectType attribute regarding the ExtrinsicObjectMetadata UUID
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return extrinsicObjectListValid;
	}

	/**
	 * 
	 * Check that the number of times the object name appears is superior or equal to the cardinality min.
	 * @param location :  Location of the tested element in the entire document
	 * @param name : Name of the metadata
	 * @param valueTested : Number of times the object name appears 
	 * @param cardinalityMin : Minimum number of times the object Name must appears
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */
	protected void validateCardinalityMin(String location, String name, int valueTested, int cardinalityMin, List<Notification> diagnostic, String technicalFramework) {

		Boolean isSuperiorToCardinalityMin = valueTested >= cardinalityMin;
		diagnostic.add(ValidatorUtil.addNotification(location, isSuperiorToCardinalityMin, name + " : " + valueTested + " elements are present, and it must have " + cardinalityMin + " elements at least", ERROR, technicalFramework));

	}

	/**
	 * 
	 * Check that the number of times the object name appears is inferior or equal to the cardinality max.
	 * @param location :  Location of the tested element in the entire document
	 * @param name : Name of the metadata
	 * @param valueTested : Number of times the object name appears 
	 * @param cardinalityMax : Maximum number of times the object Name must appears
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */

	protected void validateCardinalityMax(String location, String name, int valueTested, int cardinalityMax, List<Notification> diagnostic, String technicalFramework) {

		String cardMaxString;
		if(cardinalityMax == -1){
			cardMaxString = "*";
		}else{
			cardMaxString = cardinalityMax+"";
		}
		Boolean isInferiorToCardinalityMax = valueTested <= cardinalityMax || cardinalityMax == -1;
		diagnostic.add(ValidatorUtil.addNotification(location, isInferiorToCardinalityMax, name + " : " + valueTested + " elements are present, and it must have " + cardMaxString + " elements at max", ERROR, technicalFramework));

	}

	/**
	 * Get the list of RegistryPackage Ids from the entire document
	 * @param document : Entire XML Document
	 * @return the list of RegistryPackage Ids from the entire document
	 */
	protected static List<String> getRegistryPackageIds(String document) {
		List<String> res = new ArrayList<String>();
		try {
			
			NodeList allRegistryPackage = XpathUtils.getNodeListFromString(document, "RegistryPackage", RIM_PREFIX);
			for(int i = 0; i < allRegistryPackage.getLength(); i++){
				String currentRegistryPackageString = XpathUtils.nodeToString(allRegistryPackage.item(i));
				res.add(XpathUtils.evaluateByStringValue(currentRegistryPackageString, "/rim:RegistryPackage/@id", XDSNamespaceContext.getInstance()));
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return res;	
	}


}
