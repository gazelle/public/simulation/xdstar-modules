package net.ihe.gazelle.validator;

import java.util.List;

import javax.xml.xpath.XPathExpressionException;

import net.ihe.gazelle.metadata.model.ExternalIdentifierMetadata;
import net.ihe.gazelle.utils.ValidatorUtil;
import net.ihe.gazelle.utils.XpathUtils;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespaceContext;

public class ExternalIdentifierMetadataValidator {

	private static final String WARNING = "warning";
	private static final String ERROR = "error";

	protected  ExternalIdentifierMetadataValidator(){}

	public static final ExternalIdentifierMetadataValidator instance = new ExternalIdentifierMetadataValidator(); 

	
	public void validate(String document, String location, ExternalIdentifierMetadata eimd, List<Notification> diagnostic, String technicalFramework){
		
		RegistryObjectMetadataValidator.instance.validate(document, location, eimd, null, diagnostic, technicalFramework);
		this.validateIdentificationSchemeValue(document,location,eimd,diagnostic, technicalFramework);

	}

	/**
	 * Check that the identificationScheme of an ExternalIdentifier is valid
	 * 
	 * @param document : The ExternalIdentifier XML node tested
	 * @param location : Location of the tested element in the entire document
	 * @param eimd : The ExternalIdentifierMetadata used to test the document
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */
	public void validateIdentificationSchemeValue(String document, String location, ExternalIdentifierMetadata eimd, List<Notification> diagnostic, String technicalFramework){
		
		String xpathString = "/rim:ExternalIdentifier/@identificationScheme";
		try {
			String identificationSchemeTested = XpathUtils.evaluateByStringValue(document, xpathString, XDSNamespaceContext.getInstance());
			String identificationSchemeValid = eimd.getUuid();
			boolean isValidClassificationScheme = identificationSchemeTested.equals(identificationSchemeValid);
			String message = "The identificationScheme of " + eimd.getName() + " is equal to " + identificationSchemeTested + ".\n Value allowed is " + identificationSchemeValid;
			diagnostic.add(ValidatorUtil.addNotification(location, isValidClassificationScheme, message, ERROR, technicalFramework));
			
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
	}	

}