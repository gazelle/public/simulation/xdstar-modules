package net.ihe.gazelle.validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPathExpressionException;

import net.ihe.gazelle.metadata.model.AdhocQueryMetadata;
import net.ihe.gazelle.metadata.model.ClassificationMetaData;
import net.ihe.gazelle.metadata.model.RegistryObjectMetadata;
import net.ihe.gazelle.metadata.model.SlotMetadata;
import net.ihe.gazelle.simulator.common.model.Concept;
import net.ihe.gazelle.utils.ValidatorUtil;
import net.ihe.gazelle.utils.XpathUtils;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.xdstar.comon.util.SVSConsumerForSimulator;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespaceContext;

public class SlotMetadataValidator{

	private static final String WARNING = "warning";
	private static final String ERROR = "error";

	protected  SlotMetadataValidator(){}

	public static final SlotMetadataValidator instance = new SlotMetadataValidator(); 


	/**
	 * Validate the slot in front of the slot metadata.
	 * @param document :  The XML representation of the entire document
	 * @param location :  Location of the tested element in the entire document
	 * @param slot : The XML representation of the slot
	 * @param parentSlotMetadata : slotMetadata parent
	 * @param slotMetadata : Metadata to test the slot
	 * @param nodeRepresentation : The nodeRepresentation attribute [only used if the slot's parent is a classification]
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */
	public void validate(String document, String location, String slot, RegistryObjectMetadata parentSlotMetadata, SlotMetadata slotMetadata, String nodeRepresentation, List<Notification> diagnostic, String technicalFramework) {

		try {

			int countValue = Integer.parseInt(XpathUtils.evaluateByStringValue(slot, "count(/rim:Slot/rim:ValueList/rim:Value)", XDSNamespaceContext.getInstance()));

			for(int i = 1; i <= countValue; i++){

				String value = XpathUtils.evaluateByStringValue(slot, "/rim:Slot/rim:ValueList/rim:Value["+i+"]", XDSNamespaceContext.getInstance());

				Boolean hasParenthesis = hasParenthesis(value);
				if(parentSlotMetadata instanceof AdhocQueryMetadata){
					this.validateMultiple(location, value, slotMetadata, diagnostic, hasParenthesis, technicalFramework);
				}
				String valWithoutParenthesis = deleteParenthesis(value, slotMetadata.getMultiple(), hasParenthesis);
				Boolean hasQuotes = hasQuotes(valWithoutParenthesis);

				String[] tabVal = splitSlotValues(valWithoutParenthesis, slotMetadata.getIsNumber(), hasQuotes);
				for(String valSplit : tabVal){
					
					if(parentSlotMetadata instanceof AdhocQueryMetadata){
						this.validateIsNumber(location, valSplit, slotMetadata, diagnostic, technicalFramework);
					}
					String valSplitWithoutQuotesAndWithoutParenthesis = deleteQuotes(valSplit, slotMetadata.getIsNumber(), hasQuotes);
					this.validateSlotValueFromValueSet(location, valSplitWithoutQuotesAndWithoutParenthesis, parentSlotMetadata, slotMetadata, nodeRepresentation, diagnostic, technicalFramework);
					this.validateSlotValueByRegex(valSplitWithoutQuotesAndWithoutParenthesis, location, slotMetadata.getRegex(), diagnostic, technicalFramework);
				}

			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}



	}

	/**
	 * Return if the input string value is surrounded by quotes
	 * @param valWithoutParenthesis  Input string
	 * @return true is a the string value is surrounded by quotes, false otherwise
	 */

	protected Boolean hasQuotes(String valWithoutParenthesis) {

		if(valWithoutParenthesis.length() >= 2){
			return (valWithoutParenthesis.charAt(0) == '\'' ) && (valWithoutParenthesis.charAt(valWithoutParenthesis.length()-1)=='\'' );
		}else{
			return false;
		}
	}

	/**
	 * Return if the input string value is surrounded by parenthesis
	 * @param value  Input string
	 * @return true is a the string value is surrounded by parenthesis, false otherwise
	 */

	protected Boolean hasParenthesis(String value) {

		if(value.length() >= 2){
			return (value.charAt(0) == '(' ) && (value.charAt(value.length()-1)==')' );
		}else{
			return false;
		}
	}

	/**
	 * Check that the input value is valid regarding the regex
	 * @param value : The value tested in front of the regex
	 * @param location :  Location of the tested element in the entire document
	 * @param regex : The regex
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */
	protected void validateSlotValueByRegex(String value, String location,String regex, List<Notification> diagnostic, String technicalFramework) {

		boolean hasMatch = true;
		if(regex != null && !regex.equals("")){
			Pattern pat = Pattern.compile(regex);
			Matcher m = pat.matcher(value);
			hasMatch = m.find();
			diagnostic.add(ValidatorUtil.addNotification(location, hasMatch, "The value '" + value + "' must match the regex " + regex, ERROR, technicalFramework));

		}


	}


	/**
	 * Split the adhoc query values which are separated by a comma ',' to test each value individually.
	 * @param valWithoutParenthesis : A string value without parenthesis but potentially with quotes
	 * @param isNumber : true if the value should be a number, false otherwise
	 * @param hasQuotes : true if the string has quotes, false otherwise
	 * @return an array of split string values 
	 */
	protected String[] splitSlotValues(String valWithoutParenthesis, Boolean isNumber, Boolean hasQuotes) {

		if(isNumber != null && isNumber){
			return valWithoutParenthesis.split(",");
		}else{

			String res[] = valWithoutParenthesis.split("','");

			if(hasQuotes){
				res[0] = res[0].substring(1);
				int max = (res.length)-1;
				int lMax = res[max].length() - 1;
				res[max] = res[max].substring(0,lMax);
				for (int i = 0; i < res.length ; i++) {
					res[i] = "'"+res[i]+"'";
				}

			}
			return res;		
		}


	}

	/**
	 * Check that the valSplit value is in line with the metadata isNumber attribute.
	 * If the value must be a number : [0-9] figures accepted.
	 * If the value must NOT be a number : the value should be surrounded by quotes
	 * @param location : Location of the tested element in the entire document
	 * @param valSplit : String value tested
	 * @param metadata : The slot metadata used to test the value
	 * @param diagnostic :  List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */
	protected void validateIsNumber(String location, String valSplit,SlotMetadata metadata, List<Notification> diagnostic, String technicalFramework) {

		String regex;
		Boolean metadataIsNumber = metadata.getIsNumber();
		if(metadataIsNumber != null && metadataIsNumber){
			regex = "^[0-9]*$"; // Just numbers
			Boolean isNumber = ValidatorUtil.hasMatch(regex, valSplit);
			String message = "The value " + valSplit + " of the slot " + metadata.getName() + " must contains only numbers";
			diagnostic.add(ValidatorUtil.addNotification(location, isNumber, message, ERROR, technicalFramework));

		}else{

			Boolean hasQuotes = hasQuotes(valSplit);
			String message = "The value " + valSplit + " of the slot " + metadata.getName() + " must begin and end with quotes";
			diagnostic.add(ValidatorUtil.addNotification(location, hasQuotes, message, ERROR, technicalFramework));
		}
	}


	/**
	 * Check that the input value is in line with the multiple status of the metadata.
	 * @param location : Location of the tested element in the entire document
	 * @param value : String value tested
	 * @param metadata : The slot metadata used to test the value
	 * @param diagnostic :  List of tests results
	 * @param hasParenthesis : true is the value has parenthesis, false otherwise
	 * @param technicalFramework : Reference to the Technical Framework
	 */

	protected void validateMultiple(String location, String value, SlotMetadata metadata,List<Notification> diagnostic, Boolean hasParenthesis, String technicalFramework) {

		String message;
		if(metadata.getMultiple()){
			message = "The value " + value + " of the slot " + metadata.getName() + " is multiple. Must have parenthesis";
			diagnostic.add(ValidatorUtil.addNotification(location, hasParenthesis, message, ERROR, technicalFramework));
		}else{
			message = "The value " + value + " of the slot " + metadata.getName() + " is not multiple. Must have NO parenthesis";
			diagnostic.add(ValidatorUtil.addNotification(location, !hasParenthesis, message, ERROR, technicalFramework));
		}	

	}

	/**
	 * Take a string as input, and delete its parenthesis
	 * @param value : Input value with (potentially) parenthesis
	 * @param isMultiple : true if value should be a multiple (regarding metadata), false otherwise
	 * @param hasParenthesis : true is the value has parenthesis, false otherwise
	 * @return The value string without parenthesis
	 */

	protected static String deleteParenthesis(String value, Boolean isMultiple, Boolean hasParenthesis) {

		String res = value;
		if(!value.isEmpty() && isMultiple && hasParenthesis){
			res = value.substring(1,value.length()-1);
		}
		return res;
	}

	/**
	 * Take a string as input, and delete its quotes
	 * @param value : Input value with (potentially) quotes
	 * @param isNumber : true if value should be a number (regarding metadata), false otherwise
	 * @param hasQuotes : true is the value has quotes, false otherwise
	 * @return The value string without quotes
	 */
	protected static String deleteQuotes(String value, Boolean isNumber, Boolean hasQuotes) {

		String res = value;
		if(!value.isEmpty() && hasQuotes && (isNumber == null || !isNumber) ){
			res = value.substring(1,value.length()-1);
		}
		return res;
	}

	/**
	 * Check that the tested Value is in the value set given by the metadata
	 * @param location : Location of the tested element in the entire document
	 * @param valueTested : The value that should be in the valueSet
	 * @param parentSlotMetadata : Parent of the slotMetadata needed to know how to get the valueset
	 * @param slotMetadata : The metadata of the slot (needed to get the valueset)
	 * @param nodeRepresentation : The nodeRepresentation attribute 
	 * @param diagnostic :  List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */
	protected void validateSlotValueFromValueSet(String location, String valueTested, RegistryObjectMetadata parentSlotMetadata, SlotMetadata slotMetadata, String nodeRepresentation, List<Notification> diagnostic, String technicalFramework) {

		String valueSetRaw = getValueSetRaw(parentSlotMetadata,slotMetadata);
		
		if(valueSetRaw != null && !valueSetRaw.isEmpty()){
			String[] valueSetList = getValueSetsFromValueSetRaw(valueSetRaw);
			
			List<Concept> listValues = getConceptsListFromValueSetList(valueSetList);
			List<String> listValueConceptCode = new ArrayList<String>();
			List<String> listValueConceptCodeClassification = new ArrayList<String>();

			if(listValues != null){
				for (Concept concept : listValues) {
					if(parentSlotMetadata instanceof ClassificationMetaData){
						listValueConceptCode.add(concept.getCodeSystem());
						listValueConceptCodeClassification.add(concept.getCode());
					}else if(parentSlotMetadata instanceof AdhocQueryMetadata && !slotMetadata.getIsCodeOnlyAsBoolean()){
						listValueConceptCode.add(concept.getCode() + "^^" + concept.getCodeSystem());
					}else{
						listValueConceptCode.add(concept.getCode());
					}

				}
				Boolean hasFound = listValueConceptCode.contains(valueTested);
				diagnostic.add(ValidatorUtil.addNotification(location, hasFound, valueTested + " must be part of one of the following valueSet list " + Arrays.toString(valueSetList), ERROR, technicalFramework));

				if(parentSlotMetadata instanceof ClassificationMetaData){
					Boolean hasFoundClassification = listValueConceptCodeClassification.contains(nodeRepresentation);
					diagnostic.add(ValidatorUtil.addNotification(location, hasFoundClassification, "The nodeRepresentation attribute = " + nodeRepresentation + " of the classification must be part of one of the following valueSet list " + Arrays.toString(valueSetList), ERROR, technicalFramework));
				}
			}
		}
	}

	/**
	 * 
	 * @param valueSetList : list of allowed value set
	 * @return the concept list 
	 */
	protected List<Concept> getConceptsListFromValueSetList(String[] valueSetList) {
		
		List<Concept> res = new ArrayList<Concept>();
		for (String valueSet : valueSetList) {
			List<Concept> conceptList = SVSConsumerForSimulator.getConceptsListFromValueSet(valueSet, null);
			if(conceptList != null){
				res.addAll(conceptList);
			}
		}
		return res;
	}

	/**
	 * Get the list of ValueSets of a metadata.
	 * @param valueSetRaw : the raw String of valueSet, with all valueset separated by commas.
	 * @return the list of ValueSets of a metadata.
	 */
	
	protected static String[] getValueSetsFromValueSetRaw(String valueSetRaw) {
		return valueSetRaw.split(",");
	}

	/**
	 * Get the raw string of the valueSet list in front of which the value must be tested
	 * @param parentSlotMetadata : Parent of the slotMetadata (containing the valueSet for Slot inside Classification)
	 * @param slotMetadata : Metadata containing the valueSet
	 * @return the string representation of the valueset
	 */
	protected static String getValueSetRaw(RegistryObjectMetadata parentSlotMetadata, SlotMetadata slotMetadata) {
		if(parentSlotMetadata instanceof ClassificationMetaData){
			
			return ((ClassificationMetaData) parentSlotMetadata).getValueset();
		}else{
			return slotMetadata.getValueset();
		}
	}


}
