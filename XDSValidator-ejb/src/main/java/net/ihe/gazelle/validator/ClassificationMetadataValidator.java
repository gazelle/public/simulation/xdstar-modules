package net.ihe.gazelle.validator;

import java.util.List;

import javax.xml.xpath.XPathExpressionException;

import net.ihe.gazelle.metadata.model.ClassificationMetaData;
import net.ihe.gazelle.utils.ValidatorUtil;
import net.ihe.gazelle.utils.XpathUtils;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespaceContext;

public class ClassificationMetadataValidator {
	
	private static final String WARNING = "warning";
	private static final String ERROR = "error";
	
	protected  ClassificationMetadataValidator(){}
	
	public static final ClassificationMetadataValidator instance = new ClassificationMetadataValidator(); 

	
	public void validate(String document,String location, ClassificationMetaData cmd, List<Notification> diagnostic, String technicalFramework){

		String nodeRepresentationValue = this.getNodeRepresentation(document);
		RegistryObjectMetadataValidator.instance.validate(document, location, cmd, nodeRepresentationValue, diagnostic, technicalFramework);
		this.validateClassificationSchemeValue(document, location, cmd, diagnostic, technicalFramework);
	}

	/**
	 * Check that the classificationScheme attribute of a classification is valid.
	 * 
	 * @param document : The classification XML node tested
	 * @param location : Location of the tested element in the entire document
	 * @param cmd : The ClassificationMetadata used to test the document
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */
	protected void validateClassificationSchemeValue(String document, String location, ClassificationMetaData cmd, List<Notification> diagnostic, String technicalFramework){
		
		String xpathString = "/rim:Classification/@classificationScheme";
		try {
			String classificationSchemeTested = XpathUtils.evaluateByStringValue(document, xpathString, XDSNamespaceContext.getInstance());
			String classificationSchemeValid = cmd.getUuid();
			boolean isValidClassificationScheme = classificationSchemeTested.equals(classificationSchemeValid);
			String message = "The classificationScheme of " + cmd.getName() + " is equal to " + classificationSchemeTested + ".\n Value allowed is " + classificationSchemeValid;
			diagnostic.add(ValidatorUtil.addNotification(location, isValidClassificationScheme, message, ERROR, technicalFramework));
			
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Get the nodeRepresentation attribute of the classification
	 * @param document: The classification XML node tested
	 * @return the nodeRepresentation attribute of the classification if it exists, an empty string otherwise
	 */
	protected String getNodeRepresentation(String document){

		String xpath = "/rim:Classification/@nodeRepresentation";
		String nodeRepresentation = null;
		try {
			nodeRepresentation = XpathUtils.evaluateByStringValue(document, xpath, XDSNamespaceContext.getInstance());
			
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return nodeRepresentation;
	}
	
}