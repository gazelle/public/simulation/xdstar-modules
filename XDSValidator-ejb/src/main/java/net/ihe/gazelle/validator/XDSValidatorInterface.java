package net.ihe.gazelle.validator;

import java.util.List;

import net.ihe.gazelle.validation.Notification;

public interface XDSValidatorInterface {

	public void validate(String document, Object metadata, List<Notification> diagnostic);
	
}
