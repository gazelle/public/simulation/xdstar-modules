package net.ihe.gazelle.validator;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.xpath.XPathExpressionException;

import net.ihe.gazelle.metadata.model.AdhocQueryMetadata;
import net.ihe.gazelle.metadata.model.ConstraintSpecification;
import net.ihe.gazelle.query.ReturnTypeType;
import net.ihe.gazelle.utils.ValidatorUtil;
import net.ihe.gazelle.utils.XpathUtils;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespaceContext;

public class AdhocQueryMetadataValidator {

	private static final String WARNING = "warning";
	private static final String ERROR = "error";

	protected  AdhocQueryMetadataValidator(){}

	public static final AdhocQueryMetadataValidator instance = new AdhocQueryMetadataValidator(); 

	public void validate(String document,String location, AdhocQueryMetadata aqmd, List<Notification> diagnostic, String technicalFramework){
		
		RegistryObjectMetadataValidator.instance.validate(document,location, aqmd, null, diagnostic, technicalFramework);
		this.validateReturnTypes(document,location, aqmd, diagnostic, technicalFramework);
		this.validateExtraConstraints(document, location, aqmd, diagnostic, technicalFramework);
	}
	
	
	/**
	 * Validate the Xpath extra constraints defined in the AdhocQueryMetadata.
	 * @param adhocQueryString : The AdhocQuery XML node tested
	 * @param location : Location of the tested element in the entire document
	 * @param aqmd : The ExtrinsicObjectMetadata used to test the document
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */
	protected void validateExtraConstraints(String adhocQueryString,String location, AdhocQueryMetadata aqmd,List<Notification> diagnostic, String technicalFramework) {

		if (aqmd.getExtraConstraintSpecifications() != null){
			for (ConstraintSpecification conspec : aqmd.getExtraConstraintSpecifications()) {
				this.validateConstraintSpecification(adhocQueryString, location, conspec, diagnostic, technicalFramework);
			}
		}
	}

	/**
	 * Check that the xpath extra constraint is valid
	 * @param adhocQueryString : The AdhocQuery XML node tested
	 * @param location : Location of the tested element in the entire document
	 * @param conspec : XPATH extra constraint 
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 * 
	 */
	protected void validateConstraintSpecification(String adhocQueryString, String location, ConstraintSpecification conspec, List<Notification> diagnostic, String technicalFramework) {

		ValidatorUtil.handleXpath(conspec.getXpath(), location, conspec.getDescription(), conspec.getKind(), adhocQueryString, diagnostic, technicalFramework);
	}

	/**
	 * Check that the AdhocQueryRequest has a return type value from the allowed values defined in aqmd.
	 * @param document : The AdhocQueryRequest XML node tested
	 * @param location : Location of the tested element in the entire document
	 * @param aqmd : AdhocQueryMetadata used to test the document
	 * @param diagnostic : List of tests results
	 * @param technicalFramework : Reference to the Technical Framework
	 */
	public void validateReturnTypes(String document, String location, AdhocQueryMetadata aqmd, List<Notification> diagnostic, String technicalFramework) {
		
		String xpathString = "/query:AdhocQueryRequest/query:ResponseOption/@returnType"; // xpath Request to get the request type of the AdhocQueryRequest
		try {
			String returnTypeTested = XpathUtils.evaluateByStringValue(document, xpathString, XDSNamespaceContext.getInstance());
			List<String> returnTypeValuesAllowed = getReturnTypeValuesAllowed(aqmd.getReturnTypes());
			boolean isValidReturnType = returnTypeValuesAllowed.contains(returnTypeTested);
			String message = "The return type is equal to " + returnTypeTested + ".\n Allowed return type values are " + Arrays.toString(returnTypeValuesAllowed.toArray());
			diagnostic.add(ValidatorUtil.addNotification(location, isValidReturnType, message, ERROR, technicalFramework));
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * Get String values of valid return types from the list of ReturnType objects.
	 * @param returnTypes : list of return types
	 * @return : String list of valid return types.
	 */
	protected List<String> getReturnTypeValuesAllowed(List<ReturnTypeType> returnTypes) {
		List<String> res = new ArrayList<String>();
		if (returnTypes != null) {
			for (ReturnTypeType rt : returnTypes) {
				res.add(rt.value());
			}
		}
		return res;
	}

}