package net.ihe.gazelle.xdstar.comon.util;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.model.Concept;
import net.ihe.gazelle.simulator.common.utils.XmlUtil;

import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.seam.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SVSConsumerForSimulator{
	
	static final String IDENTIFIER = "1.3.6.1.4.1.12559.11.4.9";

	/**
	 * 
	 * @param valueSetId
	 * @param lang
	 * @return
	 */
	public static List<Concept> getConceptsListFromValueSet(String valueSetId, String lang)
	{
		if (valueSetId == null || valueSetId.isEmpty())
		{
			return null;
		}
		String svsRepository = getSVSRepositoryUrl(null);
		ClientRequest request = new ClientRequest(svsRepository);
		request.queryParameter("id", valueSetId);
		if (lang != null && !lang.isEmpty())
		{
			request.queryParameter("lang", lang);
		}
		try {
			ClientResponse<String> response = request.get(String.class);
			if (response.getStatus() == 200)
			{
				String xmlContent = response.getEntity();
				Document document = XmlUtil.parse(xmlContent);
				NodeList concepts = document.getElementsByTagName("Concept");
				if (concepts.getLength() > 0)
				{
					List<Concept> conceptsList = new ArrayList<Concept>();
					for(int index = 0; index < concepts.getLength(); index ++)
					{
						Node conceptNode = concepts.item(index);
						NamedNodeMap attributes = conceptNode.getAttributes();
						String codeScheme = null;
						if (attributes.getNamedItem("codeSystemName") != null){
							codeScheme = attributes.getNamedItem("codeSystemName").getTextContent();
						}
						if (attributes.getNamedItem("codeSystem") != null && 
								attributes.getNamedItem("codeSystem").getTextContent() != null &&
								!attributes.getNamedItem("codeSystem").getTextContent().equals(IDENTIFIER)){
							codeScheme = attributes.getNamedItem("codeSystem").getTextContent();
						}
						conceptsList.add(new Concept(attributes.getNamedItem("code").getTextContent(), 
							attributes.getNamedItem("displayName").getTextContent(), 
							codeScheme));
					}
					Concept.sort(conceptsList);
					return conceptsList;					
				}
				else
				{
					return null;
				}
			}
			else
				return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Returns the displayName attribute associate to a given code from a given value set
	 * @param valueSetId
	 * @param lang
	 * @param code
	 * @return
	 */
	public static String getDisplayNameForGivenCode(String valueSetId, String lang, String code)
	{
		Concept concept = getConceptForCode(valueSetId, lang, code);
		if (concept != null)
		{
			return concept.getDisplayName();
		}
		else
		{
			return null;
		}
	}
	
	public static Concept getConceptForCode(String valueSetId, String lang, String code)
	{
		if (valueSetId == null || valueSetId.isEmpty())
		{
			return null;
		}
		if (code == null || code.isEmpty())
		{
			return null;
		}
		String svsRepository = getSVSRepositoryUrl(null);
		ClientRequest request = new ClientRequest(svsRepository);
		request.queryParameter("id", valueSetId);
		if (lang != null && !lang.isEmpty())
		{
			request.queryParameter("lang", lang);
		}
		request.queryParameter("code", code);
		
		try {
			ClientResponse<String> response = request.get(String.class);
			if (response.getStatus() == 200)
			{
				String xmlContent = response.getEntity();
				Document document = XmlUtil.parse(xmlContent);
				NodeList concepts = document.getElementsByTagName("Concept");
				if (concepts.getLength() > 0)
				{
					Node conceptNode = concepts.item(0);
					NamedNodeMap attributes = conceptNode.getAttributes();
					String codeScheme = null;
					if (attributes.getNamedItem("codeSystemName") != null) {
						codeScheme = attributes.getNamedItem("codeSystemName").getTextContent();
					}
					if (attributes.getNamedItem("codeSystem") != null && 
							attributes.getNamedItem("codeSystem").getTextContent() != null &&
							!attributes.getNamedItem("codeSystem").getTextContent().equals(IDENTIFIER)){
						codeScheme = attributes.getNamedItem("codeSystem").getTextContent();
					}
					return new Concept(attributes.getNamedItem("code").getTextContent(), 
							attributes.getNamedItem("displayName").getTextContent(), 
							codeScheme);
				}
				else
				{
					return null;
				}
			}
			else
				return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 
	 * @param entityManager
	 * @return
	 */
	private static String getSVSRepositoryUrl(EntityManager entityManager)
	{
		// if the entityManager is not provided we use the one managed by Seam
		try{
			if(entityManager == null)
			{
				entityManager = (EntityManager) Component.getInstance("entityManager");
			}
			String svsRepository = ApplicationConfiguration.getValueOfVariable("svs_repository_url", entityManager);
			if (svsRepository == null)
			{
				ApplicationConfiguration pref = new ApplicationConfiguration("svs_repository_url", "http://gazelle.ihe.net/SVSSimulator/rest/RetrieveValueSetForSimulator");
				entityManager.merge(pref);
				entityManager.flush();
				svsRepository = pref.getValue();
			}
			return svsRepository;
		}
		catch (Exception e){
			return "https://gazelle.ihe.net/SVSSimulator/rest/RetrieveValueSetForSimulator";
		}
		
	}
	
	
}
