package net.ihe.gazelle.xdstar.comon.util;

import java.util.Iterator;

import javax.xml.namespace.NamespaceContext;


public class XDSNamespaceContext implements NamespaceContext {
	
	private XDSNamespaceContext(){}
	
	private static XDSNamespaceContext instance;
	
	public static XDSNamespaceContext getInstance(){
		if (instance == null){
			synchronized (XDSNamespaceContext.class) {
				instance = new XDSNamespaceContext();
			}
		}
		return instance;
	}

	@Override
	public String getNamespaceURI(String prefix) {
		XDSNamespace currentns = XDSNamespace.getXDSamespaceByPrefix(prefix);
		if (currentns != null){
			return currentns.getUri();
		}
		return null;
	}

	@Override
	public String getPrefix(String namespaceURI) {
		XDSNamespace currentns = XDSNamespace.getXDSamespaceByURI(namespaceURI);
		if (currentns != null){
			return currentns.getPrefix();
		}
		return null;
	}

	@Override
	public Iterator<String> getPrefixes(String namespaceURI) {
		return null;
	}

}
