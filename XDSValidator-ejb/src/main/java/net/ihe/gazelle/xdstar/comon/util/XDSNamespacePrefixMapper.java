package net.ihe.gazelle.xdstar.comon.util;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class XDSNamespacePrefixMapper extends NamespacePrefixMapper   {
	
	@Override
	public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix){
		XDSNamespace ns = XDSNamespace.getXDSamespaceByURI(namespaceUri);
		if (ns != null){
			return ns.getPrefix();
		}
		return suggestion;
	}

}
