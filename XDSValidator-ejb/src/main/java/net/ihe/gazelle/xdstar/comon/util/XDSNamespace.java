package net.ihe.gazelle.xdstar.comon.util;

public enum XDSNamespace {
	
	SOAPENVELOPE("s", "http://www.w3.org/2003/05/soap-envelope"), 
	ADDRESSING ("a", "http://www.w3.org/2005/08/addressing"),
	RIM("rim", "urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0"), 
	LCM("lcm", "urn:oasis:names:tc:ebxml-regrep:xsd:lcm:3.0"), 
	QUERY("query", "urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0"),
	RS("rs", "urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0"), 
	XDSB("xdsb", "urn:ihe:iti:xds-b:2007"), 
	XOP("xop", "http://www.w3.org/2004/08/xop/include"), 
	RAD("rad", "urn:ihe:rad:xdsi-b:2009"),
	HL7V3("hl7v3", "urn:hl7-org:v3");
	
	private String prefix;
	
	private String uri;
	
	public String getPrefix() {
		return prefix;
	}

	public String getUri() {
		return uri;
	}

	private XDSNamespace(String prefix, String uri) {
		this.prefix = prefix;
		this.uri = uri;
	}
	
	public static XDSNamespace getXDSamespaceByURI(String uri){
		for (XDSNamespace ns : XDSNamespace.values()) {
			if (ns.uri.equals(uri)){
				return ns;
			}
		}
		return null;
	}
	
	public static XDSNamespace getXDSamespaceByPrefix(String prefix){
		for (XDSNamespace ns : XDSNamespace.values()) {
			if (ns.prefix.equals(prefix)){
				return ns;
			}
		}
		return null;
	}

}
