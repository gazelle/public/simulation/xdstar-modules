package net.ihe.gazelle.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import net.sf.saxon.xpath.XPathFactoryImpl;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public final class XpathUtils {
	
	public static Boolean evaluateByString(String document, String xpathString, NamespaceContext xmlns) throws XPathExpressionException {
		Boolean b = null;
		XPathFactory fabrique = new XPathFactoryImpl();
		XPath xpath = fabrique.newXPath();
		if (xmlns != null){
			xpath.setNamespaceContext(xmlns);
		}
		InputSource is = new InputSource(new ByteArrayInputStream(document.getBytes(StandardCharsets.UTF_8)));
		b = (Boolean) xpath.evaluate(xpathString, is, XPathConstants.BOOLEAN);
		return b;
	}
	
	public static String evaluateByStringValue(String document, String xpathString, NamespaceContext xmlns) throws XPathExpressionException {
		XPathFactory fabrique = new XPathFactoryImpl();
		XPath xpath = fabrique.newXPath();
		if (xmlns != null){
			xpath.setNamespaceContext(xmlns);
		}
		InputSource is = new InputSource(new ByteArrayInputStream(document.getBytes(StandardCharsets.UTF_8)));
		return (String) xpath.evaluate(xpathString, is, XPathConstants.STRING);
		
	}
	
	public static NodeList getNodeListFromString(String string, String nodeName, String namespace) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(new ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8)));
		NodeList dd = doc.getElementsByTagNameNS(namespace, nodeName);
		return dd;
	}

	public static Node getNodeFromString(String string, String nodeName, String namespace) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(new ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8)));
		NodeList dd = doc.getElementsByTagNameNS(namespace, nodeName);
		return dd.item(0);
	}
	
	
	public static Map<Node,Integer> getNodeMapByAttribute(NodeList nl, String attributeName, String attributeValueTested){
		
	 	Map<Node,Integer> res = new HashMap<Node,Integer>();
		for(int i = 0; i < nl.getLength(); i++){
			Node current = nl.item(i);
			if(current.getAttributes().getNamedItem(attributeName) != null){
				String attributeValue = current.getAttributes().getNamedItem(attributeName).getNodeValue();
				if(attributeValueTested != null && attributeValueTested.equals(attributeValue)){
					res.put(current,i);
				}
			}
			
		}
		return res;
	}
	
	public static List<Node> getNodeListByAttribute(NodeList nl, String attributeName, String attributeValueTested){
		
	 	List<Node> res = new ArrayList<Node>();
		for(int i = 0; i < nl.getLength(); i++){
			Node current = nl.item(i);
			if(current.getAttributes().getNamedItem(attributeName) != null){
				String attributeValue = current.getAttributes().getNamedItem(attributeName).getNodeValue();
				if(attributeValueTested != null && attributeValueTested.equals(attributeValue)){
					res.add(current);
				}
			}
			
		}
		return res;
	}
	
	public static String nodeToString(Node node) {
		StringWriter sw = new StringWriter();
		try {
			Transformer t = TransformerFactory.newInstance().newTransformer();
			t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			t.transform(new DOMSource(node), new StreamResult(sw));
		} catch (TransformerException te) {
			System.out.println("nodeToString Transformer Exception");
		}
		return sw.toString();
	}

}
