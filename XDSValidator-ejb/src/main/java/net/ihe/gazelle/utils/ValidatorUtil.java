package net.ihe.gazelle.utils;


import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPathExpressionException;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespaceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ValidatorUtil {

    private static final Logger LOG = LoggerFactory.getLogger(ValidatorUtil.class);

    public static void handleXpath(String xpath, String location, String description, String optionality, String document, List<Notification> diagnostic, String technicalFramework) {

        try {
            boolean res = XpathUtils.evaluateByString(document, xpath, XDSNamespaceContext.getInstance());
            diagnostic.add(ValidatorUtil.addNotification(location, res, description, optionality, technicalFramework));

        } catch (XPathExpressionException e) {
            LOG.error("handleXpath : " + e.getMessage());
        }
    }

    public static Boolean hasMatch(String regex, String val) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(regex);
        matcher = pattern.matcher(val);
        return matcher.find();
    }

    public static Notification addNotification(String location, boolean isOk, String description, String optionality, String technicalFramework) {
        try {
            if (technicalFramework == null) {
                technicalFramework = "";
            }
            if (isOk) {
                Note not = new Note();
                not.setDescription(description + " (TF - " + technicalFramework + ").");
                not.setLocation(location);
                return not;
            } else {
                if (optionality.equalsIgnoreCase("error")) {
                    Error not = new Error();
                    not.setDescription(description + " (TF - " + technicalFramework + ").");
                    not.setLocation(location);
                    return not;
                } else if (optionality.equalsIgnoreCase("warning")) {
                    Warning not = new Warning();
                    not.setDescription(description + " (TF - " + technicalFramework + ").");
                    not.setLocation(location);
                    return not;
                }
            }
        } catch (Exception e) {
            Warning not = new Warning();
            not.setDescription("This condition can not be well performed by the validator : " + description);
            not.setLocation(location);
            return not;
        }
        return null;
    }

}
