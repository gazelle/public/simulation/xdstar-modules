package net.ihe.gazelle.metadata.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.jboss.seam.annotations.Name;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConstraintSpecification")
@Entity
@Name("constraintSpecification")
@Table(name = "constraint_specification", schema = "public")
@SequenceGenerator(name = "constraint_specification_sequence", sequenceName = "constraint_specification_id_seq", allocationSize = 1)
public class ConstraintSpecification implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XmlTransient
	@Id
	@GeneratedValue(generator = "constraint_specification_sequence", strategy = GenerationType.SEQUENCE)
	@NotNull
	@Column(name = "id")
	private Integer id;

	@XmlAttribute(name = "kind")
	protected String kind;

	@XmlAttribute(name = "xpath")
	@Column(length = 1500)
	protected String xpath;

	@XmlAttribute(name = "description")
	@Column(length = 800)
	protected String description;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getKind() {
		if ((kind != null) && kind.equals("")) {
			kind = null;
		}
		return kind;
	}

	public void setKind(String kind) {
		if ((kind != null) && kind.equals("")) {
			kind = null;
		}
		this.kind = kind;
	}

	public String getXpath() {
		if ((xpath != null) && xpath.equals("")) {
			xpath = null;
		}
		return xpath;
	}

	public void setXpath(String xpath) {
		if ((xpath != null) && xpath.equals("")) {
			xpath = null;
		}
		this.xpath = xpath;
	}

	public String getDescription() {
		if ((description != null) && description.equals("")) {
			description = null;
		}
		return description;
	}

	public void setDescription(String description) {
		if ((description != null) && description.equals("")) {
			description = null;
		}
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result)
				+ ((description == null) ? 0 : description.hashCode());
		result = (prime * result) + ((kind == null) ? 0 : kind.hashCode());
		result = (prime * result) + ((xpath == null) ? 0 : xpath.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ConstraintSpecification other = (ConstraintSpecification) obj;
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!description.equals(other.description)) {
			return false;
		}
		if (kind == null) {
			if (other.kind != null) {
				return false;
			}
		} else if (!kind.equals(other.kind)) {
			return false;
		}
		if (xpath == null) {
			if (other.xpath != null) {
				return false;
			}
		} else if (!xpath.equals(other.xpath)) {
			return false;
		}
		return true;
	}

}
