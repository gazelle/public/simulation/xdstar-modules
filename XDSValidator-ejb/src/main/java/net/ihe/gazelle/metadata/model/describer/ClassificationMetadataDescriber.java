package net.ihe.gazelle.metadata.model.describer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import net.ihe.gazelle.metadata.model.CardinalityEnum;
import net.ihe.gazelle.metadata.model.ClassificationMetaData;

import org.jboss.seam.annotations.Name;

@Entity
@Name("ClassificationMetadataDescriber")
@Table(name="classification_metadata_describer", schema = "public")
@SequenceGenerator(name = "classification_metadata_describer_sequence", sequenceName = "classification_metadata_describer_id_seq", allocationSize=1)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClassificationMetadataDescriber", propOrder = {
	"cmd",
	"cardinality"
})
@XmlRootElement(name = "ClassificationMetadataDescriber")
public class ClassificationMetadataDescriber  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="classification_metadata_describer_sequence")	
	@XmlTransient
	private Integer id;
	
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "classification_id")
	private ClassificationMetaData cmd;
	
	@Column(name = "cardinality")
	@Enumerated(EnumType.STRING)
	private CardinalityEnum cardinality;

	public ClassificationMetaData getCmd() {
		return cmd;
	}

	public void setCmd(ClassificationMetaData cmd) {
		this.cmd = cmd;
	}

	public CardinalityEnum getCardinality() {
		return cardinality;
	}

	public void setCardinality(CardinalityEnum cardinality) {
		this.cardinality = cardinality;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public static List<ClassificationMetaData> getListClassificationMetadataFromDescriber(List<ClassificationMetadataDescriber> cmdd){
		List<ClassificationMetaData> res = new ArrayList<ClassificationMetaData>();
		for (ClassificationMetadataDescriber classificationMetaData : cmdd) {
			res.add(classificationMetaData.getCmd());
		}		
		return res;
	}
	
	public void updateContent(ClassificationMetaData cmd, String cardinalityString) {
		CardinalityEnum enumc = CardinalityEnum.getCardinalityFromValue(cardinalityString);
		this.cardinality = enumc;
		this.cmd = cmd;
	}
	
	
}
