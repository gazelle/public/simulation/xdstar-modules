package net.ihe.gazelle.metadata.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Cardinality")
@XmlRootElement(name = "Cardinality")
public enum CardinalityEnum {
	
	_00(0, 0), _01(0, 1), _11(1, 1), _12(1, 2), _02(0, 2), _0star(0, -1), _1star(1, -1);
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="cardinality_enum_sequence")
	@XmlTransient
	private int id;
	
	private int min;
	
	private int max;
	
	private CardinalityEnum() {}
	
	private CardinalityEnum(int min, int max) {
		this.min = min;
		this.max = max;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}
	
	public boolean isOptional(){
		return getMin() == 0;
	}

	public boolean isRequired() {
		return getMin() != 0;

	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public String toString(){
		
		String max;
		if(this.getMax() == -1){
			max = "*";
		}else{
			max = this.getMax()+"";
		}
		
		return this.getMin() + ".." + max;
		
	}
	
	public static CardinalityEnum getCardinalityFromValue(String value){
		if(value.equals("0..0")){
			return CardinalityEnum._00;
		}else if(value.equals("0..1")){
			return CardinalityEnum._01;
		}else if(value.equals("0..2")){
			return CardinalityEnum._02;
		}else if(value.equals("1..1")){
			return CardinalityEnum._11;
		}else if(value.equals("1..2")){
			return CardinalityEnum._12;
		}else if(value.equals("0..*")){
			return CardinalityEnum._0star;
		}else if(value.equals("1..*")){
			return CardinalityEnum._1star;
		}
		return null;
	}

}
