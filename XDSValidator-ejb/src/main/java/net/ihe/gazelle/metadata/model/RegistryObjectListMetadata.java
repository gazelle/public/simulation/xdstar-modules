package net.ihe.gazelle.metadata.model;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.metadata.model.describer.ExtrinsicObjectMetadataDescriber;
import net.ihe.gazelle.metadata.model.describer.RegistryPackageMetadataDescriber;
import net.ihe.gazelle.simulator.sut.model.Usage;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Name("registryObjectListMetadata")
@Table(name = "registry_object_list_metadata", schema = "public")
@SequenceGenerator(name = "registry_object_list_sequence", sequenceName = "registry_object_list_id_seq", allocationSize = 1)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("RegistryObjectList")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegistryObjectListMetadata", propOrder = { "name", "displayAttributeName", "usages", "extrinsicObject", "registryPackage", "extraConstraintSpecifications" })
@XmlRootElement(name = "RegistryObjectListMetadata")
public class RegistryObjectListMetadata implements Comparable<RegistryObjectListMetadata>, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "registry_object_list_sequence")
	@XmlTransient
	private Integer id;

    @Column(name = "name", unique = true)
    private String name;
	
	private String displayAttributeName;
	
	@ManyToMany(cascade={CascadeType.MERGE, CascadeType.PERSIST})
	@Fetch(FetchMode.SUBSELECT)
	@JoinTable(name="registry_object_list_metadata_usage")
	private List<Usage> usages;

	@ManyToMany(fetch=FetchType.EAGER, cascade = { CascadeType.ALL })
	@Fetch(FetchMode.SUBSELECT)
	@JoinTable(name="registry_object_list_metadata_extrinsicObject")
	private List<ExtrinsicObjectMetadataDescriber> extrinsicObject;

	@ManyToMany(fetch=FetchType.EAGER, cascade = { CascadeType.ALL })
	@Fetch(FetchMode.SUBSELECT)
	@JoinTable(name="registry_object_list_metadata_registryPackage")
	private List<RegistryPackageMetadataDescriber> registryPackage;

	@OneToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "registry_object_list_extra_constraints", joinColumns = @JoinColumn(name = "registry_object_list_id"),
			inverseJoinColumns = @JoinColumn(name = "extra_constraint_specifications_id"))
	protected List<ConstraintSpecification> extraConstraintSpecifications;

	public List<ConstraintSpecification> getExtraConstraintSpecifications() {
		return extraConstraintSpecifications;
	}

	public void setExtraConstraintSpecifications(List<ConstraintSpecification> extraConstraintSpecifications) {
		this.extraConstraintSpecifications = extraConstraintSpecifications;
	}

	public List<ExtrinsicObjectMetadataDescriber> getExtrinsicObject()
	{
		if(extrinsicObject == null){
			return new ArrayList<ExtrinsicObjectMetadataDescriber>();
		}
		return extrinsicObject;
	}

	public void	setExtrinsicObject(List<ExtrinsicObjectMetadataDescriber> extrinsicObject) {
		this.extrinsicObject = extrinsicObject;
	}

	public List<RegistryPackageMetadataDescriber> getRegistryPackage() {
		if (registryPackage == null) {
			return new ArrayList<RegistryPackageMetadataDescriber>();
		}
		return registryPackage;
	}

	public void setRegistryPackage(List<RegistryPackageMetadataDescriber> registryPackage) {
		this.registryPackage = registryPackage;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getDisplayAttributeName() {
		return displayAttributeName;
	}

	public void setDisplayAttributeName(String displayAttributeName) {
		this.displayAttributeName = displayAttributeName;
	}
	
	public void setUsages(List<Usage> usages) {
		this.usages = usages;
	}

	public List<Usage> getUsages() {
		if (usages == null) {
			usages = new ArrayList<Usage>();
		}
		return usages;
	}
	
	public void addExtrinsicObject(ExtrinsicObjectMetadata extrinsic_elt, String cardinality) {
		ExtrinsicObjectMetadataDescriber tmp = new ExtrinsicObjectMetadataDescriber();
		CardinalityEnum card = CardinalityEnum.getCardinalityFromValue(cardinality);
		tmp.setCardinality(card);
		tmp.setEom(extrinsic_elt);
		if(extrinsicObject == null){
			extrinsicObject = new ArrayList<ExtrinsicObjectMetadataDescriber>();
		}
	    this.extrinsicObject.add(tmp);
	}
	
	public void addRegistryPackage(RegistryPackageMetadata registry_elt, String cardinality) {
		RegistryPackageMetadataDescriber tmp = new RegistryPackageMetadataDescriber();
		CardinalityEnum card = CardinalityEnum.getCardinalityFromValue(cardinality);
		tmp.setCardinality(card);
		tmp.setRpm(registry_elt);
		if(registryPackage == null){
			registryPackage = new ArrayList<RegistryPackageMetadataDescriber>();
		}
	    this.registryPackage.add(tmp);
	}
	
	
	public List<ExtrinsicObjectMetadata> getListExtrinsicObjectMetadatas(){
		List<ExtrinsicObjectMetadata> listExtrinsicObject = new ArrayList<ExtrinsicObjectMetadata>();
		if(extrinsicObject != null){
			for (ExtrinsicObjectMetadataDescriber eom : extrinsicObject) {
				listExtrinsicObject.add(eom.getEom());
			}
		}
		return listExtrinsicObject;
	}
	
	public List<RegistryPackageMetadata> getListRegistryPackageMetadatas(){
		List<RegistryPackageMetadata> listRegistryPackage = new ArrayList<RegistryPackageMetadata>();
		if(registryPackage != null){
			for (RegistryPackageMetadataDescriber rpm : registryPackage) {
				listRegistryPackage.add(rpm.getRpm());
			}
		}
		return listRegistryPackage;
	}
	

	public static RegistryObjectListMetadata searchRegistryObjectListMetadataByName(List<RegistryObjectListMetadata> lromld, String name) {
		for (RegistryObjectListMetadata romd : lromld) {
			if(romd.getName().equals(name)){
				return romd;
			}
		}
		return null;
	}

	public static List<RegistryObjectListMetadata> getRegistryObjectListMetadata() {
		HQLQueryBuilder<RegistryObjectListMetadata> hql = new HQLQueryBuilder<RegistryObjectListMetadata>(RegistryObjectListMetadata.class);
		return hql.getList();
	}
	
	@Override
	public int compareTo(RegistryObjectListMetadata arg0) {
		if (this.getId() == null) {
			return -1;
		}
		if (arg0 == null) {
			return 1;
		}
		if (arg0.getId() == null) {
			return 1;
		}
		return this.getId().compareTo(arg0.getId());
	}
	
	public static List<RegistryObjectListMetadata> getAllListRegistryObjectListMetadata(){
		RegistryObjectListMetadataQuery qq = new RegistryObjectListMetadataQuery();
		List<RegistryObjectListMetadata> res = qq.getList();
		return res;
	}
	
	public void editExtrinsic_Object(ExtrinsicObjectMetadataDescriber res, ExtrinsicObjectMetadata extrinsic_object, String cardinality) {
		
		res.setEom(extrinsic_object);
		res.setCardinality(CardinalityEnum.getCardinalityFromValue(cardinality));
	}
	
	public void editRegistry_Package(RegistryPackageMetadataDescriber res, RegistryPackageMetadata registry_package, String cardinality) {
		
		res.setRpm(registry_package);
		res.setCardinality(CardinalityEnum.getCardinalityFromValue(cardinality));
	}
	
	
}
