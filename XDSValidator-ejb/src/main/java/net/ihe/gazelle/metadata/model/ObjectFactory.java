package net.ihe.gazelle.metadata.model;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry

public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {}
    
    /**
     * Create an instance of {@link  ClassificationMetaData}
     * 
     */
    public ClassificationMetaData createClassificationMetaData() {
        return new ClassificationMetaData();
    }

    /**
     * Create an instance of {@link  RegistryObjectMetadata}
     * 
     */
    public RegistryObjectMetadata createRegistryObjectMetadata() {
        return new RegistryObjectMetadata();
    }

    /**
     * Create an instance of {@link  SlotMetadata}
     * 
     */
    public SlotMetadata createSlotMetadata() {
        return new SlotMetadata();
    }

    /**
     * Create an instance of {@link  ExternalIdentifierMetadata}
     * 
     */
    public ExternalIdentifierMetadata createExternalIdentifierMetadata() {
        return new ExternalIdentifierMetadata();
    }

    /**
     * Create an instance of {@link  RegistryPackageMetadata}
     * 
     */
    public RegistryPackageMetadata createRegistryPackageMetadata() {
        return new RegistryPackageMetadata();
    }

    /**
     * Create an instance of {@link  AdhocQueryMetadata}
     * 
     */
    public AdhocQueryMetadata createAdhocQueryMetadata() {
        return new AdhocQueryMetadata();
    }
    
    /**
     * Create an instance of {@link  ExtrinsicObjectMetadata}
     * 
     */
    public ExtrinsicObjectMetadata createExtrinsicObjectMetadata() {
        return new ExtrinsicObjectMetadata();
    }


}