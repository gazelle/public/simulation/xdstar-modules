package net.ihe.gazelle.metadata.model.describer;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import net.ihe.gazelle.metadata.model.AdhocQueryMetadata;
import net.ihe.gazelle.metadata.model.CardinalityEnum;

import org.jboss.seam.annotations.Name;

@Entity
@Name("AdhocQueryMetadataDescriber")
@Table(name="adhocquery_metadata_describer", schema = "public")
@SequenceGenerator(name = "adhocquery_metadata_describer_sequence", sequenceName = "adhocquery_metadata_describer_id_seq", allocationSize=1)
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("AdhocQueryMetadataDescriber")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdhocQueryMetadataDescriber", propOrder = {
	"aqm",
	"cardinality"
})
@XmlRootElement(name = "AdhocQueryMetadataDescriber")
public class AdhocQueryMetadataDescriber  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="adhocquery_metadata_describer_sequence")	
	@XmlTransient
	private Integer id;
	
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "adhoc_query_id")
	private AdhocQueryMetadata aqm;
	
	@Column(name = "cardinality")
	@Enumerated(EnumType.STRING)
	private CardinalityEnum cardinality;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public AdhocQueryMetadata getAqm() {
		return aqm;
	}

	public void setAqm(AdhocQueryMetadata aqm) {
		this.aqm = aqm;
	}

	public CardinalityEnum getCardinality() {
		return cardinality;
	}

	public void setCardinality(CardinalityEnum cardinality) {
		this.cardinality = cardinality;
	}
	
	
	
}
