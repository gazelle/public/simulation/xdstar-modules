/**
 * metadata/ClassificationMetaData.java
 * <p>
 * File generated from the ClassificationMetaData uml Class
 * Generated by the Acceleo UML 2.1 to Java generator module (Obeo)
 */
package net.ihe.gazelle.metadata.model;

// End of user code

import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;


/**
 * Description of the class ClassificationMetaData.
 */

@Entity
@Name("extrinsicObjectMetadata")
@DiscriminatorValue("ExtrinsicObject")

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtrinsicObjectMetadata")
@XmlRootElement(name = "ExtrinsicObjectMetadata")

public class ExtrinsicObjectMetadata extends net.ihe.gazelle.metadata.model.RegistryObjectMetadata {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @OneToMany(cascade = {CascadeType.ALL})
    @JoinTable(name = "extrinsic_object_extra_constraints", joinColumns = @JoinColumn(name = "registry_object_metadata_id"),
            inverseJoinColumns = @JoinColumn(name = "extraconstraintspecifications_id"))
    protected List<ConstraintSpecification> extraConstraintSpecifications;

    public List<ConstraintSpecification> getExtraConstraintSpecifications() {
        return extraConstraintSpecifications;
    }

    public void setExtraConstraintSpecifications(List<ConstraintSpecification> extraConstraintSpecifications) {
        this.extraConstraintSpecifications = extraConstraintSpecifications;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ExtrinsicObjectMetadata other = (ExtrinsicObjectMetadata) obj;
        if (this == other) {
            return true;
        }
        if (getId() == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!getId().equals(other.getId())) {
            return false;
        }
        return true;
    }

}