/**
 * metadata/RegistryPackageMetadata.java
 *
 * File generated from the RegistryPackageMetadata uml Class
 * Generated by the Acceleo UML 2.1 to Java generator module (Obeo)
 */
package net.ihe.gazelle.metadata.model;

// End of user code

import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;


/**
 * Description of the class RegistryPackageMetadata.
 *
 */
@Entity
@Name("registryPackageMetadata")
@DiscriminatorValue("RegistryPackage")

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegistryPackageMetadata")
@XmlRootElement(name = "RegistryPackageMetadata")

public class RegistryPackageMetadata extends net.ihe.gazelle.metadata.model.RegistryObjectMetadata {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@OneToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "registry_package_extra_constraints")
	protected List<ConstraintSpecification> extraConstraintSpecifications;

	public List<ConstraintSpecification> getExtraConstraintSpecifications() {
		return extraConstraintSpecifications;
	}

	public void setExtraConstraintSpecifications(List<ConstraintSpecification> extraConstraintSpecifications) {
		this.extraConstraintSpecifications = extraConstraintSpecifications;
	}
	
}