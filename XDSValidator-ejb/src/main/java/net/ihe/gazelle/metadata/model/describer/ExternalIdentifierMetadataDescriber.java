package net.ihe.gazelle.metadata.model.describer;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import net.ihe.gazelle.metadata.model.CardinalityEnum;
import net.ihe.gazelle.metadata.model.ExternalIdentifierMetadata;

import org.jboss.seam.annotations.Name;

@Entity
@Name("ExternalIdentifierMetadataDescriber")
@Table(name="external_identifier_metadata_describer", schema = "public")
@SequenceGenerator(name = "external_identifier_metadata_describer_sequence", sequenceName = "external_identifier_metadata_describer_id_seq", allocationSize=1)
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("ExternalIdentifierMetadataDescriber")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExternalIdentifierMetadataDescriber", propOrder = {
	"eim",
	"cardinality"
})
@XmlRootElement(name = "ExternalIdentifierMetadataDescriber")
public class ExternalIdentifierMetadataDescriber implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="external_identifier_metadata_describer_sequence")	
	@XmlTransient
	private int id;
	
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "external_identifier_id")
	private ExternalIdentifierMetadata eim;
	
	@Column(name = "cardinality")
	@Enumerated(EnumType.STRING)
	private CardinalityEnum cardinality;

	public ExternalIdentifierMetadata getEim() {
		return eim;
	}

	public void setEim(ExternalIdentifierMetadata eim) {
		this.eim = eim;
	}

	public CardinalityEnum getCardinality() {
		return cardinality;
	}

	public void setCardinality(CardinalityEnum cardinality) {
		this.cardinality = cardinality;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
