package net.ihe.gazelle.validator.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.metadata.model.AdhocQueryMetadata;
import net.ihe.gazelle.metadata.model.ClassificationMetaData;
import net.ihe.gazelle.metadata.model.SlotMetadata;
import net.ihe.gazelle.simulator.common.model.Concept;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validator.SlotMetadataValidator;

import org.junit.Test;

public class SlotMetadataValidatorTest extends SlotMetadataValidator{

	@Test
	public void testHasQuotes() {

		assertTrue(hasQuotes("'validTestString'"));
		assertFalse(hasQuotes("notvalidTestString1'"));
		assertFalse(hasQuotes("'notvalidTestString2"));
		assertFalse(hasQuotes("notvalidTestString3"));
		assertFalse(hasQuotes("n'ot'validTestString4"));

	}

	@Test
	public void testHasParenthesis() {
		assertTrue(hasParenthesis("(validTestString)"));
		assertFalse(hasParenthesis("notvalidTestString1)"));
		assertFalse(hasParenthesis("(notvalidTestString2"));
		assertFalse(hasParenthesis("notvalidTestString3"));
		assertFalse(hasParenthesis("n(ot)validTestString4"));
		assertFalse(hasParenthesis(")validTestString5("));

	}

	@Test
	public void testValidateSlotValueByRegex() {

		List<Notification> diagnostic = new ArrayList<Notification>();		
		validateSlotValueByRegex("123", "location", "^[0-9]*$", diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Note); // value matches the regex [only numbers]

		diagnostic = new ArrayList<Notification>();		
		validateSlotValueByRegex("abc", "location", "^[0-9]*$", diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Error); // value does'nt match the regex [only numbers]
	}

	@Test
	public void testSplitSlotValues() {

		String[] res = splitSlotValues("'val1'", false, true);
		assertTrue(res[0].equals("'val1'"));

		res = splitSlotValues("'val1','val2'", false, true);
		assertTrue(res[0].equals("'val1'"));
		assertTrue(res[1].equals("'val2'"));

		res = splitSlotValues("123", true, false);
		assertTrue(res[0].equals("123"));

		res = splitSlotValues("123,456", true, false);
		assertTrue(res[0].equals("123"));
		assertTrue(res[1].equals("456"));

		res = splitSlotValues("'val1','val2'", false, false);

		assertTrue(res[0].equals("'val1"));
		assertTrue(res[1].equals("val2'"));

		res = splitSlotValues("val1,val2", false, false);
		assertTrue(res[0].equals("val1,val2"));


	}

	@Test
	public void testValidateIsNumber() {
		SlotMetadata slotMetadata = new SlotMetadata();
		slotMetadata.setName("testSlotMetadata");
		List<Notification> diagnostic = new ArrayList<Notification>();		

		slotMetadata.setIsNumber(true);
		validateIsNumber("location", "123", slotMetadata, diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Note);

		diagnostic = new ArrayList<Notification>();		
		validateIsNumber("location", "'abc'", slotMetadata, diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Error);

		slotMetadata.setIsNumber(false);
		diagnostic = new ArrayList<Notification>();		
		validateIsNumber("location", "123", slotMetadata, diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Error);

		diagnostic = new ArrayList<Notification>();		
		validateIsNumber("location", "abc", slotMetadata, diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Error);

		diagnostic = new ArrayList<Notification>();		
		validateIsNumber("location", "'abc'", slotMetadata, diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Note);

	}

	@Test
	public void testValidateMultiple() {
		SlotMetadata slotMetadata = new SlotMetadata();
		slotMetadata.setName("testSlotMetadata");
		List<Notification> diagnostic = new ArrayList<Notification>();
		String value = "displayString";

		slotMetadata.setMultiple(true);
		validateMultiple("location", value, slotMetadata, diagnostic, true, "TF");
		assertTrue(diagnostic.get(0) instanceof Note);
		
		diagnostic = new ArrayList<Notification>();
		validateMultiple("location", value, slotMetadata, diagnostic, false, "TF");
		assertTrue(diagnostic.get(0) instanceof Error);
		
		slotMetadata.setMultiple(false);
		diagnostic = new ArrayList<Notification>();
		validateMultiple("location", value, slotMetadata, diagnostic, false, "TF");
		assertTrue(diagnostic.get(0) instanceof Note);
		
		diagnostic = new ArrayList<Notification>();
		validateMultiple("location", value, slotMetadata, diagnostic, true, "TF");
		assertTrue(diagnostic.get(0) instanceof Error);
	}

	@Test
	public void testDeleteParenthesis() {
		String res = deleteParenthesis("(stringWithParenthesis)", false, true);
		assertTrue(res.equals("(stringWithParenthesis)"));
		
		res = deleteParenthesis("(stringWithParenthesis)", true, true);
		assertTrue(res.equals("stringWithParenthesis"));

		res = deleteParenthesis("(stringWithParenthesis)", false, false);
		assertTrue(res.equals("(stringWithParenthesis)"));

		res = deleteParenthesis("(stringWithParenthesis)", true, false);
		assertTrue(res.equals("(stringWithParenthesis)"));	

	}

	@Test
	public void testDeleteQuotes() {

		String res = deleteQuotes("'stringWithQuotes'", false, true);
		assertTrue(res.equals("stringWithQuotes"));

		res = deleteQuotes("'stringWithQuotes'", true, true);
		assertTrue(res.equals("'stringWithQuotes'"));

		res = deleteQuotes("'stringWithQuotes'", false, false);
		assertTrue(res.equals("'stringWithQuotes'"));

		res = deleteQuotes("'stringWithQuotes'", true, false);
		assertTrue(res.equals("'stringWithQuotes'"));
	}

	@Test
	public void testValidateSlotValueFromValueSet() {
		
		AdhocQueryMetadata adhocQueryMetadataParent = new AdhocQueryMetadata();
		
		ClassificationMetaData classificationMetadataParent = new ClassificationMetaData();
		classificationMetadataParent.setValueset("1.3.6.1.4.1.12559.11.4.1.4");
		SlotMetadata slotMetadata = new SlotMetadata();
		slotMetadata.setValueset("falseVS");
		String valueTested = "2.16.840.1.113883.6.1";

		List<Notification> diagnostic = new ArrayList<Notification>();		
		validateSlotValueFromValueSet("location", valueTested, classificationMetadataParent, slotMetadata, "34133-9", diagnostic, "TF");
		assertAllDiagnostic(diagnostic,true);
		
		diagnostic = new ArrayList<Notification>();	
		validateSlotValueFromValueSet("location", valueTested, classificationMetadataParent, slotMetadata, "falseNodeRepresentation", diagnostic, "TF");
		assertAllDiagnostic(diagnostic,false);
		
		
		diagnostic = new ArrayList<Notification>();	
		slotMetadata.setValueset("1.3.6.1.4.1.12559.11.4.3.8");
		valueTested = "XX-Document^^1.3.6.1.4.1.21367.2017.3";
		validateSlotValueFromValueSet("location", valueTested, adhocQueryMetadataParent, slotMetadata, "", diagnostic, "TF");
		assertAllDiagnostic(diagnostic,true);
	}
	
	@Test
	public void testValidateSlotValueFromValueSet2() {
		
		AdhocQueryMetadata adhocQueryMetadataParent = new AdhocQueryMetadata();
		ArrayList<Notification> diagnostic = new ArrayList<Notification>();	
		
		SlotMetadata slotMetadata = new SlotMetadata();
		slotMetadata.setValueset("1.3.6.1.4.1.12559.11.4.3.12");
		String valueTested = "urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248";

		validateSlotValueFromValueSet("location", valueTested, adhocQueryMetadataParent, slotMetadata, "", diagnostic, "TF");
		assertAllDiagnostic(diagnostic,false);
		
		diagnostic = new ArrayList<Notification>();	
		slotMetadata.setIsCodeOnly(true);
		validateSlotValueFromValueSet("location", valueTested, adhocQueryMetadataParent, slotMetadata, "", diagnostic, "TF");
		assertAllDiagnostic(diagnostic,true);
	}

	private void assertAllDiagnostic(List<Notification> diagnostic, boolean isTrue) {
		if(isTrue){
			for (Notification notification : diagnostic){
				assertTrue(notification instanceof Note);
			}
		}else{
			for (Notification notification : diagnostic){
				if(notification instanceof Error){
					assertTrue(true);
				}
			}
		}
	}

	@Test
	public void testGetConceptsListFromValueSetList() {
		
		String[] valueSetList = {"1.3.6.1.4.1.12559.11.4.3.2"};
		List<Concept> res = getConceptsListFromValueSetList(valueSetList);
		assertTrue(res.size() == 14);

		String[] valueSetListFalse = {"falseValueSet"};
		res = getConceptsListFromValueSetList(valueSetListFalse);
		assertTrue(res.size() == 0);
		
		String[] valueSetListMultiple = {"1.3.6.1.4.1.12559.11.4.3.2","1.3.6.1.4.1.12559.11.4.1.4"};
		res = getConceptsListFromValueSetList(valueSetListMultiple);
		assertTrue(res.size() == 20);
		
	}

	@Test
	public void testGetValueSetsFromValueSetRaw() {

		String valueSetRaw = "1.2.3";
		String[] res = getValueSetsFromValueSetRaw(valueSetRaw);
		assertTrue(res[0].equals("1.2.3"));
		
		valueSetRaw = "1.2.3,4.5.6";
		res = getValueSetsFromValueSetRaw(valueSetRaw);
		assertTrue(res[0].equals("1.2.3"));
		assertTrue(res[1].equals("4.5.6"));
		
	}

	@Test
	public void testGetValueSetRaw() {

		ClassificationMetaData classificationMetadataParent = new ClassificationMetaData();
		classificationMetadataParent.setValueset("1.2.3");
		SlotMetadata slotMetadata = new SlotMetadata();
		slotMetadata.setValueset("4.5.6");
		String res = getValueSetRaw(classificationMetadataParent, slotMetadata);
		assertTrue(res.equals("1.2.3")); // If slot parent = classification, the valueset must be taken from the classificationMetadata
		
		AdhocQueryMetadata adhocQueryMetadataParent = new AdhocQueryMetadata();
		res = getValueSetRaw(adhocQueryMetadataParent, slotMetadata);
		assertTrue(res.equals("4.5.6")); // Else, directly from the slotMetadata

		
	}

}
