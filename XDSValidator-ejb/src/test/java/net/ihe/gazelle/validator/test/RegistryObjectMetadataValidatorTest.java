package net.ihe.gazelle.validator.test;

import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.metadata.model.ExtrinsicObjectMetadata;
import net.ihe.gazelle.utils.FileReadWrite;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validator.RegistryObjectMetadataValidator;

import org.junit.Test;

public class RegistryObjectMetadataValidatorTest extends RegistryObjectMetadataValidator{

	@Test
	public void testValidateCardinalityMin() {
		List<Notification> diagnostic = new ArrayList<Notification>();		
		validateCardinalityMin("location", "Name", 1, 0, diagnostic, "TF"); // Check that 0 <= 1. Must return a Note.
		assertTrue(diagnostic.get(0) instanceof Note);
		
		diagnostic = new ArrayList<Notification>();		
		validateCardinalityMin("location", "Name", 2, 2, diagnostic, "TF"); // Check that 2 <= 2. Must return a Note.
		assertTrue(diagnostic.get(0) instanceof Note);

		diagnostic = new ArrayList<Notification>();		
		validateCardinalityMin("location", "Name", 2, 0, diagnostic, "TF"); // Check that 0 <= 2. Must return a Note.
		assertTrue(diagnostic.get(0) instanceof Note);

		diagnostic = new ArrayList<Notification>();		
		validateCardinalityMin("location", "Name", 0, 1, diagnostic, "TF"); // Check that 1 <= 0. Must return an Error.
		assertTrue(diagnostic.get(0) instanceof Error);
	}

	@Test
	public void testValidateCardinalityMax() {

		List<Notification> diagnostic = new ArrayList<Notification>();		
		validateCardinalityMax("location", "Name", 1, 2, diagnostic, "TF"); // Check that 2 >= 1. Must return a Note.
		assertTrue(diagnostic.get(0) instanceof Note);

		diagnostic = new ArrayList<Notification>();		
		validateCardinalityMax("location", "Name", 2, 2, diagnostic, "TF"); // Check that 2 >= 2. Must return a Note.
		assertTrue(diagnostic.get(0) instanceof Note);

		diagnostic = new ArrayList<Notification>();		
		validateCardinalityMax("location", "Name", 2, -1, diagnostic, "TF"); // Check that * >= 2. Must return a Note.
		assertTrue(diagnostic.get(0) instanceof Note);

		diagnostic = new ArrayList<Notification>();		
		validateCardinalityMax("location", "Name", 2, 1, diagnostic, "TF"); // Check that 1 >= 2. Must return an Error.
		assertTrue(diagnostic.get(0) instanceof Error);
	}

	@Test
	public void testValidateExternalIdentifierCardinality() throws JAXBException, IOException {
		
		String document = FileReadWrite.readDoc("src/test/resources/extrinsicObject.xml");
		ExtrinsicObjectMetadata eimd = (ExtrinsicObjectMetadata) ValidatorTestUtils.load(new FileInputStream("src/test/resources/extrinsicObjectEpsosMetadata.xml"));
		List<Notification> diagnostic = new ArrayList<Notification>();		
		validateExternalIdentifierCardinality("location", document, eimd, diagnostic, "TF");
		for (Notification notification : diagnostic){
			assertTrue(notification instanceof Note); // Card Ok
		}
		
	}

	@Test
	public void testValidateClassificationCardinality()  throws JAXBException, IOException {
		String document = FileReadWrite.readDoc("src/test/resources/extrinsicObject.xml");
		ExtrinsicObjectMetadata eimd = (ExtrinsicObjectMetadata) ValidatorTestUtils.load(new FileInputStream("src/test/resources/extrinsicObjectEpsosMetadata.xml"));
		List<Notification> diagnostic = new ArrayList<Notification>();		
		validateClassificationCardinality("location", document, eimd, diagnostic, "TF");
		for (Notification notification : diagnostic){
			assertTrue(notification instanceof Note); // Card Ok
		}
	}

	@Test
	public void testValidateSlotCardinality() throws JAXBException, IOException{
		String document = FileReadWrite.readDoc("src/test/resources/extrinsicObject.xml");
		ExtrinsicObjectMetadata eimd = (ExtrinsicObjectMetadata) ValidatorTestUtils.load(new FileInputStream("src/test/resources/extrinsicObjectEpsosMetadata.xml"));
		List<Notification> diagnostic = new ArrayList<Notification>();		
		validateSlotCardinality("location", document, eimd, diagnostic, "TF");
		for (Notification notification : diagnostic){
			assertTrue(notification instanceof Note); // Card Ok
		}
	}

	@Test
	public void testValidateExternalIdentifier() throws IOException, JAXBException{
		
		String document = FileReadWrite.readDoc("src/test/resources/epsos2_provide_data_service_request.xml");
		ExtrinsicObjectMetadata eimd = (ExtrinsicObjectMetadata) ValidatorTestUtils.load(new FileInputStream("src/test/resources/extrinsicObjectEpsosMetadata.xml"));
		List<Notification> diagnostic = new ArrayList<Notification>();		
		validateExternalIdentifier(document, "location", eimd, diagnostic, "TF");
		for (Notification notification : diagnostic){
			assertTrue(notification instanceof Note); // Each test needs to be ok
		}
	
	}

	@Test
	public void testValidateClassification() throws IOException, JAXBException{
		String document = FileReadWrite.readDoc("src/test/resources/epsos2_provide_data_service_request.xml");
		ExtrinsicObjectMetadata eimd = (ExtrinsicObjectMetadata) ValidatorTestUtils.load(new FileInputStream("src/test/resources/extrinsicObjectEpsosMetadata.xml"));
		List<Notification> diagnostic = new ArrayList<Notification>();		
		validateClassification(document, "location", eimd, diagnostic, "TF");
		for (Notification notification : diagnostic){
			assertTrue(notification instanceof Note); // Each test needs to be ok
		}
		System.out.println("");
	}

	@Test
	public void testValidateSlot() throws IOException, JAXBException{
		String document = FileReadWrite.readDoc("src/test/resources/epsos2_provide_data_service_request.xml");
		ExtrinsicObjectMetadata eimd = (ExtrinsicObjectMetadata) ValidatorTestUtils.load(new FileInputStream("src/test/resources/extrinsicObjectEpsosMetadata.xml"));
		List<Notification> diagnostic = new ArrayList<Notification>();		
		validateSlot(document, "location", eimd, null, diagnostic, "TF");
		for (Notification notification : diagnostic){
			assertTrue(notification instanceof Note); // Each test needs to be ok
		}
	}

	@Test
	public void testExtractListOfSlot() throws IOException {
		String document = FileReadWrite.readDoc("src/test/resources/epsos2_provide_data_service_request.xml");
		Map<String, Integer> test = extractListOfSlot(document, "creationTime"); // Valid slot name
		assertTrue(test.size() == 1);
		
		test = extractListOfSlot(document, "notValidSlotName");  // Not valid slot name
		assertTrue(test.size() == 0);
	}

	@Test
	public void testExtractListOfRegistryObject() throws IOException {
		String document = FileReadWrite.readDoc("src/test/resources/epsos2_provide_data_service_request.xml");
		Map<String, Integer> test = extractListOfRegistryObject(document, "Classification", "classificationScheme", "urn:uuid:aa543740-bdda-424e-8c96-df4873be8500"); // Valid attributeName, registry object type and attributeValue 
		assertTrue(test.size() == 1);
		
		test = extractListOfRegistryObject(document, "NotValidRegistryObject", "classificationScheme", "urn:uuid:aa543740-bdda-424e-8c96-df4873be8500"); //  Not valid registry object type
		assertTrue(test.size() == 0);
		
		test = extractListOfRegistryObject(document, "Classification", "classificationScheme", "notValidClassificationSchemeId"); // Not valid attribute value 
		assertTrue(test.size() == 0);
		
		test = extractListOfRegistryObject(document, "Classification", "notValidAttributeName", "urn:uuid:aa543740-bdda-424e-8c96-df4873be8500"); // Not valid attribute name
		assertTrue(test.size() == 0);
	}

}
