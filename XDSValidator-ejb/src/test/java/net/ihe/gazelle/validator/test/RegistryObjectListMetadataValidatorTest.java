package net.ihe.gazelle.validator.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import net.ihe.gazelle.metadata.model.RegistryObjectListMetadata;
import net.ihe.gazelle.utils.FileReadWrite;
import net.ihe.gazelle.utils.XpathUtils;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validator.RegistryObjectListMetadataValidator;

import org.junit.Test;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;



public class RegistryObjectListMetadataValidatorTest extends RegistryObjectListMetadataValidator{

	private static final String RIM_PREFIX = "urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0";

	@Test
	public void testValidateRegistryPackage() throws JAXBException, IOException {

		RegistryObjectListMetadata rolmd = (RegistryObjectListMetadata) ValidatorTestUtils.loadROL(new FileInputStream("src/test/resources/registryObjectListMetadata.xml")); 
		String document = FileReadWrite.readDoc("src/test/resources/epsos2_provide_data_service_request.xml");

		List<Notification> diagnostic = new ArrayList<Notification>();		
		validateRegistryPackage(document, "location", rolmd, diagnostic, "TF");
		for (Notification notification : diagnostic){
			assertTrue(notification instanceof Note); // Each test needs to be ok
		}

	}
	
	@Test
	public void testValidateExtrinsicObject() throws JAXBException, IOException {

		RegistryObjectListMetadata rolmd = (RegistryObjectListMetadata) ValidatorTestUtils.loadROL(new FileInputStream("src/test/resources/registryObjectListMetadata.xml")); 
		String document = FileReadWrite.readDoc("src/test/resources/epsos2_provide_data_service_request.xml");

		List<Notification> diagnostic = new ArrayList<Notification>();		
		validateExtrinsicObject(document, "location", rolmd, diagnostic, "TF");
		for (Notification notification : diagnostic){
			assertTrue(notification instanceof Note); // Each test needs to be ok
		}

	}
	
	@Test
	public void testValidateRegistryPackageCardinality() throws JAXBException, IOException {

		RegistryObjectListMetadata rolmd = (RegistryObjectListMetadata) ValidatorTestUtils.loadROL(new FileInputStream("src/test/resources/registryObjectListMetadata.xml")); 
		String document = FileReadWrite.readDoc("src/test/resources/registryPackage.xml");

		List<Notification> diagnostic = new ArrayList<Notification>();		
		validateRegistryPackageCardinality("location", document, rolmd, diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Note); // Card min OK
		assertTrue(diagnostic.get(1) instanceof Note); // Card max OK

	}

	@Test
	public void testValidateExtrinsicObjectCardinality() throws IOException, JAXBException {
		RegistryObjectListMetadata rolmd = (RegistryObjectListMetadata) ValidatorTestUtils.loadROL(new FileInputStream("src/test/resources/registryObjectListMetadata.xml")); 
		String document = FileReadWrite.readDoc("src/test/resources/xdsDocumentEntryEpsos.xml");

		List<Notification> diagnostic = new ArrayList<Notification>();		
		validateExtrinsicObjectCardinality("location", document, rolmd, diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Note); // Card min OK
		assertTrue(diagnostic.get(1) instanceof Note); // Card max OK

	}

	@Test
	public void testValidateClassifiedObjectReferencesRegistryPackage() throws IOException {

		String document = FileReadWrite.readDoc("src/test/resources/epsos2_provide_data_service_request.xml");
		List<Notification> diagnostic = new ArrayList<Notification>();		
		validateClassifiedObjectReferencesRegistryPackage(document, "location", diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Note); // The classifiedObject references a registry package ID = urn:uuid:5ea4b528-45de-4531-b557-317e892c02b0 


	}

	@Test
	public void testValidateRegistryPackageIsReferencedByClassificationObject() throws IOException {

		String document = FileReadWrite.readDoc("src/test/resources/epsos2_provide_data_service_request.xml");
		List<Notification> diagnostic = new ArrayList<Notification>();		
		validateRegistryPackageIsReferencedByClassificationObject(document, "location", diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Note); // The Registry Package id urn:uuid:5ea4b528-45de-4531-b557-317e892c02b0 is referenced by a valid classifiedObject

	}

	@Test
	public void testExtractListOfRegistryPackage() throws JAXBException, IOException {

		String document = FileReadWrite.readDoc("src/test/resources/epsos2_provide_data_service_request.xml");
		Map<String,Integer> res = extractListOfRegistryPackage(document, "urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd");
		assertTrue(res.size() == 1);

		res = extractListOfRegistryPackage(document, "falseuuid");
		assertTrue(res.size() == 0);

	}

	@Test
	public void testExtractRegistryPackageNodeFromId() throws ParserConfigurationException, SAXException, IOException {

		String document = FileReadWrite.readDoc("src/test/resources/epsos2_provide_data_service_request.xml");
		NodeList allRegistryPackagesNodes = XpathUtils.getNodeListFromString(document, "RegistryPackage", RIM_PREFIX);
		Entry<String,Integer> res = extractRegistryPackageNodeFromId("urn:uuid:5ea4b528-45de-4531-b557-317e892c02b0", allRegistryPackagesNodes);
		assertTrue(res != null); // urn:uuid:5ea4b528-45de-4531-b557-317e892c02b0 is a valid registryPackage Id in this document 
		res = extractRegistryPackageNodeFromId("falseRegistryPackageId", allRegistryPackagesNodes);
		assertTrue(res == null); // false id
	}

	@Test
	public void testGetAttributeValueListFromNodeList() throws ParserConfigurationException, SAXException, IOException{

		String document = FileReadWrite.readDoc("src/test/resources/epsos2_provide_data_service_request.xml");
		List<Node> classificationsNodes = getClassificationNodesWithClassificationNodeOfRegistryPackageUuid(document, "urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd");
		List<String> attributeValueList = getAttributeValueListFromNodeList(classificationsNodes, "id");
		assertTrue(attributeValueList.contains("urn:uuid:26cdf722-82fc-47d8-a2c6-86c007450de4"));
	}

	@Test
	public void testGetAttributeValueFromName() throws ParserConfigurationException, SAXException, IOException {

		String document = FileReadWrite.readDoc("src/test/resources/epsos2_provide_data_service_request.xml");
		Node node  = XpathUtils.getNodeFromString(document, "RegistryPackage", RIM_PREFIX);
		String attributeValue = getAttributeValueFromName(node, "objectType");
		assertTrue(attributeValue.equals("urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:RegistryPackage"));
	}

	@Test
	public void testGetNodesWithClassificationNode() throws IOException {

		String document = FileReadWrite.readDoc("src/test/resources/epsos2_provide_data_service_request.xml");
		List<Node> res = getNodesWithClassificationNode(document);
		assertTrue(res.size() == 1);

		document = FileReadWrite.readDoc("src/test/resources/classification.xml");
		res = getNodesWithClassificationNode(document);
		assertTrue(res.size() == 0);
	}

	@Test
	public void testGetClassificationNodesWithClassificationNodeOfRegistryPackageUuid() throws IOException {

		String document = FileReadWrite.readDoc("src/test/resources/epsos2_provide_data_service_request.xml");
		List<Node> res = getClassificationNodesWithClassificationNodeOfRegistryPackageUuid(document, "urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd");
		assertTrue(res.size() == 1);

		res = getClassificationNodesWithClassificationNodeOfRegistryPackageUuid(document, "falseUuid");
		assertTrue(res.size() == 0);
	}


	@Test
	public void testExtractListOfExtrinsicObject() throws IOException {

		String document = FileReadWrite.readDoc("src/test/resources/epsos2_provide_data_service_request.xml");
		Map<Node, Integer> test = extractListOfExtrinsicObject(document, "urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1"); // Only valid objectType of the document
		assertTrue(test.size() == 1);
		test = extractListOfExtrinsicObject(document, "falseObjectTypeId"); // Not valid objectType 
		assertTrue(test.size() == 0);
		test = extractListOfExtrinsicObject(document, "urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification"); // Not valid : it's an object type attribute of a classification, not an extrinsicObject
		assertTrue(test.size() == 0);

	}


	@Test
	public void testValidateCardinalityMin() {
		List<Notification> diagnostic = new ArrayList<Notification>();		
		validateCardinalityMin("location", "Name", 1, 0, diagnostic, "TF"); // Check that 0 <= 1. Must return a Note.
		assertTrue(diagnostic.get(0) instanceof Note);

		diagnostic = new ArrayList<Notification>();		
		validateCardinalityMin("location", "Name", 2, 2, diagnostic, "TF"); // Check that 2 <= 2. Must return a Note.
		assertTrue(diagnostic.get(0) instanceof Note);

		diagnostic = new ArrayList<Notification>();		
		validateCardinalityMin("location", "Name", 2, 0, diagnostic, "TF"); // Check that 0 <= 2. Must return a Note.
		assertTrue(diagnostic.get(0) instanceof Note);

		diagnostic = new ArrayList<Notification>();		
		validateCardinalityMin("location", "Name", 0, 1, diagnostic, "TF"); // Check that 1 <= 0. Must return an Error.
		assertTrue(diagnostic.get(0) instanceof Error);
	}

	@Test
	public void testValidateCardinalityMax() {

		List<Notification> diagnostic = new ArrayList<Notification>();		
		validateCardinalityMax("location", "Name", 1, 2, diagnostic, "TF"); // Check that 2 >= 1. Must return a Note.
		assertTrue(diagnostic.get(0) instanceof Note);

		diagnostic = new ArrayList<Notification>();		
		validateCardinalityMax("location", "Name", 2, 2, diagnostic, "TF"); // Check that 2 >= 2. Must return a Note.
		assertTrue(diagnostic.get(0) instanceof Note);

		diagnostic = new ArrayList<Notification>();		
		validateCardinalityMax("location", "Name", 2, -1, diagnostic, "TF"); // Check that * >= 2. Must return a Note.
		assertTrue(diagnostic.get(0) instanceof Note);

		diagnostic = new ArrayList<Notification>();		
		validateCardinalityMax("location", "Name", 2, 1, diagnostic, "TF"); // Check that 1 >= 2. Must return an Error.
		assertTrue(diagnostic.get(0) instanceof Error);

	}

	@Test
	public void testGetRegistryPackageIds() throws IOException {

		String document = FileReadWrite.readDoc("src/test/resources/epsos2_provide_data_service_request.xml");
		List<String> registryPackageIds = getRegistryPackageIds(document);
		assertTrue(registryPackageIds.contains("urn:uuid:5ea4b528-45de-4531-b557-317e892c02b0"));
		assertFalse(registryPackageIds.contains("falseUUid"));

	}

}
