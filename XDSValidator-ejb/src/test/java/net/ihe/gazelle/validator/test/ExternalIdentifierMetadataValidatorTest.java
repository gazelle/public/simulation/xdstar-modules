package net.ihe.gazelle.validator.test;

import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.edit.FileReadWrite;
import net.ihe.gazelle.metadata.model.ExternalIdentifierMetadata;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validator.ExternalIdentifierMetadataValidator;

import org.junit.Test;

public class ExternalIdentifierMetadataValidatorTest extends ExternalIdentifierMetadataValidator{

	@Test
	public void testValidateIdentificationSchemeValue() throws IOException, JAXBException {
		ExternalIdentifierMetadata eimd = (ExternalIdentifierMetadata) ValidatorTestUtils.load(new FileInputStream("src/test/resources/externalIdentifierMetadata.xml")); 
		String document = FileReadWrite.readDoc("src/test/resources/externalIdentifier.xml");
		
		List<Notification> diagnostic = new ArrayList<Notification>();
		this.validateIdentificationSchemeValue(document, "location", eimd, diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Note);
		
		diagnostic = new ArrayList<Notification>();
		eimd.setUuid(null);
		this.validateIdentificationSchemeValue(document, "location", eimd, diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Error);
		
		diagnostic = new ArrayList<Notification>();
		eimd.setUuid("falseuuid");
		this.validateIdentificationSchemeValue(document, "location", eimd, diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Error);
	}

}
