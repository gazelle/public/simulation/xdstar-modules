package net.ihe.gazelle.validator.test;

import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.edit.FileReadWrite;
import net.ihe.gazelle.metadata.model.ClassificationMetaData;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validator.ClassificationMetadataValidator;

import org.junit.Test;

public class ClassificationMetadataValidatorTest extends ClassificationMetadataValidator{

	@Test
	public void testValidateClassificationSchemeValue() throws JAXBException, IOException {
		ClassificationMetaData cmd = (ClassificationMetaData) ValidatorTestUtils.load(new FileInputStream("src/test/resources/classificationMetadata.xml")); 
		String document = FileReadWrite.readDoc("src/test/resources/classification.xml");
		
		List<Notification> diagnostic = new ArrayList<Notification>();
		this.validateClassificationSchemeValue(document, "location", cmd, diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Note);
		
		diagnostic = new ArrayList<Notification>();
		cmd.setUuid(null);
		this.validateClassificationSchemeValue(document, "location", cmd, diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Error);
		
		diagnostic = new ArrayList<Notification>();
		cmd.setUuid("falseuuid");
		this.validateClassificationSchemeValue(document, "location", cmd, diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Error);

	}
	
	@Test
	public void testGetNodeRepresentation() throws JAXBException, IOException {
		String document = FileReadWrite.readDoc("src/test/resources/classification.xml");
		String documentNoNR = FileReadWrite.readDoc("src/test/resources/classificationNoNR.xml");
		
		String nodeRepresentation = this.getNodeRepresentation(document);
		assertTrue(nodeRepresentation.equals("N"));
		
		String noNodeRepresentation = this.getNodeRepresentation(documentNoNR);
		assertTrue(noNodeRepresentation.equals(""));
	}

}
