package net.ihe.gazelle.validator.test;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import net.ihe.gazelle.metadata.model.RegistryObjectListMetadata;
import net.ihe.gazelle.metadata.model.RegistryObjectMetadata;

public class ValidatorTestUtils {

	public static RegistryObjectMetadata load(InputStream is) throws JAXBException, UnsupportedEncodingException {
		JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.metadata.model");
		Unmarshaller u = jc.createUnmarshaller();
		Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8);
		RegistryObjectMetadata mimi = (RegistryObjectMetadata) u.unmarshal(reader);
		return mimi;
	}

	public static RegistryObjectListMetadata loadROL(FileInputStream fileInputStream) throws JAXBException, UnsupportedEncodingException {
		JAXBContext jc = JAXBContext.newInstance(RegistryObjectListMetadata.class);
		Unmarshaller u = jc.createUnmarshaller();
		Reader reader = new InputStreamReader(fileInputStream, StandardCharsets.UTF_8);
		RegistryObjectListMetadata mimi = (RegistryObjectListMetadata) u.unmarshal(reader);
		return mimi;
	}
	
}
