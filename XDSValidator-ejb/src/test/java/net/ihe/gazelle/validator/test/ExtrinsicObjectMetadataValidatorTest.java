package net.ihe.gazelle.validator.test;

import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.edit.FileReadWrite;
import net.ihe.gazelle.metadata.model.ExtrinsicObjectMetadata;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validator.ExtrinsicObjectMetadataValidator;

import org.junit.Test;

public class ExtrinsicObjectMetadataValidatorTest extends ExtrinsicObjectMetadataValidator{

	@Test
	public void testValidateObjectTypeValue() throws IOException, JAXBException {
		ExtrinsicObjectMetadata eomd = (ExtrinsicObjectMetadata) ValidatorTestUtils.load(new FileInputStream("src/test/resources/extrinsicObjectMetadata.xml")); 
		String document = FileReadWrite.readDoc("src/test/resources/extrinsicObject.xml");
		
		List<Notification> diagnostic = new ArrayList<Notification>();
		this.validateObjectTypeValue(document, "location", eomd, diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Note);
		
		diagnostic = new ArrayList<Notification>();
		eomd.setUuid(null);
		this.validateObjectTypeValue(document, "location", eomd, diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Error);
		
		diagnostic = new ArrayList<Notification>();
		eomd.setUuid("falseuuid");
		this.validateObjectTypeValue(document, "location", eomd, diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Error);
	}

}
