package net.ihe.gazelle.validator.test;

import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.edit.FileReadWrite;
import net.ihe.gazelle.metadata.model.AdhocQueryMetadata;
import net.ihe.gazelle.query.ReturnTypeType;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validator.AdhocQueryMetadataValidator;

import org.junit.Test;

public class AdhocQueryMetadataValidatorTest extends AdhocQueryMetadataValidator {



	@Test
	public void testValidateReturnTypes() throws JAXBException, IOException {
		AdhocQueryMetadata aqmd = (AdhocQueryMetadata) ValidatorTestUtils.load(new FileInputStream("src/test/resources/aqm.xml")); 
		String document = FileReadWrite.readDoc("src/test/resources/iti18.xml");
		
		List<Notification> diagnostic = new ArrayList<Notification>();
		this.validateReturnTypes(document, "location", aqmd, diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Note);
		
		diagnostic = new ArrayList<Notification>();
		aqmd.setReturnTypes(null);
		this.validateReturnTypes(document, "location", aqmd, diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Error);
		
		diagnostic = new ArrayList<Notification>();
		aqmd.setReturnTypes(Arrays.asList(new ReturnTypeType[]{ReturnTypeType.OBJECTREF}));
		this.validateReturnTypes(document, "location", aqmd, diagnostic, "TF");
		assertTrue(diagnostic.get(0) instanceof Error);
	}

	@Test
	public void testGetReturnTypeValuesAllowed() {
		List<ReturnTypeType> lrtt = new ArrayList<ReturnTypeType>();
		lrtt.add(ReturnTypeType.LEAFCLASS);
		assertTrue(this.getReturnTypeValuesAllowed(lrtt).contains("LeafClass"));
	}



}
