package net.ihe.gazelle.simulator.ws;

import java.io.ByteArrayOutputStream;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import net.ihe.gazelle.xdstar.comon.util.XDSNamespacePrefixMapper;
import net.ihe.gazelle.xdstar.xcpd.action.DateType;

import org.hl7.v3.AD;
import org.hl7.v3.ActClassControlAct;
import org.hl7.v3.AdxpCity;
import org.hl7.v3.AdxpCountry;
import org.hl7.v3.AdxpPostalCode;
import org.hl7.v3.AdxpStreetName;
import org.hl7.v3.CD;
import org.hl7.v3.CE;
import org.hl7.v3.COCTMT090100UV01AssignedPerson;
import org.hl7.v3.CS;
import org.hl7.v3.CommunicationFunctionType;
import org.hl7.v3.EN;
import org.hl7.v3.EnFamily;
import org.hl7.v3.EnGiven;
import org.hl7.v3.EntityClassDevice;
import org.hl7.v3.II;
import org.hl7.v3.IVLTS;
import org.hl7.v3.IVXBTS;
import org.hl7.v3.MCCIMT000100UV01Agent;
import org.hl7.v3.MCCIMT000100UV01Device;
import org.hl7.v3.MCCIMT000100UV01Organization;
import org.hl7.v3.MCCIMT000100UV01Receiver;
import org.hl7.v3.MCCIMT000100UV01Sender;
import org.hl7.v3.ObjectFactory;
import org.hl7.v3.PN;
import org.hl7.v3.PRPAIN201305UV02;
import org.hl7.v3.PRPAIN201305UV02QUQIMT021001UV01ControlActProcess;
import org.hl7.v3.PRPAMT201306UV02LivingSubjectAdministrativeGender;
import org.hl7.v3.PRPAMT201306UV02LivingSubjectBirthTime;
import org.hl7.v3.PRPAMT201306UV02LivingSubjectId;
import org.hl7.v3.PRPAMT201306UV02LivingSubjectName;
import org.hl7.v3.PRPAMT201306UV02MatchCriterionList;
import org.hl7.v3.PRPAMT201306UV02MothersMaidenName;
import org.hl7.v3.PRPAMT201306UV02ParameterList;
import org.hl7.v3.PRPAMT201306UV02PatientAddress;
import org.hl7.v3.PRPAMT201306UV02PatientTelecom;
import org.hl7.v3.PRPAMT201306UV02PrincipalCareProviderId;
import org.hl7.v3.PRPAMT201306UV02QueryByParameter;
import org.hl7.v3.QUQIMT021001UV01AuthorOrPerformer;
import org.hl7.v3.ST;
import org.hl7.v3.TEL;
import org.hl7.v3.TS;
import org.hl7.v3.XActMoodIntentEvent;
import org.hl7.v3.XParticipationAuthorPerformer;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

/**
 * 
 * @author abderrazek boufahja 
 *
 */
public class MessageGenerator {
    
    public MessageGenerator(){
    }
    
    private String dateGenerator(){
        DateTimeFormatter FMT = ISODateTimeFormat.basicDateTimeNoMillis();
        String t = (FMT.print(new DateTime()));
        String t1 = t.substring(0, 8);
        String t2 = t.substring(9, 15);
        return t1 + t2;
    }
    
    public String createMessageToSend(String family, String given, 
    		DateType dateType,
    		String bdValue, String bdhigh, String bdlow, String bdcenter,
            String root, String extension, String gender,
            String street, String city, String country, 
            String zipcode, String maidenName,
            String senderHomeCommunityId, String receiverHomeCommunityId,
            String telecom, String cproot, String cpextension){
        String res = new String();
        
        family = this.stringOrEmpty(family);
        given = this.stringOrEmpty(given);
        
        bdValue = this.stringOrEmpty(bdValue);
        bdhigh =  this.stringOrEmpty(bdhigh);
        bdlow = this.stringOrEmpty(bdlow);
        bdcenter = this.stringOrEmpty(bdcenter);
        
        root = this.stringOrEmpty(root);
        extension = this.stringOrEmpty(extension);
        gender = this.stringOrEmpty(gender);
        street = this.stringOrEmpty(street);
        city = this.stringOrEmpty(city);
        country = this.stringOrEmpty(country);
        zipcode = this.stringOrEmpty(zipcode);
        maidenName = this.stringOrEmpty(maidenName);
        senderHomeCommunityId = this.stringOrEmpty(senderHomeCommunityId);
        receiverHomeCommunityId = this.stringOrEmpty(receiverHomeCommunityId);
        telecom = this.stringOrEmpty(telecom);
        cproot = this.stringOrEmpty(cproot);
        cpextension = this.stringOrEmpty(cpextension);
        
        ObjectFactory of = new ObjectFactory();
        
        PRPAIN201305UV02 prpa = new PRPAIN201305UV02();
        prpa.setITSVersion("XML_1.0");
        prpa.setId(new II());
        prpa.getId().setExtension("1263507841148");
        prpa.getId().setRoot("2306fc92-7819-419b-a6a6-f30d7fbeb08f");
        prpa.setCreationTime(new TS());
        prpa.getCreationTime().setValue(this.dateGenerator());
        prpa.setVersionCode(new CS());
        prpa.getVersionCode().setCode("V3PR1");
        prpa.setInteractionId(new II());
        prpa.getInteractionId().setExtension("PRPA_IN201305UV02");
        prpa.getInteractionId().setRoot("2.16.840.1.113883.1.6");
        prpa.setProcessingCode(new CS());
        prpa.getProcessingCode().setCode("P");
        prpa.setProcessingModeCode(new CS());
        prpa.getProcessingModeCode().setCode("T");
        prpa.setAcceptAckCode(new CS());
        prpa.getAcceptAckCode().setCode("AL");
        
        prpa.getReceiver().add(new MCCIMT000100UV01Receiver());
        prpa.getReceiver().get(0).setTypeCode(CommunicationFunctionType.RCV);
        prpa.getReceiver().get(0).setDevice(new MCCIMT000100UV01Device());
        prpa.getReceiver().get(0).getDevice().setClassCode(EntityClassDevice.DEV);
        prpa.getReceiver().get(0).getDevice().setDeterminerCode("INSTANCE");
        prpa.getReceiver().get(0).getDevice().getId().add(new II());
        prpa.getReceiver().get(0).getDevice().getId().get(0).setRoot(receiverHomeCommunityId);
        MCCIMT000100UV01Agent agent = new MCCIMT000100UV01Agent();
        agent.getClassCode().add("AGNT");
        MCCIMT000100UV01Organization org = new MCCIMT000100UV01Organization();
        org.setClassCode("ORG");
        org.setDeterminerCode("INSTANCE");
        org.getId().add(new II());
        org.getId().get(0).setRoot(receiverHomeCommunityId);
        
        JAXBElement<MCCIMT000100UV01Organization> orgJ = of.createMCCIMT000100UV01AgentRepresentedOrganization(org);
        agent.setRepresentedOrganization(orgJ);
        JAXBElement<MCCIMT000100UV01Agent> agentJ = of.createMCCIMT000100UV01DeviceAsAgent(agent); 
        prpa.getReceiver().get(0).getDevice().setAsAgent(agentJ);
        
        prpa.setSender(new MCCIMT000100UV01Sender());
        prpa.getSender().setTypeCode(CommunicationFunctionType.SND);
        prpa.getSender().setDevice(new MCCIMT000100UV01Device());
        prpa.getSender().getDevice().setClassCode(EntityClassDevice.DEV);
        prpa.getSender().getDevice().setDeterminerCode("INSTANCE");
        prpa.getSender().getDevice().getId().add(new II());
        prpa.getSender().getDevice().getId().get(0).setRoot(senderHomeCommunityId);
        MCCIMT000100UV01Agent agent2 = new MCCIMT000100UV01Agent();
        agent2.getClassCode().add("AGNT");
        MCCIMT000100UV01Organization org2 = new MCCIMT000100UV01Organization();
        org2.setClassCode("ORG");
        org2.setDeterminerCode("INSTANCE");
        org2.getId().add(new II());
        org2.getId().get(0).setRoot(senderHomeCommunityId);
        JAXBElement<MCCIMT000100UV01Organization> org2J = of.createMCCIMT000100UV01AgentRepresentedOrganization(org2);
        agent2.setRepresentedOrganization(org2J);
        JAXBElement<MCCIMT000100UV01Agent> agent2J = of.createMCCIMT000100UV01DeviceAsAgent(agent2);
        prpa.getSender().getDevice().setAsAgent(agent2J);
        
        prpa.setControlActProcess(new PRPAIN201305UV02QUQIMT021001UV01ControlActProcess());
        prpa.getControlActProcess().setClassCode(ActClassControlAct.CACT);
        prpa.getControlActProcess().setMoodCode(XActMoodIntentEvent.EVN);
        prpa.getControlActProcess().setCode(new CD());
        prpa.getControlActProcess().getCode().setCode("PRPA_TE201305UV02");
        prpa.getControlActProcess().getCode().setCodeSystemName("2.16.840.1.113883.1.6");
        
        prpa.getControlActProcess().getAuthorOrPerformer().add(new QUQIMT021001UV01AuthorOrPerformer());
        prpa.getControlActProcess().getAuthorOrPerformer().get(0).setTypeCode(XParticipationAuthorPerformer.AUT);
        COCTMT090100UV01AssignedPerson ass = new COCTMT090100UV01AssignedPerson();
        ass.setClassCode("ASSIGNED");
        JAXBElement<COCTMT090100UV01AssignedPerson> assJ = of.createQUQIMT021001UV01AuthorOrPerformerAssignedPerson(ass);
        prpa.getControlActProcess().getAuthorOrPerformer().get(0).setAssignedPerson(assJ);
        
        PRPAMT201306UV02QueryByParameter query = new PRPAMT201306UV02QueryByParameter();
        prpa.getControlActProcess().setQueryByParameter(of.createPRPAIN201305UV02QUQIMT021001UV01ControlActProcessQueryByParameter(query));
        query.setQueryId(new II());
        query.getQueryId().setRoot(UUID.randomUUID().toString());
        query.setStatusCode(new CS());
        query.getStatusCode().setCode("new");
        query.setResponseModalityCode(new CS());
        query.getResponseModalityCode().setCode("R");
        query.setResponsePriorityCode(new CS());
        query.getResponsePriorityCode().setCode("I");
        query.setMatchCriterionList(of.createPRPAMT201306UV02QueryByParameterMatchCriterionList(new PRPAMT201306UV02MatchCriterionList()));
        
        query.setParameterList(new PRPAMT201306UV02ParameterList());
        
        if (gender.length()>0){
        	query.getParameterList().getLivingSubjectAdministrativeGender().add(new PRPAMT201306UV02LivingSubjectAdministrativeGender());
        	query.getParameterList().getLivingSubjectAdministrativeGender().get(0).getValue().add(new CE());
        	query.getParameterList().getLivingSubjectAdministrativeGender().get(0).getValue().get(0).setCode(gender);
        	query.getParameterList().getLivingSubjectAdministrativeGender().get(0).setSemanticsText(new ST());
        }
        
        if (dateType != null){
        	switch (dateType) {
			case EXACT_DATES:
				if (bdValue.length()>0){
		        	query.getParameterList().getLivingSubjectBirthTime().add(new PRPAMT201306UV02LivingSubjectBirthTime());
		        	query.getParameterList().getLivingSubjectBirthTime().get(0).getValue().add(new IVLTS());
		        	query.getParameterList().getLivingSubjectBirthTime().get(0).getValue().get(0).setValue(bdValue);
		        	query.getParameterList().getLivingSubjectBirthTime().get(0).setSemanticsText(new ST());
		        }
				break;
			
			case APPROXIMATE_DATES:
				if (bdcenter.length()>0){
		        	query.getParameterList().getLivingSubjectBirthTime().add(new PRPAMT201306UV02LivingSubjectBirthTime());
		        	query.getParameterList().getLivingSubjectBirthTime().get(0).getValue().add(new IVLTS());
		        	TS value = new TS();
		        	value.setValue(bdcenter);
		        	query.getParameterList().getLivingSubjectBirthTime().get(0).getValue().get(0).getRest().add(of.createIVLTSCenter(value));
		        	query.getParameterList().getLivingSubjectBirthTime().get(0).setSemanticsText(new ST());
		        }
				break;
			
			case RANGE_DATES:
				if (bdhigh.length()>0 || bdlow.length()>0){
		        	query.getParameterList().getLivingSubjectBirthTime().add(new PRPAMT201306UV02LivingSubjectBirthTime());
		        	query.getParameterList().getLivingSubjectBirthTime().get(0).getValue().add(new IVLTS());
		        	if (bdlow.length()>0){
		        		IVXBTS low = new IVXBTS();
			        	low.setValue(bdlow);
			        	query.getParameterList().getLivingSubjectBirthTime().get(0).getValue().get(0).getRest().add(of.createIVLTSLow(low));
		        	}
		        	if (bdhigh.length()>0){
		        		IVXBTS high = new IVXBTS();
			        	high.setValue(bdhigh);
			        	query.getParameterList().getLivingSubjectBirthTime().get(0).getValue().get(0).getRest().add(of.createIVLTSHigh(high));
		        	}
		        	query.getParameterList().getLivingSubjectBirthTime().get(0).setSemanticsText(new ST());
		        }
				break;

			default:
				break;
			}
        }
        
        
        
        if (root.length()>0 || extension.length()>0){
        	query.getParameterList().getLivingSubjectId().add(new PRPAMT201306UV02LivingSubjectId());
        	query.getParameterList().getLivingSubjectId().get(0).getValue().add(new II());
        	if (root.length()>0) query.getParameterList().getLivingSubjectId().get(0).getValue().get(0).setRoot(root);
        	if (extension.length()>0) query.getParameterList().getLivingSubjectId().get(0).getValue().get(0).setExtension(extension);
        	query.getParameterList().getLivingSubjectId().get(0).setSemanticsText(new ST());
        }
        
        if ((family.length()>0) || (given.length()>0)){
        	query.getParameterList().getLivingSubjectName().add(new PRPAMT201306UV02LivingSubjectName());
        	query.getParameterList().getLivingSubjectName().get(0).getValue().add(new EN());
        	if (family.length()>0){
        		EnFamily fam = new EnFamily();
        		fam.getContent().add(family);
        		JAXBElement<EnFamily> famJ = of.createENFamily(fam);
        		query.getParameterList().getLivingSubjectName().get(0).getValue().get(0).getContent().add(famJ);
        	}
        	if (given.length()>0){
        		EnGiven giv = new EnGiven();
        		giv.getContent().add(given);
        		JAXBElement<EnGiven> givJ = of.createENGiven(giv);
        		query.getParameterList().getLivingSubjectName().get(0).getValue().get(0).getContent().add(givJ);
        	}
        	query.getParameterList().getLivingSubjectName().get(0).setSemanticsText(new ST());
        	query.getParameterList().getLivingSubjectName().get(0).getSemanticsText().getContent().add("LivingSubject.name");
        }
        
        if (maidenName.length()>0){
        	query.getParameterList().getMothersMaidenName().add(new PRPAMT201306UV02MothersMaidenName());
        	query.getParameterList().getMothersMaidenName().get(0).getValue().add(new PN());
        	query.getParameterList().getMothersMaidenName().get(0).getValue().get(0).getContent().add(maidenName);
        	query.getParameterList().getMothersMaidenName().get(0).setSemanticsText(new ST());
        }
        
        if ((street.length()>0) || (city.length()>0) || (country.length()>0) || (zipcode.length()>0)){
        	query.getParameterList().getPatientAddress().add(new PRPAMT201306UV02PatientAddress());
        	query.getParameterList().getPatientAddress().get(0).getValue().add(new AD());
        	if (street.length()>0){
        		AdxpStreetName sn = new AdxpStreetName();
        		sn.getContent().add(street);
        		query.getParameterList().getPatientAddress().get(0).getValue().get(0).getContent().add(of.createADStreetName(sn));
        	}
        	if (city.length()>0){
        		AdxpCity sn = new AdxpCity();
        		sn.getContent().add(city);
        		query.getParameterList().getPatientAddress().get(0).getValue().get(0).getContent().add(of.createADCity(sn));
        	}
        	if (country.length()>0){
        		AdxpCountry sn = new AdxpCountry();
        		sn.getContent().add(country);
        		query.getParameterList().getPatientAddress().get(0).getValue().get(0).getContent().add(of.createADCountry(sn));
        	}
        	if (zipcode.length()>0){
        		AdxpPostalCode sn = new AdxpPostalCode();
        		sn.getContent().add(zipcode);
        		query.getParameterList().getPatientAddress().get(0).getValue().get(0).getContent().add(of.createADPostalCode(sn));
        	}
        	query.getParameterList().getPatientAddress().get(0).setSemanticsText(new ST());
        }
        if (telecom.length()>0){
	        query.getParameterList().getPatientTelecom().add(new PRPAMT201306UV02PatientTelecom());
	        query.getParameterList().getPatientTelecom().get(0).getValue().add(new TEL());
	        query.getParameterList().getPatientTelecom().get(0).getValue().get(0).setValue(telecom);
        }
        if (cproot.length()>0 || cpextension.length()>0){
	        query.getParameterList().getPrincipalCareProviderId().add(new PRPAMT201306UV02PrincipalCareProviderId());
	        query.getParameterList().getPrincipalCareProviderId().get(0).getValue().add(new II());
	        if (cproot.length()>0) query.getParameterList().getPrincipalCareProviderId().get(0).getValue().get(0).setRoot(cproot);
	        if (cpextension.length()>0) query.getParameterList().getPrincipalCareProviderId().get(0).getValue().get(0).setExtension(cpextension);
        }
        try {
			JAXBContext jc = JAXBContext.newInstance(PRPAIN201305UV02.class);
			Marshaller mar = jc.createMarshaller();
			mar.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			mar.setProperty("com.sun.xml.bind.namespacePrefixMapper", new XDSNamespacePrefixMapper());
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			mar.marshal(prpa, baos);
			res = baos.toString();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
        return res;
    }
    
    private String stringOrEmpty(String ss){
    	if (ss == null) return "";
    	return ss;
    }

}
