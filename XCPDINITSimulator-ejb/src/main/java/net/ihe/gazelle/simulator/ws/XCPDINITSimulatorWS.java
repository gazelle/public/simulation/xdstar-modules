package net.ihe.gazelle.simulator.ws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.xml.soap.SOAPException;

import net.ihe.gazelle.simulator.common.action.AbstractSimulatorManager;
import net.ihe.gazelle.simulator.common.action.ResultSendMessage;
import net.ihe.gazelle.simulator.common.action.SimulatorManagerRemote;
import net.ihe.gazelle.simulator.common.configuration.HL7.model.HL7V3ResponderConfiguration;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.model.ConfigurationForWS;
import net.ihe.gazelle.simulator.common.model.ContextualInformationInstance;
import net.ihe.gazelle.simulator.common.model.Message;
import net.ihe.gazelle.simulator.common.model.OIDConfiguration;
import net.ihe.gazelle.simulator.common.model.TestInstance;
import net.ihe.gazelle.simulator.common.model.TestInstanceParticipants;
import net.ihe.gazelle.simulator.common.model.TestStepsInstance;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.xdstar.xcpd.action.DateType;
import net.ihe.xcpd.init.action.HL7V3Responder;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Stateless
@Name("XCPDINITSimulatorWS")
@WebService(name ="GazelleSimulatorManagerWS", serviceName = "GazelleSimulatorManagerWSService",portName="GazelleSimulatorManagerWSPort") 
public class XCPDINITSimulatorWS extends AbstractSimulatorManager implements SimulatorManagerRemote,Serializable{

    static final String FAMILY = "family";
    
    static final String GIVEN = "given";
    
    static final String BIRTH_DATE = "birthdate";
    
    static final String ROOT = "root";

    static final String EXTENSION = "extension";
    
    static final String STREET = "street";
    
    static final String GENDER = "gender";
    
    static final String CITY = "city";
    
    static final String COUNTRY = "country";
    
    static final String POSTAL_CODE = "postalcode";
    
    static final String MAIDEN_NAME = "maidenname";
    
    

    private static Logger log = LoggerFactory.getLogger(XCPDINITSimulatorWS.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@WebMethod
	public boolean startTestInstance(@WebParam(name="testInstanceId")String testInstanceId){
		log.info("Simulator:::startTestInstance");
		try{
			return super.startTestInstance(testInstanceId);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	@WebMethod
	public boolean stopTestInstance(@WebParam(name="testInstanceId")String testInstanceId) {
		try {
            return super.stopTestInstance(testInstanceId);
        } catch (SOAPException e) {
            e.printStackTrace();
            return false;
        }
	}

	@WebMethod
	public boolean deleteTestInstance(@WebParam(name="testInstanceId")String testInstanceId) {
		// TODO Auto-generated method stub
		return false;
	}


	@WebMethod
	public String confirmMessageReception(@WebParam(name="testInstanceId")String testInstanceId,
			@WebParam(name="testInstanceParticipantsId")String testInstanceParticipantsId,@WebParam(name="transaction")Transaction transaction,
			@WebParam(name="messageType")String messageType){
		log.info("Simulator::confirmMessageReception()");
		TestInstanceParticipants inTestInstanceParticipantReceiver = TestInstanceParticipants.getTestInstanceParticipants(testInstanceParticipantsId);
        TestInstance inTestInstance = TestInstance.getTestInstanceByServerTestInstanceId(testInstanceId);
        List<Message> lm = Message.getMessageFiltred(messageType, inTestInstanceParticipantReceiver, null, transaction, inTestInstance);
        if (lm.size()>0){
            return "true";
        }
        else{
            return "false";
        }
	}


	@WebMethod
	public ResultSendMessage sendMessage(@WebParam(name="testInstanceId")String testInstanceId,
			@WebParam(name="testInstanceParticipantsId")String testInstanceParticipantsId,@WebParam(name="transaction")Transaction transaction,
			@WebParam(name="messageType")String messageType,
			@WebParam(name = "responderConfiguration") ConfigurationForWS responderConfiguration,
			@WebParam(name="listContextualInformationInstanceInput")List<ContextualInformationInstance> listContextualInformationInstanceInput,
			@WebParam(name="listContextualInformationInstanceOutput")List<ContextualInformationInstance> listContextualInformationInstanceOutput) throws SOAPException {
		log.info("Simu::sendMessage()");
		log.info("testInstanceId=="+testInstanceId);
		log.info("testInstanceParticipantsId=="+testInstanceParticipantsId);
		log.info("transaction=="+transaction.getKeyword());
		
		ResultSendMessage rsm = null;
		
		this.saveReferencesForTestStepsInstance(testInstanceId, testInstanceParticipantsId, responderConfiguration);
		
		TestInstanceParticipants testInstanceParticipants=TestInstanceParticipants.getTestInstanceParticipants(testInstanceParticipantsId);
        
		if (testInstanceParticipants != null){
    		String wsurl = null;
    		if (responderConfiguration != null){
    		    wsurl = HL7V3ResponderConfiguration.createWSUrl(responderConfiguration.gethL7V3ResponderConfiguration());
    		}
    	    if (wsurl != null){
    	        String msgString = this.createMessageToSend(listContextualInformationInstanceInput, testInstanceParticipants);
                String resString = null;
                
                boolean isXUA = this.isXUA(listContextualInformationInstanceInput);
                try {
                    String requestSOAP = HL7V3Responder.generateRequestSOAP(msgString, isXUA, wsurl);
//                    resString = SOAPSender2.sendToUrlAndGetResponse(wsurl, requestSOAP);
//                    log.info("message sended");
//                    this.saveXCPDMessageTransaction(requestSOAP, resString, wsurl);
                    // TODO
                    log.info("message saved");
                    rsm = new ResultSendMessage();
                    rsm.setEvaluation(true);
                }catch(SOAPException se){
                    log.info("SOAPException" + se.getMessage());
                    se.printStackTrace();
                } catch(Exception e){
                    log.info("Exception e = " + e.getMessage());
                    e.printStackTrace();
                }
    	    }
		}
		return rsm;
	}
	
	public void saveReferencesForTestStepsInstance(String testInstanceId, String testInstanceParticipantsId, ConfigurationForWS responderConfiguration) throws SOAPException{
	    TestInstanceParticipants testInstanceParticipants=TestInstanceParticipants.getTestInstanceParticipants(testInstanceParticipantsId);
        TestInstance testInstance = TestInstance.getTestInstanceByServerTestInstanceId(testInstanceId);
        
        HL7V3ResponderConfiguration hl7v3conf = null;
        if (responderConfiguration == null){
            SOAPException se = new SOAPException("There are no receiver configuration sent to the simulator. You have to configure your system.");
            log.info("se.getMessage()  = " + se.getMessage());
            this.saveXCPDMessageTransaction(se.getMessage(), null, null);
            throw se;
        }
        hl7v3conf = responderConfiguration.gethL7V3ResponderConfiguration();
        if (hl7v3conf == null){
            SOAPException se = new SOAPException("There are no receiver configuration sent to the simulator. You have to configure your system.");
            log.info("se.getMessage()  = " + se.getMessage());
            this.saveXCPDMessageTransaction(se.getMessage(), null, null);
            throw se;
        }
        log.info("wsconf = " + HL7V3ResponderConfiguration.createWSUrl(hl7v3conf));
        hl7v3conf.setSystem(testInstanceParticipants.getSystem());
        
        hl7v3conf = HL7V3ResponderConfiguration.mergeHL7V3ResponderConfiguration(hl7v3conf);
        
        TestStepsInstance tsi = new TestStepsInstance();
        tsi.setTestInstance(testInstance);
        tsi.sethL7V3ResponderConfiguration(hl7v3conf);
        EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
        tsi = entityManager.merge(tsi);
        entityManager.flush();
        log.info("saved !!");
	}
	
	
	private boolean isXUA(List<ContextualInformationInstance> listContextualInformationInstanceInput){
	    boolean res = false;
	    if (listContextualInformationInstanceInput != null){
    	    for (ContextualInformationInstance cii:listContextualInformationInstanceInput){
    	        if (cii.getContextualInformation().getPath().getKeyword().toLowerCase().equals("isxua")){
    	            if (cii.getValue().toLowerCase().equals("true")){
    	                res = true;
    	            }
    	            else{
    	                res = false;
    	            }
    	        }
    	    }
	    }
	    // String isx = ApplicationConfiguration.getValueOfVariable("is_xua");
	    // if (isx.equals("True")) res = true;
	    return res;
	}
	
	private void saveXCPDMessageTransaction(String soapRequest, String responseV3, String wsURL){
//	    XCPDMessage mess = new XCPDMessage();
//        mess.setMessageSentContent(soapRequest.getBytes());
//        mess.setMessageResponseContent(responseV3.getBytes());
//        mess.setMessageType("PRPA_IN201305UV02");
//        mess.setTimeStamp(new Date());
//        mess.setContext(MessageSwitch.Gazelle);
//        mess.setTransaction(Transaction.GetTransactionByKeyword("ITI-55"));
//        if (wsURL != null){
//            Responder res = Responder.getResponderByURL(wsURL);
//            if (res != null){
//                mess.setConfiguration(res);
//            }
//            else{
//                res = new Responder(wsURL, wsURL);
//                res = Responder.persistResponder(res);
//                mess.setConfiguration(res);
//            }
//        }
//        mess = XCPDMessage.persistXCPDMessage(mess);
		// TODO
	}
	
    private String createMessageToSend(List<ContextualInformationInstance> lcii, TestInstanceParticipants tip){
        String res = new String();
        String family = this.getFamilyName(lcii);
        String given  = this.getGivenName(lcii);
        String birthDate = this.getBirthDate(lcii);
        String root = this.getRoot(lcii);
        String extension = this.getExtension(lcii);
        String gender = this.getGender(lcii);
        String street = this.getStreet(lcii);
        String city = this.getCity(lcii);
        String country = this.getCountry(lcii);
        String zipcode = this.getZipCode(lcii);
        String maidenName = this.getMaidenName(lcii);
        
        MessageGenerator mg = new MessageGenerator();
        /////////////////////////////to fix about sender and receiver
        String senderHomeCommunityID = ApplicationConfiguration.getValueOfVariable("homeCommunityID");
        String responderHomeCommunityID = new String();
        
        List<OIDConfiguration> loc = tip.getSystem().getOidConfigurationList();
        for (OIDConfiguration oidConfiguration : loc) {
            if (oidConfiguration.getLabel().equals("organization OID")){
                responderHomeCommunityID = oidConfiguration.getOid();
            }
        }
        
        //TODO fix the problem of homecommunityId
        /*
        if (tip != null){
            if (tip.getSystem() != null){
                if (tip.getSystem().getWebServiceConfiguration() != null){
                    if (tip.getSystem().getWebServiceConfiguration().getAssigningAuthorityOID() != null){
                        responderHomeCommunityID = tip.getSystem().getWebServiceConfiguration().getAssigningAuthorityOID();
                    }
                }
            }
        }
        */
        res = mg.createMessageToSend(family, given, DateType.EXACT_DATES, birthDate, null, null, null, root, extension, gender, 
                street, city, country, zipcode, maidenName, senderHomeCommunityID, responderHomeCommunityID, null, null, null);
        return res;
    }
    
    private String getFamilyName(List<ContextualInformationInstance> lcii){
        String res = null;
        for (ContextualInformationInstance cii:lcii){
            if (cii.getContextualInformation() != null){
                if (cii.getContextualInformation().getPath()!= null){
                    if (cii.getContextualInformation().getPath().getKeyword().toLowerCase().equals(FAMILY)){
                        res = cii.getValue();
                    }
                }
            }
            
        }
        if (!this.listContextualInformationNotEmpty(lcii)){
            res = null;
            String val = ApplicationConfiguration.getValueOfVariable(FAMILY);
            res = val;
        }
        
        return res;
    }
    
    private String getGivenName(List<ContextualInformationInstance> lcii){
        String res = null;
        for (ContextualInformationInstance cii:lcii){
            if (cii.getContextualInformation() != null){
                if (cii.getContextualInformation().getPath()!= null){
                    if (cii.getContextualInformation().getPath().getKeyword().toLowerCase().equals(GIVEN)){
                        res = cii.getValue();
                    }
                }
            }
           
        }
        if (!this.listContextualInformationNotEmpty(lcii)){
            res = null;
            String val = ApplicationConfiguration.getValueOfVariable(GIVEN);
            res = val;
        }
        return res;
    }
    
    private String getBirthDate(List<ContextualInformationInstance> lcii){
        String res = null;
        for (ContextualInformationInstance cii:lcii){
            if (cii.getContextualInformation() != null){
                if (cii.getContextualInformation().getPath()!= null){
                    if (cii.getContextualInformation().getPath().getKeyword().toLowerCase().equals(BIRTH_DATE)){
                        res = cii.getValue();
                    }
                }
            }
            
        }
        if (!this.listContextualInformationNotEmpty(lcii)){
            res = null;
            String val = ApplicationConfiguration.getValueOfVariable(BIRTH_DATE);
            res = val;
        }
        return res;
    }
    
    private String getRoot(List<ContextualInformationInstance> lcii){
        String res = null;
        for (ContextualInformationInstance cii:lcii){
            if (cii.getContextualInformation() != null){
                if (cii.getContextualInformation().getPath()!= null){
                    if (cii.getContextualInformation().getPath().getKeyword().toLowerCase().toLowerCase().equals(ROOT)){
                        res = cii.getValue();
                    }
                }
            }
        }
        if (!this.listContextualInformationNotEmpty(lcii)){
            String val = ApplicationConfiguration.getValueOfVariable(ROOT);
            res = val;
        }
        return res;
    }
    
    private String getGender(List<ContextualInformationInstance> lcii){
        String res = null;
        for (ContextualInformationInstance cii:lcii){
            if (cii.getContextualInformation() != null){
                if (cii.getContextualInformation().getPath()!= null){
                    if (cii.getContextualInformation().getPath().getKeyword().toLowerCase().equals(GENDER)){
                        res = cii.getValue();
                    }
                }
            }
        }
        if (!this.listContextualInformationNotEmpty(lcii)){
            String val = ApplicationConfiguration.getValueOfVariable(GENDER);
            res = val;
        }
        return res;
    }
    
    private String getExtension(List<ContextualInformationInstance> lcii){
        String res = null;
        for (ContextualInformationInstance cii:lcii){
            if (cii.getContextualInformation() != null){
                if (cii.getContextualInformation().getPath()!= null){
                    if (cii.getContextualInformation().getPath().getKeyword().equals(EXTENSION)){
                        res = cii.getValue();
                    }
                }
            }
        }
        if (!this.listContextualInformationNotEmpty(lcii)){
            String val = ApplicationConfiguration.getValueOfVariable(EXTENSION);
            res = val;
        }
        return res;
    }

    private String getStreet(List<ContextualInformationInstance> lcii){
        String res = null;
        for (ContextualInformationInstance cii:lcii){
            if (cii.getContextualInformation() != null){
                if (cii.getContextualInformation().getPath()!= null){
                    if (cii.getContextualInformation().getPath().getKeyword().equals(STREET)){
                        res = cii.getValue();
                    }
                }
            }
        }
        if (!this.listContextualInformationNotEmpty(lcii)){
            String val = ApplicationConfiguration.getValueOfVariable(STREET);
            res = val;
        }
        return res;
    }
    
    private String getCity(List<ContextualInformationInstance> lcii){
        String res = null;
        for (ContextualInformationInstance cii:lcii){
            if (cii.getContextualInformation() != null){
                if (cii.getContextualInformation().getPath()!= null){
                    if (cii.getContextualInformation().getPath().getKeyword().equals(CITY)){
                        res = cii.getValue();
                    }
                }
            }
        }
        if (!this.listContextualInformationNotEmpty(lcii)){
            String val = ApplicationConfiguration.getValueOfVariable(CITY);
            res = val;
        }
        return res;
    }
    
    private String getCountry(List<ContextualInformationInstance> lcii){
        String res = null;
        for (ContextualInformationInstance cii:lcii){
            if (cii.getContextualInformation() != null){
                if (cii.getContextualInformation().getPath()!= null){
                    if (cii.getContextualInformation().getPath().getKeyword().equals(COUNTRY)){
                        res = cii.getValue();
                    }
                }
            }
        }
        if (!this.listContextualInformationNotEmpty(lcii)){
            String val = ApplicationConfiguration.getValueOfVariable(COUNTRY);
            res = val;
        }
        return res;
    }
    
    private String getZipCode(List<ContextualInformationInstance> lcii){
        String res = null;
        for (ContextualInformationInstance cii:lcii){
            if (cii.getContextualInformation() != null){
                if (cii.getContextualInformation().getPath()!= null){
                    if (cii.getContextualInformation().getPath().getKeyword().equals(POSTAL_CODE)){
                        res = cii.getValue();
                    }
                }
            }
        }
        if (!this.listContextualInformationNotEmpty(lcii)){
            String val = ApplicationConfiguration.getValueOfVariable(POSTAL_CODE);
            res = val;
        }
        return res;
    }
    
    private String getMaidenName(List<ContextualInformationInstance> lcii){
        String res = null;
        for (ContextualInformationInstance cii:lcii){
            if (cii.getContextualInformation() != null){
                if (cii.getContextualInformation().getPath()!= null){
                    if (cii.getContextualInformation().getPath().getKeyword().equals(MAIDEN_NAME)){
                        res = cii.getValue();
                    }
                }
            }
        }
        if (!this.listContextualInformationNotEmpty(lcii)){
            String val = ApplicationConfiguration.getValueOfVariable(MAIDEN_NAME);
            res = val;
        }
        return res;
    }
    
    private boolean listContextualInformationNotEmpty(List<ContextualInformationInstance> lcii){
        boolean result = false;
        List<String> listpath = new ArrayList<String>();
        listpath.add(FAMILY);
        listpath.add(GIVEN);
        listpath.add(BIRTH_DATE);
        listpath.add(ROOT);
        listpath.add(GENDER);
        listpath.add(EXTENSION);
        listpath.add(STREET);
        listpath.add(CITY);
        listpath.add(COUNTRY);
        listpath.add(POSTAL_CODE);
        listpath.add(MAIDEN_NAME);
        for (ContextualInformationInstance cii:lcii){
            if (cii.getContextualInformation() != null){
                if (cii.getContextualInformation().getPath()!= null){
                    if (listpath.contains(cii.getContextualInformation().getPath().getKeyword())){
                        result = true;
                    }
                }
            }
        }
        return result;
    }
	

}


