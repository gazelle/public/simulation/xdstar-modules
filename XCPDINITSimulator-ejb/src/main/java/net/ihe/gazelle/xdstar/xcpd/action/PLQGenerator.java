package net.ihe.gazelle.xdstar.xcpd.action;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

/**
 * @author abderrazek boufahja
 */
public class PLQGenerator {

    protected String root;

    protected String extension;

    final static String PLQ_STRUCTURE = "<xcpd:PatientLocationQueryRequest xmlns:xcpd=\"urn:ihe:iti:xcpd:2009\">\n" +
            "	<xcpd:RequestedPatientId root=\"_ROOT_\" extension=\"_EXTENSION_\"/>\n" +
            "</xcpd:PatientLocationQueryRequest>";

    // constructor ///////////////////////////////////

    public PLQGenerator() {
    }

    //~ getters and setters /////////////////////////////////////////////////////////////


    public String getRoot() {
        if (root == null) {
            root = ApplicationConfiguration.getValueOfVariable("root");
        }
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public String getExtension() {
        if (extension == null) {
            extension = ApplicationConfiguration.getValueOfVariable("extension");
        }
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }


    // methods ////////////////////////////////

    public String generateMessageFromValues() {
        String res = null;
        if ((this.root == null) && (this.extension == null)) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "All fields are empty !");
            return null;
        }
        res = PLQ_STRUCTURE;
        res = res.replace("_ROOT_", this.root);
        res = res.replace("_EXTENSION_", this.extension);
        res = res.replace("null", "");

        return res;
    }


}
