package net.ihe.gazelle.xdstar.xcpd.action;

import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("epSOS2XCPDManager")
@Scope(ScopeType.PAGE)
public class EPSOS2XCPDManager extends XCPDINITManager {
	
	private static final long serialVersionUID = 1L;

	@Create
	public void initEPSOS2XCPD(){
		this.init();
		this.selectedAffinityDomain = AffinityDomain.getAffinityDomainByKeyword("epSOS-2");
		this.selectedTransaction = Transaction.GetTransactionByKeyword("PatientIdentificationAndAuthenticationService");
		this.initFromMenu = true;
	}
	

}
