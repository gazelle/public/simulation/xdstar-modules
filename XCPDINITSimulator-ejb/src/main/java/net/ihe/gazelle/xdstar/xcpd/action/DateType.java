package net.ihe.gazelle.xdstar.xcpd.action;

/**
 * @author abderrazek boufahja
 */
public enum DateType{
	EXACT_DATES, APPROXIMATE_DATES, RANGE_DATES
}