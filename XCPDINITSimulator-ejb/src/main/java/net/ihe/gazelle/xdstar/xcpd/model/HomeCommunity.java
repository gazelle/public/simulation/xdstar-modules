package net.ihe.gazelle.xdstar.xcpd.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

/**
 * @author Abderrazek Boufahja > INRIA Rennes IHE development Project
 *
 */
@Entity
@Name("homeCommunity")
@Table(name = "xcpd_home_community", schema = "public")
@SequenceGenerator(name = "xcpd_home_community_sequence", sequenceName = "xcpd_home_community_id_seq", allocationSize = 1)
public class HomeCommunity  implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "xcpd_home_community_sequence")
    private Integer id;

    @Column(name = "country_code")
    private String countryCode;

    @Column(name = "description")
    private String homeCommunityID;

    // constructors ////////////////////////////////

    public HomeCommunity(){
    }

    public HomeCommunity(Integer id, String countryCode, String homeCommunityID) {
        super();
        this.id = id;
        this.countryCode = countryCode;
        this.homeCommunityID = homeCommunityID;
    }

    /// getters and setters ///////////////////////////////

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setHomeCommunityID(String homeCommunityID) {
        this.homeCommunityID = homeCommunityID;
    }

    public String getHomeCommunityID() {
        return homeCommunityID;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    // hashcode && equals  /////////////////

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
        + ((countryCode == null) ? 0 : countryCode.hashCode());
        result = prime * result
        + ((homeCommunityID == null) ? 0 : homeCommunityID.hashCode());
        return result;
    }



    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        HomeCommunity other = (HomeCommunity) obj;
        if (countryCode == null) {
            if (other.countryCode != null)
                return false;
        } else if (!countryCode.equals(other.countryCode))
            return false;
        if (homeCommunityID == null) {
            if (other.homeCommunityID != null)
                return false;
        } else if (!homeCommunityID.equals(other.homeCommunityID))
            return false;
        return true;
    }

    public String toString() {
        return "HomeCommunity [id=" + id + ", countryCode=" + countryCode
        + ", homeCommunityID=" + homeCommunityID + "]";
    }

    // methods ///////////////////

    @SuppressWarnings("unchecked")
    public static List<HomeCommunity> getListOfHomeCommunity(){
        EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
        Query queryContact = entityManager.createQuery("SELECT hc From HomeCommunity hc");
        return (List<HomeCommunity>)queryContact.getResultList();
    }

    @SuppressWarnings("unchecked")
    public static HomeCommunity getHomeCommunityByCountryCode(String countryCode){
        EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
        Query queryContact = entityManager.createQuery("SELECT hc From HomeCommunity hc WHERE hc.countryCode=:countryCode");
        queryContact.setParameter("countryCode", countryCode);
        List<HomeCommunity> lr =  (List<HomeCommunity>)queryContact.getResultList();
        if (lr != null){
            if (lr.size()>0){
                return lr.get(0);
            }
        }
        return null;
    }

    public static HomeCommunity persistHomeCommunity(HomeCommunity hc){
        EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
        hc = entityManager.merge(hc);
        entityManager.flush();
        return hc;
    }
    
    @SuppressWarnings("unchecked")
    public static List<String> getListOfHomeCommunityID(){
        EntityManager entityManager=(EntityManager)Component.getInstance("entityManager");
        Query queryContact = entityManager.createQuery("SELECT hc.homeCommunityID From HomeCommunity hc");
        return (List<String>)queryContact.getResultList();
    }

}
