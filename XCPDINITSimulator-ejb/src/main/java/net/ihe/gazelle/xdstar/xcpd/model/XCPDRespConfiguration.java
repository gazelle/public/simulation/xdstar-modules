package net.ihe.gazelle.xdstar.xcpd.model;



import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;

import org.jboss.seam.annotations.Name;

@Entity
@Name("xCPDRespConfiguration")
@DiscriminatorValue("XCPDRESP")
public class XCPDRespConfiguration extends SystemConfiguration implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String homeCommunityId;
	
	public XCPDRespConfiguration(){}
	
	public XCPDRespConfiguration(XCPDRespConfiguration configuration) {
		super(configuration);
		this.homeCommunityId = configuration.getHomeCommunityId();
	}

	public String getHomeCommunityId() {
		return homeCommunityId;
	}

	public void setHomeCommunityId(String homeCommunityId) {
		this.homeCommunityId = homeCommunityId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((homeCommunityId == null) ? 0 : homeCommunityId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		XCPDRespConfiguration other = (XCPDRespConfiguration) obj;
		if (homeCommunityId == null) {
			if (other.homeCommunityId != null)
				return false;
		} else if (!homeCommunityId.equals(other.homeCommunityId))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "XCARespConfiguration [homeCommunityId=" + homeCommunityId + ", id=" + id
				+ ", name=" + name + ", systemName=" + systemName + ", url="
				+ url + "]";
	}
	
	
}
