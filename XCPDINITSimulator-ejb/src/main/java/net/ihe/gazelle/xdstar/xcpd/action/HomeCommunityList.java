package net.ihe.gazelle.xdstar.xcpd.action;

import net.ihe.gazelle.xdstar.xcpd.model.HomeCommunity;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.framework.EntityQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * @author abderrazek boufahja
 */
@Name("homeCommunityList")
public class HomeCommunityList extends EntityQuery<HomeCommunity> {

    private static final Logger LOG = LoggerFactory.getLogger(HomeCommunityList.class);
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final String EJBQL = "select homeCommunity from HomeCommunity homeCommunity";

    private static final String[] RESTRICTIONS = {
            "lower(homeCommunity.countryCode) like lower(concat(#{homeCommunityList.homeCommunity.countryCode},'%'))",
            "lower(homeCommunity.homeCommunityID) like lower(concat(#{homeCommunityList.homeCommunity.homeCommunityID},'%'))",};

    private HomeCommunity homeCommunity = new HomeCommunity();

    public HomeCommunityList() {
        setEjbql(EJBQL);
        setRestrictionExpressionStrings(Arrays.asList(RESTRICTIONS));
        setMaxResults(25);
    }

    public HomeCommunity getHomeCommunity() {
        return homeCommunity;
    }

    public void resetFilter() {
        getHomeCommunity().setCountryCode(null);
        getHomeCommunity().setHomeCommunityID(null);

    }

    @Destroy
    public void destroy() {
        LOG.info("destroy homeCommunityList..");
    }
}
