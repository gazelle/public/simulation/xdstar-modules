package net.ihe.gazelle.xdstar.xcpd.action;

import net.ihe.gazelle.xdstar.xcpd.model.HomeCommunity;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.framework.EntityHome;

/**
 * @author abderrazek boufahja
 */
@Name("homeCommunityHome")
public class HomeCommunityHome extends EntityHome<HomeCommunity> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void setHomeCommunityId(Integer id) {
		setId(id);
	}

	public Integer getHomeCommunityId() {
		return (Integer) getId();
	}

	@Override
	protected HomeCommunity createInstance() {
		HomeCommunity homeCommunity = new HomeCommunity();
		return homeCommunity;
	}

	public void load() {
		if (isIdDefined()) {
			wire();
		}
	}

	public void wire() {
		getInstance();
	}

	public boolean isWired() {
		return true;
	}

	public HomeCommunity getDefinedInstance() {
		return isIdDefined() ? getInstance() : null;
	}

}
