package net.ihe.gazelle.xdstar.xcpd.action;

import net.ihe.gazelle.simulator.ws.MessageGenerator;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import java.util.Arrays;
import java.util.List;


/**
 * @author abderrazek boufahja
 */
public class IHERequestGenerator extends AbstractGenerator {

    private String bdhigh;
    private String bdcenter;
    private String bdlow;
    private DateType dateType;

    public IHERequestGenerator() {
        this.dateType = DateType.EXACT_DATES;
    }

    public String getBdlow() {
        return bdlow;
    }

    public void setBdlow(String bdlow) {
        this.bdlow = bdlow;
    }

    public String getBdhigh() {
        return bdhigh;
    }

    public void setBdhigh(String bdhigh) {
        this.bdhigh = bdhigh;
    }

    public String getBdcenter() {
        return bdcenter;
    }

    public void setBdcenter(String bdcenter) {
        this.bdcenter = bdcenter;
    }

    public DateType getDateType() {
        return dateType;
    }

    public void setDateType(DateType dateType) {
        this.dateType = dateType;
    }

    public String generateMessageFromValuesForAbstract() {
        String res;
        if ((this.family == null) && (this.given == null) && (this.gender == null) && (this.root == null)
                && (this.extension == null) && (this.maidenname == null) &&
                (this.bdcenter == null) && (this.bdhigh == null) && (this.bdlow == null) && (this.birthdate == null)
                && (this.city == null) && (this.country == null) && (this.postalcode == null)
                && (this.street == null)) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "All fields are empty !");
            return null;
        }
        MessageGenerator mg = new MessageGenerator();
        res = mg.createMessageToSend(family, given,
                this.dateType, this.birthdate, this.bdhigh, this.bdlow, this.bdcenter,
                root, extension, gender, street, city, country,
                postalcode, maidenname, senderHomeCommunityId, receiverHomeCommunityId, telecom, principalCareProviderIdRoot,
                principalCareProviderIdExtension);
        return res;
    }

    public List<DateType> getDateTypes() {
        return Arrays.asList(DateType.values());
    }
}
