package net.ihe.gazelle.xdstar.xcpd.action;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.ws.MessageGenerator;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author abderrazek boufahja
 */
public class AbstractGenerator {


    private static Logger log = LoggerFactory.getLogger(AbstractGenerator.class);

    protected String family;

    protected String given;

    protected String birthdate;

    protected String root;

    protected String extension;

    protected String gender;

    protected String street;

    protected String city;

    protected String country;

    protected String postalcode;

    protected String maidenname;

    protected String senderHomeCommunityId;

    protected String receiverHomeCommunityId;

    protected String telecom;

    protected String principalCareProviderIdRoot;

    protected String principalCareProviderIdExtension;

    // constructor ///////////////////////////////////

    public AbstractGenerator() {
    }

    //~ getters and setters /////////////////////////////////////////////////////////////

    public String getTelecom() {
        return telecom;
    }

    public void setTelecom(String telecom) {
        this.telecom = telecom;
    }

    public String getPrincipalCareProviderIdRoot() {
        return principalCareProviderIdRoot;
    }

    public void setPrincipalCareProviderIdRoot(String principalCareProviderIdRoot) {
        this.principalCareProviderIdRoot = principalCareProviderIdRoot;
    }

    public String getPrincipalCareProviderIdExtension() {
        return principalCareProviderIdExtension;
    }

    public void setPrincipalCareProviderIdExtension(
            String principalCareProviderIdExtension) {
        this.principalCareProviderIdExtension = principalCareProviderIdExtension;
    }

    public void setReceiverHomeCommunityId(String receiverHomeCommunityId) {
        this.receiverHomeCommunityId = receiverHomeCommunityId;
    }

    public String getReceiverHomeCommunityId() {
        return receiverHomeCommunityId;
    }

    public void setSenderHomeCommunityId(String senderHomeCommunityId) {
        this.senderHomeCommunityId = senderHomeCommunityId;
    }

    public String getSenderHomeCommunityId() {
        return senderHomeCommunityId;
    }

    public String getFamily() {
        if (family == null) {
            family = ApplicationConfiguration.getValueOfVariable("family");
        }
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getGiven() {
        if (given == null) {
            given = ApplicationConfiguration.getValueOfVariable("given");
        }
        return given;
    }

    public void setGiven(String given) {
        this.given = given;
    }

    public String getBirthdate() {
        if (birthdate == null) {
            birthdate = ApplicationConfiguration.getValueOfVariable("birthdate");
        }
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getRoot() {
        if (root == null) {
            root = ApplicationConfiguration.getValueOfVariable("root");
        }
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public String getExtension() {
        if (extension == null) {
            extension = ApplicationConfiguration.getValueOfVariable("extension");
        }
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getGender() {
        if (gender == null) {
            gender = ApplicationConfiguration.getValueOfVariable("gender");
        }
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStreet() {
        if (street == null) {
            street = ApplicationConfiguration.getValueOfVariable("street");
        }
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        if (city == null) {
            city = ApplicationConfiguration.getValueOfVariable("city");
        }
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        if (country == null) {
            country = ApplicationConfiguration.getValueOfVariable("country");
        }
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostalcode() {
        if (postalcode == null) {
            postalcode = ApplicationConfiguration.getValueOfVariable("postalcode");
        }
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getMaidenname() {
        if (maidenname == null) {
            maidenname = ApplicationConfiguration.getValueOfVariable("maidenname");
        }
        return maidenname;
    }

    public void setMaidenname(String maidenname) {
        this.maidenname = maidenname;
    }

    // methods ////////////////////////////////

    public String generateMessageFromValuesForAbstract() {
        String res;
        if ((this.family == null) && (this.given == null) && (this.gender == null) && (this.root == null)
                && (this.extension == null) && (this.birthdate == null) && (this.maidenname == null)
                && (this.city == null) && (this.country == null) && (this.postalcode == null)
                && (this.street == null)) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "All fields are empty !");
            return null;
        }
        MessageGenerator mg = new MessageGenerator();
        res = mg.createMessageToSend(family, given,
                DateType.EXACT_DATES, birthdate, null, null, null,
                root, extension, gender, street, city, country,
                postalcode, maidenname, senderHomeCommunityId, receiverHomeCommunityId, telecom, principalCareProviderIdRoot,
                principalCareProviderIdExtension);
        return res;
    }

}
