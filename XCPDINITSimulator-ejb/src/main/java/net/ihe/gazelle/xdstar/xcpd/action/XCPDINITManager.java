package net.ihe.gazelle.xdstar.xcpd.action;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.xua.SignatureException;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.xdstar.common.action.CommonSimulatorManager;
import net.ihe.gazelle.xdstar.common.model.ContentType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import net.ihe.gazelle.xdstar.comon.util.ConfigurationFilter;
import net.ihe.gazelle.xdstar.core.MessageSenderCommon;
import net.ihe.gazelle.xdstar.core.soap.SOAPRequestBuilder;
import net.ihe.gazelle.xdstar.validator.SchematronValidator;
import net.ihe.gazelle.xdstar.validator.XSLTransformator;
import net.ihe.gazelle.xdstar.xcpd.model.HomeCommunity;
import net.ihe.gazelle.xdstar.xcpd.model.XCPDMessage;
import net.ihe.gazelle.xdstar.xcpd.model.XCPDRespConfiguration;
import net.ihe.gazelle.xdstar.xcpd.model.XCPDRespConfigurationQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.parsers.ParserConfigurationException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Name("xcpdInitManager")
@Scope(ScopeType.PAGE)
public class XCPDINITManager extends CommonSimulatorManager<XCPDRespConfiguration, XCPDMessage> implements Serializable, UserAttributeCommon {

    private static final String PAT_IDENTIF_AND_AUTHENT_SERVICE = "PatientIdentificationAndAuthenticationService";

    private static final String IDENTIFICATION_SERVICE = "IdentificationService";

    private static final String ITI_55 = "ITI-55";

    private static final String ITI_56 = "ITI-56";

    private static final long serialVersionUID = 1L;

    public static String TRANSACTION_TYPE = "XCPD";


    private static Logger log = LoggerFactory.getLogger(XCPDINITManager.class);

    private boolean useRequestGenerator = true;

    private boolean validRequest = false;

    private String requestV3;

    private IHERequestGenerator requestGenerator;

    private PLQGenerator plqGenerator;

    private String xmlMetaDataReq = "IHE - XCPD ITI-55 (request)";

    private String resultValidation;

    private Boolean useTRC = false;

    private String patientId = null;

    @In(value = "gumUserService")
    private UserService userService;

    public Boolean getUseTRC() {
        return useTRC;
    }

    public void setUseTRC(Boolean useTRC) {
        this.useTRC = useTRC;
        if (this.useTRC == null || !this.useTRC) {
            this.patientId = null;
        }
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public PLQGenerator getPlqGenerator() {
        return plqGenerator;
    }

    public void setPlqGenerator(PLQGenerator plqGenerator) {
        this.plqGenerator = plqGenerator;
    }

    public String getResultValidation() {
        return resultValidation;
    }

    public void setResultValidation(String resultValidation) {
        this.resultValidation = resultValidation;
    }

    public String getXmlMetaDataReq() {
        return xmlMetaDataReq;
    }

    public void setXmlMetaDataReq(String xmlMetaDataReq) {
        this.xmlMetaDataReq = xmlMetaDataReq;
    }

    public IHERequestGenerator getRequestGenerator() {
        return requestGenerator;
    }

    public void setRequestGenerator(IHERequestGenerator requestGenerator) {
        this.requestGenerator = requestGenerator;
    }

    public String getRequestV3() {
        return requestV3;
    }

    public void setRequestV3(String requestV3) {
        this.requestV3 = requestV3;
    }

    public boolean isValidRequest() {
        if (this.useRequestGenerator == true) {
            validRequest = true;
        }
        return validRequest;
    }

    public void setValidRequest(boolean validRequest) {
        this.validRequest = validRequest;
    }

    public boolean isUseRequestGenerator() {
        return useRequestGenerator;
    }

    public void setUseRequestGenerator(boolean useRequestGenerator) {
        this.useRequestGenerator = useRequestGenerator;
    }

    public void listAllConfigurations() {
        XCPDRespConfigurationQuery rc = new XCPDRespConfigurationQuery();
        if (selectedAffinityDomain != null) {
            rc.listUsages().affinity().eq(selectedAffinityDomain);
        }
        if (selectedTransaction != null) {
            rc.listUsages().transaction().eq(selectedTransaction);
        }
        rc.name().order(true);
        ConfigurationFilter.filterConfigurationQueryAccodingToUserCredential(rc);
        configurations = rc.getList();
    }

    public void init() {
        super.init();
        this.requestGenerator = new IHERequestGenerator();
        this.plqGenerator = new PLQGenerator();
    }

    public void reset() {
        super.reset();
    }

    public void sendMessage() {
        log.info("XCPDimulatorManager::sendMessage");
        message = new XCPDMessage();
        if ((this.selectedTransaction != null) && (this.selectedTransaction.getKeyword().contains(ITI_56))) {
            message.setMessageType("PatientLocationQuery");
        } else if ((this.selectedTransaction != null) && (this.selectedTransaction.getKeyword().contains(ITI_55))) {
            message.setMessageType("PatientDiscovery");
        } else if ((this.selectedTransaction != null) && (this.selectedTransaction.getKeyword().contains(IDENTIFICATION_SERVICE))) {
            message.setMessageType("findIdentityByTraits");
        } else if ((this.selectedTransaction != null) && (this.selectedTransaction.getKeyword().contains(PAT_IDENTIF_AND_AUTHENT_SERVICE))) {
            message.setMessageType("findIdentityByTraits");
        }
        message.setTransaction(selectedTransaction);
        message.setAffinityDomain(selectedAffinityDomain);
        message.setContentType(ContentType.SOAP_MESSAGE);
        message.setTimeStamp(new Date());
        message.setGazelleDriven(false);
        message.setConfiguration(selectedConfiguration);

        MetadataMessage sent = new MetadataMessage();
        String messageToSend = this.createMessage();
        sent.setMessageContent(messageToSend);
        if ((this.selectedTransaction != null) && (this.selectedTransaction.getKeyword().contains(ITI_56))) {
            sent.setMessageType(MetadataMessageType.PatientLocationQueryRequest);
        } else {
            sent.setMessageType(MetadataMessageType.PRPA_IN201305UV02);
        }
        message.setSentMessageContent(sent);
        message = XCPDMessage.storeMessage(message);

        MessageSenderCommon sender = new MessageSenderCommon(selectedConfiguration.getUrl(), message);
        try {
            if ((this.selectedTransaction != null) && (this.selectedTransaction.getKeyword().contains(ITI_56))) {
                sender.sendMessageSOAP(MetadataMessageType.PatientLocationQueryResponse);
            } else {
                sender.sendMessageSOAP(MetadataMessageType.PRPA_IN201306UV02);
            }
            message = (XCPDMessage) sender.getRequest();
            message = (XCPDMessage) XCPDMessage.storeMessage(message);
        } catch (Exception e) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.xca.initgw.errorWhenSendingMessage", e.getMessage
                    ());
        }
        Contexts.getSessionContext().set("messagesToDisplay", message);
        displayResultPanel = true;
    }

    private String createMessage() {
        String res = null;
        if ((this.selectedTransaction != null) && (
                (this.selectedTransaction.getKeyword().contains(ITI_55)) ||
                        (this.selectedTransaction.getKeyword().contains(IDENTIFICATION_SERVICE)) ||
                        (this.selectedTransaction.getKeyword().contains(PAT_IDENTIF_AND_AUTHENT_SERVICE))
        )
                ) {
            if (this.useRequestGenerator == true) {
                this.requestV3 = this.requestGenerator.generateMessageFromValuesForAbstract();
            }
            String receiverUrl = this.selectedConfiguration.getUrl();
            try {
                res = SOAPRequestBuilder.createSOAPMessage(this.requestV3, useXUA, praticianID, attributes, patientId, receiverUrl,
                        "urn:hl7-org:v3:PRPA_IN201305UV02:CrossGatewayPatientDiscovery", useTRC);
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SignatureException e) {
                e.printStackTrace();
            }
        } else if ((this.selectedTransaction != null) && (this.selectedTransaction.getKeyword().contains(ITI_56))) {
            String receiverUrl = this.selectedConfiguration.getUrl();
            String req = plqGenerator.generateMessageFromValues();
            try {
                res = SOAPRequestBuilder.createSOAPMessage(req, useXUA, praticianID, attributes, null, receiverUrl,
                        "urn:ihe:iti:2009:PatientLocationQuery", false);
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SignatureException e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    public String previewMessage() {
        String res = this.createMessage();
        return res;
    }


    public void updateSpecificClient() {
        if ((this.requestGenerator != null) && (this.selectedConfiguration != null)) {
            this.requestGenerator.setReceiverHomeCommunityId(this.selectedConfiguration.getHomeCommunityId());
        }
    }

    public List<AffinityDomain> listAllAffinityDomains() {
        return listAllAffinityDomains(TRANSACTION_TYPE);
    }

    @Override
    public String getTheNameOfThePage() {
        String res = this.getNameOfTheAffinityDomainTransaction();
        if (res == null || res.equals("")) {
            res = "XCPD Initiating Gateway";
        }
        return res;
    }

    public void externalValidation() {
        if (this.xmlMetaDataReq != null) {
            this.resultValidation = SchematronValidator.getValidationResultSchematron(this.xmlMetaDataReq, requestV3);
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, "Chose a type of validation.");
        }
    }

    public String getDetailedResults() {
        if (this.resultValidation != null) {
            return resultTransformation("0", this.resultValidation);
        }
        return null;
    }

    public String resultTransformation(String index, String validationDetails) {
        if (validationDetails != null) {
            String inXslPath = ApplicationConfiguration.getValueOfVariable("validation_result_xslt");
            HashMap<String, String> hash = new HashMap<String, String>();
            hash.put("index", index);
            String res = XSLTransformator.resultTransformation(validationDetails, inXslPath, hash);
            return res;
        }
        return null;
    }

    public List<String> autocompleteListOfHomeCommunityID(String begin) {
        List<String> res = new ArrayList<String>();
        List<String> compl = this.getListOfHomeCommunityID();
        for (String string : compl) {
            if (string.contains(begin)) {
                res.add(string);
            }
        }
        return res;
    }

    public List<String> getListOfHomeCommunityID() {
        HQLQueryBuilder<HomeCommunity> hh = new HQLQueryBuilder<HomeCommunity>(HomeCommunity.class);
        List<HomeCommunity> ll = hh.getList();
        List<String> res = new ArrayList<String>();
        for (HomeCommunity homeCommunity : ll) {
            res.add(homeCommunity.getHomeCommunityID());
        }

        if ((this.selectedConfiguration != null) && (this.selectedConfiguration.getHomeCommunityId() != null) && (!this.selectedConfiguration
                .getHomeCommunityId().equals(""))) {
            res.remove(this.selectedConfiguration.getHomeCommunityId());
            res.add(this.selectedConfiguration.getHomeCommunityId());
        }
        return res;
    }

    public String extractIDFromInitialisation() {
        if (this.selectedTransaction != null &&
                this.selectedTransaction.getKeyword().equals(IDENTIFICATION_SERVICE) &&
                this.selectedAffinityDomain != null &&
                this.selectedAffinityDomain.getKeyword().equals("epSOS")) {
            return "IDENT";
        } else if (this.selectedTransaction != null &&
                this.selectedTransaction.getKeyword().equals(ITI_55) &&
                this.selectedAffinityDomain != null &&
                this.selectedAffinityDomain.getKeyword().equals("IHE_ITI")) {
            return "XCPD1";
        } else if (this.selectedTransaction != null &&
                this.selectedTransaction.getKeyword().equals(ITI_56) &&
                this.selectedAffinityDomain != null &&
                this.selectedAffinityDomain.getKeyword().equals("IHE_ITI")) {
            return "XCPD2";
        }
        return null;
    }

    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }
}
