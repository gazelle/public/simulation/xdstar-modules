package net.ihe.gazelle.test;

import static org.junit.Assert.*;
import net.ihe.gazelle.xdstar.xdsi.action.WADOManager;
import net.ihe.gazelle.xdstar.xdsi.model.ImgDocSourceConfiguration;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class WADOManagerTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testPreviewMessage() {
		WADOManager wm = new WADOManager();
		wm.setSelectedConfiguration(new ImgDocSourceConfiguration());
		wm.getSelectedConfiguration().setUrl("http://gazelle.ihe.net/wado");
		wm.setRequestType("WADO");
		wm.setStudyUID("1.3.12.2.1107.5.5.5.1170160.20070418083655.4515");
		wm.setSeriesUID("1.3.12.2.1107.5.5.5.1170160.20070418083655.4516");
		wm.setObjectUID("1.3.12.2.1107.5.5.5.1170160.20070418083805.4536");
		wm.setContentType("image/jpeg");
		String prev = wm.previewMessage();
		assertTrue(prev.contains("WADO"));
		assertTrue(prev.contains("1.3.12.2.1107.5.5.5.1170160.20070418083655.4515"));
		assertTrue(prev.indexOf("http://gazelle.ihe.net/wado") == 0);
		assertFalse(prev.contains("image/jpeg"));
		System.out.println(prev);
	}

}
