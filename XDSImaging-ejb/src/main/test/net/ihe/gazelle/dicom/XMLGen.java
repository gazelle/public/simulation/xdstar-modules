package net.ihe.gazelle.dicom;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import javax.xml.transform.TransformerConfigurationException;

import net.ihe.gazelle.dicom.util.XmlFormatter;
import net.ihe.gazelle.edit.FileReadWrite;

import org.dcm4che2.data.Tag;
import org.dcm4che2.tool.dcm2xml.Dcm2Xml;

public class XMLGen {
	
	static int i = 0;
	
	public static void main(String[] args) {
		File file = new File("/home/aboufahj/Téléchargements/KOS");
		parseAndCreateFiles(file);
	}
	
	private static void parseAndCreateFiles(File file){
		if (file.isDirectory()){
			for (File ss : file.listFiles()) {
				parseAndCreateFiles(ss);
			}
		}
		else{
			try {
				String xml = dicom2xml(file.getAbsolutePath());
				FileReadWrite.printDoc(xml, "/home/aboufahj/Téléchargements/KOS/XML/" + i + ".xml");
				i++;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	
	private static String dicom2xml(String dicompath) throws TransformerConfigurationException, IOException{
		Dcm2Xml dcm2xml = new Dcm2Xml();
		File ifile =new File(dicompath);
		File ofile = null;
		
		PrintStream ops = System.out;
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		PrintStream nps = new PrintStream(outStream,true);;
		System.setOut(nps);
		dcm2xml.setExclude(new int[] {Tag.PixelData});
		dcm2xml.convert(ifile, ofile);
		
		System.setOut(ops);
		String maChaine = outStream.toString();
		maChaine = XmlFormatter.prettyFormat(maChaine, 4);
		outStream.close();
		return maChaine;
	}

}
