package net.ihe.gazelle.xdstar.drools;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import net.ihe.gazelle.edit.FileReadWrite;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.xdstar.drools.util.DroolsDocuments;
import net.ihe.gazelle.xdstar.drools.util.DroolsProcessor;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 * @author abderrazek boufahja
 *
 */
public class DroolsProcessorTest {
	
	private final static String MANIFEST_XML = "src/test/resources/drools/manifest.xml";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testInitKnowledgeBase() {
		//fail("Not yet implemented");
	}

	@Test
	public void testResetKnowledgeBase() {
		//fail("Not yet implemented");
	}

	@Test
	public void testValidateManifestContent() throws IOException {
		String xmlContent = FileReadWrite.readDoc("src/test/resources/drools/rad69_ok.xml");
		String resources = FileReadWrite.readDoc("src/test/resources/drools/manifest.xml");
		DroolsDocuments drd = new DroolsDocuments();
		drd.setDocumentToValidate(xmlContent);
		drd.getDocumentReferences().add(resources);
		List<Notification> res = DroolsProcessor.validateManifestContent(drd);
		System.out.println(res.size());
		for (Notification notification : res) {
			System.out.println(notification.getClass().getSimpleName() +  "#" + notification.getTest() + "#" + notification.getDescription());
		}
	}
	
	@Test
	public void testValidateManifestContentForTestStudyInstanceUID() throws IOException {
		validateManifestContentByTestId("src/test/resources/drools/rad69_studyInstanceUID_ok.xml", MANIFEST_XML, "studyInstanceUID", "Note");
		validateManifestContentByTestId("src/test/resources/drools/rad69_studyInstanceUID_ko.xml", MANIFEST_XML, "studyInstanceUID", "Error");
	}
	
	@Test
	public void testValidateManifestContentForTestSeriesInstanceUID() throws IOException {
		validateManifestContentByTestId("src/test/resources/drools/rad69_seriesInstanceUID_ok.xml", MANIFEST_XML, "seriesInstanceUID", "Note");
		validateManifestContentByTestId("src/test/resources/drools/rad69_seriesInstanceUID_ko.xml", MANIFEST_XML, "seriesInstanceUID", "Error");
	}
	
	@Test
	public void testValidateManifestContentForTestRepositoryUniqueId() throws IOException {
		validateManifestContentByTestId("src/test/resources/drools/rad69_repositoryUniqueId_ok.xml", MANIFEST_XML, "repositoryUniqueId", "Note");
		validateManifestContentByTestId("src/test/resources/drools/rad69_repositoryUniqueId_ko.xml", MANIFEST_XML, "repositoryUniqueId", "Error");
	}
	
	@Test
	public void testValidateManifestContentForTestDocumentUniqueId() throws IOException {
		validateManifestContentByTestId("src/test/resources/drools/rad69_documentUniqueId_ok.xml", MANIFEST_XML, "documentUniqueId", "Note");
		validateManifestContentByTestId("src/test/resources/drools/rad69_documentUniqueId_ko.xml", MANIFEST_XML, "documentUniqueId", "Error");
	}
	
	@Test
	public void testValidateManifestContentForTestNumberStudyRequest() throws IOException {
		validateManifestContentByTestId("src/test/resources/drools/rad69_numberStudyRequest_ok.xml", MANIFEST_XML, "numberStudyRequest", "Note");
		validateManifestContentByTestId("src/test/resources/drools/rad69_numberStudyRequest_ko.xml", MANIFEST_XML, "numberStudyRequest", "Error");
	}
	
	@Test
	public void testValidateManifestContentForTestNumberSeriesRequest() throws IOException {
		validateManifestContentByTestId("src/test/resources/drools/rad69_numberSeriesRequest_ok.xml", MANIFEST_XML, "numberSeriesRequest", "Note");
		validateManifestContentByTestId("src/test/resources/drools/rad69_numberSeriesRequest_ko.xml", MANIFEST_XML, "numberSeriesRequest", "Error");
	}
	
	@Test
	public void testValidateManifestContentForTestNumberDocumentRequest() throws IOException {
		validateManifestContentByTestId("src/test/resources/drools/rad69_numberDocumentRequest_ok.xml", MANIFEST_XML, "numberDocumentRequest", "Note");
		validateManifestContentByTestId("src/test/resources/drools/rad69_numberDocumentRequest_ko.xml", MANIFEST_XML, "numberDocumentRequest", "Error");
	}
	
	private void validateManifestContentByTestId(String pathrad69, String pathmanifest, String testId, String resType) throws IOException {
		String xmlContent = FileReadWrite.readDoc(pathrad69);
		String resources = FileReadWrite.readDoc(pathmanifest);
		DroolsDocuments drd = new DroolsDocuments();
		drd.setDocumentToValidate(xmlContent);
		drd.getDocumentReferences().add(resources);
		List<Notification> res = DroolsProcessor.validateManifestContent(drd);
		Notification notification1 = null;
		for (Notification notification : res) {
			if (notification.getTest().equals(testId)){
				notification1 = notification;
			}
		}
		assertTrue(notification1 != null && notification1.getClass().getSimpleName().equals(resType));
	}

}
