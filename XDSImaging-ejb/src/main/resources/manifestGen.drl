//created on: 9 sept. 2013
package net.ihe.gazelle.xdstar.rules

//list any import classes here.
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Note;

import net.ihe.gazelle.xdstar.drools.util.DroolsProcessor;
import net.ihe.gazelle.xdstar.drools.util.DroolsDocuments;
import net.ihe.gazelle.xdstar.drools.util.XpathComparatorKind;

//expander xvalidator.dsl

//declare any global variables here
global java.util.List myGlobalList;

function void createNotif(String test, String description, java.util.List myGlobalList, boolean check) {
	Notification notif = null;
	if (check){
		notif = new Note();
	}
	else{
		notif = new Error();
	}
	notif.setTest(test);
	notif.setDescription(description);
    myGlobalList.add(notif);
}


function boolean checkForXpath(DroolsDocuments drd, int docRefIndex, String xpath1, String xpath2, XpathComparatorKind kind) {
	return DroolsProcessor.validateManifest(drd.getDocumentToValidate(), drd.getDocumentReferences().get(docRefIndex), xpath1, xpath2, kind);
}


rule studyInstanceUID
    when 
    	$drl: DroolsDocuments()
        eval(true)
    then
    	String xpath1 = "data(//rad:StudyRequest/@studyInstanceUID)";
    	String xpath2 = "data(/dicom/attr[@tag='0040A375']/item//attr[@tag='0020000D']/text())";
    	String testId = "studyInstanceUID";
    	String testDescription = "The studyInstanceUID shall have the same values of the tags '0020000D'";
    	boolean check = checkForXpath($drl, 0 , xpath1, xpath2, XpathComparatorKind.IN); 
    	createNotif(testId, testDescription, myGlobalList, check);
end

rule seriesInstanceUID
    when 
    	$drl: DroolsDocuments()
        eval(true)
    then
    	String xpath1 = "data(//rad:SeriesRequest/@seriesInstanceUID)";
    	String xpath2 = "data(/dicom/attr[@tag='0040A375']/item//attr[@tag='0020000E']/text())";
    	String testId = "seriesInstanceUID";
    	String testDescription = "The seriesInstanceUID shall have the same values of the tags '0020000E'";
    	boolean check = checkForXpath($drl, 0 , xpath1, xpath2, XpathComparatorKind.IN); 
    	createNotif(testId, testDescription, myGlobalList, check);
end

rule repositoryUniqueId
    when 
    	$drl: DroolsDocuments()
        eval(true)
    then
    	String xpath1 = "data(//rad:SeriesRequest/xdsb:DocumentRequest/xdsb:RepositoryUniqueId/text())";
    	String xpath2 = "data(/dicom/attr[@tag='0040A375']/item//attr[@tag='0040E011']/text())";
    	String testId = "repositoryUniqueId";
    	String testDescription = "The repositoryUniqueId shall have the same values of the tags '0040E011'";
    	boolean check = checkForXpath($drl, 0 , xpath1, xpath2, XpathComparatorKind.IN); 
    	createNotif(testId, testDescription, myGlobalList, check);
end

rule documentUniqueId
    when 
    	$drl: DroolsDocuments()
        eval(true)
    then
    	String xpath1 = "data(//rad:SeriesRequest/xdsb:DocumentRequest/xdsb:DocumentUniqueId/text())";
    	String xpath2 = "data(/dicom/attr[@tag='0040A375']/item//attr[@tag='00081155']/text())";
    	String testId = "documentUniqueId";
    	String testDescription = "The documentUniqueId shall have the same values of the tags '00081155'";
    	boolean check = checkForXpath($drl, 0 , xpath1, xpath2, XpathComparatorKind.EQ); 
    	createNotif(testId, testDescription, myGlobalList, check);
end

rule numberStudyRequest
    when 
    	$drl: DroolsDocuments()
        eval(true)
    then
    	String xpath1 = "data(//rad:StudyRequest)";
    	String xpath2 = "data(/dicom/attr[@tag='0040A375']/item//attr[@tag='0020000D'])";
    	String testId = "numberStudyRequest";
    	String testDescription = "The number of StudyRequest shall have the same values of the number of attr with the tag '002000D'";
    	boolean check = checkForXpath($drl, 0 , xpath1, xpath2, XpathComparatorKind.EQSIZE); 
    	createNotif(testId, testDescription, myGlobalList, check);
end

rule numberSeriesRequest
    when 
    	$drl: DroolsDocuments()
        eval(true)
    then
    	String xpath1 = "data(//rad:SeriesRequest)";
    	String xpath2 = "data(/dicom/attr[@tag='0040A375']/item//attr[@tag='0020000E'])";
    	String testId = "numberSeriesRequest";
    	String testDescription = "The number of SeriesRequest shall have the same values of the number of attr with the tag '0020000E'";
    	boolean check = checkForXpath($drl, 0 , xpath1, xpath2, XpathComparatorKind.EQSIZE); 
    	createNotif(testId, testDescription, myGlobalList, check);
end

rule numberDocumentRequest
    when 
    	$drl: DroolsDocuments()
        eval(true)
    then
    	String xpath1 = "data(//xdsb:DocumentRequest)";
    	String xpath2 = "data(/dicom/attr[@tag='0040A375']/item//attr[@tag='00081155'])";
    	String testId = "numberDocumentRequest";
    	String testDescription = "The number of DocumentRequest shall have the same values of the number of attr with the tag '00081155'";
    	boolean check = checkForXpath($drl, 0 , xpath1, xpath2, XpathComparatorKind.EQSIZE); 
    	createNotif(testId, testDescription, myGlobalList, check);
end

