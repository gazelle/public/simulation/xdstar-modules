package net.ihe.gazelle.xdstar.xdsi.model;



import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.jboss.seam.annotations.Name;

@Entity
@Name("InitImgGatewayConfiguration")
@DiscriminatorValue("INITIMGGAT")
public class InitiatingImagingGatewayConfiguration extends ImgDocSourceConfigurationParent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InitiatingImagingGatewayConfiguration(){}
	
	public InitiatingImagingGatewayConfiguration(InitiatingImagingGatewayConfiguration cop){
		super(cop);
	}

}
