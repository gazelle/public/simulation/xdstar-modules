package net.ihe.gazelle.xdstar.xdsi.action;

import net.ihe.gazelle.dicom.evs.api.Dicom3tools;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.xdstar.drools.util.DroolsDocuments;
import net.ihe.gazelle.xdstar.drools.util.DroolsProcessor;
import net.ihe.gazelle.xdstar.validator.XSLTransformator;
import net.ihe.gazelle.xdstar.validator.ws.XDSMetadataValidator;
import net.ihe.gazelle.xdstar.xdsi.utils.DicomTool;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

/**
 * @author abderrazek boufahja
 */
@Name("XDSI69ValidatorManager")
@Scope(ScopeType.PAGE)
public class XDSI69ValidatorManager implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public static final String UTF_8 = "UTF-8";

    private static Logger log = LoggerFactory.getLogger(XDSI69ValidatorManager.class);

    private String generatedManifestXML;

    private File uploadedDicomFile;

    private String selectedRetrieveImagingDocumentSetRequest;

    private String ridsName;

    private String manifestName;

    private List<Notification> resultValidation;

    private boolean viewResultValidation;

    private String dicomValidationResult;

    private String ridsValidationResult;

    public String getRidsValidationResult() {
        return ridsValidationResult;
    }

    public void setRidsValidationResult(String ridsValidationResult) {
        this.ridsValidationResult = ridsValidationResult;
    }

    public String getDicomValidationResult() {
        return dicomValidationResult;
    }

    public void setDicomValidationResult(String dicomValidationResult) {
        this.dicomValidationResult = dicomValidationResult;
    }

    public boolean isViewResultValidation() {
        return viewResultValidation;
    }

    public void setViewResultValidation(boolean viewResultValidation) {
        this.viewResultValidation = viewResultValidation;
    }

    public List<Notification> getResultValidation() {
        return resultValidation;
    }

    public void setResultValidation(List<Notification> resultValidation) {
        this.resultValidation = resultValidation;
    }

    public String getGeneratedManifestXML() {
        return generatedManifestXML;
    }

    public void setGeneratedManifestXML(String generatedManifestXML) {
        this.generatedManifestXML = generatedManifestXML;
    }

    public String getSelectedRetrieveImagingDocumentSetRequest() {
        return selectedRetrieveImagingDocumentSetRequest;
    }

    public void setSelectedRetrieveImagingDocumentSetRequest(
            String selectedRetrieveImagingDocumentSetRequest) {
        this.selectedRetrieveImagingDocumentSetRequest = selectedRetrieveImagingDocumentSetRequest;
    }

    public String getRidsName() {
        return ridsName;
    }

    public void setRidsName(String ridsName) {
        this.ridsName = ridsName;
    }

    public String getManifestName() {
        return manifestName;
    }

    public void setManifestName(String manifestName) {
        this.manifestName = manifestName;
    }

    @Create
    public void initiate() {
        this.viewResultValidation = false;
    }

    public void uploadManifestListener(FileUploadEvent event) {
        generatedManifestXML = null;
        uploadedDicomFile = null;
        viewResultValidation = false;
        UploadedFile item = event.getUploadedFile();
        try {
            uploadedDicomFile = File.createTempFile("test", "xml");
            FileOutputStream fos = new FileOutputStream(uploadedDicomFile);
            if (item.getData() != null && item.getData().length > 0) {
                fos.write(item.getData());
                fos.close();
                try {
                    generatedManifestXML = DicomTool.dicom2xml(uploadedDicomFile.getPath());
                    this.manifestName = item.getName();
                    this.initiate();
                } catch (Exception e) {
                    String prob = "problem occure when trying to dump the dicom !";
                    log.info(prob);
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, prob);
                    this.generatedManifestXML = null;
                    this.manifestName = null;
                }
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The file is empty !");
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public void uploadXDSIListener(FileUploadEvent event) {
        selectedRetrieveImagingDocumentSetRequest = null;
        viewResultValidation = false;
        UploadedFile item = event.getUploadedFile();
        try {
            ByteArrayOutputStream fos = new ByteArrayOutputStream();
            if (item.getData() != null && item.getData().length > 0) {
                fos.write(item.getData());
                fos.close();
                this.selectedRetrieveImagingDocumentSetRequest = fos.toString();
                this.ridsName = item.getName();
                this.initiate();
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The file is empty !");
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public void validateSelectedRetrieveImagingDocumentSetRequestWithManifest() {
        try {
            DroolsDocuments drd = new DroolsDocuments();
            drd.setDocumentToValidate(this.selectedRetrieveImagingDocumentSetRequest);
            drd.getDocumentReferences().add(this.generatedManifestXML);
            this.resultValidation = DroolsProcessor.validateManifestContent(drd);
        } catch (Exception e) {
            this.resultValidation = new ArrayList<Notification>();
            Error err = new Error();
            String desc = "The tool was not able to perform the validation of the XDS-I.b request according to the KOS manifest.";
            desc += "Please verify that you have uploaded a valid XML XDS-I.b RetrieveImagingDocumentSetRequest, and a valid manifest ";
            err.setDescription(desc);
            err.setTest("validation");
            this.resultValidation.add(err);
        }
        this.viewResultValidation = true;
    }

    public String validationResult() {
        String res = "PASSED";
        if (this.resultValidation != null) {
            for (Notification not : this.resultValidation) {
                if (not instanceof Error) {
                    res = "FAILED";
                }
            }
        }
        return res;
    }

    public String classStyleOf(Notification notif) {
        if (notif instanceof Note) {
            return "Report";
        }
        if (notif instanceof Error) {
            return "Error";
        }
        if (notif instanceof Warning) {
            return "Warning";
        }
        return "Unknown";
    }

    public void validateCurrentDicomFile() {
        this.dicomValidationResult = null;
        if (uploadedDicomFile != null) {
            this.dicomValidationResult = (new Dicom3tools(
                    ApplicationConfiguration
                            .getValueOfVariable("dicom3tools_validator")))
                    .validate(this.uploadedDicomFile.getPath());
        }
    }

    public void validateSelectedRetrieveImagingDocumentSetRequest() {
        this.ridsValidationResult = null;
        if (this.selectedRetrieveImagingDocumentSetRequest != null) {
            try {
                this.ridsValidationResult = XDSMetadataValidator.validateDocument(this.selectedRetrieveImagingDocumentSetRequest,
                        "IHE XDS-I.b RAD-69 - request");
            } catch (Exception e) {
                log.info("not able to validate the document XDS-I.b : ", e);
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "not able to validate the document XDS-I.b");
            }
        }
    }

    public String getValidationDetailsAsHtml(String xmlDoc) {
        if (xmlDoc != null) {
            String inXslPath = ApplicationConfiguration.getValueOfVariable("validation_result_xslt");
            HashMap<String, String> hash = new HashMap<String, String>();
            hash.put("index", "1");
            return XSLTransformator.resultTransformation(xmlDoc, inXslPath, hash);
        }
        return null;
    }

    @SuppressWarnings("resource")
    public String sampleRetrieveImagingDocumentSetRequest() {
        URL is = XDSI69ValidatorManager.class.getClassLoader().getResource("rad69_ok.xml");
        try {
            String res = new Scanner(is.openStream(), UTF_8).useDelimiter("\\A").next();
            return res;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
