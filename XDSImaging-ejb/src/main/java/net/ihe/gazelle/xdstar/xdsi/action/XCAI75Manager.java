/*
 * Copyright 2012 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.xdsi.action;

import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.common.xua.SignatureException;
import net.ihe.gazelle.xdstar.core.MTOMBuilder;
import net.ihe.gazelle.xdstar.core.MTOMElement;
import net.ihe.gazelle.xdstar.core.soap.SOAPRequestBuilder;
import net.ihe.gazelle.xdstar.xdrsrc.tools.Pair;
import net.ihe.gazelle.xdstar.xdsi.model.RAD69Message;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *  
 * @author                	Abderrazek Boufahja / KEREVAL Rennes IHE development Project
 *
 */
@Name("XCAI75Manager")
@Scope(ScopeType.PAGE)
public class XCAI75Manager extends XDSI69Manager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static String TRANSACTION_TYPE = "XCAI75";

	@Logger
	private static Log log;
	
//	@Override
//	public void listAllConfigurations() {
//		List<RespondingImagingGatewayConfiguration> rr = RespondingImagingGatewayConfiguration.listAllSystemConfigurationsResp();
//		this.configurations = new ArrayList<ImgDocSourceConfiguration>();
//		if (rr != null) this.configurations.addAll(rr);
//	}
	

	@Override
	public List<AffinityDomain> listAllAffinityDomains() {
		return this.listAllAffinityDomains(XCAI75Manager.TRANSACTION_TYPE);
	}
	

	@Override
	public String getTheNameOfThePage() {
		String res = this.getNameOfTheAffinityDomainTransaction();
		if (res == null || res.equals("")) res = "RAD-75 : Cross Gateway Retrieve Imaging Document Set";
		return res;
	}
	
	public Pair<String, byte[]> buildMTOMRequest() throws ParserConfigurationException, SignatureException, JAXBException, IOException{
		String rr = this.getRetrieveImagingDocumentSetRequestTypeAsString(this.selectedRetrieveImagingDocumentSetRequest);
		String receiverUrl = this.selectedConfiguration.getUrl();
		String soapMessage = SOAPRequestBuilder.createSOAPMessage(rr, false, null, null, null, receiverUrl, "urn:ihe:rad:2011:CrossGatewayRetrieveImagingDocumentSet", false);
		
		List<MTOMElement> listDataToSend = new ArrayList<MTOMElement>();
		MTOMElement soap = new MTOMElement();
		soap.setContent(("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + soapMessage + "\r\n").getBytes(StandardCharsets.UTF_8));
		soap.setContentType(" application/xop+xml; charset=UTF-8; type=\"application/soap+xml\"");
		soap.setTransfertEncoding("binary");
		soap.setIndex(0);
		soap.setUuid(UUID.randomUUID().toString().replace("-", "").toUpperCase());
		listDataToSend.add(soap);
		
		byte[] body = MTOMBuilder.buildMTOM(listDataToSend);
		
		String boundaryUUID = MTOMBuilder.getBoundaryUUIDFromGeneratedMTOM(body);
		
		String boundary = "MIMEBoundaryurn_uuid_" + boundaryUUID;
		String boundaryStart = "0.urn:uuid:" + soap.getUuid() + "@ws.jboss.org";
		String contentTypeProperties = new String();
		contentTypeProperties = "multipart/related; ";
		contentTypeProperties = contentTypeProperties + "boundary=" + boundary + "; ";
		contentTypeProperties = contentTypeProperties + "type=\"application/xop+xml\"; ";
		contentTypeProperties = contentTypeProperties + "start=\"<" + boundaryStart + ">\"; ";
		contentTypeProperties = contentTypeProperties + "start-info=\"application/soap+xml\"; ";
		contentTypeProperties = contentTypeProperties + "action=\"urn:ihe:rad:2011:CrossGatewayRetrieveImagingDocumentSet\"";
		Pair<String, byte[]> res = new Pair<String, byte[]>(contentTypeProperties, body);
		return res;
	}
	
	public void sendMessage(){
		super.sendMessage();
		this.message.setMessageType("CrossGatewayRetrieveImagingDocumentSet");
		this.message = RAD69Message.storeMessage(this.message);
	}
	
	@Override
	public void init() {
		super.init();
		this.selectedTransaction = Transaction.GetTransactionByKeyword("RAD-75");
		this.selectedAffinityDomain = AffinityDomain.getAffinityDomainByKeyword("IHE_XCA-I");
		this.initFromMenu = true;
	}
}
