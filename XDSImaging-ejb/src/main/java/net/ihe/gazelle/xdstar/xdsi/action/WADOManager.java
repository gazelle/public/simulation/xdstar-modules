package net.ihe.gazelle.xdstar.xdsi.action;

import net.ihe.gazelle.dicom.AttrType;
import net.ihe.gazelle.dicom.DicomType;
import net.ihe.gazelle.dicom.ItemType;
import net.ihe.gazelle.dicom.evs.api.Dicom3tools;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.xdstar.common.action.CommonSimulatorManager;
import net.ihe.gazelle.xdstar.common.model.ContentType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import net.ihe.gazelle.xdstar.comon.util.ConfigurationFilter;
import net.ihe.gazelle.xdstar.xdsi.model.*;
import org.apache.http.client.utils.URIBuilder;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MultivaluedMap;
import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Name("WADOManager")
@Scope(ScopeType.PAGE)
public class WADOManager extends CommonSimulatorManager<ImgDocSourceConfiguration, WADOMessage> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private final static String TRANSACTION_TYPE = "WADO";

    private static Logger log = LoggerFactory.getLogger(WADOManager.class);

    private String validationResult;

    private String requestType;

    private String studyUID;

    private String seriesUID;

    private String objectUID;

    private String contentType;

    private String charset;

    private Boolean anonymize;

    private String annotation;

    private String rows;

    private String columns;

    private String region;

    private String windowCenter;

    private String windowWidth;

    private String frameNumber;

    private Integer imageQuality;

    private String presentationUID;

    private String presentationSeriesUID;

    private String transferSyntax;

    private DicomType generatedDicom = null;

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    private static Map<String, Object> executeWadoRequest(String request) throws Exception {
        Map<String, Object> res = new LinkedHashMap<String, Object>();
        ClientRequest req = new ClientRequest(request);
        ClientResponse<String> response = req.get(String.class);
        if (response != null) {
            MultivaluedMap<String, String> heads = response.getHeaders();
            if (heads != null) {
                for (String key : heads.keySet()) {
                    res.put(key, heads.get(key).toString());
                }
            }
            res.put("Status", String.valueOf(response.getStatus()));
            if (response.getResponseStatus() != null) {
                res.put("ResponseStatus", response.getResponseStatus().toString());
            } else {
                res.put("ResponseStatus", null);
            }
            if (response.getStatus() == 200) {
                res.put("Base64Content", getFileAsByteArrayOutputStream(request));
            }
        }
        return res;
    }

    private static ByteArrayOutputStream getFileAsByteArrayOutputStream(String request) throws Exception {
        URL req = new URL(request);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream urlDownload = req.openStream();
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = urlDownload.read(buffer)) != -1) {
            baos.write(buffer, 0, bytesRead);
        }
        baos.close();
        urlDownload.close();
        return baos;
    }

    public static void exportToFile(String contentType, byte[] bytes, String fileNameDestination, boolean inline) {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse)
                    context.getExternalContext().getResponse();
            if (contentType != null) {
                response.setContentType(contentType);
            }
            if (inline) {
                response.setHeader("Content-Disposition", "inline;filename=" + fileNameDestination);
            } else {
                response.setHeader("Content-Disposition", "attachment;filename=" + fileNameDestination);
            }

            ServletOutputStream servletOutputStream;
            servletOutputStream = response.getOutputStream();
            servletOutputStream.write(bytes);
            servletOutputStream.flush();
            servletOutputStream.close();

            context.responseComplete();
        } catch (IOException e) {
            log.error("problem to process exportToFile", e);
        }
    }

    public static void main(String[] args) throws Exception {
        Map<String, Object> rr = executeWadoRequest(" http://gazelle.ihe" +
                ".net/wado?requestType=WADO&studyUID=1.3.12.2.1107.5.5.5.1170160.20070418083655.4515&seriesUID" +
                "=1.3.12.2.1107.5.5.5.1170160.20070418083655.4516&objectUID=1.3.12.2.1107.5.5.5.1170160.20070418083805.4536&contentType=image/jpeg");
        ByteArrayOutputStream kk = (ByteArrayOutputStream) rr.get("Content");
        File fileToWrite = new File("test.jpeg");
        FileOutputStream fos = new FileOutputStream(fileToWrite);
        System.out.println(kk);
        fos.write(kk.toByteArray());
        fos.close();
        //		for (String string : rr.keySet()) {
        //			System.out.println(string + "#" + rr.get(string));
        //		}
        //String res = executeWadoRequest("http://gazelle.ihe
        // .net/wado?requestType=WADO&studyUID=1.3.12.2.1107.5.5.5.1170160.20070418083655.4515&seriesUID
        // =1.3.12.2.1107.5.5.5.1170160.20070418083655.4516&objectUID=1.3.12.2.1107.5.5.5.1170160.20070418083805.4536");
        //System.out.println(res);
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getStudyUID() {
        return studyUID;
    }

    public void setStudyUID(String studyUID) {
        this.studyUID = studyUID;
    }

    public String getSeriesUID() {
        return seriesUID;
    }

    public void setSeriesUID(String seriesUID) {
        this.seriesUID = seriesUID;
    }

    public String getObjectUID() {
        return objectUID;
    }

    public void setObjectUID(String objectUID) {
        this.objectUID = objectUID;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public Boolean getAnonymize() {
        return anonymize;
    }

    public void setAnonymize(Boolean anonymize) {
        this.anonymize = anonymize;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public String getRows() {
        return rows;
    }

    public void setRows(String rows) {
        this.rows = rows;
    }

    public String getColumns() {
        return columns;
    }

    public void setColumns(String columns) {
        this.columns = columns;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getWindowCenter() {
        return windowCenter;
    }

    public void setWindowCenter(String windowCenter) {
        this.windowCenter = windowCenter;
    }

    public String getWindowWidth() {
        return windowWidth;
    }

    public void setWindowWidth(String windowWidth) {
        this.windowWidth = windowWidth;
    }

    public String getFrameNumber() {
        return frameNumber;
    }

    public void setFrameNumber(String frameNumber) {
        this.frameNumber = frameNumber;
    }

    public Integer getImageQuality() {
        return imageQuality;
    }

    public void setImageQuality(Integer imageQuality) {
        this.imageQuality = imageQuality;
    }

    public String getPresentationUID() {
        return presentationUID;
    }

    public void setPresentationUID(String presentationUID) {
        this.presentationUID = presentationUID;
    }

    public String getPresentationSeriesUID() {
        return presentationSeriesUID;
    }

    public void setPresentationSeriesUID(String presentationSeriesUID) {
        this.presentationSeriesUID = presentationSeriesUID;
    }

    public String getTransferSyntax() {
        return transferSyntax;
    }

    public void setTransferSyntax(String transferSyntax) {
        this.transferSyntax = transferSyntax;
    }

    public String getValidationResult() {
        return validationResult;
    }

    public void setValidationResult(String validationResult) {
        this.validationResult = validationResult;
    }

    @Override
    public void listAllConfigurations() {
        ImgDocSourceConfigurationQuery qq = new ImgDocSourceConfigurationQuery();
        qq.listUsages().affinity().eq(this.selectedAffinityDomain);
        qq.listUsages().transaction().eq(this.selectedTransaction);
        qq.name().order(true);
        ConfigurationFilter.filterConfigurationQueryAccodingToUserCredential(qq);
        configurations = qq.getList();
    }

    @Override
    public void updateSpecificClient() {
        this.initRequestParam();
    }

    @Override
    public void init() {
        super.init();
        if (this.selectedTransaction == null || this.selectedAffinityDomain == null) {
            this.selectedTransaction = Transaction.GetTransactionByKeyword("RAD-55");
            this.selectedAffinityDomain = AffinityDomain.getAffinityDomainByKeyword("IHE_XDS-I.b");
        } else {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            this.selectedTransaction = em.find(Transaction.class, this.selectedTransaction.getId());
            this.selectedAffinityDomain = em.find(AffinityDomain.class, this.selectedAffinityDomain.getId());
        }
        this.initFromMenu = true;
    }

    @Override
    public List<AffinityDomain> listAllAffinityDomains() {
        return this.listAllAffinityDomains(WADOManager.TRANSACTION_TYPE);
    }

    @Override
    public String getTheNameOfThePage() {
        String res = this.getNameOfTheAffinityDomainTransaction();
        if (res == null || res.equals("")) {
            res = "RAD-55 : WADO Retrieve";
        }
        return res;
    }

    public void initRequestParam() {
        this.requestType = "WADO";
        this.contentType = "application/dicom";
        this.studyUID = null;
        this.seriesUID = null;
        this.objectUID = null;
        this.charset = null;
        this.anonymize = null;
        this.annotation = null;
        this.rows = null;
        this.columns = null;
        this.region = null;
        this.windowCenter = null;
        this.windowWidth = null;
        this.frameNumber = null;
        this.imageQuality = null;
        this.presentationUID = null;
        this.presentationSeriesUID = null;
        this.transferSyntax = null;
    }

    public String previewMessage() {
        return this.createMessage();
    }

    public void sendMessage() {
        message = new WADOMessage();
        String req = createMessage();
        String resp = null;
        try {
            Map<String, Object> rr = executeWadoRequest(req);
            if (rr != null) {
                resp = "<Response>\n";
                for (String key : rr.keySet()) {
                    if (key.equals("Base64Content")) {
                        if (rr.get(key) != null) {
                            message.setWadoFile(new WADODownloadedFile());
                            String fileType = (String) rr.get("Content-Type");
                            if (fileType != null) {
                                if (fileType.matches("\\[.*\\]")) {
                                    fileType = fileType.substring(1, fileType.length() - 1);
                                }
                            } else {
                                fileType = this.contentType;
                            }
                            message.setWadoFile(WADODownloadedFile.saveFile(null, fileType));
                            resp = resp + "\t<FileName>" + this.getOutputLinkFile(message.getWadoFile()) +
                                    "</FileName>\n";
                            File fileToWrite = new File(this.message.getWadoFile().getPath());
                            FileOutputStream fos = new FileOutputStream(fileToWrite);
                            ByteArrayOutputStream kk = (ByteArrayOutputStream) rr.get(key);
                            fos.write(kk.toByteArray());
                            fos.close();
                        }
                        continue;
                    }
                    resp = resp + "\t<" + key + ">" + rr.get(key) + "</" + key + ">\n";
                }
                resp = resp + "</Response>";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        message.setTimeStamp(new Date());
        message.setConfiguration(selectedConfiguration);
        message.setTransaction(selectedTransaction);
        message.setAffinityDomain(this.selectedAffinityDomain);
        message.setContentType(ContentType.HTTP);
        message.setGazelleDriven(false);
        message.setMessageType("WADORetrieve");

        MetadataMessage sent = new MetadataMessage();
        sent.setMessageContent(req);
        sent.setMessageType(MetadataMessageType.HTTP_GET_REQUEST);

        MetadataMessage received = new MetadataMessage();
        received.setMessageContent(resp);
        received.setMessageType(MetadataMessageType.HTTP_GET_RESPONSE);

        message.setResponseCode(extractResponseCode(resp));

        message.setSentMessageContent(sent);
        message.setReceivedMessageContent(received);
        message = WADOMessage.storeMessage(message);
        Contexts.getSessionContext().set("messagesToDisplay", message);
        displayResultPanel = true;
    }

    private Integer extractResponseCode(String resp) {
        if (resp != null) {
            Pattern pat = Pattern.compile(".*<Status>(.*?)</Status>.*");
            Matcher m = pat.matcher(resp);
            if (m.find()) {
                String content = m.group(1);
                try {
                    return Integer.valueOf(content);
                } catch (NumberFormatException ne) {
                    return null;
                }
            }
        }
        return null;
    }

    private String createMessage() {
        URIBuilder uribuilder = new URIBuilder();
        String res = "";
        if (this.selectedConfiguration != null) {
            uribuilder.setPath(this.selectedConfiguration.getUrl().trim());
        }
        if ((this.getRequestType() != null) && (!this.getRequestType().equals(""))) {
            uribuilder.addParameter("requestType", this.getRequestType());
        } else {
            uribuilder.addParameter("requestType", "WADO");
        }

        if ((this.getStudyUID() != null) && (!this.getStudyUID().equals(""))) {
            uribuilder.addParameter("studyUID", this.getStudyUID());
        }

        if ((this.getSeriesUID() != null) && (!this.getSeriesUID().equals(""))) {
            uribuilder.addParameter("seriesUID", this.getSeriesUID());
        }

        if ((this.getObjectUID() != null) && (!this.getObjectUID().equals(""))) {
            uribuilder.addParameter("objectUID", this.getObjectUID());
        }

        if ((this.getContentType() != null) && (!this.getContentType().equals(""))) {
            uribuilder.addParameter("contentType", this.getContentType());
        }

        if ((this.getCharset() != null) && (!this.getCharset().equals(""))) {
            uribuilder.addParameter("charset", this.getCharset());
        }

        if ((this.getAnonymize() != null) && (this.getAnonymize())) {
            uribuilder.addParameter("anonymize", "yes");
        }

        if ((this.getAnnotation() != null) && (!this.getAnnotation().equals(""))) {
            uribuilder.addParameter("annotation", this.getAnnotation());
        }

        if ((this.getRows() != null) && (!this.getRows().equals(""))) {
            uribuilder.addParameter("rows", this.getRows());
        }

        if ((this.getColumns() != null) && (!this.getColumns().equals(""))) {
            uribuilder.addParameter("columns", this.getColumns());
        }

        if ((this.getRegion() != null) && (!this.getRegion().equals(""))) {
            uribuilder.addParameter("region", this.getRegion());
        }

        if ((this.getWindowCenter() != null) && (!this.getWindowCenter().equals(""))) {
            uribuilder.addParameter("windowCenter", this.getWindowCenter());
        }

        if ((this.getWindowWidth() != null) && (!this.getWindowWidth().equals(""))) {
            res = res + "&windowWidth=" + this.getWindowWidth();
            uribuilder.addParameter("windowWidth", this.getWindowWidth());
        }

        if ((this.getFrameNumber() != null) && (!this.getFrameNumber().equals(""))) {
            uribuilder.addParameter("frameNumber", this.getFrameNumber());
        }

        if (this.getImageQuality() != null) {
            uribuilder.addParameter("imageQuality", String.valueOf(this.getImageQuality()));
        }

        if ((this.getPresentationUID() != null) && (!this.getPresentationUID().equals(""))) {
            uribuilder.addParameter("presentationUID", this.getPresentationUID());
        }

        if ((this.getPresentationSeriesUID() != null) && (!this.getPresentationSeriesUID().equals(""))) {
            uribuilder.addParameter("presentationSeriesUID", this.getPresentationSeriesUID());
        }

        if ((this.getTransferSyntax() != null) && (!this.getTransferSyntax().equals(""))) {
            String tt = this.getTransferSyntax();
            if (tt.contains(":")) {
                tt = tt.substring(0, tt.indexOf(":"));
            }
            uribuilder.addParameter("transferSyntax", tt);
        }
        try {
            res = uribuilder.build().toString();
        } catch (URISyntaxException e) {
            res = "The simulator was not able to build the URL. Please Check the configuration of your system. The message is : " + e.getMessage();
            log.error(res);
        }
        return res;
    }

    public List<String> getContentTypeList() {
        List<String> ll = new ArrayList<String>();
        ll.add("application/dicom");
        ll.add("image/jpeg");
        ll.add("image/gif");
        ll.add("image/png");
        ll.add("image/jp2");
        ll.add("video/mpeg");
        ll.add("image/gif");
        ll.add("text/plain");
        ll.add("text/html");
        ll.add("application/pdf");
        ll.add("text/rtf");
        ll.add("application/x-hl7-cda-level-one+xml");
        return ll;
    }

    public Set<String> charsetList() {
        Set<String> ll = new TreeSet<String>();
        ll.add("big5");
        ll.add("euc-kr");
        ll.add("iso-8859-1");
        ll.add("iso-8859-2");
        ll.add("iso-8859-3");
        ll.add("iso-8859-4");
        ll.add("iso-8859-5");
        ll.add("iso-8859-6");
        ll.add("iso-8859-7");
        ll.add("iso-8859-8");
        ll.add("koi8-r");
        ll.add("shift-jis");
        ll.add("x-euc");
        ll.add("utf-8");
        ll.add("utf-16");
        ll.add("windows-1250");
        ll.add("windows-1251");
        ll.add("windows-1252");
        ll.add("windows-1253");
        ll.add("windows-1254");
        ll.add("windows-1255");
        ll.add("windows-1256");
        ll.add("windows-1257");
        ll.add("windows-1258");
        ll.add("windows-874");
        ll.add("ASMO-708");
        ll.add("DOS-720");
        ll.add("iso-8859-6");
        ll.add("x-mac-arabic");
        ll.add("windows-1256");
        ll.add("ibm775");
        ll.add("iso-8859-4");
        ll.add("windows-1257");
        ll.add("ibm852");
        ll.add("iso-8859-2");
        ll.add("x-mac-ce");
        ll.add("windows-1250");
        ll.add("EUC-CN");
        ll.add("gb2312");
        ll.add("us-ascii");
        ll.add("macintosh");
        ll.add("Windows-1252");
        ll.add("x-IA5-Norwegian");
        ll.add("x-IA5-Swedish");
        ll.add("x-mac-turkish");
        ll.add("iso-8859-9");
        ll.add("x-EBCDIC-Italy");
        ll.add("x-iscii-ka");
        return ll;
    }

    public List<String> annotationList() {
        List<String> ll = new ArrayList<String>();
        ll.add("patient");
        ll.add("technique");
        ll.add("patient,technique");
        ll.add("technique,patient");
        return ll;
    }

    public Set<String> transferSyntaxUIDList() {
        TransferSyntaxUIDQuery tt = new TransferSyntaxUIDQuery();
        List<TransferSyntaxUID> res = tt.getList();
        TreeSet<String> ll = new TreeSet<String>();
        for (TransferSyntaxUID tr : res) {
            ll.add(tr.getUid() + "::" + tr.getName());
        }
        return ll;
    }

    public List<TransferSyntaxUID> transferSyntaxUIDListAsObjects() {
        TransferSyntaxUIDQuery tt = new TransferSyntaxUIDQuery();
        return tt.getList();
    }

    public String getOutputLinkFile(WADODownloadedFile file) {
        if (file != null) {
            return ApplicationConfiguration.getValueOfVariable("application_url") + "/xdsi/download.seam?fileName=" + file.getName();
        }
        return null;
    }

    public File getCurrentFileAsImage() {
        if ((this.message != null) && (this.message.getWadoFile() != null)
                && (this.message.getWadoFile().getPath() != null)) {
            File ff = new File(this.message.getWadoFile().getPath());
            if (ff.exists()) {
                return ff;
            }
        }
        return null;
    }

    public void validateCurrentDicomFile() {
        if ((this.message != null) && (this.message.getWadoFile() != null)
                && (this.message.getWadoFile().getPath() != null) &&
                this.message.getWadoFile().getType().equals("application/dicom")) {
            this.validationResult = new Dicom3tools(ApplicationConfiguration.getValueOfVariable("dicom3tools_validator")).validate(this.message
                    .getWadoFile().getPath());
        }
    }

    @SuppressWarnings("resource")
    public void downloadRelatedWadoFile() {
        String fileName = getFileName();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(new File(ApplicationConfiguration.getValueOfVariable("registered_files_directory") +
                    "/" + fileName));
        } catch (FileNotFoundException e) {
            String res = "Enable to find the related file";
            exportToFile(null, res.getBytes(), "error.txt", true);
            return;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int bytesRead;
        try {
            while ((bytesRead = fis.read(buffer)) != -1) {
                baos.write(buffer, 0, bytesRead);
            }
        } catch (IOException e) {
            String res = "Enable to find the related file";
            exportToFile(null, res.getBytes(), "error.txt", true);
            return;
        }
        try {
            baos.close();
        } catch (IOException e) {
            String res = "Enable to find the related file";
            exportToFile(null, res.getBytes(), "error.txt", true);
            return;
        }
        exportToFile(null, baos.toByteArray(), fileName, true);
    }

    private String getFileName() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String pageid = params.get("fileName");
        return pageid;
    }

    public void uploadListener(FileUploadEvent event) {
        generatedDicom = KOSUploadUtil.uploadListenerAndGenerateDicomType(event);
    }

    public void generateRequestFromManifest() {
        if (this.generatedDicom != null) {
            this.initRequestParam();
            for (AttrType attr : this.generatedDicom.getAttr()) {
                if (attr.getTag() != null && attr.getTag().toLowerCase().equals("0040a375")) {
                    for (ItemType item : attr.getItem()) {
                        this.studyUID = DicomTypeProcessor.getStudyInstanceUIDFromItem(item);
                        AttrType studyRequestAttr = DicomTypeProcessor.getAttrTypeOfReferencedSeriesSequence(item);
                        if (studyRequestAttr != null) {
                            for (ItemType itmSeriesRequest : studyRequestAttr.getItem()) {
                                this.seriesUID = DicomTypeProcessor.getSeriesInstanceUIDFromItem(itmSeriesRequest);
                                AttrType docRequestAttr = DicomTypeProcessor.getAttrTypeOfReferencedSopSequence(itmSeriesRequest);
                                if (docRequestAttr != null) {
                                    for (ItemType itm : docRequestAttr.getItem()) {
                                        this.objectUID = DicomTypeProcessor.getDocumentUniqueIdFromItem(itm);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "The attributes of the request are generated from the manifest.");
    }

}
