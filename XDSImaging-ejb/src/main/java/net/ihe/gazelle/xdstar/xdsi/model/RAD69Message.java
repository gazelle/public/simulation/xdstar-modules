/*
 * Copyright 2011 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.xdsi.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.UniqueConstraint;

import net.ihe.gazelle.xdstar.common.model.AbstractMessage;
import net.ihe.gazelle.xdstar.xdrsrc.model.PnRRegisteredFile;

import org.jboss.seam.annotations.Name;


/**
 * 
 * @author abderrazek boufahja
 *
 */
@Entity
@Name("rad69Message")
@DiscriminatorValue("RAD69")
public class RAD69Message extends AbstractMessage implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManyToMany
	@JoinTable(name="message_registeres_files", 
			joinColumns=@JoinColumn(name="message_id"), 
			inverseJoinColumns=@JoinColumn(name="registered_file_id"),
			uniqueConstraints=@UniqueConstraint(columnNames={"message_id", "registered_file_id"}))
	private List<PnRRegisteredFile> registeredFiles;

	public void setRegisteredFiles(List<PnRRegisteredFile> registeredFiles) {
		this.registeredFiles = registeredFiles;
	}

	public List<PnRRegisteredFile> getRegisteredFiles() {
		return registeredFiles;
	}
	
}
