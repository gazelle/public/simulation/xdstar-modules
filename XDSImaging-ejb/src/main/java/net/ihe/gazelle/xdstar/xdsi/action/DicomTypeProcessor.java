package net.ihe.gazelle.xdstar.xdsi.action;

import net.ihe.gazelle.dicom.AttrType;
import net.ihe.gazelle.dicom.ItemType;

public class DicomTypeProcessor {
	
	public static AttrType getAttrTypeOfReferencedSopSequence(ItemType item){
		for (AttrType attr : item.getAttr()) {
			if (attr.getTag().toLowerCase().equals("00081199")){
				return attr;
			}
		}
		return null;
	}
	
	public static AttrType getAttrTypeOfReferencedSeriesSequence(ItemType item){
		for (AttrType attr : item.getAttr()) {
			if (attr.getTag().toLowerCase().equals("00081115")){
				return attr;
			}
		}
		return null;
	}
	
	private static String getValueFromItemByTag(ItemType item, String tag){
		for (AttrType attr : item.getAttr()) {
			if (attr.getTag().toLowerCase().equals(tag)){
				return attr.getListStringValues().get(0).trim();
			}
		}
		return null;
	}
	
	public static String getDocumentUniqueIdFromItem(ItemType item){
		return getValueFromItemByTag(item, "00081155");
	}
	
	public static String getRepositoryUniqueIdFromItem(ItemType item, String originalRepositoryId){
		String res = getValueFromItemByTag(item, "0040e011");
		if (res == null || res.trim().equals("")){
			res = originalRepositoryId;
		}
		return res;
	}
	
	public static String getSeriesInstanceUIDFromItem(ItemType item){
		return getValueFromItemByTag(item, "0020000e");
	}
	
	public static String getStudyInstanceUIDFromItem(ItemType item){
		return getValueFromItemByTag(item, "0020000d");
	}

}
