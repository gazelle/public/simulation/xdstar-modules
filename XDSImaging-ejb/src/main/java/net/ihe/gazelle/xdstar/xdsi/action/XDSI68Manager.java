package net.ihe.gazelle.xdstar.xdsi.action;

import net.ihe.gazelle.dicom.AttrType;
import net.ihe.gazelle.dicom.DicomType;
import net.ihe.gazelle.dicom.ItemType;
import net.ihe.gazelle.dicom.evs.api.Dicom3tools;
import net.ihe.gazelle.edit.FileReadWrite;
import net.ihe.gazelle.metadata.common.RIMGenerator;
import net.ihe.gazelle.metadata.model.SlotMetadata;
import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.test.TransformDicom;
import net.ihe.gazelle.xdstar.comon.util.ConfigurationFilter;
import net.ihe.gazelle.xdstar.comon.util.EVSClientValidator;
import net.ihe.gazelle.xdstar.comon.util.Util;
import net.ihe.gazelle.xdstar.conf.model.RepositoryConfigurationQuery;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.action.PnRIHEManager;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.action.XDSCommonModule;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.PNRMessageType;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSDocumentEntry;
import net.ihe.gazelle.xdstar.xdrsrc.model.OID;
import net.ihe.gazelle.xdstar.xdrsrc.model.XDRMessage;
import net.ihe.gazelle.xdstar.xdsi.utils.DicomTool;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;
import org.jboss.seam.util.Base64;
import org.richfaces.event.FileUploadEvent;

import javax.persistence.EntityManager;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Name("XDSI68Manager")
@Scope(ScopeType.PAGE)
public class XDSI68Manager extends PnRIHEManager implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String TEXT_XML = "text/xml";
	private static final String APPLICATION_PDF = "application/pdf";
	private static final String APPLICATION_DICOM = "application/dicom";
	private static final String CDA_VALIDATOR_NAME = "IHE - RAD - CDA document wrapper (XDS-I.b)";
	private static final String TRANSACTION_TYPE = "RAD68";
	@Logger
	private static Log log;
	private String validationResult;
	
	public String getValidationResult() {
		return validationResult;
	}

	public void setValidationResult(String validationResult) {
		this.validationResult = validationResult;
	}

	@Override
	public String getTheNameOfThePage() {
		String res = this.getNameOfTheAffinityDomainTransaction();
		if (res == null || res.isEmpty()) res = "Provide and Register Imaging Document Set - MTOM/XOP";
		return res;
	}
	
	@Override
	public void listAllConfigurations() {
		RepositoryConfigurationQuery rc = new RepositoryConfigurationQuery();
		if (selectedAffinityDomain != null) rc.listUsages().affinity().eq(selectedAffinityDomain);
		if (selectedTransaction != null) rc.listUsages().transaction().eq(selectedTransaction);
		rc.name().order(true);
		ConfigurationFilter.filterConfigurationQueryAccodingToUserCredential(rc);
		configurations = rc.getList();
	}

	@Override
	public void updateSpecificClient() {
//		this.submissionSet = this.generateXDSSubmissionSet();
//		this.selectedXDSDocumentEntry = null;
//		this.selectedXDSFolder = null;
//		this.selectedXDSFolder = null;
//		this.message = new XDRMessage();
//		this.selectedTransaction = Transaction.GetTransactionByKeyword("RAD-68");
//		this.selectedMessageType = PNRMessageType.NEW_SUBMISSION;
//		this.editXDSSubmissionSet();
//		this.initListOptionalMetadata();
	}

	@Override
	public List<AffinityDomain> listAllAffinityDomains() {
		return listAllAffinityDomains(TRANSACTION_TYPE);
	}
	
	public void init(){
		super.init();
		if (this.selectedTransaction == null ||  this.selectedAffinityDomain == null) {
			this.selectedTransaction = Transaction.GetTransactionByKeyword("RAD-68");
			this.selectedAffinityDomain = AffinityDomain.getAffinityDomainByKeyword("IHE_XDS-I.b");
		} else {
			EntityManager em = (EntityManager) Component.getInstance("entityManager");
			this.selectedTransaction = em.find(Transaction.class, this.selectedTransaction.getId());
			this.selectedAffinityDomain = em.find(AffinityDomain.class, this.selectedAffinityDomain.getId());
		}
		this.initFromMenu = true;
		Contexts.getSessionContext().set("selectedConfiguration", this.selectedConfiguration);
		Contexts.getSessionContext().set("selectedAffinityDomain", this.selectedAffinityDomain);
		Contexts.getSessionContext().set("selectedTransaction", this.selectedTransaction);
		this.submissionSet = this.generateXDSSubmissionSet();
		this.selectedXDSDocumentEntry = null;
		this.selectedXDSFolder = null;
		setMessage(new XDRMessage());
		this.selectedMessageType = PNRMessageType.NEW_SUBMISSION;
		this.editXDSSubmissionSet();
		this.initListOptionalMetadata();
	}
	
	@Override
	public void uploadListener(FileUploadEvent event) {
		super.uploadListener(event);
		assert selectedClassificationType != null;
		if (!TEXT_XML.equals(this.selectedXDSDocumentEntry.getMimeType())
						&& !APPLICATION_PDF.equals(this.selectedXDSDocumentEntry.getMimeType())) {
				this.selectedXDSDocumentEntry.setMimeType(APPLICATION_DICOM);
		}
		
		DicomType dicom = null;

		if (APPLICATION_DICOM.equals(this.selectedXDSDocumentEntry.getMimeType())) {
			try {
				String dump = DicomTool.dicom2xml(this.selectedXDSDocumentEntry.getRelatedFile().getPath());
				dicom = TransformDicom.load(new ByteArrayInputStream(dump.getBytes()));
				updatePatientID(dicom);
				updateStudyInstanceUID(dicom);
			} catch (Exception e) {
				log.info("problem occure when trying to dump the dicom !");
			}
		}
		
		this.updateFormatCode(this.selectedXDSDocumentEntry);
		
		try {
			this.updateCreationTime(this.selectedXDSDocumentEntry, dicom);
		} catch (Exception e) {
			log.info("problem occure when trying to update the creationTime !");
		}
		
		try {
			this.updateSourcePatientInfo(this.selectedXDSDocumentEntry, dicom);
		} catch (Exception e) {
			log.info("problem occure when trying to update the sourcePatientInfo !");
		}
		
		try {
			updateDocumentUniqueId(this.selectedXDSDocumentEntry, dicom);
		} catch (Exception e) {
			log.info("problem occure when trying to update the documentUniqueId !");
		}
		
		try {
			updateServiceStartTime(this.selectedXDSDocumentEntry, dicom);
		} catch (Exception e) {
			log.info("problem occure when trying to update the serviceStartTime !");
		}
		
	}

	private void updateServiceStartTime(XDSDocumentEntry docEntry, DicomType dicom) {
		if (APPLICATION_DICOM.equals(this.selectedXDSDocumentEntry.getMimeType()) && dicom != null){
			AttrType date = getAttributeByTag(dicom, "00080020");
			AttrType day = getAttributeByTag(dicom, "00080030");
			if (date != null && !date.getListStringValues().isEmpty()) {
				String serviceStartTime = date.getListStringValues().get(0).trim();
				if (day != null && !date.getListStringValues().isEmpty()) {
					serviceStartTime = serviceStartTime + day.getListStringValues().get(0).trim().substring(0, 6);
				}
	
				for (SlotType1 slot : docEntry.getSlot()) {
					if ("serviceStartTime".equals(slot.getName())) {
						List<String> valueList = new ArrayList<>();
						valueList.add(serviceStartTime);
						slot.getValueList().setValue(valueList);
					}
				}
			}
		} else {
			for (SlotType1 slot : docEntry.getSlot()) {
				if ("serviceStartTime".equals(slot.getName())) {
					List<String> valueList = new ArrayList<>();
					slot.getValueList().setValue(valueList);
				}
			}
		}
	}
	
	private void updateDocumentUniqueId(XDSDocumentEntry docEntry, DicomType dicom){
		if (APPLICATION_DICOM.equals(docEntry.getMimeType()) && (dicom != null)){
			AttrType uid = getAttributeByTag(dicom, "00080018");
			if (uid != null && !uid.getListStringValues().isEmpty()) {
				for (ExternalIdentifierType ext : docEntry.getExternalIdentifier()) {
					if (ext.getIdentificationScheme() != null && "urn:uuid:2e82c1f6-a085-4c72-9da3-8640a32e42ab".equals(ext.getIdentificationScheme())) {
						ext.setValue(StringUtils.join(uid.getListStringValues().iterator(), " "));
					}
				}
			}
		} else {
			for (ExternalIdentifierType ext : docEntry.getExternalIdentifier()) {
				if (ext.getIdentificationScheme() != null && "urn:uuid:2e82c1f6-a085-4c72-9da3-8640a32e42ab".equals(ext.getIdentificationScheme())) {
					ext.setValue(OID.getNewDocumentOid());
				}
			}
		}
	}
	
	private void updateSourcePatientInfo(XDSDocumentEntry docEntry, DicomType dicom) {
		if (APPLICATION_DICOM.equals(this.selectedXDSDocumentEntry.getMimeType()) && (dicom != null)) {
			AttrType patientName = getAttributeByTag(dicom, "00100010");
			AttrType patientBirthdate = getAttributeByTag(dicom, "00100030");
			AttrType patientSex = getAttributeByTag(dicom, "00100040");
			if ((patientName != null && !patientName.getListStringValues().isEmpty())
					|| (patientBirthdate != null && !patientBirthdate.getListStringValues().isEmpty())
					|| (patientSex != null && !patientSex.getListStringValues().isEmpty())) {
				for (SlotType1 slot : docEntry.getSlot()) {
					if ("sourcePatientInfo".equals(slot.getName())){
						List<String> valueList = new ArrayList<>();
						if (patientName != null && !patientName.getListStringValues().isEmpty()) {
							valueList.add("PID-5|" + StringUtils.join(patientName.getListStringValues().iterator(), " "));
						}
						if (patientBirthdate != null && !patientBirthdate.getListStringValues().isEmpty()) {
							valueList.add("PID-7|" + StringUtils.join(patientBirthdate.getListStringValues().iterator(), " "));
						}
						if (patientSex != null && !patientSex.getListStringValues().isEmpty()) {
							valueList.add("PID-8|" + StringUtils.join(patientSex.getListStringValues().iterator(), " "));
						}
						slot.getValueList().setValue(valueList);
					}
				}
			}
		} else {
			for (SlotType1 slot : docEntry.getSlot()) {
				if ("sourcePatientInfo".equals(slot.getName())){
					List<String> valueList = new ArrayList<>();
					slot.getValueList().setValue(valueList);
				}
			}
		}
	}
	
	private void updateCreationTime(XDSDocumentEntry docEntry, DicomType dicom) {
		if (APPLICATION_DICOM.equals(this.selectedXDSDocumentEntry.getMimeType()) && dicom != null) {
			AttrType date = getAttributeByTag(dicom, "00080012");
			AttrType day = getAttributeByTag(dicom, "00080013");
			if ((date != null) && (!date.getListStringValues().isEmpty())) {
				String creationTime = date.getListStringValues().get(0).trim();
				if ((day != null) && (!date.getListStringValues().isEmpty())) {
					creationTime = creationTime + day.getListStringValues().get(0).trim().substring(0, 6);
				}
	
				for (SlotType1 slot : docEntry.getSlot()) {
					if ("creationTime".equals(slot.getName())) {
						List<String> valueList = new ArrayList<>();
						valueList.add(creationTime);
						slot.getValueList().setValue(valueList);
					}
				}
			}
		} else {
			for (SlotType1 slot : docEntry.getSlot()) {
				if ("creationTime".equals(slot.getName())) {
					List<String> valueList = new ArrayList<>();
					valueList.add(Util.getDate());
					slot.getValueList().setValue(valueList);
				}
			}
		}
	}
	
	private void updateFormatCode(XDSDocumentEntry docEntry) {
		for (ClassificationType cl : docEntry.getClassification()) {
			if (cl.getClassificationScheme() != null && "urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d".equals(cl.getClassificationScheme())) {
				if (APPLICATION_DICOM.equals(docEntry.getMimeType())) {
					XDSCommonModule.updateClassificationByValueSet(cl, "1.2.840.10008.5.1.4.1.1.88.59");
				} else if (TEXT_XML.equals(docEntry.getMimeType())) {
					XDSCommonModule.updateClassificationByValueSet(cl, "urn:ihe:rad:TEXT");
				} else if (APPLICATION_PDF.equals(docEntry.getMimeType())) {
					XDSCommonModule.updateClassificationByValueSet(cl, "urn:ihe:rad:PDF");
				}
				return;
			}
		}
	}

	private void updatePatientID(DicomType dicom) {
		String patientIDInputText = this.submissionSet.getPatientId();
		boolean isserOfPatientIDAlreadySet = false;
		AttrType dicomPatientID = getAttributeByTag(dicom, "00100020");
		if (dicomPatientID != null && !dicomPatientID.getListStringValues().isEmpty()) {
			patientIDInputText = dicomPatientID.getListStringValues().get(0).trim();
		}
		patientIDInputText += "^^^&";
		AttrType issuerOfPatientID = getAttributeByTag(dicom, "00100021");
		if (issuerOfPatientID != null && !issuerOfPatientID.getListStringValues().isEmpty()) {
			patientIDInputText += issuerOfPatientID.getListStringValues().get(0).trim();
			isserOfPatientIDAlreadySet = true;
		}
		AttrType issuerOfPatientIDQualifiersSequence = getAttributeByTag(dicom, "00100024");
		if (issuerOfPatientIDQualifiersSequence != null && !issuerOfPatientIDQualifiersSequence.getListStringValues().isEmpty()) {
			AttrType universalEntityID = getAttributeInSequence(dicom, "00100024", "00400032");
			if (!isserOfPatientIDAlreadySet && universalEntityID != null && !universalEntityID.getListStringValues().isEmpty()) {
				patientIDInputText += universalEntityID.getListStringValues().get(0).trim();
				isserOfPatientIDAlreadySet = true;
			}
			AttrType universalEntityIDType = getAttributeInSequence(dicom, "00100024", "00400033");
			if (universalEntityIDType != null && !universalEntityIDType.getListStringValues().isEmpty()) {
				patientIDInputText += "&" + universalEntityIDType.getListStringValues().get(0).trim();
			}
		}
		if (!isserOfPatientIDAlreadySet) {
			patientIDInputText += "1.3.6.1.4.1.12559.11.13.2.5&ISO";
		}

		this.submissionSet.setPatientId(patientIDInputText);
	}

	@Override
	public boolean isPatientIDEmpty() {
		String patientIDInputText = this.submissionSet.getPatientId().split("\\^\\^\\^")[0];
		if (this.selectedXDSDocumentEntry.getRelatedFile() == null) {
			return false;
		}
		try {
			String dump = DicomTool.dicom2xml(this.selectedXDSDocumentEntry.getRelatedFile().getPath());
			DicomType dicom = TransformDicom.load(new ByteArrayInputStream(dump.getBytes()));
			AttrType dicomPatientID = getAttributeByTag(dicom, "00100020");
			return (patientIDInputText == null || patientIDInputText.isEmpty())
					&& (dicomPatientID == null || dicomPatientID.getListStringValues().isEmpty());
		} catch (Exception e) {
			log.warn("problem occure when trying to dump the dicom !");
		}
		return true;
	}

	private void updateStudyInstanceUID(DicomType dicom) {
		AttrType studyInstanceUID = getAttributeByTag(dicom, "0020000D");
		String studyUIDSlotValue = "";
		if (studyInstanceUID != null && !studyInstanceUID.getListStringValues().isEmpty()) {
			studyUIDSlotValue = studyInstanceUID.getListStringValues().get(0).trim() + "^^^^urn:ihe:iti:xds:2016:studyInstanceUID";
		}
		for (SlotType1 slot : this.selectedXDSDocumentEntry.getSlot()) {
			if ("urn:ihe:iti:xds:2013:referenceIdList".equals(slot.getName())) {
				List<String> valueList = new ArrayList<>();
				if (studyInstanceUID != null
						&& !studyInstanceUID.getListStringValues().isEmpty()
						&& !slot.getValueList().getValue().contains(studyUIDSlotValue)) {
					valueList.add(studyUIDSlotValue);
					slot.getValueList().setValue(valueList);
					return;
				}
			}
		}
		SlotMetadata slotMetadata = new SlotMetadata();
		slotMetadata.setName("urn:ihe:iti:xds:2013:referenceIdList");
		slotMetadata.setDefaultValue(studyUIDSlotValue);
		SlotType1 slot = RIMGenerator.generateSlot(slotMetadata);
		this.selectedXDSDocumentEntry.addSlot(slot);
	}
	
	private static AttrType getAttributeByTag(DicomType obj, String tag){
		for (AttrType attr : obj.getAttr()) {
			if (attr.getTag().equals(tag)) return attr;
		}
		return null;
	}

	private static AttrType getAttributeInSequence(DicomType obj, String sequenceTag, String tag) {
		for (AttrType attr : obj.getAttr()) {
			if (sequenceTag != null && sequenceTag.equals(attr.getTag()) && !attr.getMixed().isEmpty()) {
				ItemType item = attr.getItem().get(0);
				if (!item.getAttr().isEmpty()) {
					List<AttrType> subAttrs = item.getAttr();
					for (AttrType subAttr : subAttrs) {
						if (subAttr.getTag().equals(tag)) return subAttr;
					}
				}
			}
		}
		return null;
	}
	
	public void validateCurrentDicomFile() {
		if (this.selectedXDSDocumentEntry != null
				&& this.selectedXDSDocumentEntry.getMimeType() != null
				&& APPLICATION_DICOM.equals(this.selectedXDSDocumentEntry.getMimeType())) {
			this.validationResult = (new Dicom3tools(ApplicationConfiguration.getValueOfVariable("dicom3tools_validator"))).validate(this.selectedXDSDocumentEntry.getRelatedFile().getPath());
		}
	}
	
	public void validateCurrentCDAFile() {
		this.validationResult = "";
		String doc = Base64.encodeFromFile(this.selectedXDSDocumentEntry.getRelatedFile().getPath());
		String endpoint = ApplicationConfiguration.getValueOfVariable("cda_mbv_wsdl");
		this.validationResult = this.validate(doc, CDA_VALIDATOR_NAME, true, endpoint);
		this.validationResult = Util.transformXmlToHtml(this.validationResult, ApplicationConfiguration.getValueOfVariable("cda_mbv_xslt"));
	}
	
	public String validate(String document, String validator, boolean isDocumentBase64, String endpoint) {
		this.validationResult = EVSClientValidator.validate(document, validator, isDocumentBase64, endpoint);
		return this.validationResult;
	}
	
	public static void main(String[] args) throws IOException {
		XDSI68Manager xx = new XDSI68Manager();
		String document = FileReadWrite.readDoc("/home/aboufahj/art-decor/pap/sample_ccdas-master/EMERGE/Patient-286.xml");
		xx.validate(document, CDA_VALIDATOR_NAME, false, "http://gazelle.ihe.net/CDAGenerator-CDAGenerator-ejb/CDAValidatorWS?wsdl");
		System.out.println(xx.validationResult);
	}
}