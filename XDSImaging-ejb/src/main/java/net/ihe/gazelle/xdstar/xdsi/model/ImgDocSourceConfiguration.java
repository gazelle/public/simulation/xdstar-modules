package net.ihe.gazelle.xdstar.xdsi.model;



import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.jboss.seam.annotations.Name;

@Entity
@Name("imgDocSourceConfiguration")
@DiscriminatorValue("IMGDOCSRC")
public class ImgDocSourceConfiguration extends ImgDocSourceConfigurationParent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ImgDocSourceConfiguration() {}
	
	public ImgDocSourceConfiguration(ImgDocSourceConfiguration configuration) {
		super(configuration);
	}

}
