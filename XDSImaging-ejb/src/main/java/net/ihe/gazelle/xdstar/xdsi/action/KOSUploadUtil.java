package net.ihe.gazelle.xdstar.xdsi.action;

import net.ihe.gazelle.dicom.DicomType;
import net.ihe.gazelle.test.TransformDicom;
import net.ihe.gazelle.xdstar.xdsi.utils.DicomTool;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class KOSUploadUtil {

    private static Logger log = LoggerFactory.getLogger(KOSUploadUtil.class);

    public static DicomType uploadListenerAndGenerateDicomType(FileUploadEvent event) {
        DicomType generatedDicom = null;
        UploadedFile item = event.getUploadedFile();
        File ftmp = null;
        try {
            ftmp = File.createTempFile("test", "xml");
            FileOutputStream fos = new FileOutputStream(ftmp);
            if (item.getData() != null && item.getData().length > 0) {
                fos.write(item.getData());
                fos.close();
                try {
                    String dump = DicomTool.dicom2xml(ftmp.getPath());
                    generatedDicom = TransformDicom.load(new ByteArrayInputStream(dump.getBytes()));
                } catch (Exception e) {
                    log.info("problem occure when trying to dump the dicom !");
                }
            } else {
                FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "The file is empty !");
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return generatedDicom;
    }

}
