/*
 * Copyright 2011 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.xdsi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import net.ihe.gazelle.xdstar.common.model.AbstractMessage;

import org.jboss.seam.annotations.Name;

/**
 * 
 * @author abderrazek boufahja
 * 
 */
@Entity
@Name("WADOMessage")
@DiscriminatorValue("WADO")
public class WADOMessage extends AbstractMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "wado_downloaded_file_id")
	private WADODownloadedFile wadoFile;

	public WADODownloadedFile getWadoFile() {
		return wadoFile;
	}

	public void setWadoFile(WADODownloadedFile wadoFile) {
		this.wadoFile = wadoFile;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = (prime * result)
				+ ((wadoFile == null) ? 0 : wadoFile.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		WADOMessage other = (WADOMessage) obj;
		if (wadoFile == null) {
			if (other.wadoFile != null) {
				return false;
			}
		} else if (!wadoFile.equals(other.wadoFile)) {
			return false;
		}
		return true;
	}

}
