package net.ihe.gazelle.xdstar.xdsi.model;



import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.jboss.seam.annotations.Name;

@Entity
@Name("RespImgGatewayConfiguration")
@DiscriminatorValue("RESPIMGGAT")
public class RespondingImagingGatewayConfiguration extends ImgDocSourceConfigurationParent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RespondingImagingGatewayConfiguration(){}
	
	public RespondingImagingGatewayConfiguration(RespondingImagingGatewayConfiguration cop){
		super(cop);
	}

}
