/*
 * Copyright 2011 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.xdsi.model;

import java.io.IOException;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityManager;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.xdstar.common.model.RegisteredFile;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;


/**
 * 
 * @author abderrazek boufahja
 *
 */
@Entity
@Name("WADOfile")
@DiscriminatorValue("WADO")
public class WADODownloadedFile extends RegisteredFile{
	
	@Logger
	private static Log log;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public WADODownloadedFile(){}
	
	public WADODownloadedFile(String fileName, String fileType){
		super(fileName, fileType);
	}
	
	public static WADODownloadedFile storeWADODownloadedFile(WADODownloadedFile file, EntityManager em){
		if ((em != null) && (file != null)){
			WADODownloadedFile wado = em.merge(file);
			em.flush();
			return wado;
		}
		return null;
	}
	
	public static WADODownloadedFile saveFile(String fileName, String fileType) throws IOException
	{
		String absolutePath = ApplicationConfiguration.getValueOfVariable("registered_files_directory");
		if (absolutePath != null && !absolutePath.isEmpty())
		{
			EntityManager em = (EntityManager) Component.getInstance("entityManager");
			WADODownloadedFile registeredFile = new WADODownloadedFile(fileName, fileType);
			registeredFile = em.merge(registeredFile);
			em.flush();
			FileContentType ff = FileContentType.getFileTypeContentFromType(fileType);
			String extension = "";
			if (ff != null) {
				extension = "." + ff.getExtension();
			}
			absolutePath = absolutePath + "/wado_" + registeredFile.getId().toString() + extension;
			registeredFile.setPath(absolutePath);
			if (fileName == null){
				registeredFile.setName("wado_" + registeredFile.getId().toString() + extension);
			}
			registeredFile = em.merge(registeredFile);
			em.flush();
			
			return registeredFile;
		}
		else
		{
			log.error("problem when saving the wado file.");
			return null;
		}
	}
	
}
