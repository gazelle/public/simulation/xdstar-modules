/*
 * Copyright 2012 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.xdsi.action;

import java.io.Serializable;
import java.util.List;

import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.xdstar.xdsi.model.RAD69Message;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

/**
 *  
 * @author                	Abderrazek Boufahja / KEREVAL Rennes IHE development Project
 *
 */
@Name("XCAI69AManager")
@Scope(ScopeType.PAGE)
public class XCAI69AManager extends XDSI69Manager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static String TRANSACTION_TYPE = "XCAI69A";

	@Logger
	private static Log log;

//	@Override
//	public void listAllConfigurations() {
//		List<InitiatingImagingGatewayConfiguration> rr = InitiatingImagingGatewayConfiguration.listAllSystemConfigurationsInit();
//		this.configurations = new ArrayList<ImgDocSourceConfiguration>();
//		if (rr != null) this.configurations.addAll(rr);
//	}
	

	@Override
	public List<AffinityDomain> listAllAffinityDomains() {
		return this.listAllAffinityDomains(XCAI69AManager.TRANSACTION_TYPE);
	}
	

	@Override
	public String getTheNameOfThePage() {
		String res = this.getNameOfTheAffinityDomainTransaction();
		if (res == null || res.equals("")) res = "RAD-69 : Imaging Document Consumer ==> Initiating Imaging Gateway";
		return res;
	}
	
	public void sendMessage(){
		super.sendMessage();
		this.message.setMessageType("XCA-I (IMG_DOC_CONS to INIT_IMG_GW)");
		this.message = RAD69Message.storeMessage(this.message);
	}
	
	@Override
	public void init() {
		super.init();
		this.selectedTransaction = Transaction.GetTransactionByKeyword("RAD-69");
		this.selectedAffinityDomain = AffinityDomain.getAffinityDomainByKeyword("IHE_XCA-I");
		this.initFromMenu = true;
	}
}
