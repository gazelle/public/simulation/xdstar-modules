package net.ihe.gazelle.xdstar.xdsi.action;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.simulator.sut.action.AbstractSystemConfigurationManager;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationFiltering;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationType;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationTypeQuery;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationTypeType;
import net.ihe.gazelle.xdstar.xdsi.model.RespondingImagingGatewayConfiguration;
import net.ihe.gazelle.xdstar.xdsi.model.RespondingImagingGatewayConfigurationQuery;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("rigConfigurationManager")
@Scope(ScopeType.PAGE)
public class RIGConfigurationManager extends AbstractSystemConfigurationManager<RespondingImagingGatewayConfiguration> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void addConfiguration() {
		selectedSystemConfiguration = new RespondingImagingGatewayConfiguration();
		selectedSystemConfiguration.setIsPublic(true);
		editSelectedConfiguration();
	}
	
	@Override
	public void copySelectedConfiguration(RespondingImagingGatewayConfiguration inConfiguration) {
		this.selectedSystemConfiguration = new RespondingImagingGatewayConfiguration(inConfiguration);
		editSelectedConfiguration();		
	}

	public void deleteSelectedSystemConfiguration() {
		this.deleteSelectedSystemConfiguration(RespondingImagingGatewayConfiguration.class);
	}
	
	@Override
	public List<Usage> getPossibleListUsages() {
		ConfigurationTypeQuery qq = new ConfigurationTypeQuery();
		qq.confTypeType().eq(ConfigurationTypeType.RESP_IMG_GAT_CONF);
		ConfigurationType conf  = qq.getUniqueResult();
		if (conf != null){
			return conf.getListUsages();
		}
		return null;
	}

	@Override
	public void modifyQuery(HQLQueryBuilder<RespondingImagingGatewayConfiguration> queryBuilder,
			Map<String, Object> filterValuesApplied) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected HQLCriterionsForFilter<RespondingImagingGatewayConfiguration> getHQLCriterionsForFilter() {
		return new ConfigurationFiltering<RespondingImagingGatewayConfiguration, RespondingImagingGatewayConfigurationQuery>(
				new RespondingImagingGatewayConfigurationQuery()).getCriterionList();
	}

}
