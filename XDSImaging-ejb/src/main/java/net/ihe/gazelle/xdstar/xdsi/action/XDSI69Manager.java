/*
 * Copyright 2012 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.xdsi.action;

import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.dicom.AttrType;
import net.ihe.gazelle.dicom.DicomType;
import net.ihe.gazelle.dicom.ItemType;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.common.xua.SignatureException;
import net.ihe.gazelle.xds.DocumentRequestType;
import net.ihe.gazelle.xdsi.RetrieveImagingDocumentSetRequestType;
import net.ihe.gazelle.xdsi.SeriesRequestType;
import net.ihe.gazelle.xdsi.StudyRequestType;
import net.ihe.gazelle.xdsi.TransferSyntaxUIDListType;
import net.ihe.gazelle.xdstar.common.action.CommonSimulatorManager;
import net.ihe.gazelle.xdstar.common.model.AttachmentFile;
import net.ihe.gazelle.xdstar.common.model.ContentType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import net.ihe.gazelle.xdstar.comon.util.ConfigurationFilter;
import net.ihe.gazelle.xdstar.comon.util.Util;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespacePrefixMapper;
import net.ihe.gazelle.xdstar.core.*;
import net.ihe.gazelle.xdstar.core.soap.SOAPRequestBuilder;
import net.ihe.gazelle.xdstar.xdrsrc.tools.Pair;
import net.ihe.gazelle.xdstar.xdsi.model.*;
import org.apache.axiom.attachments.Attachments;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.log.Log;
import org.richfaces.component.UITree;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.event.TreeSelectionChangeEvent;

import javax.faces.event.AbortProcessingException;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Abderrazek Boufahja / KEREVAL Rennes IHE development Project
 */

@Name("XDSI69Manager")
@Scope(ScopeType.PAGE)
public class XDSI69Manager extends CommonSimulatorManager<ImgDocSourceConfigurationParent, RAD69Message> implements Serializable,
        org.richfaces.event.TreeSelectionChangeListener {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static String TRANSACTION_TYPE = "RAD69";

    @Logger
    private static Log log;

    protected RetrieveImagingDocumentSetRequestType selectedRetrieveImagingDocumentSetRequest;

    protected StudyRequestType selectedStudyRequest;

    protected SeriesRequestType selectedSeriesRequest;

    protected DocumentRequestType selectedDocumentRequest;
    protected String validationResult;
    protected Boolean adviseNodeOpened = true;
    protected Boolean editRetrieveImagingDocumentSet = true;
    protected Boolean editStudyRequest = false;
    protected Boolean editSeriesRequest = false;
    protected Boolean editDocumentRequest = false;
    protected String selectedTransferSyntaxUID;
    protected Boolean editTransferSyntax = false;
    DicomType generatedDicom = null;
    private Boolean adviseNodeSelected = false;

    public static void save(OutputStream os, Object xdww) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.xdsi");
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new XDSNamespacePrefixMapper());
        m.marshal(xdww, os);
    }

    public String getSelectedTransferSyntaxUID() {
        return selectedTransferSyntaxUID;
    }

    public void setSelectedTransferSyntaxUID(String selectedTransferSyntaxUID) {
        this.selectedTransferSyntaxUID = selectedTransferSyntaxUID;
    }

    public DicomType getGeneratedDicom() {
        return generatedDicom;
    }

    public void setGeneratedDicom(DicomType generatedDicom) {
        this.generatedDicom = generatedDicom;
    }

    public Boolean getEditTransferSyntax() {
        return editTransferSyntax;
    }

    public void setEditTransferSyntax(Boolean editTransferSyntax) {
        this.editTransferSyntax = editTransferSyntax;
    }

    public java.lang.Boolean adviseNodeSelected(org.richfaces.component.UITree tree) {
        return adviseNodeSelected;
    }

    public void nodeSelectListener(TreeSelectionChangeEvent event) {
        System.out.println("-------");
        UITree tree = (UITree) event.getComponent();
        Object obj = tree.getRowData();
        editRightPanel(obj);
    }

    public void editRightPanel(Object obj) {
        if (obj instanceof RetrieveImagingDocumentSetRequestType) {
            this.editRetrieveImagingDocumentSet();
        } else if (obj instanceof StudyRequestType) {
            this.editStudyRequest((StudyRequestType) obj);
        } else if (obj instanceof SeriesRequestType) {
            this.editSeriesRequest((SeriesRequestType) obj);
        } else if (obj instanceof DocumentRequestType) {
            this.editDocumentRequest((DocumentRequestType) obj);
        }
        adviseNodeOpened = null;
        adviseNodeSelected = null;
    }

    public Boolean getEditSeriesRequest() {
        return editSeriesRequest;
    }

    public void setEditSeriesRequest(Boolean editSeriesRequest) {
        this.editSeriesRequest = editSeriesRequest;
    }

    public Boolean getEditDocumentRequest() {
        return editDocumentRequest;
    }

    public void setEditDocumentRequest(Boolean editDocumentRequest) {
        this.editDocumentRequest = editDocumentRequest;
    }

    public Boolean getEditRetrieveImagingDocumentSet() {
        return editRetrieveImagingDocumentSet;
    }

    public void setEditRetrieveImagingDocumentSet(
            Boolean editRetrieveImagingDocumentSet) {
        this.editRetrieveImagingDocumentSet = editRetrieveImagingDocumentSet;
    }

    public Boolean getEditStudyRequest() {
        return editStudyRequest;
    }

    public void setEditStudyRequest(Boolean editStudyRequest) {
        this.editStudyRequest = editStudyRequest;
    }

    public Boolean getAdviseNodeOpened() {
        return adviseNodeOpened;
    }

    public void setAdviseNodeOpened(Boolean adviseNodeOpened) {
        this.adviseNodeOpened = adviseNodeOpened;
    }

    public java.lang.Boolean adviseNodeOpened(org.richfaces.component.UITree tree) {
        return adviseNodeOpened;
    }

    public GazelleTreeNodeImpl<Object> getTreeNodeContent() {
        return this.createTreeNodeContent();
    }

    public void setTreeNodeContent(GazelleTreeNodeImpl<Object> treeNodeContent) {
//		this.treeNodeContent = treeNodeContent;
    }

    public StudyRequestType getSelectedStudyRequest() {
        return selectedStudyRequest;
    }

    public void setSelectedStudyRequest(StudyRequestType selectedStudyRequest) {
        this.selectedStudyRequest = selectedStudyRequest;
    }

    public SeriesRequestType getSelectedSeriesRequest() {
        return selectedSeriesRequest;
    }

    public void setSelectedSeriesRequest(SeriesRequestType selectedSeriesRequest) {
        this.selectedSeriesRequest = selectedSeriesRequest;
    }

    public String getValidationResult() {
        return validationResult;
    }

    public void setValidationResult(String validationResult) {
        this.validationResult = validationResult;
    }

    public DocumentRequestType getSelectedDocumentRequest() {
        return selectedDocumentRequest;
    }

    public void setSelectedDocumentRequest(DocumentRequestType selectedDocumentRequest) {
        this.selectedDocumentRequest = selectedDocumentRequest;
    }

    public RetrieveImagingDocumentSetRequestType getSelectedRetrieveImagingDocumentSetRequest() {
        return selectedRetrieveImagingDocumentSetRequest;
    }

    public void setSelectedRetrieveImagingDocumentSetRequest(
            RetrieveImagingDocumentSetRequestType selectedRetrieveImagingDocumentSetRequest) {
        this.selectedRetrieveImagingDocumentSetRequest = selectedRetrieveImagingDocumentSetRequest;
    }

    public void initSelectedRequest() {
        this.selectedRetrieveImagingDocumentSetRequest = new RetrieveImagingDocumentSetRequestType();
        this.selectedRetrieveImagingDocumentSetRequest.getStudyRequest().add(new StudyRequestType());
        this.selectedRetrieveImagingDocumentSetRequest.getStudyRequest().get(0).getSeriesRequest().add(new SeriesRequestType());
        this.selectedRetrieveImagingDocumentSetRequest.getStudyRequest().get(0).getSeriesRequest().get(0).getDocumentRequest().add(new
                DocumentRequestType());
        this.selectedRetrieveImagingDocumentSetRequest.setTransferSyntaxUIDList(new TransferSyntaxUIDListType());
    }

    public void deleteDocumentRequest(DocumentRequestType doc) {
        if ((this.selectedRetrieveImagingDocumentSetRequest != null) && (doc != null)) {
            stad:
            for (StudyRequestType st : this.selectedRetrieveImagingDocumentSetRequest.getStudyRequest()) {
                for (SeriesRequestType sr : st.getSeriesRequest()) {
                    for (DocumentRequestType docc : sr.getDocumentRequest()) {
                        if (docc == doc) {
                            sr.getDocumentRequest().remove(docc);
                            break stad;
                        }
                    }
                }
            }
        }
        this.editDocumentRequest = false;
    }

    public void init() {
        super.init();
        if (this.selectedTransaction == null || this.selectedAffinityDomain == null) {
            this.selectedTransaction = Transaction.GetTransactionByKeyword("RAD-69");
            this.selectedAffinityDomain = AffinityDomain.getAffinityDomainByKeyword("IHE_XDS-I.b");
        } else {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            this.selectedTransaction = em.find(Transaction.class, this.selectedTransaction.getId());
            this.selectedAffinityDomain = em.find(AffinityDomain.class, this.selectedAffinityDomain.getId());
        }
        this.initFromMenu = true;
    }

    public void addDocumentRequest(SeriesRequestType sr) {
        DocumentRequestType doc = new DocumentRequestType();
        sr.getDocumentRequest().add(doc);
        this.selectedDocumentRequest = doc;
    }

    @Override
    public void listAllConfigurations() {
        ImgDocSourceConfigurationParentQuery qq = new ImgDocSourceConfigurationParentQuery();
        qq.listUsages().affinity().eq(this.selectedAffinityDomain);
        qq.listUsages().transaction().eq(this.selectedTransaction);
        qq.name().order(true);
        ConfigurationFilter.filterConfigurationQueryAccodingToUserCredential(qq);
        configurations = qq.getList();
    }

    @Override
    public void updateSpecificClient() {
        this.initSelectedRequest();
    }

    @Override
    public List<AffinityDomain> listAllAffinityDomains() {
        return this.listAllAffinityDomains(XDSI69Manager.TRANSACTION_TYPE);
    }

    public String previewMessage() throws IOException {
        Pair<String, byte[]> mom = null;
        try {
            mom = buildMTOMRequest();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        if (mom != null) {
            return new String(mom.getObject2());
        }
        return null;
    }

    public Pair<String, byte[]> buildMTOMRequest() throws ParserConfigurationException, SignatureException, JAXBException, IOException {
        String rr = this.getRetrieveImagingDocumentSetRequestTypeAsString(this.selectedRetrieveImagingDocumentSetRequest);
        String receiverUrl = this.selectedConfiguration.getUrl();
        String soapMessage = SOAPRequestBuilder.createSOAPMessage(rr, false, null, null, null, receiverUrl,
                "urn:ihe:rad:2009:RetrieveImagingDocumentSet", false);

        List<MTOMElement> listDataToSend = new ArrayList<MTOMElement>();
        MTOMElement soap = new MTOMElement();
        soap.setContent(("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + soapMessage + "\r\n").getBytes());
        soap.setContentType(" application/xop+xml; charset=UTF-8; type=\"application/soap+xml\"");
        soap.setTransfertEncoding("binary");
        soap.setIndex(0);
        soap.setUuid(UUID.randomUUID().toString().replace("-", "").toUpperCase());
        listDataToSend.add(soap);

        byte[] body = MTOMBuilder.buildMTOM(listDataToSend);

        String boundaryUUID = MTOMBuilder.getBoundaryUUIDFromGeneratedMTOM(body);

        String boundary = "MIMEBoundaryurn_uuid_" + boundaryUUID;
        String boundaryStart = "0.urn:uuid:" + soap.getUuid() + "@ws.jboss.org";
        String contentTypeProperties = new String();
        contentTypeProperties = "multipart/related; ";
        contentTypeProperties = contentTypeProperties + "boundary=" + boundary + "; ";
        contentTypeProperties = contentTypeProperties + "type=\"application/xop+xml\"; ";
        contentTypeProperties = contentTypeProperties + "start=\"<" + boundaryStart + ">\"; ";
        contentTypeProperties = contentTypeProperties + "start-info=\"application/soap+xml\"; ";
        contentTypeProperties = contentTypeProperties + "action=\"urn:ihe:rad:2009:RetrieveImagingDocumentSet\"";
        Pair<String, byte[]> res = new Pair<String, byte[]>(contentTypeProperties, body);
        return res;
    }

    public String createMessage() throws ParserConfigurationException, SignatureException, JAXBException {
        String res = "";
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        save(baos, this.selectedRetrieveImagingDocumentSetRequest);
        res = SOAPRequestBuilder.createSOAPMessage(baos.toString(), useXUA, praticianID, attributes, null, this.selectedConfiguration.getUrl(),
                "urn:ihe:iti:2007:CrossGatewayRetrieve", false);
        return res;
    }

    public void sendMessage() {
        sendMessageWithNoPersist();
        message = RAD69Message.storeMessage(message);
        if (Contexts.getSessionContext() != null) {
            Contexts.getSessionContext().set("messagesToDisplay", message);
        }
        displayResultPanel = true;
    }

    public void sendMessageWithNoPersist() {
        Pair<String, byte[]> mtom;
        try {
            mtom = this.buildMTOMRequest();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            return;
        } catch (SignatureException e) {
            log.info("probably not able to add signature");
            e.printStackTrace();
            return;
        } catch (JAXBException e) {
            e.printStackTrace();
            return;
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        MessageSenderCommon msc = new MessageSenderCommon();

        message = new RAD69Message();
        net.ihe.gazelle.util.Pair<String, InputStream> couple = msc.sendMessageAndGetInputStream(this.selectedConfiguration.getUrl(), mtom
                .getObject2(), message, mtom.getObject1());
        String resp = null;
        String resph = couple != null ? couple.getObject1() : null;
        Attachments attachments = null;
        if (couple != null) {
            if (resph != null && resph.contains("multipart")) {
                attachments = new Attachments(couple.getObject2(), couple.getObject1());
                resp = AttachmentsUtil.transformMultipartToSOAP(attachments);
            } else {
                resp = AttachmentsUtil.transformInputToString(couple.getObject2());
            }
        }

        if (resp != null) {
            resp = formatStringIfContainsXML(resp);
        }

        message.setTimeStamp(new Date());
        message.setConfiguration(selectedConfiguration);
        message.setTransaction(selectedTransaction);
        message.setAffinityDomain(this.selectedAffinityDomain);
        message.setContentType(ContentType.MTOM_XOP);
        message.setGazelleDriven(false);

        MetadataMessage sent = new MetadataMessage();
        sent.setMessageContent(this.patchXop(formatStringIfContainsXML(new String(mtom.getObject2()))));
        sent.setHttpHeader(mtom.getObject1());
        sent.setMessageType(MetadataMessageType.Retrieve_Imaging_Document_Set_REQUEST);

        MetadataMessage received = new MetadataMessage();
        received.setMessageContent(resp);
        received.setHttpHeader(resph);
        received.setMessageType(MetadataMessageType.RETRIEVE_DOCUMENT_SET_RESPONSE);
        if (attachments != null) {
            try {
                List<AttachmentFile> fileAttachedToReceiver = AttachmentsUtil.generateListAttachmentFiles(attachments, resp);
                if (fileAttachedToReceiver != null && fileAttachedToReceiver.size() > 0) {
                    received.setListAttachments(fileAttachedToReceiver);
                    for (AttachmentFile attachmentFile : fileAttachedToReceiver) {
                        attachmentFile.setMetadataMessage(received);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        message.setSentMessageContent(sent);
        message.setReceivedMessageContent(received);
    }

    private String patchXop(String to) {
        String res = to;
        Pattern pat = Pattern.compile("<([^(:<>)]*?:?)Document(.*?)>(\\s)*?<([^(:<>)]*?:?)Include(.*?)>(\\s)*?</([^(:<>)]*?:?)Document>");
        Matcher mat = pat.matcher(to);
        while (mat.find()) {
            res = res.replace(mat.group(), "<" + mat.group(1) + "Document" + mat.group(2) + "><" + mat.group(4) + "Include" + mat.group(5) + "></"
                    + mat.group(7) + "Document>");
        }
        return res;
    }

    private String formatStringIfContainsXML(String string) {
        if (string != null) {
            String res = "";
            String soapRegex = "<\\?xml.*?Envelope>";
            Pattern pp = Pattern.compile(soapRegex, Pattern.MULTILINE | Pattern.DOTALL);
            Matcher mm = pp.matcher(string);

            if (mm.find()) {
                String ss = mm.group();
                int ind = string.indexOf(ss);
                String toModify = Util.prettyFormat(ss);
                String begin = string.substring(0, ind);
                String end = string.substring(ind + ss.length());
                res = begin + toModify + end;
            } else {
                res = string;
            }

            return res;
        }
        return null;
    }


    @Override
    public String getTheNameOfThePage() {
        String res = this.getNameOfTheAffinityDomainTransaction();
        if (res == null || res.equals("")) {
            res = "Retrieve Imaging Document Set";
        }
        return res;
    }

    public GazelleTreeNodeImpl<Object> createTreeNodeContent() {
        GazelleTreeNodeImpl<Object> treeNodeContent = new GazelleTreeNodeImpl<Object>();
        if (this.selectedRetrieveImagingDocumentSetRequest != null) {
            treeNodeContent = new GazelleTreeNodeImpl<Object>();
            GazelleTreeNodeImpl<Object> retrieveImagingTreeNode = new GazelleTreeNodeImpl<Object>();
            retrieveImagingTreeNode.setData(this.selectedRetrieveImagingDocumentSetRequest);
            treeNodeContent.addChild(0, retrieveImagingTreeNode);
            int i = 0;
            int j;
            int k;
            for (StudyRequestType studyRequest : this.selectedRetrieveImagingDocumentSetRequest.getStudyRequest()) {
                GazelleTreeNodeImpl<Object> studyRequestTreeNode = new GazelleTreeNodeImpl<Object>();
                studyRequestTreeNode.setData(studyRequest);
                j = 0;
                for (SeriesRequestType seriesRequest : studyRequest.getSeriesRequest()) {
                    GazelleTreeNodeImpl<Object> seriesRequestTreeNode = new GazelleTreeNodeImpl<Object>();
                    seriesRequestTreeNode.setData(seriesRequest);
                    k = 0;
                    for (DocumentRequestType documentRequest : seriesRequest.getDocumentRequest()) {
                        GazelleTreeNodeImpl<Object> documentRequestTreeNode = new GazelleTreeNodeImpl<Object>();
                        documentRequestTreeNode.setData(documentRequest);
                        seriesRequestTreeNode.addChild(k, documentRequestTreeNode);
                        k++;
                    }
                    studyRequestTreeNode.addChild(j, seriesRequestTreeNode);
                    j++;
                }
                retrieveImagingTreeNode.addChild(i, studyRequestTreeNode);
                i++;
            }
        }
        return treeNodeContent;
    }

    public void editRetrieveImagingDocumentSet() {
        this.editRetrieveImagingDocumentSet = true;
        this.editStudyRequest = false;
        this.editSeriesRequest = false;
        this.editDocumentRequest = false;
    }

    public void addStudyRequest(RetrieveImagingDocumentSetRequestType ret) {
        this.selectedStudyRequest = new StudyRequestType();
        this.selectedStudyRequest.getSeriesRequest().add(new SeriesRequestType());
        this.selectedStudyRequest.getSeriesRequest().get(0).getDocumentRequest().add(new DocumentRequestType());
        ret.getStudyRequest().add(this.selectedStudyRequest);
        this.createTreeNodeContent();
        this.editStudyRequest(this.selectedStudyRequest);
    }

    public void editStudyRequest(StudyRequestType st) {
        this.editRetrieveImagingDocumentSet = false;
        this.editSeriesRequest = false;
        this.editDocumentRequest = false;
        this.editStudyRequest = true;
        this.selectedStudyRequest = st;
    }

    public void addSeriesRequest(StudyRequestType srt) {
        this.selectedSeriesRequest = new SeriesRequestType();
        this.selectedSeriesRequest.getDocumentRequest().add(new DocumentRequestType());
        this.editSeriesRequest(selectedSeriesRequest);
        this.createTreeNodeContent();
        srt.getSeriesRequest().add(this.selectedSeriesRequest);
    }

    public void deleteStudyRequest(StudyRequestType srt) {
        for (StudyRequestType sr : this.selectedRetrieveImagingDocumentSetRequest.getStudyRequest()) {
            if (sr == srt) {
                this.selectedRetrieveImagingDocumentSetRequest.getStudyRequest().remove(sr);
                break;
            }
        }
        this.createTreeNodeContent();
        this.editStudyRequest = false;
    }

    public void editSeriesRequest(SeriesRequestType sr) {
        this.selectedSeriesRequest = sr;
        this.editDocumentRequest = false;
        this.editRetrieveImagingDocumentSet = false;
        this.editSeriesRequest = true;
        this.editStudyRequest = false;
    }

    public void deleteSeriesRequest(SeriesRequestType sr) {
        book:
        for (StudyRequestType str : this.selectedRetrieveImagingDocumentSetRequest.getStudyRequest()) {
            for (SeriesRequestType ser : str.getSeriesRequest()) {
                if (ser == sr) {
                    str.getSeriesRequest().remove(ser);
                    break book;
                }
            }
        }
        this.createTreeNodeContent();
        this.editSeriesRequest = false;
    }

    public void editDocumentRequest(DocumentRequestType doc) {
        this.editDocumentRequest = true;
        this.editRetrieveImagingDocumentSet = false;
        this.editSeriesRequest = false;
        this.editSeriesRequest = false;
        this.editStudyRequest = false;
        this.selectedDocumentRequest = doc;
    }

    public String truncateKey(String key) {
        return key.replaceAll("org.richfaces.model.SequenceRowKey", "");
    }

    public void addSelectedTransferSyntaxUID() {
        if (this.selectedTransferSyntaxUID != null) {
            if (this.selectedRetrieveImagingDocumentSetRequest.getTransferSyntaxUIDList() == null) {
                this.selectedRetrieveImagingDocumentSetRequest.setTransferSyntaxUIDList(new TransferSyntaxUIDListType());
            }
            this.selectedRetrieveImagingDocumentSetRequest.getTransferSyntaxUIDList().getTransferSyntaxUID().add(
                    StringUtils.substring(this.selectedTransferSyntaxUID, 0, this.selectedTransferSyntaxUID.indexOf(":")));
        }
    }

    protected String getRetrieveImagingDocumentSetRequestTypeAsString(RetrieveImagingDocumentSetRequestType ret) throws JAXBException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JAXBContext jax = JAXBContext.newInstance(RetrieveImagingDocumentSetRequestType.class);
        Marshaller m = jax.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new XDSNamespacePrefixMapper());
        m.marshal(ret, baos);
        String res = baos.toString();
        res = this.patchXop(res);
        return res;
    }

    public void uploadListener(FileUploadEvent event) {
        generatedDicom = KOSUploadUtil.uploadListenerAndGenerateDicomType(event);
    }

    public void generateRequestFromManifest() {
        if (this.generatedDicom != null) {
            this.selectedRetrieveImagingDocumentSetRequest = new RetrieveImagingDocumentSetRequestType();
            for (AttrType attr : this.generatedDicom.getAttr()) {
                if (attr.getTag() != null && attr.getTag().toLowerCase().equals("0040a375")) {
                    for (ItemType item : attr.getItem()) {
                        StudyRequestType srt = new StudyRequestType();
                        srt.setStudyInstanceUID(DicomTypeProcessor.getStudyInstanceUIDFromItem(item));
                        AttrType studyRequestAttr = DicomTypeProcessor.getAttrTypeOfReferencedSeriesSequence(item);
                        if (studyRequestAttr != null) {
                            for (ItemType itmSeriesRequest : studyRequestAttr.getItem()) {
                                SeriesRequestType sreq = new SeriesRequestType();
                                sreq.setSeriesInstanceUID(DicomTypeProcessor.getSeriesInstanceUIDFromItem(itmSeriesRequest));
                                AttrType docRequestAttr = DicomTypeProcessor.getAttrTypeOfReferencedSopSequence(itmSeriesRequest);
                                if (docRequestAttr != null) {
                                    for (ItemType itm : docRequestAttr.getItem()) {
                                        DocumentRequestType docr = new DocumentRequestType();
                                        docr.setRepositoryUniqueId(DicomTypeProcessor.getRepositoryUniqueIdFromItem(itmSeriesRequest, this
                                                .selectedConfiguration.getRepositoryUniqueId()));
                                        docr.setDocumentUniqueId(DicomTypeProcessor.getDocumentUniqueIdFromItem(itm));
                                        sreq.getDocumentRequest().add(docr);
                                    }
                                }
                                srt.getSeriesRequest().add(sreq);
                            }
                        }
                        this.selectedRetrieveImagingDocumentSetRequest.getStudyRequest().add(srt);
                    }
                }
            }
        }
        this.selectedDocumentRequest = null;
        this.selectedSeriesRequest = null;
        this.selectedStudyRequest = null;
        this.editRetrieveImagingDocumentSet();
        try {
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "The attributes of the request are generated from the manifest.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void processTreeSelectionChange(TreeSelectionChangeEvent arg0)
            throws AbortProcessingException {
        nodeSelectListener(arg0);
    }

    public List<SelectItem> transferSyntaxUIDList() {
        List<SelectItem> resf = new ArrayList<SelectItem>();
        TransferSyntaxUIDQuery tt = new TransferSyntaxUIDQuery();
        List<TransferSyntaxUID> res = tt.getList();
        for (TransferSyntaxUID tr : res) {
            resf.add(new SelectItem(tr.getUid(), tr.getUid() + "::" + tr.getName()));
        }
        return resf;
    }

    public void initDivTS() {
        this.setSelectedTransferSyntaxUID(null);
        this.setEditTransferSyntax(true);
    }

    public void deleteTransferSyntaxUID(String transUID) {
        getSelectedRetrieveImagingDocumentSetRequest().getTransferSyntaxUIDList().getTransferSyntaxUID().remove(transUID);
    }
}
