package net.ihe.gazelle.xdstar.xdsi.model;

public enum FileContentType {
	
	DICOM("application/dicom", "dcm"),
	JPEG("image/jpeg", "jpeg"),
	GIF("image/gif", "gif"),
	PNG("image/png", "png"),
	JP2("image/jp2", "jp2"),
	MPEG("video/mpeg", "mpeg"),
	TEXT_PLAIN("text/plain", "txt"),
	TEXT_HTML("text/html", "html"),
	TEXT_XML("text/xml", "xml"),
	PDF("application/pdf", "pdf"),
	RTF("text/rtf", "rtf"),
	CDA("application/x-hl7-cda-level-one+xml", "xml");
	
	private String value;
	
	private String extension;
	
	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	FileContentType(String value, String ext){
		this.value = value;
		this.extension = ext;
	}
	
	public static FileContentType getFileTypeContentFromType(String type){
		for (FileContentType ftc : FileContentType.values()) {
			if (ftc.value.equals(type)){
				return ftc;
			}
		}
		return null;
	}

}
