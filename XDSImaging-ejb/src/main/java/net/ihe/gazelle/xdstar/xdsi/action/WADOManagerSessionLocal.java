package net.ihe.gazelle.xdstar.xdsi.action;

import javax.ejb.Local;

@Local
public interface WADOManagerSessionLocal {

	public void downloadRelatedWadoFile();
	
	public void destroy();

}