package net.ihe.gazelle.xdstar.xdsi.action;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.simulator.sut.action.AbstractSystemConfigurationManager;
import net.ihe.gazelle.simulator.sut.model.Usage;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationFiltering;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationType;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationTypeQuery;
import net.ihe.gazelle.xdstar.conf.model.ConfigurationTypeType;
import net.ihe.gazelle.xdstar.xdsi.model.InitiatingImagingGatewayConfiguration;
import net.ihe.gazelle.xdstar.xdsi.model.InitiatingImagingGatewayConfigurationQuery;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("iigConfigurationManager")
@Scope(ScopeType.PAGE)
public class IIGConfigurationManager extends AbstractSystemConfigurationManager<InitiatingImagingGatewayConfiguration> implements Serializable, UserAttributeCommon {

	private static final long serialVersionUID = 1L;

	@In(value="gumUserService")
	private UserService userService;

	@Override
	public void addConfiguration() {
		selectedSystemConfiguration = new InitiatingImagingGatewayConfiguration();
		selectedSystemConfiguration.setIsPublic(true);
		editSelectedConfiguration();
	}

	@Override
	public void copySelectedConfiguration(InitiatingImagingGatewayConfiguration inConfiguration) {
		this.selectedSystemConfiguration = new InitiatingImagingGatewayConfiguration(inConfiguration);
		editSelectedConfiguration();		
	}

	public void deleteSelectedSystemConfiguration() {
		this.deleteSelectedSystemConfiguration(InitiatingImagingGatewayConfiguration.class);
	}
	
	@Override
	public List<Usage> getPossibleListUsages() {
		ConfigurationTypeQuery qq = new ConfigurationTypeQuery();
		qq.confTypeType().eq(ConfigurationTypeType.INIT_IMG_GAT_CONF);
		ConfigurationType conf  = qq.getUniqueResult();
		if (conf != null){
			return conf.getListUsages();
		}
		return null;
	}

	@Override
	public void modifyQuery(HQLQueryBuilder<InitiatingImagingGatewayConfiguration> queryBuilder,
			Map<String, Object> filterValuesApplied) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected HQLCriterionsForFilter<InitiatingImagingGatewayConfiguration> getHQLCriterionsForFilter() {
		return new ConfigurationFiltering<InitiatingImagingGatewayConfiguration, InitiatingImagingGatewayConfigurationQuery>(
				new InitiatingImagingGatewayConfigurationQuery()).getCriterionList();
	}
	@Override
	public String getUserName(String userId) {
		return userService.getUserDisplayNameWithoutException(userId);
	}


}
