/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.dsub.action;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.xdstar.comon.util.ConfigurationFilter;
import net.ihe.gazelle.xdstar.dsub.model.NOTIFConfiguration;
import net.ihe.gazelle.xdstar.dsub.model.NOTIFConfigurationQuery;

import org.apache.commons.collections.ListUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  
 * @author                	Abderrazek Boufahja
 *
 */

@Name("iheNotifManagerBean")
@Scope(ScopeType.PAGE)
public class IHENotifManager extends CommonIHENotifManager implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static String TRANSACTION_TYPE = "DSUB";
	
	private static Logger log = LoggerFactory.getLogger(IHENotifManager.class);
	
	private String selectedMessageType = null;

	public String getSelectedMessageType() {
		return selectedMessageType;
	}

	public void setSelectedMessageType(String selectedMessageType) {
		this.selectedMessageType = selectedMessageType;
	}

	public void listAllConfigurations()
	{
		NOTIFConfigurationQuery rc = new NOTIFConfigurationQuery();
		if (selectedAffinityDomain != null) rc.listUsages().affinity().eq(selectedAffinityDomain);
		if (selectedTransaction != null) rc.listUsages().transaction().eq(selectedTransaction);
		rc.name().order(true);
		ConfigurationFilter.filterConfigurationQueryAccodingToUserCredential(rc);
		List<NOTIFConfiguration> lpc = rc.getList(); 
		configurations = ListUtils.typedList(lpc,  NOTIFConfiguration.class);
	}
	
	public String createMessage(){
		return createMessage(true);
	}

	@Override
	public String getTheNameOfThePage() {
		String res = this.getNameOfTheAffinityDomainTransaction();
		if (res == null || res.equals("")) res = "Document Metadata Notify [ITI-53]";
		return res;
	}
	
	public void initBean() {
		super.init();
		this.selectedAffinityDomain = AffinityDomain.getAffinityDomainByKeyword("IHE_DSUB");
		this.selectedTransaction = Transaction.GetTransactionByKeyword("ITI-53");
		this.initMetadatas();
		this.initFromMenu = true;
	}
	
	public List<String> getListOfMessageTypesForSelectedTransaction(){
		ArrayList<String> al = new ArrayList<String>();
		al.add("ihe:FullDocumentEntry");
		al.add("ihe:MinimalDocumentEntry");
		al.add("ihe:FolderMetadata");
		al.add("ihe:SubmissionSetMetadata");
		return al;
	}
	
	public void init() {
		super.init();
		this.selectedMessageType = null;
	}
	
}
