package net.ihe.gazelle.xdstar.dsub.model;



import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;

@Entity(name="subConfiguration")
@DiscriminatorValue("SUB")
public class SUBConfiguration extends SystemConfiguration implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public SUBConfiguration(){}
	
	public SUBConfiguration(SUBConfiguration configuration) {
		this.name = configuration.getName().concat("_COPY");
		this.url= configuration.getUrl();
		this.systemName = configuration.getSystemName();
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}
	
}
