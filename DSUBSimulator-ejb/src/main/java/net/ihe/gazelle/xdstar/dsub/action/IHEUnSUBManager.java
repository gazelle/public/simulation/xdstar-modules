/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.dsub.action;

import net.ihe.gazelle.dsub.b2.SubscribeType;
import net.ihe.gazelle.dsub.b2.UnsubscribeResponseType;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.xdstar.common.action.CommonSimulatorManager;
import net.ihe.gazelle.xdstar.common.model.ContentType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import net.ihe.gazelle.xdstar.comon.util.ConfigurationFilter;
import net.ihe.gazelle.xdstar.core.MessageSenderCommon;
import net.ihe.gazelle.xdstar.dsub.model.DSUBMessage;
import net.ihe.gazelle.xdstar.dsub.model.SUBConfiguration;
import net.ihe.gazelle.xdstar.dsub.model.SUBConfigurationQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author Abderrazek Boufahja
 */

@Name("iheUnsubManagerBean")
@Scope(ScopeType.PAGE)
public class IHEUnSUBManager extends CommonSimulatorManager<SUBConfiguration, DSUBMessage> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static String TRANSACTION_TYPE = "DSUB";


    private static Logger log = LoggerFactory.getLogger(IHEUnSUBManager.class);

    private String selectedSubscriptionID;


    public String getSelectedSubscriptionID() {
        return selectedSubscriptionID;
    }

    public void setSelectedSubscriptionID(String selectedSubscriptionID) {
        this.selectedSubscriptionID = selectedSubscriptionID;
    }

    public void listAllConfigurations() {
        SUBConfigurationQuery rc = new SUBConfigurationQuery();
        if (selectedAffinityDomain != null) {
            rc.listUsages().affinity().eq(selectedAffinityDomain);
        }
        if (selectedTransaction != null) {
            rc.listUsages().transaction().eq(selectedTransaction);
        }
        rc.name().order(true);
        ConfigurationFilter.filterConfigurationQueryAccodingToUserCredential(rc);
        configurations = rc.getList();
    }

    public void sendMessage() {
        message = new DSUBMessage();
        message.setMessageType("DSUB-UnSubscribe");
        message.setTransaction(selectedTransaction);
        message.setAffinityDomain(selectedAffinityDomain);
        message.setContentType(ContentType.SOAP_MESSAGE);
        message.setTimeStamp(new Date());
        message.setGazelleDriven(false);
        message.setConfiguration(selectedConfiguration);

        MetadataMessage sent = new MetadataMessage();
        String messageToSend = this.createMessage();
        sent.setMessageContent(messageToSend);
        sent.setMessageType(MetadataMessageType.UNSUBSCRIBE_REQUEST);
        message.setSentMessageContent(sent);
        message = DSUBMessage.storeMessage(message);

        MessageSenderCommon sender = new MessageSenderCommon(selectedConfiguration.getUrl(), message);
        try {
            sender.sendMessageSOAP(MetadataMessageType.UNSUBSCRIBE_RESPONSE);
            message = (DSUBMessage) sender.getRequest();
            message = (DSUBMessage) DSUBMessage.storeMessage(message);
        } catch (Exception e) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.xca.initgw.errorWhenSendingMessage", e.getMessage
                    ());
        }
        Contexts.getSessionContext().set("messagesToDisplay", message);
        displayResultPanel = true;
    }

    public void updateSpecificClient() {
        this.initUnsubscribeMessage();
    }

    private void initUnsubscribeMessage() {
        this.selectedSubscriptionID = "";
    }

    public List<AffinityDomain> listAllAffinityDomains() {
        return listAllAffinityDomains(TRANSACTION_TYPE);
    }

    public String createMessage() {
        String res = UnsubTemplate.templ;
        String receiverUrl = this.selectedConfiguration.getUrl();
        res = res.replace("__TO__", receiverUrl);
        res = res.replace("__UUID__", UUID.randomUUID().toString());
        String path = "";
        if (!receiverUrl.endsWith("/")) {
            path = "/";
        }
        res = res.replace("__SubscriptionId__", path + this.selectedSubscriptionID);
        return res;
    }

    protected String getUnsubscribeResponseTypeAsString(UnsubscribeResponseType aqr) {
        try {
            JAXBContext jc = JAXBContext.newInstance(SubscribeType.class);
            Marshaller m = jc.createMarshaller();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.marshal(aqr, baos);
            return baos.toString();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public String getTheNameOfThePage() {
        String res = this.getNameOfTheAffinityDomainTransaction();
        if (res == null || res.equals("")) {
            res = "Document Metadata Subscribe - Unsubscribe Request [ITI-52]";
        }
        return res;
    }

    public String previewMessage() {
        String res = this.createMessage();
        return res;
    }

    public void initBean() {
        super.init();
        this.selectedAffinityDomain = AffinityDomain.getAffinityDomainByKeyword("IHE_DSUB");
        this.selectedTransaction = Transaction.GetTransactionByKeyword("ITI-52");
        this.initFromMenu = true;
    }

}
