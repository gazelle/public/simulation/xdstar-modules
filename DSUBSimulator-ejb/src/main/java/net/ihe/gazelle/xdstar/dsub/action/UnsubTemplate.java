package net.ihe.gazelle.xdstar.dsub.action;

public class UnsubTemplate {

	public static final String templ = "<s:Envelope xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\"\n" + 
    "	xmlns:a=\"http://www.w3.org/2005/08/addressing\"\n" + 
    "	xmlns:wsnt=\"http://docs.oasis-open.org/wsn/b-2\"\n" + 
    "	xmlns:ihe=\"urn:ihe:iti:dsub:2009\">\n" + 
    "	<s:Header>\n" + 
    "	    <a:Action>http://docs.oasis-open.org/wsn/bw-2/SubscriptionManager/UnsubscribeRequest</a:Action>\n" + 
    "	    <a:MessageID>__UUID__</a:MessageID>\n" + 
    "	    <a:To>__TO____SubscriptionId__</a:To>\n" + 
    "	</s:Header>\n" + 
    "	<s:Body>\n" + 
    "	    <wsnt:Unsubscribe/>\n" + 
    "	</s:Body>\n" + 
    "</s:Envelope>";
}
