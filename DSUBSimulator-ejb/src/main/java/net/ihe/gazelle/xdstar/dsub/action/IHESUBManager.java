/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.dsub.action;

import net.ihe.gazelle.dsub.addr.EndpointReferenceType;
import net.ihe.gazelle.dsub.b2.FilterType;
import net.ihe.gazelle.dsub.b2.SubscribeType;
import net.ihe.gazelle.dsub.b2.TopicExpressionType;
import net.ihe.gazelle.metadata.common.RIMGenerator;
import net.ihe.gazelle.rim.AdhocQueryType;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.common.xua.SignatureException;
import net.ihe.gazelle.xdstar.common.model.ContentType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import net.ihe.gazelle.xdstar.comon.util.ConfigurationFilter;
import net.ihe.gazelle.xdstar.core.MessageSenderCommon;
import net.ihe.gazelle.xdstar.core.soap.SOAPRequestBuilder;
import net.ihe.gazelle.xdstar.dsub.model.DSUBMessage;
import net.ihe.gazelle.xdstar.dsub.model.SUBConfiguration;
import net.ihe.gazelle.xdstar.dsub.model.SUBConfigurationQuery;
import net.ihe.gazelle.xdstar.rsq.common.CommonAdhocQueryManager;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Abderrazek Boufahja
 */

@Name("ihesubManagerBean")
@Scope(ScopeType.PAGE)
public class IHESUBManager extends CommonAdhocQueryManager<SUBConfiguration, DSUBMessage> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static String TRANSACTION_TYPE = "DSUB";


    private static Logger log = LoggerFactory.getLogger(IHESUBManager.class);

    private String notificationAddress;


    public String getNotificationAddress() {
        return notificationAddress;
    }

    public void setNotificationAddress(String notificationAddress) {
        this.notificationAddress = notificationAddress;
    }

    public void listAllConfigurations() {
        SUBConfigurationQuery rc = new SUBConfigurationQuery();
        if (selectedAffinityDomain != null) {
            rc.listUsages().affinity().eq(selectedAffinityDomain);
        }
        if (selectedTransaction != null) {
            rc.listUsages().transaction().eq(selectedTransaction);
        }
        rc.name().order(true);
        ConfigurationFilter.filterConfigurationQueryAccodingToUserCredential(rc);
        configurations = rc.getList();
    }

    public void sendMessage() {
        message = new DSUBMessage();
        message.setMessageType("DSUBSubscribe");
        message.setTransaction(selectedTransaction);
        message.setAffinityDomain(selectedAffinityDomain);
        message.setContentType(ContentType.SOAP_MESSAGE);
        message.setTimeStamp(new Date());
        message.setGazelleDriven(false);
        message.setConfiguration(selectedConfiguration);

        MetadataMessage sent = new MetadataMessage();
        String messageToSend = this.createMessage();
        sent.setMessageContent(messageToSend);
        sent.setMessageType(MetadataMessageType.SUBSCRIBE_REQUEST);
        message.setSentMessageContent(sent);
        message = DSUBMessage.storeMessage(message);

        MessageSenderCommon sender = new MessageSenderCommon(selectedConfiguration.getUrl(), message);
        try {
            sender.sendMessageSOAP(MetadataMessageType.SUBSCRIBE_RESPONSE);
            message = (DSUBMessage) sender.getRequest();
            message = (DSUBMessage) DSUBMessage.storeMessage(message);
        } catch (Exception e) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.xca.initgw.errorWhenSendingMessage", e.getMessage
                    ());
        }
        Contexts.getSessionContext().set("messagesToDisplay", message);
        displayResultPanel = true;
    }

    public void updateSpecificClient() {
        this.initAdhocQueryByMessageType();
    }

    public List<AffinityDomain> listAllAffinityDomains() {
        return listAllAffinityDomains(TRANSACTION_TYPE);
    }

    public String createMessage() {
        String res = "";
        SubscribeType sub = new SubscribeType();
        sub.setConsumerReference(new EndpointReferenceType());
        sub.getConsumerReference().setAddress(this.notificationAddress);
        sub.setFilter(new FilterType());
        sub.getFilter().setTopicExpression(new TopicExpressionType());
        sub.getFilter().getTopicExpression().setDialect("http://docs.oasis-open.org/wsn/t-1/TopicExpression/Simple");
        sub.getFilter().getTopicExpression().setValue("ihe:MinimalDocumentEntry");
        sub.getFilter().setAdhocQuery(this.getAdhocQuery());

        res = this.getSubscribeTypeAsString(sub);

        String receiverUrl = this.selectedConfiguration.getUrl();
        try {
            res = SOAPRequestBuilder.createSOAPMessage(res, useXUA, praticianID, attributes, null, receiverUrl, "http://docs.oasis-open" +
                    ".org/wsn/bw-2/NotificationProducer/SubscribeRequest", false);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        }
        return res;
    }

    protected String getSubscribeTypeAsString(SubscribeType aqr) {
        try {
            JAXBContext jc = JAXBContext.newInstance(SubscribeType.class);
            Marshaller m = jc.createMarshaller();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.marshal(aqr, baos);
            return baos.toString();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public String getTheNameOfThePage() {
        String res = this.getNameOfTheAffinityDomainTransaction();
        if (res == null || res.equals("")) {
            res = "Document Metadata Subscribe - Subscribe Request [ITI-52]";
        }
        return res;
    }

    public String previewMessage() {
        String res = this.createMessage();
        return res;
    }

    public void initBean() {
        super.init();
        this.selectedAffinityDomain = AffinityDomain.getAffinityDomainByKeyword("IHE_DSUB");
        this.selectedTransaction = Transaction.GetTransactionByKeyword("ITI-52");
//		AdhocQueryMetadataQuery quer = new AdhocQueryMetadataQuery();
//		quer.name().eq("FindDocuments");
//		quer.usages().transaction().keyword().eq("ITI-52");
//		this.selectedMessageType = quer.getUniqueResult();
        this.initFromMenu = true;
    }

    @Override
    public void initAdhocQueryByMessageType() {
        this.adhocQuery = new AdhocQueryType();
        if (this.selectedMessageType != null) {
            RIMGenerator.generateAdhocQueryTypeChild(this.selectedMessageType, this.adhocQuery);
            this.initListOptionalMetadata();
            this.displayResultPanel = false;
        }
    }

}
