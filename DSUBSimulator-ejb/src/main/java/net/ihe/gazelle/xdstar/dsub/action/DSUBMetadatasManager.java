package net.ihe.gazelle.xdstar.dsub.action;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;

import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.metadata.common.RIMGenerator;
import net.ihe.gazelle.metadata.model.ClassificationMetaData;
import net.ihe.gazelle.metadata.model.ExternalIdentifierMetadata;
import net.ihe.gazelle.metadata.model.ExtrinsicObjectMetadata;
import net.ihe.gazelle.metadata.model.RegistryObjectMetadata;
import net.ihe.gazelle.metadata.model.SlotMetadata;
import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.InternationalStringType;
import net.ihe.gazelle.rim.LocalizedStringType;
import net.ihe.gazelle.rim.ObjectRefType;
import net.ihe.gazelle.rim.RegistryObjectType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.simulator.common.model.Concept;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.xdstar.common.action.ValueSetContainer;
import net.ihe.gazelle.xdstar.comon.util.RIMBuilderCommon;
import net.ihe.gazelle.xdstar.comon.util.SVSConsumerForSimulator;
import net.ihe.gazelle.xdstar.conf.model.RepositoryConfiguration;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.action.DocumentEntryManagement;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.action.XDSCommonModule;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.action.XDSDocEditor;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.action.XDSFolderEditor;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.action.XDSFolderManagement;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.action.XDSSubmissionSetEditor;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.action.XDSSubmissionSetManagement;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.PNRMessageType;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSDocumentEntry;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSFolder;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSSubmissionSet;

import org.jboss.seam.Component;
import org.richfaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DSUBMetadatasManager implements Serializable,XDSDocEditor,XDSFolderEditor, XDSSubmissionSetEditor {
	
	private static final String CODING_SCHEME = "codingScheme";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Logger log = LoggerFactory.getLogger(DSUBMetadatasManager.class);
	
	protected static String XDSDocumentEntry_UUID = "urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1";
	
	protected XDSDocumentEntry selectedXDSDocumentEntry;

	protected Boolean adviseNodeOpened = true;
	
	protected Boolean editXDSDocumentEntry;
	
	protected SlotType1 selectedSlotType;
	
	protected String valueToAdd;
	
	protected InternationalStringType selectedInternationalStringType;
	
	protected List<SelectItem> listOptionalMetadata = new ArrayList<SelectItem>();
	
	protected String selectedMetadata;
	
	protected ClassificationType selectedClassificationType;
	
	protected String selectedSlotMetadata;
	
	protected Integer fileIndex = 1;
	
	protected List<XDSDocumentEntry> listXDSDocumentEntry;
	
	protected List<XDSFolder> listXDSFolder;

	private AffinityDomain selectedAffinityDomain;

	private Transaction selectedTransaction;

	public AffinityDomain getSelectedAffinityDomain() {
		return selectedAffinityDomain;
	}

	public void setSelectedAffinityDomain(AffinityDomain selectedAffinityDomain) {
		this.selectedAffinityDomain = selectedAffinityDomain;
	}

	public Transaction getSelectedTransaction() {
		return selectedTransaction;
	}

	public void setSelectedTransaction(Transaction selectedTransaction) {
		this.selectedTransaction = selectedTransaction;
	}

	public List<XDSDocumentEntry> getListXDSDocumentEntry() {
		return listXDSDocumentEntry;
	}

	public void setListXDSDocumentEntry(List<XDSDocumentEntry> listXDSDocumentEntry) {
		this.listXDSDocumentEntry = listXDSDocumentEntry;
	}

	public String getSelectedSlotMetadata() {
		return selectedSlotMetadata;
	}

	public void setSelectedSlotMetadata(String selectedSlotMetadata) {
		this.selectedSlotMetadata = selectedSlotMetadata;
	}

	public ClassificationType getSelectedClassificationType() {
		return selectedClassificationType;
	}

	public void setSelectedClassificationType(
			ClassificationType selectedClassificationType) {
		this.selectedClassificationType = selectedClassificationType;
	}

	public String getSelectedMetadata() {
		return selectedMetadata;
	}

	public void setSelectedMetadata(String selectedMetadata) {
		this.selectedMetadata = selectedMetadata;
	}

	public List<SelectItem> getListOptionalMetadata() {
		return listOptionalMetadata;
	}

	public void setListOptionalMetadata(List<SelectItem> listOptionalMetadata) {
		this.listOptionalMetadata = listOptionalMetadata;
	}

	public InternationalStringType getSelectedInternationalStringType() {
		return selectedInternationalStringType;
	}

	public void setSelectedInternationalStringType(
			InternationalStringType selectedInternationalStringType) {
		this.selectedInternationalStringType = selectedInternationalStringType;
	}

	public SlotType1 getSelectedSlotType() {
		return selectedSlotType;
	}

	public void setSelectedSlotType(SlotType1 selectedSlotType) {
		this.selectedSlotType = selectedSlotType;
	}

	public String getValueToAdd() {
		return valueToAdd;
	}

	public void setValueToAdd(String valueToAdd) {
		this.valueToAdd = valueToAdd;
	}

	public Boolean getEditXDSDocumentEntry() {
		return editXDSDocumentEntry;
	}

	public void setEditXDSDocumentEntry(Boolean editXDSDocumentEntry) {
		this.editXDSDocumentEntry = editXDSDocumentEntry;
	}

	public Boolean getAdviseNodeOpened() {
		return adviseNodeOpened;
	}

	public void setAdviseNodeOpened(Boolean adviseNodeOpened) {
		this.adviseNodeOpened = adviseNodeOpened;
	}

	public XDSDocumentEntry getSelectedXDSDocumentEntry() {
		return selectedXDSDocumentEntry;
	}

	public void setSelectedXDSDocumentEntry(
			XDSDocumentEntry selectedXDSDocumentEntry) {
		this.selectedXDSDocumentEntry = selectedXDSDocumentEntry;
	}

	public List<PNRMessageType> availableMTs(){
		return Arrays.asList(PNRMessageType.values());
	}
	
	public GazelleTreeNodeImpl<Object> getTreeNodePublish(){
		GazelleTreeNodeImpl<Object> rootNode = new GazelleTreeNodeImpl<Object>();
		
		GazelleTreeNodeImpl<Object> publishTreeNode=new GazelleTreeNodeImpl<Object>();
		publishTreeNode.setData("DSUB metadatas");
		rootNode.addChild(1, publishTreeNode);

		int i = 0;

		if (this.getListXDSDocumentEntry() != null){
			for (XDSDocumentEntry xxd : this.getListXDSDocumentEntry()) {
				GazelleTreeNodeImpl<Object> ddd=new GazelleTreeNodeImpl<Object>();
				ddd.setData(xxd);
				publishTreeNode.addChild(i, ddd);
				i++;	
			}

		}
		if (this.listXDSFolder != null){
			for (XDSFolder folder : this.listXDSFolder) {
				GazelleTreeNodeImpl<Object> ddd=new GazelleTreeNodeImpl<Object>();
				ddd.setData(folder);
				publishTreeNode.addChild(i, ddd);
				i++;	
			}
		}
		if (this.listXDSSubmissionSet != null){
			for (XDSSubmissionSet submission : this.listXDSSubmissionSet) {
				GazelleTreeNodeImpl<Object> ddd=new GazelleTreeNodeImpl<Object>();
				ddd.setData(submission);
				publishTreeNode.addChild(i, ddd);
				i++;	
			}
		}
		if (this.listObjectRef != null){
			for (ObjectRefType objectRef : this.listObjectRef) {
				GazelleTreeNodeImpl<Object> ddd=new GazelleTreeNodeImpl<Object>();
				ddd.setData(objectRef);
				publishTreeNode.addChild(i, ddd);
				i++;	
			}
		}
		return rootNode;
	}

	public void init(AffinityDomain aff, Transaction trans){
		this.listXDSDocumentEntry = new ArrayList<XDSDocumentEntry>();
		this.listXDSFolder = new ArrayList<XDSFolder>();
		this.listXDSSubmissionSet = new ArrayList<XDSSubmissionSet>();
		this.listObjectRef = new ArrayList<ObjectRefType>();
		this.selectedXDSDocumentEntry = null;
		this.selectedAffinityDomain = aff;
		this.selectedTransaction = trans;
		this.editXDSDocumentEntry = false;
		this.editObjectRef = false;
		this.setEditXDSFolder(false);
		this.initListOptionalMetadata();
	}
	
	public void addDocumentEntry(){
		this.editXDSDocumentEntry(this.generateXDSDocumentEntry());
		this.listXDSDocumentEntry.add(this.selectedXDSDocumentEntry);
	}
	
	private XDSDocumentEntry generateXDSDocumentEntry(){
		return DocumentEntryManagement.generateXDSDocumentEntry(this, this.selectedAffinityDomain, this.selectedTransaction);
	}
	
	public void editXDSDocumentEntry(XDSDocumentEntry xx){
		this.selectedXDSDocumentEntry = xx;
		this.editXDSDocumentEntry = true;
		this.editSubmissionSet = false;
		this.editXDSFolder = false;
		this.editObjectRef = false;
	}
	
	public String getDisplayNameExt(ExternalIdentifierType ext){
		return RIMGenerator.extWeak.get(ext).getDisplayAttributeName();
	}
	
	public String getDisplayNameClass(ClassificationType ext){
		return RIMGenerator.clWeak.get(ext).getDisplayAttributeName();
	}
	
	public String rownSpanClass(ClassificationType cl){
		int i = 2;
		if (cl != null){
			SlotType1 codingScheme = this.getCodingSchemeSlot(cl);
			if (codingScheme != null){
				SlotMetadata slm = RIMGenerator.slWeak.get(codingScheme);
				if ((slm.getValueset() != null) && (!slm.getValueset().equals("")) ){
					i = 0;
				}
			}
		}
		i = i + cl.getSlot().size();
		if (this.hasOptionalSlotYet(cl)) i = i + 1;
		return String.valueOf(i);
	}
	
	private SlotType1 getCodingSchemeSlot(ClassificationType cl){
		if (cl != null){
			if (cl.getSlot() != null){
				for (SlotType1 sl : cl.getSlot()) {
					if (sl.getName().equals(CODING_SCHEME)){
						return sl;
					}
				}
			}
		}
		return null;
	}
	
	public void initSelectedSlotType(SlotType1 sl){
		this.selectedSlotType = sl;
		this.valueToAdd = null;
		this.fileIndex = 1;
	}
	
	public boolean canAddName(InternationalStringType name){
		if (name != null){
			if ((name.getLocalizedString() != null)&& (name.getLocalizedString().size()>0)){
				return false;
			}
		}
		return true;
	}
	
	public void initSelectedInternationalStringType(ClassificationType cl){
		if (cl.getName() == null) cl.setName(new InternationalStringType());
		this.selectedInternationalStringType = cl.getName();
		this.valueToAdd = null;
	}
	
	public void setValueOfTheName(String val){
		this.selectedInternationalStringType.setLocalizedString(new ArrayList<LocalizedStringType>());
		this.selectedInternationalStringType.getLocalizedString().add(new LocalizedStringType());
		this.selectedInternationalStringType.getLocalizedString().get(0).setValue(valueToAdd);
	}
	
	public void initListOptionalMetadata(){
		EntityManager em = (EntityManager) Component.getInstance("entityManager");
		this.listOptionalMetadata = new ArrayList<SelectItem>();
		this.listOptionalMetadata.add(new SelectItem(null, "please select.."));
		RegistryObjectMetadata ss = null;
		RegistryObjectType rot = null;
		if (editXDSDocumentEntry){
			ss = RIMGenerator.docWeak.get(this.selectedXDSDocumentEntry);
			rot = this.selectedXDSDocumentEntry;
			ss = em.find(RegistryObjectMetadata.class, ss.getId());
		}
		else if (this.editXDSFolder){
			ss = RIMGenerator.rpWeak.get(this.selectedXDSFolder);
			rot = this.selectedXDSFolder;
			ss = em.find(RegistryObjectMetadata.class, ss.getId());
		}
		else if (this.editSubmissionSet){
			ss = RIMGenerator.rpWeak.get(this.selectedSubmissionSet	);
			rot = this.selectedSubmissionSet;
			ss = em.find(RegistryObjectMetadata.class, ss.getId());
		}
		
		if (ss != null){
			if (ss.getClassification_optional() != null){
				for (ClassificationMetaData clm : ss.getClassification_optional()) {
					boolean isused = false;
					for (ClassificationType cl : rot.getClassification()) {
						if (cl.getClassificationScheme().equals(clm.getUuid())) isused = true;
					}
					if (!isused){
						this.listOptionalMetadata.add(new SelectItem(clm.getDisplayAttributeName(), clm.getDisplayAttributeName()));
					}
				}
			}
			
			if (ss.getExt_identifier_optional() != null){
				for (ExternalIdentifierMetadata clm : ss.getExt_identifier_optional()) {
					boolean isused = false;
					for (ExternalIdentifierType cl : rot.getExternalIdentifier()) {
						if (cl.getIdentificationScheme().equals(clm.getUuid())) isused = true;
					}
					if (!isused){
						this.listOptionalMetadata.add(new SelectItem(clm.getDisplayAttributeName(), clm.getDisplayAttributeName()));
					}
				}
			}
			
			if (ss.getSlot_optional() != null){
				for (SlotMetadata slm : ss.getSlot_optional()) {
					boolean isused = false;
					for (SlotType1 sl : rot.getSlot()) {
						if (sl.getName().equals(slm.getName())) isused = true;
					}
					if (!isused){
						this.listOptionalMetadata.add(new SelectItem(slm.getName(), slm.getName()));
					}
				}
			}
		}
		this.selectedMetadata = null;
	}
	
	public boolean canAddValueToSlot(SlotType1 sl){
		if (sl != null){
			SlotMetadata slm = RIMGenerator.slWeak.get(sl);
			if (slm != null){
				if ((slm.getMultiple() != null) && (slm.getMultiple() == false)){
					if ((sl.getValueList() != null)&& (sl.getValueList().getValue().size()>0)){
						return false;
					}
				}
			}
		}
		return true;
	}
	
	public boolean hasOptionalSlotYet(ClassificationType cl){
		ClassificationMetaData clm = RIMGenerator.clWeak.get(cl);
		EntityManager em = (EntityManager)Component.getInstance("entityManager");
		clm = em.find(ClassificationMetaData.class, clm.getId());
		if (clm != null){
			if ((clm.getSlot_optional() != null) && (clm.getSlot_optional().size()>0)){
				for (SlotMetadata slm : clm.getSlot_optional()) {
					boolean present = false;
					for (SlotType1 sl : cl.getSlot()) {
						if (slm.getName().equals(sl.getName())){
							present = true;
							break;
						}
					}
					if (!present) return true;
				}
			}
		}
		return false;
	}
	
	public List<SelectItem> getListOptionalSlotMetadata(){
		List<SelectItem> res = new ArrayList<SelectItem>();
		res.add(new SelectItem(null, "please select.."));
		if (this.selectedClassificationType != null){
			ClassificationMetaData clm = RIMGenerator.clWeak.get(this.selectedClassificationType);
			for (SlotMetadata slm : clm.getSlot_optional()) {
				boolean alreadyExist = false;
				for (SlotType1 sl : this.selectedClassificationType.getSlot()) {
					if (sl.getName().equals(slm.getName())){
						alreadyExist = true;
					}
				}
				if (!alreadyExist){
					res.add(new SelectItem(slm.getName(), slm.getName()));
				}
			}
		}
		this.selectedSlotMetadata = null;
		return res;
	}
	
	public void addSelectedSlotToClassification(){
		if (this.selectedClassificationType != null){
			if (this.selectedSlotMetadata != null){
				ClassificationMetaData clm = RIMGenerator.clWeak.get(this.selectedClassificationType);
				for (SlotMetadata slm : clm.getSlot_optional()) {
					if (slm.getName().equals(this.selectedSlotMetadata)){
						SlotType1 sl = RIMGenerator.generateSlot(slm);
						this.selectedClassificationType.getSlot().add(sl);
						break;
					}
				}
			}
		}
		this.selectedSlotMetadata = null;
	}
	
	public String getOptionalitySlotOnClassification(SlotType1 sl, ClassificationType cl){
		ClassificationMetaData clm = RIMGenerator.clWeak.get(cl);
		String res = "O";
		for (SlotMetadata slm : clm.getSlot_required()) {
			if (slm.getName().equals(sl.getName())) {
				res = "R";
				break;
			}
		}
		return res;
	}
	
	//---------------XDS Folder -------------------
	
	// -------------------------------------------
	
	// -------XDS Docuement Entry -----------------------------
	
	public String getOptionalityExtOnSelectedXDSDocumentEntry(ExternalIdentifierType ext){
		ExtrinsicObjectMetadata rpm = RIMGenerator.docWeak.get(this.selectedXDSDocumentEntry);
		ExternalIdentifierMetadata extm = RIMGenerator.extWeak.get(ext);
		for (ExternalIdentifierMetadata ee : rpm.getExt_identifier_required()) {
			if (extm.getUuid().equals(ee.getUuid())) return "R";
		}
		return "O";
	}
	
	public void removeExtFromSelectedXDSDocumentEntry(ExternalIdentifierType ext){
		this.selectedXDSDocumentEntry.getExternalIdentifier().remove(ext);
		this.initListOptionalMetadata();
	}
	
	public String getOptionalitySlotOnSelectedXDSDocumentEntry(SlotType1 sl){
		EntityManager em = (EntityManager)Component.getInstance("entityManager");
		ExtrinsicObjectMetadata extrm = RIMGenerator.docWeak.get(this.selectedXDSDocumentEntry);
		SlotMetadata slm = RIMGenerator.slWeak.get(sl);
		extrm = em.find(ExtrinsicObjectMetadata.class, extrm.getId());
		if (extrm.getSlot_required()!= null){
			for (SlotMetadata ee : extrm.getSlot_required()) {
				if (slm.getName().equals(ee.getName())) return "R";
			}
		}
		return "O";
	}
	
	public void removeSlotFromSelectedXDSDocumentEntry(SlotType1 sl){
		this.selectedXDSDocumentEntry.getSlot().remove(sl);
		this.initListOptionalMetadata();
	}
	
	public String getOptionalityClassOnSelectedXDSDocumentEntry(ClassificationType ext){
		ExtrinsicObjectMetadata rpm = RIMGenerator.docWeak.get(this.selectedXDSDocumentEntry);
		ClassificationMetaData extm = RIMGenerator.clWeak.get(ext);
		if (rpm.getClassification_required() != null){
			for (ClassificationMetaData ee : rpm.getClassification_required()) {
				if (extm.getUuid().equals(ee.getUuid())) return "R";
			}
		}
		return "O";
	}
	
	public void removeClassificationFromSelectedXDSDocumentEntry(ClassificationType cl){
		this.selectedXDSDocumentEntry.getClassification().remove(cl);
		this.initListOptionalMetadata();
	}
	
	public boolean isValueSet(ClassificationType cl){
		boolean res = false;
		if (cl != null){
			if (cl.getSlot() != null){
				for (SlotType1 sl : cl.getSlot()) {
					if (sl.getName().equals(CODING_SCHEME)){
						SlotMetadata slm = RIMGenerator.slWeak.get(sl);
						if ((slm.getValueset() != null) && (!slm.getValueset().equals(""))){
							return true;
						}
					}
				}
			}
		}
		return res;	
			
	}
	
	public List<SelectItem> listValueSet(ClassificationType cl){
		List<SelectItem> res = new ArrayList<SelectItem>();
		res.add(new SelectItem(null, "please select .."));
		if (cl != null){
			for (SlotType1 sl : cl.getSlot()) {
				if (sl.getName().equals(CODING_SCHEME)){
					SlotMetadata slm = RIMGenerator.slWeak.get(sl);
					if ((slm.getValueset() != null)&& (!slm.getValueset().equals(""))){
						String[] listValueSet = slm.getValueset().split("\\s*,\\s*");
						for (String valueSet : listValueSet) {
							List<Concept> lc = ValueSetContainer.getListConcepts().get(valueSet);
							
							if (lc == null){
								lc = SVSConsumerForSimulator.getConceptsListFromValueSet(valueSet, null);
								if (lc != null){
									Collections.sort(lc);
								}
								ValueSetContainer.getListConcepts().put(valueSet, lc);
							}
							if (lc != null){
								for (Concept concept : lc) {
									SelectItem si = new SelectItem(concept.getCode(), concept.getDisplayName());
									res.add(si);
								}
							}
						}
					}
				}
			}
		}
		return res;
	}
	
	public void updateClassificationByValueSet(ClassificationType cl){
		if ((cl != null)){
			
			ClassificationMetaData clm = RIMGenerator.clWeak.get(cl);
			SlotMetadata slmc = null;
			if (clm.getSlot_required() != null){
				for (SlotMetadata slm : clm.getSlot_required()) {
					if (slm.getName().equals(CODING_SCHEME)){
						slmc = slm;
						break;
					}
				}
			}
			
			if (cl.getNodeRepresentation() == null){
				for (SlotType1 sl : cl.getSlot()) {
					if (sl.getName().equals(CODING_SCHEME)){
						sl.getValueList().setValue(new ArrayList<String>());
					}
				}
				return;
			}
			
			if (slmc != null){
				String valueSets = slmc.getValueset();
				if ((valueSets != null) && (!valueSets.equals(""))){
					String[] listValueSet =valueSets.split("\\s*,\\s*");
					for (String valueSet : listValueSet) {
						Concept cp = SVSConsumerForSimulator.getConceptForCode(valueSet, null, cl.getNodeRepresentation());
						if (cp != null){
							cl.setName(RIMBuilderCommon.createNameOrDescription(cp.getDisplayName()));
							for (SlotType1 sl : cl.getSlot()) {
								if (sl.getName().equals(CODING_SCHEME)){
									sl.getValueList().setValue(new ArrayList<String>());
									sl.getValueList().getValue().add(cp.getCodeSystem());
								}
							}
						}
					}
				}
			}
		}
	}
	
	public void addNewMetadataToXDSDocumentEntry(){
		RegistryObjectType rot = null;
		if (this.editSubmissionSet) {
			rot = this.selectedSubmissionSet;
		}
		if (this.editXDSDocumentEntry) {
			rot = this.selectedXDSDocumentEntry;
		}
		if (this.editXDSFolder) {
			rot = this.selectedXDSFolder;
		}
		if (this.editObjectRef) {
			log.error("problem to understand the new metadata !");
			return;
		}
		XDSCommonModule.addNewMetadataToRegistryObjectType(rot, selectedMetadata, this);
	}
	
	// --------------------------------------------------------
	
	public void deleteXDSDocument(XDSDocumentEntry xdsdoc){
		this.listXDSDocumentEntry.remove(xdsdoc);
		this.editXDSDocumentEntry = false;
	}
	
	public void randomFulFilDocEntryMetadata(){
		if (this.selectedXDSDocumentEntry != null){
			for (ClassificationType clas : this.selectedXDSDocumentEntry.getClassification()) {
				List<SelectItem> lsi = listValueSet(clas);
				if ((lsi != null) && lsi.size()>1){
					SelectItem si = lsi.get((int)Math.floor(Math.random()*(lsi.size()-2)) + 1);
					clas.setNodeRepresentation((String) si.getValue());
					this.updateClassificationByValueSet(clas);
				}
			}
		}
	}

	@Override
	public int getFileIndex() {
		return this.fileIndex;
	}

	@Override
	public void setFileIndex(int fileIndex) {
		this.fileIndex = fileIndex;
	}

	@Override
	public void uploadListener(FileUploadEvent event) {
		// NOT Used
	}

	@Override
	public void initSelectedMetadata() {
		// NOT Used
	}

	@Override
	public void init(AffinityDomain aff, Transaction trans, RepositoryConfiguration conf) {
		// NOT Used
	}

	private XDSFolder selectedXDSFolder;
	
	private Boolean editXDSFolder = false;
	
	private int folderIndex = 1;
	
	@Override
	public XDSFolder getSelectedXDSFolder() {
		return selectedXDSFolder;
	}

	@Override
	public void setSelectedXDSFolder(XDSFolder selectedXDSFolder) {
		this.selectedXDSFolder = selectedXDSFolder;
	}

	@Override
	public void editXDSFolder(XDSFolder xx) {
		this.editXDSDocumentEntry = false;
		this.setEditXDSFolder(true);
		this.selectedXDSFolder = xx;
		this.editSubmissionSet = false;
	}

	@Override
	public int getFolderIndex() {
		return folderIndex;
	}

	@Override
	public void setFolderIndex(int folderIndex) {
		this.folderIndex = folderIndex;
	}

	@Override
	public void addDocumentEntry(XDSFolder xx) {
		DocumentEntryManagement.addNewDocumentEntryToXDSFolder(xx, this, this.selectedAffinityDomain, this.selectedTransaction);
	}

	@Override
	public String getOptionalityExtOnSelectedXDSFolder(ExternalIdentifierType ext) {
		return XDSFolderManagement.getOptionalityExtOnSelectedXDSFolder(this.selectedXDSFolder, ext);
	}

	@Override
	public void removeExtFromSelectedXDSFolder(ExternalIdentifierType ext) {
		XDSFolderManagement.removeExtFromSelectedXDSFolder(this.selectedXDSFolder, ext, this);
	}

	@Override
	public String getOptionalitySlotOnSelectedXDSFolder(SlotType1 sl) {
		return XDSFolderManagement.getOptionalitySlotOnSelectedXDSFolder(this.selectedXDSFolder, sl);
	}

	@Override
	public void removeSlotFromSelectedXDSFolder(SlotType1 sl) {
		XDSFolderManagement.removeSlotFromSelectedXDSFolder(this.selectedXDSFolder, sl, this);
	}

	@Override
	public String getOptionalityClassOnSelectedXDSFolder(ClassificationType ext) {
		return XDSFolderManagement.getOptionalityClassOnSelectedXDSFolder(this.selectedXDSFolder, ext);
	}

	@Override
	public void removeClassificationFromSelectedXDSFolder(ClassificationType cl) {
		XDSFolderManagement.removeClassificationFromSelectedXDSFolder(this.selectedXDSFolder, cl, this);
	}

	@Override
	public void addFolder(XDSSubmissionSet xx) {
		// Not Used
	}
	
	public void addFolder(){
		this.editXDSFolder(XDSFolderManagement.generateXDSFolder(this, this.selectedAffinityDomain	, this.selectedTransaction));
		this.listXDSFolder.add(this.selectedXDSFolder);
	}

	public Boolean getEditXDSFolder() {
		return editXDSFolder;
	}

	public void setEditXDSFolder(Boolean editXDSFolder) {
		this.editXDSFolder = editXDSFolder;
	}

	public List<XDSFolder> getListXDSFolder(){
		return this.listXDSFolder;
	}
	
	public void deleteXDSFolder(XDSFolder xdsfolder){
		this.editXDSFolder = false;
		this.listXDSFolder.remove(xdsfolder);
	}
	
	// -------------------------------
	
	private XDSSubmissionSet selectedSubmissionSet;
	
	private Boolean editSubmissionSet = false;
	
	private List<XDSSubmissionSet> listXDSSubmissionSet;
	
	private Integer submissionIndex = 0;

	public Boolean getEditSubmissionSet() {
		return editSubmissionSet;
	}

	public void setEditSubmissionSet(Boolean editSubmissionSet) {
		this.editSubmissionSet = editSubmissionSet;
	}

	public List<XDSSubmissionSet> getListXDSSubmissionSet() {
		return listXDSSubmissionSet;
	}

	public void setListXDSSubmissionSet(List<XDSSubmissionSet> listXDSSubmissionSet) {
		this.listXDSSubmissionSet = listXDSSubmissionSet;
	}

	@Override
	public void setSubmissionSet(XDSSubmissionSet submissionSet) {
		this.selectedSubmissionSet = submissionSet;
	}

	@Override
	public XDSSubmissionSet getSubmissionSet() {
		return this.selectedSubmissionSet;
	}

	@Override
	public void editXDSSubmissionSet(XDSSubmissionSet xds) {
		this.editSubmissionSet = true;
		this.editXDSDocumentEntry = false;
		this.editXDSFolder = false;
		this.selectedSubmissionSet = xds;
	}

	@Override
	public void getTreeNodeSubmissionSet() {
		// Not Used
	}

	@Override
	public void addDocumentEntry(XDSSubmissionSet xx) {
		// Not Used
	}

	@Override
	public void editXDSSubmissionSet() {
		// Not Used
	}

	@Override
	public String getOptionalityExtOnSubmissionSet(ExternalIdentifierType ext) {
		return XDSSubmissionSetManagement.getOptionalityExtOnSubmissionSet(this.selectedSubmissionSet, ext);
	}

	@Override
	public String getOptionalityClassOnSubmissionSet(ClassificationType ext) {
		return XDSSubmissionSetManagement.getOptionalityClassOnSubmissionSet(this.selectedSubmissionSet, ext);
	}

	@Override
	public void addNewMetadataToSubmissionSet() {
		RegistryObjectType rot = null;
		if (this.editSubmissionSet) {
			rot = this.selectedSubmissionSet;
		}
		if (this.editXDSDocumentEntry) {
			rot = this.selectedXDSDocumentEntry;
		}
		if (this.editXDSFolder) {
			rot = this.selectedXDSFolder;
		}
		if (this.editObjectRef) {
			log.error("error to understand the new metadata !");
			return;
		}
		XDSCommonModule.addNewMetadataToRegistryObjectType(rot, selectedMetadata, this);
	}

	@Override
	public void removeClassificationFromSubmissionSet(ClassificationType cl) {
		XDSSubmissionSetManagement.removeClassificationFromSubmissionSet(this.selectedSubmissionSet, cl, this);
	}

	@Override
	public void removeExtFromSubmissionSet(ExternalIdentifierMetadata ext) {
		XDSSubmissionSetManagement.removeExtFromSubmissionSet(this.selectedSubmissionSet, ext, this);
	}

	@Override
	public String getOptionalitySlotOnSubmissionSet(SlotType1 sl) {
		return XDSSubmissionSetManagement.getOptionalitySlotOnSubmissionSet(this.selectedSubmissionSet, sl);
	}

	@Override
	public void removeSlotFromSubmissionSet(SlotType1 sl) {
		XDSSubmissionSetManagement.removeSlotFromSubmissionSet(this.selectedSubmissionSet, sl, this);
	}
	
	public void addSubmission() {
		XDSSubmissionSet sub = XDSSubmissionSetManagement.generateXDSSubmissionSet(this.selectedAffinityDomain	, this.selectedTransaction);
		sub.setTitle(sub.getTitle() + " " + ++submissionIndex);
		this.editXDSSubmissionSet(sub);
		this.listXDSSubmissionSet.add(this.selectedSubmissionSet);
	}
	
	public void deleteXDSSubmissionSet(XDSSubmissionSet submissionSet){
		this.editSubmissionSet = false;
		this.listXDSSubmissionSet.remove(submissionSet);
	}
	
	public boolean canAddFullDocument(String selectedMessageType){
		if (selectedMessageType != null && selectedMessageType.equals("ihe:FullDocumentEntry") || selectedMessageType.equals("DSUB-Publish")){
			return true;
		}
		return false;
	}
	
	public boolean canAddMinimalObjectRef(String selectedMessageType){
		if (selectedMessageType != null && 
				(selectedMessageType.equals("ihe:MinimalDocumentEntry"))
				){
			return true;
		}
		return false;
	}
	
	public boolean canAddFolder(String selectedMessageType){
		if (selectedMessageType != null && 
				(selectedMessageType.equals("ihe:FolderMetadata") || selectedMessageType.equals("DSUB-Publish"))
					){
			return true;
		}
		return false;
	}
	
	public boolean canAddSubmissionSet(String selectedMessageType){
		if (selectedMessageType != null && 
				(selectedMessageType.equals("ihe:SubmissionSetMetadata") || selectedMessageType.equals("DSUB-Publish"))
				){
			return true;
		}
		return false;
	}
	
	// ------------------------------------------------
	
	private List<ObjectRefType> listObjectRef;
	
	private ObjectRefType selectedObjectRef;
	
	private Boolean editObjectRef = false;
	
	public List<ObjectRefType> getListObjectRef() {
		return listObjectRef;
	}

	public ObjectRefType getSelectedObjectRef() {
		return selectedObjectRef;
	}

	public void setSelectedObjectRef(ObjectRefType selectedObjectRef) {
		this.selectedObjectRef = selectedObjectRef;
	}

	public Boolean getEditObjectRef() {
		return editObjectRef;
	}

	public void setEditObjectRef(Boolean editObjectRef) {
		this.editObjectRef = editObjectRef;
	}

	public void addMinimalObjectRef(){
		this.editXDSObjectRef(new ObjectRefType());
		this.selectedObjectRef.setId("urn:uuid:" + UUID.randomUUID().toString());
		this.listObjectRef.add(this.selectedObjectRef);
	}
	
	public void editXDSObjectRef(ObjectRefType xx){
		this.selectedObjectRef = xx;
		this.editObjectRef = true;
		this.editSubmissionSet = false;
		this.editXDSFolder = false;
		this.editSubmissionSet = false;
	}
	
	public void deleteObjectRef(ObjectRefType objectRef){
		this.listObjectRef.remove(objectRef);
		this.editObjectRef = false;
	}
	
}
