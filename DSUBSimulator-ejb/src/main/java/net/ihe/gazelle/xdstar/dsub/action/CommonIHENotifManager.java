/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.dsub.action;

import com.rits.cloning.Cloner;
import net.ihe.gazelle.dsub.addr.EndpointReferenceType;
import net.ihe.gazelle.dsub.b2.MessageType;
import net.ihe.gazelle.dsub.b2.NotificationMessageHolderType;
import net.ihe.gazelle.dsub.b2.NotifyType;
import net.ihe.gazelle.dsub.b2.TopicExpressionType;
import net.ihe.gazelle.lcm.SubmitObjectsRequestType;
import net.ihe.gazelle.rim.*;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.xua.SignatureException;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.xdstar.common.action.CommonSimulatorManager;
import net.ihe.gazelle.xdstar.common.model.ContentType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import net.ihe.gazelle.xdstar.comon.util.RIMBuilderCommon;
import net.ihe.gazelle.xdstar.core.MessageSenderCommon;
import net.ihe.gazelle.xdstar.core.soap.SOAPRequestBuilder;
import net.ihe.gazelle.xdstar.dsub.model.DSUBMessage;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSDocumentEntry;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSFolder;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSSubmissionSet;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author Abderrazek Boufahja
 */

public abstract class CommonIHENotifManager extends CommonSimulatorManager<SystemConfiguration, DSUBMessage> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static String TRANSACTION_TYPE = "DSUB";

    private static Logger log = LoggerFactory.getLogger(CommonIHENotifManager.class);

    private DSUBMetadatasManager dsubMetadatasManagerClass;


    public DSUBMetadatasManager getDsubMetadatasManagerClass() {
        return dsubMetadatasManagerClass;
    }

    public void setDsubMetadatasManagerClass(
            DSUBMetadatasManager dsubMetadatasManagerClass) {
        this.dsubMetadatasManagerClass = dsubMetadatasManagerClass;
    }

    public abstract String getSelectedMessageType();

    public abstract void listAllConfigurations();

    public void sendMessage() {
        message = new DSUBMessage();
        message.setMessageType(getSelectedMessageType());
        message.setTransaction(selectedTransaction);
        message.setAffinityDomain(selectedAffinityDomain);
        message.setContentType(ContentType.SOAP_MESSAGE);
        message.setTimeStamp(new Date());
        message.setGazelleDriven(false);
        message.setConfiguration(selectedConfiguration);

        MetadataMessage sent = new MetadataMessage();
        String messageToSend = this.createMessage();
        sent.setMessageContent(messageToSend);
        sent.setMessageType(MetadataMessageType.NOTIFY);
        message.setSentMessageContent(sent);
        message = DSUBMessage.storeMessage(message);

        MessageSenderCommon sender = new MessageSenderCommon(selectedConfiguration.getUrl(), message);
        try {
            sender.sendMessageSOAP(MetadataMessageType.NO_RESPONSE);
            message = (DSUBMessage) sender.getRequest();
            message = (DSUBMessage) DSUBMessage.storeMessage(message);
        } catch (Exception e) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.xca.initgw.errorWhenSendingMessage", e.getMessage
                    ());
        }
        Contexts.getSessionContext().set("messagesToDisplay", message);
        displayResultPanel = true;
    }

    public void updateSpecificClient() {
        this.initPublishMessage();
    }

    private void initPublishMessage() {

    }

    public List<AffinityDomain> listAllAffinityDomains() {
        return listAllAffinityDomains(TRANSACTION_TYPE);
    }

    public abstract String createMessage();

    public String createMessage(boolean addTopic) {
        String res = "";
        NotifyType notif = new NotifyType();
        NotificationMessageHolderType notificationMessage = new NotificationMessageHolderType();
        notificationMessage.setMessage(new MessageType());
        notificationMessage.getMessage().setSubmitObjectsRequest(new SubmitObjectsRequestType());
        notificationMessage.getMessage().getSubmitObjectsRequest().setRegistryObjectList(new RegistryObjectListType());
        notif.getNotificationMessage().add(notificationMessage);
        if (this.dsubMetadatasManagerClass != null) {
            for (XDSDocumentEntry xdsdoc : this.dsubMetadatasManagerClass.getListXDSDocumentEntry()) {
                notificationMessage.getMessage().getSubmitObjectsRequest().getRegistryObjectList().addExtrinsicObject(
                        this.getCleanedExtrinsicObjectType(xdsdoc));
            }
            for (XDSFolder xdsfold : this.dsubMetadatasManagerClass.getListXDSFolder()) {
                notificationMessage.getMessage().getSubmitObjectsRequest().getRegistryObjectList().addRegistryPackage(
                        this.getCleanedXDSFolderType(xdsfold));
            }
            for (XDSSubmissionSet xdssub : this.dsubMetadatasManagerClass.getListXDSSubmissionSet()) {
                notificationMessage.getMessage().getSubmitObjectsRequest().getRegistryObjectList().addRegistryPackage(
                        this.getCleanedXDSSubmissionSet(xdssub));
            }
            for (ObjectRefType objref : this.dsubMetadatasManagerClass.getListObjectRef()) {
                notificationMessage.getMessage().getSubmitObjectsRequest().getRegistryObjectList().addObjectRef(objref);
            }
        }

        if (addTopic) {
            notificationMessage.setTopic(new TopicExpressionType());
            notificationMessage.getTopic().setDialect("http://docs.oasis-open.org/wsn/t-1/TopicExpression/Simple");
            notificationMessage.getTopic().setValue(getSelectedMessageType());
        }

        notificationMessage.setProducerReference(new EndpointReferenceType());
        notificationMessage.getProducerReference().setAddress(ApplicationConfiguration.getValueOfVariable("application_url"));
        String notifString = this.getNotifyTypeAsString(notif);

        String receiverUrl = this.selectedConfiguration.getUrl();
        try {
            res = SOAPRequestBuilder.createSOAPMessage(notifString, useXUA, praticianID, attributes, null,
                    receiverUrl, "http://docs.oasis-open.org/wsn/bw-2/NotificationConsumer/Notify", false);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        }
        return res;
    }

    public RegistryPackageType getCleanedXDSSubmissionSet(XDSSubmissionSet submissionSet) {
        Cloner cloner = new Cloner();
        RegistryPackageType res = cloner.deepClone(submissionSet);

        ClassificationType cl = new ClassificationType();
        cl.setClassificationNode("urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd");
        cl.setClassifiedObject(res.getId());
        cl.setId("urn:uuid:" + UUID.randomUUID().toString());
        cl.setObjectType("urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification");
        res.getClassification().add(cl);

        for (ClassificationType cll : res.getClassification()) {
            cll.setClassifiedObject(res.getId());
        }

        for (ExternalIdentifierType ext : res.getExternalIdentifier()) {
            ext.setRegistryObject(res.getId());
        }

        return res;
    }

    public RegistryPackageType getCleanedXDSFolderType(XDSFolder folder) {
        Cloner cloner = new Cloner();
        RegistryPackageType res = cloner.deepClone(folder);

        ClassificationType cl = new ClassificationType();
        cl.setClassificationNode("urn:uuid:d9d542f3-6cc4-48b6-8870-ea235fbc94c2");
        cl.setClassifiedObject(res.getId());
        cl.setId("urn:uuid:" + UUID.randomUUID().toString());
        cl.setObjectType("urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification");
        res.getClassification().add(cl);

        for (ClassificationType cll : res.getClassification()) {
            cll.setClassifiedObject(res.getId());
        }

        for (ExternalIdentifierType ext : res.getExternalIdentifier()) {
            ext.setRegistryObject(res.getId());
        }

        return res;
    }

    private ExtrinsicObjectType getCleanedExtrinsicObjectType(XDSDocumentEntry docentr) {
        String patientId = this.extractPatientIdFromXDSDocumentEntry(docentr);
        Cloner cloner = new Cloner();
        ExtrinsicObjectType res = cloner.deepClone(docentr);

        res.getSlot().add(RIMBuilderCommon.createSlot("sourcePatientId", patientId));

        for (ClassificationType cll : res.getClassification()) {
            cll.setClassifiedObject(res.getId());
        }

        for (ExternalIdentifierType ext : res.getExternalIdentifier()) {
            ext.setRegistryObject(res.getId());
        }

        return res;
    }

    private String extractPatientIdFromXDSDocumentEntry(XDSDocumentEntry docentry) {
        String res = "";
        if ((docentry != null) &&
                (docentry.getExternalIdentifier().size() > 0)) {
            for (ExternalIdentifierType ext : docentry.getExternalIdentifier()) {
                if (ext.getIdentificationScheme() != null && ext.getIdentificationScheme().equals("urn:uuid:58a6f841-87b3-4a3e-92fd-a8ffeff98427")) {
                    res = ext.getValue();
                }
            }
        }
        return res;
    }

    private String getNotifyTypeAsString(NotifyType aqr) {
        try {
            JAXBContext jc = JAXBContext.newInstance(NotifyType.class);
            Marshaller m = jc.createMarshaller();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.marshal(aqr, baos);
            return baos.toString();
        } catch (Exception e) {
            return null;
        }
    }

    public abstract String getTheNameOfThePage();

    public String previewMessage() {
        String res = this.createMessage();
        return res;
    }

    public abstract void initBean();

    public void initMetadatas() {
        this.dsubMetadatasManagerClass = new DSUBMetadatasManager();
        this.dsubMetadatasManagerClass.init(this.selectedAffinityDomain, this.selectedTransaction);
    }

}
