package net.ihe.gazelle.xdsar.dsub.ws;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import net.ihe.gazelle.xdstar.common.model.RespondingMessage;

/**
 * 
 * @author aboufahj
 *
 */
@Entity(name="DSUBNotifyMessage")
@DiscriminatorValue("DSUBNOTIF")
public class DSUBNotifyMessage extends RespondingMessage implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
