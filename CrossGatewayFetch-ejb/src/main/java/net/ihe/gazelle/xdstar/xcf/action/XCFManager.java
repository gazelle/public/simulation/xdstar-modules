/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.xcf.action;

import com.rits.cloning.Cloner;
import net.ihe.gazelle.metadata.common.RIMGenerator;
import net.ihe.gazelle.metadata.model.AdhocQueryMetadataQuery;
import net.ihe.gazelle.metadata.model.SlotMetadata;
import net.ihe.gazelle.query.AdhocQueryRequestType;
import net.ihe.gazelle.query.ResponseOptionType;
import net.ihe.gazelle.rim.AdhocQueryType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.rim.ValueListType;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.xua.SignatureException;
import net.ihe.gazelle.xdstar.common.model.ContentType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import net.ihe.gazelle.xdstar.comon.util.ConfigurationFilter;
import net.ihe.gazelle.xdstar.comon.util.Pair;
import net.ihe.gazelle.xdstar.comon.util.Util;
import net.ihe.gazelle.xdstar.core.MTOMBuilder;
import net.ihe.gazelle.xdstar.core.MTOMElement;
import net.ihe.gazelle.xdstar.core.MessageSenderCommon;
import net.ihe.gazelle.xdstar.core.soap.SOAPRequestBuilder;
import net.ihe.gazelle.xdstar.rsq.common.CommonAdhocQueryManager;
import net.ihe.gazelle.xdstar.xca.initgw.model.XCARespConfiguration;
import net.ihe.gazelle.xdstar.xca.initgw.model.XCARespConfigurationQuery;
import net.ihe.gazelle.xdstar.xcf.model.XCFMessage;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *  
 * @author                	Abderrazek Boufahja
 *
 */

@Name("xcfManagerBean")
@Scope(ScopeType.PAGE)
public class XCFManager extends CommonAdhocQueryManager<XCARespConfiguration, XCFMessage> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private static Logger log = LoggerFactory.getLogger(XCFManager.class);
	
	private static String TRANSACTION_TYPE = "XCF";

	@Override
	public void init() {
		super.init();
		AdhocQueryMetadataQuery qq = new AdhocQueryMetadataQuery();
		qq.name().eq("Cross Gateway Fetch");
		this.selectedMessageType = qq.getUniqueResult();
		this.initAdhocQueryByMessageType();
	}

	public void listAllConfigurations()
	{
		XCARespConfigurationQuery rc = new XCARespConfigurationQuery();
		if (selectedAffinityDomain != null) {
			rc.listUsages().affinity().eq(selectedAffinityDomain);
		}
		if (selectedTransaction != null) {
			rc.listUsages().transaction().eq(selectedTransaction);
		}
		rc.name().order(true);
		ConfigurationFilter.filterConfigurationQueryAccodingToUserCredential(rc);
		configurations = rc.getList();
	}
	
	public void sendMessage()
	{
		message = new XCFMessage();
		message.setMessageType("Cross Gateway Fetch");
		message.setTransaction(selectedTransaction);
		message.setAffinityDomain(selectedAffinityDomain);
		message.setContentType(ContentType.SOAP_MESSAGE);
		message.setTimeStamp(new Date());
		message.setGazelleDriven(false);
		message.setConfiguration(selectedConfiguration);
		
		Pair<String, byte[]> mtom = null;
		try{
			String soap = createMessage();
			mtom = buildMTOMFromData(soap);
		}
		catch(Exception e){
			log.error("not able to sent message : " , e);
		}
		
		MetadataMessage sent = new MetadataMessage();
		if (mtom != null) {
			sent.setMessageContent(new String(mtom.getObject2()));
		}
		sent.setMessageType(MetadataMessageType.ADHOC_QUERY_REQUEST);
		message.setSentMessageContent(sent);
		message = XCFMessage.storeMessage(message);
		
		MessageSenderCommon sender = new MessageSenderCommon(selectedConfiguration.getUrl(), message);
		net.ihe.gazelle.util.Pair<String, String> resp2 = 
				sender.sendMessage(this.selectedConfiguration.getUrl(), mtom.getObject2(), message, mtom.getObject1());
		String resp = resp2 != null? resp2.getObject2():null;
		resp = formatStringIfContainsXML(resp);
		MetadataMessage received = new MetadataMessage();
		received.setMessageContent(resp);
		received.setMessageType(MetadataMessageType.ADHOC_QUERY_RESPONSE);
		message.setReceivedMessageContent(received);
		message = XCFMessage.storeMessage(message);
		Contexts.getSessionContext().set("messagesToDisplay", message);
		displayResultPanel = true;
	}
	
	private String formatStringIfContainsXML(String string){
		String res = "";
		String soapRegex = "<\\?xml.*?Envelope>";
		Pattern pp = Pattern.compile(soapRegex,Pattern.MULTILINE|Pattern.DOTALL);
		Matcher mm = pp.matcher(string);
		
		if (mm.find()){
			String ss = mm.group();
			int ind = string.indexOf(ss);
			String toModify = Util.prettyFormat(ss);
			String begin = string.substring(0, ind);
			String end = string.substring(ind + ss.length());
			res = begin + toModify + end;
		}
		else{
			res = string;
		}
		return res;
	}
	
	public void updateSpecificClient() {
		// TODO Auto-generated method stub
		
	}
	
	public List<AffinityDomain> listAllAffinityDomains() {
		return listAllAffinityDomains(TRANSACTION_TYPE);
	}
	
	public String createMessage(){
		String res = null;
		AdhocQueryRequestType aqr = new AdhocQueryRequestType();
		aqr.setAdhocQuery(new AdhocQueryType());
		aqr.getAdhocQuery().setId(this.adhocQuery.getId());
		if (this.selectedConfiguration.getHomeCommunityId() != null) {
			aqr.getAdhocQuery().setHome(this.selectedConfiguration.getHomeCommunityId());
		}
		Cloner cloner = new Cloner();
		for (SlotType1 sl : this.adhocQuery.getSlot()) {
			SlotType1 newsl = cloner.deepClone(sl);
			SlotMetadata sm = RIMGenerator.slWeak.get(sl);
			if ((sm.getIsNumber()== null) || !sm.getIsNumber()){
				for (String ss : sl.getValueList().getValue()) {
					newsl.getValueList().getValue().remove(ss);
					newsl.getValueList().getValue().add("'" + ss.replace("'", "''") + "'");
				}
			}
			if ((sm.getMultiple() != null && sm.getMultiple()) || (sm.getSupportAndOr() != null && sm.getSupportAndOr())){
				ValueListType vlt = cloner.deepClone(newsl.getValueList());
				for (String  ss : vlt.getValue()) {
					newsl.getValueList().getValue().remove(ss);
					newsl.getValueList().getValue().add("(" + ss + ")");
				}
			}
			aqr.getAdhocQuery().getSlot().add(newsl);
		}
		aqr.setResponseOption(new ResponseOptionType());
		aqr.getResponseOption().setReturnComposedObjects(true);
		aqr.getResponseOption().setReturnType(this.selectedReturnType);
		res = this.getAdhocQueryRequestTypeAsString(aqr);
		String receiverUrl = this.selectedConfiguration.getUrl();
		try {
			res = SOAPRequestBuilder.createSOAPMessage(res, useXUA, praticianID, attributes, null, receiverUrl, "urn:ihe:iti:2011:CrossGatewayFetch", false);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SignatureException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	public static Pair<String, byte[]> buildMTOMFromData(String soapMessage) throws IOException{
		List<MTOMElement> listDataToSend = new ArrayList<MTOMElement>();
		MTOMElement soap = new MTOMElement();
		soap.setContent(("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + soapMessage + "\r\n").getBytes(StandardCharsets.UTF_8));
		soap.setContentType(" application/xop+xml; charset=UTF-8; type=\"application/soap+xml\"");
		soap.setTransfertEncoding("binary");
		soap.setIndex(0);
		soap.setUuid(UUID.randomUUID().toString().replace("-", "").toUpperCase());
		listDataToSend.add(soap);
		
		byte[] body = MTOMBuilder.buildMTOM(listDataToSend);
		
		String boundaryUUID = MTOMBuilder.getBoundaryUUIDFromGeneratedMTOM(body);
		
		String boundary = "MIMEBoundaryurn_uuid_" + boundaryUUID;
		String boundaryStart = "0.urn:uuid:" + soap.getUuid() + "@ws.jboss.org";
		String contentTypeProperties = new String();
		contentTypeProperties = "multipart/related; ";
		contentTypeProperties = contentTypeProperties + "boundary=" + boundary + "; ";
		contentTypeProperties = contentTypeProperties + "type=\"application/xop+xml\"; ";
        contentTypeProperties = contentTypeProperties + "start=\"<" + boundaryStart + ">\"; ";
        contentTypeProperties = contentTypeProperties + "start-info=\"application/soap+xml\"; ";
		contentTypeProperties = contentTypeProperties + "action=\"urn:ihe:iti:2011:CrossGatewayFetch\"";
		Pair<String, byte[]> res = new Pair<String, byte[]>(contentTypeProperties, body);
		return res;
	}

	@Override
	public String getTheNameOfThePage() {
		String res = this.getNameOfTheAffinityDomainTransaction();
		if (res == null || res.equals("")) {
			res = "Cross Gateway Fetch [XCF]";
		}
		return res;
	}
	
}
