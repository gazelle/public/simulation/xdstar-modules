/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.xdstar.xdrsrc.model;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Transient;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.xdstar.common.model.RegisteredFile;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

/**
 *  <b>Class Description :  </b>RegisteredFile<br><br>
 *  RegisteredFile represents the file provided to the receiver and registered by it
 *  
 *  RegisteredFile contains the following attributes
 *  <li>
 *  	<ul><b>id</b>id in the database</ul>
 *  	<ul><b>name</b> file name</ul>
 *  	<ul><b>type</b> file type (xml, pdf)</ul>
 *  	<ul><b>path</b> file path on the file system (directory where files are stored is given in the application preferences registered_files_directory)</ul>
 *  </li>
 *  
 * @class			RegisteredFile
 * @package			net.ihe.gazelle.simulator.xdrsrc.model
 * @author 			Abderrazek Boufahja / IHE Europe
 * @author			Anne-Gaëlle Bergé / INRIA Rennes IHE development Project
 * @version			1.0 - 2011, January 26th
 *
 */

@Entity
@Name("pnrRegisteredFile")
@DiscriminatorValue("PNRRF")
public class PnRRegisteredFile extends RegisteredFile {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Logger
	private static Log log;

	@Transient
	private List<MetadataValue> metadata;
	
	@Transient
	private String documentId;
	
	@Transient
	private String uuid;
	
	@Transient
	private Integer index;
	
	@Transient 
	private Code formatCode;
	
		
	//////////////////////////////////////////////////////////////////////////
	// Constructors

	public PnRRegisteredFile(){

	}

	public PnRRegisteredFile (String fileName, String fileType)
	{
		super(fileName, fileType);
	}

	//////////////////////////////////////////////////////////////////////////
	// Getters and Setters
	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}


	public void setMetadata(List<MetadataValue> metadata) {
		this.metadata = metadata;
	}

	public List<MetadataValue> getMetadata() {
		return metadata;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getUuid() {
		return uuid;
	}
	
	public void setFormatCode(Code formatCode) {
		this.formatCode = formatCode;
	}

	public Code getFormatCode() {
		return formatCode;
	}

	/**
	 * Stores, on the file system, a file to send to the receiver and create a pointer (RegisteredFile object) in the database
	 * @param fileName: name of the uploaded file
	 * @param fileContent: file content
	 * @param fileType: file type (xml, pdf)
	 * @return the relative pointer
	 */
	public static PnRRegisteredFile saveFile(String fileName, String fileType) throws IOException
	{
		String absolutePath = ApplicationConfiguration.getValueOfVariable("registered_files_directory");
		if (absolutePath != null && !absolutePath.isEmpty())
		{
			EntityManager em = (EntityManager) Component.getInstance("entityManager");
			PnRRegisteredFile registeredFile = new PnRRegisteredFile(fileName, fileType);
			registeredFile = em.merge(registeredFile);
			em.flush();
			String extension = "";
			if (fileType.contains("xml"))
				extension = ".xml";
			else if (fileType.contains("pdf"))
				extension = ".pdf";
			else
				extension = "";
			absolutePath = absolutePath + "/file" + registeredFile.getId().toString() + extension;
			registeredFile.setPath(absolutePath);
			registeredFile = em.merge(registeredFile);
			em.flush();
			
			return registeredFile;
		}
		else
		{
			log.error("registered_files_directory is not set in app_configuration table");
			return null;
		}
	}

	/**
	 * Deletes the file on the file system and its reference in the database
	 * @param fileToDelete
	 * @return
	 */
	public static boolean deleteFile (PnRRegisteredFile fileToDelete)
	{
		if (fileToDelete != null)
		{
			File file = new File(fileToDelete.getPath());
			if (file.exists())
			{
				if (file.delete())
				{
					EntityManager em = (EntityManager) Component.getInstance("entityManager");
					em.remove(fileToDelete);
					em.flush();
					return true;
				}
				else
				{
					log.error("Unable to delete file at " + fileToDelete.getPath());
					return false;
				}
			}
			else
			{
				log.error(fileToDelete.getPath() + " not found in file system");
				return false;
			}					
		}
		else
			return false;
	}

	/////////////////////////////////////////////////////////////////////
	// Hash Code and Equals

	@Override
	public String toString() {
		return "PnRRegisteredFile [metadata=" + metadata + ", documentId="
				+ documentId + ", uuid=" + uuid + ", index=" + index
				+ ", formatCode=" + formatCode + "]";
	}
	
	

}
