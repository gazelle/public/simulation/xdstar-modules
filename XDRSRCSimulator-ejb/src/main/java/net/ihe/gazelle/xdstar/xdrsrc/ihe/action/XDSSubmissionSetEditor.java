package net.ihe.gazelle.xdstar.xdrsrc.ihe.action;

import net.ihe.gazelle.metadata.model.ExternalIdentifierMetadata;
import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSSubmissionSet;

public interface XDSSubmissionSetEditor extends XDSCommonEditor{

	public void setSubmissionSet(XDSSubmissionSet submissionSet);
	public XDSSubmissionSet getSubmissionSet();
	public void editXDSSubmissionSet(XDSSubmissionSet xds);
	
	public void getTreeNodeSubmissionSet();
	public void addDocumentEntry(XDSSubmissionSet xx);
	public void editXDSSubmissionSet();
	public String getOptionalityExtOnSubmissionSet(ExternalIdentifierType ext);
	public String getOptionalityClassOnSubmissionSet(ClassificationType ext);
	public void addNewMetadataToSubmissionSet();
	public void removeClassificationFromSubmissionSet(ClassificationType cl);
	public void removeExtFromSubmissionSet(ExternalIdentifierMetadata ext);
	public String getOptionalitySlotOnSubmissionSet(SlotType1 sl);
	public void removeSlotFromSubmissionSet(SlotType1 sl);
}
