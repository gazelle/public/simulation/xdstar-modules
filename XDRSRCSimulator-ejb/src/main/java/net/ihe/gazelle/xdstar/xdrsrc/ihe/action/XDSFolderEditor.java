package net.ihe.gazelle.xdstar.xdrsrc.ihe.action;

import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSFolder;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSSubmissionSet;

public interface XDSFolderEditor extends XDSCommonEditor{
	
	public XDSFolder getSelectedXDSFolder();

	public void setSelectedXDSFolder(XDSFolder selectedXDSFolder) ;
	
	public void editXDSFolder(XDSFolder xx);
	
	public Boolean getEditXDSFolder();
	public void setEditXDSFolder(Boolean editXDSFolder);
	
	public int getFolderIndex();
	public void setFolderIndex(int folderIndex);
	
	public void addDocumentEntry(XDSFolder xx);
	
	public String getOptionalityExtOnSelectedXDSFolder(ExternalIdentifierType ext);
	public void removeExtFromSelectedXDSFolder(ExternalIdentifierType ext);
	public String getOptionalitySlotOnSelectedXDSFolder(SlotType1 sl);
	public void removeSlotFromSelectedXDSFolder(SlotType1 sl);
	public String getOptionalityClassOnSelectedXDSFolder(ClassificationType ext);
	public void removeClassificationFromSelectedXDSFolder(ClassificationType cl);
	
	public void addFolder(XDSSubmissionSet xx);
	
	public void deleteXDSFolder(XDSFolder xdsfolder);

}
