package net.ihe.gazelle.xdstar.xdrsrc.model;

import net.ihe.gazelle.xdstar.common.model.AbstractMessage;
import org.jboss.seam.annotations.Name;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@Name("RMUMessage")
@DiscriminatorValue("RMU")
public class RMUMessage extends AbstractMessage implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

}
