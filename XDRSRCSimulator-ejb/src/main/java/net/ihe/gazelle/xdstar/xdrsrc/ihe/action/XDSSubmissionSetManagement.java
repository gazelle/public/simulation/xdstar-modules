package net.ihe.gazelle.xdstar.xdrsrc.ihe.action;

import java.util.List;
import java.util.UUID;

import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.metadata.common.RIMGenerator;
import net.ihe.gazelle.metadata.model.ClassificationMetaData;
import net.ihe.gazelle.metadata.model.ExternalIdentifierMetadata;
import net.ihe.gazelle.metadata.model.RegistryPackageMetadata;
import net.ihe.gazelle.metadata.model.SlotMetadata;
import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSDocumentEntry;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSFolder;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSSubmissionSet;

public class XDSSubmissionSetManagement {
	
	private XDSSubmissionSetManagement(){}
	
	public static void removeExtFromSubmissionSet(XDSSubmissionSet submissionSet, ExternalIdentifierMetadata ext, XDSSubmissionSetEditor setedit){
		submissionSet.getExternalIdentifier().remove(ext);
		setedit.initListOptionalMetadata();
	}
	
	public static void removeClassificationFromSubmissionSet(XDSSubmissionSet submissionSet, ClassificationType cl, XDSSubmissionSetEditor setedit){
		submissionSet.getClassification().remove(cl);
		setedit.initListOptionalMetadata();
	}
	
	public static XDSSubmissionSet generateXDSSubmissionSet(AffinityDomain selectedAffinityDomain, Transaction selectedTransaction){
		XDSSubmissionSet res = new XDSSubmissionSet();
		HQLQueryBuilder<RegistryPackageMetadata> hh = new HQLQueryBuilder<RegistryPackageMetadata>(RegistryPackageMetadata.class);
		hh.addEq("uuid", XDSCommonModule.XDSSUBMISSIONSET_UUID);
		hh.addEq("usages.affinity", selectedAffinityDomain);
		if (selectedTransaction != null){
			hh.addEq("usages.transaction", selectedTransaction);
		}
		RegistryPackageMetadata rpm = hh.getList().get(0); 
		RIMGenerator.generateRegistryPackageTypeChild(rpm, res);
		res.setId("urn:uuid:" + UUID.randomUUID());
		XDSCommonModule.updateSourceIdAndUniqueId(res);
		return res;
	}
	
	public static String getOptionalityClassOnSubmissionSet(XDSSubmissionSet submissionSet, ClassificationType ext){
		RegistryPackageMetadata rpm = RIMGenerator.rpWeak.get(submissionSet);
		ClassificationMetaData extm = RIMGenerator.clWeak.get(ext);
		for (ClassificationMetaData ee : rpm.getClassification_required()) {
			if (extm.getUuid().equals(ee.getUuid())) {
				return "R";
			}
		}
		return "O";
	}
	
	public static String getOptionalityExtOnSubmissionSet(XDSSubmissionSet submissionSet, ExternalIdentifierType ext){
		RegistryPackageMetadata rpm = RIMGenerator.rpWeak.get(submissionSet);
		ExternalIdentifierMetadata extm = RIMGenerator.extWeak.get(ext);
		for (ExternalIdentifierMetadata ee : rpm.getExt_identifier_required()) {
			if (extm.getUuid().equals(ee.getUuid())) {
				return "R";
			}
		}
		return "O";
	}
	
	public static GazelleTreeNodeImpl<Object> getTreeNodeSubmissionSet(XDSSubmissionSet submissionSet){
		GazelleTreeNodeImpl<Object> rootNode = new GazelleTreeNodeImpl<Object>();
		
		GazelleTreeNodeImpl<Object> submissionTreeNode=new GazelleTreeNodeImpl<Object>();
		submissionTreeNode.setData(submissionSet);
		rootNode.addChild(1, submissionTreeNode);

		int i = 0;
		int j;
		if (submissionSet.getListXDSFolder() != null){
			for(XDSFolder xx : submissionSet.getListXDSFolder()){
				GazelleTreeNodeImpl<Object> folderTreeNode=new GazelleTreeNodeImpl<Object>();
				folderTreeNode.setData(xx);
				List<XDSDocumentEntry> listDE = xx.getListXDSDocumentEntry();
				j = 0;
				if (listDE != null){
					for(XDSDocumentEntry doc:listDE){
						GazelleTreeNodeImpl<Object> docNode = new GazelleTreeNodeImpl<Object>();
						docNode.setData(doc);
						folderTreeNode.addChild(j, docNode);
						j++;
					}
				}
				submissionTreeNode.addChild(i, folderTreeNode);
				i++;
			}
		}
		if (submissionSet.getListXDSDocumentEntry() != null){
			for (XDSDocumentEntry xxd : submissionSet.getListXDSDocumentEntry()) {
				GazelleTreeNodeImpl<Object> ddd=new GazelleTreeNodeImpl<Object>();
				ddd.setData(xxd);
				submissionTreeNode.addChild(i, ddd);
				i++;	
			}

		}
		return rootNode;
	}
	
	public static String getOptionalitySlotOnSubmissionSet(XDSSubmissionSet submissionSet, SlotType1 sl){
		RegistryPackageMetadata rpm = RIMGenerator.rpWeak.get(submissionSet);
		SlotMetadata slm = RIMGenerator.slWeak.get(sl);
		if (rpm.getSlot_required() != null){
			for (SlotMetadata ee : rpm.getSlot_required()) {
				if (slm.getName().equals(ee.getName())) {
					return "R";
				}
			}
		}
		return "O";
	}
	
	public static void removeSlotFromSubmissionSet(XDSSubmissionSet submissionSet, SlotType1 sl, XDSSubmissionSetEditor subedit){
		submissionSet.getSlot().remove(sl);
		subedit.initListOptionalMetadata();
	}

}
