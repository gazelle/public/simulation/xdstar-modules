package net.ihe.gazelle.xdstar.xdrsrc.action;

import net.ihe.gazelle.rim.*;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.xdstar.common.action.CommonSimulatorManager;
import net.ihe.gazelle.xdstar.common.model.ContentType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import net.ihe.gazelle.xdstar.comon.util.ConfigurationFilter;
import net.ihe.gazelle.xdstar.comon.util.Util;
import net.ihe.gazelle.xdstar.conf.model.RepositoryConfiguration;
import net.ihe.gazelle.xdstar.conf.model.RepositoryConfigurationQuery;
import net.ihe.gazelle.xdstar.xdrsrc.model.*;
import net.ihe.gazelle.xdstar.xdrsrc.tools.MessageSender;
import net.ihe.gazelle.xdstar.xdrsrc.tools.XDRFileToSend;
import net.ihe.gazelle.xdstar.xdrsrc.util.SOAPBuilder;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.log.Log;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.xml.sax.SAXException;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Name("xdrsrcSimulatorManagerBean")
@Scope(ScopeType.PAGE)
public class XDRSRCSimulatorManager extends CommonSimulatorManager<RepositoryConfiguration, XDRMessage> implements Serializable {

    private static final String THE_TOOL_IS_NOT_ABLE_TO_GENERATE_A_DOCUMENT = "The tool is not able to generate a document";

    private static final String CODING_SCHEME = "codingScheme";

    private static final String ITI_41 = "ITI-41";

    private static final long serialVersionUID = 1L;

    private static final String GENERATE_MODE = "generate";

    public static String TRANSACTION_TYPE = "PRS";


    @Logger
    private static Log log;

    private String selectedAttachDocumentsMode;
    private List<MetadataValue> submissionSetMetadata;

    private List<PnRRegisteredFile> files;
    private List<Code> codes;
    private Code selectedCode;
    private List<MetadataValue> documentMetadataValues;
    private List<AssociationType1> associations;

    private List<SelectItem> formatCodes;

    private String selectedConsentCode;

    // not used in JSF
    private Integer fileIndex;
    private String boundaryUUID;
    private String patientId;

    private static void addValueToaSlot(SlotType1 slot, String value) {
        if (slot.getValueList() == null) {
            slot.setValueList(new ValueListType());
        }
        slot.getValueList().getValue().add(value);
    }

    public String getSelectedAttachDocumentsMode() {
        return selectedAttachDocumentsMode;
    }

    public void setSelectedAttachDocumentsMode(String selectedAttachDocumentsMode) {
        this.selectedAttachDocumentsMode = selectedAttachDocumentsMode;
    }

    public List<MetadataValue> getSubmissionSetMetadata() {
        return submissionSetMetadata;
    }

    public void setSubmissionSetMetadata(List<MetadataValue> metadataValuesList) {
        this.submissionSetMetadata = metadataValuesList;
    }

    public List<PnRRegisteredFile> getFiles() {
        return files;
    }

    public void setFiles(List<PnRRegisteredFile> files) {
        this.files = files;
    }

    public List<Code> getCodes() {
        return codes;
    }

    public void setCodes(List<Code> codes) {
        this.codes = codes;
    }

    public Code getSelectedCode() {
        return selectedCode;
    }

    public void setSelectedCode(Code selectedCode) {
        this.selectedCode = selectedCode;
    }

    public List<AssociationType1> getAssociations() {
        return associations;
    }

    public void setAssociations(List<AssociationType1> associations) {
        this.associations = associations;
    }

    public List<MetadataValue> getDocumentMetadataValues() {
        return documentMetadataValues;
    }

    public void setDocumentMetadataValues(List<MetadataValue> documentMetadataValues) {
        this.documentMetadataValues = documentMetadataValues;
    }

    public List<SelectItem> getFormatCodes() {
        return formatCodes;
    }

    public void setFormatCodes(List<SelectItem> formatCodes) {
        this.formatCodes = formatCodes;
    }

    public String getSelectedConsentCode() {
        return selectedConsentCode;
    }

    public void setSelectedConsentCode(String selectedConsentCode) {
        this.selectedConsentCode = selectedConsentCode;
    }

    @PostConstruct
    public void initialize() {
        this.init();
    }

    public void listAllConfigurations() {
        RepositoryConfigurationQuery rc = new RepositoryConfigurationQuery();
        if (selectedAffinityDomain != null) {
            rc.listUsages().affinity().eq(selectedAffinityDomain);
        }
        if (selectedTransaction != null) {
            rc.listUsages().transaction().eq(selectedTransaction);
        }
        rc.name().order(true);
        ConfigurationFilter.filterConfigurationQueryAccodingToUserCredential(rc);
        configurations = rc.getList();
    }

    public void init() {
        super.init();
        selectedAttachDocumentsMode = null;
        files = null;
        fileIndex = 1;
        boundaryUUID = UUID.randomUUID().toString().replace("-", "").toUpperCase();
    }

    public void reset() {
        super.reset();
        selectedAttachDocumentsMode = null;
        files = null;
        fileIndex = 1;
    }

    public void uploadListener(FileUploadEvent event) {
        UploadedFile item = event.getUploadedFile();
        FileOutputStream fos = null;
        if (files == null) {
            files = new ArrayList<PnRRegisteredFile>();
        }
        try {
            PnRRegisteredFile registeredFile = PnRRegisteredFile.saveFile(item.getName(), item.getContentType());
            registeredFile.setUuid(boundaryUUID);
            registeredFile.setIndex(fileIndex);
            fileIndex++;
            registeredFile.setDocumentId("document" + registeredFile.getId());

            files.add(registeredFile);
            File fileToWrite = new File(registeredFile.getPath());
            fos = new FileOutputStream(fileToWrite);
            if (item.getData() != null && item.getData().length > 0) {
                fos.write(item.getData());
                fos.close();
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The file is empty !");
            }

        } catch (IOException e) {
            log.error("An error occurred when uploaded file with name " + item.getName());
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "cannot store the file", item.getName());
        } finally {
            try {
                fos.close();
            } catch (Exception e) {
                log.error("Cannot close channel : " + e.getMessage());
            }
        }
    }

    public void selectTransaction() {
        if (selectedAffinityDomain != null) {
            selectedTransaction = Transaction.GetTransactionByKeyword(XDRSRCSimulatorManager.ITI_41);
            Metadata metadata = Metadata.getMetadataByName("XDSSubmissionSet.contentTypeCode");
            codes = Code.getCodesForTransaction(metadata.getUuid(), selectedTransaction, null);
        }
    }

    public void generateSubmissionSetMetadata() {
        List<Metadata> metadata = Metadata.getMetadataByObjectType(Metadata.XDSSUBMISSIONSET);
        if (metadata != null) {
            submissionSetMetadata = new ArrayList<MetadataValue>();
            for (Metadata m : metadata) {
                if (m.getName().equals("XDSSubmissionSet.uniqueId")) {
                    submissionSetMetadata.add(new MetadataValue(m, OID.getNewSubmissionSetOid(), null));
                } else if (m.getName().equals("XDSSubmissionSet.sourceId")) {
                    submissionSetMetadata.add(new MetadataValue(m, OID.getEPSOSSourceOid(), null));
                } else if (m.getName().equals("XDSSubmissionSet.contentTypeCode")) {
                    List<Code> lc = Code.getCodesForTransaction("urn:uuid:aa543740-bdda-424e-8c96-df4873be8500", this.selectedTransaction, null);
                    Code cd = null;
                    if (lc != null && lc.size() > 0) {
                        cd = lc.get(0);
                    }
                    if (cd != null) {
                        SlotType1 slot = new SlotType1();
                        slot.setName(XDRSRCSimulatorManager.CODING_SCHEME);
                        addValueToaSlot(slot, cd.getCodingScheme());
                        List<SlotType1> slots = new ArrayList<SlotType1>();
                        slots.add(slot);
                        MetadataValue mv = new MetadataValue(m, cd.getCode(), slots);
                        mv.setLocalizedString(cd.getDisplay());
                        submissionSetMetadata.add(mv);
                    } else {
                        submissionSetMetadata.add(new MetadataValue(m, "", null));
                    }
                } else {
                    submissionSetMetadata.add(new MetadataValue(m, "", null));
                }
            }
        } else {
            submissionSetMetadata = null;
        }

    }

    public void sendMessage() {
        log.debug("XDRSRCSimulatorManager::sendMessage");

        if (selectedAffinityDomain.getKeyword().equals("epSOS")) {
            autoFillMetadataForEpsos();
        }

        if (selectedAttachDocumentsMode == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "You have to specify the files to upload !");
            return;
        }

        if (selectedAttachDocumentsMode.equals(GENERATE_MODE)) {
            try {
                PnRRegisteredFile registeredFile = PnRRegisteredFile.saveFile("File generated by XDRSRCSimulator", "text/xml");
                registeredFile.setUuid(boundaryUUID);
                registeredFile.setIndex(fileIndex);
                fileIndex++;
                registeredFile.setDocumentId("document" + registeredFile.getId());
                String templatePath = null;
                if (selectedTransaction.getKeyword().contains("Dispensation")) {
                    templatePath = ApplicationConfiguration.getValueOfVariable("dispensation_template_path");
                    List<Code> codes = Code.getCodesForTransaction("urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d", selectedTransaction,
                            "urn:epSOS:ep:dis:2010");
                    selectedCode = codes.get(0);
                } else if (selectedTransaction.getKeyword().contains("Consent")) {
                    templatePath = ApplicationConfiguration.getValueOfVariable("consent_template_path");
                    List<Code> codes = Code.getCodesForTransaction("urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d", selectedTransaction,
                            "urn:ihe:iti:bppc:2007");
                    selectedCode = codes.get(0);
                }
                registeredFile.setFormatCode(selectedCode);
                XDRFileToSend.createCDAFileTosend(registeredFile.getId().toString(), patientId, templatePath, registeredFile.getPath());
                if (files == null) {
                    files = new ArrayList<PnRRegisteredFile>();
                }
                files.add(registeredFile);
            } catch (IOException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, THE_TOOL_IS_NOT_ABLE_TO_GENERATE_A_DOCUMENT);
                e.printStackTrace();
                displayResultPanel = false;
                return;
            } catch (ParserConfigurationException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, THE_TOOL_IS_NOT_ABLE_TO_GENERATE_A_DOCUMENT);
                e.printStackTrace();
                displayResultPanel = false;
                return;
            } catch (SAXException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, THE_TOOL_IS_NOT_ABLE_TO_GENERATE_A_DOCUMENT);
                e.printStackTrace();
                displayResultPanel = false;
                return;
            } catch (TransformerException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, THE_TOOL_IS_NOT_ABLE_TO_GENERATE_A_DOCUMENT);
                e.printStackTrace();
                displayResultPanel = false;
                return;
            }
        }

        associations = new ArrayList<AssociationType1>();
        AssociationType1 association = new AssociationType1();

        if (files == null) {
            files = new ArrayList<PnRRegisteredFile>();
        }
        for (PnRRegisteredFile file : files) {
            List<MetadataValue> fileMetadata = file.getMetadata();
            fileMetadata = new ArrayList<MetadataValue>();
            fileMetadata.addAll(documentMetadataValues);

            // generate a unique OID
            fileMetadata.add(new MetadataValue(Metadata.getMetadataByName("XDSDocumentEntry.uniqueId"), OID.getNewDocumentOid(), null));

            // formatCode
            if (file.getFormatCode() != null) {
                Metadata metadata = Metadata.getMetadataByName("XDSDocumentEntry.formatCode");
                MetadataValue mv = createMetadataValueFromCode(metadata, file.getFormatCode());
                fileMetadata.add(mv);
                if (files.size() > 1) {
                    // fill documents metadata
                    association.setAssociationType("urn:ihe:iti:2007:AssociationType:XFRM");
                    association.setName(new InternationalStringType());
                    LocalizedStringType ff = new LocalizedStringType();
                    ff.setValue("Translation into epSOS pivot format");
                    association.getName().getLocalizedString().add(ff);

                    SlotType1 ss = new SlotType1();
                    ss.setName(XDRSRCSimulatorManager.CODING_SCHEME);
                    addValueToaSlot(ss, "epSOS translation types");

                    if (mv.getValue().contains("ep:dis") || mv.getValue().contains("bppc:")) {
                        association.setSourceObject(file.getDocumentId());
                    } else {
                        association.setTargetObject(file.getDocumentId());
                    }
                }
            }

            file.setMetadata(fileMetadata);

            // generate document Id
            file.setDocumentId("document" + file.getId().toString());
        }

        if (files.size() > 1) {
            associations.add(association);
        }
        message = new XDRMessage();
        message.setTimeStamp(new Date());
        message.setConfiguration(selectedConfiguration);
        message.setTransaction(selectedTransaction);
        message.setAffinityDomain(selectedAffinityDomain);
        message.setContentType(ContentType.MTOM_XOP);
        message.setRegisteredFiles(files);
        message.setGazelleDriven(false);

        SOAPBuilder soapBuilder = new SOAPBuilder();
        soapBuilder.setFilesToLink(files);
        soapBuilder.setSubmissionSetMetadata(submissionSetMetadata);
        soapBuilder.setUseXUA(useXUA);
        soapBuilder.setAttributes(attributes);
        soapBuilder.setReceiverUrl(selectedConfiguration.getUrl());
        soapBuilder.setAffinityDomain(selectedAffinityDomain);
        soapBuilder.setPraticianID(praticianID);
        soapBuilder.setTransaction(selectedTransaction);
        soapBuilder.setAssociations(associations);
        soapBuilder.setPatientId(patientId);

        String messageToSend = soapBuilder.createMessage();

        if (messageToSend != null && !messageToSend.isEmpty()) {
            MetadataMessage sent = new MetadataMessage();
            sent.setMessageContent(messageToSend);
            sent.setHttpHeader("");
            sent.setMessageType(MetadataMessageType.PROVIDE_AND_REGISTER_DOCUMENT_SET_REQUEST);
            message.setSentMessageContent(sent);
            MessageSender sender = new MessageSender();
            sender.setMessage(message);
            sender.setBoundaryUUID(boundaryUUID);
            try {
                sender.send();
            } catch (SocketTimeoutException e) {
                log.error("Cannot connect to " + message.getConfiguration().getUrl());
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Connexion time out after : ", ApplicationConfiguration
                        .getValueOfVariable("connection_timeout"));
            } catch (Exception e) {
                log.error("IOException " + e.getMessage());
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The tool can not send the request");
            } finally {
                message = sender.getMessage();
                message.setMessageType(message.getTransaction().getKeyword());
                message.getSentMessageContent().setHttpHeader(sender.getContentTypeProperties().toString());
                EntityManager em = (EntityManager) Component.getInstance("entityManager");
                message = (XDRMessage) em.merge(message);
                em.flush();
                Contexts.getSessionContext().set("messagesToDisplay", message);
                displayResultPanel = true;
            }
        } else {
            EntityManager em = (EntityManager) Component.getInstance("entityManager");
            message = (XDRMessage) em.merge(message);
            em.flush();
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to create message");
            displayResultPanel = false;
        }
    }

    public void deleteFile(PnRRegisteredFile file) {
        if (files.contains(file)) {
            files.remove(file);
        }
    }

    public void deleteAllFiles() {
        files = null;
    }

    public void autoFillDocumentMetadata() {
        Metadata metadata = null;
        documentMetadataValues = new ArrayList<MetadataValue>();
        List<Metadata> documentEntryMetadata = Metadata.getMetadataByObjectType(Metadata.XDSDOCUMENTENTRY);
        patientId = null;

        for (MetadataValue mv : submissionSetMetadata) {
            if (mv.getMetadata().getName().equals("XDSSubmissionSet.author")) {
                metadata = Metadata.getMetadataByName("XDSDocumentEntry.author");
                SlotType1 slot = null;
                List<SlotType1> slots = new ArrayList<SlotType1>();
                //authorPerson
                slot = new SlotType1();
                slot.setName("authorPerson");
                addValueToaSlot(slot, mv.getValue());
                slots.add(slot);
                documentMetadataValues.add(new MetadataValue(metadata, null, slots));
                documentEntryMetadata.remove(metadata);
                // update XDSSubmissionSet.author
                mv.setSlots(slots);
                mv.setLocalizedString(null);
                mv.setValue("");
            } else if (mv.getMetadata().getName().equals("XDSSubmissionSet.patientId")) {
                metadata = Metadata.getMetadataByName("XDSDocumentEntry.patientId");
                patientId = mv.getValue();
                documentMetadataValues.add(new MetadataValue(metadata, mv.getValue(), null));
                documentEntryMetadata.remove(metadata);
            } else if (mv.getMetadata().getName().equals("XDSSubmissionSet.contentTypeCode")) {
                metadata = Metadata.getMetadataByName("XDSDocumentEntry.classCode");
                documentMetadataValues.add(new MetadataValue(metadata, mv.getValue(), mv.getSlots()));
                documentEntryMetadata.remove(metadata);
            } else {
                continue;
            }
        }

        // generate creation time
        metadata = Metadata.getMetadataByName("creationTime");
        documentMetadataValues.add(new MetadataValue(metadata, Util.getDate(), null));

        // generate language code
        metadata = Metadata.getMetadataByName("languageCode");
        documentMetadataValues.add(new MetadataValue(metadata, "en", null));

        // generate sourcePatientId slot
        metadata = Metadata.getMetadataByName("sourcePatientId");
        documentMetadataValues.add(new MetadataValue(metadata, patientId, null));

        // generate sourcePatientInfo
        metadata = Metadata.getMetadataByName("sourcePatientInfo");
        documentMetadataValues.add(new MetadataValue(metadata, "PID-3|" + patientId, null));

        for (Metadata m : documentEntryMetadata) {
            documentMetadataValues.add(new MetadataValue(m, "", null));
        }

    }

    private void autoFillMetadataForEpsos() {
        Metadata metadata = null;
        documentMetadataValues = new ArrayList<MetadataValue>();
        patientId = null;

        for (MetadataValue mv : submissionSetMetadata) {
            if (mv.getMetadata().getName().equals("XDSSubmissionSet.author")) {
                metadata = Metadata.getMetadataByName("XDSDocumentEntry.author");
                SlotType1 slot = null;
                List<SlotType1> slots = new ArrayList<SlotType1>();
                //authorPerson
                slot = new SlotType1();
                slot.setName("authorPerson");
                addValueToaSlot(slot, mv.getValue());
                slots.add(slot);
                documentMetadataValues.add(new MetadataValue(metadata, null, slots));
                // update XDSSubmissionSet.author
                mv.setSlots(slots);
                mv.setLocalizedString(null);
                mv.setValue("");
            } else if (mv.getMetadata().getName().equals("XDSSubmissionSet.patientId")) {
                metadata = Metadata.getMetadataByName("XDSDocumentEntry.patientId");
                patientId = mv.getValue();
                documentMetadataValues.add(new MetadataValue(metadata, mv.getValue(), null));
            } else {
                continue;
            }
        }

        // classCode
        metadata = Metadata.getMetadataByName("XDSDocumentEntry.classCode");
        documentMetadataValues.add(createMetadataValueForEpsos(metadata));

        // typeCode
        metadata = Metadata.getMetadataByName("XDSDocumentEntry.typeCode");
        documentMetadataValues.add(createMetadataValueForEpsos(metadata));

        // healthcareFacilityTypeCode
        metadata = Metadata.getMetadataByName("XDSDocumentEntry.healthCareFacilityTypeCode");
        documentMetadataValues.add(createMetadataValueForEpsos(metadata));

        // practiceSettingCode
        metadata = Metadata.getMetadataByName("XDSDocumentEntry.practiceSettingCode");
        documentMetadataValues.add(createMetadataValueForEpsos(metadata));

        // confidentialityCode
        metadata = Metadata.getMetadataByName("XDSDocumentEntry.confidentialityCode");
        documentMetadataValues.add(createMetadataValueForEpsos(metadata));

        // generate creation time
        metadata = Metadata.getMetadataByName("creationTime");
        documentMetadataValues.add(new MetadataValue(metadata, Util.getDate(), null));

        // generate language code
        metadata = Metadata.getMetadataByName("languageCode");
        documentMetadataValues.add(new MetadataValue(metadata, "en", null));

        // generate sourcePatientId slot
        metadata = Metadata.getMetadataByName("sourcePatientId");
        documentMetadataValues.add(new MetadataValue(metadata, patientId, null));

        // generate sourcePatientInfo slot
        metadata = Metadata.getMetadataByName("sourcePatientInfo");
        documentMetadataValues.add(new MetadataValue(metadata, "PID-3|" + patientId, null));

        // if consent, add eventCodeList
        if (selectedTransaction.getKeyword().equals("ConsentService:put()")) {
            metadata = Metadata.getMetadataByName("XDSDocumentEntry.eventCodeList");
            List<Code> codes = Code.getCodesForTransaction(metadata.getUuid(), selectedTransaction, null);
            if (codes != null && !codes.isEmpty()) {
                for (Code code : codes) {
                    if (code.getDisplay().equals(selectedConsentCode)) {
                        MetadataValue mv = new MetadataValue(metadata, code.getCode(), null);
                        mv.setLocalizedString(code.getDisplay());
                        SlotType1 slot = new SlotType1();
                        slot.setName(XDRSRCSimulatorManager.CODING_SCHEME);
                        addValueToaSlot(slot, code.getCodingScheme());
                        List<SlotType1> slots = new ArrayList<SlotType1>();
                        slots.add(slot);
                        mv.setSlots(slots);
                        documentMetadataValues.add(mv);
                        break;
                    } else {
                        continue;
                    }
                }
            }
        }
    }

    public void getAvailableFormatCodes() {
        if (selectedAttachDocumentsMode.equals("upload")) {
            List<Code> availableCodes = Code.getCodesForTransaction("urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d", selectedTransaction, null);
            if (availableCodes != null && !availableCodes.isEmpty()) {
                formatCodes = new ArrayList<SelectItem>();
                for (Code code : availableCodes) {
                    formatCodes.add(new SelectItem(code, code.getDisplay()));
                }
            }
        }
    }

    /**
     * @param metadata
     * @return
     */
    private MetadataValue createMetadataValueForEpsos(Metadata metadata) {
        List<Code> availableCodes = Code.getCodesForTransaction(metadata.getUuid(), selectedTransaction, null);
        if (availableCodes != null && !availableCodes.isEmpty()) {
            Code code = availableCodes.get(0);
            return createMetadataValueFromCode(metadata, code);
        } else {
            return null;
        }
    }

    /**
     * @param metadata
     * @param code
     * @return
     */
    private MetadataValue createMetadataValueFromCode(Metadata metadata, Code code) {
        MetadataValue mv = new MetadataValue(metadata, code.getCode(), null);
        mv.setLocalizedString(code.getDisplay());
        SlotType1 slot = new SlotType1();
        slot.setName(XDRSRCSimulatorManager.CODING_SCHEME);
        addValueToaSlot(slot, code.getCodingScheme());
        List<SlotType1> slots = new ArrayList<SlotType1>();
        slots.add(slot);
        mv.setSlots(slots);
        return mv;
    }

    public void updateSpecificClient() {
        this.generateSubmissionSetMetadata();
        Contexts.getSessionContext().set("selectedConfiguration", this.selectedConfiguration);
        Contexts.getSessionContext().set("selectedAffinityDomain", this.selectedAffinityDomain);
    }

    public List<AffinityDomain> listAllAffinityDomains() {
        return listAllAffinityDomains(TRANSACTION_TYPE);
    }

    @Override
    public String getTheNameOfThePage() {
        String res = this.getNameOfTheAffinityDomainTransaction();
        if (res == null || res.equals("")) {
            res = "Provide and Register Set-b";
        }
        return res;
    }

    public String extractIDFromInitialisation() {
        if (this.selectedTransaction != null &&
                this.selectedTransaction.getKeyword().equals("DispensationService:initialize()") &&
                this.selectedAffinityDomain != null &&
                this.selectedAffinityDomain.getKeyword().equals("epSOS")) {
            return "DISPINIT";
        } else if (this.selectedTransaction != null &&
                this.selectedTransaction.getKeyword().equals("ConsentService:put()") &&
                this.selectedAffinityDomain != null &&
                this.selectedAffinityDomain.getKeyword().equals("epSOS")) {
            return "CONSPUT";
        } else if (this.selectedTransaction != null &&
                this.selectedTransaction.getKeyword().equals(XDRSRCSimulatorManager.ITI_41) &&
                this.selectedAffinityDomain != null &&
                this.selectedAffinityDomain.getKeyword().equals("IHE_XDS-b")) {
            return "XDSB1";
        } else if (this.selectedTransaction != null &&
                this.selectedTransaction.getKeyword().equals(XDRSRCSimulatorManager.ITI_41) &&
                this.selectedAffinityDomain != null &&
                this.selectedAffinityDomain.getKeyword().equals("IHE_XDR")) {
            return "XDR1";
        } else if (this.selectedTransaction != null &&
                this.selectedTransaction.getKeyword().equals(XDRSRCSimulatorManager.ITI_41) &&
                this.selectedAffinityDomain != null &&
                this.selectedAffinityDomain.getKeyword().equals("IHE_XDW_XDSb")) {
            return "XDW1";
        }
        return null;
    }

}
