package net.ihe.gazelle.xdstar.xdrsrc.ihe.action;

import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.xdstar.conf.model.RepositoryConfiguration;

public interface XDSCommonEditor {
	
	public void initListOptionalMetadata();

	public void initSelectedMetadata();
	
	public void init(AffinityDomain aff, Transaction trans, RepositoryConfiguration conf);

}
