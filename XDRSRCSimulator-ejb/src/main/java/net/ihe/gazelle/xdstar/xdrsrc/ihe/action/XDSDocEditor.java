package net.ihe.gazelle.xdstar.xdrsrc.ihe.action;

import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSDocumentEntry;

import org.richfaces.event.FileUploadEvent;

public interface XDSDocEditor extends XDSCommonEditor{
	
	public XDSDocumentEntry getSelectedXDSDocumentEntry();
	public void setSelectedXDSDocumentEntry(XDSDocumentEntry selectedXDSDocumentEntry);
	public void editXDSDocumentEntry(XDSDocumentEntry xx);
	
	public int getFileIndex();
	public void setFileIndex(int fileIndex);
	
	public void uploadListener(FileUploadEvent event);
	
	public String getOptionalityExtOnSelectedXDSDocumentEntry(ExternalIdentifierType ext);
	public void removeExtFromSelectedXDSDocumentEntry(ExternalIdentifierType ext);
	public String getOptionalitySlotOnSelectedXDSDocumentEntry(SlotType1 sl);
	public void removeSlotFromSelectedXDSDocumentEntry(SlotType1 sl);
	public String getOptionalityClassOnSelectedXDSDocumentEntry(ClassificationType ext);
	public void removeClassificationFromSelectedXDSDocumentEntry(ClassificationType cl);
	public void randomFulFilDocEntryMetadata();

}
