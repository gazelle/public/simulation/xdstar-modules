package net.ihe.gazelle.xdstar.xdrsrc.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

@Entity
@Name("oid")
@Table(name="oid", schema="public", uniqueConstraints=@UniqueConstraint(columnNames="id"))
@SequenceGenerator(name="oid_sequence", sequenceName="oid_id_seq", allocationSize=1)
public class OID implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static String DOCUMENT_OID = "document_root_oid";
	private final static String SUBMISSION_SET_OID = "submission_set_root_oid";
	private final static String EPSOS_SOURCE_OID = "epsos_source_root_oid";
	private final static String IHE_SOURCE_OID = "ihe_source_root_oid";
	
	
	@Id
	@GeneratedValue(generator="oid_sequence", strategy=GenerationType.SEQUENCE)
	@NotNull
	@Column(name="id", nullable=false, unique=true)
	private Integer id;
	
	public OID()
	{
		
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}
	
	/**
	 * 
	 * @return
	 */
	public static String getNewDocumentOid()
	{
		EntityManager em = (EntityManager) Component.getInstance("entityManager");
		String rootOid = ApplicationConfiguration.getValueOfVariable(DOCUMENT_OID);
		OID oid = new OID();
		oid = (OID) em.merge(oid);
		em.flush();
		return rootOid.concat(oid.getId().toString());
	}
	
	/**
	 * 
	 * @return
	 */
	public static String getNewSubmissionSetOid()
	{
		EntityManager em = (EntityManager) Component.getInstance("entityManager");
		String rootOid = ApplicationConfiguration.getValueOfVariable(SUBMISSION_SET_OID);
		OID oid = new OID();
		oid = (OID) em.merge(oid);
		em.flush();
		return rootOid.concat(oid.getId().toString());
	}
	
	/**
	 * 
	 * @return
	 */
	public static String getEPSOSSourceOid()
	{
		return ApplicationConfiguration.getValueOfVariable(EPSOS_SOURCE_OID);
	}
	
	public static String getIHESourceOid()
	{
		return ApplicationConfiguration.getValueOfVariable(IHE_SOURCE_OID);
	}
	
	public static String getSourceOid(String domain)
	{
		if (domain == null) return getEPSOSSourceOid() + " | " + getIHESourceOid();
		if (domain.toLowerCase().contains("epsos")) return getEPSOSSourceOid();
		if (domain.toLowerCase().contains("ihe")) return getIHESourceOid();
		return null;
	}

	@Override
	public String toString() {
		return "OID [id=" + id + "]";
	}
}
