package net.ihe.gazelle.xdstar.xdrepsos.action;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.xdstar.comon.util.ConfigurationFilter;
import net.ihe.gazelle.xdstar.conf.model.RepositoryConfigurationQuery;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.action.PnRIHEManager;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.PNRMessageType;
import net.ihe.gazelle.xdstar.xdrsrc.model.XDRMessage;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;

@Name("XDSbEPSOSManager")
@Scope(ScopeType.PAGE)
public class XDSbEPSOSManager extends PnRIHEManager implements Serializable {
	
	@Logger
	private static Log log;
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String validationResult;
	
	public String getValidationResult() {
		return validationResult;
	}

	public void setValidationResult(String validationResult) {
		this.validationResult = validationResult;
	}

	@Override
	public String getTheNameOfThePage() {
		String res = this.getNameOfTheAffinityDomainTransaction();
		if (res == null || res.equals("")) res = "XDS.b Provide and Register Document Set - epSOS affinity domain";
		return res;
	}
	
	public void init(){
		super.init();
		this.selectedTransaction = Transaction.GetTransactionByKeyword("ITI-41");
		this.selectedAffinityDomain = AffinityDomain.getAffinityDomainByKeyword("IHE_XDSb_epSOS");
		this.initFromMenu = true;
		Contexts.getSessionContext().set("selectedConfiguration", this.selectedConfiguration);
		Contexts.getSessionContext().set("selectedAffinityDomain", this.selectedAffinityDomain);
		Contexts.getSessionContext().set("selectedTransaction", this.selectedTransaction);
		this.submissionSet = this.generateXDSSubmissionSet();
		this.selectedXDSDocumentEntry = null;
		this.selectedXDSFolder = null;
		this.selectedXDSFolder = null;
		setMessage(new XDRMessage());
		this.selectedMessageType = PNRMessageType.NEW_SUBMISSION;
		this.editXDSSubmissionSet();
		this.initListOptionalMetadata();
	}
	
	@Override
	public void listAllConfigurations() {
		RepositoryConfigurationQuery rc = new RepositoryConfigurationQuery();
		if (selectedAffinityDomain != null) rc.listUsages().affinity().eq(selectedAffinityDomain);
		if (selectedTransaction != null) rc.listUsages().transaction().eq(selectedTransaction);
		rc.name().order(true);
		ConfigurationFilter.filterConfigurationQueryAccodingToUserCredential(rc);
		configurations = rc.getList();
	}

	@Override
	public void updateSpecificClient() {
		// Not applicable
	}

	@Override
	public List<AffinityDomain> listAllAffinityDomains() {
		List<AffinityDomain> laff = new ArrayList<AffinityDomain>();
		laff.add(AffinityDomain.getAffinityDomainByKeyword("IHE_XDSb_epSOS"));
		return laff;
	}
	
	
	public static void main(String[] args) throws IOException {
	}

}
