package net.ihe.gazelle.xdstar.xdrsrc.tools;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import net.ihe.gazelle.xdstar.core.MTOMBuilder;
import net.ihe.gazelle.xdstar.core.MTOMElement;
import net.ihe.gazelle.xdstar.core.MessageSenderCommon;
import net.ihe.gazelle.xdstar.xdrsrc.model.PnRRegisteredFile;
import net.ihe.gazelle.xdstar.xdrsrc.model.XDRMessage;

import org.apache.commons.io.IOUtils;
import org.jboss.seam.util.Base64;

public class MessageSender extends MessageSenderCommon {
	
	private XDRMessage message;
	private String boundaryUUID;
	private byte[] body;
	private StringBuffer contentTypeProperties;
	
	public MessageSender()
	{
		
	}

	public void setMessage(XDRMessage message) {
		this.message = message;
	}

	public XDRMessage getMessage() {
		return message;
	}
	
	public void setBoundaryUUID(String boundaryUUID) {
		this.boundaryUUID = boundaryUUID;
	}

	public String getBoundaryUUID() {
		return boundaryUUID;
	}

	public byte[] getBody() {
		return body;
	}

	public void setBody(byte[] body) {
		this.body = body;
	}

	public StringBuffer getContentTypeProperties() {
		return contentTypeProperties;
	}

	public void setContentTypeProperties(StringBuffer contentTypeProperties) {
		this.contentTypeProperties = contentTypeProperties;
	}

	public void send() throws Exception
	{
		buildMTOM();
		net.ihe.gazelle.util.Pair<String, String> resp2 = 
				this.sendToUrlAndGetResponse(message.getConfiguration().getUrl(), body, message, contentTypeProperties.toString());
		String resp = resp2 != null? resp2.getObject2():null;
		String resph = resp2 != null? resp2.getObject1():null;
		MetadataMessage respm = new MetadataMessage();
		respm.setMessageContent(resp);
		respm.setHttpHeader(resph);
		respm.setMessageType(MetadataMessageType.REGIQTRY_RESPONSE);
		message.setReceivedMessageContent(respm);
		message.getSentMessageContent().setMessageContent(new String(body));
	}
	
	/**
	 * 
	 * @throws IOException
	 */
	public void buildMTOM() throws IOException
	{
		Pair<String , byte[]> rr = MessageSender.buildMTOMFromData(message.getSentMessageContent().getMessageContent(), message.getRegisteredFiles());
		this.body = rr.getObject2();
		this.contentTypeProperties = new StringBuffer(rr.getObject1());
	}
	
	public static Pair<String, byte[]> buildMTOMFromData(String soapMessage, List<PnRRegisteredFile> listFile) throws IOException{
		List<MTOMElement> listDataToSend = new ArrayList<MTOMElement>();
		MTOMElement soap = new MTOMElement();
		soap.setContent(("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + soapMessage + "\r\n").getBytes(StandardCharsets.UTF_8));
		soap.setContentType(" application/xop+xml; charset=UTF-8; type=\"application/soap+xml\"");
		soap.setTransfertEncoding("binary");
		soap.setIndex(0);
		soap.setUuid(UUID.randomUUID().toString().replace("-", "").toUpperCase());
		listDataToSend.add(soap);
		
		if (listFile != null){
			for (PnRRegisteredFile file : listFile)
			{
				MTOMElement filer = new MTOMElement();
				filer.setContentType(file.getType());
				filer.setTransfertEncoding("binary");
				filer.setUuid(file.getUuid());
				filer.setIndex(file.getIndex());
				byte[] content;
				InputStream is = new FileInputStream(file.getPath());
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				IOUtils.copy(is, baos);
				content = baos.toByteArray();
				filer.setContent(content);
				listDataToSend.add(filer);
			}
		}
		
		byte[] body = MTOMBuilder.buildMTOM(listDataToSend);
		
		String boundaryUUID = MTOMBuilder.getBoundaryUUIDFromGeneratedMTOM(body);
		
		String boundary = "MIMEBoundaryurn_uuid_" + boundaryUUID;
		String boundaryStart = "0.urn:uuid:" + soap.getUuid() + "@ws.jboss.org";
		String contentTypeProperties = new String();
		contentTypeProperties = "multipart/related; ";
		if (listFile != null)
		{
			contentTypeProperties = contentTypeProperties + "boundary=" + boundary + "; ";
			contentTypeProperties = contentTypeProperties + "type=\"application/xop+xml\"; ";
			contentTypeProperties = contentTypeProperties + "start=\"<" + boundaryStart + ">\"; ";
			contentTypeProperties = contentTypeProperties + "start-info=\"application/soap+xml\"; ";
			contentTypeProperties = contentTypeProperties + "action=\"urn:ihe:iti:2007:ProvideAndRegisterDocumentSet-b\"";
		}
		Pair<String, byte[]> res = new Pair<String, byte[]>(contentTypeProperties, body);
		return res;
	}
	
	
	public static void main(String[] args) {
		String res = Base64.encodeFromFile("/home/aboufahj/Téléchargements/XDSb_REP_VISUS_2012/1685/1685.html");
		System.out.println(res);
	}
	
}
