package net.ihe.gazelle.xdstar.xdrsrc.action;

import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.lcm.SubmitObjectsRequestType;
import net.ihe.gazelle.metadata.common.RIMGenerator;
import net.ihe.gazelle.metadata.model.*;
import net.ihe.gazelle.rim.*;
import net.ihe.gazelle.simulator.common.model.Concept;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.common.xua.SignatureException;
import net.ihe.gazelle.xdstar.common.action.CommonSimulatorManager;
import net.ihe.gazelle.xdstar.common.action.ValueSetContainer;
import net.ihe.gazelle.xdstar.common.model.AbstractMessage;
import net.ihe.gazelle.xdstar.common.model.ContentType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import net.ihe.gazelle.xdstar.comon.util.*;
import net.ihe.gazelle.xdstar.conf.model.RegistryConfiguration;
import net.ihe.gazelle.xdstar.conf.model.RegistryConfigurationQuery;
import net.ihe.gazelle.xdstar.core.MessageSenderCommon;
import net.ihe.gazelle.xdstar.core.soap.SOAPRequestBuilder;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.action.XDSSubmissionSetManagement;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.*;
import net.ihe.gazelle.xdstar.xdrsrc.model.ODDMessage;
import net.ihe.gazelle.xdstar.xdrsrc.model.OID;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.*;

@Name("ODDManager")
@Scope(ScopeType.PAGE)
public class ODDManager extends CommonSimulatorManager<RegistryConfiguration, ODDMessage> implements Serializable {

    protected static final String XDSDocumentEntry_UUID = "urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248";
    protected static final String XDSSUBMISSIONSET_UUID = "urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd";
    protected static final String XDSFOLDER_UUID = "urn:uuid:d9d542f3-6cc4-48b6-8870-ea235fbc94c2";
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final String ASSOCIATION_TYPE_HAS_MEMBER = "urn:oasis:names:tc:ebxml-regrep:AssociationType:HasMember";

    private static final String CODING_SCHEME = "codingScheme";

    protected XDSSubmissionSet submissionSet;

    protected XDSFolder selectedXDSFolder;

    protected XDSDocumentEntry selectedXDSDocumentEntry;

    protected PNRMessageType selectedMessageType;

    protected GazelleTreeNodeImpl<Object> treeNodeSubmission;

    protected Boolean adviseNodeOpened = true;

    protected Boolean editSubmissionSet;

    protected Boolean editXDSDocumentEntry;

    protected Boolean editXDSFolder;

    protected SlotType1 selectedSlotType;

    protected String valueToAdd;

    protected InternationalStringType selectedInternationalStringType;

    protected List<SelectItem> listOptionalMetadata = new ArrayList<SelectItem>();

    protected String selectedMetadata;

    protected ClassificationType selectedClassificationType;

    protected String selectedSlotMetadata;

    //protected ODDMessage message;

    protected Integer fileIndex = 1;

    protected Integer folderIndex = 1;

    public ODDMessage getMessage() {
        return message;
    }

    public void setMessage(ODDMessage message) {
        this.message = message;
    }

    public String getSelectedSlotMetadata() {
        return selectedSlotMetadata;
    }

    public void setSelectedSlotMetadata(String selectedSlotMetadata) {
        this.selectedSlotMetadata = selectedSlotMetadata;
    }

    public ClassificationType getSelectedClassificationType() {
        return selectedClassificationType;
    }

    public void setSelectedClassificationType(
            ClassificationType selectedClassificationType) {
        this.selectedClassificationType = selectedClassificationType;
    }

    public String getSelectedMetadata() {
        return selectedMetadata;
    }

    public void setSelectedMetadata(String selectedMetadata) {
        this.selectedMetadata = selectedMetadata;
    }

    public List<SelectItem> getListOptionalMetadata() {
        return listOptionalMetadata;
    }

    public void setListOptionalMetadata(List<SelectItem> listOptionalMetadata) {
        this.listOptionalMetadata = listOptionalMetadata;
    }

    public InternationalStringType getSelectedInternationalStringType() {
        return selectedInternationalStringType;
    }

    public void setSelectedInternationalStringType(
            InternationalStringType selectedInternationalStringType) {
        this.selectedInternationalStringType = selectedInternationalStringType;
    }

    public SlotType1 getSelectedSlotType() {
        return selectedSlotType;
    }

    public void setSelectedSlotType(SlotType1 selectedSlotType) {
        this.selectedSlotType = selectedSlotType;
    }

    public String getValueToAdd() {
        return valueToAdd;
    }

    public void setValueToAdd(String valueToAdd) {
        this.valueToAdd = valueToAdd;
    }

    public Boolean getEditXDSDocumentEntry() {
        return editXDSDocumentEntry;
    }

    public void setEditXDSDocumentEntry(Boolean editXDSDocumentEntry) {
        this.editXDSDocumentEntry = editXDSDocumentEntry;
    }

    public Boolean getEditXDSFolder() {
        return editXDSFolder;
    }

    public void setEditXDSFolder(Boolean editXDSFolder) {
        this.editXDSFolder = editXDSFolder;
    }

    public Boolean getEditSubmissionSet() {
        return editSubmissionSet;
    }

    public void setEditSubmissionSet(Boolean editSubmissionSet) {
        this.editSubmissionSet = editSubmissionSet;
    }

    public Boolean getAdviseNodeOpened() {
        return adviseNodeOpened;
    }

    public void setAdviseNodeOpened(Boolean adviseNodeOpened) {
        this.adviseNodeOpened = adviseNodeOpened;
    }

    public XDSFolder getSelectedXDSFolder() {
        return selectedXDSFolder;
    }

    public void setSelectedXDSFolder(XDSFolder selectedXDSFolder) {
        this.selectedXDSFolder = selectedXDSFolder;
    }

    public XDSDocumentEntry getSelectedXDSDocumentEntry() {
        return selectedXDSDocumentEntry;
    }

    public void setSelectedXDSDocumentEntry(
            XDSDocumentEntry selectedXDSDocumentEntry) {
        this.selectedXDSDocumentEntry = selectedXDSDocumentEntry;
    }

    public PNRMessageType getSelectedMessageType() {
        return selectedMessageType;
    }

    public void setSelectedMessageType(PNRMessageType selectedMessageType) {
        this.selectedMessageType = selectedMessageType;
    }

    public XDSSubmissionSet getSubmissionSet() {
        return submissionSet;
    }

    public void setSubmissionSet(XDSSubmissionSet submissionSet) {
        this.submissionSet = submissionSet;
    }

    public List<PNRMessageType> availableMTs() {
        return Arrays.asList(PNRMessageType.values());
    }

    public GazelleTreeNodeImpl<Object> getTreeNodeSubmission() {
        return XDSSubmissionSetManagement.getTreeNodeSubmissionSet(this.submissionSet);
    }

    public void setTreeNodeSubmission(GazelleTreeNodeImpl<Object> treeNodeSubmission) {
        this.treeNodeSubmission = treeNodeSubmission;
    }

    @Deprecated
    public void getTreeNodeSubmissionSet() {
        GazelleTreeNodeImpl<Object> rootNode = new GazelleTreeNodeImpl<Object>();

        GazelleTreeNodeImpl<Object> submissionTreeNode = new GazelleTreeNodeImpl<Object>();
        submissionTreeNode.setData(this.submissionSet);
        rootNode.addChild(1, submissionTreeNode);

        int i = 0;
        int j;
        if (this.submissionSet.getListXDSFolder() != null) {
            for (XDSFolder xx : this.submissionSet.getListXDSFolder()) {
                GazelleTreeNodeImpl<Object> folderTreeNode = new GazelleTreeNodeImpl<Object>();
                folderTreeNode.setData(xx);
                List<XDSDocumentEntry> listDE = xx.getListXDSDocumentEntry();
                j = 0;
                if (listDE != null) {
                    for (XDSDocumentEntry doc : listDE) {
                        GazelleTreeNodeImpl<Object> docNode = new GazelleTreeNodeImpl<Object>();
                        docNode.setData(doc);
                        folderTreeNode.addChild(j, docNode);
                        j++;
                    }
                }
                submissionTreeNode.addChild(i, folderTreeNode);
                i++;
            }
        }
        if (this.submissionSet.getListXDSDocumentEntry() != null) {
            for (XDSDocumentEntry xxd : this.submissionSet.getListXDSDocumentEntry()) {
                GazelleTreeNodeImpl<Object> ddd = new GazelleTreeNodeImpl<Object>();
                ddd.setData(xxd);
                submissionTreeNode.addChild(i, ddd);
                i++;
            }

        }
        this.treeNodeSubmission = rootNode;
    }

    @Create
    public void initCreation() {
        useXUA = false;

        this.selectedAffinityDomain = AffinityDomain.getAffinityDomainByKeyword("IHE_XDS-b");
        this.selectedTransaction = Transaction.GetTransactionByKeyword("ITI-61");
        this.submissionSet = this.generateXDSSubmissionSet();
        this.selectedXDSDocumentEntry = null;
        this.selectedXDSFolder = null;
        this.selectedXDSFolder = null;
        this.message = new ODDMessage();
        this.selectedMessageType = PNRMessageType.NEW_SUBMISSION;
        this.displayResultPanel = false;
        this.editXDSSubmissionSet();
        this.initListOptionalMetadata();
    }

    @Override
    public void init() {
        super.init();
        this.initCreation();
    }

    public void addFolder(XDSSubmissionSet xx) {
        if (xx != null) {
            if (xx.getListXDSFolder() == null) {
                xx.setListXDSFolder(new ArrayList<XDSFolder>());
            }
            this.editXDSFolder(this.generateXDSFolder());
            xx.getListXDSFolder().add(this.selectedXDSFolder);

        }
    }

    public void addDocumentEntry(XDSSubmissionSet xx) {
        if (xx != null) {
            if (xx.getListXDSDocumentEntry() == null) {
                xx.setListXDSDocumentEntry(new ArrayList<XDSDocumentEntry>());
            }
            this.editXDSDocumentEntry(this.generateXDSDocumentEntry());
            xx.getListXDSDocumentEntry().add(this.selectedXDSDocumentEntry);
        }
    }

    public void addDocumentEntry(XDSFolder xx) {
        if (xx != null) {
            if (xx.getListXDSDocumentEntry() == null) {
                xx.setListXDSDocumentEntry(new ArrayList<XDSDocumentEntry>());
            }
            this.editXDSDocumentEntry(this.generateXDSDocumentEntry());
            xx.getListXDSDocumentEntry().add(this.selectedXDSDocumentEntry);
        }
    }

    private XDSDocumentEntry generateXDSDocumentEntry() {
        XDSDocumentEntry res = new XDSDocumentEntry();
        HQLQueryBuilder<ExtrinsicObjectMetadata> hh = new HQLQueryBuilder<ExtrinsicObjectMetadata>(ExtrinsicObjectMetadata.class);
        hh.addEq("uuid", XDSDocumentEntry_UUID);
        hh.addEq("usages.affinity", this.selectedAffinityDomain);
        hh.addEq("usages.transaction", this.selectedTransaction);
        ExtrinsicObjectMetadata rpm = hh.getList().get(0);
        RIMGenerator.generateExtrinsicObjectTypeChild(rpm, res);
        res.setId("urn:uuid:" + UUID.randomUUID());
        this.updateSourceIdAndUniqueId(res);
        res.setIndex(this.fileIndex);
        res.setTitle(res.getTitle() + " " + fileIndex);
        this.fileIndex++;
        return res;
    }

    private XDSFolder generateXDSFolder() {
        XDSFolder res = new XDSFolder();
        HQLQueryBuilder<RegistryPackageMetadata> hh = new HQLQueryBuilder<RegistryPackageMetadata>(RegistryPackageMetadata.class);
        hh.addEq("uuid", XDSFOLDER_UUID);
        hh.addEq("usages.affinity", this.selectedAffinityDomain);
        hh.addEq("usages.transaction", this.selectedTransaction);
        RegistryPackageMetadata rpm = hh.getList().get(0);
        RIMGenerator.generateRegistryPackageTypeChild(rpm, res);
        res.setId("urn:uuid:" + UUID.randomUUID());
        res.setTitle(res.getTitle() + " " + folderIndex);
        this.folderIndex++;
        this.updateSourceIdAndUniqueId(res);
        return res;
    }

    protected XDSSubmissionSet generateXDSSubmissionSet() {
        XDSSubmissionSet res = new XDSSubmissionSet();
        HQLQueryBuilder<RegistryPackageMetadata> hh = new HQLQueryBuilder<RegistryPackageMetadata>(RegistryPackageMetadata.class);
        hh.addEq("uuid", XDSSUBMISSIONSET_UUID);
        hh.addEq("usages.affinity", this.selectedAffinityDomain);
        hh.addEq("usages.transaction", this.selectedTransaction);
        RegistryPackageMetadata rpm = hh.getList().get(0);
        RIMGenerator.generateRegistryPackageTypeChild(rpm, res);
        res.setId("urn:uuid:" + UUID.randomUUID());
        this.updateSourceIdAndUniqueId(res);
        return res;
    }

    private void updateSourceIdAndUniqueId(RegistryObjectType ro) {
        if (ro != null) {
            for (ExternalIdentifierType ext : ro.getExternalIdentifier()) {
                ExternalIdentifierMetadata extm = RIMGenerator.extWeak.get(ext);
                if (extm.getDisplayAttributeName().contains(".sourceId")) {
                    ext.setValue(OID.getIHESourceOid());
                }
                if (extm.getDisplayAttributeName().contains(".uniqueId")) {
                    if (ro instanceof XDSSubmissionSet) {
                        ext.setValue(OID.getNewSubmissionSetOid());
                    } else {
                        ext.setValue(OID.getNewDocumentOid());
                    }
                }
            }
        }
    }

    public void editXDSDocumentEntry(XDSDocumentEntry xx) {
        this.selectedXDSDocumentEntry = xx;
        this.editXDSFolder = false;
        this.editSubmissionSet = false;
        this.editXDSDocumentEntry = true;
    }

    public void editXDSFolder(XDSFolder xx) {
        this.selectedXDSFolder = xx;
        this.editSubmissionSet = false;
        this.editXDSDocumentEntry = false;
        this.editXDSFolder = true;
    }

    public void editXDSSubmissionSet() {
        this.editSubmissionSet = true;
        this.editXDSDocumentEntry = false;
        this.editXDSFolder = false;
    }

    public String viewSubmissionSet() {
        String res = null;
        if (this.submissionSet != null) {
            try {
                JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.rim");
                Marshaller m = jc.createMarshaller();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new XDSNamespacePrefixMapper());
                m.marshal((RegistryPackageType) this.submissionSet, baos);
                res = baos.toString();
                res = Util.prettyFormat(res);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    public String getDisplayNameExt(ExternalIdentifierType ext) {
        return RIMGenerator.extWeak.get(ext).getDisplayAttributeName();
    }

    public String getOptionalityExtOnSubmissionSet(ExternalIdentifierType ext) {
        RegistryPackageMetadata rpm = RIMGenerator.rpWeak.get(this.submissionSet);
        ExternalIdentifierMetadata extm = RIMGenerator.extWeak.get(ext);
        for (ExternalIdentifierMetadata ee : rpm.getExt_identifier_required()) {
            if (extm.getUuid().equals(ee.getUuid())) {
                return "R";
            }
        }
        return "O";
    }

    public String getDisplayNameClass(ClassificationType ext) {
        return RIMGenerator.clWeak.get(ext).getDisplayAttributeName();
    }

    public String getOptionalityClassOnSubmissionSet(ClassificationType ext) {
        RegistryPackageMetadata rpm = RIMGenerator.rpWeak.get(this.submissionSet);
        ClassificationMetaData extm = RIMGenerator.clWeak.get(ext);
        for (ClassificationMetaData ee : rpm.getClassification_required()) {
            if (extm.getUuid().equals(ee.getUuid())) {
                return "R";
            }
        }
        return "O";
    }

    public String rownSpanClass(ClassificationType cl) {
        int i = 2;
        if (cl != null) {
            SlotType1 codingScheme = this.getCodingSchemeSlot(cl);
            if (codingScheme != null) {
                SlotMetadata slm = RIMGenerator.slWeak.get(codingScheme);
                if ((slm.getValueset() != null) && (!slm.getValueset().equals(""))) {
                    i = 0;
                }
            }
        }
        i = i + cl.getSlot().size();
        if (this.hasOptionalSlotYet(cl)) {
            i = i + 1;
        }
        return String.valueOf(i);
    }

    private SlotType1 getCodingSchemeSlot(ClassificationType cl) {
        if (cl != null) {
            if (cl.getSlot() != null) {
                for (SlotType1 sl : cl.getSlot()) {
                    if (sl.getName().equals(CODING_SCHEME)) {
                        return sl;
                    }
                }
            }
        }
        return null;
    }

    public void initSelectedSlotType(SlotType1 sl) {
        this.selectedSlotType = sl;
        this.valueToAdd = null;
        this.fileIndex = 1;
        this.folderIndex = 1;
    }

    public boolean canAddName(InternationalStringType name) {
        if (name != null) {
            if ((name.getLocalizedString() != null) && (name.getLocalizedString().size() > 0)) {
                return false;
            }
        }
        return true;
    }

    public void initSelectedInternationalStringType(ClassificationType cl) {
        if (cl.getName() == null) {
            cl.setName(new InternationalStringType());
        }
        this.selectedInternationalStringType = cl.getName();
        this.valueToAdd = null;
    }

    public void setValueOfTheName(String val) {
        this.selectedInternationalStringType.setLocalizedString(new ArrayList<LocalizedStringType>());
        this.selectedInternationalStringType.getLocalizedString().add(new LocalizedStringType());
        this.selectedInternationalStringType.getLocalizedString().get(0).setValue(valueToAdd);
    }

    public void initListOptionalMetadata() {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        this.listOptionalMetadata = new ArrayList<SelectItem>();
        this.listOptionalMetadata.add(new SelectItem(null, "please select.."));
        RegistryObjectMetadata ss = null;
        RegistryObjectType rot = null;
        if (editSubmissionSet) {
            ss = RIMGenerator.rpWeak.get(this.submissionSet);
            rot = this.submissionSet;
        }
        if (editXDSFolder) {
            ss = RIMGenerator.rpWeak.get(this.selectedXDSFolder);
            rot = this.selectedXDSFolder;
        }
        if (editXDSDocumentEntry) {
            ss = RIMGenerator.docWeak.get(this.selectedXDSDocumentEntry);
            rot = this.selectedXDSDocumentEntry;
        }
        ss = em.find(RegistryObjectMetadata.class, ss.getId());
        if (ss.getClassification_optional() != null) {
            for (ClassificationMetaData clm : ss.getClassification_optional()) {
                boolean isused = false;
                for (ClassificationType cl : rot.getClassification()) {
                    if (cl.getClassificationScheme().equals(clm.getUuid())) {
                        isused = true;
                    }
                }
                if (!isused) {
                    this.listOptionalMetadata.add(new SelectItem(clm.getDisplayAttributeName(), clm.getDisplayAttributeName()));
                }
            }
        }

        if (ss.getExt_identifier_optional() != null) {
            for (ExternalIdentifierMetadata clm : ss.getExt_identifier_optional()) {
                boolean isused = false;
                for (ExternalIdentifierType cl : rot.getExternalIdentifier()) {
                    if (cl.getIdentificationScheme().equals(clm.getUuid())) {
                        isused = true;
                    }
                }
                if (!isused) {
                    this.listOptionalMetadata.add(new SelectItem(clm.getDisplayAttributeName(), clm.getDisplayAttributeName()));
                }
            }
        }

        if (ss.getSlot_optional() != null) {
            for (SlotMetadata slm : ss.getSlot_optional()) {
                boolean isused = false;
                for (SlotType1 sl : rot.getSlot()) {
                    if (sl.getName().equals(slm.getName())) {
                        isused = true;
                    }
                }
                if (!isused) {
                    this.listOptionalMetadata.add(new SelectItem(slm.getName(), slm.getName()));
                }
            }
        }
        this.selectedMetadata = null;
    }

    public void addNewMetadataToSubmissionSet() {
        HQLQueryBuilder<ClassificationMetaData> hh = new HQLQueryBuilder<ClassificationMetaData>(ClassificationMetaData.class);
        hh.addEq("displayAttributeName", this.selectedMetadata);
        List<ClassificationMetaData> dd = hh.getList();
        RegistryObjectType rot = null;
        if (this.editSubmissionSet) {
            rot = this.submissionSet;
        }
        if (this.editXDSDocumentEntry) {
            rot = this.selectedXDSDocumentEntry;
        }
        if (this.editXDSFolder) {
            rot = this.selectedXDSFolder;
        }
        if ((dd != null) && (dd.size() > 0)) {
            ClassificationType cl = RIMGenerator.generateClassificationType(dd.get(0));
            rot.getClassification().add(cl);
        }

        HQLQueryBuilder<ExternalIdentifierMetadata> gg = new HQLQueryBuilder<ExternalIdentifierMetadata>(ExternalIdentifierMetadata.class);
        gg.addEq("displayAttributeName", this.selectedMetadata);
        List<ExternalIdentifierMetadata> dda = gg.getList();
        if ((dda != null) && (dda.size() > 0)) {
            ExternalIdentifierType cl = RIMGenerator.generateExternalIdentifierType(dda.get(0));
            rot.getExternalIdentifier().add(cl);
        }

        HQLQueryBuilder<SlotMetadata> QQ = new HQLQueryBuilder<SlotMetadata>(SlotMetadata.class);
        QQ.addEq("name", this.selectedMetadata);
        List<SlotMetadata> qqs = QQ.getList();
        if ((qqs != null) && (qqs.size() > 0)) {
            SlotType1 sl = RIMGenerator.generateSlot(qqs.get(0));
            rot.getSlot().add(sl);
        }
        this.initListOptionalMetadata();
        this.selectedMetadata = null;
    }

    public void removeClassificationFromSubmissionSet(ClassificationType cl) {
        this.submissionSet.getClassification().remove(cl);
        this.initListOptionalMetadata();
    }

    public void removeExtFromSubmissionSet(ExternalIdentifierMetadata ext) {
        this.submissionSet.getExternalIdentifier().remove(ext);
        this.initListOptionalMetadata();
    }

    public boolean canAddValueToSlot(SlotType1 sl) {
        if (sl != null) {
            SlotMetadata slm = RIMGenerator.slWeak.get(sl);
            if (slm != null) {
                if ((slm.getMultiple() != null) && (slm.getMultiple() == false)) {
                    if ((sl.getValueList() != null) && (sl.getValueList().getValue().size() > 0)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public String getOptionalitySlotOnSubmissionSet(SlotType1 sl) {
        RegistryPackageMetadata rpm = RIMGenerator.rpWeak.get(this.submissionSet);
        SlotMetadata slm = RIMGenerator.slWeak.get(sl);
        if (rpm.getSlot_required() != null) {
            for (SlotMetadata ee : rpm.getSlot_required()) {
                if (slm.getName().equals(ee.getName())) {
                    return "R";
                }
            }
        }
        return "O";
    }

    public void removeSlotFromSubmissionSet(SlotType1 sl) {
        this.submissionSet.getSlot().remove(sl);
        this.initListOptionalMetadata();
    }

    public boolean hasOptionalSlotYet(ClassificationType cl) {
        ClassificationMetaData clm = RIMGenerator.clWeak.get(cl);
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        clm = em.find(ClassificationMetaData.class, clm.getId());
        if (clm != null) {
            if ((clm.getSlot_optional() != null) && (clm.getSlot_optional().size() > 0)) {
                for (SlotMetadata slm : clm.getSlot_optional()) {
                    boolean present = false;
                    for (SlotType1 sl : cl.getSlot()) {
                        if (slm.getName().equals(sl.getName())) {
                            present = true;
                            break;
                        }
                    }
                    if (!present) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public List<SelectItem> getListOptionalSlotMetadata() {
        List<SelectItem> res = new ArrayList<SelectItem>();
        res.add(new SelectItem(null, "please select.."));
        if (this.selectedClassificationType != null) {
            ClassificationMetaData clm = RIMGenerator.clWeak.get(this.selectedClassificationType);
            for (SlotMetadata slm : clm.getSlot_optional()) {
                boolean alreadyExist = false;
                for (SlotType1 sl : this.selectedClassificationType.getSlot()) {
                    if (sl.getName().equals(slm.getName())) {
                        alreadyExist = true;
                    }
                }
                if (!alreadyExist) {
                    res.add(new SelectItem(slm.getName(), slm.getName()));
                }
            }
        }
        this.selectedSlotMetadata = null;
        return res;
    }

    public void addSelectedSlotToClassification() {
        if (this.selectedClassificationType != null) {
            if (this.selectedSlotMetadata != null) {
                ClassificationMetaData clm = RIMGenerator.clWeak.get(this.selectedClassificationType);
                for (SlotMetadata slm : clm.getSlot_optional()) {
                    if (slm.getName().equals(this.selectedSlotMetadata)) {
                        SlotType1 sl = RIMGenerator.generateSlot(slm);
                        this.selectedClassificationType.getSlot().add(sl);
                        break;
                    }
                }
            }
        }
        this.selectedSlotMetadata = null;
    }

    public String getOptionalitySlotOnClassification(SlotType1 sl, ClassificationType cl) {
        ClassificationMetaData clm = RIMGenerator.clWeak.get(cl);
        String res = "O";
        for (SlotMetadata slm : clm.getSlot_required()) {
            if (slm.getName().equals(sl.getName())) {
                res = "R";
                break;
            }
        }
        return res;
    }

    //---------------XDS Folder -------------------

    public String getOptionalityExtOnSelectedXDSFolder(ExternalIdentifierType ext) {
        RegistryPackageMetadata rpm = RIMGenerator.rpWeak.get(this.selectedXDSFolder);
        ExternalIdentifierMetadata extm = RIMGenerator.extWeak.get(ext);
        for (ExternalIdentifierMetadata ee : rpm.getExt_identifier_required()) {
            if (extm.getUuid().equals(ee.getUuid())) {
                return "R";
            }
        }
        return "O";
    }

    public void removeExtFromSelectedXDSFolder(ExternalIdentifierType ext) {
        this.selectedXDSFolder.getExternalIdentifier().remove(ext);
        this.initListOptionalMetadata();
    }

    public String getOptionalitySlotOnSelectedXDSFolder(SlotType1 sl) {
        RegistryPackageMetadata rpm = RIMGenerator.rpWeak.get(this.selectedXDSFolder);
        SlotMetadata slm = RIMGenerator.slWeak.get(sl);
        for (SlotMetadata ee : rpm.getSlot_required()) {
            if (slm.getName().equals(ee.getName())) {
                return "R";
            }
        }
        return "O";
    }

    public void removeSlotFromSelectedXDSFolder(SlotType1 sl) {
        this.selectedXDSFolder.getSlot().remove(sl);
        this.initListOptionalMetadata();
    }

    public String getOptionalityClassOnSelectedXDSFolder(ClassificationType ext) {
        RegistryPackageMetadata rpm = RIMGenerator.rpWeak.get(this.selectedXDSFolder);
        ClassificationMetaData extm = RIMGenerator.clWeak.get(ext);
        for (ClassificationMetaData ee : rpm.getClassification_required()) {
            if (extm.getUuid().equals(ee.getUuid())) {
                return "R";
            }
        }
        return "O";
    }

    public void removeClassificationFromSelectedXDSFolder(ClassificationType cl) {
        this.selectedXDSFolder.getClassification().remove(cl);
        this.initListOptionalMetadata();
    }

    // -------------------------------------------

    // -------XDS Docuement Entry -----------------------------

    public String getOptionalityExtOnSelectedXDSDocumentEntry(ExternalIdentifierType ext) {
        ExtrinsicObjectMetadata rpm = RIMGenerator.docWeak.get(this.selectedXDSDocumentEntry);
        ExternalIdentifierMetadata extm = RIMGenerator.extWeak.get(ext);
        for (ExternalIdentifierMetadata ee : rpm.getExt_identifier_required()) {
            if (extm.getUuid().equals(ee.getUuid())) {
                return "R";
            }
        }
        return "O";
    }

    public void removeExtFromSelectedXDSDocumentEntry(ExternalIdentifierType ext) {
        this.selectedXDSDocumentEntry.getExternalIdentifier().remove(ext);
        this.initListOptionalMetadata();
    }

    public String getOptionalitySlotOnSelectedXDSDocumentEntry(SlotType1 sl) {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        ExtrinsicObjectMetadata extrm = RIMGenerator.docWeak.get(this.selectedXDSDocumentEntry);
        SlotMetadata slm = RIMGenerator.slWeak.get(sl);
        extrm = em.find(ExtrinsicObjectMetadata.class, extrm.getId());
        if (extrm.getSlot_required() != null) {
            for (SlotMetadata ee : extrm.getSlot_required()) {
                if (slm.getName().equals(ee.getName())) {
                    return "R";
                }
            }
        }
        return "O";
    }

    public void removeSlotFromSelectedXDSDocumentEntry(SlotType1 sl) {
        this.selectedXDSDocumentEntry.getSlot().remove(sl);
        this.initListOptionalMetadata();
    }

    public String getOptionalityClassOnSelectedXDSDocumentEntry(ClassificationType ext) {
        ExtrinsicObjectMetadata rpm = RIMGenerator.docWeak.get(this.selectedXDSDocumentEntry);
        ClassificationMetaData extm = RIMGenerator.clWeak.get(ext);
        if (rpm.getClassification_required() != null) {
            for (ClassificationMetaData ee : rpm.getClassification_required()) {
                if (extm.getUuid().equals(ee.getUuid())) {
                    return "R";
                }
            }
        }
        return "O";
    }

    public void removeClassificationFromSelectedXDSDocumentEntry(ClassificationType cl) {
        this.selectedXDSDocumentEntry.getClassification().remove(cl);
        this.initListOptionalMetadata();
    }

    public boolean isValueSet(ClassificationType cl) {
        boolean res = false;
        if (cl != null) {
            if (cl.getSlot() != null) {
                for (SlotType1 sl : cl.getSlot()) {
                    if (sl.getName().equals(CODING_SCHEME)) {
                        SlotMetadata slm = RIMGenerator.slWeak.get(sl);
                        if ((slm.getValueset() != null) && (!slm.getValueset().equals(""))) {
                            return true;
                        }
                    }
                }
            }
        }
        return res;

    }

    public List<SelectItem> listValueSet(ClassificationType cl) {
        List<SelectItem> res = new ArrayList<SelectItem>();
        res.add(new SelectItem(null, "please select .."));
        if (cl != null) {
            for (SlotType1 sl : cl.getSlot()) {
                if (sl.getName().equals(CODING_SCHEME)) {
                    SlotMetadata slm = RIMGenerator.slWeak.get(sl);
                    if ((slm.getValueset() != null) && (!slm.getValueset().equals(""))) {
                        String[] listValueSet = slm.getValueset().split("\\s*,\\s*");
                        for (String valueSet : listValueSet) {
                            List<Concept> lc = ValueSetContainer.getListConcepts().get(valueSet);

                            if (lc == null) {
                                lc = SVSConsumerForSimulator.getConceptsListFromValueSet(valueSet, null);
                                if (lc != null) {
                                    Collections.sort(lc);
                                }
                                ValueSetContainer.getListConcepts().put(valueSet, lc);
                            }
                            if (lc != null) {
                                for (Concept concept : lc) {
                                    SelectItem si = new SelectItem(concept.getCode(), concept.getDisplayName());
                                    res.add(si);
                                }
                            }
                        }
                    }
                }
            }
        }
        return res;
    }

    public void updateClassificationByValueSet(ClassificationType cl) {
        if ((cl != null)) {

            ClassificationMetaData clm = RIMGenerator.clWeak.get(cl);
            SlotMetadata slmc = null;
            if (clm.getSlot_required() != null) {
                for (SlotMetadata slm : clm.getSlot_required()) {
                    if (slm.getName().equals(CODING_SCHEME)) {
                        slmc = slm;
                        break;
                    }
                }
            }

            if (cl.getNodeRepresentation() == null) {
                for (SlotType1 sl : cl.getSlot()) {
                    if (sl.getName().equals(CODING_SCHEME)) {
                        sl.getValueList().setValue(new ArrayList<String>());
                    }
                }
                return;
            }

            if (slmc != null) {
                String valueSets = slmc.getValueset();
                if ((valueSets != null) && (!valueSets.equals(""))) {
                    String[] listValueSet = valueSets.split("\\s*,\\s*");
                    for (String valueSet : listValueSet) {
                        Concept cp = SVSConsumerForSimulator.getConceptForCode(valueSet, null, cl.getNodeRepresentation());
                        if (cp != null) {
                            cl.setName(RIMBuilderCommon.createNameOrDescription(cp.getDisplayName()));
                            for (SlotType1 sl : cl.getSlot()) {
                                if (sl.getName().equals(CODING_SCHEME)) {
                                    sl.getValueList().setValue(new ArrayList<String>());
                                    sl.getValueList().getValue().add(cp.getCodeSystem());
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // --------------------------------------------------------

    public SubmitObjectsRequestType generateSubmitObjectsRequestType() throws CloneNotSupportedException {
        net.ihe.gazelle.rim.ObjectFactory ofrim = new net.ihe.gazelle.rim.ObjectFactory();
        SubmitObjectsRequestType res = new SubmitObjectsRequestType();
        res.setRegistryObjectList(new RegistryObjectListType());
        res.getRegistryObjectList().getIdentifiableGroup().add(
                ofrim.createRegistryObjectListTypeRegistryPackage(this.submissionSet.getCleanedRegistryPackageType()));
        if (this.submissionSet.getListXDSFolder() != null) {
            for (XDSFolder folder : this.submissionSet.getListXDSFolder()) {
                res.getRegistryObjectList().getIdentifiableGroup().add(
                        ofrim.createRegistryObjectListTypeRegistryPackage(folder.getCleanedRegistryPackageType(this.submissionSet.getPatientId())));

                AssociationType1 sub2folder = RIMBuilderCommon.createAssociation(ASSOCIATION_TYPE_HAS_MEMBER, this.submissionSet.getId(), folder
                        .getId(), null, null, null);
                res.getRegistryObjectList().getIdentifiableGroup().add(
                        ofrim.createRegistryObjectListTypeAssociation(sub2folder));

                if (folder.getListXDSDocumentEntry() != null) {
                    for (XDSDocumentEntry doc : folder.getListXDSDocumentEntry()) {
                        res.getRegistryObjectList().getIdentifiableGroup().add(
                                ofrim.createRegistryObjectListTypeExtrinsicObject(doc.getCleanedExtrinsicObjectType(this.submissionSet.getPatientId
                                        ())));
                        AssociationType1 sub2doc = RIMBuilderCommon.createAssociation(ASSOCIATION_TYPE_HAS_MEMBER, this.submissionSet.getId(), doc
                                .getId(), "SubmissionSetStatus", "Original", null);
                        res.getRegistryObjectList().getIdentifiableGroup().add(
                                ofrim.createRegistryObjectListTypeAssociation(sub2doc));

                        AssociationType1 folder2doc = RIMBuilderCommon.createAssociation(ASSOCIATION_TYPE_HAS_MEMBER, folder.getId(), doc.getId(),
                                null, null, null);
                        res.getRegistryObjectList().getIdentifiableGroup().add(
                                ofrim.createRegistryObjectListTypeAssociation(folder2doc));

                        AssociationType1 sub2folder2doc = RIMBuilderCommon.createAssociation(ASSOCIATION_TYPE_HAS_MEMBER, this.submissionSet.getId
                                (), folder2doc.getId(), null, null, null);
                        res.getRegistryObjectList().getIdentifiableGroup().add(
                                ofrim.createRegistryObjectListTypeAssociation(sub2folder2doc));

                        if (doc.getAssociationType() != null) {
                            AssociationType1 sub3doc = RIMBuilderCommon.createAssociation(doc.getAssociationType().getValue(), doc.getId(), doc
                                    .getTargetDocument(), null, null, null);
                            res.getRegistryObjectList().getIdentifiableGroup().add(
                                    ofrim.createRegistryObjectListTypeAssociation(sub3doc));
                        }
                    }
                }
            }
        }

        if (this.submissionSet.getListXDSDocumentEntry() != null) {
            for (XDSDocumentEntry doc : this.submissionSet.getListXDSDocumentEntry()) {
                res.getRegistryObjectList().getIdentifiableGroup().add(
                        ofrim.createRegistryObjectListTypeExtrinsicObject(doc.getCleanedExtrinsicObjectType(this.submissionSet.getPatientId())));
                AssociationType1 sub2doc = RIMBuilderCommon.createAssociation(ASSOCIATION_TYPE_HAS_MEMBER, this.submissionSet.getId(), doc.getId(),
                        "SubmissionSetStatus", "Original", null);
                res.getRegistryObjectList().getIdentifiableGroup().add(
                        ofrim.createRegistryObjectListTypeAssociation(sub2doc));

                if (doc.getAssociationType() != null) {
                    AssociationType1 sub3doc = RIMBuilderCommon.createAssociation(doc.getAssociationType().getValue(), doc.getId(), doc
                            .getTargetDocument(), null, null, null);
                    res.getRegistryObjectList().getIdentifiableGroup().add(
                            ofrim.createRegistryObjectListTypeAssociation(sub3doc));
                }
            }
        }
        return res;
    }

    public void sendProvideAndRegisterSetb() throws CloneNotSupportedException, JAXBException, ParserConfigurationException, SignatureException {
        message = new ODDMessage();
        message.setMessageType("OnDemandDocument");
        message.setTransaction(selectedTransaction);
        message.setAffinityDomain(selectedAffinityDomain);
        message.setContentType(ContentType.SOAP_MESSAGE);
        message.setTimeStamp(new Date());
        message.setGazelleDriven(false);
        message.setConfiguration(selectedConfiguration);

        MetadataMessage sent = new MetadataMessage();
        String messageToSend = this.createMessage();
        sent.setMessageContent(messageToSend);
        sent.setMessageType(MetadataMessageType.SUBMIT_OBJECT_REQUEST);
        message.setSentMessageContent(sent);
        message = AbstractMessage.storeMessage(message);

        MessageSenderCommon sender = new MessageSenderCommon(selectedConfiguration.getUrl(), message);
        try {
            sender.sendMessageSOAP(MetadataMessageType.REGIQTRY_RESPONSE);
            message = (ODDMessage) sender.getRequest();
            message = AbstractMessage.storeMessage(message);
        } catch (Exception e) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.xca.initgw.errorWhenSendingMessage", e.getMessage
                    ());
        }
        Contexts.getSessionContext().set("messagesToDisplay", message);
        displayResultPanel = true;
    }

    private String createMessage() {
        try {
            SubmitObjectsRequestType sor = this.generateSubmitObjectsRequestType();
            JAXBContext jc = JAXBContext.newInstance(SubmitObjectsRequestType.class);
            Marshaller m = jc.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new XDSNamespacePrefixMapper());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            m.marshal(sor, baos);
            String res = baos.toString();
            String receiverUrl = this.selectedConfiguration.getUrl();
            String soapAction = "urn:ihe:iti:2010:RegisterOnDemandDocumentEntry";
            res = SOAPRequestBuilder.createSOAPMessage(res, useXUA, praticianID, attributes, null, receiverUrl, soapAction, false);
            return res;
        } catch (Exception e) {
            return null;
        }
    }


    @Override
    public void listAllConfigurations() {
        RegistryConfigurationQuery rc = new RegistryConfigurationQuery();
        if (selectedAffinityDomain != null) {
            rc.listUsages().affinity().eq(selectedAffinityDomain);
        }
        if (selectedTransaction != null) {
            rc.listUsages().transaction().eq(selectedTransaction);
        }
        rc.name().order(true);
        ConfigurationFilter.filterConfigurationQueryAccodingToUserCredential(rc);
        configurations = rc.getList();
    }

    @Override
    public void updateSpecificClient() {
        this.initCreation();
    }

    @Override
    public List<AffinityDomain> listAllAffinityDomains() {
        // Not applicable
        return null;
    }

    public void deleteXDSFolder(XDSFolder xdsfolder) {
        if ((this.submissionSet != null) && (xdsfolder != null)) {
            this.submissionSet.getListXDSFolder().remove(xdsfolder);
        }
        this.editSubmissionSet = true;
        this.editXDSDocumentEntry = false;
        this.editXDSFolder = false;
    }

    public void deleteXDSDocument(XDSDocumentEntry xdsdoc) {
        if ((this.submissionSet != null) && (xdsdoc != null)) {
            if (this.submissionSet.getListXDSDocumentEntry().contains(xdsdoc)) {
                this.submissionSet.getListXDSDocumentEntry().remove(xdsdoc);
            }
            if (this.submissionSet.getListXDSFolder() != null) {
                for (XDSFolder xdsfolder : this.submissionSet.getListXDSFolder()) {
                    if (xdsfolder.getListXDSDocumentEntry() != null && xdsfolder.getListXDSDocumentEntry().contains(xdsdoc)) {
                        xdsfolder.getListXDSDocumentEntry().remove(xdsdoc);
                    }
                }
            }
        }
        this.editSubmissionSet = true;
        this.editXDSDocumentEntry = false;
        this.editXDSFolder = false;
    }

    public List<SelectItem> getListAssociationTypes() {
        List<SelectItem> res = new ArrayList<SelectItem>();
        res.add(new SelectItem(null, "please select.."));
        for (AssociationType ass : AssociationType.values()) {
            res.add(new SelectItem(ass, ass.getName()));
        }
        return res;
    }

    public void randomFulFilDocEntryMetadata() {
        if (this.selectedXDSDocumentEntry != null) {
            for (ClassificationType clas : this.selectedXDSDocumentEntry.getClassification()) {
                List<SelectItem> lsi = listValueSet(clas);
                if ((lsi != null) && lsi.size() > 1) {
                    SelectItem si = lsi.get((int) Math.floor(Math.random() * (lsi.size() - 2)) + 1);
                    clas.setNodeRepresentation((String) si.getValue());
                    this.updateClassificationByValueSet(clas);
                }
            }
        }
    }

    public String previewMessage() {
        return this.createMessage();
    }

    @Override
    public String getTheNameOfThePage() {
        return "Register On-Demand Document Entry (ITI-61 : XDS.b profile)";
    }


}
