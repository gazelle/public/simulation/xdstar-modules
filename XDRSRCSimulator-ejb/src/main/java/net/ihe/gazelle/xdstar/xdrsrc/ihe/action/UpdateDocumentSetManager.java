package net.ihe.gazelle.xdstar.xdrsrc.ihe.action;

import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.lcm.SubmitObjectsRequestType;
import net.ihe.gazelle.metadata.common.RIMGenerator;
import net.ihe.gazelle.metadata.model.ExternalIdentifierMetadata;
import net.ihe.gazelle.metadata.model.RegistryPackageMetadata;
import net.ihe.gazelle.metadata.model.RegistryPackageMetadataQuery;
import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.InternationalStringType;
import net.ihe.gazelle.rim.RegistryObjectType;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.common.xua.SignatureException;
import net.ihe.gazelle.xdstar.common.model.AbstractMessage;
import net.ihe.gazelle.xdstar.common.model.ContentType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import net.ihe.gazelle.xdstar.comon.util.ConfigurationFilter;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespacePrefixMapper;
import net.ihe.gazelle.xdstar.conf.model.RegistryConfiguration;
import net.ihe.gazelle.xdstar.conf.model.RegistryConfigurationQuery;
import net.ihe.gazelle.xdstar.core.MessageSenderCommon;
import net.ihe.gazelle.xdstar.core.soap.SOAPRequestBuilder;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSSubmissionSet;
import net.ihe.gazelle.xdstar.xdrsrc.model.OID;
import net.ihe.gazelle.xdstar.xdrsrc.model.UpdateDocumentSetMessage;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.model.SelectItem;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Name("updateDocumentSet")
@Scope(ScopeType.PAGE)
public class UpdateDocumentSetManager extends XDSMetadataEditor<RegistryConfiguration,UpdateDocumentSetMessage> {

    /**
     *
     */

    private static final long serialVersionUID = 1L;

    protected static final String XDSSUBMISSIONSET_UUID = "urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd";

    protected GazelleTreeNodeImpl<Object> treeNodeSubmission;

    protected String valueToAdd;

    protected InternationalStringType selectedInternationalStringType;

    /**
     *
     */

    public String getValueToAdd() {
        return valueToAdd;
    }

    public void setValueToAdd(String valueToAdd) {
        this.valueToAdd = valueToAdd;
    }

    public InternationalStringType getSelectedInternationalStringType() {
        return selectedInternationalStringType;
    }

    public List<SelectItem> getListOptionalMetadata() {
        return listOptionalMetadata;
    }

    public void setListOptionalMetadata(List<SelectItem> listOptionalMetadata) {
        this.listOptionalMetadata = listOptionalMetadata;
    }



    /**
     *
     */

    @Create
    @Override
    public void initCreation() {

        super.initCreation();

        this.submissionSet = this.generateXDSSubmissionSet();
        this.message = new UpdateDocumentSetMessage();
        this.displayResultPanel = false;
        this.editXDSSubmissionSet();
        this.initListOptionalMetadata();
    }

    public void init(AffinityDomain aff, Transaction trans, RegistryConfiguration conf) {
        initSamlAttributes();
        useXUA = false;

        this.submissionSet = this.generateXDSSubmissionSet();
        this.selectedConfiguration = conf;
        this.selectedAffinityDomain = aff;
        this.selectedTransaction = trans;
        this.editXDSSubmissionSet();
        this.initListOptionalMetadata();
    }

    protected XDSSubmissionSet generateXDSSubmissionSet() {
        XDSSubmissionSet res = new XDSSubmissionSet();
        RegistryPackageMetadataQuery query = new RegistryPackageMetadataQuery();
        query.uuid().eq(XDSSUBMISSIONSET_UUID);
        query.usages().affinity().eq(this.selectedAffinityDomain);
        query.usages().transaction().eq(this.selectedTransaction);
        RegistryPackageMetadata rpm = query.getUniqueResult();
        RIMGenerator.generateRegistryPackageTypeChild(rpm, res);
        res.setId("urn:uuid:" + UUID.randomUUID());
        this.updateSourceIdAndUniqueId(res);
        return res;
    }


    private void updateSourceIdAndUniqueId(RegistryObjectType ro) {
        if (ro != null) {
            for (ExternalIdentifierType ext : ro.getExternalIdentifier()) {
                ExternalIdentifierMetadata extm = RIMGenerator.extWeak.get(ext);
                if (extm.getDisplayAttributeName().contains(".sourceId")) {
                    ext.setValue(OID.getIHESourceOid());
                }
                if (extm.getDisplayAttributeName().contains(".uniqueId")) {
                    if (ro instanceof XDSSubmissionSet) {
                        ext.setValue(OID.getNewSubmissionSetOid());
                    } else {
                        ext.setValue(OID.getNewDocumentOid());
                    }
                }
            }
        }
    }

    public boolean canAddName(InternationalStringType name) {
        return !(name != null && (name.getLocalizedString() != null) && (name.getLocalizedString().size() > 0));
    }

    public List<SelectItem> listValueSet(ClassificationType cl) {
        return XDSCommonModule.listValueSet(cl);
    }

    public void initSelectedInternationalStringType(ClassificationType cl) {
        if (cl.getName() == null) {
            cl.setName(new InternationalStringType());
        }
        this.selectedInternationalStringType = cl.getName();
        this.valueToAdd = null;
    }

    @Override
    public String getTheNameOfThePage() {
        return "Update Document Set (ITI-57 : XDS.b profile)";
    }

    @Override
    public void listAllConfigurations() {
        RegistryConfigurationQuery rc = new RegistryConfigurationQuery();
        if (selectedAffinityDomain != null) {
            rc.listUsages().affinity().eq(selectedAffinityDomain);
        }
        if (selectedTransaction != null) {
            rc.listUsages().transaction().eq(selectedTransaction);
        }
        rc.name().order(true);
        ConfigurationFilter.filterConfigurationQueryAccodingToUserCredential(rc);
        configurations = rc.getList();
    }

    @Override
    public List<AffinityDomain> listAllAffinityDomains() {
        // Not applicable
        return null;
    }

    @Override
    public void updateSpecificClient() {
        this.initCreation();
    }

    public GazelleTreeNodeImpl<Object> getTreeNodeSubmission() {
        return XDSSubmissionSetManagement.getTreeNodeSubmissionSet(this.submissionSet);
    }

    public void setTreeNodeSubmission(GazelleTreeNodeImpl<Object> treeNodeSubmission) {
        this.treeNodeSubmission = treeNodeSubmission;
    }

    public boolean hasOptionalSlotYet(ClassificationType cl) {
        return XDSCommonModule.hasOptionalSlotYet(cl);
    }

    public boolean isValueSet(ClassificationType cl) {
        return XDSCommonModule.isValueSet(cl);
    }

    public void updateClassificationByValueSet(ClassificationType cl) {
        XDSCommonModule.updateClassificationByValueSet(cl, cl.getNodeRepresentation());
    }

    private String createMessage() {
        try {
            SubmitObjectsRequestType sor = this.generateSubmitObjectsRequestType();
            JAXBContext jc = JAXBContext.newInstance(SubmitObjectsRequestType.class);
            Marshaller m = jc.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new XDSNamespacePrefixMapper());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            m.marshal(sor, baos);
            String res = baos.toString();
            String receiverUrl = this.selectedConfiguration.getUrl();
            String soapAction = "urn:ihe:iti:2017:UpdateDocumentSet";
            res = SOAPRequestBuilder.createSOAPMessage(res, useXUA, praticianID, attributes, null, receiverUrl, soapAction, false);
            return res;
        } catch (Exception e) {
            return null;
        }
    }

    public void sendUpdateDocumentSet() throws CloneNotSupportedException, JAXBException, ParserConfigurationException, SignatureException {
        message = new UpdateDocumentSetMessage();
        message.setMessageType("UpdateDocumentSet");
        message.setTransaction(selectedTransaction);
        message.setAffinityDomain(selectedAffinityDomain);
        message.setContentType(ContentType.SOAP_MESSAGE);
        message.setTimeStamp(new Date());
        message.setGazelleDriven(false);
        message.setConfiguration(selectedConfiguration);

        MetadataMessage sent = new MetadataMessage();
        String messageToSend = this.createMessage();
        sent.setMessageContent(messageToSend);
        sent.setMessageType(MetadataMessageType.SUBMIT_OBJECT_REQUEST);
        message.setSentMessageContent(sent);
        message = AbstractMessage.storeMessage(message);

        MessageSenderCommon sender = new MessageSenderCommon(selectedConfiguration.getUrl(), message);
        try {
            sender.sendMessageSOAP(MetadataMessageType.REGIQTRY_RESPONSE);
            message = (UpdateDocumentSetMessage) sender.getRequest();
            message = AbstractMessage.storeMessage(message);
        } catch (Exception e) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.updateDocumentSet.errorWhenSendingMessage", e.getMessage
                    ());
        }
        Contexts.getSessionContext().set("messagesToDisplay", message);
        displayResultPanel = true;
    }

    public String previewMessage() {
        return this.createMessage();
    }

    public String getDisplayNameExt(ExternalIdentifierType ext) {
        return RIMGenerator.extWeak.get(ext).getDisplayAttributeName();
    }

    public String rownSpanClass(ClassificationType cl) {
        return XDSCommonModule.rownSpanClass(cl);
    }

    public String getDisplayNameClass(ClassificationType ext) {
        return RIMGenerator.clWeak.get(ext).getDisplayAttributeName();
    }


    @Override
    public String getTransactionKeyword() {
        return "ITI-57";
    }

    @Override
    public String getAffinityDomainKeyword() {
        return "IHE_XDS-b";
    }
}
