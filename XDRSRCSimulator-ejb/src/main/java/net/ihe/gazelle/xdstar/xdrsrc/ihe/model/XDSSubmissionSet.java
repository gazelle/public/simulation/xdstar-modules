package net.ihe.gazelle.xdstar.xdrsrc.ihe.model;

import java.util.List;
import java.util.UUID;

import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.RegistryPackageType;
import net.ihe.gazelle.xdstar.comon.util.RIMBuilderCommon;
import net.ihe.gazelle.xdstar.comon.util.Util;

import com.rits.cloning.Cloner;

public class XDSSubmissionSet extends RegistryPackageType {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<XDSFolder> listXDSFolder;
	
	private List<XDSDocumentEntry> listXDSDocumentEntry;
	
	private String title;
	
	private String patientId;




	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId != null? patientId.trim():null;
	}

	public List<XDSFolder> getListXDSFolder() {
		return listXDSFolder;
	}

	public void setListXDSFolder(List<XDSFolder> listXDSFolder) {
		this.listXDSFolder = listXDSFolder;
	}

	public void setListXDSDocumentEntry(List<XDSDocumentEntry> listXDSDocumentEntry) {
		this.listXDSDocumentEntry = listXDSDocumentEntry;
	}

	public List<XDSDocumentEntry> getListXDSDocumentEntry() {
		return listXDSDocumentEntry;
	}
	
	public String getTitle(){
		if (this.getName() != null){
			if ((this.getName().getLocalizedString() != null) && (this.getName().getLocalizedString().size()>0)){
				return this.getName().getLocalizedString().get(0).getValue();
			}
		}
		return null;
	}

	public void setTitle(String title) {
		this.title = title;
		this.setName(RIMBuilderCommon.createNameOrDescription(this.title));
	}
	
	public RegistryPackageType getCleanedRegistryPackageType() throws CloneNotSupportedException{
		Cloner cloner = new Cloner();
		RegistryPackageType res = cloner.deepClone(this);
		
		ClassificationType cl = new ClassificationType();
		cl.setClassificationNode("urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd");
		cl.setClassifiedObject(res.getId());
		cl.setId("urn:uuid:" + UUID.randomUUID().toString());
		cl.setObjectType("urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification");
		res.getClassification().add(cl);
		
		ExternalIdentifierType ext_patientId = RIMBuilderCommon.createExternalIdentifierType("XDSSubmissionSet.patientId", 
				"urn:uuid:6b5aea1a-874d-4603-a4bc-96a0a7b38446",
				this.getId(), this.patientId);
		res.getExternalIdentifier().add(ext_patientId);
		
		res.getSlot().add(RIMBuilderCommon.createSlot("submissionTime", Util.getDate()));
		
		for (ClassificationType cll : res.getClassification()) {
			cll.setClassifiedObject(res.getId());
		}
		
		for (ExternalIdentifierType ext : res.getExternalIdentifier()) {
			ext.setRegistryObject(res.getId());
		}
	
		return res;
	}
	
	public static void main(String[] args) {
		XDSSubmissionSet ss = new XDSSubmissionSet();
		ss.setId("zzzzzz");
		try {
			ss.getCleanedRegistryPackageType();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}
	
	

}
