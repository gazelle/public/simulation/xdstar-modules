package net.ihe.gazelle.xdstar.xdrsrc.ihe.action;

import net.ihe.gazelle.lcm.SubmitObjectsRequestType;
import net.ihe.gazelle.metadata.common.RIMGenerator;
import net.ihe.gazelle.metadata.model.ClassificationMetaData;
import net.ihe.gazelle.metadata.model.ExternalIdentifierMetadata;
import net.ihe.gazelle.metadata.model.RegistryObjectMetadata;
import net.ihe.gazelle.metadata.model.SlotMetadata;
import net.ihe.gazelle.rim.*;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import net.ihe.gazelle.xdstar.common.action.CommonSimulatorManager;
import net.ihe.gazelle.xdstar.common.model.AbstractMessage;
import net.ihe.gazelle.xdstar.comon.util.RIMBuilderCommon;
import net.ihe.gazelle.xdstar.conf.model.RepositoryConfiguration;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSDocumentEntry;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSFolder;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSSubmissionSet;
import net.ihe.gazelle.xdstar.xdrsrc.model.PnRRegisteredFile;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class  XDSMetadataEditor<T extends SystemConfiguration, X extends AbstractMessage> extends CommonSimulatorManager<T, X> implements Serializable, XDSDocEditor, XDSFolderEditor, XDSSubmissionSetEditor {

    protected XDSDocumentEntry selectedXDSDocumentEntry = null;

    protected Boolean editSubmissionSet;

    protected Boolean editXDSDocumentEntry;

    protected Boolean editXDSFolder;

    protected Integer fileIndex = 1;

    private static Logger log = LoggerFactory.getLogger(XDSMetadataEditor.class);

    protected XDSFolder selectedXDSFolder = null;

    protected Integer folderIndex = 1;

    protected XDSSubmissionSet submissionSet;

    protected String selectedMetadata;

    protected List<SelectItem> listOptionalMetadata = new ArrayList<SelectItem>();

    protected String selectedSlotMetadata;

    protected SlotType1 selectedSlotType;

    protected String valueToAdd;

    protected ClassificationType selectedClassificationType;

    private static final String ASSOCIATION_TYPE_HAS_MEMBER = "urn:oasis:names:tc:ebxml-regrep:AssociationType:HasMember";


    @Create
    public void initCreation() {
        useXUA = false;
        this.selectedAffinityDomain = AffinityDomain.getAffinityDomainByKeyword(getAffinityDomainKeyword());
        this.selectedTransaction = Transaction.GetTransactionByKeyword(getTransactionKeyword());

    }

    public String getSelectedMetadata() {
        return selectedMetadata;
    }

    public void setSelectedMetadata(String selectedMetadata) {
        this.selectedMetadata = selectedMetadata;
    }

    public Boolean getEditXDSDocumentEntry() {
        return editXDSDocumentEntry;
    }

    public void setEditXDSDocumentEntry(Boolean editXDSDocumentEntry) {
        this.editXDSDocumentEntry = editXDSDocumentEntry;
    }

    public String getSelectedSlotMetadata() {
        return selectedSlotMetadata;
    }

    public void setSelectedSlotMetadata(String selectedSlotMetadata) {
        this.selectedSlotMetadata = selectedSlotMetadata;
    }

    public ClassificationType getSelectedClassificationType() {
        return selectedClassificationType;
    }

    public void setSelectedClassificationType(
            ClassificationType selectedClassificationType) {
        this.selectedClassificationType = selectedClassificationType;
    }



    public Boolean getEditSubmissionSet() {
        return editSubmissionSet;
    }

    public void setEditSubmissionSet(Boolean editSubmissionSet) {
        this.editSubmissionSet = editSubmissionSet;
    }

    public SlotType1 getSelectedSlotType() {
        return selectedSlotType;
    }

    public void setSelectedSlotType(SlotType1 selectedSlotType) {
        this.selectedSlotType = selectedSlotType;
    }

    public String getValueToAdd() {
        return valueToAdd;
    }

    public void setValueToAdd(String valueToAdd) {
        this.valueToAdd = valueToAdd;
    }


    public void initSelectedSlotType(SlotType1 sl) {
        this.selectedSlotType = sl;
        this.valueToAdd = null;
        this.fileIndex = 1;
        this.folderIndex = 1;
    }

    public boolean canAddValueToSlot(SlotType1 sl) {
        if (sl != null) {
            SlotMetadata slm = RIMGenerator.slWeak.get(sl);
            if (slm != null) {
                if ((slm.getMultiple() != null) && (slm.getMultiple() == false)) {
                    if ((sl.getValueList() != null) && (sl.getValueList().getValue().size() > 0)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    @Override
    public XDSDocumentEntry getSelectedXDSDocumentEntry() {
        return selectedXDSDocumentEntry;
    }

    @Override
    public void setSelectedXDSDocumentEntry(XDSDocumentEntry selectedXDSDocumentEntry) {
        this.selectedXDSDocumentEntry = selectedXDSDocumentEntry;
    }

    @Override
    public void editXDSDocumentEntry(XDSDocumentEntry xx) {
        this.selectedXDSDocumentEntry = xx;
        this.editXDSFolder = false;
        this.editSubmissionSet = false;
        this.editXDSDocumentEntry = true;
    }

    @Override
    public int getFileIndex() {
        return this.fileIndex;
    }

    @Override
    public void setFileIndex(int fileIndex) {
        this.fileIndex = fileIndex;
    }

    @Override
    public void uploadListener(FileUploadEvent event) {
        UploadedFile item = event.getUploadedFile();
        FileOutputStream fos = null;
        try {
            PnRRegisteredFile registeredFile = PnRRegisteredFile.saveFile(item.getName(), item.getContentType());
            registeredFile.setDocumentId("document" + registeredFile.getId());
            registeredFile.setIndex(this.selectedXDSDocumentEntry.getIndex());
            registeredFile.setUuid(this.selectedXDSDocumentEntry.getId().split(":")[this.selectedXDSDocumentEntry.getId().split(":").length - 1]
                    .replace("-", "").toUpperCase());
            File fileToWrite = new File(registeredFile.getPath());
            fos = new FileOutputStream(fileToWrite);
            if (item.getData() != null && item.getData().length > 0) {
                fos.write(item.getData());
                fos.close();
            } else {
                FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "The file is empty !");
            }
            this.selectedXDSDocumentEntry.setRelatedFile(registeredFile);
            this.selectedXDSDocumentEntry.setMimeType(this.selectedXDSDocumentEntry.getRelatedFile().getType());
        } catch (IOException e) {
            log.error("An error occurred when uploaded file with name " + item.getName());
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.xdrsrc.cannotStoreFile", item.getName());
        } finally {
            try {
                fos.close();
            } catch (Exception e) {
                log.error("Cannot close channel");
            }
        }
    }

    @Override
    public String getOptionalityExtOnSelectedXDSDocumentEntry(ExternalIdentifierType ext) {
        return DocumentEntryManagement.getOptionalityExtOnSelectedXDSDocumentEntry(this.selectedXDSDocumentEntry, ext);
    }

    @Override
    public void removeExtFromSelectedXDSDocumentEntry(ExternalIdentifierType ext) {
        DocumentEntryManagement.removeExtFromSelectedXDSDocumentEntry(this.selectedXDSDocumentEntry, ext, this);
    }

    @Override
    public String getOptionalitySlotOnSelectedXDSDocumentEntry(SlotType1 sl) {
        return DocumentEntryManagement.getOptionalitySlotOnSelectedXDSDocumentEntry(this.selectedXDSDocumentEntry, sl);
    }

    @Override
    public void removeSlotFromSelectedXDSDocumentEntry(SlotType1 sl) {
        DocumentEntryManagement.removeSlotFromSelectedXDSDocumentEntry(this.selectedXDSDocumentEntry, sl, this);
    }

    @Override
    public String getOptionalityClassOnSelectedXDSDocumentEntry(ClassificationType ext) {
        return DocumentEntryManagement.getOptionalityClassOnSelectedXDSDocumentEntry(this.selectedXDSDocumentEntry, ext);
    }

    @Override
    public void removeClassificationFromSelectedXDSDocumentEntry(ClassificationType cl) {
        DocumentEntryManagement.removeClassificationFromSelectedXDSDocumentEntry(this.selectedXDSDocumentEntry, cl, this);
    }

    @Override
    public void randomFulFilDocEntryMetadata() {
        DocumentEntryManagement.randomFulFilDocEntryMetadata(selectedXDSDocumentEntry);
    }

    @Override
    public XDSFolder getSelectedXDSFolder() {
        return selectedXDSFolder;
    }

    @Override
    public void setSelectedXDSFolder(XDSFolder selectedXDSFolder) {
        this.selectedXDSFolder = selectedXDSFolder;
    }

    @Override
    public void editXDSFolder(XDSFolder xx) {
        this.selectedXDSFolder = xx;
        this.editSubmissionSet = false;
        this.editXDSDocumentEntry = false;
        this.editXDSFolder = true;
    }

    @Override
    public Boolean getEditXDSFolder() {
        return editXDSFolder;
    }

    @Override
    public void setEditXDSFolder(Boolean editXDSFolder) {
        this.editXDSFolder = editXDSFolder;
    }

    @Override
    public int getFolderIndex() {
        return this.folderIndex;
    }

    @Override
    public void setFolderIndex(int folderIndex) {
        this.folderIndex = folderIndex;
    }

    @Override
    public void addDocumentEntry(XDSFolder xx) {
        AffinityDomain aff = new AffinityDomain();
        aff.setKeyword(getAffinityDomainKeyword());
        Transaction tr = new Transaction();
        tr.setKeyword(getTransactionKeyword());
        DocumentEntryManagement.addNewDocumentEntryToXDSFolder(this.selectedXDSFolder, this, aff, tr);
        initVersionInfo(selectedXDSDocumentEntry);
    }

    @Override
    public String getOptionalityExtOnSelectedXDSFolder(ExternalIdentifierType ext) {
        return XDSFolderManagement.getOptionalityExtOnSelectedXDSFolder(this.selectedXDSFolder, ext);
    }

    @Override
    public void removeExtFromSelectedXDSFolder(ExternalIdentifierType ext) {
        XDSFolderManagement.removeExtFromSelectedXDSFolder(this.selectedXDSFolder, ext, this);
    }

    @Override
    public String getOptionalitySlotOnSelectedXDSFolder(SlotType1 sl) {
        return XDSFolderManagement.getOptionalitySlotOnSelectedXDSFolder(this.selectedXDSFolder, sl);
    }

    @Override
    public void removeSlotFromSelectedXDSFolder(SlotType1 sl) {
        XDSFolderManagement.removeSlotFromSelectedXDSFolder(this.selectedXDSFolder, sl, this);
    }

    @Override
    public String getOptionalityClassOnSelectedXDSFolder(ClassificationType ext) {
        return XDSFolderManagement.getOptionalityClassOnSelectedXDSFolder(this.selectedXDSFolder, ext);
    }

    @Override
    public void removeClassificationFromSelectedXDSFolder(ClassificationType cl) {
        XDSFolderManagement.removeClassificationFromSelectedXDSFolder(this.selectedXDSFolder, cl, this);
    }

    @Override
    public void addFolder(XDSSubmissionSet xx) {
        AffinityDomain aff = new AffinityDomain();
        aff.setKeyword(getAffinityDomainKeyword());
        Transaction tr = new Transaction();
        tr.setKeyword(getTransactionKeyword());
        XDSFolderManagement.addNewFolderToXDSSubmissionSet(xx, this, aff, tr);
    }

    @Override
    public void deleteXDSFolder(XDSFolder xdsfolder) {
        if ((this.submissionSet != null) && (xdsfolder != null)) {
            this.submissionSet.getListXDSFolder().remove(xdsfolder);
        }
        this.editSubmissionSet = true;
        this.editXDSDocumentEntry = false;
        this.editXDSFolder = false;
    }

    @Override
    public void setSubmissionSet(XDSSubmissionSet submissionSet) {
        this.submissionSet = submissionSet;
    }

    @Override
    public XDSSubmissionSet getSubmissionSet() {
        return submissionSet;
    }

    @Override
    public void editXDSSubmissionSet(XDSSubmissionSet xds) {
        this.editSubmissionSet = true;
        this.editXDSDocumentEntry = false;
        this.editXDSFolder = false;
    }

    @Deprecated
    @Override
    public void getTreeNodeSubmissionSet() {

    }

    @Override
    public void addDocumentEntry(XDSSubmissionSet xx) {
        AffinityDomain aff = new AffinityDomain();
        aff.setKeyword(getAffinityDomainKeyword());
        Transaction tr = new Transaction();
        tr.setKeyword(getTransactionKeyword());
        DocumentEntryManagement.addNewDocumentEntryToXDSSubmissionSet(this.submissionSet, this, aff, tr);
        initVersionInfo(selectedXDSDocumentEntry);
    }



    @Override
    public void editXDSSubmissionSet() {
        this.editSubmissionSet = true;
        this.editXDSDocumentEntry = false;
        this.editXDSFolder = false;
    }

    @Override
    public String getOptionalityExtOnSubmissionSet(ExternalIdentifierType ext) {
        return XDSSubmissionSetManagement.getOptionalityExtOnSubmissionSet(this.submissionSet, ext);
    }

    @Override
    public String getOptionalityClassOnSubmissionSet(ClassificationType ext) {
        return XDSSubmissionSetManagement.getOptionalityClassOnSubmissionSet(this.submissionSet, ext);
    }

    @Override
    public void addNewMetadataToSubmissionSet() {
        RegistryObjectType rot = null;
        if (this.editSubmissionSet) {
            rot = this.submissionSet;
        }
        if (this.editXDSDocumentEntry) {
            rot = this.selectedXDSDocumentEntry;
        }
        if (this.editXDSFolder) {
            rot = this.selectedXDSFolder;
        }
        XDSCommonModule.addNewMetadataToRegistryObjectType(rot, selectedMetadata, this);
    }

    @Override
    public void removeClassificationFromSubmissionSet(ClassificationType cl) {
        XDSSubmissionSetManagement.removeClassificationFromSubmissionSet(this.submissionSet, cl, this);
    }

    @Override
    public void removeExtFromSubmissionSet(ExternalIdentifierMetadata ext) {
        XDSSubmissionSetManagement.removeExtFromSubmissionSet(this.submissionSet, ext, this);
    }

    @Override
    public String getOptionalitySlotOnSubmissionSet(SlotType1 sl) {
        return XDSSubmissionSetManagement.getOptionalitySlotOnSubmissionSet(this.submissionSet, sl);
    }

    @Override
    public void removeSlotFromSubmissionSet(SlotType1 sl) {
        XDSSubmissionSetManagement.removeSlotFromSubmissionSet(this.submissionSet, sl, this);
    }

    @Override
    public void initListOptionalMetadata() {
        EntityManager em = (EntityManager) Component.getInstance("entityManager");
        this.listOptionalMetadata = new ArrayList<SelectItem>();
        this.listOptionalMetadata.add(new SelectItem(null, "please select.."));
        RegistryObjectMetadata ss = null;
        RegistryObjectType rot = null;
        if (editSubmissionSet) {
            ss = RIMGenerator.rpWeak.get(this.submissionSet);
            rot = this.submissionSet;
        }
        if (editXDSFolder) {
            ss = RIMGenerator.rpWeak.get(this.selectedXDSFolder);
            rot = this.selectedXDSFolder;
        }
        if (editXDSDocumentEntry) {
            ss = RIMGenerator.docWeak.get(this.selectedXDSDocumentEntry);
            rot = this.selectedXDSDocumentEntry;
        }
        ss = em.find(RegistryObjectMetadata.class, ss.getId());
        if (ss.getClassification_optional() != null) {
            for (ClassificationMetaData clm : ss.getClassification_optional()) {
                boolean isused = false;
                for (ClassificationType cl : rot.getClassification()) {
                    if (cl.getClassificationScheme().equals(clm.getUuid())) {
                        isused = true;
                    }
                }
                if (!isused) {
                    this.listOptionalMetadata.add(new SelectItem(clm.getDisplayAttributeName(), clm.getDisplayAttributeName()));
                }
            }
        }

        if (ss.getExt_identifier_optional() != null) {
            for (ExternalIdentifierMetadata clm : ss.getExt_identifier_optional()) {
                boolean isused = false;
                for (ExternalIdentifierType cl : rot.getExternalIdentifier()) {
                    if (cl.getIdentificationScheme().equals(clm.getUuid())) {
                        isused = true;
                    }
                }
                if (!isused) {
                    this.listOptionalMetadata.add(new SelectItem(clm.getDisplayAttributeName(), clm.getDisplayAttributeName()));
                }
            }
        }

        if (ss.getSlot_optional() != null) {
            for (SlotMetadata slm : ss.getSlot_optional()) {
                boolean isused = false;
                for (SlotType1 sl : rot.getSlot()) {
                    if (sl.getName().equals(slm.getName())) {
                        isused = true;
                    }
                }
                if (!isused) {
                    this.listOptionalMetadata.add(new SelectItem(slm.getName(), slm.getName()));
                }
            }
        }
        this.selectedMetadata = null;
    }

    @Override
    public void initSelectedMetadata() {
        this.selectedMetadata = null;
    }

    @Override
    public void init(AffinityDomain aff, Transaction trans, RepositoryConfiguration conf) {

    }

    public abstract String getTransactionKeyword() ;

    public abstract String getAffinityDomainKeyword() ;

    public List<SelectItem> getListOptionalSlotMetadata() {
        List<SelectItem> res = new ArrayList<SelectItem>();
        res.add(new SelectItem(null, "please select.."));
        if (this.selectedClassificationType != null) {
            ClassificationMetaData clm = RIMGenerator.clWeak.get(this.selectedClassificationType);
            for (SlotMetadata slm : clm.getSlot_optional()) {
                boolean alreadyExist = false;
                for (SlotType1 sl : this.selectedClassificationType.getSlot()) {
                    if (sl.getName().equals(slm.getName())) {
                        alreadyExist = true;
                    }
                }
                if (!alreadyExist) {
                    res.add(new SelectItem(slm.getName(), slm.getName()));
                }
            }
        }
        this.selectedSlotMetadata = null;
        return res;
    }

    public void addSelectedSlotToClassification() {
        if (this.selectedClassificationType != null) {
            if (this.selectedSlotMetadata != null) {
                ClassificationMetaData clm = RIMGenerator.clWeak.get(this.selectedClassificationType);
                for (SlotMetadata slm : clm.getSlot_optional()) {
                    if (slm.getName().equals(this.selectedSlotMetadata)) {
                        SlotType1 sl = RIMGenerator.generateSlot(slm);
                        this.selectedClassificationType.getSlot().add(sl);
                        break;
                    }
                }
            }
        }
        this.selectedSlotMetadata = null;
    }

    public void deleteXDSDocument(XDSDocumentEntry xdsdoc) {
        if ((this.submissionSet != null) && (xdsdoc != null)) {
            if (this.submissionSet.getListXDSDocumentEntry().contains(xdsdoc)) {
                this.submissionSet.getListXDSDocumentEntry().remove(xdsdoc);
            }
            if (this.submissionSet.getListXDSFolder() != null) {
                for (XDSFolder xdsfolder : this.submissionSet.getListXDSFolder()) {
                    if (xdsfolder.getListXDSDocumentEntry() != null && xdsfolder.getListXDSDocumentEntry().contains(xdsdoc)) {
                        xdsfolder.getListXDSDocumentEntry().remove(xdsdoc);
                    }
                }
            }
        }
        this.editSubmissionSet = true;
        this.editXDSDocumentEntry = false;
        this.editXDSFolder = false;
    }

    public SubmitObjectsRequestType generateSubmitObjectsRequestType() throws CloneNotSupportedException {
        net.ihe.gazelle.rim.ObjectFactory ofrim = new net.ihe.gazelle.rim.ObjectFactory();
        SubmitObjectsRequestType res = new SubmitObjectsRequestType();
        res.setRegistryObjectList(new RegistryObjectListType());
        res.getRegistryObjectList().getIdentifiableGroup().add(
                ofrim.createRegistryObjectListTypeRegistryPackage(this.submissionSet.getCleanedRegistryPackageType()));
        if (this.submissionSet.getListXDSFolder() != null) {
            for (XDSFolder folder : this.submissionSet.getListXDSFolder()) {
                res.getRegistryObjectList().getIdentifiableGroup().add(
                        ofrim.createRegistryObjectListTypeRegistryPackage(folder.getCleanedRegistryPackageType(this.submissionSet.getPatientId())));

                AssociationType1 sub2folder = RIMBuilderCommon.createAssociation(ASSOCIATION_TYPE_HAS_MEMBER, this.submissionSet.getId(), folder
                        .getId(), null, null, null);
                res.getRegistryObjectList().getIdentifiableGroup().add(
                        ofrim.createRegistryObjectListTypeAssociation(sub2folder));

                if (folder.getListXDSDocumentEntry() != null) {
                    for (XDSDocumentEntry doc : folder.getListXDSDocumentEntry()) {
                        res.getRegistryObjectList().getIdentifiableGroup().add(
                                ofrim.createRegistryObjectListTypeExtrinsicObject(doc.getCleanedExtrinsicObjectType(this.submissionSet.getPatientId
                                        ())));
                        AssociationType1 sub2doc = RIMBuilderCommon.createAssociation(ASSOCIATION_TYPE_HAS_MEMBER, this.submissionSet.getId(), doc
                                .getId(), "SubmissionSetStatus", "Original", null);
                        res.getRegistryObjectList().getIdentifiableGroup().add(
                                ofrim.createRegistryObjectListTypeAssociation(sub2doc));

                        AssociationType1 folder2doc = RIMBuilderCommon.createAssociation(ASSOCIATION_TYPE_HAS_MEMBER, folder.getId(), doc.getId(),
                                null, null, null);
                        res.getRegistryObjectList().getIdentifiableGroup().add(
                                ofrim.createRegistryObjectListTypeAssociation(folder2doc));

                        AssociationType1 sub2folder2doc = RIMBuilderCommon.createAssociation(ASSOCIATION_TYPE_HAS_MEMBER, this.submissionSet.getId
                                (), folder2doc.getId(), null, null, null);
                        res.getRegistryObjectList().getIdentifiableGroup().add(
                                ofrim.createRegistryObjectListTypeAssociation(sub2folder2doc));

                        if (doc.getAssociationType() != null) {
                            AssociationType1 sub3doc = RIMBuilderCommon.createAssociation(doc.getAssociationType().getValue(), doc.getId(), doc
                                    .getTargetDocument(), null, null, null);
                            res.getRegistryObjectList().getIdentifiableGroup().add(
                                    ofrim.createRegistryObjectListTypeAssociation(sub3doc));
                        }
                    }
                }
            }
        }

        if (this.submissionSet.getListXDSDocumentEntry() != null) {
            for (XDSDocumentEntry doc : this.submissionSet.getListXDSDocumentEntry()) {

                res.getRegistryObjectList().getIdentifiableGroup().add(
                        ofrim.createRegistryObjectListTypeExtrinsicObject(doc.getCleanedExtrinsicObjectType(this.submissionSet.getPatientId())));

                AssociationType1 sub2doc = RIMBuilderCommon.createAssociation(ASSOCIATION_TYPE_HAS_MEMBER, this.submissionSet.getId(), doc.getId(),
                        "SubmissionSetStatus", "Original", null);
                res.getRegistryObjectList().getIdentifiableGroup().add(
                        ofrim.createRegistryObjectListTypeAssociation(sub2doc));
                sub2doc.getSlot().add(RIMBuilderCommon.createSlot("PreviousVersion", "1"));


                if (doc.getAssociationType() != null) {
                    AssociationType1 sub3doc = RIMBuilderCommon.createAssociation(doc.getAssociationType().getValue(), doc.getId(), doc
                            .getTargetDocument(), null, null, null);
                    res.getRegistryObjectList().getIdentifiableGroup().add(
                            ofrim.createRegistryObjectListTypeAssociation(sub3doc));
                }
            }
        }
        return res;
    }

    protected static void initVersionInfo(XDSDocumentEntry selectedXDSDocumentEntry) {
        selectedXDSDocumentEntry.setVersionInfo(new VersionInfoType());
        selectedXDSDocumentEntry.getVersionInfo().setVersionName("1");
    }

}
