package net.ihe.gazelle.xdstar.xdrsrc.tools;



import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.log.Log;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *  <b>Class Description :  </b>XDRFileToSend<br><br>
 *  XDRFileToSend
 *  
 * @class			XDRFileToSend
 * @package			net.ihe.gazelle.simulator.xdrsrc.tools
 * @author			Regis Lucas / INRIA Rennes IHE development Project
 * @see	> 			epoiseau@irisa.fr  -  http://www.ihe-europe.org
 * @version			1.0 - 2010, October 11th
 *
 */

public class XDRFileToSend {
	/**
	 * @param idDocument - the Unique Id of the document
	 * @param idPatient - Id of the patient sent by Gazelle
	 * @param fileName - Name of file to send
	 * @throws IOException
	 * @throws ParserConfigurationException 
	 * @throws SAXException 
	 * @throws TransformerException 
	 */
	
	/** Logger */
	@Logger
	private static Log log;
	
	public static void createCDAFileTosend(String idDocument,String idPatient,String fileNameTemplate,String fileName) throws IOException, ParserConfigurationException, SAXException, TransformerException
	{
		DocumentBuilderFactory documentBuilderFactory =  DocumentBuilderFactory.newInstance(); 	
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		Document documentCDA = documentBuilder.parse(new File(fileNameTemplate));
		
		documentCDA = modifyCDATemplate(documentCDA, idDocument, idPatient);
		
		
		// Write the XML file filled with the value send by gazelle
		FileOutputStream fileMetadata = new FileOutputStream(fileName);
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(documentCDA);
	    StreamResult result =  new StreamResult(fileMetadata);
	    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	    transformer.transform(source, result);
	}
	
	
	/**
	 * @param fileNametoUpload - Uploaded file name 
	 * @return the CDA Document - represent CDA file uploaded
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public Document readCDAFileUpload(String fileNametoUpload) throws ParserConfigurationException, SAXException, IOException
	{

		// Read the CDA File uploaded by user
		DocumentBuilderFactory documentBuilderFactory =  DocumentBuilderFactory.newInstance(); 	
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		Document documentCDA = documentBuilder.parse(new File(fileNametoUpload));
		
		return documentCDA;
	}
	
	
	
	/**
	 * @param documentCDA - CDA XML Document 
	 * @return PatientId PatientID read in the CDA Document
	 */
	public String getPatientIdCDAFileUpload(Document documentCDA)
	{
		String idPatient="";
		// Search the id of the cda document and replace the value
		NodeList list  = documentCDA.getElementsByTagName("patientRole");
		 if(list.getLength()>0)
		 {
			 Element element =(Element) (((Element)list.item(0)).getElementsByTagName("id").item(0));
			 idPatient = element.getAttribute("extension")+"^^^&"+element.getAttribute("root")+"&ISO";
		 }
		 
		 return idPatient;
	}
	
	
	/**
	 * @param documentCDA - CDA XML Document 
	 * @return document OID read in the CDA Document
	 */
	public String getIdDocumentCDAFileUpload(Document documentCDA)
	{
		String idDocument="";
		 // Search the id of the cda document and replace the value
		 NodeList list  = documentCDA.getElementsByTagName("id");
		 
		 if(list.getLength()>0)
		 {
			 idDocument = ((Element)list.item(0)).getAttribute("root");
		 }
		 
		 return idDocument;
	}
	
	/**
	 * @param documentCDA - CDA XML Document 
	 * @param idDocument - Document OID generate by the system or read in the CDA file uploaded
	 * @param idPatient - Patient Id filled by the user or read in the CDA file
	 * @return CDA Document
	 */
	private static Document modifyCDATemplate(Document documentCDA, String idDocument, String idPatient)
	{
		// Search the id of the cda document and replace the value
		 NodeList list  = documentCDA.getElementsByTagName("id");
		
		 for(int index=0;index<list.getLength();index++)
		 {
			if(((Element)list.item(index)).getAttribute("root").equals("idDocument"))	
				((Element)list.item(index)).setAttribute("root", idDocument);
			else if(((Element)list.item(index)).getAttribute("root").equals("idPatient"))
			{	
				((Element)list.item(index)).setAttribute("root", getRoot(idPatient));
				((Element)list.item(index)).setAttribute("extension", getExtension(idPatient));
			}	
		 }
		
		return documentCDA;
	}
	
	
	/**
	 * @param idPatient - Patient Id filled by the user or read in the CDA file
	 * @return return the value of the attribut extension 
	 */
	private static String getExtension(String idPatient)
	{
		String extension ="";
		if(idPatient.indexOf("^^^&")>0 )
			extension = idPatient.substring(0, idPatient.indexOf("^^^&"));
		return extension;
	}
	
	/**
	 * @param idPatient - Patient ID
	 * @return return the value of the attribut root 
	 */
	private static String getRoot(String idPatient)
	{
		String root ="";
		if(idPatient.indexOf("&")>0 && idPatient.lastIndexOf("&")>0)
			root = 	idPatient.substring(idPatient.indexOf("&")+1, idPatient.lastIndexOf("&"));
		return root;
	}
	
	
	
}
