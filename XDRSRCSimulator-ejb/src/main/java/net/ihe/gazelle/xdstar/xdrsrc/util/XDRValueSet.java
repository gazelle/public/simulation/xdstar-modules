package net.ihe.gazelle.xdstar.xdrsrc.util;

public enum XDRValueSet {
	XDSDocumentEntry_XDREPSOS_FORMATCODE("1.3.6.1.4.1.12559.11.4.1.2");
	
	private XDRValueSet(String value) {
		this.value = value;
	}
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
