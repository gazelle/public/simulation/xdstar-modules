package net.ihe.gazelle.xdstar.xdrsrc.ihe.model;

public enum AssociationType {
	
	REPLACE("replacement", "urn:ihe:iti:2007:AssociationType:RPLC"), TRANSFORME("transformation", "urn:ihe:iti:2007:AssociationType:XFRM"),
	APPEND("append", "urn:ihe:iti:2007:AssociationType:APND"), XFRM_RPLC("transform with replace", "urn:ihe:iti:2007:AssociationType:XFRM_RPLC"), 
	SIGNS("signs", "urn:ihe:iti:2007:AssociationType:signs");
	
	private String name;
	
	private String value;
	
	private AssociationType(String name, String value) {
		this.setValue(value);
		this.setName(name);
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
