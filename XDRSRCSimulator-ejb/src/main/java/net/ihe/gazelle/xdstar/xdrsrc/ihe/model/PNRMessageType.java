package net.ihe.gazelle.xdstar.xdrsrc.ihe.model;

public enum PNRMessageType {
	
	NEW_SUBMISSION("new documents submissions"), REPLACEMENT("replacement of document");
	
	private String value;
	
	private PNRMessageType(String value) {
		this.setValue(value);
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
