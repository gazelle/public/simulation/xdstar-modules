package net.ihe.gazelle.xdstar.xdrsrc.ihe.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.metadata.common.RIMGenerator;
import net.ihe.gazelle.metadata.model.ClassificationMetaData;
import net.ihe.gazelle.metadata.model.ExternalIdentifierMetadata;
import net.ihe.gazelle.metadata.model.RegistryObjectMetadata;
import net.ihe.gazelle.metadata.model.SlotMetadata;
import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.RegistryObjectType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.simulator.common.model.Concept;
import net.ihe.gazelle.xdstar.common.action.ValueSetContainer;
import net.ihe.gazelle.xdstar.comon.util.RIMBuilderCommon;
import net.ihe.gazelle.xdstar.comon.util.SVSConsumerForSimulator;
import net.ihe.gazelle.xdstar.comon.util.Util;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSSubmissionSet;
import net.ihe.gazelle.xdstar.xdrsrc.model.OID;

import org.jboss.seam.Component;

public class XDSCommonModule {
	
	public static final String CODING_SCHEME = "codingScheme";
	
	public static final String XDSDocumentEntry_UUID = "urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1";
	
	public static final String XDSSUBMISSIONSET_UUID = "urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd";
	
	public static final String XDSFOLDER_UUID = "urn:uuid:d9d542f3-6cc4-48b6-8870-ea235fbc94c2";
	
	public static List<SelectItem> listValueSet(ClassificationType cl){
		List<SelectItem> res = new ArrayList<SelectItem>();
		res.add(new SelectItem(null, "please select .."));
		if (cl != null){
			for (SlotType1 sl : cl.getSlot()) {
				if (sl.getName().equals(XDSCommonModule.CODING_SCHEME)){
					SlotMetadata slm = RIMGenerator.slWeak.get(sl);
					if ((slm.getValueset() != null)&& (!slm.getValueset().equals(""))){
						String[] listValueSet = slm.getValueset().split("\\s*,\\s*");
						for (String valueSet : listValueSet) {
							List<Concept> lc = ValueSetContainer.getListConcepts().get(valueSet);
							
							if (lc == null){
								lc = SVSConsumerForSimulator.getConceptsListFromValueSet(valueSet, null);
								if (lc != null){
									Collections.sort(lc);
								}
								ValueSetContainer.getListConcepts().put(valueSet, lc);
							}
							if (lc != null){
								for (Concept concept : lc) {
									SelectItem si = new SelectItem(concept.getCode(), concept.getDisplayName());
									res.add(si);
								}
							}
						}
					}
				}
			}
		}
		return res;
	}
	
	static String formatStringIfContainsXML(String string){
		if (string != null){
			String res = "";
			String soapRegex = "<\\?xml.*?Envelope>";
			String regex = "^(.*)(<\\?xml.*?Envelope>)(.*)$";
			Pattern pp = Pattern.compile(soapRegex,Pattern.MULTILINE|Pattern.DOTALL);
			Matcher mm = pp.matcher(string);
			
			if (mm.find()){
				String ss = mm.group();
				int ind = string.indexOf(ss);
				String toModify = Util.prettyFormat(ss);
				String begin = string.substring(0, ind);
				String end = string.substring(ind + ss.length());
				res = begin + toModify + end;
			}
			else{
				res = string;
			}
			
			return res;
		}
		return null;
	}
	
	public static String rownSpanClass(ClassificationType cl){
		int i = 2;
		if (cl != null){
			SlotType1 codingScheme = getCodingSchemeSlot(cl);
			if (codingScheme != null){
				SlotMetadata slm = RIMGenerator.slWeak.get(codingScheme);
				if ((slm.getValueset() != null) && (!slm.getValueset().equals("")) ){
					i = 0;
				}
			}
			i = i + cl.getSlot().size();
		}
		if (hasOptionalSlotYet(cl)) {
			i = i + 1;
		}
		return String.valueOf(i);
	}
	
	public static boolean hasOptionalSlotYet(ClassificationType cl){
		ClassificationMetaData clm = RIMGenerator.clWeak.get(cl);
		EntityManager em = (EntityManager)Component.getInstance("entityManager");
		clm = em.find(ClassificationMetaData.class, clm.getId());
		if (clm != null){
			if ((clm.getSlot_optional() != null) && (clm.getSlot_optional().size()>0)){
				for (SlotMetadata slm : clm.getSlot_optional()) {
					boolean present = false;
					for (SlotType1 sl : cl.getSlot()) {
						if (slm.getName().equals(sl.getName())){
							present = true;
							break;
						}
					}
					if (!present) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	private static SlotType1 getCodingSchemeSlot(ClassificationType cl){
		if (cl != null && cl.getSlot() != null){
			for (SlotType1 sl : cl.getSlot()) {
				if (sl.getName().equals(XDSCommonModule.CODING_SCHEME)){
					return sl;
				}
			}
		}
		return null;
	}
	
	static boolean isValueSet(ClassificationType cl){
		boolean res = false;
		if (cl != null){
			if (cl.getSlot() != null){
				for (SlotType1 sl : cl.getSlot()) {
					if (sl.getName().equals(XDSCommonModule.CODING_SCHEME)){
						SlotMetadata slm = RIMGenerator.slWeak.get(sl);
						if ((slm.getValueset() != null) && (!slm.getValueset().equals(""))){
							return true;
						}
					}
				}
			}
		}
		return res;	
			
	}
	
	static ArrayList<SelectItem> initListOptionalMetadata(RegistryObjectType rot, RegistryObjectMetadata ss, XDSCommonEditor comEdit){
		EntityManager em = (EntityManager) Component.getInstance("entityManager");
		ArrayList<SelectItem> listOptionalMetadata = new ArrayList<SelectItem>();
		listOptionalMetadata.add(new SelectItem(null, "please select.."));
		
		ss = em.find(RegistryObjectMetadata.class, ss.getId());
		if (ss.getClassification_optional() != null){
			for (ClassificationMetaData clm : ss.getClassification_optional()) {
				boolean isused = false;
				for (ClassificationType cl : rot.getClassification()) {
					if (cl.getClassificationScheme().equals(clm.getUuid())) {
						isused = true;
					}
				}
				if (!isused){
					listOptionalMetadata.add(new SelectItem(clm.getDisplayAttributeName(), clm.getDisplayAttributeName()));
				}
			}
		}
		
		if (ss.getExt_identifier_optional() != null){
			for (ExternalIdentifierMetadata clm : ss.getExt_identifier_optional()) {
				boolean isused = false;
				for (ExternalIdentifierType cl : rot.getExternalIdentifier()) {
					if (cl.getIdentificationScheme().equals(clm.getUuid())) {
						isused = true;
					}
				}
				if (!isused){
					listOptionalMetadata.add(new SelectItem(clm.getDisplayAttributeName(), clm.getDisplayAttributeName()));
				}
			}
		}
		
		if (ss.getSlot_optional() != null){
			for (SlotMetadata slm : ss.getSlot_optional()) {
				boolean isused = false;
				for (SlotType1 sl : rot.getSlot()) {
					if (sl.getName().equals(slm.getName())) {
						isused = true;
					}
				}
				if (!isused){
					listOptionalMetadata.add(new SelectItem(slm.getName(), slm.getName()));
				}
			}
		}
		comEdit.initSelectedMetadata();
		return listOptionalMetadata;
	}
	
	public static void addNewMetadataToRegistryObjectType(RegistryObjectType rot, String selectedMetadata, XDSCommonEditor comEdit){
		HQLQueryBuilder<ClassificationMetaData> hh = new HQLQueryBuilder<ClassificationMetaData>(ClassificationMetaData.class);
		hh.addEq("displayAttributeName", selectedMetadata);
		List<ClassificationMetaData> dd = hh.getList();
		if ((dd != null) && (dd.size()>0)){
			ClassificationType cl = RIMGenerator.generateClassificationType(dd.get(0));
			rot.getClassification().add(cl);
		}
		
		HQLQueryBuilder<ExternalIdentifierMetadata> gg = new HQLQueryBuilder<ExternalIdentifierMetadata>(ExternalIdentifierMetadata.class);
		gg.addEq("displayAttributeName", selectedMetadata);
		List<ExternalIdentifierMetadata> dda = gg.getList();
		if ((dda != null) && (dda.size()>0)){
			ExternalIdentifierType cl = RIMGenerator.generateExternalIdentifierType(dda.get(0));
			rot.getExternalIdentifier().add(cl);
		}
		
		HQLQueryBuilder<SlotMetadata> qq = new HQLQueryBuilder<SlotMetadata>(SlotMetadata.class);
		qq.addEq("name", selectedMetadata);
		List<SlotMetadata> qqs = qq.getList();
		if ((qqs != null) && (qqs.size()>0)){
			SlotType1 sl = RIMGenerator.generateSlot(qqs.get(0));
			rot.getSlot().add(sl);
		}
		comEdit.initListOptionalMetadata();
		comEdit.initSelectedMetadata();
	}
	
	static void updateSourceIdAndUniqueId(RegistryObjectType ro){
		updateSourceId(ro);
		updateUniqueId(ro);
	}
	
	private static void updateSourceId(RegistryObjectType ro){
		if (ro != null){
			for (ExternalIdentifierType ext : ro.getExternalIdentifier()) {
				ExternalIdentifierMetadata extm = RIMGenerator.extWeak.get(ext);
				if (extm!=null && extm.getDisplayAttributeName()!= null){
					if (extm.getDisplayAttributeName().contains(".sourceId")){
						ext.setValue(OID.getIHESourceOid());
						return;
					}
				}
			}
		}
	}
	
	public static void updateUniqueId(RegistryObjectType ro){
		if (ro != null){
			for (ExternalIdentifierType ext : ro.getExternalIdentifier()) {
				ExternalIdentifierMetadata extm = RIMGenerator.extWeak.get(ext);
				if (extm.getDisplayAttributeName().contains(".uniqueId")){
					if (ro instanceof XDSSubmissionSet){
						ext.setValue(OID.getNewSubmissionSetOid());
					}
					else{
						ext.setValue(OID.getNewDocumentOid());
					}
					return;
				}
			}
		}
	}
	
	public static void updateClassificationByValueSet(ClassificationType cl, String value){
		if ((cl != null)){
			
			ClassificationMetaData clm = RIMGenerator.clWeak.get(cl);
			SlotMetadata slmc = null;
			if (clm.getSlot_required() != null){
				for (SlotMetadata slm : clm.getSlot_required()) {
					if (slm.getName().equals(XDSCommonModule.CODING_SCHEME)){
						slmc = slm;
						break;
					}
				}
			}
			
			if (value == null){
				for (SlotType1 sl : cl.getSlot()) {
					if (sl.getName().equals(XDSCommonModule.CODING_SCHEME)){
						sl.getValueList().setValue(new ArrayList<String>());
					}
				}
				return;
			}
			
			if (slmc != null){
				String valueSets = slmc.getValueset();
				if ((valueSets != null) && (!valueSets.equals(""))){
					String[] listValueSet =valueSets.split("\\s*,\\s*");
					for (String valueSet : listValueSet) {
						Concept cp = SVSConsumerForSimulator.getConceptForCode(valueSet, null, value);
						if (cp != null){
							cl.setName(RIMBuilderCommon.createNameOrDescription(cp.getDisplayName()));
							cl.setNodeRepresentation(cp.getCode());
							for (SlotType1 sl : cl.getSlot()) {
								if (sl.getName().equals(XDSCommonModule.CODING_SCHEME)){
									sl.getValueList().setValue(new ArrayList<String>());
									sl.getValueList().getValue().add(cp.getCodeSystem());
								}
							}
						}
					}
				}
			}
		}
	}

}
