package net.ihe.gazelle.xdstar.xdrsrc.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@Entity
@Name("metadata")
@Table(name="metadata", schema = "public", uniqueConstraints=@UniqueConstraint(columnNames="id"))
@SequenceGenerator(name="metadata_sequence", sequenceName="metadata_id_seq", allocationSize=1)
public class Metadata implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String EXTERNAL_IDENTIFIER = "External Identifier";
	public static final String CLASSIFICATION = "External Classification Scheme";
	public static final String CLASSIFICATION_NODE = "ClassificationNode";
	public static final String SLOT = "Slot";
	public static final String XDSSUBMISSIONSET = "XDSSubmissionSet";
	public static final String XDSDOCUMENTENTRY = "XDSDocumentEntry";
	public static final String XDSFOLDER = "XDSFolder";
	
	@Logger
	private static Log log;
	
	@Id
	@GeneratedValue(generator="metadata_sequence", strategy=GenerationType.SEQUENCE)
	@NotNull
	@Column(name="id", nullable=false, unique=true)
	private Integer id;
	
	@Column(name="uuid")
	private String uuid;
	
	@Column(name="name")
	private String name;
	
	@Column(name="type")
	private String type;
	
	@Column(name="required")
	private Boolean required;
	
	@Column(name="generated")
	private Boolean generated;
	
	
	public Metadata(){
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public Boolean getRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	public Boolean getGenerated() {
		return generated;
	}

	public void setGenerated(Boolean generated) {
		this.generated = generated;
	}

	/**
	 * 
	 * @param inName
	 * @return
	 */
	public static Metadata getMetadataByName(String inName)
	{
		if (inName != null && !inName.isEmpty())
		{
			EntityManager em = (EntityManager) Component.getInstance("entityManager");
			Session session = (Session) em.getDelegate();
			Criteria c = session.createCriteria(Metadata.class);
			c.add(Restrictions.like("name", inName));
			List<Metadata> results = c.list();
			if (results == null || results.isEmpty())
				return null;
			else
				return results.get(0);
		}
		else
			return null;
	}
	
	/**
	 * 
	 * @param objectType
	 * @return
	 */
	public static List<Metadata> getMetadataByObjectType(String objectType)
	{
		if (objectType != null && !objectType.isEmpty())
		{
			EntityManager em = (EntityManager) Component.getInstance("entityManager");
			Session session = (Session) em.getDelegate();
			Criteria c = session.createCriteria(Metadata.class);
			c.add(Restrictions.like("name", objectType.concat(".%")));
			List<Metadata> results = c.list();
			if (results == null || results.isEmpty())
				return null;
			else
				return results;
		}
		else
			return null;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((generated == null) ? 0 : generated.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((required == null) ? 0 : required.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Metadata other = (Metadata) obj;
		if (generated == null) {
			if (other.generated != null)
				return false;
		} else if (!generated.equals(other.generated))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (required == null) {
			if (other.required != null)
				return false;
		} else if (!required.equals(other.required))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Metadata [id=" + id + ", uuid=" + uuid + ", name=" + name
				+ ", type=" + type + ", required=" + required + ", generated="
				+ generated + "]";
	}
	

}
