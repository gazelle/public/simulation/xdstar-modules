package net.ihe.gazelle.xdstar.epsos2.action;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class FindCDAKind {
	
	
	private static Logger log = LoggerFactory.getLogger(FindCDAKind.class);
	
	private FindCDAKind(){}
	
	public static enum CDAKind{
		CONSENT, CONSENTSD, PS, eP, eD, HCER, MRO
	}
	
	public static CDAKind lookupCDAKind(InputStream is){
		NodeList nodl = evaluate(is, "/cda:ClinicalDocument/cda:templateId");
		if (nodl != null){
			for(int i=0; i<nodl.getLength(); i++){
				Node aa = nodl.item(i);
				String root= ((Element)aa).getAttribute("root");
				if (root.equals("1.3.6.1.4.1.12559.11.10.1.3.1.1.4")){
					return CDAKind.HCER;
				}
				else if (root.equals("1.3.6.1.4.1.12559.11.10.1.3.1.1.5")){
					return CDAKind.MRO;
				}
				else if (root.equals("1.3.6.1.4.1.12559.11.10.1.3.1.1.3")){
					return CDAKind.PS;
				}
				else if (root.equals("1.3.6.1.4.1.12559.11.10.1.3.1.1.1")){
					return CDAKind.eP;
				}
				else if (root.equals("1.3.6.1.4.1.12559.11.10.1.3.1.1.2")){
					return CDAKind.eD;
				}
				else if (root.equals("1.3.6.1.4.1.19376.1.5.3.1.1.7")){
					return CDAKind.CONSENT;
				}
				else if (root.equals("1.3.6.1.4.1.19376.1.5.3.1.1.7.1")){
					return CDAKind.CONSENTSD;
				}
			}
		}
		return null;
	}
	
	public static boolean isNonStructuredCDA(InputStream is){
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			IOUtils.copy(is, baos);
			if (baos.toString().contains("structuredBody")) {
				return false;
			}
		} catch (IOException e) {
		}
//		NodeList nodl = evaluate(is, "/cda:ClinicalDocument/cda:component/cda:structuredBody");
//		if (nodl.getLength() >0) {
//			res = false;
//		}
		return true;
	}

	public static NodeList evaluate(InputStream stream, String expression){
		List liste = null;
		try{
			InputSource source = new InputSource(stream);
			XPathFactory fabrique = XPathFactory.newInstance();
			XPath xpath = fabrique.newXPath();
			xpath.setNamespaceContext(new DatatypesNamespaceContext());
			XPathExpression exp = xpath.compile(expression);
			liste = (List)exp.evaluate(source,XPathConstants.NODESET);
		}catch(XPathExpressionException xpee){
			log.info("the tool not able to process the xpath expression on the document specified.");
		}
		return null;
	}

	public static void main(String[] args) throws FileNotFoundException {
		NodeList nodl = evaluate(new FileInputStream("/home/aboufahj/Téléchargements/epSOS_RTD_PS_MT_Pivot_CDA_L3_005.xml"), 
				"/cda:ClinicalDocument/cda:author/cda:assignedAuthor/cda:representedOrganization/cda:addr/cda:country | /cda:ClinicalDocument/cda:documentationOf/cda:serviceEvent/cda:performer/cda:assignedEntity/cda:representedOrganization/cda:addr/cda:country");
		if (nodl != null){
			for(int i=0; i<nodl.getLength(); i++){
				Node aa = nodl.item(i);
				System.out.println("'" + aa.getTextContent());
			}
		}
	}

}
