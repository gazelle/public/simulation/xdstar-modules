package net.ihe.gazelle.xdstar.xdrsrc.ihe.action;

import java.util.ArrayList;
import java.util.UUID;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.metadata.common.RIMGenerator;
import net.ihe.gazelle.metadata.model.*;
import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSFolder;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSSubmissionSet;

public class XDSFolderManagement {
	
	private XDSFolderManagement(){}
	
	public static void addNewFolderToXDSSubmissionSet(XDSSubmissionSet xx, XDSFolderEditor folderEdit,
			AffinityDomain selecAffinityDomain, Transaction selectedTransaction){
		if (xx != null){
			if (xx.getListXDSFolder() == null){
				xx.setListXDSFolder(new ArrayList<XDSFolder>());
			}
			XDSFolder selectedXDSFolder = generateXDSFolder(folderEdit, selecAffinityDomain, selectedTransaction);
			folderEdit.editXDSFolder(selectedXDSFolder);
			xx.getListXDSFolder().add(selectedXDSFolder);
		}
	}
	
	public static XDSFolder generateXDSFolder(XDSFolderEditor folderEdit, AffinityDomain selecAffinityDomain, Transaction selectedTransaction){
		XDSFolder res = new XDSFolder();
		RegistryPackageMetadataQuery hh = new RegistryPackageMetadataQuery();
		hh.uuid().eq(XDSCommonModule.XDSFOLDER_UUID);
		hh.usages().affinity().keyword().eq(selecAffinityDomain.getKeyword());
		if (selectedTransaction != null) {
			hh.usages().transaction().keyword().eq(selectedTransaction.getKeyword());
		}
		RegistryPackageMetadata rpm = hh.getList().get(0); 
		RIMGenerator.generateRegistryPackageTypeChild(rpm, res);
		res.setId("urn:uuid:" + UUID.randomUUID());
		res.setTitle(res.getTitle() + " " + folderEdit.getFolderIndex());
		folderEdit.setFolderIndex(folderEdit.getFolderIndex() + 1);
		XDSCommonModule.updateSourceIdAndUniqueId(res);
		return res;
	}
	
	public static String getOptionalityClassOnSelectedXDSFolder(XDSFolder selectedXDSFolder, ClassificationType ext){
		RegistryPackageMetadata rpm = RIMGenerator.rpWeak.get(selectedXDSFolder);
		ClassificationMetaData extm = RIMGenerator.clWeak.get(ext);
		for (ClassificationMetaData ee : rpm.getClassification_required()) {
			if (extm.getUuid().equals(ee.getUuid())) {
				return "R";
			}
		}
		return "O";
	}
	
	public static void removeClassificationFromSelectedXDSFolder(XDSFolder selectedXDSFolder, ClassificationType cl, XDSFolderEditor folderEdit){
		selectedXDSFolder.getClassification().remove(cl);
		folderEdit.initListOptionalMetadata();
	}
	
	public static void removeSlotFromSelectedXDSFolder(XDSFolder selectedXDSFolder, SlotType1 sl, XDSFolderEditor folderEdit){
		selectedXDSFolder.getSlot().remove(sl);
		folderEdit.initListOptionalMetadata();
	}
	
	public static String getOptionalitySlotOnSelectedXDSFolder(XDSFolder selectedXDSFolder, SlotType1 sl){
		RegistryPackageMetadata rpm = RIMGenerator.rpWeak.get(selectedXDSFolder);
		SlotMetadata slm = RIMGenerator.slWeak.get(sl);
		for (SlotMetadata ee : rpm.getSlot_required()) {
			if (slm.getName().equals(ee.getName())) {
				return "R";
			}
		}
		return "O";
	}
	
	public static void removeExtFromSelectedXDSFolder(XDSFolder selectedXDSFolder, ExternalIdentifierType ext, XDSFolderEditor folderEdit){
		selectedXDSFolder.getExternalIdentifier().remove(ext);
		folderEdit.initListOptionalMetadata();
	}
	
	public static String getOptionalityExtOnSelectedXDSFolder(XDSFolder selectedXDSFolder, ExternalIdentifierType ext){
		RegistryPackageMetadata rpm = RIMGenerator.rpWeak.get(selectedXDSFolder);
		ExternalIdentifierMetadata extm = RIMGenerator.extWeak.get(ext);
		for (ExternalIdentifierMetadata ee : rpm.getExt_identifier_required()) {
			if (extm.getUuid().equals(ee.getUuid())) {
				return "R";
			}
		}
		return "O";
	}

}
