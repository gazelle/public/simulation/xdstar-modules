package net.ihe.gazelle.xdstar.xdrsrc.ihe.action;

import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.lcm.SubmitObjectsRequestType;
import net.ihe.gazelle.metadata.common.RIMGenerator;
import net.ihe.gazelle.metadata.model.ExternalIdentifierMetadata;
import net.ihe.gazelle.metadata.model.RegistryPackageMetadata;
import net.ihe.gazelle.metadata.model.RegistryPackageMetadataQuery;
import net.ihe.gazelle.rim.*;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.samlassertion.common.SignatureException;
import net.ihe.gazelle.simulator.samlassertion.epr.EPRAssertionAttributes;
import net.ihe.gazelle.xdstar.common.model.AbstractMessage;
import net.ihe.gazelle.xdstar.common.model.ContentType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import net.ihe.gazelle.xdstar.comon.util.ConfigurationFilter;
import net.ihe.gazelle.xdstar.comon.util.RIMBuilderCommon;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespacePrefixMapper;
import net.ihe.gazelle.xdstar.conf.model.UpdateConfiguration;
import net.ihe.gazelle.xdstar.conf.model.UpdateConfigurationQuery;
import net.ihe.gazelle.xdstar.core.MessageSenderCommon;
import net.ihe.gazelle.xdstar.core.soap.SOAPRequestBuilderRMU;
import net.ihe.gazelle.xdstar.core.XUAType;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSDocumentEntry;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSFolder;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSSubmissionSet;
import net.ihe.gazelle.xdstar.xdrsrc.model.OID;
import net.ihe.gazelle.xdstar.xdrsrc.model.RMUMessage;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Name("rmu")
@Scope(ScopeType.PAGE)
public class RMUManager extends XDSMetadataEditor<UpdateConfiguration, RMUMessage> {

    /**
     *
     */
    private String homeCommunityId;

    private String subjectNameId;

    private EPRAssertionAttributes eprAssertionAttributes;

    private String resourceId;

    private String organizationId;

    private XUAType xuaType = XUAType.NONE;

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(RMUManager.class);

    protected static final String XDSSUBMISSIONSET_UUID = "urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd";

    protected GazelleTreeNodeImpl<Object> treeNodeSubmission;

    protected InternationalStringType selectedInternationalStringType;

    private static final String ASSOCIATION_TYPE_HAS_MEMBER = "urn:oasis:names:tc:ebxml-regrep:AssociationType:HasMember";


    /**
     *
     */

    public String getHomeCommunityId() {
        return homeCommunityId;
    }

    public void setHomeCommunityId(String homeCommunityId) {
        this.homeCommunityId = homeCommunityId;
    }

    public String getSubjectNameId() {
        return subjectNameId;
    }

    public void setSubjectNameId(String subjectNameId) {
        this.subjectNameId = subjectNameId;
    }

    public EPRAssertionAttributes getEprAssertionAttributes() {
        return eprAssertionAttributes;
    }

    public void setEprAssertionAttributes(EPRAssertionAttributes eprAssertionAttributes) {
        this.eprAssertionAttributes = eprAssertionAttributes;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getXuaType() {
        return xuaType.getXuaType();
    }

    public void setXuaType(String xuaType) {
        this.xuaType.setXuaType(xuaType);
    }

    public InternationalStringType getSelectedInternationalStringType() {
        return selectedInternationalStringType;
    }

    public List<SelectItem> getListOptionalMetadata() {
        return listOptionalMetadata;
    }

    public void setListOptionalMetadata(List<SelectItem> listOptionalMetadata) {
        this.listOptionalMetadata = listOptionalMetadata;
    }


    /**
     *
     */

    @Create
    @Override
    public void initCreation() {

        super.initCreation();

        this.submissionSet = this.generateXDSSubmissionSet();
        this.message = new RMUMessage();
        this.displayResultPanel = false;
        this.editXDSSubmissionSet();
        this.initListOptionalMetadata();
    }

    public void init(AffinityDomain aff, Transaction trans, UpdateConfiguration conf) {
        initEPRAttributes();

        //useXUA = false;

        this.submissionSet = this.generateXDSSubmissionSet();
        this.selectedConfiguration = conf;
        this.selectedAffinityDomain = aff;
        this.selectedTransaction = trans;
        this.editXDSSubmissionSet();
        this.initListOptionalMetadata();
        this.homeCommunityId = this.selectedConfiguration.getHomeCommunityId();
    }

    public void initEPRAttributes() {
        eprAssertionAttributes = new EPRAssertionAttributes();
    }

    public void addValueToDelegate() {
        eprAssertionAttributes.setResourceId(resourceId);
        eprAssertionAttributes.setOrganizationId(organizationId);
    }

    protected XDSSubmissionSet generateXDSSubmissionSet() {
        XDSSubmissionSet res = new XDSSubmissionSet();
        RegistryPackageMetadataQuery query = new RegistryPackageMetadataQuery();
        query.uuid().eq(XDSSUBMISSIONSET_UUID);
        query.usages().affinity().eq(this.selectedAffinityDomain);
        query.usages().transaction().eq(this.selectedTransaction);
        RegistryPackageMetadata rpm = query.getUniqueResult();
        RIMGenerator.generateRegistryPackageTypeChild(rpm, res);
        res.setId("urn:uuid:" + UUID.randomUUID());
        this.updateSourceIdAndUniqueId(res);
        return res;
    }

    private void updateSourceIdAndUniqueId(RegistryObjectType ro) {
        if (ro != null) {
            for (ExternalIdentifierType ext : ro.getExternalIdentifier()) {
                ExternalIdentifierMetadata extm = RIMGenerator.extWeak.get(ext);
                if (extm.getDisplayAttributeName().contains(".sourceId")) {
                    ext.setValue(OID.getIHESourceOid());
                }
                if (extm.getDisplayAttributeName().contains(".uniqueId")) {
                    if (ro instanceof XDSSubmissionSet) {
                        ext.setValue(OID.getNewSubmissionSetOid());
                    } else {
                        ext.setValue(OID.getNewDocumentOid());
                    }
                }
            }
        }
    }

    public boolean canAddName(InternationalStringType name) {
        return !(name != null && (name.getLocalizedString() != null) && (name.getLocalizedString().size() > 0));
    }

    public List<SelectItem> listValueSet(ClassificationType cl) {
        return XDSCommonModule.listValueSet(cl);
    }

    public void initSelectedInternationalStringType(ClassificationType cl) {
        if (cl.getName() == null) {
            cl.setName(new InternationalStringType());
        }
        this.selectedInternationalStringType = cl.getName();
        this.valueToAdd = null;
    }

    public GazelleTreeNodeImpl<Object> getTreeNodeSubmission() {
        return XDSSubmissionSetManagement.getTreeNodeSubmissionSet(this.submissionSet);
    }

    public void setTreeNodeSubmission(GazelleTreeNodeImpl<Object> treeNodeSubmission) {
        this.treeNodeSubmission = treeNodeSubmission;
    }

    public boolean hasOptionalSlotYet(ClassificationType cl) {
        return XDSCommonModule.hasOptionalSlotYet(cl);
    }

    public boolean isValueSet(ClassificationType cl) {
        return XDSCommonModule.isValueSet(cl);
    }

    public void updateClassificationByValueSet(ClassificationType cl) {
        XDSCommonModule.updateClassificationByValueSet(cl, cl.getNodeRepresentation());
    }

    private RegistryPackageType createRMURegistryPackageForSubmissionSet(String homeCommunityId) throws CloneNotSupportedException {
        RegistryPackageType registryPackageType = this.submissionSet.getCleanedRegistryPackageType();
        if (homeCommunityId != null && !homeCommunityId.isEmpty()) {
            registryPackageType.setHome(homeCommunityId);
        }
        return registryPackageType;
    }

    private RegistryPackageType createRMURegistryPackageForFolder(String homeCommunityId, XDSFolder folder) {
        RegistryPackageType registryPackageType = folder.getCleanedRegistryPackageType(this.submissionSet.getPatientId());
        if (homeCommunityId != null && !homeCommunityId.isEmpty()) {
            registryPackageType.setHome(homeCommunityId);
        }
        return registryPackageType;
    }

    private ExtrinsicObjectType createRMUExtrinsicObject(String homeCommunityId, XDSDocumentEntry documentEntry) {
        ExtrinsicObjectType extrinsicObjectType = documentEntry.getCleanedExtrinsicObjectType(this.submissionSet.getPatientId());
        if (homeCommunityId != null && !homeCommunityId.isEmpty()) {
            extrinsicObjectType.setHome(homeCommunityId);
        }
        return extrinsicObjectType;
    }

    private SubmitObjectsRequestType generateRMUSubmitObjectsRequestType() throws CloneNotSupportedException {
        net.ihe.gazelle.rim.ObjectFactory ofrim = new net.ihe.gazelle.rim.ObjectFactory();
        SubmitObjectsRequestType res = new SubmitObjectsRequestType();
        res.setRegistryObjectList(new RegistryObjectListType());
        res.getRegistryObjectList().getIdentifiableGroup().add(
                ofrim.createRegistryObjectListTypeRegistryPackage(createRMURegistryPackageForSubmissionSet(homeCommunityId)));
        if (this.submissionSet.getListXDSFolder() != null) {
            for (XDSFolder folder : this.submissionSet.getListXDSFolder()) {
                res.getRegistryObjectList().getIdentifiableGroup().add(
                        ofrim.createRegistryObjectListTypeRegistryPackage(createRMURegistryPackageForFolder(homeCommunityId, folder)));

                AssociationType1 sub2folder = RIMBuilderCommon.createAssociation(ASSOCIATION_TYPE_HAS_MEMBER, this.submissionSet.getId(), folder
                        .getId(), null, null, null);
                res.getRegistryObjectList().getIdentifiableGroup().add(
                        ofrim.createRegistryObjectListTypeAssociation(sub2folder));

                if (folder.getListXDSDocumentEntry() != null) {
                    for (XDSDocumentEntry doc : folder.getListXDSDocumentEntry()) {
                        res.getRegistryObjectList().getIdentifiableGroup().add(
                                ofrim.createRegistryObjectListTypeExtrinsicObject(createRMUExtrinsicObject(homeCommunityId, doc)));
                        AssociationType1 sub2doc = RIMBuilderCommon.createAssociation(ASSOCIATION_TYPE_HAS_MEMBER, this.submissionSet.getId(), doc
                                .getId(), "SubmissionSetStatus", "Original", null);
                        res.getRegistryObjectList().getIdentifiableGroup().add(
                                ofrim.createRegistryObjectListTypeAssociation(sub2doc));

                        AssociationType1 folder2doc = RIMBuilderCommon.createAssociation(ASSOCIATION_TYPE_HAS_MEMBER, folder.getId(), doc.getId(),
                                null, null, null);
                        res.getRegistryObjectList().getIdentifiableGroup().add(
                                ofrim.createRegistryObjectListTypeAssociation(folder2doc));

                        AssociationType1 sub2folder2doc = RIMBuilderCommon.createAssociation(ASSOCIATION_TYPE_HAS_MEMBER, this.submissionSet.getId
                                (), folder2doc.getId(), null, null, null);
                        res.getRegistryObjectList().getIdentifiableGroup().add(
                                ofrim.createRegistryObjectListTypeAssociation(sub2folder2doc));

                        if (doc.getAssociationType() != null) {
                            AssociationType1 sub3doc = RIMBuilderCommon.createAssociation(doc.getAssociationType().getValue(), doc.getId(), doc
                                    .getTargetDocument(), null, null, null);
                            res.getRegistryObjectList().getIdentifiableGroup().add(
                                    ofrim.createRegistryObjectListTypeAssociation(sub3doc));
                        }
                    }
                }
            }
        }

        if (this.submissionSet.getListXDSDocumentEntry() != null) {
            for (XDSDocumentEntry doc : this.submissionSet.getListXDSDocumentEntry()) {

                res.getRegistryObjectList().getIdentifiableGroup().add(
                        ofrim.createRegistryObjectListTypeExtrinsicObject(createRMUExtrinsicObject(homeCommunityId, doc)));

                AssociationType1 sub2doc = RIMBuilderCommon.createAssociation(ASSOCIATION_TYPE_HAS_MEMBER, this.submissionSet.getId(), doc.getId(),
                        "SubmissionSetStatus", "Original", null);
                res.getRegistryObjectList().getIdentifiableGroup().add(
                        ofrim.createRegistryObjectListTypeAssociation(sub2doc));
                sub2doc.getSlot().add(RIMBuilderCommon.createSlot("PreviousVersion", "1"));

                if (doc.getAssociationType() != null) {
                    AssociationType1 sub3doc = RIMBuilderCommon.createAssociation(doc.getAssociationType().getValue(), doc.getId(), doc
                            .getTargetDocument(), null, null, null);
                    res.getRegistryObjectList().getIdentifiableGroup().add(
                            ofrim.createRegistryObjectListTypeAssociation(sub3doc));
                }
            }
        }
        return res;
    }

    private SlotType1 createHomeCommunityIdSlot() {
        return RIMBuilderCommon.createSlot("homeCommunityId", "urn:oid:"+homeCommunityId);
    }

    private String createMessage() throws CloneNotSupportedException, JAXBException, ParserConfigurationException, net.ihe.gazelle.simulator.samlassertion.common.SignatureException {

        SubmitObjectsRequestType sor = this.generateRMUSubmitObjectsRequestType();
            JAXBContext jc = JAXBContext.newInstance(SubmitObjectsRequestType.class);
            Marshaller m = jc.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new XDSNamespacePrefixMapper());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            m.marshal(sor, baos);
            String res = baos.toString();
            String receiverUrl = this.selectedConfiguration.getUrl();
        String soapAction = "urn:ihe:iti:2018:RestrictedUpdateDocumentSet";
        SOAPRequestBuilderRMU soapRequestBuilderRMU = new SOAPRequestBuilderRMU();
        this.addValueToDelegate();
        res = soapRequestBuilderRMU.createSOAPMessage(res, xuaType, subjectNameId, eprAssertionAttributes, receiverUrl, soapAction, homeCommunityId);
            return res;
    }

    public void sendRMU() {
        message = new RMUMessage();
        message.setMessageType("RMU");
        message.setTransaction(selectedTransaction);
        message.setAffinityDomain(selectedAffinityDomain);
        message.setContentType(ContentType.SOAP_MESSAGE);
        message.setTimeStamp(new Date());
        message.setGazelleDriven(false);
        message.setConfiguration(selectedConfiguration);

        MetadataMessage sent = new MetadataMessage();
        try {
            String messageToSend = this.createMessage();
            sent.setMessageContent(messageToSend);
            sent.setMessageType(MetadataMessageType.SUBMIT_OBJECT_REQUEST);
            message.setSentMessageContent(sent);
            message = AbstractMessage.storeMessage(message);
        }catch (CloneNotSupportedException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error while creating XML SubmitObjectsRequest : "+ e.getMessage());
            LOG.warn("Error while creating XML SubmitObjectsRequest : "+ e.getMessage());
        }catch (JAXBException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error while creating XML SubmitObjectsRequest : "+ e.getMessage());
            LOG.warn("Error while creating XML SubmitObjectsRequest : "+ e.getMessage());
        }catch (ParserConfigurationException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error while creating Soap Request :"+ e.getMessage());
            LOG.warn("Error while creating Soap Request : "+ e.getMessage());
        }catch (SignatureException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error while signing the assertion : "+ e.getMessage());
            LOG.warn("Error while signing the assertion : "+ e.getMessage());
        }

        MessageSenderCommon sender = new MessageSenderCommon(selectedConfiguration.getUrl(), message);
        try {
            sender.sendMessageSOAP(MetadataMessageType.REGIQTRY_RESPONSE);
            message = (RMUMessage) sender.getRequest();
            message = AbstractMessage.storeMessage(message);
        } catch (Exception e) {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.RMU.errorWhenSendingMessage", e.getMessage
                    ());
        }
        Contexts.getSessionContext().set("messagesToDisplay", message);
        displayResultPanel = true;
    }

    public String previewMessage() {
        try {
            return this.createMessage();
        }catch (CloneNotSupportedException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error while creating XML SubmitObjectsRequest : "+ e.getMessage());
            LOG.warn("Error while creating XML SubmitObjectsRequest : "+ e.getMessage());
        }catch (JAXBException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error while creating XML SubmitObjectsRequest : "+ e.getMessage());
            LOG.warn("Error while creating XML SubmitObjectsRequest : "+ e.getMessage());
        }catch (ParserConfigurationException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error while creating Soap Request : "+ e.getMessage());
            LOG.warn("Error while creating Soap Request : "+ e.getMessage());
        }catch (SignatureException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error while signing the assertion : "+ e.getMessage());
            LOG.warn("Error while signing the assertion : "+ e.getMessage());
        }
        return null;
    }

    public String getDisplayNameExt(ExternalIdentifierType ext) {
        return RIMGenerator.extWeak.get(ext).getDisplayAttributeName();
    }

    public String rownSpanClass(ClassificationType cl) {
        return XDSCommonModule.rownSpanClass(cl);
    }

    public String getDisplayNameClass(ClassificationType ext) {
        return RIMGenerator.clWeak.get(ext).getDisplayAttributeName();
    }

    @Override
    public String getTransactionKeyword() {
        return "ITI-92";
    }

    @Override
    public String getAffinityDomainKeyword() {
        return "IHE_RMU";
    }

    @Override
    public void listAllConfigurations() {
        UpdateConfigurationQuery rc = new UpdateConfigurationQuery();
        if (selectedAffinityDomain != null) {
            rc.listUsages().affinity().eq(selectedAffinityDomain);
        }
        if (selectedTransaction != null) {
            rc.listUsages().transaction().eq(selectedTransaction);
        }
        rc.name().order(true);
        ConfigurationFilter.filterConfigurationQueryAccodingToUserCredential(rc);
        configurations = rc.getList();
    }

    @Override
    public void updateSpecificClient() {
        this.initCreation();
    }

    @Override
    public List<AffinityDomain> listAllAffinityDomains() {
        return null;
    }

    @Override
    public String getTheNameOfThePage() {
        return "Restricted Update Document Set (ITI-92)";
    }

    public boolean isXuaTypeEPR() {
        if (xuaType.getXuaType().equals("epr_assertion")) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isXuaTypeEpSOS() {
        if (xuaType.getXuaType().equals("epSOS_assertion")) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isXuaTypeIHE() {
        if (xuaType.getXuaType().equals("ihe_assertion")) {
            return true;
        } else {
            return false;
        }
    }
}
