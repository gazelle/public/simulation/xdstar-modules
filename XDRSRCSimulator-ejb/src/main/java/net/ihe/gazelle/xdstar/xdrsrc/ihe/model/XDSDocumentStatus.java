package net.ihe.gazelle.xdstar.xdrsrc.ihe.model;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.List;

public enum XDSDocumentStatus {

    SUBMITTED("urn:oasis:names:tc:ebxml-regrep:StatusType:Submitted"),
    APPROVED("urn:oasis:names:tc:ebxml-regrep:StatusType:Approved"),
    DEPRECATED("urn:oasis:names:tc:ebxml-regrep:StatusType:Deprecated");

    private String urn;

    XDSDocumentStatus(String urn){
        this.urn = urn;
    }

    public String getUrn() {
        return urn;
    }
}
