package net.ihe.gazelle.xdstar.xdrsrc.model;

import java.io.Serializable;
import java.util.List;

import net.ihe.gazelle.rim.SlotType1;

public class MetadataValue implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Metadata metadata;
	private String value;
	private List<SlotType1> slots;
	private String localizedString;
	
	public MetadataValue()
	{
		
	}
	
	public MetadataValue(Metadata metadata, String value, List<SlotType1> slots)
	{
		this.metadata = metadata;
		this.value = value;
		this.slots = slots;
	}

	public Metadata getMetadata() {
		return metadata;
	}

	public void setMetadata(Metadata metadata) {
		this.metadata = metadata;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setSlots(List<SlotType1> slots) {
		this.slots = slots;
	}

	public List<SlotType1> getSlots() {
		return slots;
	}

	public void setLocalizedString(String localizedString) {
		this.localizedString = localizedString;
	}

	public String getLocalizedString() {
		return localizedString;
	}

	public static MetadataValue getMetadataValueByMetadataName(List<MetadataValue> valueList, String metadataName)
	{
		if (valueList != null && !valueList.isEmpty() && metadataName != null)
		{
			Metadata metadata = Metadata.getMetadataByName(metadataName);
			MetadataValue selectedMetadataValue = null;
			if (metadata != null)
			{
				for (MetadataValue mv : valueList)
				{
					if (mv.metadata.equals(metadata))
					{
						selectedMetadataValue = mv;
						break;
					}
					else
						continue;
				}
				return selectedMetadataValue;
			}
			else
				return null;
		}
		else
			return null;
	}

	@Override
	public String toString() {
		return "MetadataValue [metadata=" + metadata + ", value=" + value
				+ ", slots=" + slots + ", localizedString=" + localizedString
				+ "]";
	}
	
}
