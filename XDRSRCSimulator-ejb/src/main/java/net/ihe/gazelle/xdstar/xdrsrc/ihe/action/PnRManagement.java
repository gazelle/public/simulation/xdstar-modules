package net.ihe.gazelle.xdstar.xdrsrc.ihe.action;

import java.io.ByteArrayOutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import net.ihe.gazelle.xds.ProvideAndRegisterDocumentSetRequestType;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespacePrefixMapper;

public class PnRManagement {
	
	private PnRManagement(){}
	
	public static String getProvideAndRegisterDocumentSetRequestTypeAsString(ProvideAndRegisterDocumentSetRequestType pnr){
		try{
			JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.xds");
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new XDSNamespacePrefixMapper());
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			m.marshal(pnr, baos);
			String res = baos.toString();
			res = patchXop(res);
			return res;
		}
		catch(Exception e){
			return null;
		}
	}
	
	static String patchXop(String to){
		String res = to;
		Pattern pat = Pattern.compile("<([^(:<>)]*?:?)Document(.*?)>(\\s)*?<([^(:<>)]*?:?)Include(.*?)>(\\s)*?</([^(:<>)]*?:?)Document>");
		Matcher mat = pat.matcher(to);
		while (mat.find()){
			res = res.replace(mat.group(), "<" + mat.group(1)  + "Document" + mat.group(2) + "><" + mat.group(4) + "Include" + mat.group(5) + "></" + mat.group(7) + "Document>");
		}
		return res;
	}

}
