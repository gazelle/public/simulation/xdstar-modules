package net.ihe.gazelle.xdstar.epsos2.action;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.xdstar.comon.util.ConfigurationFilter;
import net.ihe.gazelle.xdstar.comon.util.EVSClientValidator;
import net.ihe.gazelle.xdstar.comon.util.Util;
import net.ihe.gazelle.xdstar.conf.model.RepositoryConfigurationQuery;
import net.ihe.gazelle.xdstar.epsos2.action.FindCDAKind.CDAKind;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.action.DocumentEntryManagement;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.action.PnRIHEManager;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.action.XDSCommonModule;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.action.XDSSubmissionSetManagement;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.PNRMessageType;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSDocumentEntry;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSFolder;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSSubmissionSet;
import net.ihe.gazelle.xdstar.xdrsrc.model.XDRMessage;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@Name("ePSOS2XDRManager")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = 10000)
public class EPSOS2XDRManager extends PnRIHEManager implements Serializable {
	
	
	private static Logger log = LoggerFactory.getLogger(EPSOS2XDRManager.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String validationResult;
	
	private String selectedValidator;
	
	public String getSelectedValidator() {
		return selectedValidator;
	}

	public void setSelectedValidator(String selectedValidator) {
		this.selectedValidator = selectedValidator;
	}

	public String getValidationResult() {
		return validationResult;
	}

	public void setValidationResult(String validationResult) {
		this.validationResult = validationResult;
	}

	@Override
	public String getTheNameOfThePage() {
		String res = this.getNameOfTheAffinityDomainTransaction();
		if (res == null || res.equals("")) {
			res = "epSOS-2 - ProvideDataService";
		}
		return res;
	}
	
	public void init(){
		super.init();
		this.selectedTransaction = Transaction.GetTransactionByKeyword("ProvideDataService");
		this.selectedAffinityDomain = AffinityDomain.getAffinityDomainByKeyword("epSOS-2");
		this.initFromMenu = true;
		this.submissionSet = this.generateXDSSubmissionSet();
		this.selectedXDSDocumentEntry = null;
		this.selectedXDSFolder = null;
		this.selectedXDSFolder = null;
		setMessage(new XDRMessage());
		this.selectedMessageType = PNRMessageType.NEW_SUBMISSION;
		this.editXDSSubmissionSet();
		this.initListOptionalMetadata();
	}
	
	@Override
	protected XDSSubmissionSet generateXDSSubmissionSet(){
		return XDSSubmissionSetManagement.generateXDSSubmissionSet(this.selectedAffinityDomain, this.selectedTransaction);
	}
	
	public void addDocumentEntry(XDSSubmissionSet xx){
		DocumentEntryManagement.addNewDocumentEntryToXDSSubmissionSet(this.submissionSet, this, 
				this.selectedAffinityDomain, this.selectedTransaction);
	}
	
	public void addDocumentEntry(XDSFolder xx){
		DocumentEntryManagement.addNewDocumentEntryToXDSFolder(this.selectedXDSFolder, this, 
				this.selectedAffinityDomain, this.selectedTransaction);
	}
	
	@Override
	public void listAllConfigurations() {
		RepositoryConfigurationQuery rc = new RepositoryConfigurationQuery();
		if (selectedAffinityDomain != null) {
			rc.listUsages().affinity().eq(selectedAffinityDomain);
		}
		if (selectedTransaction != null) {
			rc.listUsages().transaction().eq(selectedTransaction);
		}
		rc.name().order(true);
		ConfigurationFilter.filterConfigurationQueryAccodingToUserCredential(rc);
		configurations = rc.getList();
	}

	@Override
	public void updateSpecificClient() {
		// Not applicable
	}

	@Override
	public List<AffinityDomain> listAllAffinityDomains() {
		List<AffinityDomain> laff = new ArrayList<AffinityDomain>();
		laff.add(AffinityDomain.getAffinityDomainByKeyword("epSOS-2"));
		return laff;
	}
	
	@Override
	public void uploadListener(org.richfaces.event.FileUploadEvent event) {
		super.uploadListener(event);
		try {
			CDAKind kind = FindCDAKind.lookupCDAKind(new FileInputStream(this.selectedXDSDocumentEntry.getRelatedFile().getPath()));
			if (kind != null){
				this.updateFormatCode(this.selectedXDSDocumentEntry, kind);
				this.updateClassCode(this.selectedXDSDocumentEntry, kind);
				this.updateTypeCode(this.selectedXDSDocumentEntry, kind);
			}
			this.updateConfidentialityCode(this.selectedXDSDocumentEntry);
			this.updateLanguageCode(this.selectedXDSDocumentEntry);
			this.updatePracticeSettingCode(this.selectedXDSDocumentEntry);
			this.updateHealthcareFacilityTypeCode(this.selectedXDSDocumentEntry);
			this.updateCreationTime(this.selectedXDSDocumentEntry);
			this.updateDocumentUniqueId(this.selectedXDSDocumentEntry);
		} catch (FileNotFoundException e) {
			log.info("The file uploaded was not saved properly.");
		}
		
	}
	
	private void updateFormatCode(XDSDocumentEntry docEntry, CDAKind kind){
		for (ClassificationType cl : docEntry.getClassification()) {
			if (cl.getClassificationScheme() != null && cl.getClassificationScheme().equals("urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d")){
				try{
					String value = null;
					switch (kind) {
						case CONSENT:
							value = "urn:ihe:iti:bppc:2007";
							break;
						case CONSENTSD:
							value = "urn:ihe:iti:bppc-sd:2007";
							break;
						case eD:
							value = "urn:epSOS:ep:dis:2010";
							if (FindCDAKind.isNonStructuredCDA(new FileInputStream(docEntry.getRelatedFile().getPath()))){
								value = "urn:ihe:iti:xds-sd:pdf:2008";
							}
							break;
						case eP:
							value = "urn:epSOS:ep:pre:2010";
							if (FindCDAKind.isNonStructuredCDA(new FileInputStream(docEntry.getRelatedFile().getPath()))){
								value = "urn:ihe:iti:xds-sd:pdf:2008";
							}
							break;
						case PS:
							value = "urn:epSOS:ps:ps:2010";
							break;
						case HCER:
							value = "urn:epSOS:hcer:hcer:2013";
							break;
						case MRO:
							value = "urn:epSOS:mro:mro:2013";
							break;
						default:
							break;
					}
					XDSCommonModule.updateClassificationByValueSet(cl, value);
				}
				catch (FileNotFoundException e) {
					log.info("The file uploaded was not saved properly.");
				}
				return;
			}
		}
	}
	
	private void updateClassCode(XDSDocumentEntry docEntry, CDAKind kind){
		for (ClassificationType cl : docEntry.getClassification()) {
			if (cl.getClassificationScheme() != null && cl.getClassificationScheme().equals("urn:uuid:41a5887f-8865-4c09-adf7-e362475b143a")){
				String value = null;
				switch (kind) {
					case CONSENT:
						value = "57016-8";
						break;
					case CONSENTSD:
						value = "57016-8";
						break;
					case eD:
						value = "60593-1";
						break;
					case eP:
						value = "57833-6";
						break;
					case PS:
						value = "60591-5";
						break;
					case HCER:
						value = "34133-9";
						break;
					case MRO:
						value = "56445-0";
						break;
					default:
						break;
				}
				XDSCommonModule.updateClassificationByValueSet(cl, value);
				return;
			}
		}
	}
	
	private void updateTypeCode(XDSDocumentEntry docEntry, CDAKind kind){
		for (ClassificationType cl : docEntry.getClassification()) {
			if (cl.getClassificationScheme() != null && cl.getClassificationScheme().equals("urn:uuid:f0306f51-975f-434e-a61c-c59651d33983")){
				String value = null;
				switch (kind) {
					case CONSENT:
						value = "57016-8";
						break;
					case CONSENTSD:
						value = "57016-8";
						break;
					case eD:
						value = "60593-1";
						break;
					case eP:
						value = "57833-6";
						break;
					case PS:
						value = "60591-5";
						break;
					case HCER:
						value = "34133-9";
						break;
					case MRO:
						value = "56445-0";
						break;
					default:
						break;
				}
				XDSCommonModule.updateClassificationByValueSet(cl, value);
				return;
			}
		}
	}
	
	private void updateHealthcareFacilityTypeCode(XDSDocumentEntry docEntry){
		try {
			String country = "";
			NodeList nodl = FindCDAKind.evaluate(new FileInputStream(docEntry.getRelatedFile().getPath()), 
				"/cda:ClinicalDocument/cda:author/cda:assignedAuthor/cda:representedOrganization/cda:addr/cda:country | " +
				"/cda:ClinicalDocument/cda:documentationOf/cda:serviceEvent/cda:performer/cda:assignedEntity/cda:representedOrganization/cda:addr/cda:country");
			if (nodl != null){
				for(int i=0; i<nodl.getLength(); i++){
					Node aa = nodl.item(i);
					if (aa.getTextContent() != null && !aa.getTextContent().trim().equals("")){
						country = aa.getTextContent();
					}
				}
			}
			if (!country.equals("")){
				for (ClassificationType cl : docEntry.getClassification()) {
					if (cl.getClassificationScheme() != null && cl.getClassificationScheme().equals("urn:uuid:f33fb8ac-18af-42cc-ae0e-ed0b0bdb91e1")){
						cl.setNodeRepresentation(country);
						XDSCommonModule.updateClassificationByValueSet(cl, cl.getNodeRepresentation());
					}
				}
			}
		} catch (FileNotFoundException e) {}
	}
	
	private void updateConfidentialityCode(XDSDocumentEntry docEntry){
		for (ClassificationType cl : docEntry.getClassification()) {
			if (cl.getClassificationScheme() != null && cl.getClassificationScheme().equals("urn:uuid:f4f85eac-e6cb-4883-b524-f2705394840f")){
				cl.setNodeRepresentation("N");
				XDSCommonModule.updateClassificationByValueSet(cl, cl.getNodeRepresentation());
				return;
			}
		}
	}
	
	private void updatePracticeSettingCode(XDSDocumentEntry docEntry){
		for (ClassificationType cl : docEntry.getClassification()) {
			if (cl.getClassificationScheme() != null && cl.getClassificationScheme().equals("urn:uuid:cccf5598-8b07-4b77-a05e-ae952c785ead")){
				cl.setNodeRepresentation("Not Used");
				XDSCommonModule.updateClassificationByValueSet(cl, cl.getNodeRepresentation());
				return;
			}
		}
	}
	
	private void updateLanguageCode(XDSDocumentEntry docEntry){
		try {
			String languageCode = "";
			NodeList nodl = FindCDAKind.evaluate(new FileInputStream(docEntry.getRelatedFile().getPath()), "/cda:ClinicalDocument/cda:languageCode/@code");
			if (nodl != null){
				for(int i=0; i<nodl.getLength(); i++){
					Node aa = nodl.item(i);
					if (aa.getTextContent() != null && !aa.getTextContent().trim().equals("")){
						languageCode = aa.getTextContent();
					}
				}
			}
			if (!languageCode.equals("")){
				for (SlotType1 slot : docEntry.getSlot()) {
					if (slot.getName().equals("languageCode")){
						slot.getValueList().setValue(new ArrayList<String>());
						slot.getValueList().getValue().add(languageCode);
					}
				}
			}
		} catch (FileNotFoundException e) {}
	}
	
	private void updateCreationTime(XDSDocumentEntry docEntry){
		try {
			String creationTime = "";
			NodeList nodl = FindCDAKind.evaluate(new FileInputStream(docEntry.getRelatedFile().getPath()), "/cda:ClinicalDocument/cda:effectiveTime/@value");
			if (nodl != null){
				for(int i=0; i<nodl.getLength(); i++){
					Node aa = nodl.item(i);
					if (aa.getTextContent() != null && !aa.getTextContent().trim().equals("")){
						creationTime = aa.getTextContent();
					}
				}
			}
			if (!creationTime.equals("")){
				if (creationTime.contains(".")){
					creationTime = creationTime.substring(0, creationTime.indexOf("."));
				}
				if (creationTime.contains("+")){
					creationTime = creationTime.substring(0, creationTime.indexOf("+"));
				}
				if (creationTime.contains("-")){
					creationTime = creationTime.substring(0, creationTime.indexOf("-"));
				}
				for (SlotType1 slot : docEntry.getSlot()) {
					if (slot.getName().equals("creationTime")){
						slot.getValueList().setValue(new ArrayList<String>());
						slot.getValueList().getValue().add(creationTime);
					}
				}
			}
		} catch (FileNotFoundException e) {}
	}
	
	private void updateDocumentUniqueId(XDSDocumentEntry docEntry){
		try{
			String uniqueId = "";
			NodeList nodl = FindCDAKind.evaluate(new FileInputStream(docEntry.getRelatedFile().getPath()), "/cda:ClinicalDocument/cda:id");
			if (nodl != null){
				for(int i=0; i<nodl.getLength(); i++){
					Node aa = nodl.item(i);
					Element el = (Element)aa;
					if (el.hasAttribute("root")){
						uniqueId = el.getAttribute("root");
						if (el.hasAttribute("extension")){
							uniqueId = uniqueId + "^" + StringUtils.substring(el.getAttribute("extension"), 0, 16);
						}
					}
					if (!uniqueId.equals("")){
						break;
					}
				}
			}
			if (!uniqueId.equals("")){
				for (ExternalIdentifierType ext : docEntry.getExternalIdentifier()) {
					if ((ext.getIdentificationScheme() != null) && (ext.getIdentificationScheme().equals("urn:uuid:2e82c1f6-a085-4c72-9da3-8640a32e42ab"))){
						ext.setValue(uniqueId);
					}
				}
			}
		}
		catch(Exception e){}
	}
	
	public void regenerateDocumentUniqueId(XDSDocumentEntry docEntry){
		XDSCommonModule.updateUniqueId(docEntry);
	}
	
	public void fulfilWithEDisp(XDSDocumentEntry docEntry){
		CDAKind kind = FindCDAKind.CDAKind.eD;
		this.updateFormatCode(this.selectedXDSDocumentEntry, kind);
		this.updateClassCode(this.selectedXDSDocumentEntry, kind);
		this.updateTypeCode(this.selectedXDSDocumentEntry, kind);
	}
	
	public void fulfilWithEConsentSD(XDSDocumentEntry docEntry){
		CDAKind kind = FindCDAKind.CDAKind.CONSENTSD;
		this.updateFormatCode(this.selectedXDSDocumentEntry, kind);
		this.updateClassCode(this.selectedXDSDocumentEntry, kind);
		this.updateTypeCode(this.selectedXDSDocumentEntry, kind);
	}
	
	public void fulfilWithEConsent(XDSDocumentEntry docEntry){
		CDAKind kind = FindCDAKind.CDAKind.CONSENT;
		this.updateFormatCode(this.selectedXDSDocumentEntry, kind);
		this.updateClassCode(this.selectedXDSDocumentEntry, kind);
		this.updateTypeCode(this.selectedXDSDocumentEntry, kind);
	}
	
	public void fulfilWithHCER(XDSDocumentEntry docEntry){
		CDAKind kind = FindCDAKind.CDAKind.HCER;
		this.updateFormatCode(this.selectedXDSDocumentEntry, kind);
		this.updateClassCode(this.selectedXDSDocumentEntry, kind);
		this.updateTypeCode(this.selectedXDSDocumentEntry, kind);
	}
	
	public void validateCurrentCDAFile(){
		this.validationResult = "";
		try{
			CDAKind kind = FindCDAKind.lookupCDAKind(new FileInputStream(this.selectedXDSDocumentEntry.getRelatedFile().getPath()));
			if (kind != null){
				this.selectedValidator = "";
				switch (kind) {
				case CONSENT:
				case CONSENTSD:
					this.selectedValidator = "epSOS - eConsent";
					break;
				case eD:
					this.selectedValidator = "epSOS - eDispensation Pivot";
					break;
				case eP:
					this.selectedValidator = "epSOS - ePrescription Pivot";
					break;
				case PS:
					this.selectedValidator = "epSOS - Patient Summary Pivot";
					break;
				case HCER:
					this.selectedValidator = "epSOS - HCER HealthCare Encounter Report";
					break;
				case MRO:
					this.selectedValidator = "epSOS - MRO Medication Related Overview";
					break;
				default:
					break;
				}
			}
		}
		catch(Exception e){
			log.info("not able to find the document on the server");
		}
		this.validateCurrentCDAFileAccordingToSelectedValidator();
	}
	
	public void validateCurrentCDAFileAccordingToSelectedValidator(){
		if (this.selectedValidator != null && !this.selectedValidator.equals("")){
			String doc = Base64.encodeFromFile(this.selectedXDSDocumentEntry.getRelatedFile().getPath());
			String endpoint = ApplicationConfiguration.getValueOfVariable("cda_mbv_wsdl");
			this.validationResult = EVSClientValidator.validate(doc, this.selectedValidator, true, endpoint);
			this.validationResult = Util.transformXmlToHtml(this.validationResult, ApplicationConfiguration.getValueOfVariable("cda_mbv_xslt"));
		}
	}
	
	public List<String> getListCDAEpsosValidators(){
		List<String> res = new ArrayList<String>();
		res.add("epSOS - eConsent");
		res.add("epSOS - eDispensation Pivot");
		res.add("epSOS - ePrescription Pivot");
		res.add("epSOS - Patient Summary Pivot");
		res.add("epSOS - HCER HealthCare Encounter Report");
		res.add("epSOS - MRO Medication Related Overview");
		return res;
	}
	
	
}
