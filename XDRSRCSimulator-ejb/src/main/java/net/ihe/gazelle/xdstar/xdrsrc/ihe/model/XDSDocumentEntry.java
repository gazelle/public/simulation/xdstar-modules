package net.ihe.gazelle.xdstar.xdrsrc.ihe.model;

import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.ExtrinsicObjectType;
import net.ihe.gazelle.xdstar.comon.util.RIMBuilderCommon;
import net.ihe.gazelle.xdstar.xdrsrc.model.PnRRegisteredFile;

import com.rits.cloning.Cloner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class XDSDocumentEntry extends ExtrinsicObjectType{
	
	public XDSDocumentEntry(){
		this.setStatus(XDSDocumentStatus.APPROVED.getUrn());
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String title;
	
	private PnRRegisteredFile relatedFile;
	
	private AssociationType associationType;
	
	private String targetDocument;
	
	private int index = 0;
	
	public AssociationType getAssociationType() {
		return associationType;
	}

	public void setAssociationType(AssociationType associationType) {
		this.associationType = associationType;
	}

	public String getTargetDocument() {
		return targetDocument;
	}

	public void setTargetDocument(String targetDocument) {
		this.targetDocument = targetDocument;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public PnRRegisteredFile getRelatedFile() {
		return relatedFile;
	}

	public void setRelatedFile(PnRRegisteredFile relatedFile) {
		this.relatedFile = relatedFile;
	}

	public String getTitle(){
		if (this.getName() != null){
			if ((this.getName().getLocalizedString() != null) && (this.getName().getLocalizedString().size()>0)){
				return this.getName().getLocalizedString().get(0).getValue();
			}
		}
		return null;
	}

	public void setTitle(String title) {
		this.title = title;
		this.setName(RIMBuilderCommon.createNameOrDescription(this.title));
	}
	
	public ExtrinsicObjectType getCleanedExtrinsicObjectType(String patientId){
		Cloner cloner = new Cloner();
		ExtrinsicObjectType res = cloner.deepClone(this);
		
//		res.getSlot().add(RIMBuilderCommon.createSlot("creationTime", Util.getDate()));
		res.getSlot().add(RIMBuilderCommon.createSlot("sourcePatientId", patientId));
		
		//if (this.relatedFile != null) res.setMimeType(this.relatedFile.getType());
		
		ExternalIdentifierType ext_patientId = RIMBuilderCommon.createExternalIdentifierType("XDSDocumentEntry.patientId", 
				"urn:uuid:58a6f841-87b3-4a3e-92fd-a8ffeff98427",
				this.getId(), patientId);
		res.getExternalIdentifier().add(ext_patientId);
		
		for (ClassificationType cll : res.getClassification()) {
			cll.setClassifiedObject(res.getId());
		}
		
		for (ExternalIdentifierType ext : res.getExternalIdentifier()) {
			ext.setRegistryObject(res.getId());
		}
		return res;
	}

	public List<String> getAvailableXDSStatus() {
		List<String> availableStatus = new ArrayList<>();
		for(XDSDocumentStatus status : XDSDocumentStatus.values()){
			availableStatus.add(status.getUrn());
		}
		return availableStatus ;
	}
	
}
