package net.ihe.gazelle.xdstar.xdrsrc.ihe.action;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.metadata.common.RIMGenerator;
import net.ihe.gazelle.metadata.model.*;
import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSDocumentEntry;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSFolder;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.XDSSubmissionSet;

import org.jboss.seam.Component;

public final class DocumentEntryManagement {
	
	private DocumentEntryManagement(){}
	
	public static void removeClassificationFromSelectedXDSDocumentEntry(
			XDSDocumentEntry selectedXDSDocumentEntry, ClassificationType cl,
			XDSDocEditor xdsdoc) {
		selectedXDSDocumentEntry.getClassification().remove(cl);
		xdsdoc.initListOptionalMetadata();
	}
	
	public static String getOptionalityClassOnSelectedXDSDocumentEntry(XDSDocumentEntry selectedXDSDocumentEntry, ClassificationType ext){
		ExtrinsicObjectMetadata rpm = RIMGenerator.docWeak.get(selectedXDSDocumentEntry);
		ClassificationMetaData extm = RIMGenerator.clWeak.get(ext);
		if (rpm.getClassification_required() != null){
			for (ClassificationMetaData ee : rpm.getClassification_required()) {
				if (extm.getUuid().equals(ee.getUuid())) {
					return "R";
				}
			}
		}
		return "O";
	}
	
	public static void removeSlotFromSelectedXDSDocumentEntry(XDSDocumentEntry selectedXDSDocumentEntry, SlotType1 sl, XDSDocEditor xdsdoc){
		selectedXDSDocumentEntry.getSlot().remove(sl);
		xdsdoc.initListOptionalMetadata();
	}
	
	public static String getOptionalitySlotOnSelectedXDSDocumentEntry(XDSDocumentEntry selectedXDSDocumentEntry, SlotType1 sl){
		EntityManager em = (EntityManager)Component.getInstance("entityManager");
		ExtrinsicObjectMetadata extrm = RIMGenerator.docWeak.get(selectedXDSDocumentEntry);
		SlotMetadata slm = RIMGenerator.slWeak.get(sl);
		extrm = em.find(ExtrinsicObjectMetadata.class, extrm.getId());
		if (extrm.getSlot_required()!= null){
			for (SlotMetadata ee : extrm.getSlot_required()) {
				if (slm.getName().equals(ee.getName())) {
					return "R";
				}
			}
		}
		return "O";
	}
	
	public static void removeExtFromSelectedXDSDocumentEntry(XDSDocumentEntry selectedXDSDocumentEntry, 
			ExternalIdentifierType ext, XDSDocEditor xdsdoc){
		selectedXDSDocumentEntry.getExternalIdentifier().remove(ext);
		xdsdoc.initListOptionalMetadata();
	}
	
	public static String getOptionalityExtOnSelectedXDSDocumentEntry(XDSDocumentEntry selectedXDSDocumentEntry, ExternalIdentifierType ext){
		ExtrinsicObjectMetadata rpm = RIMGenerator.docWeak.get(selectedXDSDocumentEntry);
		ExternalIdentifierMetadata extm = RIMGenerator.extWeak.get(ext);
		for (ExternalIdentifierMetadata ee : rpm.getExt_identifier_required()) {
			if (extm.getUuid().equals(ee.getUuid())) {
				return "R";
			}
		}
		return "O";
	}
	
	public static void randomFulFilDocEntryMetadata(XDSDocumentEntry selectedXDSDocumentEntry){
		if (selectedXDSDocumentEntry != null){
			for (ClassificationType clas : selectedXDSDocumentEntry.getClassification()) {
				List<SelectItem> lsi = XDSCommonModule.listValueSet(clas);
				if ((lsi != null) && lsi.size()>1){
					SelectItem si = lsi.get((int)Math.floor(Math.random()*(lsi.size()-2)) + 1);
					clas.setNodeRepresentation((String) si.getValue());
					XDSCommonModule.updateClassificationByValueSet(clas, clas.getNodeRepresentation());
				}
			}
		}
	}
	
	public static void addNewDocumentEntryToXDSSubmissionSet(XDSSubmissionSet xdsSubmissionSet, XDSDocEditor xdsEditor,
			AffinityDomain selectedAffinityDomain, Transaction selectedTransaction){
		if (xdsSubmissionSet != null){
			if (xdsSubmissionSet.getListXDSDocumentEntry() == null){
				xdsSubmissionSet.setListXDSDocumentEntry(new ArrayList<XDSDocumentEntry>());
			}
			XDSDocumentEntry selectedXDSDocumentEntry = generateXDSDocumentEntry(xdsEditor, selectedAffinityDomain, selectedTransaction);
			xdsEditor.editXDSDocumentEntry(selectedXDSDocumentEntry);
			xdsSubmissionSet.getListXDSDocumentEntry().add(selectedXDSDocumentEntry);
		}
	}
	
	public static void addNewDocumentEntryToXDSFolder(XDSFolder xdsFolder, XDSDocEditor xdsEditor,
			AffinityDomain selectedAffinityDomain, Transaction selectedTransaction){
		if (xdsFolder != null){
			if (xdsFolder.getListXDSDocumentEntry() == null){
				xdsFolder.setListXDSDocumentEntry(new ArrayList<XDSDocumentEntry>());
			}
			XDSDocumentEntry selectedXDSDocumentEntry = generateXDSDocumentEntry(xdsEditor, selectedAffinityDomain, selectedTransaction);
			xdsEditor.editXDSDocumentEntry(selectedXDSDocumentEntry);
			xdsFolder.getListXDSDocumentEntry().add(selectedXDSDocumentEntry);
		}
	}
	
	public static XDSDocumentEntry generateXDSDocumentEntry(XDSDocEditor xdsEditor, 
			AffinityDomain selectedAffinityDomain, Transaction selectedTransaction){
		XDSDocumentEntry res = new XDSDocumentEntry();
		ExtrinsicObjectMetadataQuery hh = new ExtrinsicObjectMetadataQuery();
		hh.uuid().eq(XDSCommonModule.XDSDocumentEntry_UUID);
		hh.usages().affinity().keyword().eq(selectedAffinityDomain.getKeyword());
		if (selectedTransaction != null) {
			hh.usages().transaction().keyword().eq(selectedTransaction.getKeyword());
		}
		ExtrinsicObjectMetadata rpm = hh.getUniqueResult();
		RIMGenerator.generateExtrinsicObjectTypeChild(rpm, res);
		res.setId("urn:uuid:" + UUID.randomUUID());
		XDSCommonModule.updateSourceIdAndUniqueId(res);
		res.setIndex(xdsEditor.getFileIndex());
		res.setTitle(res.getTitle() + " " + xdsEditor.getFileIndex());
		xdsEditor.setFileIndex(xdsEditor.getFileIndex()+1);
		return res;
	}
	
}
