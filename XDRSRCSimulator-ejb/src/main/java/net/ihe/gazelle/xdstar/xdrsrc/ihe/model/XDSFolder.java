package net.ihe.gazelle.xdstar.xdrsrc.ihe.model;

import java.util.List;
import java.util.UUID;

import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.RegistryPackageType;
import net.ihe.gazelle.xdstar.comon.util.RIMBuilderCommon;
import net.ihe.gazelle.xdstar.comon.util.Util;

import com.rits.cloning.Cloner;

public class XDSFolder extends RegistryPackageType{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public XDSFolder(){
		this.setStatus("urn:oasis:names:tc:ebxml-regrep:StatusType:Approved");
	}
	
	private List<XDSDocumentEntry> listXDSDocumentEntry;
	
	private String title;
	
	public String getTitle(){
		if (this.getName() != null){
			if ((this.getName().getLocalizedString() != null) && (this.getName().getLocalizedString().size()>0)){
				return this.getName().getLocalizedString().get(0).getValue();
			}
		}
		return null;
	}

	public void setTitle(String title) {
		this.title = title;
		this.setName(RIMBuilderCommon.createNameOrDescription(this.title));
	}
	
	public List<XDSDocumentEntry> getListXDSDocumentEntry() {
		return listXDSDocumentEntry;
	}

	public void setListXDSDocumentEntry(List<XDSDocumentEntry> listXDSDocumentEntry) {
		this.listXDSDocumentEntry = listXDSDocumentEntry;
	}
	
	public RegistryPackageType getCleanedRegistryPackageType(String patientId){
		Cloner cloner = new Cloner();
		RegistryPackageType res = cloner.deepClone(this);
		
		ClassificationType cl = new ClassificationType();
		cl.setClassificationNode("urn:uuid:d9d542f3-6cc4-48b6-8870-ea235fbc94c2");
		cl.setClassifiedObject(res.getId());
		cl.setId("urn:uuid:" + UUID.randomUUID().toString());
		cl.setObjectType("urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification");
		res.getClassification().add(cl);
		
		ExternalIdentifierType ext_patientId = RIMBuilderCommon.createExternalIdentifierType("XDSFolder.patientId", 
				"urn:uuid:f64ffdf0-4b97-4e06-b79f-a52b38ec2f8a",
				this.getId(), patientId);
		res.getExternalIdentifier().add(ext_patientId);
		
		res.getSlot().add(RIMBuilderCommon.createSlot("lastUpdateTime", Util.getDate()));
		
		for (ClassificationType cll : res.getClassification()) {
			cll.setClassifiedObject(res.getId());
		}
		
		for (ExternalIdentifierType ext : res.getExternalIdentifier()) {
			ext.setRegistryObject(res.getId());
		}
		
		return res;
	}
	
}
