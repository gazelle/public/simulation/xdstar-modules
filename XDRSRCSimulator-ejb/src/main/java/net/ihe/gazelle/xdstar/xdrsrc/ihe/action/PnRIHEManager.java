package net.ihe.gazelle.xdstar.xdrsrc.ihe.action;

import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.lcm.SubmitObjectsRequestType;
import net.ihe.gazelle.metadata.common.RIMGenerator;
import net.ihe.gazelle.metadata.model.ClassificationMetaData;
import net.ihe.gazelle.metadata.model.ExternalIdentifierMetadata;
import net.ihe.gazelle.metadata.model.RegistryObjectMetadata;
import net.ihe.gazelle.metadata.model.SlotMetadata;
import net.ihe.gazelle.rim.*;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.common.xua.SamlAssertionAttributes;
import net.ihe.gazelle.simulator.common.xua.SignatureException;
import net.ihe.gazelle.xds.DocumentType;
import net.ihe.gazelle.xds.ObjectFactory;
import net.ihe.gazelle.xds.ProvideAndRegisterDocumentSetRequestType;
import net.ihe.gazelle.xdstar.common.action.CommonSimulatorManager;
import net.ihe.gazelle.xdstar.common.model.AttachmentFile;
import net.ihe.gazelle.xdstar.common.model.ContentType;
import net.ihe.gazelle.xdstar.common.model.MetadataMessage;
import net.ihe.gazelle.xdstar.common.model.MetadataMessageType;
import net.ihe.gazelle.xdstar.comon.util.RIMBuilderCommon;
import net.ihe.gazelle.xdstar.conf.model.RepositoryConfiguration;
import net.ihe.gazelle.xdstar.core.AttachmentsUtil;
import net.ihe.gazelle.xdstar.core.MessageSenderCommon;
import net.ihe.gazelle.xdstar.core.soap.SOAPRequestBuilder;
import net.ihe.gazelle.xdstar.xdrsrc.ihe.model.*;
import net.ihe.gazelle.xdstar.xdrsrc.model.PnRRegisteredFile;
import net.ihe.gazelle.xdstar.xdrsrc.model.XDRMessage;
import net.ihe.gazelle.xdstar.xdrsrc.tools.MessageSender;
import net.ihe.gazelle.xdstar.xdrsrc.tools.Pair;
import net.ihe.gazelle.xop.Include;
import org.apache.axiom.attachments.Attachments;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Name("pnrIHEManager")
@Scope(ScopeType.PAGE)
public class PnRIHEManager extends
        CommonSimulatorManager<RepositoryConfiguration, XDRMessage> implements
        Serializable, XDSDocEditor, XDSFolderEditor, XDSSubmissionSetEditor {

    private static final String ASSOCIATION_TYPE_HAS_MEMBER = "urn:oasis:names:tc:ebxml-regrep:AssociationType:HasMember";

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static Logger log = LoggerFactory.getLogger(PnRIHEManager.class);

    protected XDSSubmissionSet submissionSet;

    protected XDSFolder selectedXDSFolder;

    protected XDSDocumentEntry selectedXDSDocumentEntry;

    protected PNRMessageType selectedMessageType;

    protected GazelleTreeNodeImpl<Object> treeNodeSubmission;

    protected Boolean adviseNodeOpened = true;

    protected Boolean editSubmissionSet;

    protected Boolean editXDSDocumentEntry;

    protected Boolean editXDSFolder;

    protected SlotType1 selectedSlotType;

    protected String valueToAdd;

    protected InternationalStringType selectedInternationalStringType;

    protected List<SelectItem> listOptionalMetadata = new ArrayList<SelectItem>();

    protected String selectedMetadata;

    protected ClassificationType selectedClassificationType;

    protected String selectedSlotMetadata;

    protected XDRMessage message;

    protected Integer fileIndex = 1;

    protected Integer folderIndex = 1;

    public int getFileIndex() {
        return this.fileIndex;
    }

    public void setFileIndex(int fileIndex) {
        this.fileIndex = fileIndex;
    }

    public XDRMessage getMessage() {
        return message;
    }

    public void setMessage(XDRMessage message) {
        this.message = message;
    }

    public String getSelectedSlotMetadata() {
        return selectedSlotMetadata;
    }

    public void setSelectedSlotMetadata(String selectedSlotMetadata) {
        this.selectedSlotMetadata = selectedSlotMetadata;
    }

    public ClassificationType getSelectedClassificationType() {
        return selectedClassificationType;
    }

    public void setSelectedClassificationType(
            ClassificationType selectedClassificationType) {
        this.selectedClassificationType = selectedClassificationType;
    }

    public String getSelectedMetadata() {
        return selectedMetadata;
    }

    public void setSelectedMetadata(String selectedMetadata) {
        this.selectedMetadata = selectedMetadata;
    }

    @Override
    public void initSelectedMetadata() {
        this.selectedMetadata = null;
    }

    public List<SelectItem> getListOptionalMetadata() {
        return listOptionalMetadata;
    }

    public void setListOptionalMetadata(List<SelectItem> listOptionalMetadata) {
        this.listOptionalMetadata = listOptionalMetadata;
    }

    public InternationalStringType getSelectedInternationalStringType() {
        return selectedInternationalStringType;
    }

    public void setSelectedInternationalStringType(
            InternationalStringType selectedInternationalStringType) {
        this.selectedInternationalStringType = selectedInternationalStringType;
    }

    public SlotType1 getSelectedSlotType() {
        return selectedSlotType;
    }

    public void setSelectedSlotType(SlotType1 selectedSlotType) {
        this.selectedSlotType = selectedSlotType;
    }

    public String getValueToAdd() {
        return valueToAdd;
    }

    public void setValueToAdd(String valueToAdd) {
        this.valueToAdd = valueToAdd;
    }

    public Boolean getEditXDSDocumentEntry() {
        return editXDSDocumentEntry;
    }

    public void setEditXDSDocumentEntry(Boolean editXDSDocumentEntry) {
        this.editXDSDocumentEntry = editXDSDocumentEntry;
    }

    public Boolean getEditXDSFolder() {
        return editXDSFolder;
    }

    public void setEditXDSFolder(Boolean editXDSFolder) {
        this.editXDSFolder = editXDSFolder;
    }

    public Boolean getEditSubmissionSet() {
        return editSubmissionSet;
    }

    public void setEditSubmissionSet(Boolean editSubmissionSet) {
        this.editSubmissionSet = editSubmissionSet;
    }

    public Boolean getAdviseNodeOpened() {
        return adviseNodeOpened;
    }

    public void setAdviseNodeOpened(Boolean adviseNodeOpened) {
        this.adviseNodeOpened = adviseNodeOpened;
    }

    public XDSFolder getSelectedXDSFolder() {
        return selectedXDSFolder;
    }

    public void setSelectedXDSFolder(XDSFolder selectedXDSFolder) {
        this.selectedXDSFolder = selectedXDSFolder;
    }

    public XDSDocumentEntry getSelectedXDSDocumentEntry() {
        return selectedXDSDocumentEntry;
    }

    public void setSelectedXDSDocumentEntry(
            XDSDocumentEntry selectedXDSDocumentEntry) {
        this.selectedXDSDocumentEntry = selectedXDSDocumentEntry;
    }

    public PNRMessageType getSelectedMessageType() {
        return selectedMessageType;
    }

    public void setSelectedMessageType(PNRMessageType selectedMessageType) {
        this.selectedMessageType = selectedMessageType;
    }

    public XDSSubmissionSet getSubmissionSet() {
        return submissionSet;
    }

    public void setSubmissionSet(XDSSubmissionSet submissionSet) {
        this.submissionSet = submissionSet;
    }

    public List<PNRMessageType> availableMTs() {
        return Arrays.asList(PNRMessageType.values());
    }

    public GazelleTreeNodeImpl<Object> getTreeNodeSubmission() {
        return XDSSubmissionSetManagement.getTreeNodeSubmissionSet(this.submissionSet);
    }

    public void setTreeNodeSubmission(GazelleTreeNodeImpl<Object> treeNodeSubmission) {
        this.treeNodeSubmission = treeNodeSubmission;
    }

    @Deprecated
    public void getTreeNodeSubmissionSet() {
//		this.treeNodeSubmission = XDSSubmissionSetManagement.getTreeNodeSubmissionSet(this.submissionSet);
    }

    public void init(AffinityDomain aff, Transaction trans, RepositoryConfiguration conf) {
        attributes = new SamlAssertionAttributes(false);
        attributes.getXSPAPermissionsHL7().add(CommonSimulatorManager.HL7_PERMISSION_PRD_006);
        attributes.getXSPAPermissionsHL7().add(CommonSimulatorManager.HL7_PERMISSION_PRD_003);
        attributes.getXSPAPermissionsHL7().add(CommonSimulatorManager.HL7_PERMISSION_PRD_004);
        attributes.getXSPAPermissionsHL7().add(CommonSimulatorManager.HL7_PERMISSION_PRD_005);
        attributes.getXSPAPermissionsHL7().add(CommonSimulatorManager.HL7_PERMISSION_PRD_010);
        attributes.getXSPAPermissionsHL7().add(CommonSimulatorManager.HL7_PERMISSION_PRD_016);

        attributes.getXSPAPermissionsHL7().add(CommonSimulatorManager.HL7_PERMISSION_PPD_032);
        attributes.getXSPAPermissionsHL7().add(CommonSimulatorManager.HL7_PERMISSION_PPD_046);
        useXUA = false;

        this.submissionSet = this.generateXDSSubmissionSet();
        this.selectedXDSDocumentEntry = null;
        this.selectedXDSFolder = null;
        this.selectedConfiguration = conf;
        this.selectedAffinityDomain = aff;
        this.message = new XDRMessage();
        this.selectedTransaction = trans;
        this.selectedMessageType = PNRMessageType.NEW_SUBMISSION;
        this.editXDSSubmissionSet();
        this.initListOptionalMetadata();
    }

    public void addFolder(XDSSubmissionSet xx) {
        AffinityDomain aff = (AffinityDomain) Component.getInstance("selectedAffinityDomain");
        Transaction tr = (Transaction) Component.getInstance("selectedTransaction");
        XDSFolderManagement.addNewFolderToXDSSubmissionSet(xx, this, aff, tr);
    }

    public void addDocumentEntry(XDSSubmissionSet xx) {
        AffinityDomain aff = (AffinityDomain) Component.getInstance("selectedAffinityDomain");
        Transaction tr = (Transaction) Component.getInstance("selectedTransaction");
        DocumentEntryManagement.addNewDocumentEntryToXDSSubmissionSet(this.submissionSet, this, aff, tr);
    }

    public void addDocumentEntry(XDSFolder xx) {
        AffinityDomain aff = (AffinityDomain) Component.getInstance("selectedAffinityDomain");
        Transaction tr = (Transaction) Component.getInstance("selectedTransaction");
        DocumentEntryManagement.addNewDocumentEntryToXDSFolder(this.selectedXDSFolder, this, aff, tr);
    }

    protected XDSSubmissionSet generateXDSSubmissionSet() {
        AffinityDomain aff = (AffinityDomain) Component.getInstance("selectedAffinityDomain");
        Transaction trans = (Transaction) Component.getInstance("selectedTransaction");
        return XDSSubmissionSetManagement.generateXDSSubmissionSet(aff, trans);
    }

    public List<SelectItem> listValueSet(ClassificationType cl) {
        return XDSCommonModule.listValueSet(cl);
    }

    public void editXDSDocumentEntry(XDSDocumentEntry xx) {
        this.selectedXDSDocumentEntry = xx;
        this.editXDSFolder = false;
        this.editSubmissionSet = false;
        this.editXDSDocumentEntry = true;
    }

    public void editXDSFolder(XDSFolder xx) {
        this.selectedXDSFolder = xx;
        this.editSubmissionSet = false;
        this.editXDSDocumentEntry = false;
        this.editXDSFolder = true;
    }

    public void editXDSSubmissionSet() {
        this.editXDSSubmissionSet(this.submissionSet);
    }

    public void editXDSSubmissionSet(XDSSubmissionSet xds) {
        this.editSubmissionSet = true;
        this.editXDSDocumentEntry = false;
        this.editXDSFolder = false;
    }

    public String getDisplayNameExt(ExternalIdentifierType ext) {
        return RIMGenerator.extWeak.get(ext).getDisplayAttributeName();
    }

    public String getOptionalityExtOnSubmissionSet(ExternalIdentifierType ext) {
        return XDSSubmissionSetManagement.getOptionalityExtOnSubmissionSet(this.submissionSet, ext);
    }

    public String getDisplayNameClass(ClassificationType ext) {
        return RIMGenerator.clWeak.get(ext).getDisplayAttributeName();
    }

    public boolean shouldNodeRepresentationBeLetEmpty(ClassificationType ext) {
        return "XDSDocumentEntry.author".equals(getDisplayNameClass(ext));
    }

    public String getOptionalityClassOnSubmissionSet(ClassificationType ext) {
        return XDSSubmissionSetManagement.getOptionalityClassOnSubmissionSet(this.submissionSet, ext);
    }

    public String rownSpanClass(ClassificationType cl) {
        return XDSCommonModule.rownSpanClass(cl);
    }


    public void initSelectedSlotType(SlotType1 sl) {
        this.selectedSlotType = sl;
        this.valueToAdd = null;
        this.fileIndex = 1;
        this.folderIndex = 1;
    }

    public boolean canAddName(InternationalStringType name) {
        return !(name != null && (name.getLocalizedString() != null) && (name.getLocalizedString().size() > 0));
    }

    public boolean isPatientIDEmpty() {
        return false;
    }

    public void initSelectedInternationalStringType(ClassificationType cl) {
        if (cl.getName() == null) {
            cl.setName(new InternationalStringType());
        }
        this.selectedInternationalStringType = cl.getName();
        this.valueToAdd = null;
    }

    public void setValueOfTheName(String val) {
        if (this.selectedInternationalStringType == null) {
            this.selectedInternationalStringType = new InternationalStringType();
        }
        this.selectedInternationalStringType.setLocalizedString(new ArrayList<LocalizedStringType>());
        this.selectedInternationalStringType.getLocalizedString().add(new LocalizedStringType());
        this.selectedInternationalStringType.getLocalizedString().get(0).setValue(val);
    }

    public void initListOptionalMetadata() {
        RegistryObjectMetadata ss = null;
        RegistryObjectType rot = null;
        if (editSubmissionSet) {
            ss = RIMGenerator.rpWeak.get(this.submissionSet);
            rot = this.submissionSet;
        }
        if (editXDSFolder) {
            ss = RIMGenerator.rpWeak.get(this.selectedXDSFolder);
            rot = this.selectedXDSFolder;
        }
        if (editXDSDocumentEntry) {
            ss = RIMGenerator.docWeak.get(this.selectedXDSDocumentEntry);
            rot = this.selectedXDSDocumentEntry;
        }
        this.listOptionalMetadata = XDSCommonModule.initListOptionalMetadata(rot, ss, this);
    }

    public void addNewMetadataToSubmissionSet() {
        RegistryObjectType rot = null;
        if (this.editSubmissionSet) {
            rot = this.submissionSet;
        }
        if (this.editXDSDocumentEntry) {
            rot = this.selectedXDSDocumentEntry;
        }
        if (this.editXDSFolder) {
            rot = this.selectedXDSFolder;
        }
        XDSCommonModule.addNewMetadataToRegistryObjectType(rot, selectedMetadata, this);
    }

    public void removeClassificationFromSubmissionSet(ClassificationType cl) {
        XDSSubmissionSetManagement.removeClassificationFromSubmissionSet(this.submissionSet, cl, this);
    }

    public void removeExtFromSubmissionSet(ExternalIdentifierMetadata ext) {
        XDSSubmissionSetManagement.removeExtFromSubmissionSet(this.submissionSet, ext, this);
    }

    public boolean canAddValueToSlot(SlotType1 sl) {
        if (sl != null) {
            SlotMetadata slm = RIMGenerator.slWeak.get(sl);
            if (slm != null) {
                if ((slm.getMultiple() != null) && (slm.getMultiple() == false)) {
                    if ((sl.getValueList() != null) && (sl.getValueList().getValue().size() > 0)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public String getOptionalitySlotOnSubmissionSet(SlotType1 sl) {
        return XDSSubmissionSetManagement.getOptionalitySlotOnSubmissionSet(this.submissionSet, sl);
    }

    public void removeSlotFromSubmissionSet(SlotType1 sl) {
        XDSSubmissionSetManagement.removeSlotFromSubmissionSet(this.submissionSet, sl, this);
    }

    public boolean hasOptionalSlotYet(ClassificationType cl) {
        return XDSCommonModule.hasOptionalSlotYet(cl);
    }

    public List<SelectItem> getListOptionalSlotMetadata() {
        List<SelectItem> res = new ArrayList<SelectItem>();
        res.add(new SelectItem(null, "please select.."));
        if (this.selectedClassificationType != null) {
            ClassificationMetaData clm = RIMGenerator.clWeak.get(this.selectedClassificationType);
            for (SlotMetadata slm : clm.getSlot_optional()) {
                boolean alreadyExist = false;
                for (SlotType1 sl : this.selectedClassificationType.getSlot()) {
                    if (sl.getName().equals(slm.getName())) {
                        alreadyExist = true;
                    }
                }
                if (!alreadyExist) {
                    res.add(new SelectItem(slm.getName(), slm.getName()));
                }
            }
        }
        this.selectedSlotMetadata = null;
        return res;
    }

    public void addSelectedSlotToClassification() {
        if (this.selectedClassificationType != null) {
            if (this.selectedSlotMetadata != null) {
                ClassificationMetaData clm = RIMGenerator.clWeak.get(this.selectedClassificationType);
                for (SlotMetadata slm : clm.getSlot_optional()) {
                    if (slm.getName().equals(this.selectedSlotMetadata)) {
                        SlotType1 sl = RIMGenerator.generateSlot(slm);
                        this.selectedClassificationType.getSlot().add(sl);
                        break;
                    }
                }
            }
        }
        this.selectedSlotMetadata = null;
    }

    public String getOptionalitySlotOnClassification(SlotType1 sl, ClassificationType cl) {
        ClassificationMetaData clm = RIMGenerator.clWeak.get(cl);
        String res = "O";
        for (SlotMetadata slm : clm.getSlot_required()) {
            if (slm.getName().equals(sl.getName())) {
                res = "R";
                break;
            }
        }
        return res;
    }

    //---------------XDS Folder -------------------

    public String getOptionalityExtOnSelectedXDSFolder(ExternalIdentifierType ext) {
        return XDSFolderManagement.getOptionalityExtOnSelectedXDSFolder(this.selectedXDSFolder, ext);
    }

    public void removeExtFromSelectedXDSFolder(ExternalIdentifierType ext) {
        XDSFolderManagement.removeExtFromSelectedXDSFolder(this.selectedXDSFolder, ext, this);
    }

    public String getOptionalitySlotOnSelectedXDSFolder(SlotType1 sl) {
        return XDSFolderManagement.getOptionalitySlotOnSelectedXDSFolder(this.selectedXDSFolder, sl);
    }

    public void removeSlotFromSelectedXDSFolder(SlotType1 sl) {
        XDSFolderManagement.removeSlotFromSelectedXDSFolder(this.selectedXDSFolder, sl, this);
    }

    public String getOptionalityClassOnSelectedXDSFolder(ClassificationType ext) {
        return XDSFolderManagement.getOptionalityClassOnSelectedXDSFolder(this.selectedXDSFolder, ext);
    }

    public void removeClassificationFromSelectedXDSFolder(ClassificationType cl) {
        XDSFolderManagement.removeClassificationFromSelectedXDSFolder(this.selectedXDSFolder, cl, this);
    }

    // -------------------------------------------

    // -------XDS Docuement Entry -----------------------------

    public String getOptionalityExtOnSelectedXDSDocumentEntry(ExternalIdentifierType ext) {
        return DocumentEntryManagement.getOptionalityExtOnSelectedXDSDocumentEntry(this.selectedXDSDocumentEntry, ext);
    }

    public void removeExtFromSelectedXDSDocumentEntry(ExternalIdentifierType ext) {
        DocumentEntryManagement.removeExtFromSelectedXDSDocumentEntry(this.selectedXDSDocumentEntry, ext, this);
    }

    public String getOptionalitySlotOnSelectedXDSDocumentEntry(SlotType1 sl) {
        return DocumentEntryManagement.getOptionalitySlotOnSelectedXDSDocumentEntry(this.selectedXDSDocumentEntry, sl);
    }

    public void removeSlotFromSelectedXDSDocumentEntry(SlotType1 sl) {
        DocumentEntryManagement.removeSlotFromSelectedXDSDocumentEntry(this.selectedXDSDocumentEntry, sl, this);
    }

    public String getOptionalityClassOnSelectedXDSDocumentEntry(ClassificationType ext) {
        return DocumentEntryManagement.getOptionalityClassOnSelectedXDSDocumentEntry(this.selectedXDSDocumentEntry, ext);
    }

    public void removeClassificationFromSelectedXDSDocumentEntry(ClassificationType cl) {
        DocumentEntryManagement.removeClassificationFromSelectedXDSDocumentEntry(this.selectedXDSDocumentEntry, cl, this);
    }

    public void uploadListener(FileUploadEvent event) {

        UploadedFile item = event.getUploadedFile();
        FileOutputStream fos = null;
        try {
            PnRRegisteredFile registeredFile = PnRRegisteredFile.saveFile(item.getName(), item.getContentType());
            registeredFile.setDocumentId("document" + registeredFile.getId());
            registeredFile.setIndex(this.selectedXDSDocumentEntry.getIndex());
            registeredFile.setUuid(this.selectedXDSDocumentEntry.getId().split(":")[this.selectedXDSDocumentEntry.getId().split(":").length - 1]
                    .replace("-", "").toUpperCase());
            File fileToWrite = new File(registeredFile.getPath());
            fos = new FileOutputStream(fileToWrite);
            if (item.getData() != null && item.getData().length > 0) {
                fos.write(item.getData());
                fos.close();
            } else {
                FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "The file is empty !");
            }
            this.selectedXDSDocumentEntry.setRelatedFile(registeredFile);
            this.selectedXDSDocumentEntry.setMimeType(this.selectedXDSDocumentEntry.getRelatedFile().getType());
        } catch (IOException e) {
            log.error("An error occurred when uploaded file with name " + item.getName());
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.xdrsrc.cannotStoreFile", item.getName());
        } finally {
            try {
                fos.close();
            } catch (Exception e) {
                log.error("Cannot close channel");
            }
        }
    }

    public boolean isValueSet(ClassificationType cl) {
        return XDSCommonModule.isValueSet(cl);
    }

    public void updateClassificationByValueSet(ClassificationType cl) {
        XDSCommonModule.updateClassificationByValueSet(cl, cl.getNodeRepresentation());
    }

    // --------------------------------------------------------

    public ProvideAndRegisterDocumentSetRequestType generateProvideAndRegisterDocumentSetRequestType() throws CloneNotSupportedException {
        ProvideAndRegisterDocumentSetRequestType res = new ProvideAndRegisterDocumentSetRequestType();
        res.setSubmitObjectsRequest(this.generateSubmitObjectsRequestType());
        if (this.submissionSet != null) {
            if (this.submissionSet.getListXDSDocumentEntry() != null) {
                for (XDSDocumentEntry doc : this.submissionSet.getListXDSDocumentEntry()) {
                    DocumentType dt = new DocumentType();
                    dt.setId(doc.getId());
                    Include inc = new Include();
                    inc.setHref("cid:" + doc.getIndex() + ".urn:uuid:" + doc.getId().split(":")[this.selectedXDSDocumentEntry.getId().split(":")
                            .length - 1].replace("-", "").toUpperCase() + "@ws.jboss.org");
                    ObjectFactory of = new ObjectFactory();
                    JAXBElement<Include> addd = of.createDocumentTypeInclude(inc);
                    dt.getMixed().add(addd);
                    res.getDocument().add(dt);
                }
            }
            if (this.submissionSet.getListXDSFolder() != null) {
                for (XDSFolder folder : this.submissionSet.getListXDSFolder()) {
                    if (folder.getListXDSDocumentEntry() != null) {
                        for (XDSDocumentEntry doc : folder.getListXDSDocumentEntry()) {
                            DocumentType dt = new DocumentType();
                            dt.setId(doc.getId());
                            Include inc = new Include();
                            inc.setHref("cid:" + doc.getIndex() + ".urn:uuid:" + doc.getId().split(":")[this.selectedXDSDocumentEntry.getId().split
                                    (":").length - 1].replace("-", "").toUpperCase() + "@ws.jboss.org");
                            ObjectFactory of = new ObjectFactory();
                            JAXBElement<Include> addd = of.createDocumentTypeInclude(inc);
                            dt.getMixed().add(addd);
                            res.getDocument().add(dt);
                        }
                    }
                }
            }
        }
        return res;
    }

    public SubmitObjectsRequestType generateSubmitObjectsRequestType() throws CloneNotSupportedException {
        SubmitObjectsRequestType res = new SubmitObjectsRequestType();
        net.ihe.gazelle.rim.ObjectFactory ofrim = new net.ihe.gazelle.rim.ObjectFactory();
        res.setRegistryObjectList(new RegistryObjectListType());
        res.getRegistryObjectList().getIdentifiableGroup().add(
                ofrim.createRegistryObjectListTypeRegistryPackage(this.submissionSet.getCleanedRegistryPackageType()));
        if (this.submissionSet.getListXDSFolder() != null) {
            for (XDSFolder folder : this.submissionSet.getListXDSFolder()) {
                res.getRegistryObjectList().getIdentifiableGroup().add(
                        ofrim.createRegistryObjectListTypeRegistryPackage(folder.getCleanedRegistryPackageType(this.submissionSet.getPatientId())));

                AssociationType1 sub2folder = RIMBuilderCommon.createAssociation(ASSOCIATION_TYPE_HAS_MEMBER, this.submissionSet.getId(), folder
                        .getId(), null, null, null);
                res.getRegistryObjectList().getIdentifiableGroup().add(ofrim.createRegistryObjectListTypeAssociation(sub2folder));

                if (folder.getListXDSDocumentEntry() != null) {
                    for (XDSDocumentEntry doc : folder.getListXDSDocumentEntry()) {
                        res.getRegistryObjectList().getIdentifiableGroup().add(
                                ofrim.createRegistryObjectListTypeExtrinsicObject(doc.getCleanedExtrinsicObjectType(this.submissionSet.getPatientId
                                        ())));
                        AssociationType1 sub2doc = RIMBuilderCommon.createAssociation(ASSOCIATION_TYPE_HAS_MEMBER, this.submissionSet.getId(), doc
                                .getId(), "SubmissionSetStatus", "Original", null);
                        res.getRegistryObjectList().getIdentifiableGroup().add(
                                ofrim.createRegistryObjectListTypeAssociation(sub2doc));

                        AssociationType1 folder2doc = RIMBuilderCommon.createAssociation(ASSOCIATION_TYPE_HAS_MEMBER, folder.getId(), doc.getId(),
                                null, null, null);
                        res.getRegistryObjectList().getIdentifiableGroup().add(
                                ofrim.createRegistryObjectListTypeAssociation(folder2doc));

                        AssociationType1 sub2folder2doc = RIMBuilderCommon.createAssociation(ASSOCIATION_TYPE_HAS_MEMBER, this.submissionSet.getId
                                (), folder2doc.getId(), null, null, null);
                        res.getRegistryObjectList().getIdentifiableGroup().add(
                                ofrim.createRegistryObjectListTypeAssociation(sub2folder2doc));

                        if (doc.getAssociationType() != null) {
                            AssociationType1 sub3doc = RIMBuilderCommon.createAssociation(doc.getAssociationType().getValue(), doc.getId(), doc
                                    .getTargetDocument(), null, null, null);
                            res.getRegistryObjectList().getIdentifiableGroup().add(
                                    ofrim.createRegistryObjectListTypeAssociation(sub3doc));
                        }
                    }
                }
            }
        }

        if (this.submissionSet.getListXDSDocumentEntry() != null) {
            for (XDSDocumentEntry doc : this.submissionSet.getListXDSDocumentEntry()) {
                res.getRegistryObjectList().getIdentifiableGroup().add(
                        ofrim.createRegistryObjectListTypeExtrinsicObject(doc.getCleanedExtrinsicObjectType(this.submissionSet.getPatientId())));
                AssociationType1 sub2doc = RIMBuilderCommon.createAssociation(ASSOCIATION_TYPE_HAS_MEMBER, this.submissionSet.getId(), doc.getId(),
                        "SubmissionSetStatus", "Original", null);
                res.getRegistryObjectList().getIdentifiableGroup().add(
                        ofrim.createRegistryObjectListTypeAssociation(sub2doc));

                if (doc.getAssociationType() != null) {
                    AssociationType1 sub3doc = RIMBuilderCommon.createAssociation(doc.getAssociationType().getValue(), doc.getId(), doc
                            .getTargetDocument(), null, null, null);
                    res.getRegistryObjectList().getIdentifiableGroup().add(
                            ofrim.createRegistryObjectListTypeAssociation(sub3doc));
                }
            }
        }
        return res;
    }

    public Pair<String, byte[]> buildMTOMRequest() throws CloneNotSupportedException, ParserConfigurationException, SignatureException {
        ProvideAndRegisterDocumentSetRequestType sub = this.generateProvideAndRegisterDocumentSetRequestType();
        String rr = PnRManagement.getProvideAndRegisterDocumentSetRequestTypeAsString(sub);
        String patientId = this.submissionSet.getPatientId();
        String receiverUrl = this.selectedConfiguration.getUrl();
        if (patientId != null && receiverUrl != null) {
            String soapMessage = SOAPRequestBuilder.createSOAPMessage(rr, useXUA, praticianID, attributes, patientId, receiverUrl,
                    "urn:ihe:iti:2007:ProvideAndRegisterDocumentSet-b", true);
            List<PnRRegisteredFile> listFile = createListRegisteredFile();
            Pair<String, byte[]> res = null;
            try {
                res = MessageSender.buildMTOMFromData(soapMessage, listFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return res;
        }
        return null;
    }

    private List<PnRRegisteredFile> createListRegisteredFile() {
        List<PnRRegisteredFile> listFile = new ArrayList<PnRRegisteredFile>();
        if (this.submissionSet != null) {
            if (this.submissionSet.getListXDSFolder() != null) {
                for (XDSFolder folder : this.submissionSet.getListXDSFolder()) {
                    if (folder.getListXDSDocumentEntry() != null) {
                        for (XDSDocumentEntry doc : folder.getListXDSDocumentEntry()) {
                            if (doc.getRelatedFile() != null) {
                                listFile.add(doc.getRelatedFile());
                            }
                        }
                    }
                }
            }
            if (this.submissionSet.getListXDSDocumentEntry() != null) {
                for (XDSDocumentEntry doc : this.submissionSet.getListXDSDocumentEntry()) {
                    if (doc.getRelatedFile() != null) {
                        listFile.add(doc.getRelatedFile());
                    }
                }
            }
        }
        return listFile;
    }

    public void sendProvideAndRegisterSetb() throws CloneNotSupportedException, JAXBException, ParserConfigurationException, SignatureException {
        Pair<String, byte[]> mtom = this.buildMTOMRequest();
        if (mtom != null) {
            MessageSenderCommon msc = new MessageSenderCommon();

            message = new XDRMessage();
            net.ihe.gazelle.util.Pair<String, String> resppail = msc.sendMessage(this.selectedConfiguration.getUrl(), mtom.getObject2(), message, mtom
                    .getObject1());
            String resp = resppail != null ? resppail.getObject2() : null;
            String resph = resppail != null ? resppail.getObject1() : null;
            if (resp != null) {
                resp = XDSCommonModule.formatStringIfContainsXML(resp);
            }

            message.setTimeStamp(new Date());
            message.setConfiguration(selectedConfiguration);
            message.setTransaction(selectedTransaction);
            message.setAffinityDomain(this.selectedAffinityDomain);
            message.setContentType(ContentType.MTOM_XOP);
            message.setRegisteredFiles(this.createListRegisteredFile());
            message.setGazelleDriven(false);

            MetadataMessage sent = new MetadataMessage();

            String req = null;
            Attachments attachments = null;
            if (mtom != null) {
                attachments = new Attachments(new ByteArrayInputStream(mtom.getObject2()), mtom.getObject1());
                req = AttachmentsUtil.transformMultipartToSOAP(attachments);
            }

            if (req != null) {
                req = XDSCommonModule.formatStringIfContainsXML(req);
            }

            sent.setMessageContent(PnRManagement.patchXop(req));
            sent.setHttpHeader(mtom.getObject1());
            sent.setMessageType(MetadataMessageType.PROVIDE_AND_REGISTER_DOCUMENT_SET_REQUEST);
            if (attachments != null) {
                try {
                    List<AttachmentFile> fileAttachedToReceiver = AttachmentsUtil.generateListAttachmentFiles(attachments, req);
                    if (fileAttachedToReceiver != null && fileAttachedToReceiver.size() > 0) {
                        sent.setListAttachments(fileAttachedToReceiver);
                        for (AttachmentFile attachmentFile : fileAttachedToReceiver) {
                            attachmentFile.setMetadataMessage(sent);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            MetadataMessage received = new MetadataMessage();
            received.setMessageContent(resp);
            received.setHttpHeader(resph);
            received.setMessageType(MetadataMessageType.REGIQTRY_RESPONSE);


            message.setSentMessageContent(sent);
            message.setReceivedMessageContent(received);
            message = XDRMessage.storeMessage(message);
            Contexts.getSessionContext().set("messagesToDisplay", message);
            displayResultPanel = true;
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Patient id or configuration url is null");
        }
    }

    @Override
    public void listAllConfigurations() {
        // Not applicable
    }

    @Override
    public void updateSpecificClient() {
        // Not applicable
    }

    @Override
    public List<AffinityDomain> listAllAffinityDomains() {
        // Not applicable
        return null;
    }

    public void deleteXDSFolder(XDSFolder xdsfolder) {
        if ((this.submissionSet != null) && (xdsfolder != null)) {
            this.submissionSet.getListXDSFolder().remove(xdsfolder);
        }
        this.editSubmissionSet = true;
        this.editXDSDocumentEntry = false;
        this.editXDSFolder = false;
        this.folderIndex -= 1;
    }

    public void deleteXDSDocument(XDSDocumentEntry xdsdoc) {
        if ((this.submissionSet != null) && (xdsdoc != null)) {
            if (this.submissionSet.getListXDSDocumentEntry().contains(xdsdoc)) {
                this.submissionSet.getListXDSDocumentEntry().remove(xdsdoc);
            }
            if (this.submissionSet.getListXDSFolder() != null) {
                for (XDSFolder xdsfolder : this.submissionSet.getListXDSFolder()) {
                    if (xdsfolder.getListXDSDocumentEntry() != null && xdsfolder.getListXDSDocumentEntry().contains(xdsdoc)) {
                        xdsfolder.getListXDSDocumentEntry().remove(xdsdoc);
                    }
                }
            }
        }
        this.editSubmissionSet = true;
        this.editXDSDocumentEntry = false;
        this.editXDSFolder = false;
        this.fileIndex -= 1;
    }

    public List<SelectItem> getListAssociationTypes() {
        List<SelectItem> res = new ArrayList<SelectItem>();
        res.add(new SelectItem(null, "please select.."));
        for (AssociationType ass : AssociationType.values()) {
            res.add(new SelectItem(ass, ass.getName()));
        }
        return res;
    }

    public void randomFulFilDocEntryMetadata() {
        DocumentEntryManagement.randomFulFilDocEntryMetadata(selectedXDSDocumentEntry);
    }

    public String previewMessage() {
        Pair<String, byte[]> mom = null;
        try {
            mom = buildMTOMRequest();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        }
        if (mom != null) {
            Attachments attachments = new Attachments(new ByteArrayInputStream(mom.getObject2()), mom.getObject1());
            String req = AttachmentsUtil.transformMultipartToSOAP(attachments);

            if (req != null) {
                req = XDSCommonModule.formatStringIfContainsXML(req);
                return req;
            }
            return null;
        }
        return null;
    }

    @Override
    public String getTheNameOfThePage() {
        // NotUsed
        return null;
    }

    @Override
    public int getFolderIndex() {
        return this.folderIndex;
    }

    @Override
    public void setFolderIndex(int folderIndex) {
        this.folderIndex = folderIndex;
    }


}
