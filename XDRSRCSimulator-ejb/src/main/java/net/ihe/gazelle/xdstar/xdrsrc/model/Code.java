package net.ihe.gazelle.xdstar.xdrsrc.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import net.ihe.gazelle.simulator.common.tf.model.Transaction;

import org.hibernate.annotations.Type;
import org.jboss.seam.annotations.Name;

@Entity
@Name("code")
@Table(name = "code", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "code_sequence", sequenceName = "code_id_seq")
public class Code implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	@GeneratedValue(generator = "code_sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	// used for nodeRepresentation attribute
	@Column(name = "code")
	private String code;

	@Column(name = "coding_scheme")
	private String codingScheme;

	// used for localizedString node
	@Column(name = "display")
	@Lob
	@Type(type = "text")
	private String display;

	@Column(name = "class_scheme")
	private String classScheme;

	@ManyToOne
	@JoinColumn(name = "transaction_id")
	private Transaction transaction;

	public Code() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodingScheme() {
		return codingScheme;
	}

	public void setCodingScheme(String codingScheme) {
		this.codingScheme = codingScheme;
	}

	public void setClassScheme(String classScheme) {
		this.classScheme = classScheme;
	}

	public String getClassScheme() {
		return classScheme;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	/**
	 * 
	 * @param transaction
	 * @param code
	 *            TODO
	 * @param clazz
	 * @param <T>
	 * @return
	 */
	public static List<Code> getCodesForTransaction(String classScheme,
			Transaction transaction, String code) {
	    CodeQuery q = new CodeQuery();
	    if (transaction != null) q.transaction().eq(transaction);
	    if (classScheme != null) q.classScheme().eq(classScheme);
	    if (code != null) q.code().eq(code);
	    return q.getList();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((code == null) ? 0 : code.hashCode());
		result = (prime * result)
				+ ((codingScheme == null) ? 0 : codingScheme.hashCode());
		result = (prime * result)
				+ ((display == null) ? 0 : display.hashCode());
		result = (prime * result)
				+ ((classScheme == null) ? 0 : classScheme.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Code other = (Code) obj;
		if (code == null) {
			if (other.code != null) {
				return false;
			}
		} else if (!code.equals(other.code)) {
			return false;
		}
		if (codingScheme == null) {
			if (other.codingScheme != null) {
				return false;
			}
		} else if (!codingScheme.equals(other.codingScheme)) {
			return false;
		}
		if (display == null) {
			if (other.display != null) {
				return false;
			}
		} else if (!display.equals(other.display)) {
			return false;
		}
		if (classScheme == null) {
			if (other.classScheme != null) {
				return false;
			}
		} else if (!classScheme.equals(other.classScheme)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Code [id=" + id + ", code=" + code + ", codingScheme="
				+ codingScheme + ", display=" + display + ", classScheme="
				+ classScheme + ", transaction=" + transaction + "]";
	}

}
