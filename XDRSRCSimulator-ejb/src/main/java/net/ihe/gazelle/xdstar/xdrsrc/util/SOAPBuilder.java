package net.ihe.gazelle.xdstar.xdrsrc.util;

import net.ihe.gazelle.lcm.SubmitObjectsRequestType;
import net.ihe.gazelle.rim.*;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.AffinityDomain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.common.utils.XmlUtil;
import net.ihe.gazelle.simulator.common.xua.SamlAssertionAttributes;
import net.ihe.gazelle.simulator.common.xua.SamlAssertionSupplier;
import net.ihe.gazelle.simulator.common.xua.SignatureException;
import net.ihe.gazelle.xds.DocumentType;
import net.ihe.gazelle.xds.ObjectFactory;
import net.ihe.gazelle.xds.ProvideAndRegisterDocumentSetRequestType;
import net.ihe.gazelle.xdstar.comon.util.RIMBuilderCommon;
import net.ihe.gazelle.xdstar.comon.util.Util;
import net.ihe.gazelle.xdstar.comon.util.XDSNamespacePrefixMapper;
import net.ihe.gazelle.xdstar.core.SignatureManager;
import net.ihe.gazelle.xdstar.xdrsrc.model.Metadata;
import net.ihe.gazelle.xdstar.xdrsrc.model.MetadataValue;
import net.ihe.gazelle.xdstar.xdrsrc.model.PnRRegisteredFile;
import net.ihe.gazelle.xop.Include;
import org.apache.log4j.Logger;
import org.picketlink.identity.federation.core.wstrust.WSTrustException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 *  <b>Class Description :  </b>SOAPBuilder<br><br>
 *  SOAPBuilder the  interface  of SearchManager
 *  
 * @class			SOAPBuilder manage all the method to build hte soap envelopp
 * @package			net.ihe.gazelle.simulator.xdrsrc.action
 * @author 			Abderrazek Boufahja / IHE Europe
 * @author			Anne-Gaëlle Bergé / INRIA Rennes IHE development Project
 * @version			1.1 - 2010, January 26th
 *
 */
public class SOAPBuilder {
	
		
	private static final String SUBMISSION_SET = "SubmissionSet";

	private static Logger log = Logger.getLogger(SOAPBuilder.class);
	
	private boolean useXUA;
	private List<MetadataValue> submissionSetMetadata;
	private List<PnRRegisteredFile> filesToLink;
	private SamlAssertionAttributes attributes;
	private String praticianID;
	private String receiverUrl;
	private AffinityDomain affinityDomain;
	private Transaction transaction;
	private List<AssociationType1> associations;
	private String patientId;
	
	
	public SOAPBuilder()
	{
		
	}

    public static void save(OutputStream os, Object xdww) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.xds");
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new XDSNamespacePrefixMapper());
        m.marshal(xdww, os);
    }

    /**
     * request an assertion to the X Assertion Provider
     *
     * @param praticianID
     * @param stsEndpoint             The X Assertion Provider to request an assertion
     * @param samlAssertionAttributes
     * @return
     */
    private static Element requestAssertion(String praticianID, String stsEndpoint, SamlAssertionAttributes samlAssertionAttributes) {
        try {
            if (praticianID == null || praticianID.isEmpty()) {
                praticianID = ApplicationConfiguration.getValueOfVariable("default_pratician_id");
            }
            Element assertion = SamlAssertionSupplier.getAssertion(praticianID, samlAssertionAttributes, SignatureManager.getSignatureUtil());
            return assertion;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) throws FileNotFoundException, JAXBException {
        Scanner scan = new Scanner(new File("/Users/aboufahj/Downloads/Sans titre.xml"));
        String res = "";
        while (scan.hasNext()) {
            res = res + scan.nextLine() + "\n";
        }
        SOAPBuilder sb = new SOAPBuilder();
        System.out.println(sb.patchXop(res));
    }

	public boolean isUseXUA() {
		return useXUA;
	}

	public void setUseXUA(boolean useXUA) {
		this.useXUA = useXUA;
	}

    public List<MetadataValue> getSubmissionSetMetadata() {
        return submissionSetMetadata;
    }

    public void setSubmissionSetMetadata(List<MetadataValue> submissionSetMetadata) {
        this.submissionSetMetadata = submissionSetMetadata;
    }

	public List<PnRRegisteredFile> getFilesToLink() {
		return filesToLink;
	}

	public void setFilesToLink(List<PnRRegisteredFile> filesToLink) {
		this.filesToLink = filesToLink;
	}

	public SamlAssertionAttributes getAttributes() {
        return attributes;
    }

    public void setAttributes(SamlAssertionAttributes attributes) {
        this.attributes = attributes;
	}

    public String getReceiverUrl() {
        return receiverUrl;
    }

    public void setReceiverUrl(String receiverUrl) {
        this.receiverUrl = receiverUrl;
    }

	public String getPraticianID() {
		return praticianID;
	}

	public void setPraticianID(String praticianID) {
		this.praticianID = praticianID;
	}

	public AffinityDomain getAffinityDomain() {
		return affinityDomain;
	}

	public void setAffinityDomain(AffinityDomain affinityDomain) {
		this.affinityDomain = affinityDomain;
	}

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

	public String getPatientId() {
		return patientId;
	}

    public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

    public List<AssociationType1> getAssociations() {
        return associations;
    }

    public void setAssociations(List<AssociationType1> associations) {
        this.associations = associations;
    }

    /**
     *
	 * @return
	 */
	public String createMessage()
	{
		net.ihe.gazelle.rim.ObjectFactory ofrim = new net.ihe.gazelle.rim.ObjectFactory();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		try {
			// create document
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document request = builder.newDocument();
			request.setXmlVersion("1.0");
			request.setXmlStandalone(true);
			Element root = createEnvelope(request);
			Element assertion = null;
			Element trcAssertion = null;

			// generate assertion if required
			if (useXUA)
			{
				assertion = requestAssertion(praticianID, null, attributes);

				// generate TRC assertion if epSOS domain
				if (affinityDomain.getKeyword().equals("epSOS"))
				{
					trcAssertion = requestTRCAssertion(assertion, attributes);
				}
			}

			// header
			Element header = createSOAPHeader(request, assertion, trcAssertion);
            root.appendChild(header);

			// body
            Element body = request.createElement("s:Body");
            body.setAttribute("xmlns:s", "http://www.w3.org/2003/05/soap-envelope");

			// provideAndRegisterDocumentSetRequest
			//------------------------
			ProvideAndRegisterDocumentSetRequestType pnr = new ProvideAndRegisterDocumentSetRequestType();

			pnr.setSubmitObjectsRequest(new SubmitObjectsRequestType());
			pnr.getSubmitObjectsRequest().setRegistryObjectList(new RegistryObjectListType());

			Metadata xdsDocumentEntry = Metadata.getMetadataByName("XDSDocumentEntry");
			List<String> uuids = null;
			for (PnRRegisteredFile file : filesToLink)
			{
				uuids = new ArrayList<String>();
				uuids.add(xdsDocumentEntry.getUuid());
				ExtrinsicObjectType extrinsicObject = createExtrinsicObject(xdsDocumentEntry, file.getType(), file.getDocumentId());

				List<SlotType1> slots = new ArrayList<SlotType1>();
				List<ClassificationType> classifications = new ArrayList<ClassificationType>();
				List<ExternalIdentifierType> externalIdentifiers = new ArrayList<ExternalIdentifierType>();

				for (MetadataValue mv : file.getMetadata())
				{
					// Slots
					if (mv.getMetadata().getType().equals(Metadata.SLOT))
					{
						slots.add(RIMBuilderCommon.createSlot(mv.getMetadata().getName(), mv.getValue()));
						continue;
					}
					// Classifications
					else if (mv.getMetadata().getType().equals(Metadata.CLASSIFICATION))
					{
						classifications.add(createClassification(mv, file.getDocumentId()));
						uuids.add(mv.getMetadata().getUuid());
						continue;
					}
					// External Identifications
					else if (mv.getMetadata().getType().equals(Metadata.EXTERNAL_IDENTIFIER))
					{
						externalIdentifiers.add(createExternalIdentifier(mv, file.getDocumentId()));
						continue;
					}
					else
						continue;
				}
				for (String uuid : uuids)
				{
					pnr.getSubmitObjectsRequest().getRegistryObjectList().getIdentifiableGroup().add(
							ofrim.createRegistryObjectListTypeObjectRef(createObjectRef(uuid)));
				}
				for (SlotType1 slot : slots){
					extrinsicObject.getSlot().add(slot);
				}
				extrinsicObject.setName(RIMBuilderCommon.createNameOrDescription(file.getDocumentId()));
				extrinsicObject.setDescription(RIMBuilderCommon.createNameOrDescription(file.getName()));

				for (ClassificationType classification: classifications){
					extrinsicObject.getClassification().add(classification);
				}
				for (ExternalIdentifierType externalId : externalIdentifiers) {
                    extrinsicObject.getExternalIdentifier().add(externalId);
                }
				pnr.getSubmitObjectsRequest().getRegistryObjectList().getIdentifiableGroup().add(
						ofrim.createRegistryObjectListTypeExtrinsicObject(extrinsicObject));
			}

			if (associations != null && !associations.isEmpty())
			{
				for (AssociationType1 association: associations)
				{
					pnr.getSubmitObjectsRequest().getRegistryObjectList().getIdentifiableGroup().add(
							ofrim.createRegistryObjectListTypeAssociation(association));
				}
			}

			uuids = new ArrayList<String>();
			Metadata submissionSet = Metadata.getMetadataByName(Metadata.XDSSUBMISSIONSET);
			RegistryPackageType registryPackage = RIMBuilderCommon.createRegistryPackage(SUBMISSION_SET, submissionSet.getUuid());
			// submission time
			registryPackage.getSlot().add(RIMBuilderCommon.createSlot("submissionTime", Util.getDate()));
			registryPackage.setName(RIMBuilderCommon.createNameOrDescription("Submission Set"));
			registryPackage.setDescription(RIMBuilderCommon.createNameOrDescription("Submission Set Description"));

			List<SlotType1> slots = new ArrayList<SlotType1>();
			List<ClassificationType> classifications = new ArrayList<ClassificationType>();
			List<ExternalIdentifierType> externalIdentifiers = new ArrayList<ExternalIdentifierType>();
			for (MetadataValue mv : submissionSetMetadata)
			{
				if (mv.getMetadata().getType().equals(Metadata.SLOT))
				{
					slots.add(RIMBuilderCommon.createSlot(mv.getMetadata().getName(), mv.getValue()));
				}
				else if (mv.getMetadata().getType().equals(Metadata.CLASSIFICATION))
				{
					classifications.add(createClassification(mv, SUBMISSION_SET));
					uuids.add(mv.getMetadata().getUuid());
				}
				else if (mv.getMetadata().getType().equals(Metadata.EXTERNAL_IDENTIFIER))
				{
					externalIdentifiers.add(createExternalIdentifier(mv, SUBMISSION_SET));
				}
			}

			for (String uuid : uuids)
			{
				pnr.getSubmitObjectsRequest().getRegistryObjectList().getIdentifiableGroup().add(
						ofrim.createRegistryObjectListTypeObjectRef(createObjectRef(uuid)));
			}
			for (SlotType1 slot : slots){
				registryPackage.getSlot().add(slot);
			}
			for (ClassificationType classification: classifications){
				registryPackage.getClassification().add(classification);
			}
			for (ExternalIdentifierType externalId : externalIdentifiers){
				registryPackage.getExternalIdentifier().add(externalId);
			}
			pnr.getSubmitObjectsRequest().getRegistryObjectList().getIdentifiableGroup().add(
					ofrim.createRegistryObjectListTypeRegistryPackage(registryPackage));

			ClassificationType classification = new ClassificationType();
			classification.setClassificationNode(submissionSet.getUuid());
			classification.setClassifiedObject(SUBMISSION_SET);
			classification.setId(String.valueOf(UUID.randomUUID()));
			pnr.getSubmitObjectsRequest().getRegistryObjectList().getIdentifiableGroup().add(
					ofrim.createRegistryObjectListTypeClassification(classification));

			// Association (link documents to submission set)
			for (PnRRegisteredFile file : filesToLink)
			{
				pnr.getSubmitObjectsRequest().getRegistryObjectList().getIdentifiableGroup().add(
                        ofrim.createRegistryObjectListTypeAssociation(RIMBuilderCommon.createAssociation(
                                "urn:oasis:names:tc:ebxml-regrep:AssociationType:HasMember",
                                SUBMISSION_SET,
					file.getDocumentId(),
					"SubmissionSetStatus",
					"Original",
					"SubmissionSet Status")));
			}
			for (PnRRegisteredFile file: filesToLink)
			{
				pnr.getDocument().add(createDocument(file));
			}

			//------------------------
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			SOAPBuilder.save(baos, pnr);
			String pnrstring = baos.toString();
			pnrstring = this.patchXop(pnrstring);
			Document pnrDoc = Util.string2DOM(pnrstring);
			Node firstDocImportedNode = request.importNode(pnrDoc.getDocumentElement(), true);
			body.appendChild(firstDocImportedNode);
			root.appendChild(body);
			request.appendChild(root);
			return XmlUtil.outputDOM(request);
		}catch(Exception e){
				log.error("An error occurred when building the message: " + e.getMessage());
				e.printStackTrace();
				return null;
		}
	}
	
	private String patchXop(String to){
		String res = to;
		Pattern pat = Pattern.compile("<([^(:<>)]*?:?)Document(.*?)>(\\s)*?<([^(:<>)]*?:?)Include(.*?)>(\\s)*?</([^(:<>)]*?:?)Document>");
		Matcher mat = pat.matcher(to);
		while (mat.find()){
			res = res.replace(mat.group(), "<" + mat.group(1)  + "Document" + mat.group(2) + "><" + mat.group(4) + "Include" + mat.group(5) + "></" + mat.group(7) + "Document>");
		}
		return res;
	}
	
	/**
     *
	 * @param request
	 * @param assertion
	 * @return
	 */
	private Element createSOAPHeader(Document request, Element assertion, Element trcAssertion)
	{
		Element header = request.createElement("s:Header");
		header.setAttribute("xmlns:s", "http://www.w3.org/2003/05/soap-envelope");

		if (assertion != null)
		{
			Element security = request.createElement("wss:Security");
			security.setAttribute("xmlns:wss", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
			Node assertionToken = request.importNode(assertion, true);
			security.appendChild(assertionToken);
			if (trcAssertion != null)
			{
				Node trcAssertionToken = request.importNode(trcAssertion, true);
				security.appendChild(trcAssertionToken);
			}
            header.appendChild(security);
		}

		Element action = request.createElement("a:Action");
		action.setAttribute("s:mustUnderstand", "1");
		action.setTextContent("urn:ihe:iti:2007:ProvideAndRegisterDocumentSet-b");

		header.appendChild(action);

		Element messageID = request.createElement("a:MessageID");
        UUID uuid = UUID.randomUUID();
        messageID.setTextContent("urn:uuid:" + String.valueOf(uuid));
        header.appendChild(messageID);

		Element replyTo = request.createElement("a:ReplyTo");
		Element address = request.createElement("a:Address");
		address.setTextContent("http://www.w3.org/2005/08/addressing/anonymous");
		replyTo.appendChild(address);
		header.appendChild(replyTo);

		Element to = request.createElement("a:To");
		to.setAttribute("s:mustUnderstand", "1");
		to.setTextContent(receiverUrl);
		header.appendChild(to);

		return header;
	}
	
	private Element requestTRCAssertion(Element identityAssertion, SamlAssertionAttributes samlAssertionAttributes)
	{
		try{
			if (praticianID == null || praticianID.isEmpty())
				praticianID = ApplicationConfiguration.getValueOfVariable("default_pratician_id");
			samlAssertionAttributes.setXSPASubjectTRC(patientId);
			samlAssertionAttributes.setXSPAPurposeOfUse("TREATMENT");
			Element assertion = SamlAssertionSupplier.getAssertionTRC(praticianID, samlAssertionAttributes, identityAssertion, SignatureManager.getSignatureUtil());
			return assertion;
		}catch(WSTrustException e){
			log.error("Cannot generate TRC assertion");
			return null;
		}catch(SignatureException e){
			log.error("Cannot sign TRC assertion");
			return null;
		}
	}
	
	/**
	 * Creates the root element Envelope of the message
	 * @param request : Document representing the SOAP message
	 * @return Element : root element off the document
	 */
	private  Element createEnvelope(Document request)
	{
		Element root = request.createElement("s:Envelope");
		root.setAttribute("xmlns:s", "http://www.w3.org/2003/05/soap-envelope");
		root.setAttribute("xmlns:a", "http://www.w3.org/2005/08/addressing");
		return root;
	}
	
	private ExternalIdentifierType createExternalIdentifier (MetadataValue metadataValue, String registryObject)
	{
		ExternalIdentifierType externalIdentifier = new ExternalIdentifierType();
		externalIdentifier.setId(String.valueOf(UUID.randomUUID()));
		externalIdentifier.setIdentificationScheme( metadataValue.getMetadata().getUuid());
		externalIdentifier.setValue(metadataValue.getValue());
		externalIdentifier.setRegistryObject(registryObject);
		InternationalStringType ii = new InternationalStringType();
		LocalizedStringType ll = new LocalizedStringType();
		ll.setValue(metadataValue.getMetadata().getName());
		ii.getLocalizedString().add(ll);
		externalIdentifier.setName(ii);
        return externalIdentifier;
    }

    /**
     * Creates a list of ObjectRef elements according the metadata of type "External Classification Scheme" defined by the user
     * @param request
	 * @return
	 */
	private ObjectRefType createObjectRef(String uuid)
	{
		ObjectRefType objectRef = new ObjectRefType();
		objectRef.setId(uuid);
		return objectRef;
	}
	
	private ClassificationType createClassification(MetadataValue metadataValue, String extrinsicObjectId)
	{
		ClassificationType classification = new ClassificationType();
		classification.setId(String.valueOf(UUID.randomUUID()));
		classification.setClassificationScheme(metadataValue.getMetadata().getUuid());
		classification.setClassifiedObject(extrinsicObjectId);
        if (metadataValue.getValue() != null){
            classification.setNodeRepresentation(metadataValue.getValue());
		} else{
			classification.setNodeRepresentation("");
		}

		//slots
		if (metadataValue.getSlots() != null && !metadataValue.getSlots().isEmpty())
		{
			for (SlotType1 slot : metadataValue.getSlots())
			{
				classification.getSlot().add(slot);
			}
		}
		//name
		if (metadataValue.getLocalizedString() != null)
		{
			classification.setName(new InternationalStringType());
			LocalizedStringType toto = new LocalizedStringType();
			toto.setValue(metadataValue.getLocalizedString());
			classification.getName().getLocalizedString().add(toto);
		}

		return classification;
	}
	
	private DocumentType createDocument(PnRRegisteredFile file)
	{
		DocumentType document = new DocumentType();
		document.setId(file.getDocumentId());
		Include inc = new Include();
		inc.setHref("cid:" + file.getIndex() + ".urn:uuid:" + file.getUuid() + "@ws.jboss.org");
		ObjectFactory of = new ObjectFactory();
		JAXBElement<Include> addd = of.createDocumentTypeInclude(inc);
		document.getMixed().add(addd);
		document.setId(file.getDocumentId());
		return document;
	}
	
	private ExtrinsicObjectType createExtrinsicObject(Metadata objectType, String fileType, String registryObject)
	{
		ExtrinsicObjectType extrinsicObject = new ExtrinsicObjectType();
		extrinsicObject.setId(registryObject);
		extrinsicObject.setObjectType(objectType.getUuid());
		extrinsicObject.setMimeType(fileType);
		extrinsicObject.setStatus("urn:oasis:names:tc:ebxml-regrep:StatusType:Approved");
		return extrinsicObject;
	}
	
	
}
